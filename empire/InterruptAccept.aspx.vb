﻿Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Data.SqlClient
Partial Class InterruptAccept
    Inherits System.Web.UI.Page
    'Dim tdes As New tripleDES
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim IntId As String = Request.QueryString("int")

        If Not (IsPostBack) Then
            If IntId.length > 0 Then
                'getUserInfo(tdes.Decrypt(IntId))                
                getUserInfo(Request.QueryString("int"))
            Else
                'pnlEmailPassword.Visible = False
                lblMessage.Text = "Invalid Interrupt Request."
            End If
        End If
    End Sub
    Public Sub getUserInfo(ByVal IntId As Integer)
        'tdes.Decrypt(Request.QueryString("e"))
        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            'Const SQL As String = "SELECT * FROM dsm_interrupt WHERE Id = @Id"
            Const SQL As String = "SELECT A.id AS Id,A.CustomerId as CustomerId,B.Name AS CustomerName, B.InActive, A.hours, A.date, A.ApproveInterrupt AS Approved,A.StartTime,A.EndTime FROM dsm_interrupt AS A INNER JOIN dsm_customer AS B ON A.CustomerId = B.id WHERE A.id = @Id"
            Dim myCommand As New SqlCommand(SQL, myConnection)
            'lblMessage.Text = SQL
            myConnection.Open()

            myCommand.Parameters.AddWithValue("@Id", IntId)
            Dim myReader As SqlDataReader = myCommand.ExecuteReader

            While myReader.Read
                If myReader("Approved") = False Then
                    lblMessage.Text = "<br/>" & myReader("CustomerName").ToString & " is scheduled to participate in the curtailment duration of " & myReader("hours").ToString & " hours on " & CDate(myReader("date").ToString).ToShortDateString & " from " & myReader("StartTime") & " to " & myReader("EndTime") & "<br/>"
                Else
                    lblMessage.Text = "<br/>Thank you for accepting the curtailment.<br/>"
                    Button1.Enabled = False

                End If
            End While

            myReader.Close()
            myConnection.Close()

        End Using
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQL As String = "update dsm_interrupt set ApproveInterrupt='True' WHERE Id = @Id"
            Dim myCommand As New SqlCommand(SQL, myConnection)

            myConnection.Open()

            myCommand.Parameters.AddWithValue("@Id", Request.QueryString("int"))
            'myCommand.Parameters.AddWithValue("@Id", tdes.Decrypt(Request.QueryString("int")))
            Dim myReader As SqlDataReader = myCommand.ExecuteReader

            While myReader.Read
                lblMessage.Text = myReader("hours").ToString & " " & myReader("date").ToString
            End While

            myReader.Close()
            myConnection.Close()

        End Using
        Response.Redirect(Request.RawUrl)
    End Sub
End Class
