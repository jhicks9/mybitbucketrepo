<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="OutageMap.aspx.vb" Inherits="_OutageMap" Title="Empire District Outage Map" %>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftPlaceHolder" Runat="Server">
    <div style="padding:3px;background-color:#f5f5f5;">
        <div id="divLoadMessage" style="margin:5px auto 5px auto;text-align:center;">
            <img id="imgLoading" src="~/images/slimbox2-loader.gif" style="background-color:transparent;" runat="server" alt="loader" />&nbsp;Loading Outages...
        </div>
        <div style="padding-bottom:2px;text-align:center;">
            <asp:LinkButton ID="lbRefresh" runat="server" OnClientClick="javascript:refresh();" Text='Refresh Map' ToolTip='Refresh Outage Data' />
        </div>
        <div style="margin:3px auto 3px auto;text-align:center;font-size:.85em">Outage information updated every&nbsp;<span id="interval">fifteen</span>&nbsp;minutes.</div>
    </div>
        
    <div style="padding:3px;color:#fff;text-align:center">Total Systemwide Outages</div>
    <div style="padding:15px 3px 15px 3px;background-color:#f5f5f5;">
        <div id='totalColor' style='float:left;background-color:#C4C4C4;width:25px'>&nbsp;</div><div id='totalRange'>&nbsp;No current outages</div>
    </div>
        
    <div style="padding:3px;color:#fff;text-align:center">Options</div>
    <div style="padding-bottom:10px;background-color:#f5f5f5;">
        <asp:CheckBox ID="cbRadar" runat="server" onclick="toggleRadar();" Text="Radar" Checked="false" /><br />
        <asp:CheckBox ID="cbCounty" runat="server" onclick="toggleCounty();" Text="Service Area" Checked="true" /><br />
        <asp:CheckBox ID="cbOutages" runat="server" onclick="toggleOutages();" Text="Outages" Checked="true" />
    </div>

    <div style="padding:3px;color:#fff;text-align:center">Outage Range</div>
    <div style="padding:3px;background-color:#f5f5f5;height:300px">
        <div id='color1' style='float:left;color:#0026FF;background-color:#0026FF;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale1' style='margin-bottom:2px;'>&nbsp;between 10 and 100</div>
        <div id='color2' style='float:left;color:#00FF21;background-color:#00FF21;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale2' style='margin-bottom:2px;'>&nbsp;between 100 and 250</div>
        <div id='color3' style='float:left;color:#FFFF21;background-color:#FFFF21;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale3' style='margin-bottom:2px;'>&nbsp;between 250 and 500</div>
        <div id='color4' style='float:left;color:#FFB218;background-color:#FFB218;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale4' style='margin-bottom:2px;'>&nbsp;between 500 and 1000</div>
        <div id='color5' style='float:left;color:#FF0000;background-color:#FF0000;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale5' style='margin-bottom:2px;'>&nbsp;between 1000 and 2000</div>
        <div id='color6' style='float:left;color:#FF51FF;background-color:#FF51FF;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale6' style='margin-bottom:2px;'>&nbsp;between 2000 and 3000</div>
        <div id='color7' style='float:left;color:#873EF4;background-color:#873EF4;width:25px;margin-bottom:2px;'>&nbsp;</div><div id='scale7' style='margin-bottom:2px;'>&nbsp;more than 3000</div>
        <br />
        <div id="divService" style='float:left;color:#C4C4C4;background-color:#C4C4C4;width:25px;'>&nbsp;</div><div id='service'>&nbsp;Empire Service Area</div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style="width:760px"><div style="padding:3px;color:#fff;background-color:#36587a;font-size:.85em;text-align:center">Outage numbers are estimates based on currently available data and may fluctuate during the restoration process due to the timing of system updates.</div></div>
    <div id='myMap' style='width:760px;height:500px;position:relative;z-index:1;'></div>
    <img id="RadarImage" src="" alt="" style="display:none;" />
    <div id="infoBox" style='visibility:hidden;position:absolute;top:0px;left:0px;background-color:#f5f5f5;border:4px solid #000;font-size:.85em'>
        <table id="ibTable" style="border-spacing:0;border-collapse:collapse">
            <tr><td colspan="2" id="ibTitle" style="text-align:center;white-space:nowrap;padding:3px;font-weight:bold">Outage Information</td></tr>
            <tr><td id="ibArea" style="text-align:center;white-space:nowrap;padding:3px"></td></tr>
            <tr><td style="text-align:center;white-space:nowrap;padding: 0px 3px 3px 3px"><div id="ibRange"></div></td></tr>
        </table>
    </div>
    <script type="text/javascript">
        function refresh() {
            window.location.reload(true);
        }
            </script>      
    <script type="text/javascript" src="https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBfpSajyrqjINoJyVWJbFwmqjypj_SMB8&sensor=false"></script>
    <script type="text/javascript" src="../js/googleInfobox.js"></script>
    <script type="text/javascript">
        var map = null;
        var latMin = 0;var lonMin = 0;var latMax = 0;var lonMax = 0;  //strict panning boundaries
        var center = new Microsoft.Maps.Location(37.1725, -94.05);
        var initView = null;
        var pinLayer = new Microsoft.Maps.EntityCollection();
        var radarLayer = new Microsoft.Maps.EntityCollection();
        radarLayer.setOptions({ visible: false });  //default to not visible -- checkbox will toggle
        var indexRadar;  //save index of radar so it can be removed if toggled
        var saLayer = new Microsoft.Maps.EntityCollection();
        saLayer.setOptions({ visible: false });  //default to not visible -- checkbox will toggle
        var countyLayer = new Microsoft.Maps.EntityCollection();

        var Radar = [];
        var gMarkers = [];  //array to contain all outage markers--can toggle all on/off
        var gZones = [];  //array to contain all highlighted zones--can toggle all on/off
        var gCounties = [];  //array to contain county polygons--gives ability to toggle all on/off
        var gCountyLabels = [];  //array for toggling all county labels
        var strictBounds = null;  //strict panning boundaries
        var gCenter = new google.maps.LatLng('37.1725', '-94.05')  //default Joplin

        function loadGoogleMap() {
            try {
                var mapOptions = {
                    center: gCenter,
                    zoom: 8,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    disableDoubleClickZoom: false,
                    panControl: true,
                    draggable: true,
                    streetViewControl: false
                };

                map = new google.maps.Map(document.getElementById('myMap'), mapOptions);

                google.maps.event.addListener(map, 'zoom_changed', function () {
                    if (map.getZoom() > 12) {
                        map.setZoom(12);
                        //map.setCenter(gCenter);
                    }
                    if (map.getZoom() < 8) {
                        map.setZoom(8);
                        //map.setCenter(gCenter);
                    }
                });

                google.maps.event.addListener(map, 'bounds_changed', function () {
                    if (strictBounds == null) {
                        strictBounds = map.getBounds();
                    }
                });

                google.maps.event.addListener(map, 'center_changed', function () {
                    if (strictBounds.contains(map.getCenter())) return;
                    map.setZoom(8);
                    map.setCenter(gCenter);
                });

                initialize();
                loadGCounty();
            }
            catch (E) { alert(E.message); }
        }

        function getBingMap() {
            map = new Microsoft.Maps.Map(document.getElementById('myMap'),
            { showMapTypeSelector: false,
                credentials: 'AhKzS2kz7-h2jU5EDEeAwBL_PCaaIx2ANTryFwMqw8xlCk6Jc3Y7tuDMjGATRy-P',
                showScalebar: false,
                showDashboard: true,
                enableClickableLogo: false,
                enableSearchLogo: false,
                disableKeyboardInput: true
            });
            map.setView({ zoom: 8, center: center, mapTypeId: Microsoft.Maps.MapTypeId.road});

            map.getZoomRange = function () {
                return {
                    max: 12,
                    min: 8
                };
            };

            //disables map drag/scroll/zoom
            Microsoft.Maps.Events.addHandler(map, 'mousewheel', function (e) {
                e.handled = true;
                return true;
            });

            //disables map drag/scroll/zoom
            //Microsoft.Maps.Events.addHandler(map, 'mousedown', function (e) {
            //    e.handled = true;
            //    return true;
            //});

            Microsoft.Maps.Events.addHandler(map, 'viewchange', function (e) {
                // do not pan outside original boundaries
                var tCenter = map.getTargetBounds();
                if ((tCenter.center.latitude < latMin) || (tCenter.center.latitude > latMax) || (tCenter.center.longitude < lonMin) || (tCenter.center.longitude > lonMax)) {
                    map.setView({ 'zoom': 8, 'center': center });
                }
            });

            Microsoft.Maps.Events.addHandler(map, 'viewchangeend', function (e) {
                //get initial map boundaries
                if (initView == null) {
                    initView = map.getBounds();
                    var topLeft = initView.getNorthwest();
                    var bottomRight = initView.getSoutheast();
                    latMin = bottomRight.latitude;
                    lonMin = topLeft.longitude;
                    latMax = topLeft.latitude;
                    lonMax = bottomRight.longitude;
                }
            });

            //set zoom limits
            var restrictZoom = function () {
                if (map.getZoom() <= map.getZoomRange().min) {
                    map.setView({
                        'zoom': map.getZoomRange().min,
                        'animate': false
                    });
                }
                else if (map.getZoom() >= map.getZoomRange().max) {
                    map.setView({
                        'zoom': map.getZoomRange().max,
                        'animate': false
                    });
                }
            };

            loadCounty();
            initialize();
        }

        function loadCounty() {
            var currentColor = new Microsoft.Maps.Color(100, 196 ,196, 196); //70, 182, 175, 169);

            var pointsBarry = [new Microsoft.Maps.Location(36.926402, -94.064277), new Microsoft.Maps.Location(36.745476, -94.072252), new Microsoft.Maps.Location(36.487168, -94.078317), new Microsoft.Maps.Location(36.485931, -93.854807), new Microsoft.Maps.Location(36.486091, -93.593753), new Microsoft.Maps.Location(36.816707, -93.580590), new Microsoft.Maps.Location(36.819366, -93.622747), new Microsoft.Maps.Location(36.919234, -93.621906), new Microsoft.Maps.Location(36.926402, -94.064277)]
            var pointsBarton = [new Microsoft.Maps.Location(37.638935, -94.077938), new Microsoft.Maps.Location(37.646535, -94.616162), new Microsoft.Maps.Location(37.356976, -94.615952), new Microsoft.Maps.Location(37.343182, -94.086380), new Microsoft.Maps.Location(37.579910, -94.078938), new Microsoft.Maps.Location(37.638935, -94.077938)]
            var pointsBenton = [new Microsoft.Maps.Location(36.485596, -94.614404), new Microsoft.Maps.Location(36.474873, -94.604492), new Microsoft.Maps.Location(36.164511, -94.552932), new Microsoft.Maps.Location(36.106654, -94.542211), new Microsoft.Maps.Location(36.103467, -94.392027), new Microsoft.Maps.Location(36.144407, -94.389445), new Microsoft.Maps.Location(36.143329, -94.335263), new Microsoft.Maps.Location(36.214009, -94.331378), new Microsoft.Maps.Location(36.206677, -94.008804), new Microsoft.Maps.Location(36.236728, -94.006631), new Microsoft.Maps.Location(36.235206, -93.864912), new Microsoft.Maps.Location(36.306989, -93.860625), new Microsoft.Maps.Location(36.485948, -93.857404), new Microsoft.Maps.Location(36.487168, -94.078317), new Microsoft.Maps.Location(36.485596, -94.614404)]
            var pointsCedar = [new Microsoft.Maps.Location(37.579910, -94.078938), new Microsoft.Maps.Location(37.569958, -93.621332), new Microsoft.Maps.Location(37.737614, -93.614484), new Microsoft.Maps.Location(37.739897, -93.636843), new Microsoft.Maps.Location(37.822233, -93.634205), new Microsoft.Maps.Location(37.828902, -93.821956), new Microsoft.Maps.Location(37.884748, -93.820559), new Microsoft.Maps.Location(37.892087, -94.070197), new Microsoft.Maps.Location(37.638935, -94.077938), new Microsoft.Maps.Location(37.579910, -94.078938)]
            var pointsCherokee = [new Microsoft.Maps.Location(37.323905, -94.616163), new Microsoft.Maps.Location(37.331777, -95.068881), new Microsoft.Maps.Location(36.997677, -95.069024), new Microsoft.Maps.Location(36.996926, -95.029944), new Microsoft.Maps.Location(36.993182, -94.617600), new Microsoft.Maps.Location(37.056255, -94.617898), new Microsoft.Maps.Location(37.323905, -94.616163)]
            var pointsChristian = [new Microsoft.Maps.Location(37.094673, -93.614887), new Microsoft.Maps.Location(36.987818, -93.619883), new Microsoft.Maps.Location(36.982364, -93.344049), new Microsoft.Maps.Location(36.813990, -93.345057), new Microsoft.Maps.Location(36.812381, -93.311140), new Microsoft.Maps.Location(36.807494, -92.911652), new Microsoft.Maps.Location(37.067928, -92.899948), new Microsoft.Maps.Location(37.082855, -92.900619), new Microsoft.Maps.Location(37.087436, -93.071135), new Microsoft.Maps.Location(37.094673, -93.614887)]
            var pointsCraig = [new Microsoft.Maps.Location(36.996926, -95.029944), new Microsoft.Maps.Location(36.997677, -95.069024), new Microsoft.Maps.Location(36.996714, -95.403812), new Microsoft.Maps.Location(36.943614, -95.405929), new Microsoft.Maps.Location(36.943295, -95.428297), new Microsoft.Maps.Location(36.600431, -95.426868), new Microsoft.Maps.Location(36.598751, -95.324246), new Microsoft.Maps.Location(36.515157, -95.323112), new Microsoft.Maps.Location(36.513911, -95.010227), new Microsoft.Maps.Location(36.674636, -94.998925), new Microsoft.Maps.Location(36.943101, -95.002397), new Microsoft.Maps.Location(36.967781, -95.034477), new Microsoft.Maps.Location(36.987771, -95.037846), new Microsoft.Maps.Location(36.996926, -95.029944)]
            var pointsDade = [new Microsoft.Maps.Location(37.288284, -94.085324), new Microsoft.Maps.Location(37.284461, -94.054959), new Microsoft.Maps.Location(37.276994, -93.629538), new Microsoft.Maps.Location(37.420069, -93.625908), new Microsoft.Maps.Location(37.569958, -93.621332), new Microsoft.Maps.Location(37.579910, -94.078938), new Microsoft.Maps.Location(37.343182, -94.086380), new Microsoft.Maps.Location(37.288284, -94.085324)]
            var pointsDallas = [new Microsoft.Maps.Location(37.893987, -93.071691), new Microsoft.Maps.Location(37.895932, -93.187828), new Microsoft.Maps.Location(37.793291, -93.193597), new Microsoft.Maps.Location(37.725683, -93.193943), new Microsoft.Maps.Location(37.723574, -93.172887), new Microsoft.Maps.Location(37.409692, -93.185329), new Microsoft.Maps.Location(37.405667, -93.076182), new Microsoft.Maps.Location(37.485352, -93.069457), new Microsoft.Maps.Location(37.483760, -92.852342), new Microsoft.Maps.Location(37.720195, -92.847351), new Microsoft.Maps.Location(37.722146, -92.860742), new Microsoft.Maps.Location(37.889621, -92.856766), new Microsoft.Maps.Location(37.893987, -93.071691)]
            var pointsDelaware = [new Microsoft.Maps.Location(36.666707, -94.618308), new Microsoft.Maps.Location(36.674636, -94.998925), new Microsoft.Maps.Location(36.513911, -95.010227), new Microsoft.Maps.Location(36.167991, -95.013119), new Microsoft.Maps.Location(36.167270, -94.798347), new Microsoft.Maps.Location(36.160696, -94.552224), new Microsoft.Maps.Location(36.474873, -94.604492), new Microsoft.Maps.Location(36.485596, -94.614404), new Microsoft.Maps.Location(36.666707, -94.618308)]
            var pointsGreene = [new Microsoft.Maps.Location(37.420069, -93.625908), new Microsoft.Maps.Location(37.276994, -93.629538), new Microsoft.Maps.Location(37.277810, -93.609742), new Microsoft.Maps.Location(37.094673, -93.614887), new Microsoft.Maps.Location(37.087436, -93.071135), new Microsoft.Maps.Location(37.265820, -93.064629), new Microsoft.Maps.Location(37.267745, -93.080885), new Microsoft.Maps.Location(37.405667, -93.076182), new Microsoft.Maps.Location(37.409692, -93.185329), new Microsoft.Maps.Location(37.420069, -93.625908)]
            var pointsHickory = [new Microsoft.Maps.Location(38.068300, -93.507203), new Microsoft.Maps.Location(37.910465, -93.519024), new Microsoft.Maps.Location(37.911736, -93.576327), new Microsoft.Maps.Location(37.820419, -93.580036), new Microsoft.Maps.Location(37.809561, -93.579429), new Microsoft.Maps.Location(37.793291, -93.193597), new Microsoft.Maps.Location(37.895932, -93.187828), new Microsoft.Maps.Location(37.893987, -93.071691), new Microsoft.Maps.Location(38.056491, -93.064335), new Microsoft.Maps.Location(38.068300, -93.507203)]
            var pointsJasper = [new Microsoft.Maps.Location(37.056255, -94.617898), new Microsoft.Maps.Location(37.048985, -94.062459), new Microsoft.Maps.Location(37.284461, -94.054959), new Microsoft.Maps.Location(37.288284, -94.085324), new Microsoft.Maps.Location(37.343182, -94.086380), new Microsoft.Maps.Location(37.356976, -94.615952), new Microsoft.Maps.Location(37.323905, -94.616163), new Microsoft.Maps.Location(37.197229, -94.616981), new Microsoft.Maps.Location(37.056255, -94.617898)]
            var pointsLawrence = [new Microsoft.Maps.Location(37.284461, -94.054959), new Microsoft.Maps.Location(37.048985, -94.062459), new Microsoft.Maps.Location(36.926402, -94.064277), new Microsoft.Maps.Location(36.922161, -93.790579), new Microsoft.Maps.Location(36.919234, -93.621906), new Microsoft.Maps.Location(36.987838, -93.619853), new Microsoft.Maps.Location(37.094673, -93.614887), new Microsoft.Maps.Location(37.277810, -93.609742), new Microsoft.Maps.Location(37.276994, -93.629538), new Microsoft.Maps.Location(37.284461, -94.054959)]
            var pointsMcDonald = [new Microsoft.Maps.Location(36.759796, -94.619055), new Microsoft.Maps.Location(36.666707, -94.618308), new Microsoft.Maps.Location(36.485596, -94.614404), new Microsoft.Maps.Location(36.487168, -94.078317), new Microsoft.Maps.Location(36.745476, -94.072252), new Microsoft.Maps.Location(36.759796, -94.619055)]
            var pointsNewton = [new Microsoft.Maps.Location(37.056255, -94.617898), new Microsoft.Maps.Location(36.759796, -94.619055), new Microsoft.Maps.Location(36.745476, -94.072252), new Microsoft.Maps.Location(36.926402, -94.064277), new Microsoft.Maps.Location(37.048985, -94.062459), new Microsoft.Maps.Location(37.056255, -94.617898)]
            var pointsOttawa = [new Microsoft.Maps.Location(36.996926, -95.029944), new Microsoft.Maps.Location(36.987771, -95.037846), new Microsoft.Maps.Location(36.967781, -95.034477), new Microsoft.Maps.Location(36.943101, -95.002397), new Microsoft.Maps.Location(36.674636, -94.998925), new Microsoft.Maps.Location(36.666707, -94.618308), new Microsoft.Maps.Location(36.759796, -94.619055), new Microsoft.Maps.Location(36.993182, -94.617600), new Microsoft.Maps.Location(36.996926, -95.029944)]
            var pointsPolk = [new Microsoft.Maps.Location(37.822233, -93.634205), new Microsoft.Maps.Location(37.739897, -93.636843), new Microsoft.Maps.Location(37.737614, -93.614484), new Microsoft.Maps.Location(37.569958, -93.621332), new Microsoft.Maps.Location(37.420069, -93.625908), new Microsoft.Maps.Location(37.409692, -93.185329), new Microsoft.Maps.Location(37.723574, -93.172887), new Microsoft.Maps.Location(37.725683, -93.193943), new Microsoft.Maps.Location(37.793291, -93.193597), new Microsoft.Maps.Location(37.809561, -93.579429), new Microsoft.Maps.Location(37.820419, -93.580036), new Microsoft.Maps.Location(37.822233, -93.634205)]
            var pointsStClair = [new Microsoft.Maps.Location(38.182649, -93.505194), new Microsoft.Maps.Location(38.184450, -93.519955), new Microsoft.Maps.Location(38.204645, -93.522857), new Microsoft.Maps.Location(38.216600, -93.566727), new Microsoft.Maps.Location(38.208833, -93.572795), new Microsoft.Maps.Location(38.197004, -93.561672), new Microsoft.Maps.Location(38.204943, -93.887257), new Microsoft.Maps.Location(38.200087, -93.907859), new Microsoft.Maps.Location(38.207309, -93.920907), new Microsoft.Maps.Location(38.209021, -94.050119), new Microsoft.Maps.Location(37.987367, -94.064650), new Microsoft.Maps.Location(37.892087, -94.070197), new Microsoft.Maps.Location(37.884748, -93.820559), new Microsoft.Maps.Location(37.828902, -93.821956), new Microsoft.Maps.Location(37.822233, -93.634205), new Microsoft.Maps.Location(37.820419, -93.580036), new Microsoft.Maps.Location(37.911736, -93.576327), new Microsoft.Maps.Location(37.910465, -93.519024), new Microsoft.Maps.Location(38.068300, -93.507203), new Microsoft.Maps.Location(38.182649, -93.505194)]
            var pointsStone = [new Microsoft.Maps.Location(36.987838,-93.619853),new Microsoft.Maps.Location(36.919234,-93.621906),new Microsoft.Maps.Location(36.819366,-93.622747),new Microsoft.Maps.Location(36.816707,-93.580590),new Microsoft.Maps.Location(36.486091,-93.593753),new Microsoft.Maps.Location(36.486399,-93.325586),new Microsoft.Maps.Location(36.812381,-93.311140),new Microsoft.Maps.Location(36.813990,-93.345057),new Microsoft.Maps.Location(36.982374,-93.344039),new Microsoft.Maps.Location(36.987838,-93.619853)]
            var pointsTaney = [new Microsoft.Maps.Location(36.812381,-93.311140),new Microsoft.Maps.Location(36.486399,-93.325586),new Microsoft.Maps.Location(36.486074,-92.775137),new Microsoft.Maps.Location(36.802498,-92.769489),new Microsoft.Maps.Location(36.807494,-92.911652),new Microsoft.Maps.Location(36.812381,-93.311140)]
            
            var polyBarry = new Microsoft.Maps.Polygon(pointsBarry, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyBarry);
            var polyBarton = new Microsoft.Maps.Polygon(pointsBarton, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyBarton);
            var polyBenton = new Microsoft.Maps.Polygon(pointsBenton, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyBenton);
            var polyCedar = new Microsoft.Maps.Polygon(pointsCedar, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyCedar);
            var polyCherokee = new Microsoft.Maps.Polygon(pointsCherokee, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyCherokee);
            var polyChristian = new Microsoft.Maps.Polygon(pointsChristian, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyChristian);
            var polyCraig = new Microsoft.Maps.Polygon(pointsCraig, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyCraig);
            var polyDade = new Microsoft.Maps.Polygon(pointsDade, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyDade);
            var polyDallas = new Microsoft.Maps.Polygon(pointsDallas, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyDallas);
            var polyDelaware = new Microsoft.Maps.Polygon(pointsDelaware, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyDelaware);
            var polyGreene = new Microsoft.Maps.Polygon(pointsGreene, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyGreene);
            var polyHickory = new Microsoft.Maps.Polygon(pointsHickory, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyHickory);
            var polyJasper = new Microsoft.Maps.Polygon(pointsJasper, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyJasper);
            var polyLawrence = new Microsoft.Maps.Polygon(pointsLawrence, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyLawrence);
            var polyMcDonald = new Microsoft.Maps.Polygon(pointsMcDonald, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyMcDonald);
            var polyNewton = new Microsoft.Maps.Polygon(pointsNewton, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyNewton);
            var polyOttawa = new Microsoft.Maps.Polygon(pointsOttawa, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyOttawa);
            var polyPolk = new Microsoft.Maps.Polygon(pointsPolk, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyPolk);
            var polyStClair = new Microsoft.Maps.Polygon(pointsStClair, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyStClair);
            var polyStone = new Microsoft.Maps.Polygon(pointsStone, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyStone);
            var polyTaney = new Microsoft.Maps.Polygon(pointsTaney, { fillColor: currentColor, strokeColor: new Microsoft.Maps.Color(200, 100, 100, 100), strokeThickness: 1 });
            countyLayer.push(polyTaney);

            pushpinOptions = { text: 'Barry', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinBarry = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsBarry), pushpinOptions);
            countyLayer.push(pinBarry);
            pushpinOptions = { text: 'Barton', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinBarton = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsBarton), pushpinOptions);
            countyLayer.push(pinBarton);
            pushpinOptions = { text: 'Benton', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinBenton = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsBenton), pushpinOptions);
            countyLayer.push(pinBenton);
            pushpinOptions = { text: 'Cedar', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinCedar = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsCedar), pushpinOptions);
            countyLayer.push(pinCedar);
            pushpinOptions = { text: 'Cherokee', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 18), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinCherokee = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsCherokee), pushpinOptions);
            countyLayer.push(pinCherokee);
            pushpinOptions = { text: 'Christian', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinChristian = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsChristian), pushpinOptions);
            countyLayer.push(pinChristian);
            pushpinOptions = { text: 'Craig', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinCraig = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsCraig), pushpinOptions);
            countyLayer.push(pinCraig);
            pushpinOptions = { text: 'Dade', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-15, -20), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinDade = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsDade), pushpinOptions);
            countyLayer.push(pinDade);
            pushpinOptions = { text: 'Dallas', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinDallas = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsDallas), pushpinOptions);
            countyLayer.push(pinDallas);
            pushpinOptions = { text: 'Delaware', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinDelaware = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsDelaware), pushpinOptions);
            countyLayer.push(pinDelaware);
            pushpinOptions = { text: 'Greene', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinGreene = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsGreene), pushpinOptions);
            countyLayer.push(pinGreene);
            pushpinOptions = { text: 'Hickory', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinHickory = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsHickory), pushpinOptions);
            countyLayer.push(pinHickory);
            pushpinOptions = { text: 'Jasper', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0),textOffset : new Microsoft.Maps.Point(-20,0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='};
            var pinJasper = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsJasper), pushpinOptions);
            countyLayer.push(pinJasper);
            pushpinOptions = { text: 'Lawrence', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-25, 15), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinLawrence = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsLawrence), pushpinOptions);
            countyLayer.push(pinLawrence);
            pushpinOptions = { text: 'McDonald', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinMcDonald = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsMcDonald), pushpinOptions);
            countyLayer.push(pinMcDonald);
            pushpinOptions = { text: 'Newton', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 10), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinNewton = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsNewton), pushpinOptions);
            countyLayer.push(pinNewton);
            pushpinOptions = { text: 'Ottawa', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinOttawa = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsOttawa), pushpinOptions);
            countyLayer.push(pinOttawa);
            pushpinOptions = { text: 'Polk', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinPolk = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsPolk), pushpinOptions);
            countyLayer.push(pinPolk);
            pushpinOptions = { text: 'St Clair', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinStClair = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsStClair), pushpinOptions);
            countyLayer.push(pinStClair);
            pushpinOptions = { text: 'Stone', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinStone = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsStone), pushpinOptions);
            countyLayer.push(pinStone);
            pushpinOptions = { text: 'Taney', typeName: 'labelPin', draggable: false, visible: true, anchor: new Microsoft.Maps.Point(0, 0), textOffset: new Microsoft.Maps.Point(-20, 0), icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' };
            var pinTaney = new Microsoft.Maps.Pushpin(simplePolygonCentroid(pointsTaney), pushpinOptions);
            countyLayer.push(pinTaney);

            map.entities.push(countyLayer);
        }

        function loadGCounty() {
            var pointsBarry = [new google.maps.LatLng(36.926402, -94.064277), new google.maps.LatLng(36.745476, -94.072252), new google.maps.LatLng(36.487168, -94.078317), new google.maps.LatLng(36.485931, -93.854807), new google.maps.LatLng(36.486091, -93.593753), new google.maps.LatLng(36.816707, -93.580590), new google.maps.LatLng(36.819366, -93.622747), new google.maps.LatLng(36.919234, -93.621906), new google.maps.LatLng(36.926402, -94.064277)]
            var pointsBarton = [new google.maps.LatLng(37.638935, -94.077938), new google.maps.LatLng(37.646535, -94.616162), new google.maps.LatLng(37.356976, -94.615952), new google.maps.LatLng(37.343182, -94.086380), new google.maps.LatLng(37.579910, -94.078938), new google.maps.LatLng(37.638935, -94.077938)]
            var pointsBenton = [new google.maps.LatLng(36.485596, -94.614404), new google.maps.LatLng(36.474873, -94.604492), new google.maps.LatLng(36.164511, -94.552932), new google.maps.LatLng(36.106654, -94.542211), new google.maps.LatLng(36.103467, -94.392027), new google.maps.LatLng(36.144407, -94.389445), new google.maps.LatLng(36.143329, -94.335263), new google.maps.LatLng(36.214009, -94.331378), new google.maps.LatLng(36.206677, -94.008804), new google.maps.LatLng(36.236728, -94.006631), new google.maps.LatLng(36.235206, -93.864912), new google.maps.LatLng(36.306989, -93.860625), new google.maps.LatLng(36.485948, -93.857404), new google.maps.LatLng(36.487168, -94.078317), new google.maps.LatLng(36.485596, -94.614404)]
            var pointsCedar = [new google.maps.LatLng(37.579910, -94.078938), new google.maps.LatLng(37.569958, -93.621332), new google.maps.LatLng(37.737614, -93.614484), new google.maps.LatLng(37.739897, -93.636843), new google.maps.LatLng(37.822233, -93.634205), new google.maps.LatLng(37.828902, -93.821956), new google.maps.LatLng(37.884748, -93.820559), new google.maps.LatLng(37.892087, -94.070197), new google.maps.LatLng(37.638935, -94.077938), new google.maps.LatLng(37.579910, -94.078938)]
            var pointsCherokee = [new google.maps.LatLng(37.323905, -94.616163), new google.maps.LatLng(37.331777, -95.068881), new google.maps.LatLng(36.997677, -95.069024), new google.maps.LatLng(36.996926, -95.029944), new google.maps.LatLng(36.993182, -94.617600), new google.maps.LatLng(37.056255, -94.617898), new google.maps.LatLng(37.323905, -94.616163)]
            var pointsChristian = [new google.maps.LatLng(37.094673, -93.614887), new google.maps.LatLng(36.987818, -93.619883), new google.maps.LatLng(36.982364, -93.344049), new google.maps.LatLng(36.813990, -93.345057), new google.maps.LatLng(36.812381, -93.311140), new google.maps.LatLng(36.807494, -92.911652), new google.maps.LatLng(37.067928, -92.899948), new google.maps.LatLng(37.082855, -92.900619), new google.maps.LatLng(37.087436, -93.071135), new google.maps.LatLng(37.094673, -93.614887)]
            var pointsCraig = [new google.maps.LatLng(36.996926, -95.029944), new google.maps.LatLng(36.997677, -95.069024), new google.maps.LatLng(36.996714, -95.403812), new google.maps.LatLng(36.943614, -95.405929), new google.maps.LatLng(36.943295, -95.428297), new google.maps.LatLng(36.600431, -95.426868), new google.maps.LatLng(36.598751, -95.324246), new google.maps.LatLng(36.515157, -95.323112), new google.maps.LatLng(36.513911, -95.010227), new google.maps.LatLng(36.674636, -94.998925), new google.maps.LatLng(36.943101, -95.002397), new google.maps.LatLng(36.967781, -95.034477), new google.maps.LatLng(36.987771, -95.037846), new google.maps.LatLng(36.996926, -95.029944)]
            var pointsDade = [new google.maps.LatLng(37.288284, -94.085324), new google.maps.LatLng(37.284461, -94.054959), new google.maps.LatLng(37.276994, -93.629538), new google.maps.LatLng(37.420069, -93.625908), new google.maps.LatLng(37.569958, -93.621332), new google.maps.LatLng(37.579910, -94.078938), new google.maps.LatLng(37.343182, -94.086380), new google.maps.LatLng(37.288284, -94.085324)]
            var pointsDallas = [new google.maps.LatLng(37.893987, -93.071691), new google.maps.LatLng(37.895932, -93.187828), new google.maps.LatLng(37.793291, -93.193597), new google.maps.LatLng(37.725683, -93.193943), new google.maps.LatLng(37.723574, -93.172887), new google.maps.LatLng(37.409692, -93.185329), new google.maps.LatLng(37.405667, -93.076182), new google.maps.LatLng(37.485352, -93.069457), new google.maps.LatLng(37.483760, -92.852342), new google.maps.LatLng(37.720195, -92.847351), new google.maps.LatLng(37.722146, -92.860742), new google.maps.LatLng(37.889621, -92.856766), new google.maps.LatLng(37.893987, -93.071691)]
            var pointsDelaware = [new google.maps.LatLng(36.666707, -94.618308), new google.maps.LatLng(36.674636, -94.998925), new google.maps.LatLng(36.513911, -95.010227), new google.maps.LatLng(36.167991, -95.013119), new google.maps.LatLng(36.167270, -94.798347), new google.maps.LatLng(36.160696, -94.552224), new google.maps.LatLng(36.474873, -94.604492), new google.maps.LatLng(36.485596, -94.614404), new google.maps.LatLng(36.666707, -94.618308)]
            var pointsGreene = [new google.maps.LatLng(37.420069, -93.625908), new google.maps.LatLng(37.276994, -93.629538), new google.maps.LatLng(37.277810, -93.609742), new google.maps.LatLng(37.094673, -93.614887), new google.maps.LatLng(37.087436, -93.071135), new google.maps.LatLng(37.265820, -93.064629), new google.maps.LatLng(37.267745, -93.080885), new google.maps.LatLng(37.405667, -93.076182), new google.maps.LatLng(37.409692, -93.185329), new google.maps.LatLng(37.420069, -93.625908)]
            var pointsHickory = [new google.maps.LatLng(38.068300, -93.507203), new google.maps.LatLng(37.910465, -93.519024), new google.maps.LatLng(37.911736, -93.576327), new google.maps.LatLng(37.820419, -93.580036), new google.maps.LatLng(37.809561, -93.579429), new google.maps.LatLng(37.793291, -93.193597), new google.maps.LatLng(37.895932, -93.187828), new google.maps.LatLng(37.893987, -93.071691), new google.maps.LatLng(38.056491, -93.064335), new google.maps.LatLng(38.068300, -93.507203)]
            var pointsJasper = [new google.maps.LatLng(37.056255, -94.617898), new google.maps.LatLng(37.048985, -94.062459), new google.maps.LatLng(37.284461, -94.054959), new google.maps.LatLng(37.288284, -94.085324), new google.maps.LatLng(37.343182, -94.086380), new google.maps.LatLng(37.356976, -94.615952), new google.maps.LatLng(37.323905, -94.616163), new google.maps.LatLng(37.197229, -94.616981), new google.maps.LatLng(37.056255, -94.617898)]
            var pointsLawrence = [new google.maps.LatLng(37.284461, -94.054959), new google.maps.LatLng(37.048985, -94.062459), new google.maps.LatLng(36.926402, -94.064277), new google.maps.LatLng(36.922161, -93.790579), new google.maps.LatLng(36.919234, -93.621906), new google.maps.LatLng(36.987838, -93.619853), new google.maps.LatLng(37.094673, -93.614887), new google.maps.LatLng(37.277810, -93.609742), new google.maps.LatLng(37.276994, -93.629538), new google.maps.LatLng(37.284461, -94.054959)]
            var pointsMcDonald = [new google.maps.LatLng(36.759796, -94.619055), new google.maps.LatLng(36.666707, -94.618308), new google.maps.LatLng(36.485596, -94.614404), new google.maps.LatLng(36.487168, -94.078317), new google.maps.LatLng(36.745476, -94.072252), new google.maps.LatLng(36.759796, -94.619055)]
            var pointsNewton = [new google.maps.LatLng(37.056255, -94.617898), new google.maps.LatLng(36.759796, -94.619055), new google.maps.LatLng(36.745476, -94.072252), new google.maps.LatLng(36.926402, -94.064277), new google.maps.LatLng(37.048985, -94.062459), new google.maps.LatLng(37.056255, -94.617898)]
            var pointsOttawa = [new google.maps.LatLng(36.996926, -95.029944), new google.maps.LatLng(36.987771, -95.037846), new google.maps.LatLng(36.967781, -95.034477), new google.maps.LatLng(36.943101, -95.002397), new google.maps.LatLng(36.674636, -94.998925), new google.maps.LatLng(36.666707, -94.618308), new google.maps.LatLng(36.759796, -94.619055), new google.maps.LatLng(36.993182, -94.617600), new google.maps.LatLng(36.996926, -95.029944)]
            var pointsPolk = [new google.maps.LatLng(37.822233, -93.634205), new google.maps.LatLng(37.739897, -93.636843), new google.maps.LatLng(37.737614, -93.614484), new google.maps.LatLng(37.569958, -93.621332), new google.maps.LatLng(37.420069, -93.625908), new google.maps.LatLng(37.409692, -93.185329), new google.maps.LatLng(37.723574, -93.172887), new google.maps.LatLng(37.725683, -93.193943), new google.maps.LatLng(37.793291, -93.193597), new google.maps.LatLng(37.809561, -93.579429), new google.maps.LatLng(37.820419, -93.580036), new google.maps.LatLng(37.822233, -93.634205)]
            var pointsStClair = [new google.maps.LatLng(38.182649, -93.505194), new google.maps.LatLng(38.184450, -93.519955), new google.maps.LatLng(38.204645, -93.522857), new google.maps.LatLng(38.216600, -93.566727), new google.maps.LatLng(38.208833, -93.572795), new google.maps.LatLng(38.197004, -93.561672), new google.maps.LatLng(38.204943, -93.887257), new google.maps.LatLng(38.200087, -93.907859), new google.maps.LatLng(38.207309, -93.920907), new google.maps.LatLng(38.209021, -94.050119), new google.maps.LatLng(37.987367, -94.064650), new google.maps.LatLng(37.892087, -94.070197), new google.maps.LatLng(37.884748, -93.820559), new google.maps.LatLng(37.828902, -93.821956), new google.maps.LatLng(37.822233, -93.634205), new google.maps.LatLng(37.820419, -93.580036), new google.maps.LatLng(37.911736, -93.576327), new google.maps.LatLng(37.910465, -93.519024), new google.maps.LatLng(38.068300, -93.507203), new google.maps.LatLng(38.182649, -93.505194)]
            var pointsStone = [new google.maps.LatLng(36.987838, -93.619853), new google.maps.LatLng(36.919234, -93.621906), new google.maps.LatLng(36.819366, -93.622747), new google.maps.LatLng(36.816707, -93.580590), new google.maps.LatLng(36.486091, -93.593753), new google.maps.LatLng(36.486399, -93.325586), new google.maps.LatLng(36.812381, -93.311140), new google.maps.LatLng(36.813990, -93.345057), new google.maps.LatLng(36.982374, -93.344039), new google.maps.LatLng(36.987838, -93.619853)]
            var pointsTaney = [new google.maps.LatLng(36.812381, -93.311140), new google.maps.LatLng(36.486399, -93.325586), new google.maps.LatLng(36.486074, -92.775137), new google.maps.LatLng(36.802498, -92.769489), new google.maps.LatLng(36.807494, -92.911652), new google.maps.LatLng(36.812381, -93.311140)]
            createCountyLabel(0,pointsBarry, map, 'Barry',0,0);
            createCountyLabel(1,pointsBarton, map, 'Barton',0,0);
            createCountyLabel(2,pointsBenton, map, 'Benton',0,-20);
            createCountyLabel(3,pointsCedar, map, 'Cedar',0,0);
            createCountyLabel(4,pointsCherokee, map, 'Cherokee',0,0);
            createCountyLabel(5,pointsChristian, map, 'Christian',20,0);
            createCountyLabel(6,pointsCraig, map, 'Craig',0,0);
            createCountyLabel(7,pointsDade, map, 'Dade', 0,0);
            createCountyLabel(8, pointsDallas, map, 'Dallas', 0, 0);
            createCountyLabel(9, pointsDelaware, map, 'Delaware', 0, 0);
            createCountyLabel(10, pointsGreene, map, 'Greene', 0, -30);
            createCountyLabel(11, pointsHickory, map, 'Hickory', 0, -20);
            createCountyLabel(12, pointsJasper, map, 'Jasper', 0, 0);
            createCountyLabel(13, pointsLawrence, map, 'Lawrence', 0, 0);
            createCountyLabel(14, pointsMcDonald, map, 'McDonald', 0, 0);
            createCountyLabel(15, pointsNewton, map, 'Newton', 0, 10);
            createCountyLabel(16, pointsOttawa, map, 'Ottawa', 0, 0);
            createCountyLabel(17, pointsPolk, map, 'Polk', 0, 0);
            createCountyLabel(18, pointsStClair, map, 'St Clair', 0, 0);
            createCountyLabel(19, pointsStone, map, 'Stone', 0, 0);
            createCountyLabel(20, pointsTaney, map, 'Taney', 0, 0);
            toggleCounty();
        }

        function createCountyLabel(index, points, map, desc, offsetX, offsetY) {
            var currentColor = "#c4c4c4"
            var poly = new google.maps.Polygon({ paths: points, clickable: false, strokeColor: '#404040', strokeOpacity: 0.8, strokeWeight: 1, fillColor: currentColor, fillOpacity: 0.40 });
            gCounties[index] = poly;
            var i;var bounds = new google.maps.LatLngBounds(); for (i = 0; i < points.length; i++) { bounds.extend(points[i]); }
            var ibOptions = { content: desc, boxStyle: { textAlign: 'center', color: '#303030', fontSize: '.85em !important', width: '50px', fontWeight: 'bold' }, pixelOffset: new google.maps.Size(offsetX-25, offsetY), position: bounds.getCenter(), closeBoxURL: '', pane: 'mapPane' };
            var ib = new InfoBox(ibOptions);
            ib.open(map);
            gCountyLabels[index] = ib;
        }

        function loadRadar() {
            // add radar to map
            //Register and load the Image Overlay Module
            Microsoft.Maps.registerModule("ImageOverlayModule", "../js/bingImageOverlay.js");
            Microsoft.Maps.loadModule("ImageOverlayModule", { callback: function () {
                var radarImageRect = Microsoft.Maps.LocationRect.fromCorners(new Microsoft.Maps.Location(39.715140, -96.106013), new Microsoft.Maps.Location(34.745826, -90.684949));
                var imgRadar = ImageOverlay(map, 'RadarImage.ashx?product=N0R&rid=SGF&frames=10&rnd=' + Math.random(), radarImageRect);
                imgRadar.SetOpacity(0.3);
                map.entities.push(imgRadar);
                indexRadar = map.entities.indexOf(imgRadar);
                }
            });
        }

        function loadGRadar() {
            var bounds;
            bounds = new google.maps.LatLngBounds(new google.maps.LatLng(34.745826, -96.106013), new google.maps.LatLng(39.715140, -90.684949));
            var srcImage = document.getElementById("RadarImage").src = 'RadarImage.ashx?product=N0R&rid=SGF&frames=10&rnd=' + Math.random();
            Radar[0] = new USGSOverlay(bounds, srcImage, map, 30, 99);
        }

        function pinMouseOver(e) {
            displayInfobox(e);
        }
        function pinMouseOut(e) {
            hideInfoBox(e);
        }

        function hideInfoBox(e) {
            var infoBox = document.getElementById('infoBox');
            infoBox.style.visibility = "hidden";
        }

        function displayGInfoBox(marker, range, area, num) {
            var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
            var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
            var scale = Math.pow(2, map.getZoom());
            var worldPoint = map.getProjection().fromLatLngToPoint(marker.getPosition());
            var point = new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
            var borderColor = '#f5f5f5';
            if (range == "between 10 and 100") {    //blue
                borderColor = '#0026FF';
            }
            if (range == "between 100 and 250") {   //green
                borderColor = '#00FF21';
            }
            if (range == "between 250 and 500") {   //yellow
                borderColor = '#FFFF21';
            }
            if (range == "between 500 and 1000") {  //orange
                borderColor = '#FFB218';
            }
            if (range == "between 1000 and 2000") { //red
                borderColor = '#FF0000';
            }
            if (range == "between 2000 and 3000") { //pink
                borderColor = '#FF51FF';
            }
            if (range == "more than 3000") {        //purple
                borderColor = '#873EF4';
            }
            document.getElementById("ibTitle").style.backgroundColor = borderColor;
            if (borderColor == '#0026FF') { document.getElementById("ibTitle").style.color = '#fff'; } else { document.getElementById("ibTitle").style.color = '#222' };  //change text color of Title if background is blue
            document.getElementById("ibArea").innerHTML = area + ' area';
            document.getElementById("ibRange").innerHTML = num + ' customers affected';

            var infoBox = document.getElementById('infoBox');
            if ((point.x + getDivW(infoBox) + 100) > (960 - getDivW(infoBox))) {  // move infoBox to the left if it falls wider than map
                infoBox.style.left = (point.x - getDivW(infoBox) - 20) + "px";
            }
            else {
                infoBox.style.left = (point.x + 20) + "px";
            };
            infoBox.style.top = (point.y - 45) + "px";
            infoBox.style.borderColor = borderColor;
            infoBox.style.visibility = "visible";
            document.getElementById('myMap').appendChild(infoBox);
        }

        function displayInfobox(e) {
            var borderColor = '#f5f5f5';
            if (e.target.range == "between 10 and 100") {    //blue
                scaleDiv = document.getElementById('scale1');
                borderColor = '#0026FF';
            }
            if (e.target.range == "between 100 and 250") {   //green
                scaleDiv = document.getElementById('scale2');
                borderColor = '#00FF21';
            }
            if (e.target.range == "between 250 and 500") {   //yellow
                scaleDiv = document.getElementById('scale3');
                borderColor = '#FFFF21';
            }
            if (e.target.range == "between 500 and 1000") {  //orange
                scaleDiv = document.getElementById('scale4');
                borderColor = '#FFB218';
            }
            if (e.target.range == "between 1000 and 2000") { //red
                scaleDiv = document.getElementById('scale5');
                borderColor = '#FF0000';
            }
            if (e.target.range == "between 2000 and 3000") { //pink
                scaleDiv = document.getElementById('scale6');
                borderColor = '#FF51FF';
            }
            if (e.target.range == "more than 3000") {        //purple
                scaleDiv = document.getElementById('scale7');
                borderColor = '#873EF4';
            }
            document.getElementById("ibTitle").style.backgroundColor = borderColor;
            if (borderColor == '#0026FF') { document.getElementById("ibTitle").style.color = '#fff'; } else { document.getElementById("ibTitle").style.color = '#222' };  //change text color of Title if background is blue
            document.getElementById("ibArea").innerHTML = e.target.area;
            document.getElementById("ibRange").innerHTML = e.target.num;

            var pix = map.tryLocationToPixel(e.target.getLocation(), Microsoft.Maps.PixelReference.control);
            var infoBox = document.getElementById('infoBox');
            if ((pix.x + getDivW(infoBox) + 100) > (960 - getDivW(infoBox))) {  // move infoBox to the left if it falls wider than map
                infoBox.style.left = (pix.x - getDivW(infoBox) - 20) + "px";
            }
            else {
                infoBox.style.left = (pix.x + 20) + "px";
            };
            infoBox.style.top = (pix.y - 45) + "px";
            infoBox.style.borderColor = borderColor;
            infoBox.style.visibility = "visible";
            document.getElementById('myMap').appendChild(infoBox);
        }

        function getDivW(el) {
            return (el ? (el.offsetWidth || el.style.pixelWidth || 0) : 0);
        }

        function closeMapWindow() { window.close(); }

        function toggleRadar() {
            var useBingMap = isBingMap();
            if (useBingMap == true) {
                if (!radarLayer.getVisible()) {
                    radarLayer.setOptions({ visible: true });
                    loadRadar();
                }
                else {
                    radarLayer.setOptions({ visible: false });
                    if (indexRadar != -1) {  //radar exists
                        map.entities.removeAt(indexRadar);  //remove radar
                    }
                };
            } else {   // load custom overlay radar image
                if (Radar.length > 0) {  // if image already loaded
                    Radar[0].toggle();  // toggle image on/off
                } else {
                    loadGRadar();  //load radar image
                }
            };
        }

        function toggleSArea() {
            saLayer.setOptions({ visible: !saLayer.getVisible() });
        }

        function toggleOutages() {
            var useBingMap = isBingMap();
            if (useBingMap == true) {
                pinLayer.setOptions({ visible: !pinLayer.getVisible() });
            } else {
                for (var i = 0; i < gMarkers.length; i++) {
                    gMarkers[i].setVisible(!gMarkers[i].getVisible());  //toggle markers on/off
                    if (!gMarkers[i].getVisible()) {  //toggle zones on/of by removing from the map
                        gZones[i].setMap(null);
                    } else {
                        gZones[i].setMap(map);
                    }
                }
            }
        }

        function toggleCounty() {
            var useBingMap = isBingMap();
            if (useBingMap == true) {
                countyLayer.setOptions({ visible: !countyLayer.getVisible() });
            } else {
                for (var i = 0; i < gCounties.length; i++) {
                    if (gCounties[i].getMap()) {  //if poly is on the google map
                        gCounties[i].setMap(null);
                        gCountyLabels[i].hide();
                    } else {
                        gCounties[i].setMap(map);
                        gCountyLabels[i].show();
                    }
                }
            }
        }

        function simplePolygonCentroid(points) { // param can be either a Bing Map v7 Point class or Location class
            var sumY = 0; var sumX = 0; var partialSum = 0; var sum = 0;
            points.push(points[0]);  //close polygon
            var n = points.length;
            for (var i=0; i < n-1; i++){
                partialSum = points[i].longitude * points[i+1].latitude - points[i+1].longitude * points[i].latitude;
                sum += partialSum;
                sumX += (points[i].longitude + points[i + 1].longitude) * partialSum;
                sumY += (points[i].latitude + points[i + 1].latitude) * partialSum;
            }
            var area = 0.5 * sum;
            return new Microsoft.Maps.Location(sumY / 6 / area, sumX / 6 / area);
        }

        function isBingMap() {
            var today = new Date();
            var mm = today.getMonth() + 1; //January is 0! -- months are a zero starting array
            if ((mm % 2 == 0)) {  //if even month
                return true;  //use bing map
            } else {
                return false;  //use google map
            }
        }

        window.onload = function () {
            var useBingMap = isBingMap();
            if (useBingMap == true) {
                getBingMap();
            } else {
                loadGoogleMap();
            }
        }
        window.onunload = function () {
            GUnload();
            if (window.detwin && !window.detwin.closed) {
                window.detwin.close();
                window.detwin = null;
            }
        }

        USGSOverlay.prototype = new google.maps.OverlayView();
        function USGSOverlay(bounds, image, map, opacity, zindex) {
            // Now initialize all properties.
            this.bounds_ = bounds;
            this.image_ = image;
            this.map_ = map;
            this.opacity_ = opacity;
            this.zindex_ = zindex;

            // We define a property to hold the image's div. We'll
            // actually create this div upon receipt of the onAdd()
            // method so we'll leave it null for now.
            this.div_ = null;

            // Explicitly call setMap on this overlay
            this.setMap(map);
        }

        USGSOverlay.prototype.onAdd = function () {
            // Note: an overlay's receipt of onAdd() indicates that
            // the map's panes are now available for attaching
            // the overlay to the map via the DOM.

            // Create the DIV and set some basic attributes.
            var div = document.createElement('div');
            div.style.borderStyle = 'none';
            div.style.borderWidth = '0px';
            div.style.position = 'absolute';
            div.style.zIndex = this.zindex_;

            // Create an IMG element and attach it to the DIV.
            var img = document.createElement('img');
            img.src = this.image_;
            img.style.width = '100%';
            img.style.height = '100%';
            img.style.position = 'absolute';
            div.appendChild(img);

            // Set the overlay's div_ property to this DIV
            this.div_ = div;
            this.setDivOpacity(this.opacity_);

            // We add an overlay to a map via one of the map's panes.
            // We'll add this overlay to the overlayLayer pane.
            var panes = this.getPanes();
            panes.overlayLayer.appendChild(div);
        }

        USGSOverlay.prototype.draw = function () {
            // Size and position the overlay. We use a southwest and northeast
            // position of the overlay to peg it to the correct position and size.
            // We need to retrieve the projection from this overlay to do this.
            var overlayProjection = this.getProjection();

            // Retrieve the southwest and northeast coordinates of this overlay
            // in latlngs and convert them to pixels coordinates.
            // We'll use these coordinates to resize the DIV.
            var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
            var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

            // Resize the image's DIV to fit the indicated dimensions.
            var div = this.div_;
            div.style.left = sw.x + 'px';
            div.style.top = ne.y + 'px';
            div.style.width = (ne.x - sw.x) + 'px';
            div.style.height = (sw.y - ne.y) + 'px';
        }

        USGSOverlay.prototype.onRemove = function () {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        }

        USGSOverlay.prototype.show = function () {
            if (this.div_) {
                this.div_.style.visibility = "visible";
            }
        }

        USGSOverlay.prototype.hide = function () {  // Set the visibility to 'hidden' or 'visible'.
            if (this.div_) {  // The visibility property must be a string enclosed in quotes.
                this.div_.style.visibility = 'hidden';
            }
        }

        USGSOverlay.prototype.toggle = function () {
            if (this.div_) {
                if (this.div_.style.visibility == 'hidden') {
                    this.show();
                } else {
                    this.hide();
                }
            }
        }

        USGSOverlay.prototype.setDivOpacity = function (opacity) {
            if (opacity < 0) {
                opacity = 0;
            }
            if (opacity > 100) {
                opacity = 100;
            }
            var c = opacity / 100;
            if (typeof (this.div_.style.filter) == 'string') {
                this.div_.style.filter = 'alpha(opacity:' + opacity + ')';
            }
            if (typeof (this.div_.style.KHTMLOpacity) == 'string') {
                this.div_.style.KHTMLOpacity = c;
            }
            if (typeof (this.div_.style.MozOpacity) == 'string') {
                this.div_.style.MozOpacity = c;
            }
            if (typeof (this.div_.style.opacity) == 'string') {
                this.div_.style.opacity = c;
            }
        }
    </script>
</asp:Content>