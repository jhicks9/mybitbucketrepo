﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="OutageReport.aspx.vb" Inherits="Outages_OutageReport" Title="Report an Outage" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MultiView1" ActiveViewIndex="0" runat="server">
                <asp:View ID="vwReport" runat="server">
                        
                        <table cellpadding="3" cellspacing="0">
                        <tr>
                            <td></td>
                            <td>Outages can also be reported via telephone at <span style="font-weight:bold;color:#113962">800-206-2300</span>.</td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblContactName" Text="* Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtContactName" runat="server" Width="300px" ValidationGroup="avg"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvContactName" runat="server" ControlToValidate="txtContactName" ErrorMessage="name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblPrimaryPhone" Text="* Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblAddress" Text="* Outage Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:Label ID="lblState" Text="State:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="txtState" ErrorMessage="State required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revState" runat="server" ControlToValidate="txtState"
                                    Display="Dynamic" ErrorMessage="required" ValidationExpression="^(([Oo][Kk])|([Kk][Ss])|([Mm][Oo])|([Aa][Rr]))$" ValidationGroup="avg" />
                                <asp:Label ID="lblZip" Text="Zip:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtZip" runat="server" ValidationGroup="avg" MaxLength="10" Width="75px" />
                                <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revZip" runat="server" ControlToValidate="txtZip"
                                    Display="Dynamic" ErrorMessage="5 digit or zip + 4 " ValidationExpression="\d{5}(-\d{4})?" ValidationGroup="avg" />
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLocationNumber" Text="11-digit location number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCustToken" runat="server" Width="72px" ValidationGroup="avg" MaxLength="6" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCustToken" runat="server" ControlToValidate="txtCustToken" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtAcctToken" runat="server" Width="30px" ValidationGroup="avg" MaxLength="2" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revAcctToken" runat="server" ControlToValidate="txtAcctToken" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtAPkgToken" runat="server" Width="30px" ValidationGroup="avg" MaxLength="3" />
                                <asp:RegularExpressionValidator ID="revAPkgToken" runat="server" ControlToValidate="txtAPkgToken" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                            <td align="right" valign="top"><asp:label ID="lblProblems" runat="server" Text="Problems:&nbsp;" Font-Bold="true" /></td>
                            <td>
                                <asp:CheckBoxList ID="cblProblems" runat="server" RepeatColumns="3" >
                                        <asp:ListItem>Structure fire</asp:ListItem>
                                        <asp:ListItem>Lights off</asp:ListItem>
                                        <asp:ListItem>Lights dim/bright</asp:ListItem>
                                        <asp:ListItem>Tree in line</asp:ListItem>
                                        <asp:ListItem>Pole down</asp:ListItem>
                                        <asp:ListItem>Wire down</asp:ListItem>
                                        <asp:ListItem>Lights blinking</asp:ListItem>
                                        <asp:ListItem>Lights off again</asp:ListItem>
                                        <asp:ListItem>Partial power</asp:ListItem>
                                        <asp:ListItem>Fire</asp:ListItem>
                                        <asp:ListItem>Flash</asp:ListItem>
                                        <asp:ListItem>Lights on</asp:ListItem>
                                        <asp:ListItem>Arcs</asp:ListItem>
                                        <asp:ListItem>Noise from equipment</asp:ListItem>
                                        <asp:ListItem>Trees/limbs sparking</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top"><asp:label ID="lblComments" runat="server" Text="Comments:&nbsp;" Font-Bold="true" /></td>
                            <td><asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Height="100px" Width="450px" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" ValidationGroup="avg" OnClick="lbSubmit_Click"><span>Report Outage</span></asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td align="right">* (indicates required fields)</td>
                            <td></td>
                        </tr>
                        </table>
                </asp:View>
            
                <asp:View ID="vwEmailSent" runat="server">
                    <div style="clear:both;width:175px;">
                        <asp:label ID="lblStatus" Font-Bold="True" ForeColor="Red" Text="" runat="server"></asp:label>
                    </div>    
                    <div>&nbsp;</div>                
                </asp:View>
            </asp:MultiView>
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

