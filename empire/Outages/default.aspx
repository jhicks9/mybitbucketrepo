<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="_default" Title="Outage Center" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="RightPlaceHolder">
    <div class="landingtext">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="middle" align="center" style="width:16px"><asp:Image ID="imgOutageMap" ImageAlign="Top" CssClass="icon" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" Visible="false" /></td>
                            <td>
                                <div class="links" style="width:100px">
                                    <asp:HyperLink ID="hlOutageMap" runat="server" NavigateUrl="~/Outages/OutageMap.aspx" Text="Outage Map" Font-Underline="true" Visible="false" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblOutageMap" runat="server" Text="Locate outages affecting Empire customers.<br />" Visible="false" />
                            </td>
                        </tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr>
                            <td valign="middle" align="center" style="width:16px"><asp:Image ID="imgDocument2" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                            <td>
                                <div class="links" style="width:100px">
                                    <asp:HyperLink ID="hlOutageReport" runat="server" NavigateUrl="~/Outages/OutageReport.aspx" Text="Report Outage" Font-Underline="true" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">Please contact if you are experiencing a service interruption.</td>
                        </tr>
                    </table>        
                </td>
                <td valign="top"><asp:Image ID="OutageMain" Imageurl="~/images/landing/landing-outage.jpg" runat="server" /></td>
            </tr>
        </table>

        <br /><hr style="color:#113962;height:1px;"/><br />
        <acrobat:uc4 ID="uc4" runat="server" />
        <asp:Label ID="lblLinks" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>

