﻿
Partial Class Outages_OutageReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dynamically add javascript
        Dim autotab As HtmlGenericControl = New HtmlGenericControl
        autotab.Attributes.Add("type", "text/javascript")
        autotab.TagName = "script"
        autotab.Attributes.Add("src", ResolveUrl("~/js/autotab.js"))
        Page.Header.Controls.Add(autotab)
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Try
            txtPrimaryPhone1.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtPrimaryPhone1.Attributes("onkeyup") = "onFull(event, this)"
            txtPrimaryPhone2.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtPrimaryPhone2.Attributes("onkeyup") = "onFull(event, this)"
            txtPrimaryPhone3.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtPrimaryPhone3.Attributes("onkeyup") = "onFull(event, this)"
            txtCustToken.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtCustToken.Attributes("onkeyup") = "onFull(event, this)"
            txtAcctToken.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtAcctToken.Attributes("onkeyup") = "onFull(event, this)"
            txtAPkgToken.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtAPkgToken.Attributes("onkeyup") = "onFull(event, this)"
            txtContactName.Focus()
        Catch
        End Try
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim sb As New StringBuilder
            Dim MailObj As New System.Net.Mail.SmtpClient
            Dim message As New System.Net.Mail.MailMessage()
            Dim fromAddress As New System.Net.Mail.MailAddress("outagereport@empiredistrict.com")

            message.To.Add("outagereport@empiredistrict.com")
            message.From = fromAddress
            message.Subject = "Web Reported Outage"
            sb.Append(txtContactName.Text).AppendLine()
            sb.Append(txtAddress.Text).AppendLine()
            sb.Append(txtCity.Text & " " & txtState.Text & ", " & txtZip.Text).AppendLine()
            sb.Append(txtPrimaryPhone1.Text & "-" & txtPrimaryPhone2.Text & "-" & txtPrimaryPhone3.Text).AppendLine()
            sb.Append(txtCustToken.Text & "-" & txtAcctToken.Text & "-" & txtAPkgToken.Text).AppendLine()
            sb.AppendLine()
            sb.AppendLine()
            For i = 0 To cblProblems.Items.Count - 1
                If cblProblems.Items(i).Selected Then
                    sb.Append(cblProblems.Items(i).Text).AppendLine()
                End If
            Next
            sb.AppendLine()
            sb.AppendLine()
            sb.Append(txtComments.Text)
            message.Body = sb.ToString
            'MailObj.Host = "" -- obtained from web.config 03/14/2012
            MailObj.Send(message)
            lblStatus.Text = "** Outage Reported **"
            MultiView1.ActiveViewIndex = 1
        Catch
        End Try
    End Sub
End Class
