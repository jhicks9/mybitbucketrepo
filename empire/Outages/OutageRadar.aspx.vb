﻿Imports System
Imports System.Net
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

Partial Class OutageRadar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header

        If Not (IsPostBack) Then
            Select Case Request.QueryString("i")
                Case 0
                    'getNOAAImage("uppermissvly_loop.gif")
                    'imgNOAA.ImageUrl = "~/images/uppermissvly_loop.gif"
                    imgNOAA.ImageUrl = "http://radar.weather.gov/Conus/Loop/uppermissvly_loop.gif"
                    imgNOAA.Visible = True
                    lblOutageInfo.Text = "Radar powered by NOAA at weather.gov"
                Case 1
                    'getNOAAImage("southplains_loop.gif")
                    'imgNOAA.ImageUrl = "~/images/southplains_loop.gif"
                    imgNOAA.ImageUrl = "http://radar.weather.gov/Conus/Loop/southplains_loop.gif"
                    imgNOAA.Visible = True
                    lblOutageInfo.Text = "Radar powered by NOAA at weather.gov"
                Case 2
                    drawBoundaries()
            End Select
        End If
    End Sub

    Public Function getNOAAImage(ByVal filename As String) As String
        ' retrieves image from NOAA and saves it in ~/images folder.  Only gets new image every 20 minutes.
        Try
            If System.IO.File.Exists(Server.MapPath(ResolveUrl("~/images")) & "\" & filename) Then
                Dim objFileInfo As New System.IO.FileInfo(Server.MapPath(ResolveUrl("~/images")) & "\" & filename)
                Dim dtLastWriteTime As DateTime = objFileInfo.LastWriteTime
                Dim span As TimeSpan = Now.Subtract(objFileInfo.LastWriteTime)
                If span.TotalMinutes > 20 Then
                    saveURLBinary("http://radar.weather.gov/Conus/Loop/" & filename, filename)
                End If
            Else
                saveURLBinary("http://radar.weather.gov/Conus/Loop/" & filename, filename)
            End If
            Return ResolveUrl("~/images/" & filename)
        Catch
            Return ""
        End Try
    End Function

    Public Delegate Function RemoteCertificateValidationCallback(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean

    Public Sub saveURLBinary(ByVal url As String, ByVal filename As String, Optional ByRef UserName As String = "", Optional ByRef Password As String = "")
        Dim SourceStream As System.IO.Stream = Nothing
        Dim TempStream As New System.IO.MemoryStream
        Dim mywebResp As HttpWebResponse = Nothing
        Dim tempArea As String = System.IO.Path.GetTempPath.ToString
        Try
            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function() True  'Ignore bad https certificates - expired, untrusted, bad name, etc.
            Dim mywebReq As HttpWebRequest = CType(HttpWebRequest.Create(url), HttpWebRequest)
            mywebReq.UserAgent = Request.ServerVariables.Item("HTTP_USER_AGENT").ToString()
            mywebReq.Timeout = 4000 '4 seconds
            If Not String.IsNullOrEmpty(UserName) Then
                mywebReq.Credentials = New NetworkCredential(UserName, Password)
            End If
            mywebResp = mywebReq.GetResponse()
            SourceStream = mywebResp.GetResponseStream()
            'SourceStream has no ReadAll, so we must read data block-by-block
            'Temporary Buffer and block size
            Dim Buffer(4096) As Byte, BlockSize As Integer
            Do
                BlockSize = SourceStream.Read(Buffer, 0, 4096)
                If BlockSize > 0 Then TempStream.Write(Buffer, 0, BlockSize)
            Loop While BlockSize > 0

            Dim outStream As System.IO.Stream = System.IO.File.Create(Server.MapPath(ResolveUrl("~/images")) & "\" & filename)  'save file
            TempStream.WriteTo(outStream)
            outStream.Flush()
            outStream.Close()
            'Return TempStream.ToArray()  'return the document binary data (if used as function)
        Catch ex As Exception
        Finally
            SourceStream.Close()
            mywebResp.Close()
        End Try
    End Sub

    Public Sub drawBoundaries()
        Dim conString As String = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
        Dim dataReaderInit As SqlDataReader
        Dim dataReader As SqlDataReader
        Dim sql As String = "select DISTINCT ESZ_Boundary.ESZ, ESZ_Boundary.NUM, outages.total from ESZ_Boundary left outer join outages on ESZ_Boundary.ESZ = outages.esz"
        Dim cmdInit As SqlCommand = New SqlCommand(sql, New SqlConnection(conString))
        cmdInit.Connection.Open()
        dataReaderInit = cmdInit.ExecuteReader()
        Dim GmapScript As String = ""

        GmapScript += "<script type='text/javascript'>"
        GmapScript += "function initialize() {"
        GmapScript += "var footer = document.getElementById('footer');"

        Dim ESZ As String = ""
        Dim NUM As String = ""
        Dim sTTL As String = ""
        Dim iTTL As Integer = 0
        Dim outage_amount As String = ""

        If dataReaderInit.HasRows Then
            While dataReaderInit.Read()
                Dim i As Integer = 0
                Dim lat As String = Nothing
                Dim lon As String = Nothing
                Dim desc As String = Nothing

                ESZ = dataReaderInit.GetValue(0).ToString
                NUM = dataReaderInit.GetValue(1).ToString
                sTTL = dataReaderInit.GetValue(2).ToString
                If String.IsNullOrEmpty(sTTL) Then
                    iTTL = 0
                Else
                    iTTL = System.Convert.ToInt32(sTTL)
                End If
                sql = "SELECT ESZ_boundary.latitude,ESZ_boundary.longitude,ESZ_boundary.ESZ,ESZ_boundary.NUM from ESZ_boundary WHERE ESZ_boundary.ESZ = '" & ESZ & "' ORDER BY ESZ_boundary.ESZ"

                Dim cmd As SqlCommand = New SqlCommand(sql, New SqlConnection(conString))
                cmd.Connection.Open()
                dataReader = cmd.ExecuteReader()

                If dataReader.HasRows Then
                    While dataReader.Read()
                        lat += dataReader.GetValue(0) & ";"
                        lon += dataReader.GetValue(1) & ";"
                        i = i + 1
                    End While
                End If

                dataReader.Close()
                cmd.Connection.Close()
                cmd.Connection.Dispose()

                Dim arlat() As String
                Dim arlon() As String

                arlat = lat.Split(";")
                arlon = lon.Split(";")

                Dim j As Integer = 0
                Dim endchar As String

                GmapScript += "var polyCoords" & ESZ & " = ["
                For j = 0 To i - 1
                    If j < i - 1 Then
                        endchar = ","
                    Else
                        endchar = Nothing
                    End If
                    GmapScript += "new google.maps.LatLng(" & arlat(j) & "," & arlon(j) & ")" & endchar
                Next
                GmapScript += "];"

                Dim strColor As String = "#606060"
                Dim range1 As Integer = 100
                Dim range2 As Integer = 250
                Dim range3 As Integer = 500
                Dim range4 As Integer = 1000
                Dim range5 As Integer = 2000
                Dim range6 As Integer = 3000
                If (iTTL >= 10) And (iTTL < range1) Then
                    strColor = "#0026FF" 'blue
                    outage_amount = "between 10 and 100"
                Else
                    If (iTTL >= range1) And (iTTL < range2) Then
                        strColor = "#00FF21" 'green
                        outage_amount = "between 100 and 250"
                    Else
                        If (iTTL >= range2) And (iTTL < range3) Then
                            strColor = "#FFFF21" 'yellow
                            outage_amount = "between 250 and 500"
                        Else
                            If iTTL >= range3 And iTTL < range4 Then
                                strColor = "#FFB218" 'orange
                                outage_amount = "between 500 and 1000"
                            Else
                                If iTTL >= range4 And iTTL < range5 Then
                                    strColor = "#FF0000" 'red
                                    outage_amount = "between 1000 and 2000"
                                Else
                                    If iTTL >= range5 And iTTL < range6 Then
                                        strColor = "#FF51FF" 'pink
                                        outage_amount = "between 2000 and 3000"
                                    Else
                                        If iTTL >= range6 Then
                                            strColor = "#873EF4" 'purple
                                            outage_amount = "more than 3000"
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                If strColor = "#606060" Then  'no outage, so no outline
                    GmapScript += "var poly" & ESZ & " = new google.maps.Polygon({paths:polyCoords" & ESZ & ",clickable:false,strokeColor:'" & strColor & "',strokeOpacity:0.4,strokeWeight:0,fillColor:'" & strColor & "',fillOpacity:0.40});"
                Else  'outline the polygon to highlight outage
                    GmapScript += "var poly" & ESZ & " = new google.maps.Polygon({paths:polyCoords" & ESZ & ",clickable:true,strokeColor:'#404040',strokeOpacity:0.8,strokeWeight:1,fillColor:'" & strColor & "',fillOpacity:0.40});"
                    GmapScript += "google.maps.event.addListener(poly" & ESZ & ", 'mouseout', function(event) {this.setOptions({ fillColor:'" & strColor & "',fillOpacity:0.40 }); footer.innerHTML = '&nbsp';   });"
                    GmapScript += "google.maps.event.addListener(poly" & ESZ & ", 'mouseover', function(event) {this.setOptions({ fillColor:'" & strColor & "',fillOpacity:0.60}); footer.innerHTML = '" & outage_amount & "';  });"
                End If
                GmapScript += "poly" & ESZ & ".setMap(map);"
            End While

            dataReaderInit.Close()
            cmdInit.Connection.Close()
            cmdInit.Connection.Dispose()
        End If
        GmapScript += "}</script>"
        gmapInitOutageArea.Text = GmapScript
    End Sub

    Public Function getOutages() As DataSet
        Dim conString As String = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
        Dim ds As New DataSet
        Try
            'get OMS data from SQL2005 tables
            Dim conn As New SqlConnection(conString)
            Dim query As String = "select esz, total as NUM FROM outages WHERE controlrecord = 0 order by id"
            Dim daOutageInfo As New SqlDataAdapter(query, conn)
            conn.Open()
            daOutageInfo.Fill(ds, "OutageInfo")
            Return ds
        Catch
            Return Nothing
        End Try
    End Function
End Class