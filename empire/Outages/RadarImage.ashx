﻿<%@ WebHandler Language="VB" Class="RadarImage" %>
Imports System
Imports System.Web
Imports System.Net
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Public Class RadarImage : Implements IHttpHandler

    Dim cwservice As New cw.Service
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim product As String = context.Request.QueryString("product")
            If product Is Nothing Then product = "N0R" 'set default product if not provided in querystring
            Dim rid As String = context.Request.QueryString("rid")
            If rid Is Nothing Then rid = "SGF" 'set default ridge id if not provided in querystring
            Dim frames As Integer = 0
            If IsNumeric(context.Request.QueryString("frames")) Then  'limit to number of frames indicated
                frames = context.Request.QueryString("frames")
            Else
                frames = 10  'default number of frames if number is not provided in querystring
            End If
        
            Dim animatedImageStream As New System.IO.MemoryStream(cwservice.GetNOAAImage(product, rid, frames))
        
            context.Response.Clear()
            animatedImageStream.Position = 0
            Dim image As System.Drawing.Image = System.Drawing.Image.FromStream(animatedImageStream)
            context.Response.ContentType = "image/gif"
            image.Save(context.Response.OutputStream, Drawing.Imaging.ImageFormat.Gif)  'output the finished animated .gif
            animatedImageStream.Flush()
            animatedImageStream.Close()
            context.Response.End()
        Catch
        End Try
    End Sub
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class