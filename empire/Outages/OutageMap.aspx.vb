Imports System
Imports System.Net
Imports System.Web
Imports System.Net.Security
Imports System.Data.SqlClient
Imports System.Data
Imports System.Security.Cryptography.X509Certificates

Partial Class _OutageMap
    Inherits System.Web.UI.Page

    Dim totalOutages As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load       
        Try
            Response.CacheControl = "Private"
            Response.Cache.SetCacheability(HttpCacheability.NoCache)

            cbRadar.Checked = False
            cbCounty.Checked = True
            cbOutages.Checked = True

            If isBingMap() Then
                Dim litPinStyle As New Literal
                litPinStyle.Text = "<style>.labelPin{overflow:visible !important;}.labelPin img{display:none;}.labelPin div{font-size:.85em !important;white-space:nowrap;color:#3B3B3B !important;}</style>"
                Me.Header.Controls.Add(litPinStyle)
            End If

            Dim litMapInfo As New Literal
            litMapInfo.ID = "mapInitOutageArea"
            litMapInfo.Text = "<script type='text/javascript'>function initialize() {"
            litMapInfo.Text &= processOutages()
            litMapInfo.Text &= "}</script>"
            Me.Header.Controls.Add(litMapInfo)
        Catch
        End Try
    End Sub

    Public Function isBingMap() As Boolean
        Try  'toggle using bing and google maps
            If DateTime.Today.Month Mod 2 = 0 Then
                Return True
            Else
                Return False
            End If
        Catch
            Return False
        End Try
    End Function

    Public Function processOutages() As String
        Dim mapScript As String = ""
        Try
            Dim cwservice As New cw.Service
            Dim dsOutageInfo As DataSet = Nothing

            dsOutageInfo = cwservice.GetOutages()  'read outages from web service

            Dim dr As DataRow
            Dim increment As Integer = 0
            Dim ESZ As String = ""
            Dim NUM As Integer = 0

            If Not dsOutageInfo Is Nothing Then
                mapScript &= "var strColorBlue = new Microsoft.Maps.Color(100,0,38,255);"
                mapScript &= "var strColorGreen = new Microsoft.Maps.Color(100,0,255,33);"
                mapScript &= "var strColorYellow = new Microsoft.Maps.Color(100,255,255,33);"
                mapScript &= "var strColorOrange = new Microsoft.Maps.Color(100,255,178,24);"
                mapScript &= "var strColorRed = new Microsoft.Maps.Color(100,255,0,0);"
                mapScript &= "var strColorPink = new Microsoft.Maps.Color(100,255,81,255);"
                mapScript &= "var strColorPurple = new Microsoft.Maps.Color(100,135,62,244);"
                mapScript &= "var gBlue='#0026FF';var gGreen='#00FF21';var gYellow='#FFFF21';var gOrange='#FFB218';"
                mapScript &= "var gRed='#FF0000';var gPink='#FF51FF';var gPurple='#873EF4';"
                mapScript &= "var currentColor;"

                For Each dr In dsOutageInfo.Tables(0).Rows
                    ESZ = dsOutageInfo.Tables("OutageInfo").Rows(increment).Item("ESZ")
                    NUM = dsOutageInfo.Tables("OutageInfo").Rows(increment).Item("NUM")

                    If ESZ <> "0" Then  'ensure esz defined  -- undefined esz are reported as zero
                        mapScript &= drawPoly(ESZ, NUM)
                    End If
                    increment = increment + 1
                Next
                If isBingMap() Then
                    mapScript &= "map.entities.push(pinLayer);"
                End If
                If totalOutages > 0 Then
                    Dim totalRangeData() As String = getRange(totalOutages).Split(";")
                    mapScript &= "document.getElementById('totalRange').innerHTML = '&nbsp;" & totalOutages & " customers affected';" 'display total outages
                    mapScript &= "var totalColor = document.getElementById('totalColor');totalColor.style.backgroundColor = '" & totalRangeData(2) & "';"
                End If
            Else
                mapScript &= "document.getElementById('totalRange').innerHTML = '<div style=\'text-align:center;font-weight:bold;font-size:1em;color:maroon;\'>Outage Data unavailable</div><br/>';"
            End If
            Dim timeData As String = cwservice.GetLastOMSRead()
            Dim lastupdate As String = timeData.Split(";")(0).ToString
            Dim interval As String = timeData.Split(";")(1).ToString
            mapScript &= "document.getElementById('divLoadMessage').innerHTML = '<div style=\'margin:5px auto 5px auto;text-align:center;\'>Last Updated:<br />" & lastupdate & "</div>';" 'remove loading image
            mapScript &= "document.getElementById('interval').innerHTML = '" & interval & "';"
        Catch
            mapScript &= "alert('error: " & Err.Description & "');"
        End Try
        Return mapScript
    End Function

    Public Function drawPoly(ByVal ESZ As String, ByVal NUM As Integer) As String
        Dim mapScript As String = ""
        Dim gmapScript As String = ""
        Try
            Dim area As String = ""
            Dim range As String = ""
            Dim path As String = ""
            Dim lat As String = Nothing
            Dim lon As String = Nothing
            Dim numberOfPoints As Integer = 0
            Dim sqlStatement As String = "SELECT ESZ_boundary.latitude,ESZ_boundary.longitude,ESZ_boundary.ESZ,ESZ_boundary.NUM,(select esz_description.description from esz_description where esz_description.esz = esz_boundary.esz) as area from ESZ_boundary WHERE ESZ_boundary.ESZ = '" & ESZ & "' ORDER BY ESZ_boundary.ESZ"
            Dim cmd As SqlCommand = New SqlCommand(sqlStatement, New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString))
            cmd.Connection.Open()
            Dim dataReader As SqlDataReader = cmd.ExecuteReader()

            If dataReader.HasRows Then
                While dataReader.Read()
                    area = dataReader.GetValue(4)
                    lat += dataReader.GetValue(0) & ";"
                    lon += dataReader.GetValue(1) & ";"
                    numberOfPoints = numberOfPoints + 1
                End While
            End If
            dataReader.Close()
            cmd.Connection.Close()
            cmd.Connection.Dispose()

            If Not String.IsNullOrEmpty(lat) AndAlso ((lat.Split(";").Length - 1) = numberOfPoints) AndAlso Not String.IsNullOrEmpty(lon) AndAlso ((lon.Split(";").Length - 1) = numberOfPoints) Then  '--check for valid esz and valid points
                Dim arlat() As String
                Dim arlon() As String

                arlat = lat.Split(";")
                arlon = lon.Split(";")

                Dim j As Integer
                Dim endchar As String
                mapScript &= "var points = ["
                gmapScript &= "var points = ["
                For j = 0 To numberOfPoints - 1
                    If j < numberOfPoints - 1 Then
                        endchar = ","
                    Else
                        endchar = Nothing
                    End If
                    mapScript &= "new Microsoft.Maps.Location(" & arlat(j) & "," & arlon(j) & ")" & endchar
                    gmapScript += "new google.maps.LatLng(" & arlat(j) & "," & arlon(j) & ")" & endchar
                Next
                mapScript &= "];"
                gmapScript &= "];"

                If NUM >= 10 Then 'do not show outages < 10
                    Dim rangeData() As String = getRange(NUM).Split(";")
                    range = rangeData(0)
                    path = VirtualPathUtility.ToAbsolute("~/images/outages-pushpin-" & rangeData(1) & ".png")

                    gmapScript &= "currentColor = g" & StrConv(rangeData(1), VbStrConv.ProperCase) & ";"
                    'Create a polygon
                    gmapScript &= "var poly" & ESZ & " = new google.maps.Polygon({paths:points,clickable:true,strokeColor:'#404040',strokeOpacity:0.8,strokeWeight:1,fillColor:currentColor,fillOpacity:0.50});"
                    gmapScript &= "var i;var bounds = new google.maps.LatLngBounds();for (i = 0; i < points.length; i++) {bounds.extend(points[i]);}"
                    gmapScript &= "var marker" & ESZ & " = new google.maps.Marker({position:bounds.getCenter(),map:map,icon:'" & path & "'});"
                    gmapScript &= "google.maps.event.addListener(marker" & ESZ & ", 'mouseover', function(event) { displayGInfoBox(marker" & ESZ & ",'" & range & "','" & area & "','" & NUM & "');});"
                    gmapScript &= "google.maps.event.addListener(marker" & ESZ & ", 'mouseout', function(event) {hideInfoBox(); });"
                    gmapScript &= "gMarkers.push(marker" & ESZ & ");"
                    gmapScript &= "gZones.push(poly" & ESZ & ");"
                    gmapScript &= "poly" & ESZ & ".setMap(map);"

                    mapScript &= "currentColor = strColor" & StrConv(rangeData(1), VbStrConv.ProperCase) & ";"
                    'Create a polygon
                    mapScript &= "var poly" & ESZ & " = new Microsoft.Maps.Polygon(points,{fillColor: currentColor,strokeColor: new Microsoft.Maps.Color(200,100,100,100),strokeThickness: 1});"
                    'Create a pushpin  -- calculated center of poly
                    mapScript &= "var pin" & ESZ & " = new Microsoft.Maps.Pushpin(simplePolygonCentroid(points), {text:'', draggable: false,icon:'" & path & "'});"
                    mapScript &= "pin" & ESZ & ".range = '" & range & "';"
                    mapScript &= "pin" & ESZ & ".num = '" & NUM & " customers affected';"
                    mapScript &= "pin" & ESZ & ".area = '" & area & " area';"
                    mapScript &= "Microsoft.Maps.Events.addHandler(pin" & ESZ & ", 'mouseover', pinMouseOver);"
                    mapScript &= "Microsoft.Maps.Events.addHandler(pin" & ESZ & ", 'mouseout', pinMouseOut);"
                    mapScript &= "pinLayer.push(poly" & ESZ & ");"
                    mapScript &= "pinLayer.push(pin" & ESZ & ");"
                    totalOutages = totalOutages + NUM  'total up displayed outages
                End If  '--end show only outages > 10
            End If  '--end valid points
        Catch
            mapScript &= "alert('error: " & Err.Description & "');"
            gmapScript &= "alert('error: " & Err.Description & "');"
        End Try
        If isBingMap() Then
            Return mapScript
        Else
            Return gmapScript
        End If
    End Function

    Public Function getRange(ByVal num As Integer) As String
        Dim range As String = "no current outages;black;#000"
        Try
            Dim range1 As Integer = 100
            Dim range2 As Integer = 250
            Dim range3 As Integer = 500
            Dim range4 As Integer = 1000
            Dim range5 As Integer = 2000
            Dim range6 As Integer = 3000
            If (num >= 10) And (num < range1) Then
                range = "between 10 and 100;blue;#0026FF"
            ElseIf (num >= range1) And (num < range2) Then
                range = "between 100 and 250;green;#00FF21"
            ElseIf (num >= range2) And (num < range3) Then
                range = "between 250 and 500;yellow;#FFFF21"
            ElseIf (num >= range3) And (num < range4) Then
                range = "between 500 and 1000;orange;#FFB218"
            ElseIf (num >= range4) And (num < range5) Then
                range = "between 1000 and 2000;red;#FF0000"
            ElseIf (num >= range5) And (num < range6) Then
                range = "between 2000 and 3000;pink;#FF51FF"
            ElseIf (num > range6) Then
                range = "more than 3000;purple;#873EF4"
            End If
        Catch
        End Try
        Return range
    End Function

    Public Function ESZServiceArea(ByVal conString As String) As String
        Dim mapScript As String = ""
        Try
            Dim dataReaderInit As SqlDataReader
            Dim dataReader As SqlDataReader
            Dim sqlStatementInit As String = "SELECT distinct ESZ_boundary.ESZ, ESZ_boundary.NUM from ESZ_boundary"
            Dim cmdInit As SqlCommand = New SqlCommand(sqlStatementInit, New SqlConnection(conString))
            cmdInit.Connection.Open()
            dataReaderInit = cmdInit.ExecuteReader()

            Dim esz As String = ""

            If dataReaderInit.HasRows Then
                While dataReaderInit.Read()
                    Dim i As Integer = 0
                    Dim lat As String = Nothing
                    Dim lon As String = Nothing
                    'Dim desc As String = Nothing

                    esz = dataReaderInit.GetValue(0).ToString
                    'NUM = dataReaderInit.GetValue(1).ToString
                    Dim sqlStatement As String = "SELECT ESZ_boundary.latitude,ESZ_boundary.longitude,ESZ_boundary.ESZ,ESZ_boundary.NUM from ESZ_boundary WHERE ESZ_boundary.ESZ = '" & esz & "' ORDER BY ESZ_boundary.ESZ"

                    Dim cmd As SqlCommand = New SqlCommand(sqlStatement, New SqlConnection(conString))
                    cmd.Connection.Open()
                    dataReader = cmd.ExecuteReader()

                    If dataReader.HasRows Then
                        While dataReader.Read()
                            lat += dataReader.GetValue(0) & ";"
                            lon += dataReader.GetValue(1) & ";"
                            i = i + 1
                        End While
                    End If

                    dataReader.Close()
                    cmd.Connection.Close()
                    cmd.Connection.Dispose()

                    Dim arlat() As String
                    Dim arlon() As String

                    arlat = lat.Split(";")
                    arlon = lon.Split(";")

                    Dim j As Integer = 0
                    Dim endchar As String
                    mapScript &= "var points = ["
                    For j = 0 To i - 1
                        If j < i - 1 Then
                            endchar = ","
                        Else
                            endchar = Nothing
                        End If
                        mapScript &= "new Microsoft.Maps.Location(" & arlat(j) & "," & arlon(j) & ")" & endchar
                    Next
                    mapScript &= "];"

                    'Create a polygon
                    mapScript &= "var poly" & esz & " = new Microsoft.Maps.Polygon(points,{fillColor: new Microsoft.Maps.Color(90,125,125,125),strokeColor: new Microsoft.Maps.Color(90,125,125,125),strokeThickness: 0});"
                    mapScript &= "saLayer.push(poly" & esz & ");"
                End While
                dataReaderInit.Close()
                cmdInit.Connection.Close()
                cmdInit.Connection.Dispose()
            End If
            mapScript &= "map.entities.push(saLayer);"
        Catch
            mapScript &= "alert('error: " & Err.Description & "');"
        End Try
        Return mapScript
    End Function

    ' Code used to have a linkbutton perform code behind THEN launch javascript
    'Private startUpScript As String = ""
    'Private imageButtonClicked As Boolean = False
    'Protected Sub ImageButtonClick(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim filename As String = ""
    '    Dim lb As LinkButton = CType(sender, LinkButton)
    '    If lb.ClientID = "lbNOAA1" Then
    '        filename = "southplains_loop.gif"
    '    End If
    '    If lb.ClientID = "lbNOAA2" Then
    '        filename = "uppermissvly_loop.gif"
    '    End If
    '    getNOAAImage(filename)
    '    startUpScript = "$(document).ready(function(){ jQuery.slimbox('" & ResolveUrl("~/images/" & filename) & "', 'Radar powered by NOAA at weather.gov'); });"
    '    imageButtonClicked = True
    'End Sub
    'Protected Overloads Overrides Sub OnPreRender(ByVal e As EventArgs)
    '    MyBase.OnPreRender(e)
    '    If Me.imageButtonClicked Then
    '        Me.Page.ClientScript.RegisterStartupScript(Me.[GetType](), "ImageButtonPopup", startUpScript, True)
    '    End If
    'End Sub

    ' Run the following query to simulate outages on the test server:
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2092162','205')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2092291','91')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2112101','61')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2112102','18')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2112273','1')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122041','401')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122042','12')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122061','116')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122063','307')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122151','700')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122191','56')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122192','71')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122193','91')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2122194','10')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2132121','63')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2132122','12')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2132141','679')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2132142','79')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2132221','15')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2132222','155')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2142011','138')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2142031','235')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2142211','128')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2142281','322')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'21522051','1422')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'21522261','846')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2162022','18')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2162082','678')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2162251','4950')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2162181','24')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2162182','356')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2162341','2111')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2172071','5166')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2172241','1000')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2172242','10')
    'insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE(),'2172301','500')
    'update outages set refreshdate=GETDATE() where controlrecord = 1
    ' end simulate query
End Class
