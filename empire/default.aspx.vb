﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Runtime.Remoting.Messaging
Imports System.Xml
Imports System.Xml.Linq
Imports System.Linq


Partial Class _default
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dynamically add javascript
        'Dim cycle As HtmlGenericControl = New HtmlGenericControl
        'cycle.Attributes.Add("type", "text/javascript")
        'cycle.TagName = "script"
        'cycle.Attributes.Add("src", ResolveUrl("~/js/jquery.nivo.slider.pack.js"))
        'cycle.Attributes.Add("src", ResolveUrl("~/js/jquery.cycle.js"))
        'Page.Header.Controls.Add(cycle)
        Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sm As SiteMapPath = Page.Master.FindControl("SiteMapPath")
            sm.Visible = False
            BuildContent()
            processModuleXML()
        Catch
        End Try
    End Sub

    Public Sub BuildAnnouncements()
        Dim dsAnnouncements As New DataSet
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim sql As String = "select CONVERT(VARCHAR(12),date,107) AS date, description from messages where type = 'announcement' and ('" & Now.Date & "' >= begdate AND '" & Now.Date & "' <= enddate) order by date"
            ' Date Formats
            '(yy)  (yyyy)    Input/Output
            '-     0/100     mon dd yyyy hh:miAM (or PM) 
            '1     101       mm/dd/yy 
            '2     102       yy.mm.dd 
            '3     103       dd/mm/yy 
            '4     104       dd.mm.yy 
            '5     105       dd-mm-yy 
            '6     106       dd mon yy 
            '7     107       Mon dd, yy 
            '8     108       hh:mm:ss 
            '-     9/109     mon dd yyyy hh:mi:ss:mmmAM (or PM) 
            '10    110       mm-dd-yy 
            '11    111       yy/mm/dd 
            '12    112       yymmdd 
            '-     13/113    dd mon yyyy hh:mm:ss:mmm(24h) 
            '14    114       hh:mi:ss:mmm(24h) 
            '-     20/120    yyyy-mm-dd hh:mi:ss(24h) 
            '-     21/121    yyyy-mm-dd hh:mi:ss.mmm(24h) 
            '-     126       yyyy-mm-dd Thh:mm:ss.mmm(no spaces) 
            '-     130       dd mon yyyy hh:mi:ss:mmmAM 
            '-     131       dd/mm/yy hh:mi:ss:mmmAM 
            conn.Open()
            Dim dRow As DataRow
            Dim dTable As New DataTable
            Dim dcDate As New DataColumn("Date", System.Type.GetType("System.String"))
            Dim dcDescription = New DataColumn("Description", System.Type.GetType("System.String"))
            dTable.Columns.Add(dcDate)
            dTable.Columns.Add(dcDescription)

            Dim cmd As New SqlCommand(sql, conn)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    dRow = dTable.NewRow()
                    dRow("Date") = dr("date").ToString
                    dRow("Description") = dr("description").ToString
                    dTable.Rows.Add(dRow)
                End While
            Else
            End If
            dsAnnouncements.Tables.Add(dTable)
            dr.Close()
            conn.Close()
            conn.Dispose()

            gvAnnouncements.DataSource = dsAnnouncements
            gvAnnouncements.DataBind()
            pnlAnnouncements.Visible = True
        Catch
            gvAnnouncements.DataSource = Nothing
            pnlAnnouncements.Visible = False
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Public Sub BuildLinksofInterest(ByVal needSpacer As Boolean)
        Dim dsLinks As New DataSet
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim sql As String = "select title,url,newwindow from links where type = 'linksofinterest' order by listorder"
            Dim daLinks As New SqlDataAdapter(sql, conn)
            conn.Open()
            daLinks.Fill(dsLinks, "LinksInterest")
            conn.Close()
            gvLinks.DataSource = dsLinks
            gvLinks.DataBind()
            If needSpacer Then
                pnlSpacer1.Visible = True
            Else
                pnlSpacer1.Visible = False
            End If
            pnlLinksofInterest.Visible = True
        Catch
            gvLinks.DataSource = Nothing
            pnlLinksofInterest.Visible = False
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Protected Sub gvLinks_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLinks.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hl As HyperLink = CType(e.Row.FindControl("hlLinks"), HyperLink)

            If Not String.IsNullOrEmpty(hl.NavigateUrl.Split(",")(0).ToString) Then
                Dim url As String = hl.NavigateUrl.Split(",")(0).ToString
                If hl.NavigateUrl.Split(",")(1) = True Then
                    hl.Target = "_blank"
                Else
                    hl.Target = "_self"
                End If
                If url = "~/" Then  'if link is "internal"
                    hl.NavigateUrl = ResolveUrl("url")
                Else
                    hl.NavigateUrl = url
                End If
            End If
        End If
    End Sub

    Public Sub BuildContent()
        Dim content As String = ""
        Dim needSpacer As Boolean = False
        Dim sql As String = ""
        Try
            'check for content (documents) database connectivity
            'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            'conn.Open()
            Dim connector As New sqlConnections.ConnectionStateTracker(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim conn = connector.Connect(4000)
            If conn Is Nothing Then 'connfailed
                'page defaults here
            Else
                sql = "select module_name from modules where module_visible = 'True' and module_columnonpage = 'static' order by module_name"
                Dim cmd0 As New SqlCommand(sql, conn)
                Dim dr0 As SqlDataReader
                dr0 = cmd0.ExecuteReader
                If dr0.HasRows Then
                    While dr0.Read
                        Select Case dr0("module_name").ToString
                            Case "Announcements"
                                BuildAnnouncements()
                                needSpacer = True
                            Case "Links of Interest"
                                BuildLinksofInterest(needSpacer)
                            Case "Customer Outage"
                                lblOutageNumber.Text = BuildOutageNumber()
                        End Select
                    End While
                Else

                End If
                dr0.Close()
                conn.Close()
                conn.Dispose()
            End If
        Catch
            'page defaults here
        End Try
    End Sub

    Public Function BuildOutageNumber() As String
        Dim outagenumber As String = ""
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim Sql As String = "select top 1 description from messages where type = 'Outage' "

            conn.Open()
            Dim cmd0 As New SqlCommand(Sql, conn)
            Dim dr0 As SqlDataReader
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    outagenumber = dr0(0).ToString
                End While
            Else
                outagenumber = "Outage data not available at this time"
            End If

            dr0.Close()
            conn.Close()
            pnlOutageNumber.Visible = True
            pnlDefault.Visible = False
            Return "Approximately <b style='font-size:1.4em;font-weight:bold;'>" & outagenumber & "</b> customers are currently experiencing an outage."
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            pnlOutageNumber.Visible = False
            pnlDefault.Visible = True
            Return "Outage data not available at this time"
        End Try
    End Function

    Public Sub buildModuleXML()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim abyt() As Byte = Nothing
        Try
            Dim sql As String = "select module_id,module_name,module_link,module_filename,module_image from modules where module_visible = 'True' and module_columnonpage = 'Middle' order by module_order"
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                Dim xml_text_writer As New XmlTextWriter(System.IO.Path.GetTempPath & "modules.xml", System.Text.Encoding.UTF8)
                xml_text_writer.Formatting = Formatting.Indented
                xml_text_writer.Indentation = 4
                xml_text_writer.WriteStartDocument(True)
                xml_text_writer.WriteStartElement("ActiveModuleInfo")
                While dr.Read
                    If System.IO.File.Exists(Server.MapPath("~/images/module/") & dr("module_filename").ToString) Then  'only create xml to display the image IF the image exists
                        xml_text_writer.WriteStartElement("module")
                        xml_text_writer.WriteStartElement("id")
                        xml_text_writer.WriteString(dr("module_id").ToString)
                        xml_text_writer.WriteEndElement() 'id
                        xml_text_writer.WriteStartElement("filename")
                        xml_text_writer.WriteString(dr("module_filename").ToString)
                        xml_text_writer.WriteEndElement() 'filename
                        xml_text_writer.WriteStartElement("link")
                        xml_text_writer.WriteString(dr("module_link").ToString)
                        xml_text_writer.WriteEndElement() 'link
                        xml_text_writer.WriteStartElement("name")
                        xml_text_writer.WriteString(dr("module_name").ToString)
                        xml_text_writer.WriteEndElement() 'name for alt tag
                        xml_text_writer.WriteEndElement() 'module
                    End If

                    'extract the module image to the ~/images/module folder
                    'If System.IO.File.Exists(Server.MapPath("~/images/module/") & dr("module_filename").ToString) Then  'remove existing file if it exists
                    '    System.IO.File.Delete(Server.MapPath("~/images/module/") & dr("module_filename").ToString)
                    'End If
                    'Dim fs As New IO.FileStream(Server.MapPath("~/images/module/") & dr("module_filename").ToString, IO.FileMode.CreateNew)
                    'Dim bw As New IO.BinaryWriter(fs)
                    'abyt = dr("module_image")
                    'bw.Write(abyt)
                    'bw.Close()

                End While
                xml_text_writer.WriteEndElement() 'ActiveModuleInfo
                xml_text_writer.WriteEndDocument()
                xml_text_writer.Flush()
                xml_text_writer.Close()
            End If
            dr.Close()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Public Function createLiteral(Optional ByVal tag As String = "") As Literal
        Dim lit As New Literal
        Try
            lit.Text = tag
        Catch
        End Try
        Return lit
    End Function

    Public Sub processModuleXML()
        Try
            Dim phRight As ContentPlaceHolder = Page.Master.FindControl("RightPlaceHolder")
            Dim ph As PlaceHolder = phRight.FindControl("phSlideshow")
            If Not (phRight Is Nothing) And Not (ph Is Nothing) Then

                If Not System.IO.File.Exists(System.IO.Path.GetTempPath & "modules.xml") Then
                    buildModuleXML()  'build modules.xml file if it does not exist
                End If
                Dim document As XElement = XElement.Load(System.IO.Path.GetTempPath & "modules.xml")
                Dim modules = From xe In document.Descendants("module") _
                             Select New With _
                             { _
                                .id = xe.Element("id").Value, _
                                .filename = xe.Element("filename").Value, _
                                .link = xe.Element("link").Value, _
                                .alt = xe.Element("name").Value _
                             }

                ph.Controls.Add(createLiteral("<ul class='slideshow'>"))
                For Each xe In modules
                    ph.Controls.Add(createLiteral("<li>"))
                    If String.IsNullOrEmpty(xe.link) Then  'no link associated with module
                        Dim img As New Image
                        img.ImageUrl = ResolveUrl("~/images/module/" & xe.filename)
                        img.Attributes.Add("alt", xe.alt)
                        ph.Controls.Add(img)
                    Else
                        Dim hl As New HyperLink
                        If Left(xe.link, 2) = "~/" Then
                            hl.NavigateUrl = ResolveUrl(xe.link)  'internal link
                            hl.Target = "_self"
                        Else
                            hl.NavigateUrl = xe.link  'external link
                            hl.Target = "_blank"
                        End If
                        'hl.ToolTip = "learn more"
                        hl.Text = xe.alt
                        hl.ImageUrl = ResolveUrl("~/images/module/" & xe.filename)
                        ph.Controls.Add(hl)
                    End If
                    ph.Controls.Add(createLiteral("</li>"))
                Next
                ph.Controls.Add(createLiteral("</ul>"))
            End If
        Catch
        End Try
    End Sub

End Class

Public Class sqlConnections
    Public Class ConnectionStateTracker
        Private _abandonConnection As Boolean
        Private _run As Boolean
        Private _connStr As String
        Private _sync As Object
        'ctors
        Private Sub New()
            _run = False
            _abandonConnection = False
            _sync = New Object()
        End Sub
        Public Sub New(ByVal connStr As String)
            Me.New()
            _connStr = connStr
        End Sub
        'public properties
        Private Property AbandonConnection() As Boolean
            Get
                Return VolatileRead(_abandonConnection)
            End Get
            Set(ByVal value As Boolean)
                VolatileWrite(_abandonConnection, value)
            End Set
        End Property
        'private methods
        Private Function VolatileRead(Of T)(ByRef Address As T) As T
            VolatileRead = Address
            Threading.Thread.MemoryBarrier()
        End Function
        Private Sub VolatileWrite(Of T)(ByRef Address As T, ByVal Value As T)
            Threading.Thread.MemoryBarrier()
            Address = Value
        End Sub
        Private Sub ConnectCallback(ByVal ar As IAsyncResult)
            'Clean up the connection if it was abandoned
            SyncLock _sync
                If Me.AbandonConnection Then
                    Dim result As AsyncResult = CType(ar, AsyncResult)
                    Dim conn As SqlConnection = CType(ar.AsyncState, SqlConnection)
                    Dim del As Action = CType(result.AsyncDelegate, Action)
                    Try
                        del.EndInvoke(ar)
                    Catch ex As Exception
                        Console.WriteLine("Connection Error: " + ex.Message)
                    End Try
                    conn.Close()
                    conn.Dispose()
                End If
            End SyncLock
        End Sub
        'public methods
        Public Function Connect(ByVal timeout As Integer) As SqlConnection
            If (_run) Then Throw New InvalidOperationException("Connect can only be called once.")
            _run = True

            SyncLock _sync
                Dim conn As New SqlConnection(_connStr)
                Dim del As Action = AddressOf conn.Open
                Dim ar As IAsyncResult = del.BeginInvoke(AddressOf ConnectCallback, conn)
                Dim finished As Boolean = ar.AsyncWaitHandle.WaitOne(timeout)
                If (Not finished) Or (conn.State <> ConnectionState.Open) Then
                    Me.AbandonConnection = True
                    Return Nothing
                Else
                    Me.AbandonConnection = False
                    del.EndInvoke(ar)
                    Return conn
                End If
            End SyncLock

            Return Nothing
        End Function
    End Class
End Class