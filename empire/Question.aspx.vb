Imports System.Data.SqlClient

Partial Class Question
    Inherits System.Web.UI.Page
    Dim conString As String = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString

    Private Function Save_Answer(ByVal answers As String) As Boolean
        Try
            Dim sqldatasourceInsert As SqlDataSource = New SqlDataSource
            Dim SqlInsertSource As SqlDataSource = New SqlDataSource()
            Dim strInsert = "INSERT Question (QandA_answer,QandA_answer_date) VALUES ('" & answers & "','" & Now & "')"
            SqlInsertSource.ConnectionString = conString
            SqlInsertSource.InsertCommand = strInsert
            SqlInsertSource.Insert()
            Return True
        Catch
            Return False
        End Try

    End Function

    Protected Sub lbContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim strchklist As String = ""
            lblList.Visible = False
            Dim li As ListItem
            Dim result As Boolean
            If CheckBoxList1.SelectedIndex = "-1" Then
                lblError.Text = "No item Selected"
                lblError.Visible = True
            Else
                For Each li In CheckBoxList1.Items
                    If li.Selected Then
                        If li.Value <> 6 Then
                            lblError.Visible = False
                            strchklist += li.Text + ","
                        End If
                    End If
                Next
                If CheckBoxList1.Items(6).Selected = True And txtOther.Text = "" Then
                    lblError.Text = "You must enter a value for other"
                    lblError.Visible = True
                    lblList.Visible = False
                ElseIf CheckBoxList1.Items(6).Selected = True And txtOther.Text <> "" Then
                    strchklist += txtOther.Text + ","
                    lblError.Visible = False
                    'write strchklist to database
                    'lblList.Text = "You selected : " + strchklist
                    lblList.Visible = True
                    result = Save_Answer(strchklist)
                    If result Then
                        Response.Redirect("~/Register.aspx")
                    End If
                ElseIf CheckBoxList1.Items(6).Selected = False And CheckBoxList1.SelectedIndex <> "-1" Then
                    'write strchklist to database
                    'lblList.Text = "You selected : " + strchklist
                    lblList.Visible = True
                    result = Save_Answer(strchklist)
                    If result Then
                        Response.Redirect("~/Register.aspx")
                    End If
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub
End Class
