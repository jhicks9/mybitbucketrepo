﻿
Partial Class adobeAcrobat
    Inherits System.Web.UI.UserControl

    Private _enabled As Boolean = True

    Public Property Enabled() As Boolean
        Get
            Return _enabled
        End Get
        Set(ByVal value As Boolean)
            _enabled = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If _enabled Then
                pnlAcrobat.Visible = True
            End If
        Catch
        End Try
    End Sub
End Class
