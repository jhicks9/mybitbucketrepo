﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src='<%#ResolveUrl("~/js/jquery.cycle2.min.js")%>' type="text/javascript" charset="utf-8"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table style="border-spacing:0;border-collapse:collapse">
        <tr>
            <td valign="top" style="width:250px">
                <div style="padding:20px">
                    <asp:Panel ID="pnlDefault" runat="server" Visible="true">
                        <div class="heading" style="text-align:center">The Empire District Electric Company</div>
                        <br />Based in Joplin, Missouri, <b>The Empire District Electric Company</b> (NYSE: EDE) is an investor-owned, regulated utility providing electric, natural gas (through its wholly owned subsidiary
                        The Empire District Gas Company), and water service, with approximately 217,000 customers in Missouri, Kansas, Oklahoma, and Arkansas. A subsidiary of the company provides fiber optic services.
                    </asp:Panel>
                    <asp:Panel ID="pnlOutageNumber" runat="server" Visible="false" >
                        <div style="text-align:center;background-color:#FFFFAA;padding:10px;border:solid 1px #113962;">
                            <div class="heading">Customer Outages</div>
                            <br /><img id="imgOutageCenter" src="~/images/outages.png" style="border-style:none;" runat="server" alt="" /><br />
                            <asp:Label ID="lblOutageNumber" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
            </td>
            <td style="width:500px;vertical-align:top;padding:0;margin:0">
                <asp:PlaceHolder ID="phSlideshow" runat="server" />
                <div class="slideshow-pager" style="text-align: left"></div>
            </td>
        </tr>
    </table>
                                
    <table style="border-spacing:0;border-collapse:collapse;margin-top:5px">
        <tr>
            <asp:Panel ID='pnlAnnouncements' runat='server' Visible='false'>
                <td valign="top" style="padding:0px;width:500px">
                    <div class="heading">Announcements</div>
                    <asp:GridView ID="gvAnnouncements" runat="server" SkinID="GridViewList" Width="100%" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                <div style='padding-top:3px;padding-bottom:3px;'>
                                    <asp:Image ID="imgSepGV" ImageAlign="Top" ImageUrl="~/images/icon/icon_announcement.gif" AlternateText="" runat="server" />&nbsp; 
                                    <asp:Label ID="lblDate" runat="server"  style="white-space:nowrap;vertical-align:top;font-weight:bold;" Text='<%#Eval("Date") & "  " %>'></asp:Label>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>Nothing scheduled at this time.</EmptyDataTemplate> 
                    </asp:GridView>
                </td>
            </asp:Panel>

            <asp:Panel ID="pnlSpacer1" runat="server" Visible="false">
                <td valign="top" style="width:5px">&nbsp;</td>
            </asp:Panel>
                
            <asp:Panel ID='pnlLinksofInterest' runat='server' Visible='false'>
                <td valign="top" style="padding:0px;width:245px">
                    <div class="heading">Links of Interest</div>
                    <asp:GridView ID="gvLinks" runat="server" Width="100%" BackColor="transparent" GridLines="None" BorderStyle="None" ShowHeader="False" AutoGenerateColumns="False" Visible="true">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="top" align="left" style='padding-top:3px;width:15px;'><asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" AlternateText="" /></td>
                                            <td valign="top" align="left" class="links"><asp:HyperLink ID="hlLinks" Text='<%#eval("title") %>' NavigateUrl='<%#eval("url") & "," & eval("newwindow") %>' Target='' ForeColor="#113962" runat="server"/></td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </asp:Panel>    
        </tr>
    </table>

    <script type="text/javascript">
        $('.slideshow').cycle({ fx: 'fade', speed: 900, timeout: 15500, slides: 'li', pager: '.slideshow-pager', pagerActiveClass: 'slideshow-pager-active', pagerTemplate: '<span><a href=#>&nbsp;</a></span>' /*, pagerTemplate: '<span><a href=#>{{slideNum}}</a></span>'*/ });
    </script>
</asp:Content>