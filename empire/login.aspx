<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" title="Account Login" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" style="width:250px;">
                        <table width="100%" cellpadding="2" cellspacing="0">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblUserName" runat="server" AssociatedControlID="txtUserName" Text="User Name:" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserName" runat="server" Width="150px" Font-Size="1em"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="*" ValidationGroup="avg" Display="Dynamic" SetFocusOnError="true" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword" Text="Password:" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" Width="150px" Font-Size="1em" TextMode="Password" />
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ValidationGroup="avg" Display="Dynamic" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="padding:5px 0 5px 0;color:Red;">
                                        <asp:Label ID="lblStatus" runat="server" Visible="true" ForeColor="Red" Text="" />
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <div style="float:left;width:100px;">&nbsp;</div>
                                    <asp:LinkButton ID="lbLogin" runat="server" CssClass="ovalbutton" CommandName="Login" ValidationGroup="avg" OnClick="lbLogin_Click"><span>Log In</span></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <a id="hlRetrievePassword" href="<%= ResolveSSLUrl("~/RetrievePassword.aspx") %>" >Forgot Password</a>
                        <br />
                        <a id="hlRetrieveUsername" href="<%= ResolveSSLUrl("~/RetrievePassword.aspx?id=name") %>" >Forgot User Name</a>
                    </td>
                    <td valign="top">
                        <asp:Image ID="imgLogin" runat="server" ImageUrl="~/images/landing/landing-myaccount.jpg" />
                    </td>
                </tr>
            </table>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Login2" />
            <br />
            <div class="heading" style="text-align:center">MyAccount allows you secure and instant online access to your account and billing information � anytime and anywhere.</div>
            <br />
            It is the most convenient way to access your Empire account. Click below to follow the easy tips to register. Instantly you can begin accessing your Empire account information.
            The best part is that using MyAccount is free!  MyAccount offers easy-to-use features including:<br /><br />
            <div style="padding:0 30px 0 30px;">
                <img id="imgFeature1" runat="server" class='icon' alt='' src='~/images/icon/icon_bullet.gif'/>&nbsp;<b>MyStatements</b><br />
                Examine your current bill at any time, plus view previous bills to compare each month.<br /><br />
                <img id="imgFeature2" runat="server" class='icon' alt='' src='~/images/icon/icon_bullet.gif'/>&nbsp;<b>MyPayments</b><br />
                View account payment history of the previous 12 months.<br /><br />
                <img id="imgFeature3" runat="server" class='icon' alt='' src='~/images/icon/icon_bullet.gif'/>&nbsp;<b>MyUsage</b><br />
                View and track your usage over the past 12 or 24 months. You can see your monthly meter readings to compare energy use month to month.<br /><br />
                <img id="imgFeature4" runat="server" class='icon' alt='' src='~/images/icon/icon_bullet.gif'/>&nbsp;<b>MyEbill</b><br />
                Have your monthly Empire bill sent directly to your inbox. Registered customers will no longer receive a paper bill.<br />
            </div>
            <div style="height:25px">&nbsp</div>
            <asp:LinkButton ID="lbRegister" runat="server" CssClass="ovalbutton" PostBackUrl="~/question.aspx"><span>Register MyAccount</span></asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>