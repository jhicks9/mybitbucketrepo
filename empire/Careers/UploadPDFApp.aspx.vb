Imports System.Data.SqlClient
Imports System.Data
Imports System.Security.Cryptography

Partial Class Careers_UploadPDFApp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            getApplication()
        Else
        End If
    End Sub

    Public Sub getApplication()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Try
            conn.Open()
            cmd.CommandText = "SELECT top 1 documents.webdescription,documents.id from documents INNER JOIN doctype on documents.doctypeid = doctype.id where (documents.groupid = 14 and documents.doctypeid = 99)"
            drLookup = cmd.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    hlApplication.Text = drLookup("webdescription").ToString
                    hlApplication.NavigateUrl = ResolveUrl("~/DocHandler.ashx?id=" & drLookup("id").ToString & "&saveoption=1")
                End While
            Else
            End If
            drLookup.Close()
            conn.Close()
        Catch ex As Exception
            logData("[getApplication]" & txtName.Text & "; " & txtEmail.Text & "; " & ex.Message)
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim extension As String = ""
            Dim ci As New clientInfo
            Dim filehash As String = ""
            Dim attachments As New ArrayList

            'Verify valid email address
            If Not ci.IsEmail(txtEmail.Text) Then
                rfvEmail.IsValid = False
                logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & rfvEmail.ErrorMessage)
                Exit Sub
            End If
            'Verify application exists
            If fluApplication.PostedFile Is Nothing OrElse String.IsNullOrEmpty(fluApplication.PostedFile.FileName) OrElse fluApplication.PostedFile.InputStream Is Nothing Then
                lblApplicationValidation.Text = "Attached application pdf file is required."
                logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & lblApplicationValidation.Text)
                Exit Sub
            Else
                extension = System.IO.Path.GetExtension(fluApplication.PostedFile.FileName).ToLower()
                Select Case extension
                    Case ".pdf"
                        'check if pdf is still empty -- trying to attach empty jobapplication.pdf
                        Dim md5 As MD5 = md5.Create
                        filehash = BitConverter.ToString(md5.ComputeHash(fluApplication.PostedFile.InputStream)).Replace("-","").ToLower()
                        If filehash = "0ff0a081db5f5fca07c96a67b14a471e" Then 'hash of original unaltered jobapplication.pdf
                            lblApplicationValidation.Text = "The pdf has required fillable fields. Please complete the form."
                            logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & lblApplicationValidation.Text)
                            Exit Sub
                        Else
                            attachments.Add(fluApplication.PostedFile)
                        End If
                    Case Else
                        lblApplicationValidation.Text = "Application file not found or not a valid filetype."
                        logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & lblApplicationValidation.Text)
                        Exit Sub
                End Select
            End If
            lblApplicationValidation.Text = ""

            'Verify if resume is attached that is valid document type
            If fluResume.PostedFile Is Nothing OrElse String.IsNullOrEmpty(fluResume.PostedFile.FileName) OrElse fluResume.PostedFile.InputStream Is Nothing Then
            Else
                extension = System.IO.Path.GetExtension(fluResume.PostedFile.FileName).ToLower()
                Select Case extension
                    Case ".pdf"
                        attachments.Add(fluResume.PostedFile)
                    Case ".doc"
                        attachments.Add(fluResume.PostedFile)
                    Case ".txt"
                        attachments.Add(fluResume.PostedFile)
                    Case ".docx"
                        attachments.Add(fluResume.PostedFile)
                    Case ".rtf"
                        attachments.Add(fluResume.PostedFile)
                    Case ".odt"
                        attachments.Add(fluResume.PostedFile)
                    Case Else
                        lblResumeValidation.Text = "Attached resume is not a valid filetype."
                        logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & lblResumeValidation.Text)
                        Exit Sub
                End Select
            End If
            lblResumeValidation.Text = ""

            Dim emailBody As String = "Name: " & txtName.Text & vbCrLf & "Email: " & txtEmail.Text & vbCrLf & txtMessage.Text
            Dim toList As New ArrayList
            toList.Add("employment@empiredistrict.com")
            If ci.sendEMail(toList, "onlineapplication@empiredistrict.com", "EDOApp", emailBody.ToString, False, attachments) Then
                logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & "Appliation successfully emailed")
            Else
                logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & "Application not successfully emailed")
            End If
            pnlForm.Visible = False
            pnlEmailSent.Visible = True
            lblSent.Text = "Your application has been submitted.<br/><br/>Thank you for your interest in The Empire District Electric Company."
        Catch ex As Exception
            logData("[submit]" & txtName.Text & "; " & txtEmail.Text & "; " & ex.Message)
        End Try
    End Sub

    Public Function logData(ByVal msg As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim ci As New clientInfo
            msg = msg.Replace("'", "''")  'single quotes cause sql statements to fail
            conn.Open()
            Dim logCmd As New SqlCommand("insert into log (date,username,web,ipaddress,platform,browser) values('" & DateTime.Now.ToString & "','JobApplication','" & msg & "','" & ci.GetIP4Address() & "','" & ci.getBrowserInfo("platform") & "','" & ci.getBrowserInfo("name") & "')", conn)
            logCmd.ExecuteNonQuery()
            ' delete old logs - only keep 2 weeks
            logCmd.CommandText = "delete from log where (date <= GETDATE() - 14) and (username = 'JobApplication')"
            logCmd.ExecuteNonQuery()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return False
        End Try
        Return True
    End Function
End Class
