<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Benefits.aspx.vb" Inherits="Careers_Benefits" title="Benefits" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table style="border-collapse:collapse;border-spacing:0">
        <tr>
            <td style="vertical-align:top">
                <center>
                    <h4>The Empire Advantage</h4>
                        <div style="padding-left:20px;padding-right:20px">
                            The Empire District Electric Company values our employees and their families. A comprehensive, competitive benefit package is designed to meet a broad range of employee needs.
                        </div>
                </center>
            </td>
            <td rowspan="2"><asp:Image ID="imgCareers" Imageurl="~/images/landing/landing-benefits.jpg" runat="server" AlternateText="" /></td>
        </tr>
        <tr>
            <td style="vertical-align:bottom">
                <div style="padding-left:80px;">
                    <b style="text-decoration:underline">Manage your health</b><br />
                    Health, Dental, and Vision Insurance<br />
                    Wellness Program<br />
                    Medical Reimbursement Account<br /><br />
                </div>
                <div style="padding-left:80px;">
                    <b style="text-decoration:underline">Security for your family</b><br />
                    Life and Accident Insurance<br />
                    Short and Long-Term Disability Insurance
                </div>
            </td>
        </tr>
    </table>
    <br />
    <div style="float:left;width:275px;padding-left:80px;">
        <b style="text-decoration:underline">Achieve a balance</b><br />
        Vacation and Holiday Leave<br />
        Dependent Care Reimbursement Account<br />
        Employee Assistance Program<br />
        Tuition Reimbursement<br /><br />
    </div>
    <div style="float:left;">
        <b style="text-decoration:underline">Plan for the future</b><br />
        Pension Plan<br />
        401(k) Plan<br />
        Stock Purchase Plan<br />
        Retirement Planning<br />
    </div>
    <br style="clear:both;" />
</asp:Content>

