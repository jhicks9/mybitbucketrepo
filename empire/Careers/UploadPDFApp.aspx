<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="UploadPDFApp.aspx.vb" Inherits="Careers_UploadPDFApp" title="Online Application" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:Panel ID="pnlForm" runat="server" Visible="true">
        <table style="border-collapse:collapse;border-spacing:0;width:100%">
            <tr>
                <td style="width:60%">
                    <table style="border-collapse:collapse;border-spacing:0;width:100%">
                        <tr><td>Dear Applicant:</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>Thank you for your interest in The Empire District Electric Company. For over 100 years, our workforce has been serving our customers with pride. To learn more about Empire�s history, I encourage you to explore our Web site.</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>As we face the challenges of the future, we will continue to build on our reputation of providing excellent customer service through a skilled, highly motivated workforce. We provide our employees with a challenging work environment, while maintaining a company culture of high ethical standards.</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>Five core competencies are valued in all employees � Customer Focus, Teamwork, Effective Communication, Personal Accountability/Initiative and Flexible/Adaptable.  We believe these competencies are keys to success at Empire District, and it is our mission to recruit, hire, train, and retain such individuals.</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>If you are interested in possible employment with Empire, I encourage you to submit your application by following the listed steps.</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td style="text-align:right">Regards,</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td style="text-align:right">Vicki Williams</td></tr>
                        <tr><td style="text-align:right">Director of Human Resources</td></tr>
                    </table>
                </td>
                <td style="width:40%;vertical-align:top;padding:5px;background-color:#E6E6E6">
                    <table>
                        <tr><td colspan="2" style="text-align:center"><div class="sectionheader">Application Instructions</div></td></tr>
                        <tr>
                            <td style="white-space:nowrap;vertical-align:top">Step 1:</td>
                            <td>Download the <asp:HyperLink ID="hlApplication" Text="" runat="server"/></td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td style="white-space:nowrap;vertical-align:top">Step 2:</td>
                            <td>Complete the application and save to a location on your computer where it can be retrieved and uploaded.</td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td style="white-space:nowrap;vertical-align:top">Step 3:</td>
                            <td>Submit the completed application using the form below.</td>
                        </tr>
                        <tr><td colspan="2" style="padding-top:45px"><acrobat:uc4 ID="uc4" runat="server" /></td></tr>
                    </table>
                </td>
            </tr>
        </table>

        <hr style="color:#113962;height:1px;" />

        <table style="border-collapse:separate;border-spacing:3px;width:100%">
            <tr>
                <td style="text-align:right;vertical-align:top;white-space:nowrap"><b>Applicant Full Name:</b></td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" Width="300px" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" ValidationGroup="avg" Display="Static" />
                </td>
            </tr>
            <tr>
                <td style="text-align:right;vertical-align:top;white-space:nowrap"><b>Applicant Email address:</b></td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="300px" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required" ValidationGroup="avg" Display="Static" />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                        Display="Static" ErrorMessage="Valid Email required" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="avg">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:right"><b>Subject:</b></td>
                <td>Online Application</td>
            </tr>
            <tr>
                <td style="text-align:right;vertical-align:top"><b>Attach application:</b></td>
                <td>
                    <asp:FileUpload ID="fluApplication" runat="server" />&nbsp;<asp:Label ID="lblApplicationValidation" runat="server" Font-Bold="true" ForeColor="Red"/>
                    <div style="font-size:11px;color:#800000">Completed pdf application only.</div>
                </td>
            </tr>
            <tr>
                <td style="text-align:right;vertical-align:top"><b>Attach resume:</b></td>
                <td>
                    <asp:FileUpload ID="fluResume" runat="server" />&nbsp;<asp:Label ID="lblResumeValidation" runat="server" Font-Bold="true" ForeColor="Red"/>
                    <div style="font-size:11px;color:#800000">Acceptable file formats are 'doc', 'docx', 'txt', 'rtf', 'odt' and 'pdf'</div>
                </td>
            </tr>
            <tr>
                <td style="text-align:right;vertical-align:top"><b>Message:</b></td>
                <td>
                    <asp:TextBox ID="txtMessage" runat="server" ValidationGroup="avg" TextMode="MultiLine" Height="100px" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:CheckBox ID="ckbConfirm" runat="server" Text="I understand that clicking 'Submit' will be the equivalent of my signature and is a representation that I agree to the terms as stated on the application form." />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSubmit_Click" OnClientClick="if(!validate(this))return false;" Enabled="true" ><span>Submit</span></asp:LinkButton>
                </td>
            </tr>
        </table>
        <asp:label ID="lblStatus" Font-Bold="True" ForeColor="Red" Text="" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlEmailSent" runat="server" Visible="false">
        <asp:label ID="lblSent" Font-Bold="True" Text="" runat="server" />
    </asp:Panel>

    <script type="text/javascript">
        function validate(checkbox) {
            var isChecked = document.getElementById('<%= ckbConfirm.ClientID  %>').checked;
            if (!isChecked) {
                alert('Please agree to the terms.');
            }
            //alert(document.getElementById('<%= ckbConfirm.ClientID  %>').checked);
            return isChecked;
        }
    </script>
</asp:Content>


