<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="JobOpenings.aspx.vb" Inherits="Careers_JobOpenings" title="Current Career Opportunities" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <b>Job Openings</b>
    <asp:GridView ID="GridView1" runat="server" SkinID="gridviewlist" DataSourceID="sqlDS1">
        <Columns>
            <asp:TemplateField HeaderText="Job Openings" SortExpression="DocDescription">
                <ItemTemplate>
                    <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" AlternateText="" />&nbsp;
                    <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                                    Target="_blank" runat="server"/>
                </ItemTemplate>                
            </asp:TemplateField>
            <asp:BoundField DataField="lastupdate" HeaderText="Last Changed" SortExpression="lastupdate" />
        </Columns>
        <EmptyDataTemplate>
            <p>Currently there are no open positions at Empire District but feel free to fill out an 
            <asp:HyperLink ID="hlApplication" Text='application' runat='server' NavigateUrl='~/Careers/UploadPDFApp.aspx' ForeColor="Maroon" />
            to be on file as new openings develop.&nbsp;&nbsp;Thank you.</p>
        </EmptyDataTemplate>
    </asp:GridView>
    <br /><br />
    <center>
        Equal Opportunity Employer including Vets and Disabled
        <br />
        <a href="http://www1.eeoc.gov/employers/upload/eeoc_self_print_poster.pdf" target="_blank" >EEO is the Law</a>
        <br />
        <a href="http://www.dol.gov/ofccp/regs/compliance/posters/pdf/OFCCP_EEO_Supplement_Final_JRF_QA_508c.pdf" target="_blank" >EEO supplement</a>
    </center>
        
    <asp:SqlDataSource ID="sqlDS1" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT documents.lastupdate, documents.filename, documents.filebytes, documents.description AS DocDescription, documents.id FROM documents WHERE (documents.groupid = 14 AND documents.doctypeid = 55 AND ((documents.expdate IS NULL) OR ((documents.expdate IS NOT NULL) AND (documents.expdate >= { fn NOW() })))) ORDER BY lastupdate desc">
    </asp:SqlDataSource>
</asp:Content>

