Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports AjaxControlToolkit

Partial Class Register
    Inherits System.Web.UI.Page
    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim autotab As HtmlGenericControl = New HtmlGenericControl
        autotab.Attributes.Add("type", "text/javascript")
        autotab.TagName = "script"
        autotab.Attributes.Add("src", ResolveUrl("~/js/autotab.js"))
        Page.Header.Controls.Add(autotab)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Textbox1.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLookup.UniqueID + "','')")
        'Textbox2.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLookup.UniqueID + "','')")
        'TextBox3.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLookup.UniqueID + "','')")
        'txtName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLookup.UniqueID + "','')")
        'txtSSN.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLookup.UniqueID + "','')")

        Textbox1.Attributes("onkeypress") = "return recPreKey(event, this)"
        Textbox1.Attributes("onkeyup") = "onFull(event, this)"
        Textbox2.Attributes("onkeypress") = "return recPreKey(event, this)"
        Textbox2.Attributes("onkeyup") = "onFull(event, this)"
        TextBox3.Attributes("onkeypress") = "return recPreKey(event, this)"
        TextBox3.Attributes("onkeyup") = "onFull(event, this)"
        txtSSN.Attributes("onkeypress") = "return recPreKey(event, this)"
        txtSSN.Attributes("onkeyup") = "onFull(event, this," & lbLookup.ClientID & ")"
        Textbox1.Focus()
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String

        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "Username already exists. Please enter a different user name."

            Case MembershipCreateStatus.DuplicateEmail
                Return "A username for that e-mail address already exists. Please enter a different e-mail address."

            Case MembershipCreateStatus.InvalidPassword
                Return "Passwords are required to be at least 8 characters long including one special character (for example: john_123)."

            Case MembershipCreateStatus.InvalidEmail
                Return "The e-mail address provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidAnswer
                Return "The password retrieval answer provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidQuestion
                Return "The password retrieval question provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidUserName
                Return "The user name provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.ProviderError
                Return "The authentication provider Returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator."

            Case MembershipCreateStatus.UserRejected
                Return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator."

            Case Else
                Return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator."
        End Select
    End Function

    Protected Sub lbLookup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'System.Threading.Thread.Sleep(1000)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader
        Try
            lbLookup.Enabled = False 'disable button to avoid double click
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Textbox1.Text)
            Dim strBillPkg As String = Trim(Textbox2.Text)
            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            Dim status = cwservice.ValidateCustomer(encrypt, strBillPkg, txtName.Text, txtSSN.Text)

            If status = 1 Then
                lblStatus.Text = ""

                Dim UserExists As Boolean = False  'Check if account already exists
                conn.Open()
                cmd.CommandText = "select count(*) AS result from aspnet_Users inner join aspnet_Membership on aspnet_Users.UserId = aspnet_Membership.UserId inner join aspnet_Profile ON aspnet_Users.UserId = aspnet_Profile.UserId where (patindex('" & Textbox1.Text & strBillPkg & "', aspnet_Profile.PropertyValuesString) > 0)"
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        UserExists = dr("result").ToString
                    End While
                End If
                dr.Close()
                conn.Close()
                If UserExists Then
                    lblStatus.Text = "Online account already exists for this customer. "
                Else
                    pnlValidateUser.Visible = False
                    pnlCreateUser.Visible = True
                    UserName.Focus()
                End If

            ElseIf status = 0 Then
                lblStatus.Text = "Cannot find user. Please verify account information."
            ElseIf status = 2 Then
                lblStatus.Text = "Unable to validate account. Please contact Customer Service at 800-206-2300."
            End If
            lbLookup.Enabled = True
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Protected Sub lbCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lbCreate.Enabled = False
        lblStatus.Text = ""  'clear out any previous status message
        Dim uc As Web.UI.UserControl = CType(Page.Master.FindControl("modal1"), Web.UI.UserControl)
        Dim mp As ModalPopupExtender = CType(uc.FindControl("mpeProgress"), ModalPopupExtender)
        If Not uc Is Nothing Then
            If Not mp Is Nothing Then
                mp.Show()  'enable modal loading screen
            End If
        End If

        lblStatus.Text = ""
        If CreateUser() Then
            pnlCreateUser.Visible = False
            pnlSuccess.Visible = True
            mp.Hide() 'disable modal loading screen
            'Response.Redirect("~/CustomerService/MyAccount/Default.aspx")
        Else
            lbCreate.Enabled = True
            mp.Hide() 'disable modal loading screen
        End If
    End Sub

    Public Function CreateUser() As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim result As MembershipCreateStatus
        Dim newUser As MembershipUser
        Dim strBillPkg As String = Trim(Textbox2.Text)
        If strBillPkg.Length = 1 Then
            strBillPkg = "0" & strBillPkg
        End If

        Try
            ' Create new user.
            newUser = Membership.CreateUser(UserName.Text, Password.Text, Email.Text, Nothing, Nothing, True, result)

            If Not newUser Is Nothing Then
                Dim thisUserProfile As ProfileCommon = ProfileCommon.Create(newUser.UserName, True)
                If Not Roles.IsUserInRole(newUser.UserName, "Customer") Then
                    Roles.AddUserToRole(newUser.UserName, "Customer")
                    thisUserProfile.CustomerAccountNumber = Textbox1.Text
                    thisUserProfile.BillingPackageToken = strBillPkg
                    thisUserProfile.Save()
                    FormsAuthentication.SetAuthCookie(newUser.UserName, False)

                    ' *** Add info to table to get email address included with customer watch
                    Dim cmd As New SqlCommand("", conn)
                    Dim dr As SqlDataReader
                    Dim RecordExists As Boolean = False  'Check if ebilling record already exists
                    conn.Open()

                    cmd.CommandText = "select count(*) AS result from ebilling where (customer_tkn='" & Textbox1.Text & "' and billing_pkg_tkn='" & strBillPkg & "')"
                    dr = cmd.ExecuteReader
                    If dr.HasRows Then
                        While dr.Read
                            RecordExists = dr("result").ToString
                        End While
                    End If
                    dr.Close()

                    Dim updatequery As String = "update ebilling set email='" & Email.Text & "', status=1,actioncode=3,comment='update email',lastupdate='" & Now & "' where customer_tkn='" & Textbox1.Text & "' and billing_pkg_tkn='" & strBillPkg & "'"
                    Dim insertquery As String = "insert into ebilling(customer_tkn,billing_pkg_tkn,email,status,actioncode,comment,lastupdate) " & _
                    "values('" & Textbox1.Text & "','" & strBillPkg & "','" & Email.Text & "'," & 1 & "," & 3 & ",'update email','" & Now & "')"
                    If RecordExists Then
                        cmd.CommandText = updatequery
                    Else
                        cmd.CommandText = insertquery
                    End If
                    cmd.ExecuteNonQuery()
                    conn.Close()
                    ' *** end update table

                End If
            Else
                Dim resultParam As MembershipCreateStatus = result
                lblStatus.Text = GetErrorMessage(resultParam)
                Return False
            End If
        Catch er As MembershipCreateUserException
            lblStatus.Text = GetErrorMessage(er.StatusCode)
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return False
        Catch er As HttpException
            lblStatus.Text = "ErrorMessage2: " & er.Message
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return False
        End Try
        Return True
    End Function
End Class
