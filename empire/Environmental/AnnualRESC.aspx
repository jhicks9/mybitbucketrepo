﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AnnualRESC.aspx.vb" Inherits="Environmental_AnnualRESC" Title="Annual RESC" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <hr style="color:#113962;height:1px;"/>

    <br /><b>Annual Renewable Energy Standard (RES) Compliance Report</b>
    <br /><br />
    Annually Empire submits a report to the Missouri Public Service Commission outlining measures the Company has taken to meet the Renewable Energy Standard set forth in Missouri. 
    <br /><br />
    Empire makes this report available to customers to ensure they can fully understand how their electric company is working to meet their energy needs through a balanced mix of fuel resources including renewable sources.
    <br /><br />
    <asp:DataList ID="dlRESC" runat="server" SkinID="DataList" DataSourceID="sqlDS">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("Description") %>'
                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                    Target="_blank" runat="server"/>
        </itemtemplate>
    </asp:DataList>

    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select id,description from documents where groupid = 14 and doctypeid = 102 and (webdescription like '%RES' or webdescription like 'RES%' or webdescription like '%RES%')">
    </asp:SqlDataSource>
</asp:Content>
