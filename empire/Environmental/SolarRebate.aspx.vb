﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class SolarRebate
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader
        Try
            conn.Open()
            cmd.CommandText = "select top 1 id,description from documents where groupid = 14 and doctypeid = 77 and (webdescription like '%Solar Rebate Rider' or webdescription like 'Solar Rebate Rider%' or webdescription like '%Solar Rebate Rider%')"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hlSRRider.NavigateUrl = "~/DocHandler.ashx?id=" & dr("id").ToString
                End While
            End If
            dr.Close()

            cmd.CommandText = "select top 1 id,description from documents where groupid = 14 and doctypeid = 134 and (webdescription like '%Solar Rebate Application' or webdescription like 'Solar Rebate Application%' or webdescription like '%Solar Rebate Application%')"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hlSRApplication.NavigateUrl = "~/DocHandler.ashx?id=" & dr("id").ToString
                End While
            End If
            dr.Close()

            cmd.CommandText = "select top 1 id,description from documents where groupid = 14 and doctypeid = 134 and (webdescription like '%Net Metering Application' or webdescription like 'Net Metering Application%' or webdescription like '%Net Metering Application%')"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hlNMAgreement.NavigateUrl = "~/DocHandler.ashx?id=" & dr("id").ToString
                End While
            End If
            dr.Close()

            cmd.CommandText = "select top 1 id,description from documents where groupid = 14 and doctypeid = 77 and (webdescription like '%Net Metering Rider' or webdescription like 'Net Metering Rider%' or webdescription like '%Net Metering Rider%')"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hlNMRider.NavigateUrl = "~/DocHandler.ashx?id=" & dr("id").ToString
                End While
            End If
            dr.Close()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub
End Class