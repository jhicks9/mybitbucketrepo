﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="EnergyChallenges.aspx.vb" Inherits="Environmental_EnergyChallenges" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <hr style="color:#113962;height:1px;"/>
    <br />
     <div style='float:right;margin-left:5px'>
        <asp:Image ID="imgEnvironmentalMain" Imageurl="~/images/environmental-energychallenges.jpg" runat="server" />
    </div>
    To ensure safe, reliable energy is there when you need it, we face more challenges today than ever before.  We’re working to meet these challenges with cost-effective solutions.  
    <br /><br />
    Since 2005, our investments in environmental and efficiency upgrades, including current projects at our Asbury and Riverton Power Plants, total nearly $475 million.  While beneficial, these upgrades do not come without a cost to ratepayers.  To moderate the rate impact, we work diligently to identify least-cost options. 
    <br /><br />
    Read more about how we’re working to meet today’s energy challenges.
    <br />
    <asp:DataList ID="dlMC" runat="server" SkinID="DataList" DataSourceID="sqlDS">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("Description") %>'
                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                    Target="_blank" runat="server"/>
        </itemtemplate>
    </asp:DataList>

    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select top 1 id,description from documents where groupid = 14 and doctypeid = 102 and (webdescription like '%challenge' or webdescription like 'challenge%' or webdescription like '%challenge%')">
    </asp:SqlDataSource>
</asp:Content>

