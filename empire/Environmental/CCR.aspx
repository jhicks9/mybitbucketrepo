﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="CCR.aspx.vb" Inherits="Environmental_CCR" title="CCR Rule Compliance Data and Information" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content5" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <hr style="color:#113962;height:1px;"/>
    <div id="CCRAsbury">
    <br /><b>CCR Rule Compliance Data and Information</b>
    <br /><br />
    The <asp:Hyperlink ID="hlAsbury" runat="server" Text="Asbury Power Plant" NavigateUrl="~/images/maps/Asbury.jpg" /> has been providing safe, reliable and economical energy to Empire customers for over 40 years. The Plant utilizes the latest environmental controls to reduce emissions including a selective catalytic reduction system, scrubber, baghouse and powder-activated carbon injection system.        
    <br /><br />
    The Asbury Plant utilizes coal fuel to generate electricity. An impoundment is located on the plant site for disposal of coal combustion residuals (CCR). As required by the Environmental Protection Agency's CCR Rule, this page provides information related to compliance, inspections and actions regarding the impoundment.
    <br /><br />
    <b>What are Coal Combustion Residuals (CCR)?</b>
    <br /><br />
    Coal combustion residuals are the materials that remain after pulverized coal is burned to generate electricity. There are several different types of materials produced including:
    <br /><br />
    Fly Ash - a very fine, powdery material composed mostly of silica which "flies" up into the stack with flue gases and is removed by electrostatic precipitators and fabric filter baghouses.
    <br /><br />
    Bottom Ash and boiler slag - coarse particles that are too large to be carried up into the stack, so they settle to the bottom of the combustion chamber.
    <br /><br />
    Flue Gas Desulfurization Material (FGD) - a material leftover from the process of reducing sulfur dioxide emissions from a coal-fired boiler.
    <br /><br />
    These materials have a wide range of beneficial uses in the construction, manufacturing, environmental remediation and other industries. Those not recycled for beneficial use are disposed of in utility landfills or impoundments.
    <br /><br />
    <b>Links</b>
    <br />
    </div>
    <asp:DataList ID="dlCCR" runat="server" SkinID="DataList" DataSourceID="sqlDS">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("Description") %>'
                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                    Target="_blank" runat="server"/>
        </itemtemplate>
    </asp:DataList>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <asp:HyperLink ID="hlContact" runat="server" NavigateUrl="~/About/ContactUs.aspx" Text="Empire Contacts" />
        <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select top 5 id,description from documents where groupid = 14 and doctypeid = 102 and (webdescription like '%CCR' or webdescription like 'CCR%' or webdescription like '%CCR%') ">
    </asp:SqlDataSource>
    <script type="text/javascript">jQuery("#CCRAsbury a").slimbox();</script>
</asp:Content>
