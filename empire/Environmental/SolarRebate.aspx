﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="SolarRebate.aspx.vb" Inherits="SolarRebate" Title="Solar Rebate"  %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content6" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <hr style="color:#113962;height:1px;"/>
    <div id="SRInfo">
    <br />
    <center><b>Solar Rebate Information</b><br />(for Missouri Electric Customers)</center>
    <br /><br />
    Empire is currently accepting solar rebate applications pursuant to its Solar Rebate Rider, tariff sheets 23-23h, and rebate payments are available pursuant to law and Empire's tariff. Empire has not filed with the MoPSC a sixty-day notice of reaching its annual retail rate impact limit pursuant to RSMo. Section 393.1030.
    <br /><br />
    Effective May 16, 2015, Empire began offering rebates for Missouri customers for qualifying solar installations in accordance with the Missouri Renewable Energy Standard and <asp:Hyperlink ID="hlSRRider" runat="server" Target="_blank" NavigateUrl="" Text="Empire's Solar Rebate Rider"/> approved by the Missouri Public Service Commission (MPSC). Rebates are available on a first-come, first-served basis determined by the application postmark date and availability guidelines listed below.
    <br />
        <ul >
            <li>
                Customer must submit <asp:Hyperlink ID="hlSRApplication" runat="server" Target="_blank" NavigateUrl="" Text="Missouri Solar Electric Rebate Application" /> with all required documentation to:<br /><br />
                &nbsp&nbsp&nbsp&nbsp The Empire District Electric Company<br />
                &nbsp&nbsp&nbsp&nbsp ATTN: ENERGY SERVICES/NET METERING<br />
                &nbsp&nbsp&nbsp&nbsp PO Box 127<br />
                &nbsp&nbsp&nbsp&nbsp Joplin MO 64802<br /><br />
            </li>
            <li>
                Customer must have a completed and approved <asp:Hyperlink ID="hlNMAgreement" runat="server" Target="_blank" NavigateUrl="" Text="Net Metering Application and Agreement" /> on file with the Company in accordance with the Company's <asp:Hyperlink ID="hlNMRider" runat="server" Target="_blank" NavigateUrl="" Text="Net Metering Rider" />.
            </li>
            <li>
                Customer must be an active account on the Company's system and in good payment standing.
            </li>
            <li>
                The system must be permanently installed on the customer's premise.
            </li>
            <li>
                The customer must declare the installed system will remain in place on the account holder's premise for the duration of its useful life which shall be deemed to be a minimum of ten (10) years.
            </li>
            <li>
                The solar modules and inverters shall be new equipment and include a manufacturer's warranty of ten (10) years.
            </li>
            <li>
                No retail electric account will be eligible for a solar rebate for less than 500 watts (0.5 kW) or more than twenty-five kilowatts (25 kW) of new or expanded new capacity irrespective of the number of meters/service points associated with the account holder.
            </li>
            <li>
                The system shall meet all requirements of 4 CSR 240-20.065 and the Company's Net Metering Rider.
            </li>
            <li>
                The system must be situated in a location where a minimum of eighty-five percent (85%) of the solar resource is available to the system.
            </li>
            <li>
                The customer must execute an affidavit for Company's use in complying with §393.1030 RSMo.
            </li>
            <li>
                The system or expansion of an existing system must become operational after December 31, 2009, and on or before June 30, 2020.
            </li>
            </ul>
    <br /><br />
    <center>
        <b>Summary of Rebate Levels for Approved Applications</b><br />
        <asp:Image ID="imgSRSummary" ImageAlign="Middle" Style="padding-top: 5px" Imageurl="~/images/environmental-solarrebatesummary.png" runat="server" />
    </center>
    <br /><br />
    <b>Related Document Links</b>
    <br /><br />
    </div>
    <asp:DataList ID="dlSolar" runat="server" SkinID="DataList" DataSourceID="sqlDS">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("Description") %>'
                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                    Target="_blank" runat="server"/>
        </itemtemplate>
    </asp:DataList>
        
    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,description FROM documents where (doctypeid = 134 or (doctypeid in (70,77) and (webdescription like '%net metering%standards' or webdescription like 'net metering%standards%' or webdescription like '%net metering%standards%'))) order by description">
    </asp:SqlDataSource>
</asp:Content>
