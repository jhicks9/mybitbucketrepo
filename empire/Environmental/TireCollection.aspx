﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="TireCollection.aspx.vb" Inherits="Environmental_TireCollection" title="Tire Collection" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <hr style="color:#113962;height:1px;"/>
    <br />
     <div style='float:right;margin-left:5px'>
        <asp:Image ID="imgEnvironmentalMain" Imageurl="~/images/environmental-tirecollection.jpg" runat="server" />
    </div>

    Since 2002, Empire has utilized tire-derivied fuel (TDF) for generating electricity at the Asbury Power Plant. Since then, the Company has used about 96 million pounds of TDF or approximately 4.8 million tires. TDF accounts for approximately one percent of the plant’s total fuel mix by weight.
    <br /><br />
    <div>
        Empire is committed to using this fuel source for electric production as it helps free the environment of used tires that are considered an environmental hazard. The benefits of TDF include:
        <ul style="list-style-type:none">
        <li>√ Saving landfill space</li>
        <li>√ Reduced illegal tire dumping</li>
        <li>√ Removes a mosquito-breeding habitat</li>
        <li>√ Preserves our natural resources by replacing fossil fuels</li>
        </ul>
        In communities throughout the service territory, Empire holds free used tire collections. These collections conserve our natural resources and keep communities clean. Look for the dates of these collections in the announcement section on the front page.
        Guidelines for tire collections includes:
        <ul style="list-style-type:none">
        <li>√ Automotive tires only</li>
        <li>√ Limit of 10 tires per household</li>
        <li>√ Tires should be clean</li>
        <li>√ Commercial tires not accepted</li>
        </ul>
        <asp:DataList ID="dlResidential" runat="server" SkinID="DataList" DataSourceID="sqlDS">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("Description") %>'
                       NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                       Target="_blank" runat="server"/>
        </itemtemplate>
        </asp:DataList>
    </div>
    <div style="clear:both;"></div>

    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select top 1 id,description from documents where groupid = 14 and doctypeid = 102 and (webdescription like '%tire' or webdescription like 'Tire%' or webdescription like '%tire%') ">
    </asp:SqlDataSource>        
</asp:Content>