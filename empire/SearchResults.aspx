﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="SearchResults.aspx.vb" Inherits="sr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .search-input{padding-top:3px;line-height:1.25em;color:#646458;background:none;border:none;width:250px;margin:0;outline:none;height:20px;}
    </style>
    <script type="text/javascript">
        function EnterEvent(e) {
            if (e.keyCode == 13) {
                __doPostBack('<%=imgSearch.UniqueId%>', "");
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
        <asp:HiddenField ID="hdnSearchString" runat="server" />
        <table style="border-collapse:collapse;border-spacing:0;width:100%">
            <tr>
                <td align"left" style="width:50%">
                    <table style="border-collapse:collapse;border-spacing:0;border:solid 1px #ccc;background-color:#fff;">
                        <tr>
                            <td><asp:TextBox id="txtSearch" runat="server" CssClass="search-input" Text="Search" onKeyPress="return EnterEvent(event)" /></td>
                            <td style="padding-right:3px"><asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/images/icon/icon_search.png" /></td>
                        </tr>
                        <tr><td colspan="2"><asp:Label ID="lblStatus" runat="server" Text="" /></td></tr>
                    </table>
                </td>
                <td align="right" style="width:50%"><asp:Label ID="lblTotalHits" runat="server" Text="" /></td>
            </tr>
        </table>
        <hr>
        <asp:Repeater id="rptSearchResults" runat="server">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>
                <a href='<%# DataBinder.Eval(Container.DataItem, "Url") %>' style='font-weight:bold;/*color:#1A0DAB*/'><%# DataBinder.Eval(Container.DataItem, "Title")%></a><br />
                <div style='color:#006621'><%# DataBinder.Eval(Container.DataItem, "url")%></div>
                <%# DataBinder.Eval(Container.DataItem, "html")%>
            </ItemTemplate>
            <SeparatorTemplate><br/ ><br /></SeparatorTemplate>
        </asp:Repeater>
</asp:Content>
