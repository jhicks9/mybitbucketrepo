<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ServiceMap.aspx.vb" Inherits="ServiceMap" title="Service Map" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style="width:600px;">
        <div style="text-align:center;color:#36587a;font-weight:bold;font-size:12pt;font-family:Arial;">Select a State</div>
    </div>

    <img id="imgMap" src="~/images/maps/map-usa.gif" runat="server" alt="Empire District" border="0" usemap="#Map" />
    <map name="Map" id="Map">
    <area shape="poly" coords="297,154,339,154,370,205,310,210" href="javascript:void(0)" alt="Missouri" onclick="window.open('servicearea.aspx?state=MO','_blank','resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes');" onmouseover="hlMap('map-mo.gif');" onmouseout="hlMap('map-usa.gif');"/>
    <area shape="poly" coords="309,212,365,213,349,260,316,250" href="javascript:void(0)" alt="Arkansas" onclick="window.open('servicearea.aspx?state=AR','_blank','resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes');" onmouseover="hlMap('map-ar.gif');" onmouseout="hlMap('map-usa.gif');"/>
    <area shape="poly" coords="231,160,301,162,309,205,227,200" href="javascript:void(0)" alt="Kansas"   onclick="window.open('servicearea.aspx?state=KS','_blank','resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes');" onmouseover="hlMap('map-ks.gif');" onmouseout="hlMap('map-usa.gif');"/>
    <area shape="poly" coords="216,201,308,204,311,251,248,237,249,210,216,208" href="javascript:void(0)" alt="Oklahoma" onclick="window.open('servicearea.aspx?state=OK','_blank','resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes');" onmouseover="hlMap('map-ok.gif');" onmouseout="hlMap('map-usa.gif');"/>
    </map>

<script type="text/javascript">
    function hlMap(src)
    {   var clid = "<%= imgMap.ClientID %>";
        document.getElementById(clid).src = '<%=ResolveUrl("~/images/maps/' + src + '")%>';  }
</script> 
</asp:Content>

