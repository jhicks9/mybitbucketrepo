<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Leadership.aspx.vb" Inherits="Leadership" title="Leadership" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <img id="imgBradB" src="~/Images/leadership/BradB.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Bradley Beecher"/>
    <b>Bradley P. Beecher, President and Chief Executive Officer</b>,
    became President and CEO on June 1, 2011.  He joined The Company in 1988 as a Staff Engineer at the Riverton Power Plant.  He was elected Vice President � Energy Supply in 2001 and Vice President and COO � Electric in 2006.  He was elected Executive Vice President in February 2010.<br /><br />
    Mr. Beecher graduated from Kansas State University with a Bachelor of Science degree in Chemical Engineering. He is a registered professional engineer in the State of Kansas.<br /><br />
    Mr. Beecher serves on the boards of the Edison Electric Institute, Missouri Energy Development Association, Joplin Chamber of Commerce, Boys and Girls Club of Southwest Missouri, Kiwanis Club of Joplin and Joplin Regional Partnership.  He is a graduate of Leadership Missouri. 
    <br /><br /><asp:HyperLink ID="hlPublishingBradB" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/BradBeecher.jpg">Download a publishing-quality photo of Brad Beecher</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

    <img id="imgLaurieD" src="~/Images/leadership/LaurieD.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Laurie Delano"/>
    <b>Laurie A. Delano, Vice President � Finance and Chief Financial Officer</b>,
    was elected to her current position in July 2011.  She first joined the Company in 1979 and served as Director of Internal Auditing from 1983 to 1991.  After an eleven-year separation from Empire District, Ms. Delano re-joined the Company in 2002 as Director of Financial Services and Assistant Controller.  She was named to the position of Controller, Assistant Secretary and Assistant Treasurer in July 2005.<br /><br />
    During the separation in employment, she was an Accounting Lecturer at Pittsburg State University and held accounting management positions with TAMKO Building Products, Inc. and Lozier Corporation.<br /><br />
    A native of southwest Missouri, Ms. Delano received an Associate of Arts from Crowder College and a Bachelor of Science in Business Administration from Missouri Southern State University.  She also holds a Master of Business Administration from Missouri State University.  Ms. Delano is a Certified Public Accountant and Certified Management Accountant.  She is a member of the American Institute of Certified Public Accountants and the Institute of Management Accountants.<br /><br />
    Ms. Delano serves on the board of the Joplin Redevelopment Corporation (JRC) and the Missouri Southern State University School of Business Advisory Council.  She has also been active with United Way organizations and agencies, and is a past President of the board of directors of the United Way of Southwest Missouri and the Lafayette House.  She currently serves on the Endowment Committee for the Lafayette House.  She is a member of the Joplin Daybreak Rotary.
    <br /><br /><asp:HyperLink ID="hlPublishingLaurieD" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/LaurieDelano.jpg">Download a publishing-quality photo of Laurie Delano</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

    <img id="imgKellyW" src="~/Images/leadership/KellyW.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Kelly Walters"/>
    <b>Kelly S. Walters, Vice President and Chief Operating Officer � Electric</b>,
    was elected to her current position in 2011.  After joining the Company in 1988 as a Fuel Accountant, she held various accounting and management positions before being named Director of Auditing in 1997.  During a four-year separation she served as the Director of Financial Services for Crowder College in Neosho.  She returned to the Company in 2001 as Director of Planning and Regulatory and was later named General Manager of Regulatory and General Services.  In 2006 Ms. Walters was elected Vice President � Regulatory and General Services.<br /><br />
    A native of southwest Missouri, Ms. Walters holds a Bachelor of Science degree in Accounting from Pittsburg State University and a Masters degree in Human Resource Management from Webster University, St. Louis, Missouri.<br /><br />
    Ms. Walters serves on the Missouri Workforce Investment Board, United Way of Southwest Missouri and Southeast Kansas Board, Financial Research Institute Advisory Board and the Members Committee of the Southwest Power Pool Board.  She is a former board member of the American Red Cross Southwest Missouri Chapter, Joseph Newman Innovation Center and Crowder College Foundation.  She is a graduate of Leadership Missouri.
    <br /><br /><asp:HyperLink ID="hlPublishingKellyW" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/KellyWalters.jpg">Download a publishing-quality photo of Kelly Walters</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

    <img id="imgRonG" src="~/Images/leadership/RonG.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Ronald Gatz"/>
    <b>Ronald F. Gatz, Vice President and Chief Operating Officer � Gas</b>,
    was elected to his current position in June 2006. He joined the Company as General Manager of Nonregulated Services in 2001 and was elected to Vice President in April of that year.<br /><br />
    Mr. Gatz is a native of Newton, Kansas, and earned a Bachelor of Science degree in Agricultural Economics from Kansas State University and a graduate degree in banking from the University of Delaware. Gatz was in the banking industry for 28 years, with 12 of those years at Mercantile Bank of Joplin as Executive Vice President, Senior Credit Officer and Chief Financial Officer.  Prior to coming to Empire, he was Chief Administrative Officer for Hook Up, Inc. of Joplin.<br /><br />
    Mr. Gatz serves on the board and is Treasurer of the Joplin Schools Foundation. He also serves as�Chairman of the Joplin Industrial Development Authority and is Chairman of the Joplin Tax Increment Finance Commission. He is a member of the Joplin Rotary Club.
    <br /><br /><asp:HyperLink ID="hlPublishingRonG" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/RonGatz.jpg">Download a publishing-quality photo of Ron Gatz</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />
 
    <img id="imgBlakeM" src="~/Images/leadership/BlakeM.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Blake Mertens"/>
    <b>Blake A. Mertens, Vice President � Energy Supply and Delivery Operations</b>,
    was elected to his current position in May 2011. He joined the Company in 2001 as Staff Engineer in Energy Supply.  Positions he has held include Planning Engineer, Combustion Turbine Construction Project Manager, Manager of Strategic Projects and Associate Director of Strategic Projects.  He was named Director of Strategic Projects, Safety, and Environmental Services in January of 2010 prior to accepting the position of General Manager � Energy Supply in November of 2010.  Prior to his employment with Empire, Mr. Mertens was an Energy Consultant for Black & Veatch of Overland Park, Kansas.<br /><br />
    A native of the Wichita, Kansas area, Mr. Mertens received a Bachelor of Science degree in Chemical Engineering at Kansas State University and a Masters in Business Administration at Missouri State University.  He is a registered professional engineer in the State of Kansas.<br /><br />
    Mr. Mertens serves on the board of the Wildcat Glades Conservation and Audubon Center, currently as Vice-Chairman, and is a member of the Joseph Newman Innovation Center board.
    <br /><br /><asp:HyperLink ID="hlPublishingBlakeM" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/BlakeMertens.jpg">Download a publishing-quality photo of Blake Mertens</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />
    
    <img id="imgBrentB" src="~/Images/leadership/BrentB.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Brent Baker"/>
    <b>Brent A. Baker, Vice President � Customer Service, Transmission and Engineering</b>,
    was elected to his current position in 2015. He began his career at Empire District in 2002. Positions he has held include Structural Engineer, Senior Structural Engineer, Manager of Construction Design and Director of Customer Service.<br /><br />
    Mr. Baker, a native of Republic, Missouri, earned a Bachelor of Science in Civil Engineering at Missouri University of Science and Technology in Rolla, Missouri.  He is a registered professional engineer.<br /><br />
    Mr. Baker serves on the board for the Downtown Joplin Alliance and Children�s Haven of SW MO.  He is a member of Joplin Regional Economic Development Association and the Republic, Missouri Business & Industrial Development Association.  He is the past President and state director for the Missouri Society of Professional Engineers Southwest Chapter.  Mr. Baker is co-chair of the Young Professionals Network of the Joplin Area Chamber of Commerce.  He is a member of the Joplin Daybreak Rotary Club.
    <br /><br /><asp:HyperLink ID="hlPublishingBrentB" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/BrentBaker.jpg">Download a publishing-quality photo of Brent Baker</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

    <img id="imgDaleH" src="~/Images/leadership/DaleH.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Dale Harrington"/>
    <b>Dale W. Harrington, Corporate Secretary & Director of Investor Relations</b>,
    was elected Secretary effective May 1, 2015. He was named Director of Investor Relations in August 2014 and elected Assistant Secretary in October 2014. He joined the Company in 1989 as an Internal Auditor. Mr. Harrington has held positions in Financial and Regulatory Accounting and Human Resources. He was named to the position of Director of Financial Services in July 2011.<br /><br />
    A native of southwest Missouri, Mr. Harrington graduated from Missouri Southern State University with a Bachelor of Science in Business Administration with a major in Accounting.<br /><br />
    Mr. Harrington is a past President of the board of directors of the Lafayette House, and continues to actively serve on the Lafayette House board.� He also serves on the board of College Heights Christian School and as an elder of College Heights Christian Church.
    <br /><br /><asp:HyperLink ID="hlPublisingDaleH" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/DaleHarrington.jpg">Download a publishing-quality photo of Dale Harrington</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

    <img id="imgMarkT" src="~/Images/leadership/MarkT.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Mark Timpe"/>
    <b>Mark T. Timpe, Treasurer</b>,
    was elected to his current position in October 2014.  He joined the Company as Director of Financial Services in August 2014. Prior to employment with Empire, Mr. Timpe spent over 21 years with Con-way Truckload/CFI in Joplin where he served as CFI�s treasurer for 16 years and as Director in the Finance Department and Assistant Treasurer for Con-way Truckload for six years.<br /><br />
    A native of Quincy, Illinois, Mr. Timpe holds a Bachelor of Science in Business Administration�Finance and a Masters of Business Administration from St. Louis University.<br /><br />
    Mr. Timpe serves on the St. Mary's Parish (Joplin) Finance Committee and was its Parish Council President for several years.
    <br /><br /><asp:HyperLink ID="hlPublishingMarkT" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/MarkTimpe.jpg">Download a publishing-quality photo of Mark Timpe</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

    <img id="imgRobS" src="~/Images/leadership/RobS.jpg" runat="server" style="float:left;padding:0 5px 3px 0" alt="Robert Sager"/>
    <b>Robert W. Sager, Controller, Assistant Secretary and Assistant Treasurer</b>,
    was elected to his current position in July 2011. He joined the staff at Empire in 2006 as Director of Financial Servies. Prior to employment with Empire, Mr. Sager held the position of Senior Manager-Auditing and Consulting with BKD LLP in Joplin.<br /><br />
    A native of Hays, Kansas, Mr. Sager received a Bachelor of Science in Business Administration�Accounting from Pittsburg State University and is a Certified Public Accountant.  He is a member of the American Institute of Certified Public Accountants and serves on the Accounting Services Committee of the Edison Electric Institute.<br /><br />
    Mr. Sager serves on the board of the Community Clinic of Joplin and as elder at Good Shepherd Lutheran Church. Mr. Sager has previously served as a board member of the United way of Crawford County and the Pittsburg State University Accounting Advisory Council.
    <br /><br /><asp:HyperLink ID="hlPublishingRobS" runat="server" Target="_blank" NavigateUrl="~/Images/leadership/RobSager.jpg">Download a publishing-quality photo of Rob Sager</asp:HyperLink><br />
    <br style="clear:both;"/><hr style="color:#113962;height:1px;"/><br />

<strong>Directors<sup>1</sup></strong><br /><br />
<table style="width:100%">
    <tr>
        <td style="width:50%">
            <img id="img2" src="~/Images/leadership/KenA.jpg" runat="server" alt="Ken Allen"/><br />
            <strong><i>Kenneth Allen</i></strong><br />
            Retired Vice President, Finance and Chief Financial Officer<br />
            Texas Industries, Inc.<br />
            Dallas, Texas<br />
            (Age 57, Director since 2005)<br />
        </td>
        <td style="width:50%">
            <img id="img5" src="~/Images/leadership/BradB.jpg" runat="server" alt="Bradley Beecher"/><br />
            <strong><i>Bradley Beecher</i></strong><br />
            President and CEO<br />
            The Empire District Electric Company<br />
            Carl Junction, Missouri<br />
            (Age 49, Director since 2011)<br />
        </td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td>
            <img id="img4" src="~/Images/leadership/RossH.jpg" runat="server" alt="Ross Hartley"/><br />
            <strong><i>Ross Hartley</i></strong><br />
            Co-Founder and Director<br />
            NIC, Inc.<br />
            Teton Village, Wyoming<br />
            (Age 67, Director since 1988)<br />
        </td>
        <td>
            <img id="img6" src="~/Images/leadership/RandyL.jpg" runat="server" alt="Randy Laney"/><br />
            <strong><i>Randy Laney</i></strong><br />
            Chairman of the Board of Directors<br />
            The Empire District Electric Company<br />
            Farmington, Arkansas<br />
            (Age 60, Director since 2003)<br />
        </td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td>
            <img id="img8" src="~/Images/leadership/BonnieL.jpg" runat="server" alt="Bonnie Lind"/><br />
            <strong><i>Bonnie Lind</i></strong><br />
            Senior Vice President, Chief Financial Officer and Treasurer<br />
            Neenah Paper Inc.<br />
            Alpharetta, Georgia<br />
            (Age 56, Director since 2009)<br />
        </td>
        <td>
            <img id="img9" src="~/Images/leadership/TomM.jpg" runat="server" alt="Thomas Mueller"/><br />
            <strong><i>Thomas Mueller</i></strong><br />
            Founder, President and CEO<br />
            SALOV North America Corporation<br />
            Montclair, New Jersey<br />
            (Age 67, Director since 2003)<br />
        </td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td>
            <img id="img12" src="~/Images/leadership/TomO.jpg" runat="server" alt="Thomas Ohlmacher"/><br />
            <strong><i>Thomas Ohlmacher</i></strong><br />
            Retired President and COO of Black Hills<br /> 
            Corporation�s Non-Regulated Energy Group<br />
            Fort Collins, Colorado <br />
            (Age 63, Director since 2011)<br />
        </td>
        <td>
            <img id="img10" src="~/Images/leadership/PaulP.jpg" runat="server" alt="Paul Portney"/><br />
            <strong><i>Dr. Paul Portney</i></strong><br />
            Retired Professor of Economics and former Dean<br />
            Eller College of Management<br />
            University of Arizona<br />
            Santa Barbara, California<br />
            (Age 69, Director since 2009)<br />
        </td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td>
            <img id="img11" src="~/Images/leadership/HerbS.jpg" runat="server" alt="Herbert Schmidt"/><br />
            <strong><i>Herbert Schmidt</i></strong><br />
            Retired Executive Vice President, Con-way Inc. and<br /> 
            President, Con-way Truckload<br />
            The Villages, Florida<br />
            (Age 59, Director since 2010)<br />
        </td>
        <td>
            <img id="img7" src="~/Images/leadership/JamesS.jpg" runat="server" alt="James Sullivan"/><br />
            <strong><i>C. James Sullivan</i></strong><br />
            Principal<br />
            The Sullivan Group LLC<br />
            Birmingham, Alabama<br />
            (Age 68, Director since 2010)<br />
        </td>
    </tr>
   </table>

<br /><br /><strong>Committees of the Board</strong><br />
Audit Committee � Allen<sup>2</sup> (Chair), Hartley, Lind<sup>2</sup>, Mueller<sup>2</sup><br />
Compensation Committee � Laney, Mueller, Ohlmacher (Chair), Portney<br />
Nominating/Corporate Governance Committee � Hartley, Laney, Lind (Chair), Sullivan<br />
Retirement Committee � Allen, Schmidt, Sullivan (Chair)<br />
Security and Strategic Projects Committee � Ohlmacher, Portney, Schmidt (Chair), Sullivan<br />
Executive Committee � Allen, Beecher (Chair), Laney<br />
Risk Oversight Committee � Allen, Laney (Chair), Lind, Ohlmacher, Schmidt<br />
<br />   
<sup>1</sup> Ages shown as of March 1, 2015.<br />
<sup>2</sup> Audit Committee Financial Expert.<br />
</asp:Content>