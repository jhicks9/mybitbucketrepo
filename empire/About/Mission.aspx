<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Mission.aspx.vb" Inherits="Mission" title="Mission" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">

     <div style='float:right;padding:0 0 10px 5px'>
        <img id="imgMission" src="~/images/landing/landing-mission.jpg" runat="server" alt=""/>
    </div>

    <div >
        <table cellpadding="2" cellspacing="0" width="345px">
            <tr>
                <td colspan="2" style="background-color:#b0c4de;color:#113962;font-weight:bolder;font-size:1.2em;text-align:center">Vision</td>
            </tr>
            <tr>
                <td valign="top"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;margin-top:5px;"></div></td>
                <td valign="top">Making lives better every day with reliable energy and service</td>
            </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
                <td colspan="2" style="background-color:#b0c4de;color:#113962;font-weight:bolder;font-size:1.2em;text-align:center;">Goals</td>
            </tr>
            <tr>
                <td><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td>Effectively meet our customers’ expectations</td>
            </tr>
            <tr>
                <td><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td>Provide increasing value to our shareholders</td>
            </tr>
            <tr>
                <td><div style="margin-bottom:14px;width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td>Provide employees a safe and positive work experience</td>
            </tr>
            <tr>
                <td><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td>Be responsible stewards of the environment</td>
            </tr>
            <tr>
                <td><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td>Be a respected leader in the region</td>
            </tr>
        </table>
    </div>
</asp:Content>

