﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="MobileApp.aspx.vb" Inherits="About_MobileApp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style='float:right;padding:0 0 10px 5px'>
            <img id="imgMobile" src="~/images/aboutus-mobileapp.png" runat="server" alt=""/>
    </div>
    <div>    
        <table cellpadding="0" cellspacing="0" width="440px">
            <tr>
                <td colspan="2">The Empire District mobile application provides convenient online access to your Empire account and billing information.</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td valign="middle">Find payment locations</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td valign="middle">See current outages</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td valign="middle">Get stock quote</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td valign="middle">Access MyAccount</td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2">Access to MyAccount (online billing information) requires an active Empire District online account. MyAccount offers easy-to-use features including:</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td valign="middle">Examine your current bill at any time.</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px;"></div></td>
                <td valign="middle">View account payment history.</td>
            </tr>
            <tr>
                <td valign="middle"><div style="width:7px;height:7px;background-color:#113962;margin-left:15px"></div></td>
                <td valign="middle">View and track your usage.</td>
            </tr>
        </table>
        <br /><br />
        <table cellpadding="0" cellspacing="0" width="440px">
            <tr>
                <td><a href="https://itunes.apple.com/us/app/empire-district/id700715059?mt=8&uo=4" target="_blank"><img id="imgAppStore" src="~/images/aboutus-appstore.jpg" runat="server" alt="" class="icon"/></a></td>
                <td><a href="https://play.google.com/store/apps/details?id=com.empiredistrict" target="_blank"><img id="imgPlayStore" src="~/images/aboutus-playstore.jpg" runat="server" alt="" class="icon"/></a></td>
            </tr>
        </table>
        <br /><br />
        There are no costs involved with the Empire District mobile application. Connectivity and data usage rates from your communications provider may apply.
    </div>
</asp:Content>

