<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ServiceArea.aspx.vb" Inherits="About_ServiceArea" %>

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Empire District Service Area</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link id="css1" href="~/slimbox2.css" rel="stylesheet" runat="server" type="text/css" />
    <script src='<%#ResolveUrl("~/js/jquery-1.8.3.min.js")%>' type="text/javascript" charset="utf-8"></script>
    <script src='<%#ResolveUrl("~/js/slimbox2.js")%>' type="text/javascript" charset="utf-8"></script>
</head>
<body style="height:100%;margin:0;padding:0;text-align:center;background:#707883;color:#222;font:0.8em arial,verdana,sans-serif;">
    <form id="form1" runat="server">
        <div id="AR" style="display:none;" ><%--ARKANSAS--%>
            <asp:ImageMap ID="imgMapAR" runat="server" ImageUrl="~/images/maps/map-ar-detail.gif" />
        </div>
        <div id="KS" style="display:none;" ><%--KANSAS--%>
            <asp:ImageMap ID="imgMapKS" runat="server" ImageUrl="~/images/maps/map-ks-detail.gif" />
        </div>
        <div id="MO" style="display:none;" ><%--MISSOURI--%>
            <asp:ImageMap ID="imgMapMO" runat="server" ImageUrl="~/images/maps/map-mo-detail.gif" />
        </div>
        <div id="OK" style="display:none;" ><%--OKLAHOMA--%>
            <asp:ImageMap ID="imgMapOK" runat="server" ImageUrl="~/images/maps/map-ok-detail.gif" />
        </div>
    </form>
</body>
<script type="text/javascript">jQuery("#AR area,#KS area,#OK area,#MO area").slimbox();</script>
</html>