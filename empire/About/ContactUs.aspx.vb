
Partial Class About_ContactUs
    Inherits System.Web.UI.Page

    Public Sub prepareEmail(ByVal sender As Object, ByVal e As System.EventArgs)
        lblEmailTo.Text = sender.Text
        If lblEmailTo.Text = "Customer Service" Then
            trAcctNumber.Visible = True
        Else
            trAcctNumber.Visible = False
        End If
        MultiView1.ActiveViewIndex = 1
        txtName.Focus()
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim MailObj As New System.Net.Mail.SmtpClient
            Dim message As New System.Net.Mail.MailMessage()
            Dim fromAddress As New System.Net.Mail.MailAddress(txtEmail.Text, txtName.Text)

            Select Case lblEmailTo.Text
                Case "Customer Service"
                    message.To.Add("customer.service@empiredistrict.com")
                Case "Corporate Communications"
                    message.To.Add("corp.communications@empiredistrict.com")
                Case "Investor Relations"
                    message.To.Add("investor.relations@empiredistrict.com")
                Case "Employment"
                    message.To.Add("employment@empiredistrict.com")
                Case "Economic Development"
                    message.To.Add("economic.development@empiredistrict.com")
                Case "Vegetation Management"
                    message.To.Add("vegetationmanagement@empiredistrict.com")
                Case "Environmental/CCR"
                    message.To.Add("environmental.ccr@empiredistrict.com")
            End Select
            message.From = fromAddress
            message.Subject = txtSubject.Text
            message.Body = "Account Number: " & txtAcctNumber.Text & vbLf & vbLf & txtMessage.Text
            'MailObj.Host = "" -- obtained from web.config 03/14/2012
            MailObj.Send(message)
            lblStatus.Text = "** Email Sent **"
            MultiView1.ActiveViewIndex = 2
        Catch
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbSubmit.UniqueID + "','')")
        txtEmail.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbSubmit.UniqueID + "','')")
        txtSubject.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbSubmit.UniqueID + "','')")

        If Len(Request.QueryString("type")) > 0 Then
            Select Case Request.QueryString("type")
                Case "cs"
                    lblEmailTo.Text = "Customer Service"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri  'returns user to appropriate page if they cancel
                Case "cc"
                    lblEmailTo.Text = "Corporate Communications"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri
                Case "ir"
                    lblEmailTo.Text = "Investor Relations"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri
                Case "em"
                    lblEmailTo.Text = "Employment"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri
                Case "ed"
                    lblEmailTo.Text = "Economic Development"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri
                Case "vm"
                    lblEmailTo.Text = "Vegetation Management"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri
                Case "ccr"
                    lblEmailTo.Text = "Environmental/CCR"
                    MultiView1.ActiveViewIndex = 1
                    lbCancel.PostBackUrl = Page.Request.UrlReferrer.AbsoluteUri
            End Select
        End If
    End Sub
End Class
