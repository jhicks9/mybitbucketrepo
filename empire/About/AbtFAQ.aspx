<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AbtFAQ.aspx.vb" Inherits="AbtFAQ" title="FAQ" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <a href="AbtFAQ.aspx#A1">How late are your offices open? What are the Contact Center hours?</a><br />
    <a href="AbtFAQ.aspx#A2">What is a kilowatt hour?</a><br />
    <a href="AbtFAQ.aspx#A3">Could my meter be faulty?</a><br />
    <a href="AbtFAQ.aspx#A4">Is there a fee to transfer my service?</a><br />
    <a href="AbtFAQ.aspx#A5">Do I have to be home when you come to turn on my service?</a><br />
    <a href="AbtFAQ.aspx#A6">I have a tree on my service line that needs trimming, do you do that?</a><br />
    <a href="AbtFAQ.aspx#A7">Can I get my service hooked up on Saturday or Sunday?</a><br />
    <a href="AbtFAQ.aspx#A8">About how long does it take to have a service turned on or my service transferred?</a><br />
    <a href="AbtFAQ.aspx#A9">Does Empire sell meter bases, poles, etc. to residential customers?</a><br />
    <a href="AbtFAQ.aspx#A10">What is the charge I'm paying per kilowatt hour?</a><br />
    <a href="AbtFAQ.aspx#A11">Why do my electric bills vary from month to month?</a><br />
    <a href="AbtFAQ.aspx#A12">I have a problem in my breaker box, with my weather-head, etc., can you have someone check on it?</a><br />
    <a href="AbtFAQ.aspx#A13">What does it mean if I have a utility right-of-way on my property?</a><br />
    <a href="AbtFAQ.aspx#A14">What number should I call if I need my underground utilities located?</a><br />
    <br /><br />

        <a id="A1"></a><strong>Q. How late are your offices open? What are the Contact Center hours?</strong><br />
        <b>A.</b> Our offices are open 8:00 a.m. to 4:30 p.m., Monday through Friday. The Contact Center is available for routine service requests and inquiries from 7 a.m. to 7 p.m., Monday through Friday, by calling 800-206-2300 for electric customers and 800-424-0427 for natural gas customers.  To report a gas leak, call 800-406-9220.  Contact Center representatives are always here to handle emergencies and outages.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A2"></a><strong>Q. What is a kilowatt hour?</strong><br />
        <b>A.</b> A kilowatt hour (KWH) is the measurement of electrical power used over a period of time. A 100-watt light bulb burning for ten hours uses one KWH of electricity.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A3"></a><strong>Q. Could my meter be faulty?</strong><br />
        <b>A.</b> It is very unlikely. Before a meter is installed, it must meet strict performance, reliability and accuracy standards, and while in use, meters are periodically checked by Empire. If your meter is faulty, it's more likely that it is running slow rather than fast. Because meters are mechanical, as they age, they tend to run slower due to the build-up on the operating parts.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A4"></a><strong>Q. Is there a fee to transfer my service?</strong><br />
        <b>A.</b> Transferring service requires no fee.  You can request your service to be transferred by filling out an online <asp:HyperLink ID="HyperLink1" runat='server' Text='transfer request' NavigateUrl='~/CustomerService/ServiceTransfer.aspx' /> or calling our Contact Center at 800-206-2300 for electric customers and 800-424-0427 for natural gas customers.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A5"></a><strong>Q. Do I have to be home when you come to turn on my service?</strong><br />
        <b>A.</b> For our electric customers, our service personnel will connect your service at your electric meter, which is located on the outside of your home. You do not have to be present.  For natural gas customers, you will need to be present to meet an Empire technician when turning on service.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A6"></a><strong>Q. I have a tree on my service line that needs trimming, do you do that?</strong><br />
        <b>A.</b> We trim tree limbs that are in the distribution and transmission lines. We do not trim tree limbs that are in individual service lines; however, if you need to trim trees around your service, we will disconnect your service while you trim and reconnect it when you are finished. Call our office a few days ahead to schedule this service.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />

        <a id="A7"></a><strong>Q. Can I get my service hooked up on Saturday or Sunday?</strong><br />
        <b>A.</b> During the weekends, we do not do routine service work.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />

        <a id="A8"></a><strong>Q. About how long does it take to have a service turned on or my service transferred?</strong><br />
        <b>A.</b> Like any business, our volume of business varies, but usually we can complete these services within two days.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />

        <a id="A9"></a><strong>Q. Does Empire sell meter bases, poles, etc. to residential customers?</strong><br />
        <b>A.</b> We sell 400 amp and larger meter bases for residential usage. We also sell a meter base, loop and pole setup for mobile homes and other specialty usages.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A10"></a><strong>Q. What is the charge I'm paying per kilowatt hour?</strong><br />
        <b>A.</b> Current rates, including sample residential bills, are available on our <asp:HyperLink ID="hlRates" runat='server' Text='Electric Rates and Standards page' NavigateUrl='~/CustomerService/Electric.aspx' />.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />

        <a id="A11"></a><strong>Q. Why do my electric bills vary from month to month?</strong><br />
        <b>A.</b> The largest variation in your bill is due to the seasonal temperature changes, and as a result, your usage. Some of our customers use more electricity in the summer months when they utilize their air conditioners. Our total electric customers may use more in the winter when they run their electric heat. Other things that may cause fluctuations, include vacations, visitors, new babies, new appliances, the list is endless.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A12"></a><strong>Q. I have a problem in my breaker box, with my weather-head, etc., can you have someone check on it?</strong><br />
        <b>A.</b> Our equipment ends at the weather-head. If you have a problem beyond that, we recommend you contact a licensed electrician. There are several listed in the phone book.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
        <a id="A13"></a><strong>Q. What does it mean if I have a utility right-of-way on my property?</strong><br />
        <b>A.</b> A utility right-of-way is a portion of your land that may be used by utilities. The right-of-way may contain underground power lines, transformers, poles and/or overhead lines. No obstructions are permitted within a right-of-way so utilities are able to repair equipment when necessary.
        <br /><br /><hr style="color:#113962;height:1px;"/><br />

        <a id="A14"></a><strong>Q. What number should I call if I need my underground utilities located?</strong><br />
        <b>A.</b> Nationwide you can call 811.  Otherwise, you can call each states service:
        &nbsp;&nbsp;Missouri 800&#8209;DIG&#8209;RITE (344&#8209;7483)
        &nbsp;&nbsp;Arkansas 800&#8209;482&#8209;8998
        &nbsp;&nbsp;Kansas 800&#8209;DIG&#8209;SAFE (344&#8209;7233)
        &nbsp;&nbsp;Oklahoma 800&#8209;522&#8209;OKIE (6543)
</asp:Content>

