
Partial Class About_ServiceArea
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim hsPlumbPoint As CircleHotSpot = New CircleHotSpot
        hsPlumbPoint.AlternateText = "Plum Point"
        hsPlumbPoint.Radius = 10
        hsPlumbPoint.X = 851
        hsPlumbPoint.Y = 183
        hsPlumbPoint.NavigateUrl = ResolveUrl("~/images/maps/PlumPoint.jpg")
        imgMapAR.HotSpots.Add(hsPlumbPoint)

        Dim hsRiverton As CircleHotSpot = New CircleHotSpot
        hsRiverton.AlternateText = "Riverton"
        hsRiverton.Radius = 10
        hsRiverton.X = 1173
        hsRiverton.Y = 769
        hsRiverton.NavigateUrl = ResolveUrl("~/images/maps/Riverton.jpg")
        imgMapKS.HotSpots.Add(hsRiverton)

        Dim hsMeridianWay As CircleHotSpot = New CircleHotSpot
        hsMeridianWay.AlternateText = "Meridian Way"
        hsMeridianWay.Radius = 10
        hsMeridianWay.X = 713
        hsMeridianWay.Y = 275
        hsMeridianWay.NavigateUrl = ResolveUrl("~/images/maps/MeridianWay.jpg")
        imgMapKS.HotSpots.Add(hsMeridianWay)

        Dim hsElkRiver As CircleHotSpot = New CircleHotSpot
        hsElkRiver.AlternateText = "Elk River"
        hsElkRiver.Radius = 10
        hsElkRiver.X = 864
        hsElkRiver.Y = 669
        hsElkRiver.NavigateUrl = ResolveUrl("~/images/maps/ElkRiver.jpg")
        imgMapKS.HotSpots.Add(hsElkRiver)

        Dim hsStateLine As CircleHotSpot = New CircleHotSpot
        hsStateLine.AlternateText = "State Line"
        hsStateLine.Radius = 10
        hsStateLine.X = 213
        hsStateLine.Y = 861
        hsStateLine.NavigateUrl = ResolveUrl("~/images/maps/StateLine.jpg")
        imgMapMO.HotSpots.Add(hsStateLine)

        Dim hsOzarkBeach As CircleHotSpot = New CircleHotSpot
        hsOzarkBeach.AlternateText = "Ozark Beach"
        hsOzarkBeach.Radius = 10
        hsOzarkBeach.X = 474
        hsOzarkBeach.Y = 955
        hsOzarkBeach.NavigateUrl = ResolveUrl("~/images/maps/OzarkBeach.jpg")
        imgMapMO.HotSpots.Add(hsOzarkBeach)

        Dim hsEnergyCenter As CircleHotSpot = New CircleHotSpot
        hsEnergyCenter.AlternateText = "Energy Center"
        hsEnergyCenter.Radius = 10
        hsEnergyCenter.X = 288
        hsEnergyCenter.Y = 858
        hsEnergyCenter.NavigateUrl = ResolveUrl("~/images/maps/EnergyCenter.jpg")
        imgMapMO.HotSpots.Add(hsEnergyCenter)

        Dim hsIatan As CircleHotSpot = New CircleHotSpot
        hsIatan.AlternateText = "Iatan"
        hsIatan.Radius = 10
        hsIatan.X = 164
        hsIatan.Y = 316
        hsIatan.NavigateUrl = ResolveUrl("~/images/maps/Iatan.jpg")
        imgMapMO.HotSpots.Add(hsIatan)

        Dim hsAsbury As CircleHotSpot = New CircleHotSpot
        hsAsbury.AlternateText = "Asbury"
        hsAsbury.Radius = 10
        hsAsbury.X = 222
        hsAsbury.Y = 815
        hsAsbury.NavigateUrl = ResolveUrl("~/images/maps/Asbury.jpg")
        imgMapMO.HotSpots.Add(hsAsbury)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header

        If Request.QueryString("state") = "MO" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "ShowPanel", _
            "<script language=""javascript"">document.getElementById('MO').style.display = 'inline';</script>")
        End If
        If Request.QueryString("state") = "KS" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "ShowPanel", _
            "<script language=""javascript"">document.getElementById('KS').style.display = 'inline';</script>")
            ClientScript.RegisterStartupScript(Me.GetType(), "ScrollRight", _
            "<script language=""javascript"">window.scrollBy(1500,500);</script>")
        End If
        If Request.QueryString("state") = "AR" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "ShowPanel", _
            "<script language=""javascript"">document.getElementById('AR').style.display = 'inline';</script>")
        End If
        If Request.QueryString("state") = "OK" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "ShowPanel", _
            "<script language=""javascript"">document.getElementById('OK').style.display = 'inline';</script>")
            ClientScript.RegisterStartupScript(Me.GetType(), "ScrollRight", _
            "<script language=""javascript"">window.scrollBy(1500,0);</script>")
        End If
    End Sub
End Class
