<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="History.aspx.vb" Inherits="History" title="History" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style="float:right;padding-left:5px;text-align:center;width:315px;">
        <img id="Img1" src="~/images/aboutus-history.jpg" runat="server" style="" alt="history"/>
        <br />
        <div style="width:100%;background-color:#113962;color:#ffffff;text-align:center;">Video</div>
        <div style="padding:5px 0 5px 0;">
            <asp:HyperLink ID="hlHistory" runat="server" NavigateUrl="~/CustomerService/TelevisionCommercials.aspx?v=5">Celebrating A Century Of Service</asp:HyperLink>
        </div>
        
        <div style="width:100%;background-color:#113962;color:#ffffff;text-align:center;">Book</div>
        <table cellpadding="2">
            <tr>
                <td valign="top">
                    <div style="width:190px;">
                        <asp:DataList ID="dlHistoryBook" runat="server" SkinID="DataList" DataSourceID="sqlDSOnlineBook">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                    </div>                 
                </td>
                <td valign="top" style="padding-left:5px;" align="left">
                    <acrobat:uc4 ID="uc4" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    The history of The Empire District Electric Company is closely interwoven with the story of the development of the four-state region. Although Empire was organized in 1909, tracing its history takes us back beyond the turn of the century for a look at a growing mining industry in the area.
    <br /><br />
    The use of electric power in the mining industry had been untested prior to 1890. Individual zinc and lead miners used mules and hard labor to scratch out a living. The names of the mines, Yellow Dog, Grasshopper, Windy Bill, and Morning Star, were as colorful as the people who worked them. As mining companies sprung up across the tri-state region, electric motors began to replace the mule and steam powered engines in several of the mines.
    <br /><br />
    Electricity generating companies were born in several locations across the region. These small companies appeared haphazardly to supply the new mining district with the power it demanded. On October 16, 1909, papers of incorporation were filed in Topeka, Kansas, bringing together Consolidated Light, Power and Ice Company, Spring River Power Company, The Galena Light and Power Company, and the Joplin Light, Power, and Water Company to form The Empire District Electric Company, under its parent company, Cities Services.
    <br /><br />
    The formation of Empire marked the end of indiscriminate electrical development in the area, and was the beginning of a construction program designed to meet the needs of a growing region. The vision of the New York financiers who invested in this area is still alive today. Hoping to develop a financially feasible venture, they referred to this region as �Little Empire� to denote their pride in New York, the Empire State. To this day, the name Empire represents the history of mining development in this area.
    <br /><br />
    At the time of its organization, Empire had 109 miles of transmission line, 8 megawatts of generating capacity, and 2,400 customers. Today, the Company utilizes about 1,300 miles of transmission line and over 1,300 megawatts of capacity to serve over 167,000 customers.
    <br /><br />
    As industry grew and families moved into the four-state region, the demand for electrical power grew as well. Power for Empire was first produced from generation at the Lowell and Redings Mill hydroelectric plants. Soon, however, the demand for more generation became evident.
    <br /><br />
    In the summer of 1909, work began on the Riverton Generating Station. Nine months later, it was complete. One of the generators at the plant was dubbed �Old Kate.� Kate was famous for supplying power to the St. Louis World�s Fair in 1904. Today, the Riverton Plant is still in operation, generating 286 megawatts of electricity. During the historic floods of 1993, overflow from Spring River flooded the plant. After this event, measures were enacted to minimize damage should future flooding occur. In 2007, Riverton greatly increased its capacity with the installation of Riverton Unit 12. This natural gas-fueled turbine is highly efficient and produces low emissions. With its six turbines, Riverton is one of the oldest operational power plants in the United States.
    <br /><br />
    Construction of the Ozark Beach Dam began in 1911 in Taney County, Missouri. Two years later, the 1,300 foot long dam was complete. The dam forms Lake Taneycomo, a crystal clear lake stretching 22 miles throughout the picturesque Ozarks. Today, Ozark Beach Hydroelectric Plant supplies Empire with 16 megawatts of power and the Taney County area with a beautiful recreational area. Empire Park, located just above the dam, is a park maintained by Empire District. In 1999, the park was honored with the annual Advisory Council on Disabilities Community Award for the refurbishment and addition of handicap accessible facilities.
    <br /><br />
    From 1910 to its independence from Cities Services, Empire had purchased 27 separate power companies to become the dominate provider of electric service in the region. In 1944, The Empire District Electric Company separated from its parent company. Empire�s common stock was listed on the New York Stock Exchange under the symbol EDE in 1946.
    <br /><br />
    From 1959 to 1966, Empire spent approximately $25 million on construction, mostly on new transmission and distribution facilities, to handle the increased demand for electric energy. This construction program included new high-voltage transmission lines and new substations. It also included new transmission lines to tie Empire�s system with neighboring electric companies, making Empire an important part of a vast power grid embracing the Midwest.
    <br /><br />
    In the early 1960�s, the need for more automation in Empire�s operations was apparent. This realization brought about the construction of an operations and communications center in Joplin. System Operations was completed in 1964. Equipped with the latest design in electronic technology, this nerve center monitors and controls the production and power flow throughout the entire Empire system.
    <br /><br />
    By the mid-60�s, the energy needs of this growing and diverse economic region would once again exceed the capacity of existing generation facilities. On September 10, 1967, just north of Asbury, Missouri, ground was broken for a 200-megawatt, coal-fired power plant. The $26 million Asbury Generating Station was put into operation in June 1970.
    <br /><br />
    Asbury was designed as a �mine-mouth� plant to burn coal from the Empire Mine located just north of the plant. Total plant capacity is approximately 210 megawatts. Unit One is rated at 193 megawatts. Unit Two, completed in 1986, generates 17 megawatts of energy from the excess boiler capacity of Unit One. In 1990, the plant was converted to use a blend of low-sulfur Wyoming coal and native coal to comply with new clean air standards. Further environmental upgrades were instituted in 2007 with the investment in a $37 million selective catalytic reduction (SCR) system to the plant to further reduce nitrogen oxide (NOX) emissions.
    <br /><br />
    As the demand for energy has grown, Empire has met the need with additional building projects and purchase power agreements. In 1978, Empire added the first of two 90-megawatt combustion turbine peaking units at the Empire Energy Center near LaRussell, Missouri, approximately 20 miles east of Joplin. An opportunity to purchase 12 percent, or 80 megawatts, of the 650-megawatt Iatan Power Plant, near Kansas City, allowed Empire to delay the addition of a second 90-megawatt unit at the Empire Energy Center until 1981. Originally these peaking units utilized only fuel oil for combustion, but were converted to natural gas use in 1994. In April 2003, two 50-megawatt gas-fired units were added at the Energy Center.
    <br /><br />
    In 1993, Empire announced the addition of the State Line Power Plant located west of Joplin. A 98-megawatt combustion turbine began providing energy to Empire�s customers in May 1995. Two years later, a second 150-megawatt turbine was added to State Line. Plans were announced in 1998 for construction of an additional 350 megawatts of power generation at the plant. A new 150-megawatt combustion turbine, combined with the existing 150-megawatt unit, generate enough waste heat to produce an additional 200 megawatts of steam-powered, combined-cycle energy. The combined cycle unit began providing energy to Empire customers in June 2001. Westar Generating, Inc., a wholly owned subsidiary of Western Resources, is a partner in the State Line combined cycle. Empire owns 60 percent and serves as the plant operator. Total plant capacity is approximately 596 megawatts, making it Empire�s highest megawatt output plant. All three combustion turbines at State Line Power Plant use natural gas.
    <br /><br />
    By 2004 Empire was searching for ways to invest in renewable energy for customers. In December 2004, Empire announced a 20-year contract with PPM Energy (now Iberdrola) to receive all energy generated at the 150-megawatt Elk River Windfarm, located in Butler County, Kansas. The first energy was received from Elk River on October 17, 2005.
    <br /><br />
    The success of the agreement with PPM Energy led Empire to sign a second purchase power agreement with Horizon Wind Energy. Ground was broken on the Meridian Way Wind Farm in April 2008. Meridian Way is a 201-megawatt wind farm located eight miles south of Concordia, Kansas. Empire�s contract includes the energy generated by the 105-megawatt portion of the Meridian Way Wind Farm. Westar Energy purchases the energy from the additional 96-megawatt portion of this project that was completed in late 2008.
    <br /><br />
    To increase its baseload generation, in 2006, Empire announced two new construction projects for coal-fired plants. Empire owns 12 percent, or approximately 100 megawatts, of Iatan II which is located with Iatan I, near Kansas City. Plum Point Energy Station is located near Osceola, Arkansas. Empire is a part owner of the plant with 50 megawatts and also has a purchased power agreement for an additional 50 megawatts of energy produced by the plant. Both plants are highly-efficient and utilize the latest in environmental technology. They were completed in 2010.
    <br /><br />
    Empire also provides both water and natural gas service to customers in Missouri. Empire Water Company was created in 1926 when Empire purchased the Lawrence County Water, Light, and Cold Storage Company. The company served the electric and water needs of Aurora, Missouri. Currently the water company meets the water needs of approximately 4,500 customers in Aurora, Marionville, and Verona, Missouri.
    <br /><br />
    In 2006, Empire acquired natural gas distribution rights from Aquila, Inc. This acquisition created The Empire District Gas Company, a wholly owned subsidiary of The Empire District Electric Company. Empire District Gas serves the natural gas needs of approximately 44,000 customers in 45 communities in northwest, north central, and west central Missouri.
    <br /><br />
    As Empire begins its second century of service, the dedication and professionalism of its employees has allowed Empire to fulfill its mission of providing safe, reliable, and low-cost energy to homes and industry alike. From its early beginnings, Empire has strived to be a good corporate citizen while providing a fair return for its owners, the stockholders. The Company has utilized technical improvements to produce and deliver energy more efficiently while maintaining the quality of the environment for future generations.
    <br /><br />
    From the early mining camps to the diverse array of industry found across the four-state region today, the "Empire District" is alive and thriving due to the leadership, courage, and spirit of The Empire District Electric Company.
    <br /><br />
    
    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select id,title,description from images where type = 'commercial' and (title like '%hist%' or title like '%anniversary%' or title like '%celebrat%' or title like '%century%') order by date desc">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSOnlineBook" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select documents.webdescription AS docdescription, documents.id from documents inner join doctype on documents.doctypeid = doctype.id inner join groups on documents.groupid = groups.id where documents.groupid = 14 and documents.doctypeid = 62 order by docdescription">
    </asp:SqlDataSource>    
</asp:Content>