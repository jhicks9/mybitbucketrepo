<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ContactUs.aspx.vb" Inherits="About_ContactUs" title="Contact Us" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:MultiView ID="MultiView1" ActiveViewIndex="0" runat="server">
        <asp:View ID="vwContacts" runat="server">
             <div style='float:right;margin:0 0 10px 10px'>
                <asp:Image ID="imgAboutUs" Imageurl="~/images/landing/landing-contactus.jpg" runat="server" />
            </div>
            <div class="landingtext">
                <asp:LinkButton id="lbCS" runat="server" OnClick="prepareEmail" Text="Customer Service" /><br />
                Contact regarding bills, payments, assistance agencies, service, or customer inquiries. To report an outage, please select the Report Outage contact information.
                <br /><br />
                <asp:LinkButton id="lbOR" runat="server"  PostBackUrl="~/Outages/OutageReport.aspx" Text="Report Outage" /><br />
                Please contact if you are experiencing a service interruption.
                <br /><br />
                <asp:LinkButton id="lbCC" runat="server" OnClick="prepareEmail" Text="Corporate Communications" /><br />
                Send a message for questions about media stories or news releases.
                <br /><br />
                <asp:LinkButton id="lbIR" runat="server" OnClick="prepareEmail" Text="Investor Relations" /><br />
                Please email for questions regarding stock or investing in Empire.
                <br /><br />
                <asp:LinkButton id="lbEMP" runat="server" OnClick="prepareEmail" Text="Employment" /><br />
                Contact if interested in following up on an application, keeping your application file active, or for general human resources questions.
                <br /><br />
                <asp:LinkButton id="lbED" runat="server" OnClick="prepareEmail" Text="Economic Development" /><br />
                Email if interested in learning about or exploring possible economic development opportunities with Empire.
                <br /><br />
                <asp:LinkButton id="lbVM" runat="server" OnClick="prepareEmail" Text="Vegetation Management" /><br />
                Send a message regarding trimming of branches and brush around power lines and system maintenance.
                <br /><br />
                <asp:LinkButton id="lbCCR" runat="server" OnClick="prepareEmail" Text="Environmental/CCR" /><br />
                For comments or questions related to CCR Rule Compliance Data and Information.
                <br /><br />
            </div>
        </asp:View>

        <asp:View ID="vwEmail" runat="server">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>To:</td>
                    <td><asp:label ID="lblEmailTo" runat="server" Text="" /></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" Width="300px" ValidationGroup="avg"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" ValidationGroup="avg" Display="Static" />
                    </td>
                </tr>
                <tr id="trAcctNumber" runat="server" visible="false">
                    <td>Account Number:</td>
                    <td>
                        <asp:TextBox ID="txtAcctNumber" runat="server" Width="300px" ValidationGroup="avg" MaxLength="9"></asp:TextBox><asp:Label ID="lblAccountNumber" runat="server"><sub>(First 6 digits only)</sub></asp:Label>
                        <asp:RequiredFieldValidator ID="rfvAcctNumber" runat="server" ControlToValidate="txtAcctNumber" ErrorMessage="Account Number is required" ValidationGroup="avg" Display="Static" />
                    </td>
                </tr>
                <tr>
                    <td>Email address:</td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="300px" ValidationGroup="avg"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required" ValidationGroup="avg" Display="Static" />
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="Static" ErrorMessage="Valid Email required" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="avg">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>Subject:</td>
                    <td>
                        <asp:TextBox ID="txtSubject" runat="server" Width="300px" ValidationGroup="avg"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ControlToValidate="txtSubject" ErrorMessage="Subject required" ValidationGroup="avg" Display="Static" />
                    </td>
                </tr>
                <tr>
                    <td>Message:</td>
                    <td>
                        <asp:TextBox ID="txtMessage" runat="server" ValidationGroup="avg" TextMode="MultiLine" Height="175px" Width="450px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvMessage" runat="server" ControlToValidate="txtMessage" ErrorMessage="Message required" ValidationGroup="avg" Display="Static" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSubmit_Click" ><span>Submit</span></asp:LinkButton>
                        <div style="float:left;width:6px;">&nbsp;</div>
                        <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" OnClick="lbCancel_Click"><span>Cancel</span></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </asp:View>
        
        <asp:View ID="vwEmailSent" runat="server">
            <asp:label ID="lblStatus" Font-Bold="True" ForeColor="Red" Text="" runat="server"></asp:label>
        </asp:View>
    </asp:MultiView>

    <hr style="color:#113962;height:1px;"/>
    <br />
    The Empire District Electric Company<br />
    602 Joplin Avenue<br />
    P.O. Box 127<br />
    Joplin, Missouri  64802
    <br /><br />
    <table cellspacing="2" cellpadding="0">
        <tr>
            <td>Electric & Water Service:</td>
            <td><div style="width:20px;"></div></td>
            <td>Gas Service:</td>
            <td><div style="width:20px;"></div></td>
            <td>Report a Gas Leak:</td>
            <td><div style="width:20px;"></div></td>
            <td>Corporate Office:(for vendors)</td>
        </tr>
        <tr>
            <td><img id="img5" src="~/images/navigation/separator-blue.png" runat="server" alt="s1"/>&nbsp;800-206-2300</td>
            <td><div style="width:20px;"></div></td>
            <td><img id="img6" src="~/images/navigation/separator-blue.png" runat="server" alt="s1"/>&nbsp;800-424-0427</td>
            <td><div style="width:20px;"></div></td>
            <td><img id="img7" src="~/images/navigation/separator-blue.png" runat="server" alt="s1"/>&nbsp;800-406-9220</td>
            <td><div style="width:20px;"></div></td>
            <td><img id="img8" src="~/images/navigation/separator-blue.png" runat="server" alt="s1"/>&nbsp;417-625-5100</td>
        </tr>
    </table>
</asp:Content>

