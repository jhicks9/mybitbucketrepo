﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ResourcePlan.aspx.vb" Inherits="About_ResourcePlan" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <hr style="color:#113962;height:1px;"/>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                In the utility business, planning for the future is one of our most crucial tasks.  On April 1, 2016, we filed our 2016 Integrated Resource Plan (IRP) in Missouri.  The IRP has a 20-year planning horizon and sets forth our preferred resource plan, contingency plans, and other plans required by the Commission’s IRP rule.  During the planning process, we considered demand-side, supply-side, and renewable resource options.  Minimizing costs, risks, and rate impacts while maintaining reliability, resource diversity, and meeting legal mandates and energy policy were key considerations.
                <br /><br />
                We are required to file a Plan with the Missouri Public Service Commission every three years. The IRP will also be submitted to the other states served by Empire that have IRP requirements.
                <br /><br />
                <asp:DataList ID="dlRESP" runat="server" SkinID="DataList" DataSourceID="sqlDS">
                    <itemtemplate>
                        <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                        <asp:HyperLink ID="HyperLink1" Text='<%#Eval("Description") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/>
                    </itemtemplate>
                </asp:DataList>
            </td>
            <td valign="top">
                 <div style='margin:0 0 10px 10px'><asp:Image ID="imgAboutUs" Imageurl="~/images/aboutus-resourceplan.jpg" runat="server" /></div>
            </td>
        </tr>
    </table>

    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select top 1 id,description from documents where groupid = 14 and doctypeid = 133 and (webdescription like '%resource plan' or webdescription like 'resource plan%' or webdescription like '%resource plan%')">
    </asp:SqlDataSource>
</asp:Content>