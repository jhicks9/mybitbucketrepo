<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="FastFacts.aspx.vb" Inherits="FastFacts" title="Fast Facts" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <dl>
        <dt><strong>Who we are, what we do, and where we are located -</strong></dt>
        <dd>Founded in October 1909 as a part of Cities Services Company, The Empire District Electric Company is an investor-owned, regulated utility company, based in Joplin, Missouri, that provides electric, natural gas (through its wholly owned subsidiary, The Empire District Gas Company), and water service, with approximately 218,000 customers in Missouri, Kansas, Oklahoma, and Arkansas.  A subsidiary of the Company also provides fiber optic services.  Empire has been listed on the New York Stock Exchange under EDE since 1946.</dd>
    </dl>
    <br /><hr style="color:#113962;height:1px;"/><br />
    <dl>
        <dt><strong>Some general information you might like to know (12/31/2015) -</strong></dt>
        <dd>
            <table cellpadding="3" cellspacing="0">
                <tr>
                    <td>Population of Service Area</td>
                    <td>Over 450,000</td>
                </tr>
                <tr>
                    <td>Cities and Towns Served/Electric</td>
                    <td>119</td> 
                </tr>
                <tr>
                    <td>Cities and Towns Served/Gas</td>
                    <td>48</td> 
                </tr>
                <tr>
                    <td>Electric Customers</td>
                    <td>170,158</td>
                </tr>
                <tr>
                    <td>Gas Customers</td>
                    <td>43,639</td>
                </tr>
                <tr>
                    <td>Average Yearly Residential Usage (KWH)</td>
                    <td>12,881</td>
                </tr>
                <tr>
                    <td>Average Residential Price per KWH</td>
                    <td>$0.1256</td>
                </tr>
                <tr>
                    <td>Average Commercial Price per KWH</td>
                    <td>$0.1088</td>
                </tr>
                <tr>
                    <td>Average Industrial Price per KWH</td>
                    <td>$0.0828</td>
                </tr>
                <tr>
                    <td>Employees</td>
                    <td>749</td>
                </tr>
                <tr>
                    <td>Owned Capability</td>
                    <td>1,280 MW</td>
                </tr>
                <tr>
                    <td>Purchased Capacity</td>
                    <td>86 MW</td>
                </tr>
                <tr>
                    <td>On-System Revenues (000)</td>
                    <td>$565,934</td>
                </tr>
                <tr>
                    <td>Operating Income (000)</td>
                    <td>$96,301</td>
                </tr>
                <tr>
                    <td>Net Income (000)</td>
                    <td>$56,597</td>
                </tr>
                <tr>
                    <td>Earnings per Weighted Average Common Share(Basic)</td>
                    <td>$1.30</td>
                </tr>
                <tr>
                    <td>Dividends Paid</td>
                    <td>$1.04</td>
                </tr>
                <tr>
                    <td>Gross Plant (000)</td>
                    <td>$2,601,592</td>
                </tr>
                <tr>
                    <td>On-System Electric Sales (mWh)</td>
                    <td>4,935,725</td>
                </tr>
                <tr>
                    <td>On-System Gas Sales (000) (Mcf)</td>
                    <td>7,783</td>
                </tr> 
            </table>
        </dd>
    </dl>
    <br /><hr style="color:#113962;height:1px;"/><br />
    <dl>
        <dt></dt>
        <dd>
            <div id="gallery">
            <table cellpadding="3" cellspacing="3">
               <tr style="color:Silver;background-color:#113962">
                   <td>Power Plant</td>
                   <td>Power Plant Rating</td>
                   <td>Percent of Capacity</td>
               </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlAsbury" runat="server" Text="Asbury" NavigateUrl="~/images/maps/Asbury.jpg" /></td>
                    <td>198 MW</td>
                    <td>14.63%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlRiverton" runat="server" Text="Riverton" NavigateUrl="~/images/maps/Riverton.jpg" /></td>
                    <td>177 MW*</td>
                    <td>17.04%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlEnergyCenter" runat="server" Text="Energy Center" NavigateUrl="~/images/maps/EnergyCenter.jpg" /></td>
                    <td>257 MW</td>
                    <td>19.61%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlOzarkBeach" runat="server" Text="Ozark Beach" NavigateUrl="~/images/maps/OzarkBeach.jpg" /></td>
                    <td>16 MW</td>
                    <td>1.21%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlStateLine" runat="server" Text="State Line Combined Cycle" NavigateUrl="~/images/maps/StateLine.jpg" /> (our 60% ownership)</td>
                    <td>295 MW</td>
                    <td>22.40%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlStateLine1" runat="server" Text="State Line Unit #1" NavigateUrl="~/images/maps/StateLine.jpg" /></td>
                    <td>96 MW</td>
                    <td>7.01%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlIatan" runat="server" Text="Iatan" NavigateUrl="~/images/maps/Iatan.jpg" /> (our 12% ownership)</td>
                    <td>191 MW</td>
                    <td>14.33%</td>
                </tr>
                <tr>
                    <td><asp:Hyperlink ID="hlPlumPoint" runat="server" Text="Plum Point" NavigateUrl="~/images/maps/PlumPoint.jpg" /> (our 7.5% ownership)</td>
                    <td>50 MW</td>
                    <td>3.77%</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>1,280 MW</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />* Reflects the retirement of 54MW Unit 8. A combined cycle conversion of Unit 12 will add approximately 107MW by mid-2016.
                    </td>
                </tr>
            </table>
            </div>
        </dd>
    </dl>

    <br /><hr style="color:#113962;height:1px;"/><br />
    <dl>
        <dt><strong>Our highest demand -</strong></dt>
        <dd>The Company's electric all-time summer peak load is 1,198 MW and occurred on August 2, 2011. The Company's electric all-time winter peak of 1,199 MW was set on January 8, 2010. The Company's gas maximum daily flow was a volume of 73,280 Mcf on January 7, 2010.</dd>
    </dl>

    <script type="text/javascript">jQuery("#gallery a").slimbox();</script>
</asp:Content>

