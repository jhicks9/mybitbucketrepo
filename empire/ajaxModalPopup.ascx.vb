﻿
Partial Class ajaxModalPopup
    Inherits System.Web.UI.UserControl

    Private _show As Boolean = False

    Public Property Show() As Boolean
        Get
            Return _show
        End Get
        Set(ByVal value As Boolean)
            _show = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dynamically add javascript
        'Dim simplemodal As HtmlGenericControl = New HtmlGenericControl
        'simplemodal.Attributes.Add("type", "text/javascript")
        'simplemodal.TagName = "script"
        'simplemodal.Attributes.Add("src", ResolveUrl("~/js/jquery.simplemodal.1.4.min.js"))
        'Page.Header.Controls.Add(simplemodal)

        ' Dynamically add stylesheet
        'Dim css As HtmlLink = New HtmlLink
        'css.Href = ResolveUrl("~/slimbox2.css")
        'css.Attributes("rel") = "stylesheet"
        'css.Attributes("type") = "text/css"
        'css.Attributes("media") = "all"
        'Page.Header.Controls.Add(css)
        'System.Threading.Thread.Sleep(5000)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _show Then
            mpeProgress.Show()
        End If
    End Sub
End Class
