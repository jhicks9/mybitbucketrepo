﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="header.ascx.vb" Inherits="header" %>

    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td rowspan="2" align="left" style="width:130px;padding-right:10px;padding-left:10px;">
                <asp:Panel ID="pnlLogo" runat="server" Visible="true">
                    <img id="imgLogo" runat="server" alt="Company Logo" src="~/images/navigation/empire-logo.png"/>
                </asp:Panel>
            </td>
            <td colspan="2" align="right">
                <asp:LoginView ID="LoginView1" runat="server">
                    <AnonymousTemplate>
                        <div style="float:right;padding:5px;">
                            <a id="hlLogin" href="<%= ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx") %>" >Log In</a>
                            <img id="imgsep1" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                            <a href="<%= ResolveSSLUrl("~/question.aspx") %>">Register</a>
                            <div style="float:left;width:4px">&nbsp;</div>
                            <img id="imgsep2" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                            <a href="<%= ResolveSSLUrl("~/RetrievePassword.aspx") %>">Forgot Password</a>
                            <img id="imgsep3" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                            <a href="<%= ResolveSSLUrl("~/RetrievePassword.aspx?id=name") %>">Forgot User Name</a>
                        </div>                        
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <div class="logout">
                            <asp:LoginName ID="LoginName1" runat="server" />
                            <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutPageUrl="~/Default.aspx" LogoutAction="Redirect" OnLoggingOut="LoginStatus1_LoggingOut" onclick="onInvoke()" />
                        </div>
                    </LoggedInTemplate>
                </asp:LoginView>  
            </td>
        </tr>
        <tr>
            <td valign="bottom" align="left" style="white-space:nowrap">
                <ul id='fadeMenu' style='width:100%'>
                    <asp:Panel ID="pnlFadeMenu" runat="server" Visible="true">
                        <asp:Literal ID="litHeaderMenu" runat="server" Text="" Visible="true" />
                    </asp:Panel>
                </ul>
            </td>
            <td valign="bottom" align="right"></td>
        </tr>
    </table>
    <asp:Label ID="lblTest" runat="server" Text="" ForeColor="White" />
        
<script type="text/javascript">
    $(document).ready(function () {
        //Append a div with hover class to all the LI
        if (!("ontouchstart" in document.documentElement)) {
            $('#fadeMenu li').append('<div class="hover"><\/div>');
        }
        $('#fadeMenu li').hover(
        //Mouseover, fadeIn the hidden hover class
		function () {
		    $(this).children('div').stop(true, true).fadeIn('1000');
		},
        //Mouseout, fadeOut the hover class
		function () {
		    $(this).children('div').stop(true, true).fadeOut('1000');
		});
    });
</script>
