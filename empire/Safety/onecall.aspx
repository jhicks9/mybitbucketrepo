<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="onecall.aspx.vb" Inherits="Safety_onecall" title="Utility Location Services" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="#113962">
        <tr>
            <td valign="top">
                <div style="padding:3px;background-color:#113962;color:#fff;text-align:center"><b>Nationwide</b></div>
                <div style="padding:10px">
                    <asp:Image ID="imgLogin" runat="server" ImageUrl="~/images/safety-811.gif" />
                </div>
            </td>
            <td valign="top">
                <div style="padding:3px;background-color:#113962;color:#fff;text-align:center"><b>Local</b></div>
                <div style="padding:10px">
                    <table cellpadding="0" cellspacing="0">
                        <tr><td><asp:Image ID="Image1" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /> </td><td class="links"><a href="http://www.arkonecall.com/" target="_blank" class="sectionheader">Arkansas</a></td></tr>
                        <tr><td colspan="2">800-482-8998</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td><asp:Image ID="Image2" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td><td class="links"><a href="http://www.kansasonecall.com/" target="_blank" class="sectionheader">Kansas</a></td></tr>
                        <tr><td colspan="2">800-DIG-SAFE (800-344-7233)</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td><asp:Image ID="Image3" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td><td class="links"><a href="http://www.mo1call.com/" target="_blank" class="sectionheader">Missouri</a></td></tr>
                        <tr><td colspan="2">800-DIG-RITE (800-344-7483)</td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr><td><asp:Image ID="Image4" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td><td class="links"><a href="http://www.callokie.com/" target="_blank" class="sectionheader">Oklahoma</a></td></tr>
                        <tr><td colspan="2">800-522-OKIE (800-522-6543)</td></tr>
                    </table>
                </div>
            </td>
            <td valign="top" align="center" style="background-color:#b0c4de;">
                <div style="padding:3px;background-color:#113962;color:#fff;text-align:center"><b>Utility Color Codes</b></div>
                <div style="padding:10px">
                <table cellpadding='3' cellspacing='0' border="1" bordercolor="#113962" style="background-color:#f5f5f5;">
                    <tr><td>Electric</td><td align="center" style="background-color:red">red</td></tr>
                    <tr><td>Gas, Oil, Steam</td><td align="center" style="background-color:yellow">yellow</td></tr>
                    <tr><td>Communications, Cable TV</td><td align="center" style="background-color:orange">orange</td></tr>
                    <tr><td>Water</td><td align="center" style="background-color:#1e90ff">blue</td></tr>
                    <tr><td>Sewer</td><td align="center" style="background-color:#32cd32">green</td></tr>
                    <tr><td>temporary survey markings</td><td align="center" style="background-color:pink">pink</td></tr>
                    <tr><td>proposed excavation</td><td align="center" style="background-color:white">white</td></tr>
                    <tr><td>reclaimed water</td><td align="center" style="background-color:purple">purple</td></tr>             
                </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>