<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ElectricSafety.aspx.vb" Inherits="Safety_ElectricSafety" title="Electric Safety" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <b>Electrical Safety</b>
    <asp:GridView ID="gvDoc" runat="server" SkinID="GridViewList" DataSourceID="sqlDocDS">
        <Columns>
            <asp:TemplateField HeaderText="Electrical Safety Documents" SortExpression="description">
                <ItemTemplate>
                    <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" AlternateText="" />&nbsp;
                    <asp:HyperLink ID="hlDocument" runat="server" Target="_blank" Text='<%# Eval("description")%>' NavigateUrl='<%# Eval("id", "~/DocHandler.ashx?id={0}")%>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource ID="sqlDocDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT description, id FROM documents WHERE groupid = 14 AND doctypeid = 135" />
</asp:Content>