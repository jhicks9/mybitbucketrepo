<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Education.aspx.vb" Inherits="Safety_Education" title="Safety Education" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td valign="middle" align="center" >
                <a href="http://empire.electricuniverse.com/" target="_blank"><asp:Image ID="imgEU" runat="server" ImageUrl="~/images/safety-edu-eu.png"/></a>
            </td>
            <td><div style="width:5px">&nbsp;</div></td>
            <td valign="middle" align="center">
                   <a href="http://www.empiredistrictelectricngsw.com/" target="_blank"><asp:Image ID="imgNGSW" runat="server" ImageUrl="~/images/safety-edu-gas.png"/></a>
            </td>
            <td><div style="width:5px">&nbsp;</div></td>
            <td valign="middle" align="center">
                <a href="http://c03.apogee.net/contentplayer/?utilityid=empiredistrict&coursetype=kids" target="_blank"><asp:Image ID="imgKK" runat="server" ImageUrl="~/images/safety-edu-kk.png"/></a>
            </td>
        </tr>
    </table>
    <div style="padding-top:25px;float:right;">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/safety-louie.gif" />
    </div>
</asp:Content>