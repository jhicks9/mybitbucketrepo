﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MakePayment.ascx.vb" Inherits="MakePayment" %>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td valign="middle" align="center">
            <img id="imgPaymentBanner" src="~/images/customerservice-makepayment.png" runat="server" alt=""
                onmouseover="TagToTip('Num1',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" 
                onmouseout="UnTip();"/>
        </td>
        <td valign="middle" align="center">&nbsp;</td>
        <td valign="middle" align="center">
            <a href="https://paynow40.speedpay.com/EmpireDistrictWeb/login.aspx" target="_blank" runat="server"
                onclick="javascript:if(!confirm('This action will direct the browser to a third party payment processing vendor.  \r\rYou will be leaving Empire District\'s website.')){return false;}">
                Make an online payment
            </a>
        </td>
    </tr>
</table>

<div id="Num1" style="display:none;">
    <div style="width:250px">
    <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Online Payment</b></div>
    Empire District offers an online payment service.  This service can be used to pay by credit card or electronic funds transfer from a checking or savings account.
    There is a convenience fee for using this online payment service.
    </div>
</div>
