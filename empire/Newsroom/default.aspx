﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="newsroom_default" Title="Newsroom"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td rowspan="2" valign="top" style="width:80%">
                        <asp:SiteMapPath ID="SiteMapPath" runat="server">
                            <RootNodeTemplate>
                                <asp:Image ID="imgHome" ImageAlign="Top" ImageUrl="~/images/icon/icon_home.gif" runat="server" />&nbsp;
                                <asp:hyperlink ID="hlHome" runat="server" Text='<%# Eval("title") %>' NavigateUrl='<%# Eval("url") %>' />
                            </RootNodeTemplate>
                            <CurrentNodeTemplate>
                                <asp:Label ID="lblCurrent" runat="server" Font-Bold="true" Text='<%# Eval("title") %>'></asp:Label>
                                <div style="padding-bottom:12px"></div>
                            </CurrentNodeTemplate>
                        </asp:SiteMapPath>
                    
                        <acrobat:uc4 ID="uc4" runat="server" />
                        <div style="padding-right:5px;">

                            <asp:LinkButton ID="lbNewsReleases" runat="server" CausesValidation="true" PostBackUrl="~/Newsroom/default.aspx" Text="News Releases"/>&nbsp&nbsp|&nbsp
                            <asp:LinkButton ID="lbArchNewsReleases" runat="server" CausesValidation="true" PostBackUrl="~/Newsroom/default.aspx?archive=y" Text="Archived News Releases"/>
                            <br />&nbsp

                            <asp:Panel ID="pnlNewsReleases" runat="server" HeaderText="News Releases">
                                <ContentTemplate>
                                    <asp:GridView ID="gvNewsReleases" runat="server" DataSourceID="sqlDSNewsReleases" SkinID="gridviewlist">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                        <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp; 
                                                        <asp:HyperLink ID="hlDocument" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" ForeColor="#113962" runat="server"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:Panel><%--end pnlDocuments--%>
                            
                        
                        </div>
                    </td>
                    
                    <td valign="top" style="width:25%;background-color:#c0c0c0">
                        <div style="padding: 10px 5px 10px 5px;background-color:#b0c4de;text-align:center;">
                            <asp:HyperLink ID="hlMedia" Text='Media Kit' NavigateUrl='~/newsroom/media.aspx' runat="server"/>
                        </div>
                        <div style="padding:5px;background-color:#c0c0c0;text-align:center;">
                            <b>Julie Maus</b><br />
                            <div style="color:blue;">Director of Corporate Communications</div>
                            417-625-5101<br />
                            <a href="~/About/ContactUs.aspx?type=cc" runat="server">jmaus@empiredistrict.com</a><br /><br /><br />
                      
                            <b>Robin McAlester</b><br />
                            <div style="color:blue;">Communications<br />Specialist</div>
                            417-625-5180<br />
                            <a href="~/About/ContactUs.aspx?type=cc" runat="server">rmcalester@empiredistrict.com</a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" style="background-color:#c0c0c0;width:25%;padding:20px 5px 5px 5px;text-align:center;">
                        <div class='heading' >
                            EDE:&nbsp;<asp:Label ID="lblQuote" runat="server" Text=""></asp:Label>
                        </div>
                        <asp:Label ID="lblChange" runat="server"/><br />
                        <asp:Label ID="lblDateTime" runat="server"/>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
                    
    <asp:SqlDataSource ID="sqlDSNewsReleases" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>">
        </asp:SqlDataSource>
    
<script type="text/javascript">
    function ActiveTabChanged(sender, e) {
        __doPostBack(sender.get_id(), sender.get_activeTabIndex());
    } 
</script>
</asp:Content>

