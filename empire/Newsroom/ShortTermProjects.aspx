﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ShortTermProjects.aspx.vb" Inherits="Newsroom_ShortTermProjects" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="padding-bottom:12px">
        <tr>
            <td valign="middle" align="center">&nbsp;<asp:Image ID="imgHome" ImageAlign="Top" ImageUrl="~/images/icon/icon_home.gif" runat="server" />&nbsp;</td>
            <td valign="middle" align="center">&nbsp;<asp:hyperlink ID="hlHome" runat="server" Text='Home' NavigateUrl="~/default.aspx" />&nbsp;>&nbsp;</td>
            <td valign="middle" align="center"><asp:Label ID="lblCurrentNavigation" runat="server" Font-Bold="true" Text="" /></td>
        </tr>
    </table>                
    <acrobat:uc4 ID="uc4" runat="server" />
    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="" /><br /><br />
    <asp:DataList ID="dlShortTermProjects" runat="server" SkinID="DataList" DataSourceID="sqlSTP" Visible="true">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                        NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' 
                        Target="_blank" runat="server"/> 
        </itemtemplate>
    </asp:DataList>
    <br />
    If you have any additional questions or comments, please <a href="~/About/ContactUs.aspx?type=cc" runat="server">email</a>.

    <asp:SqlDataSource ID="sqlSTP" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 130 ORDER BY documents.webdescription">
    </asp:SqlDataSource>
</asp:Content>