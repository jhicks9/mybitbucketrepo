﻿
Partial Class newsroom_media
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'ClientScript.RegisterClientScriptInclude("litebox", ResolveUrl("~/js/litebox-1.0.js"))
        Dim jquery As HtmlGenericControl = New HtmlGenericControl
        jquery.Attributes.Add("type", "text/javascript")
        jquery.TagName = "script"
        jquery.Attributes.Add("src", ResolveUrl("~/js/jquery-1.3.2.min.js"))
        Page.Header.Controls.Add(jquery)
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub TabMainContainer_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabMainContainer.ActiveTabChanged
        Select Case TabMainContainer.ActiveTabIndex
        End Select
    End Sub
End Class
