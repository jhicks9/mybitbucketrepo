﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="media.aspx.vb" Inherits="newsroom_media" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style='float:right;margin-left:5px'>
                <img id="imgMediaKit" src="~/images/landing/landing-mediakit.jpg" runat="server" alt=""/>
            </div>
            <div class="landingtext">
                <asp:HyperLink ID="hlNews" runat="server" NavigateUrl="~/newsroom/default.aspx">News Releases</asp:HyperLink><br />
                Press releases issued by Empire District. These documents are in pdf format and require Adobe Acrobat Reader software.<br /><br />
                <asp:HyperLink ID="hlLeadership" runat="server" NavigateUrl="~/About/Leadership.aspx">Leadership</asp:HyperLink><br />
                A brief overview of Empire District Electric's Officers and Board of Directors.<br /><br />
                <asp:HyperLink ID="hlFastFacts" runat="server" NavigateUrl="~/About/FastFacts.aspx">Fast Facts</asp:HyperLink><br />
                Interesting facts about Empire District.<br /><br />
                <asp:HyperLink ID="hlServiceMap" runat="server" NavigateUrl="~/About/ServiceMap.aspx">Service Maps</asp:HyperLink><br />
                This detailed map shows what areas of coverage we have for our utilities.<br /><br />
                <asp:HyperLink ID="hlOutage" runat="server" NavigateUrl="~/Outages/default.aspx">Outage Information</asp:HyperLink><br />
                View a map of Empire District's electric service area and current outage information.<br /><br />
                <asp:HyperLink ID="hlEnergy" runat="server" NavigateUrl="~/EnergySolutions/default.aspx">Energy Efficiency</asp:HyperLink><br />
                Various programs designed to make efficient use of energy and keep customer costs down.<br /><br />
                <asp:HyperLink ID="hlVegetation" runat="server" NavigateUrl="~/Trees/VegetationFAQ.aspx">Vegetation Management Information </asp:HyperLink><br />
                Find answers to some frequently asked questions regarding Empire District Electric's vegetation management.<br /><br />
                <asp:HyperLink ID="hlContacts" runat="server" NavigateUrl="~/About/ContactUs.aspx">Contact Us</asp:HyperLink><br />
                A list of email contacts for Empire District.<br /><br />
            </div>

            <div style="color:#113962;"><b>Photos and Logos</b></div>
            Utilize images and logos of Empire for publication.
            <br /><br />
            <cc1:TabContainer ID="TabMainContainer" runat="server" ActiveTabIndex="0" CssClass="customTabXP" OnActiveTabChanged="TabMainContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged">
                <cc1:TabPanel ID="tabElectric" runat="server" HeaderText="Electric">
                    <ContentTemplate>
                        <table>
                            <tr><td colspan="4"><b>Plants</b></td></tr>
                            <tr>
                                <td><a href="~/images/media/empiremedia-electric-plant1.jpg" runat="server" target="_blank"><img id="img10" src="~/images/media/empiremedia-electric-plant1-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-plant2.jpg" runat="server" target="_blank"><img id="img11" src="~/images/media/empiremedia-electric-plant2-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-plant3.jpg" runat="server" target="_blank"><img id="img12" src="~/images/media/empiremedia-electric-plant3-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-plant4.jpg" runat="server" target="_blank"><img id="img13" src="~/images/media/empiremedia-electric-plant4-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                            </tr>
                            <tr>
                                <td><a href="~/images/media/empiremedia-electric-plant5.jpg" runat="server" target="_blank"><img id="img14" src="~/images/media/empiremedia-electric-plant5-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-plant6.jpg" runat="server" target="_blank"><img id="img15" src="~/images/media/empiremedia-electric-plant6-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-plant7.jpg" runat="server" target="_blank"><img id="img16" src="~/images/media/empiremedia-electric-plant7-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-plant8.jpg" runat="server" target="_blank"><img id="img17" src="~/images/media/empiremedia-electric-plant8-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                            </tr>
                        </table>
                        <br /><br />
                        <table>
                            <tr><td colspan="4"><b>Employees</b></td></tr>
                            <tr>
                                <td><a href="~/images/media/empiremedia-electric-employee1.jpg" runat="server" target="_blank"><img id="img5" src="~/images/media/empiremedia-electric-employee1-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-employee2.jpg" runat="server" target="_blank"><img id="img6" src="~/images/media/empiremedia-electric-employee2-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-employee3.jpg" runat="server" target="_blank"><img id="img7" src="~/images/media/empiremedia-electric-employee3-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-electric-employee4.jpg" runat="server" target="_blank"><img id="img8" src="~/images/media/empiremedia-electric-employee4-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                            </tr>
                            <tr>
                                <td><a href="~/images/media/empiremedia-electric-employee5.jpg" runat="server" target="_blank"><img id="img9" src="~/images/media/empiremedia-electric-employee5-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td colspan="3"></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </cc1:TabPanel><%--end tabElectric--%>

                <cc1:TabPanel ID="tabGas" runat="server" HeaderText="Gas">
                    <ContentTemplate>
                        <table>
                            <tr><td colspan="2"><b>Employees</b></td></tr>
                            <tr>
                                <td><a href="~/images/media/empiremedia-gas-employee1.jpg" runat="server" target="_blank"><img id="imgGasEmployee1" src="~/images/media/empiremedia-gas-employee1-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-gas-employee2.jpg" runat="server" target="_blank"><img id="imgGasEmployee2" src="~/images/media/empiremedia-gas-employee2-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                                <td><a href="~/images/media/empiremedia-gas-employee3.jpg" runat="server" target="_blank"><img id="imgGasEmployee3" src="~/images/media/empiremedia-gas-employee3-thumbnail.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/></a></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </cc1:TabPanel><%--end tabGas--%>

            </cc1:TabContainer><%--end TabMainContainer--%>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            __doPostBack(sender.get_id(), sender.get_activeTabIndex());
        } 
    </script>
</asp:Content>

