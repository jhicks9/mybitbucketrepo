﻿
Partial Class newsroom_default
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim stockquote As String = cwservice.GetStockQuote("ede")
            lblQuote.Text = stockquote.Split(",")(1).Trim
            lblChange.Text = stockquote.Split(",")(4).Trim & " (" & removeQuotes(stockquote.Split(",")(11).Trim) & ")"
            If Not String.IsNullOrEmpty(lblChange.Text) Then
                If Left(lblChange.Text, 1) = "-" Then
                    lblQuote.ForeColor = Drawing.Color.Red
                Else
                    lblQuote.ForeColor = Drawing.Color.Green
                End If
            End If
            lblDateTime.Text = removeQuotes(stockquote.Split(",")(2).Trim) & " " & removeQuotes(stockquote.Split(",")(3).Trim) & " Eastern"

            Dim sm As SiteMapPath = Page.Master.FindControl("SiteMapPath")  'disable sitemap
            sm.Visible = False

            If String.IsNullOrEmpty(Request.QueryString("archive")) Then
                sqlDSNewsReleases.SelectCommand = "SELECT groups.description, documents.lastupdate, documents.filename, documents.filebytes, documents.doctypeid, documents.groupid, doctype.description AS documenttype, documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE (YEAR(lastupdate) = YEAR({ fn NOW() })) AND ((documents.groupid = 1 AND documents.doctypeid = 6) OR (documents.groupid = 1 AND documents.doctypeid = 54)) ORDER BY YEAR(documents.lastupdate) DESC, documents.webdescription DESC"
                gvNewsReleases.DataBind()
                lbNewsReleases.Font.Bold = True
                lbArchNewsReleases.Font.Bold = False
            ElseIf Request.QueryString.ToString = "archive=y" Then
                sqlDSNewsReleases.SelectCommand = "SELECT groups.description, documents.lastupdate, documents.filename, documents.filebytes, documents.doctypeid, documents.groupid, doctype.description AS documenttype, documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE ((YEAR(lastupdate) < YEAR({ fn NOW() })) AND (YEAR(lastupdate) > YEAR({ fn NOW() }) - 3)) AND ((documents.groupid = 1 AND documents.doctypeid = 6) OR (documents.groupid = 1 AND documents.doctypeid = 54)) ORDER BY YEAR(documents.lastupdate) DESC, documents.webdescription DESC"
                gvNewsReleases.DataBind()
                lbArchNewsReleases.Font.Bold = True
                lbNewsReleases.Font.Bold = False
            End If

        Catch
        End Try
    End Sub

    Public Function removeQuotes(ByVal inputString As String) As String
        Try
            If Left(inputString, 1) = """" Then
                inputString = Right(inputString, inputString.Length - 1)
            End If
            If Right(inputString, 1) = """" Then
                inputString = Left(inputString, inputString.Length - 1)
            End If
            Return inputString
        Catch
            Return inputString
        End Try
    End Function
End Class
