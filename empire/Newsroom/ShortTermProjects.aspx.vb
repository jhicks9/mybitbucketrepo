﻿
Partial Class Newsroom_ShortTermProjects
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sm As SiteMapPath = Page.Master.FindControl("SiteMapPath")  'disable sitemap
            sm.Visible = False
            lblCurrentNavigation.Text = Request.QueryString("title")
            Page.Header.Title = Request.QueryString("title")
            lblTitle.Text = Request.QueryString("title")
        Catch
        End Try
    End Sub
End Class
