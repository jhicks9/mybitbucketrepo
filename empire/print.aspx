<%@ Page Language="VB" AutoEventWireup="false" CodeFile="print.aspx.vb" Inherits="Print" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print</title>
    <style type="text/css" media="print">
        body {color:#000000;background:#ffffff;font-family:"Times New Roman",Times,serif;font-size:12pt; }
        a {text-decoration:underline;color:#0000ff;}
    </style>    
    <script type="text/javascript">
        function GetPrintContent()
                  {     var PrintDiv =  document.getElementById('printcontent');
                        var ContentDiv =  window.opener.document.getElementById('print_content');
                        PrintDiv.innerHTML = ContentDiv.innerHTML;   }
    </script>
</head>
<body onload="GetPrintContent();">
    <form id="form1" runat="server">
        <div style="width:100%;background-color:#ffffff;">
            <div style="float:left;width:20%;color:#000000;background-color:#ffffff;">
                <asp:Image id="imgLogo" runat="server" ImageUrl="~/images/navigation/logo_bw.jpg" />
            </div>
            <div style="float:left;width:80%;color:#000000;background-color:#ffffff;font-size:24pt;text-align:center;">
                <asp:Label ID="lblTitle" runat="server" Text="" ></asp:Label>
            </div>
            <div style="float:left;width:80%;color:#000000;background-color:#ffffff;text-align:center;">&nbsp;Confidential - For Internal Use Only</div>
            <div style="float:left;width:80%;color:#000000;background-color:#ffffff;text-align:center;">
                <asp:Label ID="lblDate" runat="server" Text="" ></asp:Label>
            </div>            
        </div>
        <div id="printcontent" style="clear:both;"></div>
    </form>
</body>
</html>
<%--Recommendations for a Print-Friendly Webpage
Change colors to black on white
Change the font to a serif face
Change the font size to 12pt (or larger)
Underline all links
Remove non-essential images,navigation,all advertising, JavaScript, Flash, and animated images
Include a by-line
Include the original URL
Include a copyright notification--%>