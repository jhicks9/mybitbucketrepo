Imports AjaxControlToolkit

Partial Class login
    Inherits System.Web.UI.Page
    Dim headermsg As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'dont show login page if user is currently logged in
            Dim userInfo As MembershipUser = Membership.GetUser()
            If Not userInfo Is Nothing Then
                EvalRoles(userInfo.ToString)
            End If

            txtUserName.Focus()
            If (Request.QueryString("ReturnUrl") <> "") Then
                Response.Redirect("~/login.aspx")
            End If

            'Have the ENTER key work as a login click
            txtUserName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLogin.UniqueID + "','')")
            txtPassword.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLogin.UniqueID + "','')")
        Catch
        End Try
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Try
            Dim lblPageHeader As Label = Page.Master.FindControl("lblPageHeader")
            lblPageHeader.Text = "Account Login and Registration"
        Catch
        End Try
    End Sub

    Function ResolveSSLUrl(ByVal url As String) As String
        Dim ssl As String = ""
        Try
            ssl = String.Format("https://{0}{1}", Request.ServerVariables("HTTP_HOST"), ResolveUrl(url))
        Catch
        End Try
        Return ssl
    End Function

    Protected Sub lbLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            loadScreen(0)  'enable modal processing screen
            Dim authUser As Boolean = True

            Dim userInfo As MembershipUser = Membership.GetUser(txtUserName.Text)  'See if this user exists
            If userInfo Is Nothing Then  'The user entered an invalid username
                lblStatus.Text = "Your login attempt was not successful."
                authUser = False
            End If

            If userInfo.IsLockedOut Then  'See if the user is locked out
                If Now > userInfo.LastLockoutDate.AddMinutes(30) Then  'if > 30 minutes then unlock account
                    lblStatus.Text = ""
                    userInfo.UnlockUser()
                    authUser = True
                Else
                    lblStatus.Text = "Your account has been locked for exceeding the maximum number of login attempts. Please try again in 30 minutes."
                    authUser = False  'account is locked -- do not authenticate
                End If
            End If

            If authUser Then  'account exists and is not locked -- proceed to authenticate user
                If (Membership.ValidateUser(txtUserName.Text, txtPassword.Text)) Then  'valid username/password
                    FormsAuthentication.SetAuthCookie(txtUserName.Text, True)
                    lblStatus.Text = ""
                    EvalRoles(txtUserName.Text)  'determine location to send user (based on role)
                Else  ''The user entered an invalid password
                    lblStatus.Text = "Your login attempt was not successful."
                    loadScreen(0)  'disable modal processing screen
                End If
            Else
                loadScreen(0)  'account doesnt exist or is locked  -- do not authenticate
            End If
        Catch
        End Try
    End Sub

    Public Sub EvalRoles(ByVal userName As String)
        If Roles.IsUserInRole(userName, "Customer") Or Roles.IsUserInRole(userName, "Admin") Then
            Response.Redirect("~/CustomerService/MyAccount/default.aspx")
        ElseIf Roles.IsUserInRole(userName, "Assistance") = True Then
            Response.Redirect("~/CustomerService/Assistance/default.aspx")
        ElseIf Roles.IsUserInRole(userName, "Commission") = True Then
            Response.Redirect("~/CustomerService/Commission/default.aspx")
        Else
            Response.Redirect("~/default.aspx")
        End If
    End Sub

    Public Function loadScreen(ByVal toggle As Boolean) As Boolean
        Dim uc As Web.UI.UserControl = CType(Page.Master.FindControl("modal1"), Web.UI.UserControl)
        Dim mp As ModalPopupExtender = CType(uc.FindControl("mpeProgress"), ModalPopupExtender)
        If Not uc Is Nothing Then
            If Not mp Is Nothing Then
                If toggle Then
                    mp.Show()  'enable modal loading screen
                Else
                    mp.Hide()  'disable modal loading screen
                End If
            End If
        End If
        Return True
    End Function
End Class
