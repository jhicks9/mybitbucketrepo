﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="PrivacyPolicy.aspx.vb" Inherits="PrivacyPolicy" Title="Privacy Policy" %>

<asp:Content ID="Privacy" ContentPlaceHolderID="RightPlaceHolder" runat="server">
    <strong>Empire Privacy Statement</strong><br />
    This privacy policy discloses the privacy practices of The Empire District Electric Company ("Empire"). This privacy policy applies to information collected by the website “empiredistrict.com” and all means of data collection through Empire business processes. It will notify you of the following:
    <ul>
        <li>
            What personally identifiable information is collected from you, how it is used and with whom it may be shared.</li>
        <li>
            What choices are available to you regarding the use of your data.</li>
        <li>
            The security procedures in place to protect against misuse of your information.</li>
        <li>
            How you can correct any inaccuracies in the information.</li>
    </ul>
    <strong>Information Collection, Use and Sharing</strong><br />
    Empire is the sole owner of the information collected and the company may only have access to and collect information that you voluntarily provide via email or other direct contact. Empire will not sell or rent this information to anyone.<br />
    <br />
    Empire will use your information to respond to you concerning the reason you contacted the company and to provide services. Empire will not share your information with any third party outside of the organization, other than as necessary to fulfill your request or normal business functions.<br />
    <br />
    <strong>Your Access to and Control over Information</strong><br />
    You may do the following at any time by contacting us via the email address or phone number given on our website or by visiting one of our customer service locations:
    <ul>
        <li>
            See what data we have about you, if any.</li>
        <li>
            Change/correct any data we have about you.</li>
        <li>
            Have us delete any data we have about you that is not required to provide service or required reporting.</li>
        <li>
            Express any concern you have about our use of your data.</li>
    </ul>
    <strong>Registration</strong><br />
    In order to use services available on our website, a user must first complete the registration form. During registration a user is required to give certain information (such as name and email address). This information is used to contact you about the products/services on our site in which you have expressed interest.<br />
    <br />
    <strong>Security</strong><br />
    We take precautions to protect your information. When you provide or submit sensitive information via the website, your information is protected both online and offline.<br />
    <br />
    Wherever we collect sensitive information (Personally Identifiable Information), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a closed lock icon at the bottom of your web browser, or looking for "https" at the beginning of the address of the web page.<br />
    <br />
    While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment. Security protection such as anti-virus software and firewalls are installed to limit access.<br />
    <br />
    <strong>Sharing</strong><br />
    We partner with another party to provide specific services. When the user signs up for services, we will share only information that is necessary for the third party to provide these services. These parties are not allowed to use personally identifiable information except for the purpose of providing these services.<br />
    <br />
    <strong>Links</strong><br />
    This web site contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our customers to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.<br />
    <br />
    <strong>Updates</strong><br />
    Our Privacy Policy may change from time to time and all updates will be posted on this page.<br />
    <br />
    If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at (800)206-2300 or via our website.
</asp:Content>