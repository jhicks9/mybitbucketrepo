Imports System.Xml.Linq
Imports System.Linq

Partial Class sidebar
    Inherits System.Web.UI.UserControl
    Private _navmenu As Boolean = False
    Private _imgurl As String = ""
    Dim psm As New processSiteMap

    Public Property navMenu() As Boolean
        Get
            Return _navmenu
        End Get
        Set(ByVal value As Boolean)
            _navmenu = value
        End Set
    End Property

    Public Property ImageUrl() As String
        Get
            Return _imgurl
        End Get
        Set(ByVal value As String)
            _imgurl = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'example how to inject javascript
        'Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        'colorbox.Attributes.Add("type", "text/javascript")
        'colorbox.TagName = "script"
        'colorbox.Attributes.Add("src", ResolveUrl("~/js/jquery.superfish.js"))
        'Page.Header.Controls.Add(colorbox)

        'example how to inject stylesheet
        'Dim css As HtmlLink = New HtmlLink
        'css.Href = ResolveUrl("~/slimbox2.css")
        'css.Attributes("rel") = "stylesheet"
        'css.Attributes("type") = "text/css"
        'css.Attributes("media") = "all"
        'Page.Header.Controls.Add(css)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If _imgurl <> "" Then
                imgSideImage.ImageUrl = _imgurl
                imgSideImage.Visible = True
            End If

            If _navmenu Then
                lblMenu.Visible = True
                Select Case psm.getCurrentNodeKey("resourcekey")
                    Case "MyAccount", "Assistance", "Commission"  ' only show single (current) node of menu tree when surfing in the following areas: myaccount, assistance agency, commission
                        lblMenu.Text = BuildSingleNode(psm.getCurrentNodeKey("resourcekey"))
                    Case "MyPayments"  'child node of myaccount so use parent
                        lblMenu.Text = BuildSingleNode(psm.getParentNodeKey("resourcekey"))
                    Case Else
                        lblMenu.Text = buildMenu()  'otherwise build normal site-wide menu
                End Select
            End If
        Catch
        End Try
    End Sub

    Private Function BuildSingleNode(ByVal node As String) As String
        Dim classSelected As String = ""
        Try
            Dim output As String = String.Empty
            Dim xelement2 As XElement = XElement.Load(Server.MapPath("~/web.sitemap"))
            Dim urlDescList1 = xelement2.Descendants().Where(Function(sel) CStr(sel.Attribute("resourceKey")) = node).SelectMany(Function(sel) sel.Elements()).Select(Function(nd) New With {Key .title = nd.Attribute("title").Value, Key .url = nd.Attribute("url").Value, Key .resourceKey = nd.Attribute("resourceKey").Value})
            output = "<ul id='sideMenu'>"
            For Each s In urlDescList1
                If s.resourceKey = node Then
                    If psm.getCurrentNodeKey("title") = s.title Or psm.getParentNodeKey("title") = s.title Then
                        classSelected = " class='selected'"
                    Else
                        classSelected = ""
                    End If
                    If Len(Request.QueryString("c")) > 0 And Len(Request.QueryString("b")) > 0 Then
                        'pass encrypted query string information if present (assistance/commission section use only)
                        output &= String.Format("<li{2}><a href=""{0}"">{1}</a></li>", ResolveUrl(s.url) & "?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n")), s.title, classSelected)
                    Else
                        output &= String.Format("<li{2}><a href=""{0}"">{1}</a></li>", ResolveUrl(s.url), s.title, classSelected)
                    End If
                End If
            Next s
            output &= "</ul>"
            Return output
        Catch
            Return Nothing
        End Try
    End Function

    Private Function buildMenu() As String
        Try
            ' Get all nodes from only the first two levels of the current site map (first level is empty)
            Dim imgurl As String = Page.ResolveUrl("~/images/icon")
            Dim Menu As String = String.Empty
            Dim rootCollection As SiteMapNodeCollection
            Dim rootNode As SiteMapNode
            'Dim childCollection As SiteMapNodeCollection
            'Dim childNode As SiteMapNode
            Dim classSelected As String = ""

            rootCollection = New SiteMapNodeCollection(SiteMap.RootNode.ChildNodes) 'start with second level
            Menu = "<ul id='sideMenu'>"
            For Each rootNode In rootCollection
                If SiteMap.CurrentNode.ResourceKey = rootNode.ResourceKey Then
                    classSelected = " class='selected'"
                Else
                    classSelected = ""
                End If
                If Not (LCase(rootNode.ResourceKey) = "news" Or LCase(rootNode.ResourceKey) = "outages" Or LCase(rootNode.ResourceKey) = "search") Then  '
                    Menu &= String.Format("<li{2}><a href=""{0}"">{1}", rootNode.Url, rootNode.Title, classSelected) & "</a></li>"

                    'If rootNode.HasChildNodes Then
                    '    childCollection = rootNode.ChildNodes
                    '    Menu &= "<ul>"
                    '    For Each childNode In childCollection
                    '        If childNode("imgUrl") IsNot Nothing Then
                    '            Menu &= String.Format("<li><a href=""{0}""><img class=""icon"" alt="""" src=""{3}/{2}""/> {1}</a></li>", _
                    '                                  childNode.Url, childNode.Title, childNode("imgUrl"), imgurl)
                    '        Else
                    '            Menu &= String.Format("<li><a href=""{0}"">{1}</a></li>", _
                    '                                  childNode.Url, childNode.Title)
                    '        End If
                    '    Next
                    '    Menu &= "</ul>"
                    'End If
                    'Menu &= "</li>"
                End If
            Next
            Menu &= "</ul>"
            Return Menu
        Catch
            Return Nothing
        End Try
    End Function

End Class
