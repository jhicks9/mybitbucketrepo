<%@ Control Language="VB" AutoEventWireup="false" CodeFile="footer.ascx.vb" Inherits="footer" %>
    
<div id="footercontent" style="width:960px;margin:0 auto 0px;">
    <table style="border-collapse:collapse;border-spacing:0;width:100%">
        <tr>
            <td align="left" style="width:125px">&nbsp;</td>
            <td style="padding-top:5px">
                <img id="imgsep1" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                602 S Joplin Avenue
                <img id="imgsep2" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                PO Box 127
                <img id="imgsep3" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                Joplin, MO 64802
                <img id="imgsep4" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
                <%--<br /><br /><a href="~/TermsofService.aspx" runat="server">Copyright � 2007-<asp:Label ID="lblEndYear" runat="server" Text="" /> Empire District. All Rights Reserved.</a>--%>
                <br /><br />Copyright � 2007-<asp:Label ID="lblEndYear" runat="server" Text="" /> Empire District. All Rights Reserved.<a href="<%= ResolveUrl("~/TermsofService.aspx") %>"> Terms</a> &<a href="<%= ResolveUrl("~/PrivacyPolicy.aspx")%>"> Privacy</a>
            </td>
            <td align="right" style="width:125px">
                <a href="https://www.youtube.com/channel/UCAZ5RA8lMtWFXRNp4M5-MaQ" target="_blank" title="Empire's youtube channel"><img id="imgYoutube" class="icon" runat="server" alt="Empire's youtube channel" src="~/images/icon/icon-youtube.gif"/></a>
                <a href="https://www.facebook.com/EmpireDistrictElectric" target="_blank" title="Empire's facebook site"><img id="imgFacebook" class="icon" runat="server" alt="Empire's facebook site" src="~/images/icon/icon-facebook.gif"/></a>
                <a href="https://m.empiredistrict.com" target="_blank" title="Empire's mobile site"><img id="img1" class="icon" runat="server" alt="Empire's mobile site" src="~/images/icon/icon_mobile.gif"/></a>
            </td>
        </tr>
    </table>
</div>