﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationBrochures.aspx.vb" Inherits="Trees_VegetationBrochures" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:Panel ID="pnlForm" runat="server" Visible="True">
        Learn more about specific vegetation management questions through resources provided by Empire District. Simply fill out and submit the following form to have brochures sent directly to you. You can choose which brochure you would like to receive by checking the appropriate box below. You may request more than one brochure topic.
        <br /><br />
        <table cellpadding="1" cellspacing="0" width="100%">
            <tr>
                <td style="width:110px;">First Name: *</td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First name required" ValidationGroup="avg" Display="Dynamic" ></asp:RequiredFieldValidator>
                </td>
                <td rowspan="12" valign="top">
                    <asp:Image ID="imgVegetationBrochures" runat="server" ImageUrl="~/images/vegetation-brochures.png" /> 
                </td>
            </tr>
            <tr>
                <td>Last Name: *</td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name required" ValidationGroup="avg" Display="Dynamic" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Address 1: *</td>
                <td>
                    <asp:TextBox ID="txtAddress1" runat="server" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>        
                </td>
            </tr>
            <tr>
                <td>Address 2:</td>
                <td>
                    <asp:TextBox ID="txtAddress2" runat="server" ValidationGroup="avg"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>City: *</td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>State: *</td>
                <td>
                    <asp:DropDownList ID="cboState" ValidationGroup="avg" runat="server">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem>AR</asp:ListItem>
                        <asp:ListItem>KS</asp:ListItem>
                        <asp:ListItem>MO</asp:ListItem>
                        <asp:ListItem>OK</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="cboState" ErrorMessage="State is required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>  
                </td>
            </tr>
            <tr>
                <td>Zip Code:*</td>
                <td>
                    <asp:TextBox ID="txtZipCode" runat="server" width="65px" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvZipCode" runat="server" ControlToValidate="txtZipCode" ErrorMessage="Zip Code required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>                     
                    <asp:RegularExpressionValidator ID="revZipCode" runat="server" ControlToValidate="txtZipCode"
                        Display="Dynamic" ErrorMessage="5 digit or zip + 4 " ValidationExpression="\d{5}(-\d{4})?"
                        ValidationGroup="avg"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>Email: *</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                        Display="Dynamic" ErrorMessage="Invalid Email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="avg"></asp:RegularExpressionValidator>            
                </td>
            </tr>
            <tr>
                <td>Home Phone:</td>
                <td>
                    <asp:TextBox ID="txtHomePhone" runat="server" ValidationGroup="avg"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revHomePhone" runat="server" ControlToValidate="txtHomePhone"
                        Display="Dynamic" ErrorMessage="Phone Number format xxx-xxx-xxxx " ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                        ValidationGroup="avg"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>Work Phone:</td>
                <td>
                    <asp:TextBox ID="txtWorkPhone" runat="server" ValidationGroup="avg"></asp:TextBox>&nbsp;&nbsp;&nbsp;Ext<asp:TextBox ID="txtWorkPhoneExtension" runat="server" Width="65px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revWorkPhone" runat="server" ControlToValidate="txtWorkPhone"
                        Display="Dynamic" ErrorMessage="Phone Number format xxx-xxx-xxxx " ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                        ValidationGroup="avg"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>Account Number: *</td>
                <td>
                    <asp:TextBox ID="txtAN" runat="server" Width="72px" MaxLength="6" ValidationGroup="avg"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAN" runat="server" ControlToValidate="txtAN" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator><b>&nbsp;- &nbsp;</b>
                    <asp:TextBox ID="txtBP" runat="server" Width="25px" ValidationGroup="avg" MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvBP" runat="server" ControlToValidate="txtBP" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator><b>&nbsp;- &nbsp;</b>
                    <asp:TextBox ID="txtCD" runat="server" Width="25px" ValidationGroup="avg" MaxLength="1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCD" runat="server" ControlToValidate="txtCD" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Shipping Method:&nbsp;&nbsp;</td>
                <td>USPS</td>
            </tr>
            <tr>
                <td valign="top">Brochure Type: *</td>
                <td colspan="2">
                    <asp:CheckBoxList ID="ckMaterialRequest" runat="server" RepeatColumns="2">
                        <asp:ListItem Value="Avoiding Tree & Utility Conflicts">Avoiding Tree & Utility Conflicts</asp:ListItem>
                        <asp:ListItem Value="Avoiding Tree Damage during Construction">Avoiding Tree Damage during Construction</asp:ListItem>
                        <asp:ListItem Value="Benefits of Trees">Benefits of Trees</asp:ListItem>
                        <asp:ListItem Value="Buying High-Quality Trees">Buying High-Quality Trees</asp:ListItem>
                        <asp:ListItem value="Insects and Disease Problems">Insects and Disease Problems</asp:ListItem>
                        <asp:ListItem Value="Mature Tree Care">Mature Tree Care</asp:ListItem>
                        <asp:ListItem Value="New Tree Planting">New Tree Planting</asp:ListItem>
                        <asp:ListItem Value="Plant Health Care">Plant Health Care</asp:ListItem>
                        <asp:ListItem Value="Proper Mulching Techniques">Proper Mulching Techniques</asp:ListItem>
                        <asp:ListItem Value="Pruning Mature Trees">Pruning Mature Trees</asp:ListItem>
                        <asp:ListItem Value="Pruning Young Trees">Pruning Young Trees</asp:ListItem>
                        <asp:ListItem value="Tree Selection and Placement">Tree Selection and Placement</asp:ListItem>
                        <asp:ListItem Value="Why Hire an Arborist">Why Hire an Arborist</asp:ListItem>
                        <asp:ListItem Value="Why Topping Hurts Trees">Why Topping Hurts Trees</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td style="font-size:smaller">* Required Information</td>
                <td colspan="2"></td>
            </tr>
        </table>

        <br /><asp:Label ID="lblStatus" runat="server" Font-Bold="true" ForeColor="Red" /><br />
        <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSubmit_Click"><span>Submit</span></asp:LinkButton>
    </asp:Panel>
    
    <asp:Panel ID="pnlEmailSent" runat="server" Visible="False">
        <div style="padding:15px">
            <asp:Label ID="lblEmailSent" runat="server" />
        </div>
    </asp:Panel>
</asp:Content>
