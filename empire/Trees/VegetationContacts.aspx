﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationContacts.aspx.vb" Inherits="Trees_VegetationContacts" Title="Vegetation Management Contacts"%>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <ul style="list-style:none;">
        <li style="padding:0;">Jason Grossman, Forester</li>
        <li style="padding:0;color:blue;">Vegetation Control Coordinator - Transmission</li>
        <li style="padding:0;"><a href="~/About/ContactUs.aspx?type=vm" runat="server">jgrossman@empiredistrict.com</a></li>
    </ul>
    <ul style="list-style:none;">
        <li style="padding:0;">Rob McGovern, Forester</li>
        <li style="padding:0;color:blue;">Vegetation Control Coordinator - East</li>
        <li style="padding:0;"><a href="~/About/ContactUs.aspx?type=vm" runat="server">rmcgovern@empiredistrict.com</a></li>
    </ul>
    <ul style="list-style:none;">
        <li style="padding:0;">Marilyn Ponder</li>
        <li style="padding:0;color:blue;">Vegetation Management Administrator</li>
        <li style="padding:0;"><a href="~/About/ContactUs.aspx?type=vm" runat="server">mponder@empiredistrict.com</a></li>
    </ul>
    <br />
    It is our desire at The Empire District Electric Company to inform and discuss our vegetation management practices with property owners.  If you have questions about Empire's vegetation management program, you may call the Contact Center at 800-206-2300.  All vegetation customer inquiries and responses are logged and we strive to return all customer calls within five days.
</asp:Content>

