﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationArboretumMapJoplin.aspx.vb" Inherits="Trees_VegetationArboretumMapJoplin" Title="Utility Arboretum Joplin"%>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <img id="imgJoplin" src="~/images/arboretum/joplin_arboretum.jpg" runat="server" alt="" border="0" usemap="#Arboretum" />
    <map name="Arboretum" id="Map1">
        <area shape="circle" coords="109,1070,13" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num1',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="131,1028,30" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num2',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="169,1025,09" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num3',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="181,1007,16" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num4',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="190,958,20" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num5',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="195,892,42" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num6',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="250,859,08" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num7',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="257,812,13" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num8',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="235,727,45" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num9',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="303,683,18" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num10',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="275,613,55" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num11',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="337,558,23" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num12',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="348,502,29" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num13',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="378,480,11" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num14',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="385,465,11" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num14',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="394,392,16" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num15',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="385,324,50" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num16',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="443,279,14" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num17',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="440,236,29" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num18',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="468,203,15" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num19',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="494,153,08" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num20',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="469,100,45" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num21',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="279,1092,16" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num22',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="274,1059,25" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num23',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="304,1023,20" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num24',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="284,958,19" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num25',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="321,900,35" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num26',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="301,860,12" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num27',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="311,816,05" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num28',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="372,804,59" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num29',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="344,745,15" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num30',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="351,716,09" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num31',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="391,687,34" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num32',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="400,622,23" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num33',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="412,576,20" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num34',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="418,545,14" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num35',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="455,513,38" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num36',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="465,457,28" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num37',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="456,412,07" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num38',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="472,367,08" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num39',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="477,351,08" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num39',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="513,254,10" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num40',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="360,429,38" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num41',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="562,227,50" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num42',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="563,151,20" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num43',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="597,104,37" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num44',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="538,321,54" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num45',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
    </map>

    <%--begin popup hovers--%>
    <div id="Num1" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Fringetree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj1Vegetation" src="~/images/arboretum/j1-fringetree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 20 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 15 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Flowering</td>
                <td><b>Shape:</b>&nbsp;Oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;1” long white flowers,<br />pleasant fragrance</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Simple, pinnately-<br />veined</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;Berries</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun-partial shade</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;The common name refers to the slightly fragrant, spring-blooming flowers which feature airy, terminal, drooping clusters of fringe-like, creamy white petals. They are easily grown in average, moist, well-drained soil.  They may be used in native plant gardens or near streams or ponds. Most commonly seen 10’ – 15’ tall in landscapes where they are grown in the open.</div></td></tr>
            <tr><td colspan="2"><img id="imgj1Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj1Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num2" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Japanese Larch</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj2Vegetation" src="~/images/arboretum/j2-japanese larch.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 80 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 40 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous conifer</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;n/a</td>
                <td><b>Foliage:</b>&nbsp;Bright green needles</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;3-7</td>
                <td><b>Fruit:</b>&nbsp;n/a</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;One of the few conifers that lose its leaves (needles) in the winter.  At that time it is recognized by its knobby branches.  In spring, bright green tufts of soft young needles appear. This tree can be used in residential landscapes since it grows at a slow rate.</div></td></tr>
            <tr><td colspan="2"><img id="imgj2Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 30 feet from wire zone&nbsp;</b><img id="imgj2Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num3" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Japanese Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj3Vegetation" src="~/images/arboretum/j3-japanese maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Broad rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;n/a</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Reddish-purple in<br />spring and again in the fall</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;A very showy, versatile species.  Use as a single specimen or in borders or groupings.  Can be a single-stemmed small tree or multi-stemmed shrub.</div></td></tr>
            <tr><td colspan="2"><img id="imgj3Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj3Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num4" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Kousa Dogwood</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj4Vegetation" src="~/images/arboretum/j4-kousa dogwood.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Flowering</td>
                <td><b>Shape:</b>&nbsp;Upright oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Creamy white in<br />May & June</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deep green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Berries</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This handsome small tree adds year-round beauty. White flowers in May and June give a milky way effect, purple and scarlet fall leaves add intense color. Beautiful tree form, with horizontal branching.</div></td></tr>
            <tr><td colspan="2"><img id="imgj4Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj4Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num5" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Oklahoma Eastern Redbud</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj5Vegetation" src="~/images/arboretum/j5-o8-oklahoma eastern redbud.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Flowering ornamental</td>
                <td valign="top"><b>Shape:</b>&nbsp;Twisted trunk and<br />spreading branches</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Light pink–magenta<br />1.5 cm long</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deep green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;Berries</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full shade</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;In the wild, Eastern Redbud is a frequent native understory tree in mixed forests and hedgerows.  It is also planted as a landscape ornamental plant.  The Redbud is the state tree of Oklahoma.</div></td></tr>
            <tr><td colspan="2"><img id="imgj5Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj5Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num6" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Deodar Cedar</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj6Vegetation" src="~/images/arboretum/j6-deodar cedar.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 70 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 40 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Conifer</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;n/a</td>
                <td><b>Foliage:</b>&nbsp;Blue green needles</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;6-9</td>
                <td><b>Fruit:</b>&nbsp;3’ – 6’ oval  cone</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Medium</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;The Deodar Cedar tree makes an excellent specimen tree for landscapes because of its pleasing shape and attractive silver-green coloring.  The tree also makes a fast growing wind screen and has been successfully used as street tree with the lower branches pruned for pedestrian traffic.</div></td></tr>
            <tr><td colspan="2"><img id="imgj6Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 30 feet from wire zone&nbsp;</b><img id="imgj6Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num7" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Butterfly Bush</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj7Vegetation" src="~/images/arboretum/j7-butterfly bush.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 15 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 15 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Woody perennial</td>
                <td><b>Shape:</b>&nbsp;Shrub</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;6-8” long flower<br /> heads - July</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Gray-green to <br />dark green leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td><b>Fruit:</b>&nbsp;Pods</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Attracts birds, hummingbirds, and butterflies.  Suitable for cut flowers.  Can be used as a border plant or in containers.  For best flowering, cut back to entire bush in early spring to about 1/3 of its original height.</div></td></tr>
            <tr><td colspan="2"><img id="imgj7Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj7Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num8" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Japanese Tree Lilac</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj8Vegetation" src="~/images/arboretum/j8-japanese tree lilac.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Flowering</td>
                <td valign="top"><b>Shape:</b>&nbsp;Oval vase-<br />shaped crown</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Creamy white</td>
                <td><b>Foliage:</b>&nbsp;Dark green</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-9</td>
                <td valign="top"><b>Fruit:</b>&nbsp;.5”-1” clusters<br />of capsules</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Medium</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Windbreaks and highway beautification. Prefers moist well-drained sites.  Moderately drought tolerant.  Good as a specimen or in groupings.  Effective on public grounds, parks, and boulevards.</div></td></tr>
            <tr><td colspan="2"><img id="imgj8Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj8Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num9" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Chinkapin Oak</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj9Vegetation" src="~/images/arboretum/j9-chinkapin oak.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 80 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 50 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Round spreading </td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;n/a</td>
                <td valign="top"><b>Foliage:</b>&nbsp;4”–6” dark<br />green leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;3-9</td>
                <td><b>Fruit:</b>&nbsp;1” sweet acorns</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;A worthy specimen for larger lawns or parks.  Their acorns are at the top of the food preference list for many wildlife species.  Does best in well-drained soil and adapts to many different soil types.  Grows 40’ - 50’ high under landscaping conditions, becoming 70’ – 80’ high in the wild.</div></td></tr>
            <tr><td colspan="2"><img id="imgj9Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj9Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num10" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Amur Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj10Vegetation" src="~/images/arboretum/j10-o2-amur maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Dense rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, April – June,<br />pleasant fragrance</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Oval shape, 3-lobed</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full–partial sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Used in the landscape trade for its dense rounded shape and beautiful fall foliage color.  Can be grown either as a multi-stemmed structure or pruned to a single-stem tree.  It prefers moist, well-drained soils but is also drought tolerant.</div></td></tr>
            <tr><td colspan="2"><img id="imgj10Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj10Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num11" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Princeton American Elm</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj11Vegetation" src="~/images/arboretum/j11-princeton american elm.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 90 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 70 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Vase shaped</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Simple, serrate,<br />ovate</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;2-9</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun-<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Adaptable to a wide variety of soil conditions, wind, heat, and pH.  Aggressive roots can break sidewalks and raise pavement if trees are improperly located.</div></td></tr>
            <tr><td colspan="2"><img id="imgj11Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 45 feet from wire zone&nbsp;</b><img id="imgj11Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num12" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Goldenraintree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj12Vegetation" src="~/images/arboretum/j12-goldenraintree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 40 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 35 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Yellow blossoms-<br />summer</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Bright green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td><b>Fruit:</b>&nbsp;Papery hanging capsules</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Adapts to many soil types and tolerates air pollution, drought, and alkalinity. </div></td></tr>
            <tr><td colspan="2"><img id="imgj12Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 28 feet from wire zone&nbsp;</b><img id="imgj12Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num13" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Shangtung (Purpleblow) Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj13Vegetation" src="~/images/arboretum/j13-o12-shangtung maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Vase shaped</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Yellow to green, .5”,<br />clustered in 3” flower heads</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green<br />star-shaped</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Samara</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;An outstanding small tree with glossy dark green, star-shaped leaves contrasted with newly emerging reddish-purple leaves.  Prefers well-drained soils.</div></td></tr>
            <tr><td colspan="2"><img id="imgj13Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj13Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num14" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Crape Myrtle</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj14Vegetation" src="~/images/arboretum/j14-crape myrtle.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Tree or hedge</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Pink & red clusters</td>
                <td><b>Foliage:</b>&nbsp;Dark green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;7-9</td>
                <td><b>Fruit:</b>&nbsp;Less than .5” berries</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Clusters of deciduous blooms from mid-to-late summer in various shades of pink and red.  Does best in moist, well-drained soil.</div></td></tr>
            <tr><td colspan="2"><img id="imgj14Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj14Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num15" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Trident Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj15Vegetation" src="~/images/arboretum/j15-trident maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 45 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 30 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Oval round</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Yellow, slightly<br /> rounded clusters</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun–<br />partial shade </td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Attractive small patio, lawn, or street tree.  Dark green leaves changing to yellow, orange, and red in the fall.</div></td></tr>
            <tr><td colspan="2"><img id="imgj15Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 25 feet from wire zone&nbsp;</b><img id="imgj15Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num16" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Japanese Pagoda Tree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj16Vegetation" src="~/images/arboretum/j16-japanese pagoda tree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 75 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 60 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Broad round crown</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, mid–<br />late summer</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;3-8” green pod</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This medium sized tree has a medium growth rate and tolerates city conditions, heat, and drought.  When grown in a yard, it can reach a height from 50’ – 75’; however, when grown along the road in compact soil it only grows to 40’.  It prefers an open, sunny location.</div></td></tr>
            <tr><td colspan="2"><img id="imgj16Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 40 feet from wire zone&nbsp;</b><img id="imgj16Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num17" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Sweetbay Magnolia</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj17Vegetation" src="~/images/arboretum/j17-sweetbay magnolia.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 20 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Columnar vase shape</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;2” – 3” creamy white,<br />spring and summer, lemon scent</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green leaves <br />with silver underside</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;5-10</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Bright scarlet-red<br /> seeded fruit</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Has a very elegant shape and a good choice for a specimen or patio tree.  The bright scarlet red fruit ripens in late summer attracting many birds.  Prefers moist, acid soil.</div></td></tr>
            <tr><td colspan="2"><img id="imgj17Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj17Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num18" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Red Sunset Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj18Vegetation" src="~/images/arboretum/j18-red sunset maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 40 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Oval</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Reddish winter/spring </td>
                <td><b>Foliage:</b>&nbsp;Green, star shaped</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Winged summer fruits</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Partial shade-<br />full sun</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Red Sunset is a red maple cultivar with superior fall color and good branch structure.</div></td></tr>
            <tr><td colspan="2"><img id="imgj18Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 30 feet from wire zone&nbsp;</b><img id="imgj18Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num19" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Paperbark Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj19Vegetation" src="~/images/arboretum/j19-paperbark maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Oval to rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;3”–6” Green trifoliate<br />leave, silvery underside</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;1”-3” Elongated oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Sun–partial<br />shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Cinnamon brown to reddish brown exfoliating bark.  Old bark is purple brown. It casts a light shade.  Prefers moist, well-drained soil but grows in a variety of soil types.</div></td></tr>
            <tr><td colspan="2"><img id="imgj19Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj19Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num20" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Diablo Ninebark</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj20Vegetation" src="~/images/arboretum/j20-diablo ninebark.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 6 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 5 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental shrub</td>
                <td><b>Shape:</b>&nbsp;Upright</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White button-like<br /> blooms, summer</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deep burgundy</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-7</td>
                <td valign="top"><b>Fruit:</b>&nbsp;1/4’ brown follicles <br />borne in dense, upright<br />hemispherical clusters</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Diablo is an easy growing shrub that is adaptable to a wide range of conditions.  Tolerant of many soils, preferring a well-drained site.  This will do great in back of the perennial garden or mixed in shrub borders or even as a foundation plant.</div></td></tr>
            <tr><td colspan="2"><img id="imgj20Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj20Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num21" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>White Fir</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj21Vegetation" src="~/images/arboretum/j21-white fir.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 30 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Evergreen Conifer</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Purple bloom when young</td>
                <td valign="top"><b>Foliage:</b>&nbsp;green blue needles</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-7</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Cylindrical cones, 5 inches</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun to light shade</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This tree makes a wonderful specimen tree.  Its bluish color makes a wonderful contrast when planted with other green conifers.  It is more tolerant of drought and heat than other fir trees.  It does not tolerate heavy clay soils.  Most famous for its role as a Christmas tree.  This tree is known to have medicinal properties.</div></td></tr>
            <tr><td colspan="2"><img id="imgj21Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 25 feet from wire zone&nbsp;</b><img id="imgj21Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num22" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>American Holly</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj22Vegetation" src="~/images/arboretum/j22-american holly.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Evergreen</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Green, white, cream or<br />gray, inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Thick and spiny</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td><b>Fruit:</b>&nbsp;Red berries attracts birds</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Medium</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;An excellent privacy screen and can also be pruned to hedge form.  Outstanding landscape single specimen tree.  Because it is evergreen, it is a beautiful focal point in the yard in the winter.</div></td></tr>
            <tr><td colspan="2"><img id="imgj22Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj22Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num23" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Autumn Brilliance Serviceberry</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj23Vegetation" src="~/images/arboretum/j23-autumn brilliance serviceberry.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 15 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White blossoms,<br />spring</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Blue-green foliage</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Sweet purplish-<br />black fruit </td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Its spectacular show of white flowers in early spring is one of its main features.  They begin to flower as a pale pink and fade to snowy white fragrant flowers while the blue green foliage turns a brilliant reddish-orange in the autumn.  This Serviceberry is easily grown in average, well-drained soil.</div></td></tr>
            <tr><td colspan="2"><img id="imgj23Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj23Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num24" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Red Buckeye</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj24Vegetation" src="~/images/arboretum/j24-red buckeye.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 20 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Round pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Deep red flowers</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Drooping dark<br />green leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;6-9</td>
                <td><b>Fruit:</b>&nbsp;Non-edible nut</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full shade-<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;With an attractive springtime display of showy deep red flowers, this tall shrub or small tree is quite a delight to many hummingbirds.  Likes moist, well-drained soil and full sun.  Makes a great specimen tree.</div></td></tr>
            <tr><td colspan="2"><img id="imgj24Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj24Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num25" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Green Giant Arborvitae</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj25Vegetation" src="~/images/arboretum/j25-green giant arborvitae.jpg" runat="server" alt=""  /></td>
                <td valign="top"><b>Mature Height:</b>&nbsp;Up to 60 feet</td>
                <td valign="top"><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Conifer</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Yellow in spring</td>
                <td><b>Foliage:</b>&nbsp;Dense green foilage</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-7</td>
                <td><b>Fruit:</b>&nbsp;n/a</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun-<br />partial shade</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Ideal for a hedge, screen, windbreak or single specimen for a large landscape.</div></td></tr>
            <tr><td colspan="2"><img id="imgj25Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 30 feet from wire zone&nbsp;</b><img id="imgj25Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num26" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Japanese Zelkova</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj26Vegetation" src="~/images/arboretum/j26-japanese zelkova.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 80 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 75 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Vase shaped</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Spring, yellow-green,<br />not showy</td>
                <td valign="top"><b>Foliage:</b>&nbsp;1.5” – 4”,<br />oblong-serrated</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;green/brown small fruit,<br />not showy</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Fast when young;<Br />later moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;A good street and shade tree that has an appealing vase-shaped form with a rounded crown. Green leaves turn yellow, copper, orange, or deep red to purplish-red in fall putting on a showy display. The peeling bark on older trees exposes orange patches which can be quite impressive.  It is pH adaptable, and when established is tolerant of wind, drought, and air pollution.  Some sources say that it is highly resistant to Dutch Elm Disease and is a good replacement for Elms.</div></td></tr>
            <tr><td colspan="2"><img id="imgj26Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 50 feet from wire zone&nbsp;</b><img id="imgj26Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num27" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding: 2px 2px 2px 2px;"><b>Seven Sun Flower</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj27Vegetation" src="~/images/arboretum/j27-seven-sun flower.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 20 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 15 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Upright</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, seven per<br /> stem, fall</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Strap-like</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Small rounded<br />fruit in fall</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun-light shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Early autumn brings the showy, fragrant creamy white clusters of seven flowers each produced in terminal panicles. Bark is bronzed and peeling, standing out in winter.</div></td></tr>
            <tr><td colspan="2"><img id="imgj27Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj27Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num28" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Rose of Sharon</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj28Vegetation" src="~/images/arboretum/j28-rose of sharon.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 12 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 10 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Deciduous</td>
                <td valign="top"><b>Shape:</b>&nbsp;Upright, can be <br />shrub or small tree</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Trumpet shaped flowers</td>
                <td><b>Foliage:</b>&nbsp;Medium to dark green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;The Rose of Sharon is a deciduous, upright, occasionally spreading shrub or small tree with multiple trunks.  The branches grow upright and will not droop except when in flower.  The leaves emerge late in the spring.</div></td></tr>
            <tr><td colspan="2"><img id="imgj28Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj28Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num29" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>London Planetree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj29Vegetation" src="~/images/arboretum/j29-london planetree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 90 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 100 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Round</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Red, hairy balls in Spring</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Large, palmately-lobed</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Not edible</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Is very tolerant of atmospheric pollution and root compaction. For those reasons it is a popular urban roadside tree.</div></td></tr>
            <tr><td colspan="2"><img id="imgj29Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 45 feet from wire zone&nbsp;</b><img id="imgj29Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num30" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Jon Jon Magnolia</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj30Vegetation" src="~/images/arboretum/j30-jon jon magnolia.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Round</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green,<br />leathery leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;7-9</td>
                <td><b>Fruit:</b>&nbsp;n/a</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full-partial sun</td>
                <td><b>Growth rate:</b>&nbsp;Medium</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Rose colored buds open to 10” – 12” white flowers.</div></td></tr>
            <tr><td colspan="2"><img id="imgj30Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj30Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num31" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Witch Hazel</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj31Vegetation" src="~/images/arboretum/j31-witch hazel.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 20 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 30 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Yellow, fragrant<br />blooms</td>
                <td valign="top"><b>Shape:</b>&nbsp;Upright spreading</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Yellow–copper,<br />January - March</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Good fall color</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Small hard capsules</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;A native small tree or large shrub with fantastic fall attributes. Attractive foliage in all growing seasons with leaves bright green in spring, yellow-orange by fall.  A great tree to plant as an understory or for a shrub border in large areas.  Prefers moist soils, but is tolerant of a variety of conditions.</div></td></tr>
            <tr><td colspan="2"><img id="imgj31Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 25 feet from wire zone&nbsp;</b><img id="imgj31Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num32" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Autumn Gold Gingko</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj32Vegetation" src="~/images/arboretum/j32-autumn gold gingko.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 35 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Conifer</td>
                <td><b>Shape:</b>&nbsp;Symmetrical</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Pleasant fragrance<br />inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Fan-shaped leaves</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Naked seed, 1” long<br />with a fleshy covering</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Prefers moist, sandy, well-drained soils.  Tolerant of a wide range of soil conditions, including both alkaline and acidic soils and compacted soils.  Adapts well to most urban environments. The Autumn Gold Gingko tree is a 100% male tree taken from cuttings.</div></td></tr>
            <tr><td colspan="2"><img id="imgj32Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 28 feet from wire zone&nbsp;</b><img id="imgj32Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num33" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Chinese Pistache</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj33Vegetation" src="~/images/arboretum/j33-chinese pistache.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 35 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 35 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Showy and red<br />in spring</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Outstanding autumn<br />color</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;6-9</td>
                <td><b>Fruit:</b>&nbsp;Oval, red, green or blue</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun-<br />partial shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Merits consideration as urban street tree plantings. Perfect shade tree.</div></td></tr>
            <tr><td colspan="2"><img id="imgj33Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 25 feet from wire zone&nbsp;</b><img id="imgj33Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num34" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Dawn Redwood</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj34Vegetation" src="~/images/arboretum/j34-dawn redwood.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 100 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous conifer</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td><b>Foliage:</b>&nbsp;Feathery</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Cones</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Dawn redwood is a deciduous conifer known for its buttress-shaped trunk, attractive lacy needles, and reddish-brown bark. It is too large for most typical landscapes, although it can be used on the corner of an ample residential lot.</div></td></tr>
            <tr><td colspan="2"><img id="imgj34Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgj34Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num35" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Burning Bush</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj35Vegetation" src="~/images/arboretum/j35-burning bush.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 10 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 15 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Shrub</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deep, dark<br />red leaf </td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;3-7</td>
                <td><b>Fruit:</b>&nbsp;Red berry</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun–<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This plant is good as a specimen for an informal hedge.  It leafs out early in spring and the summer foliage is blue-green. The Burning Bush gives you an amazing show in the fall months with incredible, bright red foliage.</div></td></tr>
            <tr><td colspan="2"><img id="imgj35Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj35Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num36" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Tulip Tree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj36Vegetation" src="~/images/arboretum/j36-tulip tree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 100 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 50 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Yellow-green petals</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Somewhat shaped like<br />a tulip </td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Oblong cone-like<br />aggregate of samaras</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun-light shade</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Trees become too large for most landscape situations, but they are nice additions to large properties and parks.  Trees bloom in May.</div></td></tr>
            <tr><td colspan="2"><img id="imgj36Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj36Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num37" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>American Hornbeam</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj37Vegetation" src="~/images/arboretum/j37-american hornbeam.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 30 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Orange or yellow</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Green, oblong leaves<br />with serrated edges</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;3-9</td>
                <td><b>Fruit:</b>&nbsp;Seeds, buds or catkins<br />eaten by several wildlife species</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Best in<br />partial-shade</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;A handsome tree in many locations, multiple trunked, low branching specimen can be very attractive, showing off the bark and trunk form.</div></td></tr>
            <tr><td colspan="2"><img id="imgj37Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 27 feet from wire zone&nbsp;</b><img id="imgj37Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num38" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Arrowwood Viburnum</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj38Vegetation" src="~/images/arboretum/j38-arrowwood viburnum.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 8 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 10 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Rounded</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Creamy white</td>
                <td><b>Foliage:</b>&nbsp;Attractive fall colors</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Bluish black,<br />oval drupes</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun-partial shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Arrowwood viburnum is a large multi-stemmed shrub with showy creamy white flowers in spring and attractive fall color. It prefers moist, well-drained soil and makes a good hedge or screen.</div></td></tr>
            <tr><td colspan="2"><img id="imgj38Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj38Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num39" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Chastetree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj39Vegetation" src="~/images/arboretum/j39-chastetree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 15 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Deciduous Tree<br />or Shrub</td>
                <td valign="top"><b>Shape:</b>&nbsp;Rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Fragrant shades<br />of purple</td>
                <td valign="top"><b>Foliage:</b>&nbsp;5-fingered, grey-green<br /> slightly fuzzy, sage scented</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;7-9</td>
                <td><b>Fruit:</b>&nbsp;Black, fleshy fruit</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;The Chastetree can serve as a small tree or large multi-stemmed shrub with a gray, knotty, and somewhat ornamental trunk.  The showy, fragrant, upwardly pointing panicles of lavender blooms attract butterflies and bees in the summer.</div></td></tr>
            <tr><td colspan="2"><img id="imgj39Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj39Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num40" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Jane Magnolia</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj40Vegetation" src="~/images/arboretum/j40-jane magnolia.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 15 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 10 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Deciduous</td>
                <td valign="top"><b>Shape:</b>&nbsp;Upright, round, oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Purple, tulip-shaped,<br />showy & large</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green & leathery</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Inconspicuous red berries<br />attract birds</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun-<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Small tree or hardy shrub, great choices for any accent, specimen or border use.  Showy flowers emerge in Spring before leaf out.</div></td></tr>
            <tr><td colspan="2"><img id="imgj40Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgj40Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num41" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Katsura Tree</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj41Vegetation" src="~/images/arboretum/j41-katsura tree.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 60 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 40 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Pyramidal in youth,<br />then spreading</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Heart-shaped – similar to<br />Redbud, emerge purple changing to<br />green then yellow/apricot in fall</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;n/a</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun</td>
                <td><b>Growth rate:</b>&nbsp;Medium to fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Good shade tree.  Prefers rich, moist yet well-drained soil.  Attractive purplish leaves at emergence that turn green and eventually turn a gorgeous yellow/apricot color in fall.   Transplanting can be difficult and should be done in the spring.  Tree exudes a strong spicy fragrance right before leaves drop in the fall.  Some mature trees may have the added bonus of peeling bark.</div></td></tr>
            <tr><td colspan="2"><img id="imgj41Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj41Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num42" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Sawtooth Oak</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj42Vegetation" src="~/images/arboretum/j42-sawtooth oak.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 45 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 50 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Round pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark lustrous summer<br />foliage, turns clear yellow to<br />golden brown in fall</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td valign="top"><b>Fruit:</b>&nbsp;1” acorns popular<br />with wildlife</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;As one of the fastest growing trees in its youth, the Sawtooth Oak is an attractive shade tree.</div></td></tr>
            <tr><td colspan="2"><img id="imgj42Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj42Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num43" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Yoshino Cherry</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj43Vegetation" src="~/images/arboretum/j43-yoshino cherry.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 45 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 40 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Fragrant,<br />white-pink</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;.5”–1” round<br />is attractive to birds</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This tree, along with other cousins of the same species, is the very symbol of spring beauty.  One of the most widely planted ornamental cherry trees, it is ideal for planting close to sidewalks or as a patio shade tree.</div></td></tr>
            <tr><td colspan="2"><img id="imgj43Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 30 feet from wire zone&nbsp;</b><img id="imgj43Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num44" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>American Yellowwood</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj44Vegetation" src="~/images/arboretum/j44-o10-american yellowwood.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 50 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Round</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, spring</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Greenish-<br />blue in summer</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Flat brown pods</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun-light shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Medium size tree for restricted spaces.  Plant it in the rear corner of the residential backyard landscape or use it as a shade tree on smaller properties.  The fragrant flowers are borne in hanging chains in spring resembling wisteria.</div></td></tr>
            <tr><td colspan="2"><img id="imgj44Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj44Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>

    <div id="Num45" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Jefferson American Elm</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgj45Vegetation" src="~/images/arboretum/j45-jefferson american elm.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 70 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 50 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Vase</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Green</td>
                <td><b>Foliage:</b>&nbsp;Simple, ovate, serrate</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-7</td>
                <td><b>Fruit:</b>&nbsp;Papery samaras</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun</td>
                <td><b>Growth rate:</b>&nbsp;Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;They are a favorite because of their urban tolerance.  The arching graceful limbs make for a great shade and street tree.</div></td></tr>
            <tr><td colspan="2"><img id="imgj45Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgj45Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <%--end popup hovers--%>
</asp:Content>