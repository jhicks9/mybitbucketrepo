﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationFAQ.aspx.vb" Inherits="Trees_VegetationFAQ" Title="Vegetation Management FAQ"%>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <a href="VegetationFAQ.aspx#A1">Why does Empire need to manage vegetation?</a><br />
    <a href="VegetationFAQ.aspx#A2">Which overhead power lines do you need to prune for?</a><br />
    <a href="VegetationFAQ.aspx#A3">How often do you patrol and prune for vegetation?</a><br />
    <a href="VegetationFAQ.aspx#A4">Is the work done by Empire crews or by contractor crews?</a><br />
    <a href="VegetationFAQ.aspx#A5">Does Empire employ certified arborists?</a><br />
    <a href="VegetationFAQ.aspx#A6">What do the different colored flags and painted dots that are on my tree mean?</a><br />
    <a href="VegetationFAQ.aspx#A7">As a property owner, do you need my permission to prune?</a><br />
    <a href="VegetationFAQ.aspx#A8">How far will you prune my trees from the wires?</a><br />
    <a href="VegetationFAQ.aspx#A9">What will my trees look like when you trim them?</a><br />
    <a href="VegetationFAQ.aspx#A10">My trees are not touching any electrical wires, why do you want to trim them?</a><br />
    <a href="VegetationFAQ.aspx#A11">When will the tree crews arrive to begin trimming?</a><br />
    <a href="VegetationFAQ.aspx#A12">Why do you need to cut so much?</a><br />
    <a href="VegetationFAQ.aspx#A13">How should I go about safely trimming the tree branches around my power lines?</a><br />
    <a href="VegetationFAQ.aspx#A14">How can residents help in ensuring that trees are not trimmed?</a><br />
    <a href="VegetationFAQ.aspx#A15">What safety rules should I follow if I do my own landscaping and gardening?</a><br />
    <a href="VegetationFAQ.aspx#A16">What about emergencies and storms?</a><br />
    <a href="VegetationFAQ.aspx#A17">Can I do the work myself?</a><br />
    <a href="VegetationFAQ.aspx#A18">Can I prune near the low-voltage service line?</a><br />
    <a href="VegetationFAQ.aspx#A19">Can I get mulch or logs?</a><br />
    <a href="VegetationFAQ.aspx#A20">I'm planting new trees on my property, do you have any tips on what to plant?</a><br />
    <a href="VegetationFAQ.aspx#A21">What about tree houses?</a><br />
    <a href="VegetationFAQ.aspx#A22">Why can’t you put the power lines underground in order to avoid pruning?</a><br />
    <a href="VegetationFAQ.aspx#A23">Can I plant vegetation near pad-mount transformers?</a><br />
    <a href="VegetationFAQ.aspx#A24">Why is Empire cutting more trees on the transmission rights-of-way than they did in the past?</a><br />
    <a href="VegetationFAQ.aspx#A27">Why does Empire need access roads through landowner property and on the rights-of-way?</a><br />
    <a href="VegetationFAQ.aspx#A28">Does Empire have the right to cut landowner’s trees?</a><br />
    <a href="VegetationFAQ.aspx#A29">Isn’t Empire trespassing when they come onto private property?</a><br />
    <a href="VegetationFAQ.aspx#A30">Who should landowners contact before cutting trees that are near or in Empire’s right-of-way?</a><br />
    <a href="VegetationFAQ.aspx#A31">Will Empire top trees in the right-of-way instead of cutting it?</a><br />
    <a href="VegetationFAQ.aspx#A32">How often are areas patrolled?</a><br />
    <br /><br />
    <a id="A1"></a><strong>Q. Why does Empire need to manage vegetation?</strong><br />
    <b>A.</b> Empire prunes trees to deliver safe and reliable electric service.  Empire manages the vegetation on and near its rights-of-way (ROW) to keep the system reliable. Trees and other tall growing vegetation that grow close to high-voltage transmission lines can be a hazard. Trees don’t have to touch a high-voltage transmission line to be dangerous. In certain circumstances, electricity can “arc” from wires through the air to trees or equipment, resulting in fires, injuries, or even fatalities to anyone near the tree or equipment. Touching any part of the electrical path can cause serious injury or death. 
    Tree-related power outages are more than just an inconvenience, they can disrupt service to homes, businesses, hospitals, and important community services. In order to prevent these power disruptions, trees close to power lines are removed or trimmed. For all of these reasons Empire maintains an active vegetation management program. 
    <br /><br />
    <a id="A2"></a><strong>Q. Which overhead power lines do you need to prune for?</strong><br />
    <asp:Image ID="imgPowerLines" Imageurl="~/images/vegetation-CEDL.jpg" runat="server" /><br />
    <b>A.</b> High-voltage overhead power lines are cleared of vegetation. In most cases, these are the lines in the highest position on power poles.  Low-voltage or secondary lines, those below the transformer, require less maintenance and are usually cleared during our routine cycle pruning. Service lines to your house are also low voltage, they need pruned only if there is heavy strain or abrasion present.  These lines must be cleared by customers or their contractors if they can safely do so.
    <br /><br />
    <a id="A3"></a><strong>Q. How often do you patrol and prune for vegetation?</strong><br />
    <b>A.</b> Empire’s service territory is patrolled and pruned every four years in Oklahoma and on a six-year maintenance cycle in Kansas and Arkansas.
    In Missouri, the service territory is patrolled every four years for urban areas and six years for rural areas on the “routine pruning cycle” in Missouri. The urban area is patrolled once again at two years and the rural area at three years for the and again at either two years or three years, respectively, for the “mid-cycle survey.”
    Routine Cycle: Trees are inspected and pruned as need to maintain compliance.
    Mid-cycle Survey: Only trees that will cause outages during the respective cycle are pruned at this time as determined by the utility.
    <br /><br />
    <a id="A4"></a><strong>Q. Is the work done by Empire crews or by contractor crews?</strong><br />
    <b>A</b>. Only during emergency situations would a lineman do any trimming. Empire hires qualified line clearance tree companies to do their maintenance pruning.  
    Empire vegetation management contracts with Wright Tree Service and Shade Tree.  All companies working on Empire’s facilities are required by the National Electrical Safety Code (NESC) and Occupational Safety and Health Administration (OSHA) to be line-clearance certified in order to work near high voltage power lines. Empire utilizes ACRT, an outside contractor, to provide job planning services.
    <br /><br />
    <a id="A5"></a><strong>Q. Does Empire employ certified arborists?</strong><br />
    <b>A.</b> It is important that each tree professional working for Empire be trained in proper techniques and safety.  Extensive training is provided by the contractors.  We also have both foresters and certified arborists who oversee all facets of our vegetation management programs.  
    <br /><br />
    <a id="A6"></a><strong>Q. What do the different colored flags and painted dots that are on my tree mean?</strong><br />
    <b>A.</b> The ACRT job planners tie different colored flagging tape around the trees to correspond with the work plan the tree trimming crews receive.  The different colored flags indicate:<br />
    <div style="padding: 10px 50px 0px 30px;">
        <table cellpadding="0" cellspacing="0" width="100%" >
            <tr>
                <td valign="top"><div style="width:16px;height:16px;background-color:orange;margin-left:15px;margin-right:4px;">&nbsp;</div></td>
                <td>Orange tape means the tree is a good candidate for removal, but without the customers approval we will only be trimming it this cycle;</td>
            </tr>
            <tr>
                <td valign="top"><div style="width:16px;height:16px;background-color:blue;padding-top:2px;margin-left:15px;margin-right:4px;">&nbsp;</div></td>
                <td>Blue tape means the tree is a good candidate for removal, but doesn’t need trimmed at this time.  If the customer does not agree to removing it, nothing will happen to their tree this cycle; and</td>
            </tr>
            <tr>
                <td valign="top"><div style="width:16px;height:16px;background-color:red;margin-left:15px;margin-right:4px;">&nbsp;</div></td>
                <td>A red dot means tree will be trimmed using natural pruning methods.</td>
            </tr>
        </table>
    </div>
    <br />
    <a id="A7"></a><strong>Q. As a property owner, do you need my permission to prune?</strong><br />
    <b>A.</b> When our ACRT job planners identify tree pruning to be done on your property, you will be notified in person or with a 
    <a href="~/trees/VegetationManagementAdditional.aspx#doorcards" runat="server" >door hanger</a>.  Your permission is not required, because state law mandates that we maintain our lines, and keep them reliable. 
    <br /><br />
    <a id="A8"></a><strong>Q. How far will you prune my trees from the wires?</strong><br />
    <b>A.</b> The amount of pruning is based on tree growth and structure, wind sway, and line sag. Factors that influence the amount and type of pruning necessary include species of tree, environmental factors, irrigation, proximity of the tree to a line, line voltage, and line configuration. We may need to include a reasonable margin of safety above the absolute minimum clearance requirements.
    Trees and other vegetation growing in proximity to overhead utility facilities must adhere to federal and state regulations.  
    <br /><br />
    <a id="A9"></a><strong>Q. What will my trees look like when you trim them?</strong><br />
    <b>A.</b> That depends on a lot of different factors- the proximity of your tree to our lines, the growth habit of the tree, the size of the limbs that need to be removed, the availability of suitable lateral limbs to prune to, and other trimming that has been done to the tree (such as thinning, stubbing or trimming for other utilities etc.). The trimming of trees down the street may not reflect what your trees will look like.  Very often trees that are directly under the lines and are taller than the lines will have a “V” or “Y” shape. While this may not be the most aesthetically pleasing form, it is an arboriculturally correct method that will help promote the long-term health of the trees by removing less of the crown.
    <br /><br />
    <a id="A10"></a><strong>Q. My trees are not touching any electrical wires, why do you want to trim them?</strong><br />
    <b>A.</b> Your trees may not be actually touching the wires, at this time, but they could be close enough to sway into the lines in high winds. Also, all the trees on this line should be on the same approximate maintenance schedule to improve reliability to everyone on the circuit. It would not be a good use of resources to have each of the 1,000 to 2,000 customers on a given circuit on a different trimming schedule. It is ideal to prune limbs before they cause a problem. By doing so, we can direct the growth away from the lines while the tree is young and resilient.
    <br /><br />
    <a id="A11"></a><strong>Q. When will the tree crews arrive to begin trimming?</strong><br />
    <b>A.</b> Tree crews can arrive anywhere between 7 – 90 days after an ACRT job planner has patrolled an area. The residents are notified by the planner in person or with a 
    <a href="~/trees/VegetationManagementAdditional.aspx#doorcards" runat="server">door hanger</a> notice.  At times, scheduling conflicts, emergencies, or other interruptions occur that may alter the schedule. If the work is urgent, crews may arrive in a matter of days. If you have questions or concerns, call the job planner listed on the door hanger or Empire at 800-206-2300.
    <br /><br />
    <a id="A12"></a><strong>Q. Why do you need to cut so much?</strong><br />
    <b>A.</b> Our standards are based on several factors - the minimum safe working distance for a tree worker, how far trees sway in high winds, the ability of limbs to break and fall on lines, and the varying growth and regrowth rates of different species of trees. We can never trim enough to prevent all tree outages; however, we attempt to get enough clearance to withstand normal seasonal weather conditions. 
    Empire’s goal is to deliver safe and reliable electric service to you and your neighbors. In order to accomplish this, we need to maintain a safe distance between your trees and our wires. 
    <br /><br />
    <a id="A13"></a><strong>Q. How should I go about safely trimming the tree branches around my power lines?</strong><br />
    <b>A.</b> You should never get near an electric line or use any kind of equipment close to a line.
    If tree branches have grown into lines in an alley, street, or backyard utility easement (this includes all major lines except the service lines that go over your property from the pole to the electric meter), you should call Empire so we can schedule an inspection of your property.
    If the branches have grown too close or into the service line running across your property from the pole to the meter, please contact Empire to schedule that inspection. If you will be doing the trimming yourself, you should call our call center and arrange to have the service line disconnected to safely prune. There is no cost involved to have the line disconnected.
    You can avoid the expense and inconvenience of calling a qualified tree trimmer by never planting trees that could grow tall near or under the service line. 
    <br /><br />
    <a id="A14"></a><strong>Q. How can residents help in ensuring that trees are not trimmed?</strong><br />
    <b>A.</b> Residents can help Empire minimize tree-related outages and ensure the beauty of their landscapes through thoughtful planting of new trees. Trees planted near the electric lines should either be low growing shrubs or trees, planted at least as far away from the lines as the trees' natural spread from power lines. Spreading trees, such as elms and oaks, are common outage-causing culprits and should be planted at least 50 feet away from power lines.
    Before planting trees on your property, the location of utility lines should be considered. Are lines overhead or underground? Never dig near underground utility lines. Future access to these underground lines should be considered when planting.
    <br /><br />
    <a id="A15"></a><strong>Q. What safety rules should I follow if I do my own landscaping and gardening?</strong><br />
    <b>A.</b> If overhead electric lines serve your home, never get too close to the lines, and don’t take tools or other objects near the lines. If you have underground electric service, never dig until you know where the lines are buried. To have underground lines located and marked at no charge, call 1.800.DIG.RITE. Please call at least two business days before you start your project to 
    <br /><br />
    <a id="A16"></a><strong>Q. What about emergencies and storms?</strong><br />
    <b>A.</b> If you see a downed power line or need to report an outage, please call Empire at 800-203-2300. During emergency efforts to restore power, uprooted trees and or broken branches may be removed from the power lines by Empire crews and their tree pruning contractors. Once the vegetation has been cleared from the lines the final clean-up of storm damaged trees and limbs is the responsibility of the tree owner.
    When outages occur, Empire is only responsible for restoring the electric service. The homeowner is responsible for contacting the phone and cable companies if work is needed to restore those services. 
    <br /><br />
    <a id="A17"></a><strong>Q. Can I do the work myself?</strong><br />
    <b>A.</b> Pruning trees near electric lines can be dangerous to you and others. Unqualified tree workers put their lives in jeopardy without specialized training or the proper insulated tools required to work near electric lines. Hiring an unqualified tree contractor could put the contractor and yourself at a significant liability risk, should a worker be injured or killed while performing work. 
    Observe the 10-foot rule.  If you want to remove a tree or any part of it that is closer than 10 feet to a high-voltage or primary power line, always call Empire’s vegetation management office at 800-206-2300 or by email at <a href="~/About/ContactUs.aspx?type=vm" runat="server">vegetationmanagement@empiredistrict.com</a>.   
    When you contact us, we will visit your site and inspect the line at no charge to determine if it is safe for you to proceed. We may determine that we need to assist you by making it safe for you to complete your work. If so, Empire will create a safe distance between the tree and the power lines so your own tree trimming contractor can safely perform the work needed. This is known as a “make safe.” Pruning is scheduled, and the clean up of the cut limbs is the responsibility of the property owner.
    <br /><br />
    <a id="A18"></a><strong>Q. Can I prune near the low-voltage service line?</strong><br />
    <b>A.</b> Limbs are pruned during routine pruning cycle only if there is strain or abrasion present. If you are having tree work done and you or the contractor you hire are uncomfortable working near the lines, you may request to have the service line disconnected while you perform the work. Call the Empire call center at 800-206-2300 to request a service disconnect. 
    <br /><br />
    <a id="A19"></a><strong>Q. Can I get mulch or logs?</strong><br />
    <b>A.</b> Free mulch or logs are available for Empire customers to use. Customers must be willing to accept a full truck load. <asp:HyperLink ID="hlMulchRequest" runat="server" NavigateUrl="~/Trees/VegetationMulchRequestForm.aspx" Text="Click here" /> for the online mulch request form.
    <br /><br />
    <a id="A20"></a><strong>Q. I'm planting new trees on my property, do you have any tips on what to plant?</strong><br />
    <b>A.</b> Planting the Right Tree in the Right Place provides a permanent solution for maintaining reliable electric service. There are many tips through The National Arbor Day site – <a href="http://www.arborday.org/trees/righttreeandplace/" target="_blank" >“Tree Planting – Right Tree, Right Place.”</a>  There are also helpful links under <asp:hyperlink ID="hlTreePlantingTops" runat="server" NavigateUrl="~/Trees/TreePlanting.aspx">tree planting guidelines</asp:hyperlink>.
    <br /><br />
    <a id="A21"></a><strong>Q. What about tree houses?</strong><br />
    <b>A.</b> If any part of a tree holding a tree house is within 10 feet of a power line, it is too close, and the risk of electrocution to children playing in the tree house is very high. Make sure that children cannot reach the lines with a pole or any other object. 
    <br /><br />
    <a id="A22"></a><strong>Q. Why can’t you put the power lines underground in order to avoid pruning?</strong><br />
    <b>A.</b> This sounds like an aesthetically pleasing and safe alternative to overhead lines, but there are significant challenges associated with undergrounding high-voltage lines. To convert an existing overhead system to underground, all trees and vegetation must be removed or have the roots trenched through in order to bury the underground cable. New easements also need to be negotiated. Undergrounding comes with an extremely high price tag, coupled with longer outages and more difficult repairs in the event of a power failure. 
    You should find out where underground facilities are located before you dig on your property. Call 1-800-DIG-RITE at least two working days before you dig.
    <br /><br />
    <a id="A23"></a><strong>Q. Can I plant vegetation near pad-mount transformers?</strong><br />
    <b>A.</b>
    Please keep vegetation and structures 10 feet from the front and 5 feet from the sides of this equipment. Empire field crews need room to work safely on these devices. Obstructions will cause safety issues and delays when restoring electric service.
    <br /><br />
    <a id="A24"></a><strong>Q. Why is Empire cutting more trees on the transmission rights-of-way than they did in the past?</strong><br />
    <b>A.</b> In June of 2007 the North America Electric Reliability Corporation (NERC) mandated a new vegetation management standard for all transmission utility owners.  Tree caused outages may cause Empire to be out of compliance with this new standard and subject to penalties.  
    <br /><br />
    <a id="A27"></a><strong>Q. Why does Empire need access roads through landowner property and on the rights-of-way?</strong><br />
    <b>A.</b> Empire acquires, constructs, and maintains road systems that provide access to and along Empire’s transmission lines for the primary use by transmission line maintenance crews.  Empire has legal rights, established in the easement, for roads located within the ROW.  For roads located outside of the ROW, Empire has rights to use the access for ingress and egress.  The goal is to provide safe access and reduced response times for field and line crews during emergency and regular maintenance activities that restore or enhance system reliability. 
    <br /><br />
    <a id="A28"></a><strong>Q. Does Empire have the right to cut landowner’s trees?</strong><br />
    <b>A.</b> Empire owns a rights-of-way easement along the length of the transmission line. Width varies by transmission line.  Empire’s rights within the easement include the right to construct, reconstruct, operate, maintain, and patrol the transmission line.  These rights apply to both occupied and unoccupied ROWs.  Rights usually reserved to the landowner include the right to cultivate, occupy, and use the land for any purpose that does not conflict with Empire’s use of its easement. In order to avoid potential conflicts, it is Empire’s policy to review all proposed uses within the transmission line rights-of-way easement. In such reviews we consider the safety of the public, the safety of our employees, and the ability to operate and maintain the line.
    <br /><br />
    <a id="A29"></a><strong>Q. Isn’t Empire trespassing when they come onto private property?</strong><br />
    <b>A.</b> No, Empire’s easement rights allow us to access private property to construct, reconstruct, operate, maintain, and patrol the transmission line.  Empire will make efforts to notify landowners when we are planning to remove vegetation or have performed vegetation clearing.  
    <br /><br />
    <a id="A30"></a><strong>Q. Who should landowners contact before cutting trees that are near or in Empire’s right-of-way?</strong><br />
    <b>A.</b> Never cut or trim a tree near a transmission line.  You should contact Empire’s vegetation management department at 800&#8209;206&#8209;2300, for assistance.
    <br /><br />
    <a id="A31"></a><strong>Q. Will Empire top trees in the right-of-way instead of cutting it?</strong><br />
    <b>A.</b> It is Empire’s policy not to top trees within the ROW.  Continual topping and trimming of trees that are close to high-voltage transmission lines is not as safe or reliable as removing them.  
    <br /><br />
    <a id="A32"></a><strong>Q. How often are areas patrolled?</strong><br />
    <b>A.</b> Vegetation management personnel patrol every mile, of every line, every year to inspect all vegetation to insure reliability of the system with an aerial patrol of the transmission lines each year. Other Empire departments may also patrol areas for additional maintenance needs.  
    <br /><br />
    If you spot an area of concern or have questions, please contact the Vegetation Management Department at 800&#8209;206&#8209;2300.
</asp:Content>

