﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Trees_VegetationManagementAdditional
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader
        Try
            conn.Open()
            cmd.CommandText = "select top 1 id,description from documents where groupid = 14 and doctypeid = 103 and (webdescription like '%distribution line door card' or webdescription like 'distribution line door card%' or webdescription like '%distribution line door card%')"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hlDistributionLineCard.NavigateUrl = "~/DocHandler.ashx?id=" & dr("id").ToString
                    hlDistributionLineCard.Text = dr("description").ToString
                End While
            End If
            dr.Close()

            cmd.CommandText = "select top 1 id,description from documents where groupid = 14 and doctypeid = 103 and (webdescription like '%transmission line door card' or webdescription like 'transmission line door card%' or webdescription like '%transmission line door card%')"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hlTransmissionLineCard.NavigateUrl = "~/DocHandler.ashx?id=" & dr("id").ToString
                    hlTransmissionLineCard.Text = dr("description").ToString
                End While
            End If
            dr.Close()

            conn.Close()
        Catch
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub
End Class
