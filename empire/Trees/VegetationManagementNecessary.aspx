﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationManagementNecessary.aspx.vb" Inherits="Trees_VegetationManagementNecessary" Title="Why Trimming is Necessary" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style='float:right;margin:0 0 10px 10px'>
        <asp:Image ID="imgVegetationNecessary" Imageurl="~/images/vegetation-necessary.jpg" runat="server" />    
    </div>
    <div>
        Empire customers have seen the devastation first hand that trees can cause to power lines. The damage caused by fallen branches and trees can result in outages to numerous customers. Empire's vegetation management program addresses trees and brush encroaching the utility right of way, the cause of outages.
        <br /><br />
        Empire's serves customers in four states, Missouri, Oklahoma, Kansas, and Arkansas.  Each state has a service commission mandating the minimum requirements that Empire's vegetation program must achieve.  Based on these regulations, Empire has developed a cyclical maintenance schedule with mid-cycle surveys for each circuit.
        <br /><br />
        Empire’s vegetation management’s mission is to maintain a high level of service reliability by limiting outages caused by vegetation.
    </div>
</asp:Content>

