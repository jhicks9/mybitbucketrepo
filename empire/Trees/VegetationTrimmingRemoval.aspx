﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationTrimmingRemoval.aspx.vb" Inherits="Trees_VegetationTrimmingRemoval" Title="Tree Trimming and Removal"%>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    In order to adhere to both federal and state requirements, utilities must remove vegetation growing near power lines. A regular maintenance plan for power lines can help prevent outages caused by trees and branches.
    <br /><br />
    Empire reminds customers never to trim trees near power lines. If you need assistance, call Empire at 800-206-2300. All vegetation inquiries are handled through our call center to monitor customer inquiries and responses.
    <br /><br />
    <h4>Who is responsible for trimming and debris removal?</h4>
    <b>Pole to pole</b><br />
    Empire is responsible for maintaining the lines between poles. If you believe that tree limbs, broken branches, or other hazards may cause issues on lines between poles, please contact Empire so vegetation management personnel can inspect and assess the situation.
    <br /><br />
    <b>Pole to meter</b><br />
    This is called a service line. It delivers the electricity from Empire’s lines directly to your meter. Maintenance of this line is the homeowner’s responsibility.
    <br /><br />
    <b>Drop service request</b><br />
    If you have trees that need to be trimmed near your service line, Empire will disconnect the service wire free of charge to allow for safe tree work for you or your contractor.  Please call 800-206-2300 to request this service at least 48 hours in advance of tree work.  This advance notice will allow us to schedule a crew to drop the service.  We request that you, your representative, or your tree contractor be present when your service is disconnected.
    <br /><br />
    <b>Tree Removal</b><br />
    If you would like to remove a tree within 10 feet of a pole to pole line, please contact Empire at 800-206-2300 to have it inspected at no charge by an Empire vegetation management representative. They can inspect the line and determine if Empire needs to trim or remove limbs so you or your contractor can safely remove the tree. The homeowner is responsible for the debris removal.
    <br /><br />
    <b>Debris Removal</b><br />
    Empire will chip and remove limbs from routine line clearance work.  Logs will be cut into manageable lengths and stacked on the property unless property owner contacts Empire prior to the trimming to request removal of the logs.  The property owner is responsible for materials they request to have left on their property.
    <br /><br />
    <b>Storm Work</b><br />
    Empire does not clean up brush, limbs, or trees from storm related work. Our priority is power restoration to our customers. We will clear all fallen trees and limbs from Empire lines in a safe manner to restore service and prevent recurring outages. All logs and limbs are left on site, and crews will not return to remove them.
</asp:Content>

