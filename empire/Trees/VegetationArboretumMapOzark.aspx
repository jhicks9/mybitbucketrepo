﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationArboretumMapOzark.aspx.vb" Inherits="Trees_VegetationArboretumMapOzark" Title="Utility Arboretum Ozark"%>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <img id="imgOzark" src="~/images/arboretum/ozark_arboretum.jpg" runat="server" alt="" border="0" usemap="#Arboretum" />
    <map name="Arboretum" id="Arboretum">
        <area shape="circle" coords="507,129,31" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num1',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="515,240,33" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num2',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="515,319,15" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num3',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="515,413,45" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num4',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="446,473,31" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num5',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="443,413,24" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num6',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="455,344,28" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num7',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="446,241,33" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num8',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="438,129,22" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num9',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="248,198,51" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num10',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="300,326,37" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num11',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="299,413,29" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num12',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="300,490,38" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num13',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="300,576,33" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num14',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="205,568,56" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num15',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="205,439,69" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num16',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
        <area shape="circle" coords="197,327,60" alt="" title="" href="javascript:void(0)" onmouseover="TagToTip('Num17',FADEIN,200,FADEOUT,200,FOLLOWMOUSE,false,BGCOLOR,'#f5f5f5',BORDERCOLOR,'#113962',BORDERWIDTH,1,PADDING,5,JUMPVERT,true,JUMPHORZ,false)" onmouseout="UnTip();" />
    </map>
    
    <%--begin popup hovers--%>
    <div id="Num1" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Brakens Brown Magnolia</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo1Vegetation" src="~/images/arboretum/o1-brakens brown magnolia.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Evergreen</td>
                <td><b>Shape:</b>&nbsp;Oval upright</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Spring and summer<br /> showing, pleasant fragrance</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Leathery dark green</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;7-10</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Cones containing a<br /> bright red seed</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun to Shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;One of the finest evergreen Magnolias ever developed and is probably one of the most cold tolerate of the southern Magnolia cultivars.</div></td></tr>
            <tr><td colspan="2"><img id="imgo1Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo1Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num2" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Amur Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo2Vegetation" src="~/images/arboretum/j10-o2-amur maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Dense rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, April – June,<br />pleasant fragrance</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Oval shape, 3-lobed</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full to partial sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Used in the landscape trade for its dense rounded shape and beautiful fall foliage color.  Can be grown either as a multi-stemmed structure or pruned to a single-stem tree.<br />  It prefers moist, well-drained soils but also drought tolerant.</div></td></tr>
            <tr><td colspan="2"><img id="imgo2Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo2Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num3" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Foster Holly</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo3Vegetation" src="~/images/arboretum/o3-foster holly.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 12 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Evergreen ornamental</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Small white flowers</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Small dark green <br />with mini spines</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;6-9</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Red berries in<br />fall and winter</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun -<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Excellent plant for dark evergreen color.  Can be used as a screening hedge or specimen plant even in pots. Very drought tolerant once established.</div></td></tr>
            <tr><td colspan="2"><img id="imgo3Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo3Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num4" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Kwanzan Cherry</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo4Vegetation" src="~/images/arboretum/o4-kwanzan cherry.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Vase shaped</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Double pink in clusters<br />of 3–5 flowers, April</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Showy in fall, turns <br />copper, orange, or yellow</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td><b>Fruit:</b>&nbsp;Inconspicuous</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This is one of the easiest flowering trees to grow.  Thrives in almost any soil and climate.  Blooms double pink flowers in April and also a delight in the fall with golden leaves.</div></td></tr>
            <tr><td colspan="2"><img id="imgo4Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgo4Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num5" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Pink Dogwood</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo5Vegetation" src="~/images/arboretum/o5-pink dogwood.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Flowering</td>
                <td><b>Shape:</b>&nbsp;Upright oval</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Pink blooms</td>
                <td><b>Foliage:</b>&nbsp;Deep green </td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Berries</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun -<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This handsome small tree adds year-round beauty. Pink flowers in late winter and early spring, fall foliage is a bright red. This tree is easy to grow.</div></td></tr>
            <tr><td colspan="2"><img id="imgo5Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgo5Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num6" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Autumn Brilliance Serviceberry</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo6Vegetation" src="~/images/arboretum/o6-autumn brilliance serviceberry.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 15 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Rounded</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;White blossoms, spring</td>
                <td><b>Foliage:</b>&nbsp;Blue-green foliage</td>
            </tr>
            <tr>
                <td valign="top"><b>Hardiness Zone:</b>&nbsp;3-8</td>
                <td valign="top"><b>Fruit:</b>&nbsp;Berry like pome,<br /> red to purple </td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This is a very popular ornamental shrub.  Its spectacular show of white flowers in early spring is one of its main features. Is easily grown in average, well-drained soil.</div></td></tr>
            <tr><td colspan="2"><img id="imgo6Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo6Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num7" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>White Dogwood</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="6" valign="top"><img id="imgo7Vegetation" src="~/images/arboretum/o7-white dogwood.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Flowering</td>
                <td><b>Shape:</b>&nbsp;Upright oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White spring time<br /> blossoms</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deep Green </td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-8</td>
                <td><b>Fruit:</b>&nbsp;Berries</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Full sun <br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This handsome small tree adds year-round beauty. White flowers in May and June give a milky way effect; scarlet fall leaves add intense color. Beautiful tree form, with horizontal branching.</div></td></tr>
            <tr><td></td><td colspan="2"><img id="imgo7Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgo7Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num8" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Oklahoma Eastern Redbud</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="img08Vegetation" src="~/images/arboretum/j5-o8-oklahoma eastern redbud.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td valign="top"><b>Type:</b>&nbsp;Flowering ornamental</td>
                <td valign="top"><b>Shape:</b>&nbsp;Twisted trunk and<br />spreading branches</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Light pink–magenta<br />1.5 cm long</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deep green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;Berries</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full shade</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;In the wild, Eastern Redbud is a frequent native understory tree in mixed forests and hedgerows.  It is also planted as a landscape ornamental plant.  The Redbud is the state tree of Oklahoma.</div></td></tr>
            <tr><td colspan="2"><img id="imgo8Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgo8Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num9" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Tina Crabapple Sargent</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="5" valign="top"><img id="imgo9Vegetation" src="~/images/arboretum/o8-tina crabapple sargent.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 10 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 12 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Small round</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;White</td>
                <td><b>Foliage:</b>&nbsp;Medium green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Red</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td></td><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Grown as a 40” top graft, this smallest of all crabapple trees features a compact, rounded head.  Flowers, fruit, and leaves are all quite small.  Excellent disease resistance.</div></td></tr>
            <tr><td></td><td colspan="2"><img id="imgo9Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo9Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num10" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>American Yellowwood</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo10Vegetation" src="~/images/arboretum/j44-o10-american yellowwood.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 50 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Round</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, spring</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Greenish-blue<br />in summer</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Flat brown pods</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun - light shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Medium size tree for restricted spaces.  Plant it in the rear corner of the residential backyard landscape or use it as a shade tree on smaller properties.  The fragrant flowers are borne in hanging chains in spring resembling wisteria.</div></td></tr>
            <tr><td colspan="2"><img id="imgo10Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 35 feet from wire zone&nbsp;</b><img id="imgo10Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num11" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Sango Kaku Japanese Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo11Vegetation" src="~/images/arboretum/o11-sango kaku japanese maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 25 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Upright rounded</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;n/a</td>
                <td><b>Foliage:</b>&nbsp;Light green </td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;6-8</td>
                <td><b>Fruit:</b>&nbsp;n/a</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Sun -<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Moderate-slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;The bright coral colored bark is one of many outstanding features of this maple.  Its Japanese name means “Coral Tower.”  At times the bark color becomes almost fluorescent. The colors brighten in the fall and intensify even more as winter approaches.</div></td></tr>
            <tr><td colspan="2"><img id="imgo11Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo11Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num12" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Shangtung (Purpleblow) Maple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo12Vegetation" src="~/images/arboretum/j13-o12-shangtung maple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 30 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 25 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Vase shaped</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Yellow to green, .5”,<br /> clustered in 3” flower heads</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green<br />Star-shaped</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Samara</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;An outstanding small tree with glossy dark green, star-shaped leaves contrasted with newly emerging reddish-purple leaves.  Prefers well drained soils.</div></td></tr>
            <tr><td colspan="2"><img id="imgo12Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 23 feet from wire zone&nbsp;</b><img id="imgo12Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num13" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Cleveland Select Callery Pear</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo13Vegetation" src="~/images/arboretum/o13-cleveland select callery pear.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 30 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Flowering</td>
                <td><b>Shape:</b>&nbsp;Upright oval</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;White, spring</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Dark green<br /> oval leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;5” rounded pome</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;This tree is remarkably resistant to sicknesses and blight. It is killed more often by storms and high winds than by sickness.</div></td></tr>
            <tr><td colspan="2"><img id="imgo13Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 25 feet from wire zone&nbsp;</b><img id="imgo13Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num14" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Prairiefire Crabapple</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo14Vegetation" src="~/images/arboretum/o14-prairiefire crabapple.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 20 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 20 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Ornamental</td>
                <td><b>Shape:</b>&nbsp;Upright rounded</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Dark purplish<br /> red in color</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Red-purple sharp<br /> toothed leaves mature<br /> into dark green leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;Red–purple fruit</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Full sun</td>
                <td><b>Growth rate:</b>&nbsp;Slow</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;The Prairiefire Crabapple is not only magnificent in flower, fruit, foliage and form it is also virtually disease-free.</div></td></tr>
            <tr><td colspan="2"><img id="imgo14Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 20 feet from wire zone&nbsp;</b><img id="imgo14Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num15" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>River Birch</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo15Vegetation" src="~/images/arboretum/o15-river birch.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 50 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 35 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td><b>Foliage:</b>&nbsp;Bright green</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-9</td>
                <td><b>Fruit:</b>&nbsp;1”-1.5” long cone-like</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun - light shade</td>
                <td><b>Growth rate:</b>&nbsp;Moderate - Fast</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;River Birch has peeling cinnamon brown bark.  It makes a great specimen tree, although it can be short lived in the landscape.  To encourage a healthy, long lived tree in your landscape, remove all grass in a large circle under the tree and cover the soil with 2-4” of organic mulch.</div></td></tr>
            <tr><td colspan="2"><img id="imgo15Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 28 feet from wire zone&nbsp;</b><img id="imgo15Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num16" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Pin Oak</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo16Vegetation" src="~/images/arboretum/o16-pin oak.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 75 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 40 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;Inconspicuous</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Deeply lobed, sharp<br /> pointed, glossy green leaves</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;4-8</td>
                <td><b>Fruit:</b>&nbsp;Acorns</td>
            </tr>
            <tr>
                <td><b>Sun exposure:</b>&nbsp;Sun</td>
                <td><b>Growth rate:</b>&nbsp;Slow to moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Oaks are sensitive to soil compaction so care should be taken when heavy construction equipment is used around their roots.</div></td></tr>
            <tr><td colspan="2"><img id="imgo16Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 30 feet from wire zone&nbsp;</b><img id="imgo16Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <div id="Num17" style="display:none;">
        <div style="display:block;text-align:center;color:#ffffff;background-color:#113962;padding:2px 0 2px 0;"><b>Bald Cypress</b></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
                <td rowspan="7" valign="top"><img id="imgo17Vegetation" src="~/images/arboretum/o17-bald cypress.jpg" runat="server" alt=""  /></td>
                <td><b>Mature Height:</b>&nbsp;Up to 70 feet</td>
                <td><b>Mature Spread:</b>&nbsp;Up to 30 feet</td>
            </tr>
            <tr>
                <td><b>Type:</b>&nbsp;Deciduous conifer</td>
                <td><b>Shape:</b>&nbsp;Pyramidal</td>
            </tr>
            <tr>
                <td valign="top"><b>Flower:</b>&nbsp;n/a</td>
                <td valign="top"><b>Foliage:</b>&nbsp;Greenish-yellow<br /> needle like</td>
            </tr>
            <tr>
                <td><b>Hardiness Zone:</b>&nbsp;5-9</td>
                <td><b>Fruit:</b>&nbsp;Cones</td>
            </tr>
            <tr>
                <td valign="top"><b>Sun exposure:</b>&nbsp;Sun -<br />partial shade</td>
                <td valign="top"><b>Growth rate:</b>&nbsp;Moderate</td>
            </tr>
            <tr><td colspan="2"><div style="width:375px"><b>Description:</b>&nbsp;Very popular ornamental tree. Grown for its light feathery foliage, turning brown-orange in the fall. State tree of Louisiana.</div></td></tr>
            <tr><td colspan="2"><img id="imgo17Distance1" src="~/images/arboretum/wiredistance1.png" runat="server" alt=""  /><b>&nbsp;Plant at least 25 feet from wire zone&nbsp;</b><img id="imgo17Distance2" src="~/images/arboretum/wiredistance2.png" runat="server" alt=""  /></td></tr>
        </table>
    </div>
    <%--end popup hovers--%>
</asp:Content>