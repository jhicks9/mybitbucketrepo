﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationManagementAdditional.aspx.vb" Inherits="Trees_VegetationManagementAdditional" Title="Additional Services and Information"%>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style="float:right;width:280px;padding:0 0 5px 5px;">
        <asp:Image ID="imgVMWireZone" runat="server" ImageUrl="~/images/vegetation-wirezone.jpg" />
    </div>
    <div>
        <b>Transmission right of ways</b><br />
        To control vegetation growth underneath and on both sides of a transmission structure, Empire utilizes a zoned approach. The zone directly underneath and 20 feet beyond the wires is called the wire zone. Empire encourages growth of grasses and other low-lying plant species.
        <br /><br />
        The zones on either side of the transmission structure are called the border zones, where scattered shrubs and dwarf trees are allowed. 
        <br /><br />
        Beyond the border zone, plants of any height are managed by the hazards they may present to the lines.
    </div>
    <div style="clear:both"></div>
    <br />
    <b>Contractors</b><br />
    Empire utilizes several contractors for both vegetation management surveying and tree trimming. Our contractors are recognized as professional companies who utilize approved arboricultural standards.
    <br /><br />
    These companies are visible in both urban and rural areas when they are performing their jobs. We encourage customers who have questions about personnel on or near their property to ask if they are affiliated with Empire.
    <br /><br />
    Empire employs the following contractors:
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" style='width:210px;background-color:#113962;padding:3px 0 3px 0;color:#fff'><b>Planning Services</b></td>
            <td><div style='width:10px'>&nbsp;</div></td>
            <td align="center" style='width:210px;background-color:#113962;padding:3px 0 3px 0;color:#fff'><b>Tree Trimming Services</b></td>
            <td><div style='width:10px'>&nbsp;</div></td>
            <td align="center" style='width:210px;background-color:#113962;padding:3px 0 3px 0;color:#fff'><b>Heavy Equipment Contractors</b></td>
        </tr>
        <tr>
            <td align="center" valign="middle"><asp:Image ID="imgVegContractors4" runat="server" ImageUrl="~/images/vegetation-contractors4.png" /></td>
            <td ><div style='width:10px'>&nbsp;</div></td>
            <td align="center" valign="middle"><asp:Image ID="imgVegContractors7" runat="server" ImageUrl="~/images/vegetation-contractors7.png" /></td>
            <td><div style='width:10px'>&nbsp;</div></td>
            <td align="center" valign="middle"><asp:Image ID="imgVegContractors2" runat="server" ImageUrl="~/images/vegetation-contractors2.png" /></td>
        </tr>
        <tr>
            <td align="center" valign="middle">&nbsp</td>
            <td ><div style='width:10px'>&nbsp;</div></td>
            <td align="center" valign="middle"><asp:Image ID="imgVegContractors6" runat="server" ImageUrl="~/images/vegetation-contractors6.png" /></td>
            <td><div style='width:10px'>&nbsp;</div></td>
            <td align="center" valign="middle"><asp:Image ID="imgVegContractors5" runat="server" ImageUrl="~/images/vegetation-contractors5.png" /></td>
        </tr>
        <tr>
            <td align="center" valign="middle">&nbsp;</td>
            <td ><div style='width:10px'>&nbsp;</div></td>
            <td align="center" valign="middle"><asp:Image ID="imgVegContractors3" runat="server" ImageUrl="~/images/vegetation-contractors3.png" /></td>
            <td><div style='width:10px'>&nbsp;</div></td>
            <td align="center" valign="middle" style="padding-top:5px"><asp:Image ID="imgVegContractors1" runat="server" ImageUrl="~/images/vegetation-contractors1.png" /></td>
        </tr>
    </table>    
    <br /><br />
    <b>Door cards</b><br /><div><a id="doorcards"></a></div>
    Once an Empire contractor has surveyed your property and determined that you do have trees that need to be trimmed for the betterment of the system, a door card will be left at your residence outlining the work that needs to be completed.
    <br /><br />    
    Click here to see a <asp:Image ID="imgDocument2" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;<asp:HyperLink ID="hlDistributionLineCard" runat="server" Target="_blank" NavigateUrl="" Text="" />.
    <br />
    Click here to see a <asp:Image ID="imgDocument3" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;<asp:HyperLink ID="hlTransmissionLineCard" runat="server" Target="_blank" NavigateUrl="" Text="" />.
    <br /><br />
    <b>Equipment and tools</b><br />
    In many instances Empire contractors will utilize conventional trimming techniques through the use of chain saws. Other times contractors will mechanically clear an area. These tools are site nutrient recycling tools; mowers, side trimmers, and tree grinders.
    <br /><br />
    In addition to trimming, Empire contractors utilize a variety of herbicides to control vegetation growth.  All herbicides have met rigorous testing by Environmental Protection Agency (EPA) and are prudent tools when used according to label directions.  Herbicides are the preferred method of control because they inhibit re-sprouting, thereby reducing the need for repetitive cutting; and through selected application to undesirable invasive plant species, we can promote bio-diversity to benefit wild life.  The wildlife habitat is enhanced by the use of herbicides, not destroyed. Plus, they are safe for domesticated animals.
    <br /><br />
    Part of Empire’s vegetation management program is the use of Tree Growth Regulators (TGR) on selected trees. The use of TGR enables Empire to reduce the frequency of trimming away from our power lines.  We apply the TGR compound into the soil at the tree’s base. During the growing season, the compound is absorbed by the tree’s roots and travels into the entire tree. 
    <br /><br />
    This specially designed compound limits growth in the crown of the tree, resulting in a more stable and balanced tree. Plus, over time trees appear revived and healthier when compared with surrounding trees of the same species. 
    <br /><br />
    TGRs have no adverse effect on critical processes that naturally occur in trees, and treated trees require no special care. TGRs have met the rigorous standards set by the EPA. They pose no unreasonable adverse effects to humans, animals, or the environment. Plus, TGRs do not accumulate in fruit or nuts so there is not a risk to wildlife. TGRs may temporarily limit growth on grass or shrubs against the base of the tree, but cause no damage to those plants. Other tree byproducts, leaves, wood, or chips, are safe to use as the compound breaks down into harmless elements. 
    <br /><br />
    The use of TGR is a service we provide as part of normal maintenance and at no charge to the property owner.
    <br /><br />
    <h4>Brush and debris removal</h4>
    <b>Debris Removal</b><br />
    Empire specifications in handling debris from tree work in yards are to grind and dispose of limbs and cut and stack wood at the base of the tree or stump.  However, Empire works with homeowners during the planning phase to determine what debris will be removed after trimming.
    <br /><br />
    In instances of mechanical clearing, all debris is left on site after it is ground by a large mower.
    <br /><br />
    <b>Storm Work</b><br />
    Empire does not clean up brush, limbs, or trees from storm related work. Our priority is power restoration to our customers. We will clear all fallen trees and limbs from Empire lines in a safe manner that restores service and prevents future outages. However, all logs and limbs are left on site, and crews will not return to remove them.
    <br /><br />
    <b>Free Mulch</b><br />
    The debris left behind from tree trimming and ground clearance is ground up and can be used by our customers as mulch.
    <br /><br />
    If you are interested in receiving a truck load of mulch, please use the form below. Please note, mulch is delivered on an “as available” basis and in the order of request.
    <br /><br />
    <asp:HyperLink ID="hlMulchRequest" runat="server" NavigateUrl="~/Trees/VegetationMulchRequestForm.aspx" Text="Mulch Request Form" />
    <br />
</asp:Content>

