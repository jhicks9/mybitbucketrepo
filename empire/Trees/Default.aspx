﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Trees_Default" Title="Trees" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style='float:right;margin:0 0 10px 10px'>
        <asp:Image ID="imgVegetationMain" Imageurl="~/images/landing/landing-vegetation.jpg" runat="server" />    
    </div>
    <div>
        Empire has an extensive integrated vegetation management program designed to keep our system safely and reliably delivering electricity to our customers. This department is assigned with the task of managing the vegetation on nearly 7,000 miles of lines. 
        <br /><br />
        More about Empire’s vegetation management policies and procedures:
    </div>
    
    <div class="landingtext">
        <br />
        <asp:Label ID="txtList" runat="server" Text=""></asp:Label>        
    </div>
</asp:Content>