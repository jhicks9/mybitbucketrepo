﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="VegetationMulchRequestForm.aspx.vb" Inherits="Trees_VegetationMulchRequestForm" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div style='float:right;width:350px;margin:0px 0 0 5px;'>
                <img id='imgMulchRequest' src='~/images/vegetation-mulch.jpg' runat='server' alt='' />
            </div>
            <div>
                The debris left behind from tree trimming and ground clearance is ground up and can be used by our customers as mulch.
                <br /><br />
                <asp:Panel ID="pnlMulchDesc" runat="server" Visible="true">
                    If you are interested in receiving a truck load of mulch, please fill out and submit the form below.
                    Please note, mulch is delivered on an “as available” basis and in the order of request.
                    <br /><br />
                    Reminders for our customers about the mulch offered by Empire:
                    <ul>
                        <li>Mulch is not always available. We cannot guarantee date and time of delivery.</li>
                        <li>The mulch offered is not the decorative mulch purchased at garden stores.</li>
                        <li>If mulch is in the compost stage, it takes approximately four to six weeks to deteriorate. During this time it may severely damage garden plants and shrubs.</li>
                        <li>Do not place mulch near structures, patios, or play areas due to risk of pests, mold, and/or ignition.</li>
                        <li>During the summer season, green mulch may attract stinging insects.</li>
                        <li>As it deteriorates, the mulch may have a strong odor like that of rotting grass and leaves.</li>
                        <li>As it deteriorates, we encourage you to keep a hose nearby as the decaying material does have an ignition risk.</li>
                        <li>The mulch will kill grass under it and may stain a driveway or sidewalk.</li>
                        <li>The mulch will likely contain stringy material including small sticks and yard rakings.</li>
                        <li>Do not burn wood mulch in any wood burning stove, fireplace, furnace, etc.</li>
                        <li>Average size of mulch load contains between seven and ten cubic yards.</li>
                        <li>All deliveries must be made on a solid surface on your property, and Empire and its contractors are not responsible for any yard or surface damage caused by delivery.</li>
                        <li>Once a load is delivered and unloaded, it becomes your property and Empire will not pick it up.</li>
                    </ul>
                </asp:Panel>
            </div>
            <asp:Panel ID="pnlFinalize" runat="server" Visible="false">
                <div style="padding:25px 0 0 0">
                    <asp:label ID="lblStatus" Text="" runat="server" Font-Bold="false" />
                    <br /><br />
                    <asp:label ID="lblInstructions" runat="server" Font-Bold="false" Visible="false" Text="An Empire representative will contact you before a delivery is made to confirm delivery and will inquire about the location of the delivery." />
                </div>
            </asp:Panel>
            <div style="clear:both;"></div>

            <asp:Panel ID="pnlMulchInfo" runat="server" Visible="true">
                <font style="color:#113962"><b>Mulch Request Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr valign="top">
                            <td align="right" valign="middle"><asp:label ID="lblName" Text="* Customer Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right" valign="middle"><asp:label ID="lblStreetAddress" Text="* Street Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtStreetAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvStreetAddress" runat="server" ControlToValidate="txtStreetAddress" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right" valign="middle"><asp:label ID="lblCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvfCity" runat="server" ControlToValidate="txtCity" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblState" Text="State: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="txtState" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblZip" Text="Zip: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtZip" runat="server" ValidationGroup="avg" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right" valign="middle"><asp:label ID="lblPrimaryPhone" Text="* Daytime Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right" valign="middle"><asp:label ID="lblEmail" Text="* Email address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="avg" Display="Dynamic"
                                    ErrorMessage="Invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right" valign="middle"><asp:label ID="lblLocation" Text="* Property:&nbsp;<br />Description:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLocation" runat="server" ValidationGroup="avg" Width="300px" TextMode="MultiLine" Height="35px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="txtLocation" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td></td><td>(detailed location on the property where the mulch can be deposited)</td></tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td colspan="2" align="center">
                                <div style="width:720px">
                                    I willingly assume the risks described above and any other risks associated with receiving the mulch as described above, and I hereby waive the right to hold Empire or its contractors liable for these risks.
                                </div>
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top">
                            <td></td>
                            <td>
                                <asp:CheckBox ID="ckbAgree" runat="server" Text="I accept the terms and conditions" Font-Bold="false" AutoPostBack="true" />&nbsp;&nbsp;<asp:Label ID="lblAgreeStatus" runat="server" ForeColor="Red" Text="" />
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td><asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg"><span>Submit Request</span></asp:LinkButton></td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function autotab(original, destination) {
            if (original.getAttribute && original.value.length == original.getAttribute("maxlength"))
                destination.focus();
        }
    </script>
</asp:Content>
