﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="TreePlanting.aspx.vb" Inherits="Trees_TreePlanting" Title="Tree Planting Guidelines" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />

    <hr style="color:#113962;height:1px;"/>    
    <div style='float:left;width:400px;background:#ffffff;margin:0px 5px 5px 0px;'>
        <img id="Img1" src="~/images/vegetation-treeplantingtips.jpg" runat="server" alt="tree planting tips"/>
    </div>
    <div>
        Whether it’s spring, summer, or fall, if you plan to work on your landscaping, following a few tips can ensure you pick the right tree for the right location.
        <br /><br />
        <asp:DataList ID="dlResidential" runat="server" SkinID="DataList" DataSourceID="sqlDS">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("description") %>'
                       NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>'
                       Target="_blank" runat="server"/>
        </itemtemplate>
        </asp:DataList>
        <br />
        For more information regarding planting trees in your neighborhood, visit the Arbor Day Foundation's<br /> <a href="http://www.arborday.org/trees/righttreeandplace/" target="_blank" >Right Tree, Right Place</a> Web site or our <asp:HyperLink ID="hlUtilityArboretum" runat="server" NavigateUrl="~/Trees/VegetationArboretum.aspx">Utility Arboretum</asp:HyperLink>.
    </div>
    <div style="clear:both;"></div>
    <asp:SqlDataSource ID="sqlDS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select id,description from documents where groupid = 14 and doctypeid = 103 and (webdescription like '%tree planting%' or description like '%tree planting%') ">
    </asp:SqlDataSource>
</asp:Content>

