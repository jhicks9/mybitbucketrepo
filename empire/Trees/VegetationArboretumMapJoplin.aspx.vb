﻿
Partial Class Trees_VegetationArboretumMapJoplin
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim js As New HtmlGenericControl("script")
        js.Attributes("type") = "text/javascript"
        js.Attributes("src") = ResolveUrl("~/js/wz_tooltip-5.31.js")
        Page.Form.Controls.Add(js)
    End Sub
End Class
