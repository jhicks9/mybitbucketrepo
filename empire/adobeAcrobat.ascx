﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="adobeAcrobat.ascx.vb" Inherits="adobeAcrobat" %>

<asp:Panel ID='pnlAcrobat' runat='server' Visible='false'>
    <div style="padding-top:0px;margin-bottom:20px;">
        <a href="http://get.adobe.com/reader/" target="_blank"><asp:Image ID="imgAcrobat" runat="server" style="float:left;padding-right:5px" ImageUrl="~/images/navigation/get_adobe_reader.gif" AlternateText="" ToolTip=""/></a>Adobe Acrobat Reader is required to view documents on this site.<br />
        <a href="http://get.adobe.com/reader/" target="_blank">Download Adobe Acrobat Reader</a>
    </div>
</asp:Panel>
