<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" debug="true" AutoEventWireup="false" CodeFile="Register.aspx.vb" Inherits="Register" title="Registration Page" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlValidateUser" runat="server" Visible="true">
                You will need to know your account number, your name, and the last four digits of your Social Security Number to register.<br />
                Your name and Empire District account number should match <b>exactly</b> as it appears on your statement.  For example:
                <div id="gallery">
                <a id="A1" href="~/images/bill_sample_full_page.jpg" runat="server" title="Click to enlarge">
                    <img id="img1" src="~/images/bill_sample_thumbnail.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
                </div>
                <br /><hr style="color:#113962;height:1px;" /><br />
                Account Number<br />
                <asp:TextBox ID="Textbox1" runat="server" Width="72px" MaxLength="6" ValidationGroup="ValidateUser"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAN" runat="server" ControlToValidate="Textbox1" ValidationGroup="ValidateUser" ErrorMessage="*" SetFocusOnError="True" Display="Dynamic" />
                -
                <asp:TextBox ID="Textbox2" runat="server" Width="25px" ValidationGroup="ValidateUser" MaxLength="2"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvBP" runat="server" ControlToValidate="Textbox2" ValidationGroup="ValidateUser" ErrorMessage="*" SetFocusOnError="True" Display="Dynamic" />
                -
                <asp:TextBox ID="TextBox3" runat="server" Width="25px" ValidationGroup="ValidateUser" MaxLength="1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCD" runat="server" ControlToValidate="Textbox3" ValidationGroup="ValidateUser" ErrorMessage="*" SetFocusOnError="True" Display="Dynamic" />
                <br />
                Name:<br />
                <asp:TextBox ID="txtName" runat="server" Width="155px" ValidationGroup="ValidateUser"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvfName" runat="server" ControlToValidate="txtName" ValidationGroup="ValidateUser" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                <br />
                Last 4 digits of SSN or Tax ID:<br />
                <asp:TextBox ID="txtSSN" runat="server" Width="40px" MaxLength="4" ValidationGroup="ValidateUser"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSSN" runat="server" ControlToValidate="txtSSN" ValidationGroup="ValidateUser" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                <br /><br />
                <div style="">
                    <asp:LinkButton ID="lbLookup" runat="server" CssClass="ovalbutton" ValidationGroup="ValidateUser" CausesValidation="true" OnClick="lbLookup_Click"><span>Continue</span></asp:LinkButton>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlCreateUser" runat="server" Visible="false">
                <table cellpadding="0" cellspacing="3" width="100%">
                    <tr><td colspan="2"><div style="width:330px;background-color:#36587a;color:White;font-weight:bold;text-align:center;">Create Your New Account</div></td></tr>
                    <tr>
                        <td align="right" style="width:125px">User Name:&nbsp;</td>
                        <td>
                            <asp:TextBox ID="UserName" runat="server" Width="200px" ValidationGroup="CreateUser" />
                            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="UserName" ValidationGroup="CreateUser" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr style="clear:both;"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">Passwords are required to be at least 8 characters long including one special character (for example: john_123).</td>
                    </tr>
                    <tr>
                        <td align="right">Password:&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="200px" ValidationGroup="CreateUser" />
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="Password" ValidationGroup="CreateUser" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="CreateUser" runat="server" ControlToValidate="Password" Display="Dynamic" 
                                ErrorMessage="Passwords are required to be at least 8 characters long including one special character (for example: john_123)." ValidationExpression="(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Confirm Password:&nbsp;</td>
                        <td>
                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Width="200px" ValidationGroup="CreateUser" />
                            <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="ConfirmPassword" ValidationGroup="CreateUser" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                            <asp:CompareValidator ID="cvPassword" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" ValidationGroup="CreateUser" ErrorMessage="Passwords do not match" SetFocusOnError="True" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr style="clear:both;"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">Valid email required. The email address you enter will be used for retrieval of account information.</td>
                    </tr>
                    <tr>
                        <td align="right">E-mail:&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Email" runat="server" Width="200px" ValidationGroup="CreateUser" />
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="Email" ValidationGroup="CreateUser" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="Email" ErrorMessage="invalid email format" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="CreateUser" />
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><asp:LinkButton ID="lbCreate" runat="server" CssClass="ovalbutton" ValidationGroup="CreateUser" CausesValidation="true" OnClick="lbCreate_Click"><span>Create Account</span></asp:LinkButton></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
                <br /><br />
                <div style="text-align:center">
                    <b>Account successfully created</b><br /><br />
                    <a href="~/CustomerService/MyAccount/Default.aspx" runat="server">Continue</a>
                </div>
            </asp:Panel>

            <br /><br />
            <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>


     
    <script type="text/javascript">
        jQuery("#gallery a").slimbox();
    </script>
</asp:Content>
