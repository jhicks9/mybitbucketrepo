﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Xml.Linq
Imports System.Linq

Partial Class Admin_Modules
    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Try
            Dim lblPageHeader As Label = Page.Master.FindControl("lblPageHeader")
            lblPageHeader.Text = "Module Maintenance"
            lblPageHeader.Visible = True

            Dim ph As ContentPlaceHolder = Page.Master.FindControl("LeftPlaceHolder")
            Dim sb As UserControl = ph.FindControl("sidebar1")
            Dim pic As Image = sb.FindControl("imgSide")
            Dim txt As Label = sb.FindControl("txtNavMenu")
            txt.Visible = False
            pic.ImageUrl = ResolveUrl("~/images/outageleft.jpg")
            pic.Visible = True
        Catch
        End Try
    End Sub

    Protected Sub gvCenterModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCenterModules.RowCommand
        Select Case e.CommandName
            Case "Select"
        End Select
    End Sub

    Protected Sub gvStaticModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStaticModules.RowCommand
        Select Case e.CommandName
            Case "Select"
        End Select
    End Sub

    Protected Sub buildModuleXML_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim abyt() As Byte = Nothing
        Try
            lblStatus.Text = ""
            Dim sql As String = "select module_id,module_name,module_link,module_filename,module_image from modules where module_visible = 'True' and module_columnonpage = 'Middle' order by module_order"
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                Dim xml_text_writer As New XmlTextWriter(System.IO.Path.GetTempPath & "modules.xml", System.Text.Encoding.UTF8)
                xml_text_writer.Formatting = Formatting.Indented
                xml_text_writer.Indentation = 4
                xml_text_writer.WriteStartDocument(True)
                xml_text_writer.WriteStartElement("ActiveModuleInfo")
                While dr.Read
                    If System.IO.File.Exists(Server.MapPath("~/images/module/") & dr("module_filename").ToString) Then  'only create xml to display the image IF the image exists
                        xml_text_writer.WriteStartElement("module")
                        xml_text_writer.WriteStartElement("id")
                        xml_text_writer.WriteString(dr("module_id").ToString)
                        xml_text_writer.WriteEndElement() 'id
                        xml_text_writer.WriteStartElement("filename")
                        xml_text_writer.WriteString(dr("module_filename").ToString)
                        xml_text_writer.WriteEndElement() 'filename
                        xml_text_writer.WriteStartElement("link")
                        xml_text_writer.WriteString(dr("module_link").ToString)
                        xml_text_writer.WriteEndElement() 'link
                        xml_text_writer.WriteStartElement("name")
                        xml_text_writer.WriteString(dr("module_name").ToString)
                        xml_text_writer.WriteEndElement() 'name for alt tag
                        xml_text_writer.WriteEndElement() 'module
                    End If

                    'extract the module image to the ~/images/module folder
                    'If System.IO.File.Exists(Server.MapPath("~/images/module/") & dr("module_filename").ToString) Then  'remove existing file if it exists
                    '    System.IO.File.Delete(Server.MapPath("~/images/module/") & dr("module_filename").ToString)
                    'End If
                    'Dim fs As New IO.FileStream(Server.MapPath("~/images/module/") & dr("module_filename").ToString, IO.FileMode.CreateNew)
                    'Dim bw As New IO.BinaryWriter(fs)
                    'abyt = dr("module_image")
                    'bw.Write(abyt)
                    'bw.Close()

                End While
                xml_text_writer.WriteEndElement() 'ActiveModuleInfo
                xml_text_writer.WriteEndDocument()
                xml_text_writer.Flush()
                xml_text_writer.Close()
            End If
            dr.Close()
            conn.Close()
            lblStatus.Text = "Module structure built"
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            lblStatus.Text = "Error building module structure"
        End Try
    End Sub
End Class
