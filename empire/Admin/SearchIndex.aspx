﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="SearchIndex.aspx.vb" Inherits="Admin_SearchIndex" %>

<asp:Content ID="Content4" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
        <asp:Repeater id="rptSearchIndex" runat="server">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>
                Index build:&nbsp;<%# DataBinder.Eval(Container.DataItem, "date")%>
                <asp:LinkButton ID="lbCrawlResults" runat="server" Text='Results' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "id")%>' CommandName="Select" />
            </ItemTemplate>
            <SeparatorTemplate><br/ ><br /></SeparatorTemplate>
        </asp:Repeater>
        <br /><br />
        <asp:LinkButton ID="lbRebuild" runat="server" Text='Rebuild' CommandName="Select" OnClientClick="if(!confirm('This will rebuild the search index. Continue?'))return false;" />&nbsp;index
</asp:Content>

