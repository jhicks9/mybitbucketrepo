﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Admin_SearchIndex
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            getResultData()
        End If
    End Sub

    Public Sub getResultData()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim sr As New SearchResults
        Dim cmd As New SqlDataAdapter("SELECT search.id,search.date from search where search.controlrecord = 1 and search.title = '-search results-'", conn)
        Dim dsSearch As New System.Data.DataSet()
        Try
            conn.Open()
            cmd.Fill(dsSearch)
            conn.Close()
            rptSearchIndex.DataSource = dsSearch
            rptSearchIndex.DataBind()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub rptSearchIndex_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSearchIndex.ItemCommand
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim sr As New SearchResults
        Dim cmd As New SqlCommand("SELECT search.html from search where search.id = '" & e.CommandArgument & "'", conn)
        Dim drLookup As SqlDataReader
        Dim html As String = ""
        Try
            conn.Open()
            drLookup = cmd.ExecuteReader  'check for control record
            If drLookup.HasRows Then
                While drLookup.Read
                    html = drLookup("html").ToString
                End While
            End If
            conn.Close()
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "crawlresults.html"))
            HttpContext.Current.Response.ContentType = "text/HTML"
            '  render the htmlwriter into the response
            HttpContext.Current.Response.Write(html.ToString)
            HttpContext.Current.Response.End()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub lbRebuild_Click(sender As Object, e As System.EventArgs) Handles lbRebuild.Click
        'Dim files() As String = IO.Directory.GetFiles(System.IO.Path.GetTempPath() & "webIndex")  'remove physical index files
        'For Each file As String In files
        '    Try
        '        IO.File.Delete(file)
        '    Catch ex As Exception
        '        If TypeOf ex Is IOException AndAlso IsFileLocked(ex) Then ' do something
        '        Else 'otherwise we just ignore it.  we will pick it up on the next pass
        '        End If
        '    End Try
        'Next
        SearchResults.createSearchIndex()
        getResultData()
    End Sub

    Private Shared Function IsFileLocked(exception As Exception) As Boolean
        Dim errorCode As Integer = Runtime.InteropServices.Marshal.GetHRForException(exception) And ((1 << 16) - 1)
        Return errorCode = 32 OrElse errorCode = 33
    End Function

End Class
