﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Modules.aspx.vb" Inherits="Admin_Modules" Title="Manage Modules" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" >
       <asp:View ID="tabModules" runat="server" >
            <div>Center Modules</div>
            <asp:GridView ID="gvCenterModules" runat="server" DataSourceID="sqlDSCenterModules" DataKeyNames="module_id" skinid="GridView" AutoGenerateColumns="false" OnRowCommand="gvCenterModules_RowCommand">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" ToolTip="Edit module" CommandName="Edit" CommandArgument="EditRecord" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                          <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="true" Text="Update"></asp:LinkButton>&nbsp;
                          <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel"></asp:LinkButton>&nbsp;
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                    <asp:BoundField DataField="Module_ID" HeaderText="Module_ID" Visible="False" ReadOnly="True" />
                    <asp:BoundField DataField="Module_Image" HeaderText="Module_Image" Visible="False" />
                    <asp:CheckBoxField DataField="module_visible" HeaderText="Visible" SortExpression="Module_Visible" />
                    <asp:BoundField DataField="Module_Order" HeaderText="Order" SortExpression="Module_Order" />
                    <asp:TemplateField HeaderText="Name" SortExpression="Module_Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCMName" runat="server" Text='<%# Bind("Module_Name") %>'></asp:TextBox>
                            <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvCMName" runat="server" ControlToValidate="txtCMName"
                            Display="Dynamic" ErrorMessage="Name is required." SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("Module_Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Link" SortExpression="Module_Link">
                        <ItemTemplate>
                            <asp:Label ID="lblCMLink" runat="server" Text='<%# Bind("Module_Link") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:templatefield headertext="Image" >
                      <ItemTemplate>
                          <asp:HyperLink ID="hlViewImage" runat="server" Target="_blank" NavigateUrl='<%# Eval("module_id", "~/DisplayBinary.aspx?dtype=modules&id={0}") %>'>View Image</asp:HyperLink>
                      </ItemTemplate>
                    </asp:templatefield>
                </Columns>
                <EmptyDataTemplate>
                
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:LinkButton ID="BuildModuleXML" runat="server" CssClass="ovalbutton" CausesValidation="false" OnClick="BuildModuleXML_Click"><span>Build Modules</span></asp:LinkButton>
            <br />
            <asp:Label ID="lblStatus" runat="server" ForeColor="Red" Text=""></asp:Label>
            <br />
            <hr style="color:#113962;height:1px;"/>            
            
            <div>Static Modules</div>
            <asp:GridView ID="gvStaticModules" runat="server" DataSourceID="sqlDSStaticModules" DataKeyNames="module_id" skinid="GridView" AutoGenerateColumns="false" OnRowCommand="gvStaticModules_RowCommand">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" ToolTip="Edit module" CommandName="Edit" CommandArgument="EditRecord" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                          <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="true" Text="Update"></asp:LinkButton>&nbsp;
                          <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel"></asp:LinkButton>&nbsp;
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                    <asp:BoundField DataField="Module_ID" HeaderText="Module_ID" Visible="False" ReadOnly="True" />
                    <asp:BoundField DataField="Module_Image" HeaderText="Module_Image" Visible="False" />
                    <asp:CheckBoxField DataField="module_visible" HeaderText="Visible" SortExpression="Module_Visible" />
                    <asp:TemplateField HeaderText="Name" SortExpression="Module_Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("Module_Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Link" SortExpression="Module_Link">
                        <ItemTemplate>
                            <asp:Label ID="lblSMLink" runat="server" Text='<%# Bind("Module_Link") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:templatefield headertext="Image" >
                      <ItemTemplate>
                          <asp:HyperLink ID="hlViewImage" runat="server" Target="_blank" NavigateUrl='<%# Eval("module_id", "~/DisplayBinary.aspx?dtype=modules&id={0}") %>'>View Image</asp:HyperLink>
                      </ItemTemplate>
                    </asp:templatefield>
                </Columns>
                <EmptyDataTemplate>
                    <asp:LinkButton ID="lbAddEmpty" runat="server" ToolTip="Add new module" CausesValidation="false" CommandArgument="AddRecord" CommandName="AddRecord" Text="Add"></asp:LinkButton>
                </EmptyDataTemplate>           
           </asp:GridView>
       </asp:View>       
    </asp:MultiView>                                                            
        
    <asp:SqlDataSource ID="sqlDSCenterModules" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select module_id,module_name,module_image,module_visible,module_link,module_order from modules where module_columnonpage = 'Middle' order by module_order,module_name"
        UpdateCommand="update modules SET module_name=@module_name,module_visible=@module_visible,module_order=@module_order where module_id = @module_id">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="sqlDSStaticModules" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select module_id,module_name,module_image,module_visible,module_link from modules where module_columnonpage = 'Static' order by module_name"
        UpdateCommand="update modules SET module_visible=@module_visible where module_id = @module_id">
    </asp:SqlDataSource>    
</asp:Content>

