﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ajaxModalPopup.ascx.vb" Inherits="ajaxModalPopup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Panel ID="pnlProgress" runat="server" style="background-color:#f5f5f5;display:none;width:400px">
    <div style="padding:8px">
        <table border="0" cellpadding="2" cellspacing="0" style="width:100%">
            <tr>
                <td style="width:50%"></td>
                <td style="text-align:right">
                    <img id="img1" src="~/images/slimbox2-loader.gif" style="background-color:transparent;" runat="server" alt="loader" />
                </td>
                <td style="text-align:left;white-space:nowrap">
                    <span style="font-size:larger">Loading, Please wait ...</span>
                </td>
                <td style="width:50%"></td>
            </tr>
        </table>
    </div>
</asp:Panel>
<ajaxToolKit:ModalPopupExtender ID="mpeProgress" runat="server" TargetControlID="pnlProgress" PopupControlID="pnlProgress" BackgroundCssClass="modalBackground" DropShadow="false" />

<script type="text/javascript">
    Sys.Net.WebRequestManager.add_invokingRequest(onInvoke);
    Sys.Net.WebRequestManager.add_completedRequest(onComplete);

    function onInvoke(sender, args) {
        $find('<%= mpeProgress.ClientID %>').show();
    }

    function onComplete(sender, args) {
        $find('<%= mpeProgress.ClientID %>').hide();
    }

    function pageUnload() {
        Sys.Net.WebRequestManager.remove_invokingRequest(onInvoke);
        Sys.Net.WebRequestManager.remove_completedRequest(onComplete);
    }
</script>