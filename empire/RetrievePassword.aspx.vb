Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Partial Class RetrievePassword
    Inherits System.Web.UI.Page
    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES
    Dim customerid As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim autotab As HtmlGenericControl = New HtmlGenericControl
        autotab.Attributes.Add("type", "text/javascript")
        autotab.TagName = "script"
        autotab.Attributes.Add("src", ResolveUrl("~/js/autotab.js"))
        Page.Header.Controls.Add(autotab)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub lbAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Threading.Thread.Sleep(1000)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader

        Try
            'Dim Pi As ProfileInfo
            Dim strEmail As String = ""
            lblError.Text = ""
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Textbox1.Text)
            Dim strBillPkg As String = Trim(Textbox2.Text)
            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            Dim status As Integer = cwservice.ValidateCustomer(encrypt, strBillPkg, txtName.Text, txtSSN.Text)

            If status = 1 Then
                Dim UserName As String = ""  'Check if account exists
                conn.Open()
                cmd.CommandText = "select aspnet_Users.UserName AS result from aspnet_Users inner join aspnet_Membership on aspnet_Users.UserId = aspnet_Membership.UserId inner join aspnet_Profile ON aspnet_Users.UserId = aspnet_Profile.UserId where (patindex('" & Textbox1.Text & strBillPkg & "', aspnet_Profile.PropertyValuesString) > 0)"
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        UserName = dr("result").ToString
                    End While
                End If
                dr.Close()
                conn.Close()
                If Not String.IsNullOrEmpty(UserName) Then 'account created so attempt password change
                    If txtNewPassword.Text = txtConfirmPassword.Text Then
                        If txtNewPassword.Text = UserName Then
                            lblError.Text = "Password cannot be the same as your User Name"
                            lblError.ForeColor = Drawing.Color.Red
                            lblError.Visible = True
                        Else
                            strEmail = Membership.GetUser(UserName).Email
                            lblMessage.Visible = True
                            Dim userPassword As New MembershipUser("empireMembershipProvider", UserName, Nothing, strEmail, Nothing, Nothing, True, False, Nothing, Nothing, Nothing, Nothing, Nothing)

                            Dim userInfo As MembershipUser = Membership.GetUser(userPassword.UserName.ToString)
                            If userInfo.IsLockedOut Then  'if account is locked out
                                If Now > userInfo.LastLockoutDate.AddMinutes(30) Then  'check if 30 minutes passed
                                    userInfo.UnlockUser()  'if so, unlock account and change password
                                    userPassword.ChangePassword(userPassword.GetPassword.ToString, txtNewPassword.Text)
                                    pnlInput.Visible = False
                                    pnlEmailInput.Visible = False
                                    pnlDisplay.Visible = True
                                Else
                                    lblError.ForeColor = Drawing.Color.Red  'else display error message
                                    lblError.Text = "Your account has been temporarily locked out because of a maximum number of incorrect login attempts. Please try again in 30 minutes."
                                End If
                            Else
                                userPassword.ChangePassword(userPassword.GetPassword.ToString, txtNewPassword.Text)
                                pnlInput.Visible = False
                                pnlEmailInput.Visible = False
                                pnlDisplay.Visible = True
                            End If
                        End If
                    Else
                        lblError.ForeColor = Drawing.Color.Red
                        lblMessage.Text = "New Password and Confirm Passwords do not match"
                    End If
                Else 'account valid in customer watch but not created on web
                    lblError.Text = "You must register your account before changing a password."
                    lblError.ForeColor = Drawing.Color.Red
                    lblError.Visible = True
                End If
                'End If
                'Next
            ElseIf status = 0 Then
                lblError.ForeColor = Drawing.Color.Red
                lblError.Text = "Cannot find user. Please verify account information."
                lblError.Visible = True
            ElseIf status = 2 Then
                lblError.ForeColor = Drawing.Color.Red
                lblError.Text = "Unable to validate account. Please contact Customer Service at 800-206-2300."
                lblError.Visible = True
            End If

        Catch
            lblMessage.Font.Bold = False
            lblMessage.ForeColor = Drawing.Color.Red
            If Err.Description = "Non alpha numeric characters in 'newPassword' needs to be greater than or equal to '1'." Then
                lblMessage.Text = "Passwords are required to be at least 8 characters long including one special character (for example: john_123)"
            ElseIf Err.Description = "The length of parameter 'newPassword' needs to be greater or equal to '8'." Then
                lblMessage.Text = "Passwords are required to be at least 8 characters long including one special character (for example: john_123)"
            Else
                lblError.Text = Err.Description
                lblError.ForeColor = Drawing.Color.Red
                lblMessage.Text = ""
            End If

        End Try
    End Sub

    Protected Sub lbEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim username As String = ""
            username = Membership.GetUserNameByEmail(txtEmail.Text)
            If Len(username) > 0 Then  'valid email address found, send email
                Dim MailObj As New System.Net.Mail.SmtpClient
                Dim message As New System.Net.Mail.MailMessage()
                Dim strEmailBody As String = "We received your request for a MyPassword reset from " & Context.Request.ServerVariables("REMOTE_ADDR") & " on " & Now.ToString & ".  For security reasons, we are unable to provide your old password.  However, you may select a new password by following the directions below." & vbCrLf & vbCrLf
                strEmailBody = strEmailBody & "**IMPORTANT! If you did not request a MyPassword reset, delete this email. Only follow the instructions if you need to reset your MyPassword." & vbCrLf & vbCrLf
                strEmailBody = strEmailBody & "To reset your MyPassword, simply click on the following link:" & vbCrLf
                strEmailBody = strEmailBody & Context.Request.Url.Scheme & "://" + Context.Request.Url.Host & ResolveUrl("~/RetrievePasswordEmail.aspx") & "?u=" & Server.UrlEncode(tdes.Encrypt(username)) & "&e=" & Server.UrlEncode(tdes.Encrypt(txtEmail.Text))
                Dim fromAddress As New System.Net.Mail.MailAddress("customer.service@empiredistrict.com")
                message.From = fromAddress
                message.To.Add(txtEmail.Text)
                message.Subject = "MyPassword Reset"
                message.Body = strEmailBody
                'MailObj.Host = "" -- obtained from web.config 03/14/2012
                MailObj.Send(message)
            End If
            pnlInput.Visible = False
            pnlEmailInput.Visible = False
            lblEmailSent.Text = txtEmail.Text
            pnlEmailSent.Visible = True
        Catch
            pnlInput.Visible = False
            pnlEmailInput.Visible = False
        End Try
    End Sub

    Protected Sub lbUserName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Threading.Thread.Sleep(1000)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader

        Try
            Dim strBillPkg As String = Trim(txtUserNameBP.Text)
            Dim strUserName As String = ""

            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            If txtUserNameAN.Text = "" Or txtUserNameBP.Text = "" Or txtUserNameCD.Text = "" Then
                lblMessage.Text = "All or part of the account number is empty."
                lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Visible = True
            Else
                Dim UserName As String = ""  'Check if account exists
                conn.Open()
                cmd.CommandText = "select aspnet_Users.UserName AS result from aspnet_Users inner join aspnet_Membership on aspnet_Users.UserId = aspnet_Membership.UserId inner join aspnet_Profile ON aspnet_Users.UserId = aspnet_Profile.UserId where (patindex('" & txtUserNameAN.Text & strBillPkg & "', aspnet_Profile.PropertyValuesString) > 0)"
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        UserName = dr("result").ToString
                    End While
                End If
                dr.Close()
                conn.Close()

                If Not String.IsNullOrEmpty(UserName) Then 'account found
                    lblUserNameMessage.Visible = False
                    pnlUserNameInput.Visible = False
                    pnlUserNameRecovered.Visible = True
                    lblShowUserName.Text = UserName
                Else
                    lblUserNameMessage.Text = "User Name not found for account number"
                    lblUserNameMessage.ForeColor = Drawing.Color.Red
                    lblUserNameMessage.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If rbAN.Checked = True Then
            pnlSelect.Visible = False
            pnlInput.Visible = True
        ElseIf rbEmail.Checked = True Then
            pnlSelect.Visible = False
            pnlEmailInput.Visible = True
        ElseIf rbUN.Checked = True Then
            pnlSelect.Visible = False
            pnlUserNameInput.Visible = True
        Else
            MsgBox("Please select a method.", MsgBoxStyle.OkOnly, "Selection Required")
        End If
    End Sub

    Protected Sub lbReselect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlInput.Visible = False
        pnlEmailInput.Visible = False
        pnlUserNameInput.Visible = False
        pnlDisplay.Visible = False
        pnlEmailSent.Visible = False
        pnlUserNameRecovered.Visible = False
        pnlSelect.Visible = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            If Request.QueryString("id") = "name" Then
                pnlSelect.Visible = False
                pnlUserNameInput.Visible = True
            End If

            Textbox1.Attributes("onkeypress") = "return recPreKey(event, this)"
            Textbox1.Attributes("onkeyup") = "onFull(event, this)"
            Textbox2.Attributes("onkeypress") = "return recPreKey(event, this)"
            Textbox2.Attributes("onkeyup") = "onFull(event, this)"
            TextBox3.Attributes("onkeypress") = "return recPreKey(event, this)"
            TextBox3.Attributes("onkeyup") = "onFull(event, this)"
            txtSSN.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtSSN.Attributes("onkeyup") = "onFull(event, this)"


            txtUserNameAN.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtUserNameAN.Attributes("onkeyup") = "onFull(event, this)"
            txtUserNameBP.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtUserNameBP.Attributes("onkeyup") = "onFull(event, this)"
            txtUserNameCD.Attributes("onkeypress") = "return recPreKey(event, this)"
            txtUserNameCD.Attributes("onkeyup") = "onFull(event, this," & lbUserName.ClientID & ")"
        End If
    End Sub

End Class