﻿
Partial Class empiremaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header
            Dim current As SiteMapNode = SiteMap.CurrentNode  'disable sitemappath if page is NOT in the sitemap
            If current Is Nothing Then
                SiteMapPath.Visible = False
            End If
        Catch
        End Try
    End Sub
End Class

