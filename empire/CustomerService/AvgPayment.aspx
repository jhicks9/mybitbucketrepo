<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AvgPayment.aspx.vb" Inherits="CustomerService_AvgPayment" title="Average Payment" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <b>Join the Average Payment Plan because you don't need surprises.</b>
    <br /><br /><img id="Img1" src="~/images/payments-appgraph.gif" runat="server" alt="graph" style="float:right;"/>
    Hot summers and cold winters can cause significant swings in your energy bill and problems for your budget. With our Average Payment Plan (APP), you won't have to deal with surprises when your bill arrives.
    <br /><br />
    Here's how it works. APP calculates your expected annual service charges and divides it into equal payments.  At the end of the contract year, the actual usage is renewed and a customer's contract and installment amount are adjusted for the next 12 months.  Participation in APP requires customers pay their pre-determined average amount every month � even if the status shown on their contract information is a credit. Customers who fail to make monthly installment payments are subject to have their APP contract terminated. Customers who wish to pay more than their monthly installment must contact Empire before doing so.
    <br /><br />
    When you select APP, you can choose an alternate compatible due date.&nbsp;&nbsp;
    <asp:HyperLink ID="HyperLink1" runat="server" Text="Auto-Pay" NavigateUrl="~/CustomerService/AutoPay.aspx" />
    is another available service from Empire District.
    <br /><br />
    To take advantage of APP, log in to <a href='<%= ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx") %>' >MyAccount</a> and complete the enrollment form.
    <br /><br />
    <b><i>To enroll in APP, account must be in good standing with no balance due.</i></b>
    <br /><br />
</asp:Content>

