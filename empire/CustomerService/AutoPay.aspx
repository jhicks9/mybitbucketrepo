<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AutoPay.aspx.vb" Inherits="CustomerService_AutoPay" title="Auto Pay" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <img id="Img1" src="~/images/payments-autopay.jpg" runat="server" alt="graph" style="float:right;"/>
    When you choose Auto-Pay, your payment is automatically deducted from your bank account. You still receive your bill that will show , in advance, the exact amount and date that your bank account will be debited. Payment will be verified each month on your bank statement. If you wish to cancel Auto-Pay, just contact Empire District.
    <br /><br />
    To take advantage of Auto-Pay,  log in to <a href='<%= ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx") %>' >MyAccount</a> and complete the enrollment form.
</asp:Content>

