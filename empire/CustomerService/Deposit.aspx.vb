
Partial Class CustomerService_Deposit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If String.IsNullOrEmpty(Request.QueryString("deposit")) Then
                'If no variable is pass default to MO
                pnlMO.Visible = True
                pnlAR.Visible = False
                pnlKS.Visible = False
                pnlOK.Visible = False
                lbMO.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "deposit=AR" Then
                pnlMO.Visible = False
                pnlAR.Visible = True
                pnlKS.Visible = False
                pnlOK.Visible = False
                lbAR.Font.Bold = True
                lbMO.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "deposit=KS" Then
                pnlMO.Visible = False
                pnlAR.Visible = False
                pnlKS.Visible = True
                pnlOK.Visible = False
                lbKS.Font.Bold = True
                lbAR.Font.Bold = False
                lbMO.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "deposit=OK" Then
                pnlMO.Visible = False
                pnlAR.Visible = False
                pnlKS.Visible = False
                pnlOK.Visible = True
                lbOK.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbMO.Font.Bold = False
            End If
        Catch
        End Try
    End Sub
End Class
