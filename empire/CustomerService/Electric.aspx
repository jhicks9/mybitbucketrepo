﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Electric.aspx.vb" Inherits="CustomerService_Electric" Title="Electric Rates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
            <asp:LinkButton ID="lbAR" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Electric.aspx?electric=AR" Text="Arkansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbKS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Electric.aspx?electric=KS" Text="Kansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbMO" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Electric.aspx" Text="Missouri"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbOK" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Electric.aspx?electric=OK" Text="Oklahoma"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbFED" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Electric.aspx?electric=FED" Text="Federal"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbSS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Electric.aspx?electric=SS" Text="Service Standards"/>
            <br />&nbsp
                
            <asp:Panel ID="pnlState" runat="server" HeaderText="Electric">
                    <b>Bill Explanation</b><br /><br />
                    <asp:DataList ID="dlBillExplanation" runat="server" SkinID="DataList" DataSourceID="sqlBE">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' 
                                    Target="_blank" runat="server"/> 
                        </itemtemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="sqlBE" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = @doctypeid ORDER BY documents.webdescription">
                        <SelectParameters>
                            <asp:Parameter Type="Int32" Name="doctypeid" DefaultValue="108" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br /><hr style="color:#113962;height:1px;"/>

                    <b>Residential Service Rates</b><br /><br />
                    <asp:DataList ID="dlResidential" runat="server" SkinID="DataList" DataSourceID="sqlResidential">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="sqlResidential" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = @doctypeid ORDER BY documents.webdescription">
                        <SelectParameters>
                            <asp:Parameter Type="Int32" Name="doctypeid" DefaultValue="77" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br /><hr style="color:#113962;height:1px;"/>

                    <b>Commercial Service Rates</b>
                    <br /><br />
                    <asp:DataList ID="dlCommercial" runat="server" SkinID="DataList" DataSourceID="sqlCommercial">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                    </asp:DataList>                                                      
                    <asp:SqlDataSource ID="sqlCommercial" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = @doctypeid ORDER BY documents.webdescription">
                        <SelectParameters>
                            <asp:Parameter Type="Int32" Name="doctypeid" DefaultValue="78" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br /><hr style="color:#113962;height:1px;"/>

                    <b>Industrial Service Rates</b>
                    <br /><br />
                    <asp:DataList ID="dlIndustrial" runat="server" SkinID="DataList" DataSourceID="sqlIndustrial">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                    </asp:DataList>                                                      
                    <asp:SqlDataSource ID="sqlIndustrial" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = @doctypeid ORDER BY documents.webdescription">
                        <SelectParameters>
                            <asp:Parameter Type="Int32" Name="doctypeid" DefaultValue="79" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </asp:Panel><!--end pnlState-->

            <asp:Panel ID="pnlFederal" runat="server" HeaderText="Federal" Visible="false"> 
                    <b>Transmission</b><br /><br />
                    <asp:DataList ID="dlFederal" runat="server" SkinID="DataList" DataSourceID="sqlFederal">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="sqlFederal" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 87 ORDER BY documents.webdescription">
                    </asp:SqlDataSource>
                    <br /><hr style="color:#113962;height:1px;"/>
                        
                    <b>FERC Standard of Conduct</b><br /><br />
                    <asp:DataList ID="dlFERC" runat="server" SkinID="DataList" DataSourceID="sqlFERC">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="sqlFERC" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 104 ORDER BY documents.webdescription">
                    </asp:SqlDataSource>
            </asp:Panel><!--end pnlFederal-->
                
                <asp:Panel ID="pnlSS" runat="server" HeaderText="Service Standards" Visible="false">
                        <b>Service Standards</b><br /><br />
                        <asp:DataList ID="dlSS" runat="server" SkinID="DataList" DataSourceID="sqlSS">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlSS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 70 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                </asp:Panel><%--end pnlSS--%>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

