﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Brochures.aspx.vb" Inherits="CustomerService_Brochures" Title="Brochures" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <asp:DataList ID="dlBrochures" runat="server" SkinID="DataList" DataSourceID="sqlBrochures">
        <itemtemplate>
            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                   NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' 
                   Target="_blank" runat="server"/> 
        </itemtemplate>
    </asp:DataList>
                        
    <asp:SqlDataSource ID="sqlBrochures" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 119 ORDER BY documents.webdescription">
    </asp:SqlDataSource>
</asp:Content>

