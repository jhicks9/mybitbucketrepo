﻿<%@ Page Title="Service Connect Residential" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ServiceConnectResidential.aspx.vb" Inherits="CustomerService_ServiceConnectResidential" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div style='float:right;width:350px;margin:0px 0 0 5px;'>
                <img id='imgServiceConnect' src='~/images/customerservice_serviceconnect.jpg' runat='server' alt='' />
            </div>
            <div>
                <b>Thank you for using Empire District's online application.</b><br />
                <br />
                We will respond to your request by telephone or email within two business days of receipt.  An account representative may need to verify the service address, schedule a date for a service person to meet you at the property, and discuss any other account requirements.<br />
                <br />
                Twenty-four hour notice, excluding weekends and holidays, is required for service requests.  Please note, it may take longer than 24 hours to have your service connected.<br />
                <br />
                A deposit may be required before service can be turned on.<br />
                <br />
                By requesting service, you allow Empire to check credit information and verify the information provided.<br />
                <br />
                A handbook with important details for customers including contact numbers and payment information is available by clicking <asp:HyperLink ID="hlHandbook" runat="server" NavigateUrl ="~/CustomerService/Brochures.aspx">here</asp:HyperLink>.  We encourage customers to print this handbook for an easy-to-use tool.<br />
                <br />
                Do not use this form for emergency requests.  If you need assistance in filling out this application, please contact Empire at 800-206-2300 for electric service or 800-424-0427 for natural gas service.<br />
                <br />
            </div>
            <div style="clear:both;"></div>
            
            <asp:Panel ID="pnlService" runat="server" Visible="true">
                <font style="color:#113962"><b>Service</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0" >
                        <tr>
                            <td align="right"><asp:label ID="lblCounty" Text="* County:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList ID="ddlCounty" ValidationGroup="avg" runat="server" AutoPostBack="true" >
                                    <asp:ListItem Value="" Text="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="01-Gas" Text="Andrew"></asp:ListItem>
                                    <asp:ListItem Value="02-Gas" Text="Atchison"></asp:ListItem>
                                    <asp:ListItem Value="01-Electric" Text="Barry"></asp:ListItem>
                                    <asp:ListItem Value="02-Electric" Text="Barton"></asp:ListItem>
                                    <asp:ListItem Value="03-Electric" Text="Benton"></asp:ListItem>
                                    <asp:ListItem Value="04-Electric" Text="Cedar"></asp:ListItem>
                                    <asp:ListItem Value="03-Gas" Text="Chariton"></asp:ListItem>
                                    <asp:ListItem Value="05-Electric" Text="Cherokee"></asp:ListItem>
                                    <asp:ListItem Value="06-Electric" Text="Christian"></asp:ListItem>
                                    <asp:ListItem Value="04-Gas" Text="Cooper"></asp:ListItem>
                                    <asp:ListItem Value="07-Electric" Text="Craig"></asp:ListItem>
                                    <asp:ListItem Value="08-Electric" Text="Dade"></asp:ListItem>
                                    <asp:ListItem Value="09-Electric" Text="Dallas"></asp:ListItem>
                                    <asp:ListItem Value="10-Electric" Text="Delaware"></asp:ListItem>
                                    <asp:ListItem Value="11-Electric" Text="Greene"></asp:ListItem>
                                    <asp:ListItem Value="05-Gas" Text="Grundy"></asp:ListItem>
                                    <asp:ListItem Value="06-Gas" Text="Henry"></asp:ListItem>
                                    <asp:ListItem Value="12-Electric" Text="Hickory"></asp:ListItem>
                                    <asp:ListItem Value="07-Gas" Text="Holt"></asp:ListItem>
                                    <asp:ListItem Value="08-Gas" Text="Howard"></asp:ListItem>
                                    <asp:ListItem Value="13-Electric" Text="Jasper"></asp:ListItem>
                                    <asp:ListItem Value="09Gas" Text="Johnson"></asp:ListItem>                                
                                    <asp:ListItem Value="10Gas" Text="Lafayette"></asp:ListItem>
                                    <asp:ListItem Value="14-Electric" Text="Lawrence"></asp:ListItem>
                                    <asp:ListItem Value="11-Gas" Text="Linn"></asp:ListItem>
                                    <asp:ListItem Value="12-Gas" Text="Livingston"></asp:ListItem>
                                    <asp:ListItem Value="15-Electric" Text="McDonald"></asp:ListItem>
                                    <asp:ListItem Value="16-Electric" Text="Newton"></asp:ListItem>
                                    <asp:ListItem Value="13-Gas" Text="Nodaway"></asp:ListItem>
                                    <asp:ListItem Value="17-Electric" Text="Ottowa"></asp:ListItem>
                                    <asp:ListItem Value="14-Gas" Text="Pettis"></asp:ListItem>
                                    <asp:ListItem Value="15-Gas" Text="Platte"></asp:ListItem>
                                    <asp:ListItem Value="18-Electric" Text="Polk"></asp:ListItem>
                                    <asp:ListItem Value="16-Gas" Text="Ray"></asp:ListItem>
                                    <asp:ListItem Value="17-Gas" Text="Saline"></asp:ListItem>
                                    <asp:ListItem Value="19-Electric" Text="St. Clair"></asp:ListItem>
                                    <asp:ListItem Value="20-Electric" Text="Stone"></asp:ListItem>
                                    <asp:ListItem Value="21-Electric" Text="Taney"></asp:ListItem>
                                    <asp:ListItem Value="18-Gas" Text="Vernon"></asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;<a id="A1" href="~/About/ServiceMap.aspx" runat="server" target="_blank" >see service area</a>
                                <asp:RequiredFieldValidator ID="rvfCounty" runat="server" ControlToValidate="ddlCounty" ErrorMessage="County required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:HiddenField ID="hdnServiceType" runat="server" Value="" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>    
                                <asp:RadioButtonList ID="rblService" runat="server" RepeatDirection="Horizontal" Visible="false" AutoPostBack="true" >
                                    <asp:ListItem Value="Electric" Text="Electric Service"></asp:ListItem>
                                    <asp:ListItem Value="Electric and Water" Text="Electric and Water Service"></asp:ListItem>
                                    <asp:ListItem Value="Water" Text="Water Service"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvService" runat="server" ControlToValidate="rblService" ErrorMessage="Service required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td><asp:LinkButton ID="lbCustomerInfoNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbCustomerInfoNext_Click"><span>Next</span></asp:LinkButton></td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
            
            <asp:Panel ID="pnlCustomerInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>Customer Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right"><asp:label ID="lblFirstName" Text="* First Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblMiddleName" Text="* Middle Name or Initial:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtMiddleName" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvMiddleName" runat="server" ControlToValidate="txtMiddleName" ErrorMessage="Middle name/initial required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastName" Text="* Last Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastName" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSuffix" Text="Suffix:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList ID="cboSuffix" ValidationGroup="avg" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value ="JR">JR</asp:ListItem>
                                    <asp:ListItem Value="SR">SR</asp:ListItem>
                                    <asp:ListItem Value="I">I</asp:ListItem>
                                    <asp:ListItem Value="II">II</asp:ListItem>
                                    <asp:ListItem Value="III">III</asp:ListItem>
                                    <asp:ListItem Value="IV">IV</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblBirthDate" Text="* Date of Birth:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtBirthDate" runat="server" ValidationGroup="avg" Width="120px"></asp:TextBox>
                                <asp:ImageButton ID="imgBirthDate" ImageUrl="~/images/icon/icon_calendar.png" runat="server" /> (mm/dd/yyyy)
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtBirthDate" Format="MM/dd/yyyy" PopupButtonID="imgBirthDate"></cc1:CalendarExtender>        
                                <asp:RequiredFieldValidator ID="rfvBirthDate" runat="server" ControlToValidate="txtBirthDate" ErrorMessage="Birth date required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revBirthDate" runat="server" ControlToValidate="txtBirthDate"
                                        Display="Dynamic" ErrorMessage="Date format MM/DD/YYYY" ValidationExpression="^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="avg">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>You must be at least 18 years of age to apply for service.</b></td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblPrimaryPhone" Text="* Primary Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCellPhone" Text="Cell Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCellPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCellPhone1" runat="server" ControlToValidate="txtCellPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCellPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCellPhone2" runat="server" ControlToValidate="txtCellPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCellPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revCellPhone3" runat="server" ControlToValidate="txtCellPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblWorkPhone" Text="Work Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtWorkPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revWorkPhone1" runat="server" ControlToValidate="txtWorkPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtWorkPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revWorkPhone2" runat="server" ControlToValidate="txtWorkPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtWorkPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revWorkPhone3" runat="server" ControlToValidate="txtWorkPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSSN" Text="* Social Security Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtSSN1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revSSN1" runat="server" ControlToValidate="txtSSN1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtSSN2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="2" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revSSN2" runat="server" ControlToValidate="txtSSN2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtSSN3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revSSN3" runat="server" ControlToValidate="txtSSN3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvSSN1" runat="server" ControlToValidate="txtSSN1" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvSSN2" runat="server" ControlToValidate="txtSSN2" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvSSN3" runat="server" ControlToValidate="txtSSN3" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmail" Text="Email address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="avg" Display="Dynamic"
                                    ErrorMessage="Invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblOccupation" Text="* Occupation:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtOccupation" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvOccupation" runat="server" ControlToValidate="txtOccupation" ErrorMessage="Occupation required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmployer" Text="* Employer:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmployer" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmployer" runat="server" ControlToValidate="txtEmployer" ErrorMessage="Employer required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:RadioButtonList ID="rboEmployerTime" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="less than 1 year">less than 1 year</asp:ListItem>
                                    <asp:ListItem Value="more than 1 year">more than 1 year</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvEmployerTime" runat="server" ControlToValidate="rboEmployerTime" ErrorMessage="Time employed required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblDLState" Text="* Driver's License/State Issued ID State:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList id="ddlState" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="N/A" Text="N/A"></asp:ListItem>
                                    <asp:ListItem Value="AL">Alabama</asp:ListItem>
                                    <asp:ListItem Value="AK">Alaska</asp:ListItem>
                                    <asp:ListItem Value="AS">American Somoa</asp:ListItem>
                                    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                                    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="CA">California</asp:ListItem>
                                    <asp:ListItem Value="CO">Colorado</asp:ListItem>
                                    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="DE">Delaware</asp:ListItem>
                                    <asp:ListItem Value="FM">Federated States Of Micronesia</asp:ListItem>
                                    <asp:ListItem Value="FL">Florida</asp:ListItem>
                                    <asp:ListItem Value="GA">Georgia</asp:ListItem>
                                    <asp:ListItem Value="GU">Guam</asp:ListItem>
                                    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="ID">Idaho</asp:ListItem>
                                    <asp:ListItem Value="IL">Illinois</asp:ListItem>
                                    <asp:ListItem Value="IN">Indiana</asp:ListItem>
                                    <asp:ListItem Value="IA">Iowa</asp:ListItem>
                                    <asp:ListItem Value="KS">Kansas</asp:ListItem>
                                    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="ME">Maine</asp:ListItem>
                                    <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                                    <asp:ListItem Value="MD">Maryland</asp:ListItem>
                                    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="MI">Michigan</asp:ListItem>
                                    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="MO">Missouri</asp:ListItem>
                                    <asp:ListItem Value="MT">Montana</asp:ListItem>
                                    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="NV">Nevada</asp:ListItem>
                                    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="NY">New York</asp:ListItem>
                                    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                                    <asp:ListItem Value="OH">Ohio</asp:ListItem>
                                    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="OR">Oregon</asp:ListItem>
                                    <asp:ListItem Value="PW">Palau</asp:ListItem>
                                    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                                    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="TX">Texas</asp:ListItem>
                                    <asp:ListItem Value="UT">Utah</asp:ListItem>
                                    <asp:ListItem Value="VT">Vermont</asp:ListItem>
                                    <asp:ListItem Value="VI">Virgin Islands</asp:ListItem>
                                    <asp:ListItem Value="VA">Virginia</asp:ListItem>
                                    <asp:ListItem Value="WA">Washington</asp:ListItem>
                                    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="WY">Wyoming</asp:ListItem> 
                                </asp:DropDownList>
                                <asp:CompareValidator ID="cpvState" runat="server" Operator="NotEqual" ControlToValidate="ddlState" ValueToCompare="N/A" ErrorMessage="<br/>If you cannot fill out the required fields, please contact Empire District at 800-206-2300." Display="Dynamic" ValidationGroup="avg"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="ddlState" ErrorMessage="State required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td></td><td><asp:label ID="lblDLStateMsg" Text="" runat="server" Font-Bold="true" ForeColor="Red" /></td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblDL" Text="* Driver's License/State Issued ID Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtDL" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDL" runat="server" ControlToValidate="txtDL" ErrorMessage="Number required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastResidentAddress" Text="* Last Residential Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastResidentAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastResidentAddress" runat="server" ControlToValidate="txtLastResidentAddress" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastResidentCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastResidentCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastResidentCity" runat="server" ControlToValidate="txtLastResidentCity" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblLastResidentState" Text="State:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtResidentialAddressState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvResidentialAddressState" runat="server" ControlToValidate="txtResidentialAddressState" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblLastResidentZip" Text="Zip:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtResidentialAddressZip" runat="server" ValidationGroup="avg" MaxLength="10" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvResidentialAddressZip" runat="server" ControlToValidate="txtResidentialAddressZip" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblJointCustomerInfo" Text="* Joint Customer Account:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblJointCustomer" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvJointCustomer" runat="server" ControlToValidate="rblJointCustomer" ErrorMessage="Joint Customer Account required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbServicePrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbServicePrev_Click"><span>Previous</span></asp:LinkButton>
                                <asp:LinkButton ID="lbJointCustomerInfoNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbJointCustomerInfoNext_Click"><span>Next</span></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
            
            <asp:Panel ID="pnlJointCustomerInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>Joint Customer Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right"><asp:label ID="lblJointCustomerInfo1" Text="Please choose one:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblJointCustomerInfo1" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Spouse">Spouse</asp:ListItem>
                                    <asp:ListItem Value="Roommate">Roommate</asp:ListItem>
                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblFirstName1" Text="* First Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtFirstName1" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvFirstName1" runat="server" ControlToValidate="txtFirstName1" ErrorMessage="First name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblMiddleName1" Text="* Middle Name or Initial:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtMiddleName1" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvMiddleName1" runat="server" ControlToValidate="txtMiddleName1" ErrorMessage="Middle name/initial required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastName1" Text="* LastName:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastName1" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvLastName1" runat="server" ControlToValidate="txtLastName1" ErrorMessage="Last name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSuffix1" Text="Suffix:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList ID="cboSuffix1" ValidationGroup="avg" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value ="JR">JR</asp:ListItem>
                                    <asp:ListItem Value="SR">SR</asp:ListItem>
                                    <asp:ListItem Value="I">I</asp:ListItem>
                                    <asp:ListItem Value="II">II</asp:ListItem>
                                    <asp:ListItem Value="III">III</asp:ListItem>
                                    <asp:ListItem Value="IV">IV</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblBirthDate1" Text="* Date of Birth:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtBirthDate1" runat="server" ValidationGroup="avg" Width="120px"></asp:TextBox>
                                <asp:ImageButton ID="imgBirthDate1" ImageUrl="~/images/icon/icon_calendar.png" runat="server" /> (mm/dd/yyyy)
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtBirthDate1" Format="MM/dd/yyyy" PopupButtonID="imgBirthDate1"></cc1:CalendarExtender>        
                                <asp:RegularExpressionValidator ID="revBirthDate1" runat="server" ControlToValidate="txtBirthDate1"
                                        Display="Dynamic" ErrorMessage="Date format MM/DD/YYYY" ValidationExpression="^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="avg">
                                </asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfvBirthDate1" runat="server" ControlToValidate="txtBirthDate1" ErrorMessage="Birth date required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                            <tr>
                                <td></td>
                                <td><b>You must be at least 18 years of age to apply for service.</b></td>
                            </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblPrimaryPhone1" Text="* Primary Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone1_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone1_1" runat="server" ControlToValidate="txtPrimaryPhone1_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone2_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone2_1" runat="server" ControlToValidate="txtPrimaryPhone2_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone3_1" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revPrimaryPhone3_1" runat="server" ControlToValidate="txtPrimaryPhone3_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone1_1" runat="server" ControlToValidate="txtPrimaryPhone1_1" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone2_1" runat="server" ControlToValidate="txtPrimaryPhone2_1" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone3_1" runat="server" ControlToValidate="txtPrimaryPhone3_1" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCellPhone1" Text="Cell Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCellPhone1_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCellPhone1_1" runat="server" ControlToValidate="txtCellPhone1_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCellPhone2_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCellPhone2_1" runat="server" ControlToValidate="txtCellPhone2_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCellPhone3_1" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revCellPhone3_1" runat="server" ControlToValidate="txtCellPhone3_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblWorkPhone1" Text="Work Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtWorkPhone1_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revWorkPhone1_1" runat="server" ControlToValidate="txtWorkPhone1_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtWorkPhone2_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revWorkPhone2_1" runat="server" ControlToValidate="txtWorkPhone2_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtWorkPhone3_1" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revWorkPhone3_1" runat="server" ControlToValidate="txtWorkPhone3_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSSN1" Text="* Social Security Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtSSN1_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revSSN1_1" runat="server" ControlToValidate="txtSSN1_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtSSN2_1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="2" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revSSN2_1" runat="server" ControlToValidate="txtSSN2_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtSSN3_1" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revSSN3_1" runat="server" ControlToValidate="txtSSN3_1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvSSN1_1" runat="server" ControlToValidate="txtSSN1_1" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvSSN2_1" runat="server" ControlToValidate="txtSSN2_1" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvSSN3_1" runat="server" ControlToValidate="txtSSN3_1" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmail1" Text="Email address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmail1" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmail1" runat="server" ControlToValidate="txtEmail1" ValidationGroup="avg" Display="Dynamic"
                                    ErrorMessage="Invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblOccupation1" Text="* Occupation:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtOccupation1" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvOccupation1" runat="server" ControlToValidate="txtOccupation1" ErrorMessage="Occupation required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmployer1" Text="* Employer:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmployer1" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmployer1" runat="server" ControlToValidate="txtEmployer1" ErrorMessage="Employer required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:RadioButtonList ID="rboEmployerTime1" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="less than 1 year">less than 1 year</asp:ListItem>
                                    <asp:ListItem Value="more than 1 year">more than 1 year</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvEmployerTime1" runat="server" ControlToValidate="rboEmployerTime1" ErrorMessage="Time employed required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblDLState1" Text="* Driver's License/State Issued ID State:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList id="ddlState1" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="N/A" Text="N/A"></asp:ListItem>
                                    <asp:ListItem Value="AL">Alabama</asp:ListItem>
                                    <asp:ListItem Value="AK">Alaska</asp:ListItem>
                                    <asp:ListItem Value="AS">American Somoa</asp:ListItem>
                                    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                                    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="CA">California</asp:ListItem>
                                    <asp:ListItem Value="CO">Colorado</asp:ListItem>
                                    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="DE">Delaware</asp:ListItem>
                                    <asp:ListItem Value="FM">Federated States Of Micronesia</asp:ListItem>
                                    <asp:ListItem Value="FL">Florida</asp:ListItem>
                                    <asp:ListItem Value="GA">Georgia</asp:ListItem>
                                    <asp:ListItem Value="GU">Guam</asp:ListItem>
                                    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="ID">Idaho</asp:ListItem>
                                    <asp:ListItem Value="IL">Illinois</asp:ListItem>
                                    <asp:ListItem Value="IN">Indiana</asp:ListItem>
                                    <asp:ListItem Value="IA">Iowa</asp:ListItem>
                                    <asp:ListItem Value="KS">Kansas</asp:ListItem>
                                    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="ME">Maine</asp:ListItem>
                                    <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                                    <asp:ListItem Value="MD">Maryland</asp:ListItem>
                                    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="MI">Michigan</asp:ListItem>
                                    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="MO">Missouri</asp:ListItem>
                                    <asp:ListItem Value="MT">Montana</asp:ListItem>
                                    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="NV">Nevada</asp:ListItem>
                                    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="NY">New York</asp:ListItem>
                                    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                                    <asp:ListItem Value="OH">Ohio</asp:ListItem>
                                    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="OR">Oregon</asp:ListItem>
                                    <asp:ListItem Value="PW">Palau</asp:ListItem>
                                    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                                    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="TX">Texas</asp:ListItem>
                                    <asp:ListItem Value="UT">Utah</asp:ListItem>
                                    <asp:ListItem Value="VT">Vermont</asp:ListItem>
                                    <asp:ListItem Value="VI">Virgin Islands</asp:ListItem>
                                    <asp:ListItem Value="VA">Virginia</asp:ListItem>
                                    <asp:ListItem Value="WA">Washington</asp:ListItem>
                                    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="WY">Wyoming</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="cpvState1" runat="server" Operator="NotEqual" ControlToValidate="ddlState1" ValueToCompare="N/A" ErrorMessage="<br/>If you cannot fill out the required fields, please contact Empire District at 800-206-2300." Display="Dynamic" ValidationGroup="avg"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="rvfState1" runat="server" ControlToValidate="ddlState1" ErrorMessage="State required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblDL1" Text="* Driver's License/State Issued ID Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtDL1" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDL1" runat="server" ControlToValidate="txtDL1" ErrorMessage="Number required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastResidentAddress1" Text="* Last Residential Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastResidentAddress1" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastResidentAddress1" runat="server" ControlToValidate="txtLastResidentAddress1" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastResidentCity1" Text="City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastResidentCity1" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastResidentCity1" runat="server" ControlToValidate="txtLastResidentCity1" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblLastResidentState1" Text="State:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtResidentialAddressState1" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvResidentialAddressState1" runat="server" ControlToValidate="txtResidentialAddressState1" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblLastResidentZip1" Text="Zip:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtResidentialAddressZip1" runat="server" ValidationGroup="avg" MaxLength="10" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvResidentialAddressZip1" runat="server" ControlToValidate="txtResidentialAddressZip1" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSecondJointCustomerInfo" Text="* Second Joint Customer Account:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblSecondJointCustomer" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvSecondJointCustomer" runat="server" ControlToValidate="rblSecondJointCustomer" ErrorMessage="Second Joint Customer Account required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbCustomerInfoPrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbCustomerInfoPrev_Click"><span>Previous</span></asp:LinkButton>
                                <asp:LinkButton ID="lbSecondJointCustomerInfoNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSecondJointCustomerInfoNext_Click"><span>Next</span></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlSecondJointCustomerInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>Second Joint Customer Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right"><asp:label ID="lblJointCustomerInfo2" Text="Please choose one:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblJointCustomerInfo2" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Spouse">Spouse</asp:ListItem>
                                    <asp:ListItem Value="Roommate">Roommate</asp:ListItem>
                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblFirstName2" Text="* First Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtFirstName2" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvFirstName2" runat="server" ControlToValidate="txtFirstName2" ErrorMessage="First name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblMiddleName2" Text="* Middle Name or Initial:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtMiddleName2" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvMiddleName2" runat="server" ControlToValidate="txtMiddleName2" ErrorMessage="Middle name/initial required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastName2" Text="* LastName:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastName2" runat="server" Width="300px" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvLastName2" runat="server" ControlToValidate="txtLastName2" ErrorMessage="Last name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSuffix2" Text="Suffix:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList ID="cboSuffix2" ValidationGroup="avg" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value ="JR">JR</asp:ListItem>
                                    <asp:ListItem Value="SR">SR</asp:ListItem>
                                    <asp:ListItem Value="I">I</asp:ListItem>
                                    <asp:ListItem Value="II">II</asp:ListItem>
                                    <asp:ListItem Value="III">III</asp:ListItem>
                                    <asp:ListItem Value="IV">IV</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblBirthDate2" Text="* Date of Birth:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtBirthDate2" runat="server" ValidationGroup="avg" Width="120px"></asp:TextBox>
                                <asp:ImageButton ID="imgBirthDate2" ImageUrl="~/images/icon/icon_calendar.png" runat="server" /> (mm/dd/yyyy)
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtBirthDate2" Format="MM/dd/yyyy" PopupButtonID="imgBirthDate2"></cc1:CalendarExtender>        
                                <asp:RegularExpressionValidator ID="revBirthDate2" runat="server" ControlToValidate="txtBirthDate2"
                                        Display="Dynamic" ErrorMessage="Date format MM/DD/YYYY" ValidationExpression="^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="avg">
                                </asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfvBirthDate2" runat="server" ControlToValidate="txtBirthDate2" ErrorMessage="Birth date required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                            <tr>
                                <td></td>
                                <td><b>You must be at least 18 years of age to apply for service.</b></td>
                            </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblPrimaryPhone2" Text="* Primary Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone1_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone1_2" runat="server" ControlToValidate="txtPrimaryPhone1_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone2_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone2_2" runat="server" ControlToValidate="txtPrimaryPhone2_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone3_2" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revPrimaryPhone3_2" runat="server" ControlToValidate="txtPrimaryPhone3_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone1_2" runat="server" ControlToValidate="txtPrimaryPhone1_2" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone2_2" runat="server" ControlToValidate="txtPrimaryPhone2_2" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone3_2" runat="server" ControlToValidate="txtPrimaryPhone3_2" ErrorMessage="Phone required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCellPhone2" Text="Cell Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCellPhone1_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCellPhone1_2" runat="server" ControlToValidate="txtCellPhone1_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCellPhone2_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revCellPhone2_2" runat="server" ControlToValidate="txtCellPhone2_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCellPhone3_2" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revCellPhone3_2" runat="server" ControlToValidate="txtCellPhone3_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblWorkPhone2" Text="Work Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtWorkPhone1_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revWorkPhone1_2" runat="server" ControlToValidate="txtWorkPhone1_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtWorkPhone2_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revWorkPhone2_2" runat="server" ControlToValidate="txtWorkPhone2_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtWorkPhone3_2" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revWorkPhone3_2" runat="server" ControlToValidate="txtWorkPhone3_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSSN2" Text="* Social Security Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtSSN1_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revSSN1_2" runat="server" ControlToValidate="txtSSN1_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtSSN2_2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="2" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revSSN2_2" runat="server" ControlToValidate="txtSSN2_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtSSN3_2" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revSSN3_2" runat="server" ControlToValidate="txtSSN3_2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvSSN1_2" runat="server" ControlToValidate="txtSSN1_2" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvSSN2_2" runat="server" ControlToValidate="txtSSN2_2" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvSSN3_2" runat="server" ControlToValidate="txtSSN3_2" ErrorMessage="SSN required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmail2" Text="Email address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmail2" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmail2" runat="server" ControlToValidate="txtEmail2" ValidationGroup="avg" Display="Dynamic"
                                    ErrorMessage="Invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblOccupation2" Text="* Occupation:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtOccupation2" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvOccupation2" runat="server" ControlToValidate="txtOccupation2" ErrorMessage="Occupation required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmployer2" Text="* Employer:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmployer2" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmployer2" runat="server" ControlToValidate="txtEmployer2" ErrorMessage="Employer required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:RadioButtonList ID="rboEmployerTime2" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="less than 1 year">less than 1 year</asp:ListItem>
                                    <asp:ListItem Value="more than 1 year">more than 1 year</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvEmployerTime2" runat="server" ControlToValidate="rboEmployerTime2" ErrorMessage="Time employed required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblDLState2" Text="* Driver's License/State Issued ID State:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList id="ddlState2" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="N/A" Text="N/A"></asp:ListItem>
                                    <asp:ListItem Value="AL">Alabama</asp:ListItem>
                                    <asp:ListItem Value="AK">Alaska</asp:ListItem>
                                    <asp:ListItem Value="AS">American Somoa</asp:ListItem>
                                    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                                    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="CA">California</asp:ListItem>
                                    <asp:ListItem Value="CO">Colorado</asp:ListItem>
                                    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="DE">Delaware</asp:ListItem>
                                    <asp:ListItem Value="FM">Federated States Of Micronesia</asp:ListItem>
                                    <asp:ListItem Value="FL">Florida</asp:ListItem>
                                    <asp:ListItem Value="GA">Georgia</asp:ListItem>
                                    <asp:ListItem Value="GU">Guam</asp:ListItem>
                                    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="ID">Idaho</asp:ListItem>
                                    <asp:ListItem Value="IL">Illinois</asp:ListItem>
                                    <asp:ListItem Value="IN">Indiana</asp:ListItem>
                                    <asp:ListItem Value="IA">Iowa</asp:ListItem>
                                    <asp:ListItem Value="KS">Kansas</asp:ListItem>
                                    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="ME">Maine</asp:ListItem>
                                    <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                                    <asp:ListItem Value="MD">Maryland</asp:ListItem>
                                    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="MI">Michigan</asp:ListItem>
                                    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="MO">Missouri</asp:ListItem>
                                    <asp:ListItem Value="MT">Montana</asp:ListItem>
                                    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="NV">Nevada</asp:ListItem>
                                    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="NY">New York</asp:ListItem>
                                    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                                    <asp:ListItem Value="OH">Ohio</asp:ListItem>
                                    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="OR">Oregon</asp:ListItem>
                                    <asp:ListItem Value="PW">Palau</asp:ListItem>
                                    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                                    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="TX">Texas</asp:ListItem>
                                    <asp:ListItem Value="UT">Utah</asp:ListItem>
                                    <asp:ListItem Value="VT">Vermont</asp:ListItem>
                                    <asp:ListItem Value="VI">Virgin Islands</asp:ListItem>
                                    <asp:ListItem Value="VA">Virginia</asp:ListItem>
                                    <asp:ListItem Value="WA">Washington</asp:ListItem>
                                    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="WY">Wyoming</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="cpvState2" runat="server" Operator="NotEqual" ControlToValidate="ddlState2" ValueToCompare="N/A" ErrorMessage="<br/>If you cannot fill out the required fields, please contact Empire District at 800-206-2300." Display="Dynamic" ValidationGroup="avg"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="rfvState2" runat="server" ControlToValidate="ddlState2" ErrorMessage="State required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblDL2" Text="* Driver's License/State Issued ID Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtDL2" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDL2" runat="server" ControlToValidate="txtDL2" ErrorMessage="Number required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastResidentAddress2" Text="* Last Residential Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastResidentAddress2" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastResidentAddress2" runat="server" ControlToValidate="txtLastResidentAddress2" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblLastResidentCity2" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtLastResidentCity2" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastResidentCity2" runat="server" ControlToValidate="txtLastResidentCity2" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblLastResidentState2" Text="State:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtResidentialAddressState2" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvResidentialAddressState2" runat="server" ControlToValidate="txtResidentialAddressState2" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblLastResidentZip2" Text="Zip:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtResidentialAddressZip2" runat="server" ValidationGroup="avg" MaxLength="10" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvResidentialAddressZip2" runat="server" ControlToValidate="txtResidentialAddressZip2" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbJointCustomerInfoPrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbJointCustomerInfoPrev_Click"><span>Previous</span></asp:LinkButton>
                                <asp:LinkButton ID="lbServiceInfoNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbServiceInfoNext_Click"><span>Next</span></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            
            <asp:Panel ID="pnlServiceInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>New Service Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right"><asp:label ID="lblPreviousAccount" Text="* Have you ever had an&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblPreviousAccount" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvPreviousAccount" runat="server" ControlToValidate="rblPreviousAccount" ErrorMessage="answer required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblPreviousAccout1" Text="Empire District account?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><b>&nbsp;&nbsp;If a past due amount is owed, service will not begin.</b></td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblRentBuy" Text="* Are you renting or buying?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblRentBuy" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Renting">Renting</asp:ListItem>
                                    <asp:ListItem Value="Buying">Buying</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvRentBuy" runat="server" ControlToValidate="rblRentBuy" ErrorMessage="answer required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblServiceAddress" Text="* Service Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvServiceAddress" runat="server" ControlToValidate="txtServiceAddress" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblServiceAddressAN" Text="Apartment Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceAddressAN" runat="server" ValidationGroup="avg" Width="115px" ></asp:TextBox>
                                <asp:label ID="lblServiceAddressLN" Text="Lot Number: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtServiceAddressLN" runat="server" ValidationGroup="avg" Width="95px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblServiceAddressCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceAddressCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvfServiceAddressCity" runat="server" ControlToValidate="txtServiceAddressCity" ErrorMessage="City required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblServiceAddressState" Text="State: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtServiceAddressState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvServiceAddressState" runat="server" ControlToValidate="txtServiceAddressState" ErrorMessage="State required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblServiceAddressZip" Text="Zip: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtServiceAddressZip" runat="server" ValidationGroup="avg" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvServiceAddressZip" runat="server" ControlToValidate="txtServiceAddressZip" ErrorMessage="Zip required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblMailingAddress" Text="* Mailing Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right"><asp:Button ID="btnDuplicateAddress" runat="server" Text="Same as service address" CausesValidation="false" />&nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtMailingAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMailingAddress" runat="server" ControlToValidate="txtMailingAddress" ErrorMessage="Address required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblMailingAddressAN" Text="Apartment Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtMailingAddressAN" runat="server" ValidationGroup="avg" Width="115px" ></asp:TextBox>
                                <asp:label ID="lblMailingAddressLN" Text="Lot Number: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtMailingAddressLN" runat="server" ValidationGroup="avg" Width="95px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblMailingAddressCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtMailingAddressCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMailingAddressCity" runat="server" ControlToValidate="txtMailingAddressCity" ErrorMessage="City required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblMailingAddressState" Text="State: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtMailingAddressState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMailingAddressState" runat="server" ControlToValidate="txtMailingAddressState" ErrorMessage="State required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblMailingAddressZip" Text="Zip: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtMailingAddressZip" runat="server" ValidationGroup="avg" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMailingAddressZip" runat="server" ControlToValidate="txtMailingAddressZip" ErrorMessage="Zip required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblServiceStartDate" Text="* Service Start Date:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceStartDate" runat="server" ValidationGroup="avg" Width="110px"></asp:TextBox>
                                <asp:ImageButton ID="imgServiceStartDate" ImageUrl="~/images/icon/icon_calendar.png" runat="server" /> (mm/dd/yyyy)
                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtServiceStartDate" Format="MM/dd/yyyy" PopupButtonID="imgServiceStartDate"></cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfvServiceStartDate" runat="server" ControlToValidate="txtServiceStartDate" ErrorMessage="service date required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revServiceStartDate" runat="server" ControlToValidate="txtServiceStartDate"
                                        Display="Dynamic" ErrorMessage="Date format MM/DD/YYYY" ValidationExpression="^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="avg">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>24 hour advanced notice required. Weekends and holidays are excluded.</b></td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmergencyContactName" Text="* Emergency Contact Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmergencyContactName" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmergencyContactName" runat="server" ControlToValidate="txtEmergencyContactName" ErrorMessage="name required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmergencyContactPhone" Text="* Emergency Contact Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmergencyContactPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RequiredFieldValidator ID="rfvEmergencyContactPhone1" runat="server" ControlToValidate="txtEmergencyContactPhone1" ErrorMessage="phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revEmergencyContactPhone1" runat="server" ControlToValidate="txtEmergencyContactPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtEmergencyContactPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RequiredFieldValidator ID="rfvEmergencyContactPhone2" runat="server" ControlToValidate="txtEmergencyContactPhone2" ErrorMessage="phone required" ValidationGroup="avg" Display="Dynamic" />                
                                <asp:RegularExpressionValidator ID="revEmergencyContactPhone2" runat="server" ControlToValidate="txtEmergencyContactPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtEmergencyContactPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RequiredFieldValidator ID="rfvEmergencyContactPhone3" runat="server" ControlToValidate="txtEmergencyContactPhone3" ErrorMessage="phone required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revEmergencyContactPhone3" runat="server" ControlToValidate="txtEmergencyContactPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblEmergencyContactRelationship" Text="* Contact Relationship:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmergencyContactRelationship" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmergencyContactRelationship" runat="server" ControlToValidate="txtEmergencyContactRelationship" ErrorMessage="relationship required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbSecondJointCustomerInfoPrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbSecondJointCustomerInfoPrev_Click"><span>Previous</span></asp:LinkButton>
                                <asp:LinkButton ID="lbContactInfoNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbContactInfoNext_Click"><span>Next</span></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
            
            <asp:Panel ID="pnlAdditionalInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>Additional Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right"><asp:label ID="lblContactMethod" Text="* How may we contact you?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblContactMethod" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Phone">Phone</asp:ListItem>
                                    <asp:ListItem Value="Email">Email</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvContactMethod" runat="server" ControlToValidate="rblContactMethod" ErrorMessage="contact method required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblAdditionalInformation" Text="Additional Information&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="right" valign="top"><asp:label ID="lblInformation" Text="Message:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><asp:TextBox ID="txtInformation" runat="server" ValidationGroup="avg" Width="300px" MaxLength="500" TextMode="MultiLine" Height="75px"></asp:TextBox></td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblNewConstruction" Text="* Is this a new construction?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblNewConstruction" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvNewConstruction" runat="server" ControlToValidate="rblNewConstruction" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbServiceInfoPrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbServiceInfoPrev_Click"><span>Previous</span></asp:LinkButton>
                                <asp:LinkButton ID="lbConstructionInfoNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbConstructionInfoNext_Click"><span>Next</span></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
            
            <asp:Panel ID="pnlConstructionInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>Construction Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right"><asp:label ID="lblCDContactName" Text="Contact Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><asp:TextBox ID="txtCDContactName" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDContactType" Text="Contact Type:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList id="ddlCDContactType" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Builder">Builder</asp:ListItem>
                                    <asp:ListItem Value="Customer">Customer</asp:ListItem>
                                    <asp:ListItem Value="Developer">Developer</asp:ListItem>
                                    <asp:ListItem Value="Electrician">Electrician</asp:ListItem>
                                    <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDContactPhone" Text="Contact Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCDContactPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmergencyContactPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCDContactPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmergencyContactPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtCDContactPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmergencyContactPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDServiceType" Text="* Type of Service:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDServiceType" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Overhead">Overhead</asp:ListItem>
                                    <asp:ListItem Value="Underground">Underground</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDServiceType" runat="server" ControlToValidate="rblCDServiceType" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDServiceSize" Text="* Size of Service (Amp):&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblServiceSize" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="200 or less">200 or less</asp:ListItem>
                                    <asp:ListItem Value="400">400</asp:ListItem>
                                    <asp:ListItem Value="600">600</asp:ListItem>
                                    <asp:ListItem Value="Other">Other</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDServiceSize" runat="server" ControlToValidate="rblServiceSize" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDHomeType" Text="* Home Type:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:DropDownList id="ddlCDHomeType" runat="server">
                                    <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Apt Lower Floor">Apt Lower Floor</asp:ListItem>
                                    <asp:ListItem Value="Apt Top Floor">Apt Top Floor</asp:ListItem>
                                    <asp:ListItem Value="Manufactured Home">Manufactured Home</asp:ListItem>
                                    <asp:ListItem Value="Multi Story">Multi Story</asp:ListItem>
                                    <asp:ListItem Value="Single Story">Single Story</asp:ListItem>
                                    <asp:ListItem Value="Split Level">Split Level</asp:ListItem>
                                    <asp:ListItem Value="Townhouse End">Townhouse End</asp:ListItem>
                                    <asp:ListItem Value="Townhouse Mid">Townhouse Mid</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCDHomeType" runat="server" ControlToValidate="ddlCDHomeType" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDElectricHeat" Text="* Electric Heat?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDElectricHeat" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDElectricHeat" runat="server" ControlToValidate="rblCDElectricHeat" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDCooled" Text="* Is it Cooled?&nbsp;" runat="server" Font-Bold="true" /></td>                        
                            <td>
                                <asp:RadioButtonList ID="rblCDCooled" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDCooled" runat="server" ControlToValidate="rblCDCooled" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDSquareFoot" Text="* Square Foot Heat/Cool:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCDSquareFoot" runat="server" ValidationGroup="avg" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCDSquareFoot" runat="server" ControlToValidate="txtCDSquareFoot" ErrorMessage="Square foot required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDBackupGeneration1" Text="* Will a permanent back-up&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDBackupGeneration" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDBackupGeneration" runat="server" ControlToValidate="rblCDBackupGeneration" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDBackupGeneration2" Text="generator be used?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <b>(</b>
                                <asp:HyperLink ID="hlCDBackupGeneration" runat="server" Target="_blank" NavigateUrl="~/CustomerService/Electric.aspx">Click here</asp:HyperLink>
                                <b>&nbsp;to see EDEC Generator Requirements for Installation)</b>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDGenerator" Text="* Standby Generator?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDGenerator" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDGenerator" runat="server" ControlToValidate="rblCDGenerator" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDTempService" Text="* Temporary Service Needed?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDTempService" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDTempService" runat="server" ControlToValidate="rblCDTempService" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                                <b>(</b>
                                <asp:HyperLink ID="hlCDTempService" runat="server" Target="_blank" NavigateUrl="~/CustomerService/Electric.aspx">Click here</asp:HyperLink>
                                <b>&nbsp;to see EDEC Service Standards Installation Requirements)</b>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDSiteClear" Text="* Site clearing completed?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDSiteClear" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDSiteClear" runat="server" ControlToValidate="rblCDSiteClear" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDFinalGrade" Text="* Lot within 6 inches of final grade?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDFinalGrade" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDFinalGrade" runat="server" ControlToValidate="rblCDFinalGrade" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblCDBuildingStaked" Text="* Location of building staked?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:RadioButtonList ID="rblCDBuildingStaked" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rfvCDBuildingStaked" runat="server" ControlToValidate="rblCDBuildingStaked" ErrorMessage="question required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top"><asp:label ID="lblCDAdditionalInformation" Text="Additional Information:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><asp:TextBox ID="txtCDAdditionalInformation" runat="server" ValidationGroup="avg" Width="300px" MaxLength="500" TextMode="MultiLine" Height="75px"></asp:TextBox></td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbAdditionalInfoPrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbAdditionalInfoPrev_Click"><span>Previous</span></asp:LinkButton>
                                <asp:LinkButton ID="lbFinalizeNext" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbFinalizeNext_Click"><span>Next</span></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
            
            <asp:Panel ID="pnlFinalize" runat="server" Visible="false">
                <font style="color:#113962"><b>Finalize</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <asp:label ID="lblStatus" Text="To complete your service request, please hit submit request. You will be contacted within two business days of receipt of your application for service." runat="server" Font-Bold="false" />
                    <br /><br />
                    <asp:LinkButton ID="lbConstructionInfoPrev" runat="server" CssClass="ovalbutton" CausesValidation="false" ValidationGroup="avg" OnClick="lbConstructionInfoPrev_Click"><span>Previous</span></asp:LinkButton>
                    <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSubmit_Click"><span>Submit Request</span></asp:LinkButton>
                    <br /><br />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
<script type="text/javascript">
    function autotab(original,destination){
     if (original.getAttribute && original.value.length == original.getAttribute("maxlength"))
         destination.focus();
    }
 </script>
</asp:Content>
