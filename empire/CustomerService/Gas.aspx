<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Gas.aspx.vb" Inherits="CustomerService_Gas" title="Gas Rates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lbRES" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Gas.aspx?" Text="Residential"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbCOMM" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Gas.aspx?gas=COMM" Text="Commercial"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbIND" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Gas.aspx?gas=IND" Text="Industrial"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbTS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Gas.aspx?gas=TS" Text="Transportation Services"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbPGA" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Gas.aspx?gas=PGA" Text="Purchased Gas Adjustments"/>
            <br />&nbsp

                <asp:Panel ID="pnlResidential" runat="server" HeaderText="Residential">
                    <ContentTemplate>
                        <b>Bill Explanation</b>
                        <br /><br />
                        <asp:DataList ID="dlBillExplanation" runat="server" SkinID="DataList" DataSourceID="sqlBillExplanation">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlBillExplanation" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 110 ORDER BY documents.webdescription">
                        </asp:SqlDataSource> 
                        <br /><hr style="color:#113962;height:1px;"/>
                        <b>Residential Gas Rates</b>
                        <br /><br />
                        <asp:DataList ID="dlResidential" runat="server" SkinID="DataList" DataSourceID="sqlDSResidential">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSResidential" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 63 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlResidential--%>
                
                <asp:Panel ID="pnlGas" runat="server" HeaderText="Gas" Visible="false">
                    <ContentTemplate>
                        <asp:Label ID="lblGas" runat="server" font-bold="true" />
                        <br /><br />
                        <asp:DataList ID="dlGas" runat="server" SkinID="DataList" DataSourceID="sqlDSGas">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSGas" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="">
                        </asp:SqlDataSource>
                        <br /><hr style="color:#113962;height:1px;"/>
                    </ContentTemplate>
                </asp:Panel><%--end pnlGas--%>
                           
                <asp:Panel ID="pnlLocations" runat="Server">
                <%--<hr style="color:#113962;height:1px;" />--%>
                <div style="float:left;width:30%;padding:1%;">
                    <b>Northern System Communities:</b>
                    <div style="float:left;width:49%;">
                        Brookfield<br />
                        Brunswick<br />
                        Bucklin<br />
                        Chillicothe<br />
                        Chula<br />
                        Glasgow<br />
                        Keytesville<br />
                        Laclede
                    </div>
                    <div style="float:left;width:50%">
                        Marceline<br />
                        Meadville<br />
                        Rural Territory<br />
                        Salisbury<br />
                        Trenton<br />
                        Utica<br />
                        Wheeling
                    </div> 
                </div>
                <div style="float:left;width:30%;padding:1%;">
                    <b>Southern System Communities:</b>
                    <div style="float:left;width:49%;">
                        Clinton<br />
                        Deerfield<br />
                        Dresden<br />
                        Henrietta<br />
                        Leeton<br />
                        Lexington<br />
                        Marshall<br />
                        Nevada
                    </div>
                    <div style="float:left;width:50%">
                        Otterville<br />
                        Platte City<br />
                        Richmond<br />
                        Rural Territory<br />
                        Sedalia<br />
                        Smithton<br />
                        Tracy<br />
                        Weston
                    </div>
                </div>
                <div style="float:left;width:30%;padding:1%;">
                    <b>Northwest Communities:&nbsp;&nbsp;&nbsp;</b>
                    <div style="float:left;width:49%;">
                        Barnard<br />
                        Craig<br />
                        Graham<br />
                        Mound City<br />
                        Rock Port<br />
                        Bigelow<br />
                        Fairfax<br />
                        Maitland            
                    </div>
                    <div style="float:left;width:50%">
                        Oregon<br />
                        Skidmore<br />
                        Bolckow<br />
                        Forest City<br />
                        Maryville<br />
                        Ravenwood<br />
                        Tarkio
                    </div>
                </div>
                <div style="clear:both;"></div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
