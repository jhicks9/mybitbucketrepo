﻿<%@ Page Title="Service Connect Commercial" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ServiceConnectCommercial.aspx.vb" Inherits="CustomerService_ServiceConnectCommercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <b>Thank you for using Empire’s online commercial application for service.</b><br />
            <br />
            We will respond to your request by telephone or email within two business days of receipt.  An account representative may need to verify the service address, schedule a date for a service person to meet you at the property, and discuss any other account requirements.<br />
            <br />
            Twenty-four hour notice, excluding weekends and holidays, is required for service requests.  Please note, it may take longer than 24 hours to have your service connected.<br />
            <br />
            A deposit may be required before service can be turned on.<br />
            <br />
            By requesting service, you allow Empire to check credit information and verify the information provided.<br />
            <br />
            Do not use this form for emergency requests.  If you need assistance in filling out this application, please contact Empire at 800-206-2300 for electric service or 800-424-0427 for natural gas service.<br />
            <br /><hr style="color:#113962;height:1px;" /><br />
            Applications vary based upon the type of business. Please select the correct application for your business based upon the definitions found below.<br />
            <br />
            <b>Individual/Sole Proprietorship</b> – A business owned by one person who is responsible for its debts. The business is not incorporated.<br />
            <br />
            <b>Partnership</b> – A business owned by more than one individual or business. Each of the general partners is responsible for its debt.<br />
            <br />
            <b>Corporation</b> – A business that has a legal existence separate from its owner or partners. A corporation must have appropriate documents on file with the Secretary of State of the state in which service is requested.<br />
            <br />

            <asp:Panel ID="pnlTerms" runat="server" Visible="true" >
                The Customer agrees and guarantees to pay the Company for service rendered in accordance with the Company's rates, rules and regulations applicable to the service supplied hereunder, and which shall upon the date of this contract or any time during the period the location is supplied with service as provided herein, be currently in effect as published and prescribed schedules on file with the Commission governing public utilities in the state in which service is rendered.<br />
                <br />
                When and if this contract is accepted by the Company as herein provided, The Company will furnish, and the Customer will take electric and/or water service or gas service from the Company as soon as the Company commences delivering service and shall continue until written notice by either party to the other party shall be given, for the discontinuance or termination of same.<br />
                <br />
                The Company shall use reasonable diligence in providing a regular and uninterrupted supply of electric and/or water service or gas service, but in case the supply of electric and/or water service or gas service is interrupted by reason of strike, riot, invasion, storm, fire, accident, breakdown, unexpected or prolonged increase in usage, legal process, state or municipal interference, or any cause beyond its control, the Company shall not be liable for damages to the Customer for interruption in the service due to any of the causes aforesaid.<br />
                <br />
                It is understood that the terms hereinbefore made or agreed to by the parties hereto in relation to said gas service are merged into this Contract, and that no previous or contemporaneous representations or agreements, whether written or verbal, shall be binding upon the Company or the Customer, except as and to the extent contained herein.<br />
                <br />
                <asp:CheckBox ID="ckbAgree" runat="server" Text="I agree. By agreeing you understand and accept the terms of the above statement." />&nbsp;&nbsp;<asp:Label ID="lblAgreeStatus" runat="server" ForeColor="Red" Text="" /><br />
                <br />
                <asp:Button ID="btnContinue" runat="server" Text="Continue" CausesValidation="true" />
            </asp:Panel>

            <asp:Panel ID="pnlAgree" runat="server" Visible="false" >
                <acrobat:uc4 ID="uc4" runat="server" />

                <asp:DataList ID="dlCommercial" runat="server" DataSourceID="sqlCommercial" Visible="true">
                    <itemtemplate>
                        <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                            NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' 
                            Target="_blank" runat="server"/> 
                    </itemtemplate>
                </asp:DataList>
                <asp:SqlDataSource ID="sqlCommercial" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                    SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 122 and (webdescription like '%service connect' or webdescription like 'service connect%' or webdescription like '%service connect%') ORDER BY documents.lastupdate desc">
                </asp:SqlDataSource>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

