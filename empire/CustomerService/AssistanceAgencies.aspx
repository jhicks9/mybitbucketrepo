<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AssistanceAgencies.aspx.vb" Inherits="CustomerService_AssistanceAgencies" title="Assistance Agencies" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lbAR" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/AssistanceAgencies.aspx?aagency=AR" Text="Arkansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbKS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/AssistanceAgencies.aspx?aagency=KS" Text="Kansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbMO" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/AssistanceAgencies.aspx" Text="Missouri"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbOK" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/AssistanceAgencies.aspx?aagency=OK" Text="Oklahoma"/>
            <br />&nbsp

                <asp:Panel ID="pnlAR" runat="server" HeaderText="Arkansas">
                    <ContentTemplate>
                        <b>Arkansas Energy Assistance Agencies for Electric Customers</b>
                        <br /><br />
                        <asp:GridView ID="gvAR" runat="server" DataSourceID="sqlDSAR" SkinID="Gridview">
                            <Columns>
                                <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                            </Columns>                        
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlDSAR" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT county,agency,phone FROM agencies where state = 'Arkansas' and type = 'electric' order by county,location">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlAR--%>
                
                <asp:Panel ID="pnlKS" runat="server" HeaderText="Kansas">
                    <ContentTemplate>
                        <b>Kansas Energy Assistance Agencies for Electric Customers</b>
                        <br /><br />
                        <asp:GridView ID="gvKS" runat="server" DataSourceID="sqlDSKS" SkinID="Gridview">
                            <Columns>
                                <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                            </Columns>                        
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlDSKS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT county,agency,phone FROM agencies where state = 'Kansas' and type = 'electric' order by county,location">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlKS--%>
                
                <asp:Panel ID="pnlMO" runat="server" HeaderText="Missouri">
                    <ContentTemplate>
                        <b>Missouri Energy Assistance Agencies for Electric Customers</b>
                        <br /><br />
                        <asp:GridView ID="gvMOElectric" runat="server" DataSourceID="sqlDSMOElectric" SkinID="GridView">
                            <Columns>
                                <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                                <asp:BoundField DataField="familyservices" HeaderText="Family Services" SortExpression="familyservices" />
                                <asp:BoundField DataField="energycrisis" HeaderText="Energy Crisis" SortExpression="energycrisis" />
                                <asp:BoundField DataField="projecthelp" HeaderText="Project Help" SortExpression="projecthelp" />
                            </Columns>                        
                        </asp:GridView>
                        <hr style="color:#113962;height:1px;"/>            
                        <b>Missouri Energy Assistance Agencies for Gas Customers</b>
                        <br /><br />
                        <asp:GridView ID="gvMOGas" runat="server" DataSourceID="sqlDSMOGas" SkinID="GridView">
                            <Columns>
                                <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                <asp:BoundField DataField="projecthelp" HeaderText="Project Help" SortExpression="projecthelp" />
                            </Columns>                        
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlDSMOElectric" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT county,location,familyservices,energycrisis,projecthelp FROM agencies where state = 'Missouri' and type = 'electric' order by county,location">
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="sqlDSMOGas" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT county,agency,phone,projecthelp FROM agencies where state = 'Missouri' and type = 'gas' order by county,location">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlMO--%>
                
                <asp:Panel ID="pnlOK" runat="server" HeaderText="Oklahoma">
                    <ContentTemplate>
                        <b>Oklahoma Energy Assistance Agencies for Electric Customers</b>
                        <br /><br />
                        <asp:GridView ID="gvOK" runat="server" DataSourceID="sqlDSOK" SkinID="GridView">
                            <Columns>
                                <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                            </Columns>                        
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlDSOK" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT county,agency,phone FROM agencies where state = 'Oklahoma' and type = 'electric' order by county,location">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlOK--%>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>