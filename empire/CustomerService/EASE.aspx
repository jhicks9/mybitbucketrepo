<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="EASE.aspx.vb" Inherits="CustomerService_EASE" title="EASE" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    Empire's Action to Support the Elderly (EASE) is designed to lift the burden of worry from the elderly (age 60 and older) and handicapped for customers in the Empire District Electric Company electric service area. For customers who qualify, late penalties are waived, due dates may be adjusted, deposits waived, and third party notification is available when an account becomes delinquent.
</asp:Content>

