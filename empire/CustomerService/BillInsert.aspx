<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="BillInsert.aspx.vb" Inherits="CustomerService_BillInsert" title="Bill Inserts" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>      
            
            <asp:LinkButton ID="lbAR" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/BillInsert.aspx?bill=AR" Text="Arkansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbKS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/BillInsert.aspx?bill=KS" Text="Kansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbMO" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/BillInsert.aspx" Text="Missouri"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbOK" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/BillInsert.aspx?bill=OK" Text="Oklahoma"/>
            <br />&nbsp
              
                <asp:Panel ID="pnlBI" runat="server" HeaderText="Bill Insert">
                    <ContentTemplate>
                        <asp:GridView ID="gvBI" runat="server" SkinID="GridView" DataSourceID="sqlDSBI">
                            <Columns>
                                <asp:TemplateField HeaderText="Type" SortExpression="pkgdescription">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# eval("pkgdescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Date" SortExpression="begdate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEndDate" runat="server" Text='<%# eval("enddate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="name">
                                    <ItemTemplate>
                                      <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                      <asp:HyperLink ID="hlViewInsert" runat="server" Target="_blank" Text='<%# Eval("name") %>' NavigateUrl='<%# Eval("id", "~/DisplayBinary.aspx?dtype=inserts&id={0}") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State" SortExpression="state">
                                    <ItemTemplate>
                                        <asp:Label ID="lblState" runat="server" Text='<%# eval("state") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Class" SortExpression="revenueclass">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRevenueClass" runat="server" Text='<%# eval("revenueclass") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                Not Available
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqlDSBI" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlBI--%>
                            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>