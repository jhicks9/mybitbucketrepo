
Partial Class CustomerService_BillInsert
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If String.IsNullOrEmpty(Request.QueryString("bill")) Then
                'If no variable is pass default to MO
                sqlDSBI.SelectCommand = "SELECT id,name,begdate,enddate,state,revenueclass,pkgdescription from billinsert where (state is null or state = 'MO') and (billinsert.begdate <= getdate()) AND (billinsert.enddate >= getdate()) order by billinsert.enddate desc"
                gvBI.DataBind()
                lbMO.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "bill=AR" Then
                sqlDSBI.SelectCommand = "SELECT id,name,begdate,enddate,state,revenueclass,pkgdescription from billinsert where (state is null or state = 'AR') and (billinsert.begdate <= getdate()) AND (billinsert.enddate >= getdate()) order by billinsert.enddate desc"
                gvBI.DataBind()
                lbAR.Font.Bold = True
                lbMO.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "bill=KS" Then
                sqlDSBI.SelectCommand = "SELECT id,name,begdate,enddate,state,revenueclass,pkgdescription from billinsert where (state is null or state = 'KS') and (billinsert.begdate <= getdate()) AND (billinsert.enddate >= getdate()) order by billinsert.enddate desc"
                gvBI.DataBind()
                lbKS.Font.Bold = True
                lbAR.Font.Bold = False
                lbMO.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "bill=OK" Then
                sqlDSBI.SelectCommand = "SELECT id,name,begdate,enddate,state,revenueclass,pkgdescription from billinsert where (state is null or state = 'OK') and (billinsert.begdate <= getdate()) AND (billinsert.enddate >= getdate()) order by billinsert.enddate desc"
                gvBI.DataBind()
                lbOK.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbMO.Font.Bold = False
            End If
        Catch
        End Try
    End Sub
  
End Class
