﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="TelevisionCommercials.aspx.vb" Inherits="CustomerService_TelevisionCommercials" title="Commercials" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            View Empire District television advertisements and informational videos by clicking on a link below or by visiting Empire District's
            <a href="http://www.youtube.com/channel/UCAZ5RA8lMtWFXRNp4M5-MaQ" target="_blank" >YouTube channel</a>.
            <br />
            <table style="border-collapse:separate;border-spacing:5px;width:100%">
                <tr>
                    <th colspan="3" align="left"><br />Informational ad campaigns</th>
                </tr>
                <tr>
                    <td style="width:34%"><asp:LinkButton ID="lbChallenges" runat="server" Text="Meeting Energy Challenges" ToolTip="" /></td>
                    <td style="width:33%"><asp:LinkButton ID="lbEmpireSafety" runat="server" Text="Safely Powering Your Life" ToolTip="" /></td>
                    <td style="width:33%"><asp:LinkButton ID="lbPowerFutureCapacity" runat="server" Text="Power the Future - Capacity" ToolTip="" /></td>
                </tr>
                <tr>
                    <td><asp:LinkButton ID="lbValueElectricty1" runat="server" Text="The Value of Electricity 1" ToolTip="" /></td>
                    <td><asp:LinkButton ID="lbPowerFutureReliability" runat="server" Text="Power the Future - Reliability" ToolTip="" /></td>
                    <td><asp:LinkButton ID="lbNaturalGas" runat="server" Text="Natural Gas, the Natural Choice" ToolTip="" /></td>
                </tr>
                <tr>
                    <td><asp:LinkButton ID="lbValueElectricty2" runat="server" Text="The Value of Electricity 2" ToolTip="" /></td>
                    <td><asp:LinkButton ID="lbPowerFutureEfficiency" runat="server" Text="Power the Future - Efficiency" ToolTip="" /></td>
                    <td></td>
                </tr>
                <tr>
                    <th colspan="3" align="left"><br />History and significant events</th>
                </tr>
                <tr>
                    <td><asp:LinkButton ID="lbOurTown" runat="server" Text="Our Town" ToolTip="" /></td>
                    <td><asp:LinkButton ID="lbCenturyService" runat="server" Text="A Century of Service" ToolTip="" /></td>
                    <td><asp:LinkButton ID="lbIceStorm" runat="server" Text="Ice Storm 2007" ToolTip="" /></td>
                </tr>
            </table>
            <br /><br />
            <asp:Panel ID="pnlYouTube" runat="server" Visible="false">
                <iframe id="ifYouTube" class="youtube-player" src="" runat="server" allowfullscreen frameborder="0" width="640" height="385" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

