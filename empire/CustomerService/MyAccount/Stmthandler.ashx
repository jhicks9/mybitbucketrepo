﻿<%@ WebHandler Language="VB" Class="StmtHandler" %>

Imports System
Imports System.Web
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO
Imports ibex4
Imports ibex4.logging

Public Class StmtHandler : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pdfStream As New MemoryStream
        Dim filetype As String = "application/pdf"
        Dim statementToken As String = ""
        Dim cwservice As New cw.Service        
        Try
            Dim tdes As New tripleDES
            Try
                Dim id As string = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(context.Request("data")))
                If Not id is nothing Then
                    statementToken = id
                End If
            Catch ex As Exception
            End Try

            Dim rtnStatement As String = cwservice.GetStatement(tdes.Encrypt(statementToken), "894")
            If Left(rtnStatement,5) = "Error" Then
                rtnStatement = "-,-"
            End If
            Dim rtnValueArray() As String = Split(rtnStatement, "-,-")
            If String.IsNullOrEmpty(rtnValueArray(0).ToString) Then
                Throw New Exception("document not found")
            Else
                'create the FO using xml/xsl
                Dim rs As XmlReaderSettings = New XmlReaderSettings()
                Dim xslt As XslCompiledTransform = New XslCompiledTransform()
                rs.ProhibitDtd = False 'rs.DtdProcessing = DtdProcessing.Parse  '**  net4.0 only
                rs.IgnoreWhitespace = True
                Dim x As XmlDocument = New XmlDocument
                x.LoadXml(rtnValueArray(1))
                Dim xmlNode As XmlNodeReader = New XmlNodeReader(x)
                xslt.Load(System.Xml.XmlReader.Create(xmlNode, rs))
                Dim xmldoc As XmlDocument = New XmlDocument
                xmldoc.LoadXml(rtnValueArray(0))
                Dim xmlNode2 As XmlNodeReader = New XmlNodeReader(xmldoc)
                Dim foStream As New System.IO.MemoryStream
                Using(foStream)
                    xslt.Transform(System.Xml.XmlReader.Create(xmlNode2, rs), Nothing, foStream)
                    'IBEX ***************** create pdf using generated FO
                    Dim key As String = "0002008082310A491433269D1E1B5F2ALLR/YAXSSIRD03WS9XZ5AG=="
                    ibex4.licensing.Generator.setRuntimeKey(key)
                    Dim doc As New FODocument
                    Using(pdfStream)
                        doc.generate(foStream, pdfStream, False)
                        context.Response.Clear()
                        context.Response.ContentType = "application/pdf"
                        context.Response.AddHeader("content-length", System.Convert.ToString(pdfStream.Length))
                        context.Response.AppendHeader("content-disposition", "attachment; filename=" & statementToken & ".pdf")  'give option to save file
                        context.Response.BinaryWrite(pdfStream.ToArray())
                        context.Response.End()
                    End Using 'pdfstream
                End Using 'fostream
                'end Ibex ************************                
            End If
        Catch ex As Exception
            ' ** displays message in browser
            'context.Response.Write("Document Not Found")
            ' ** or download error message as a file
            'Dim enc As New UTF8Encoding
            'Dim arrBytData() As Byte = enc.GetBytes(ex.Message)
            'pdfStream.Write(arrBytData, 0, arrBytData.Length)
            'filetype = "text/plain"
            'pdfStream.Position = 0
            'context.Response.AppendHeader("content-disposition", "attachment; filename=error.txt") 'download error message
        End Try       
    End Sub
         
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class