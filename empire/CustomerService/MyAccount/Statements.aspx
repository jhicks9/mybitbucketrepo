<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Statements.aspx.vb" Inherits="CustomerService_MyAccount_Statements" title="MyStatements" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>
<%@ Register TagPrefix="makepayment" TagName="uc5" Src="~/MakePayment.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvSummary">        
        <asp:View ID="vwAccounts" runat="server">
            <asp:DataList id="dlBillPkg" runat="server" >
                <ItemTemplate>
                    <b><asp:LinkButton id="LinkButton1" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                </ItemTemplate>
            </asp:DataList>                
        </asp:View>                    
                
        <asp:View ID="vwStatements" runat="server">
            <%--STATEMENTS--%>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="middle" align="left">
                        <asp:Label ID="lblAccount" runat="server" />
                    </td>
                    <td valign="middle" align="right">
                        <a href="javascript:PrintThisPage();" ><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a>
                    </td>
                </tr>
            </table>
                    
            <div id="print_content"><%--begin print content--%>
                <asp:GridView ID="dgStatements" runat="server" SkinID="GridView" AllowSorting="false" >
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Statement Date
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="left"/>
                            <ItemTemplate>
                                <a onclick="window.location.href='<%#resolveurl("~/CustomerService/MyAccount/Stmthandler.ashx?data=" & Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Eval("cust_statement_tkn")))) %>';" href="javascript:void(0)"><%#Eval("STMT_DATE") %></a>
                            </ItemTemplate>
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="cust_statement_tkn" HeaderText="Statement Token" SortExpression="cust_statement_tkn" Visible="false" />
                        <asp:BoundField ItemStyle-Width="150px" DataField="amount" HeaderStyle-HorizontalAlign="Left" HeaderText="Total Amount Due" SortExpression="amount" />
                        <asp:BoundField DataField="duedate" HeaderStyle-HorizontalAlign="Left" HeaderText="Delinquent Date" SortExpression="duedate" />
                    </Columns>
                </asp:GridView> 
            </div><%--end print content--%>
            <asp:Label ID="lblDirections" runat="server" Text="Select a date to display the statement." /><br />
            <%--END STATEMENTS--%>

            <%--INSERTS--%>
            <br /><b>Bill Inserts</b><br />
            <asp:DataList ID="dlInserts" runat="server" SkinID="DataList" DataSourceID="sqlDSInserts">
                <itemtemplate>
                    <asp:Image ID="imgRedFlag" ImageAlign="Top" ImageUrl="~/images/icon/icon_redflag.gif" runat="server" />&nbsp;
                    <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                    <asp:HyperLink ID="hlViewInsert" runat="server" Text='<%#Eval("name") %>' Target="_blank" NavigateUrl='<%# Eval("id", "~/DisplayBinary.aspx?dtype=inserts&id={0}") %>'></asp:HyperLink>
                </itemtemplate>
                <FooterTemplate>
                    <asp:Label ID="lblNoDataMsg" runat="server" Visible='<%# boolean.Parse((dlInserts.Items.Count=0).ToString())%>'>There are no inserts at this time.</asp:Label>
                </FooterTemplate>
            </asp:DataList>
            <%--END INSERTS--%>
            <br /><br />
            <makepayment:uc5 ID="uc5" runat="server" />
            <br />
            <asp:Panel ID="pnlNote" runat="server" Visible="true" Width="100%">
                <div style="background-color:#113962;color:White;text-align:center;Font-weight:bold;border:solid 1px black;">*Note</div>
                <div style="background-color:#cccccc;color:black;border:solid 1px black;text-align:center;">Your statement will open in a new window. If it does not open you may need to allow pop-ups or disable any pop-up blockers depending on your browser.</div>
            </asp:Panel>

        </asp:View>
    </asp:MultiView>
    
    <asp:SqlDataSource ID="sqlDSInserts" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="">
    </asp:SqlDataSource>

    <script type="text/javascript">
    function PrintThisPage() 
    { 
       var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
             sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
       var winprint=window.open("<%=printurl %>?t=Statements","Print",sOption); 
       winprint.focus(); 
    }
    </script>    
</asp:Content>