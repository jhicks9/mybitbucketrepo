Imports System.Data
Imports System
Imports System.Web
Imports System.Drawing
Imports System.Configuration
Imports System.Web.UI.WebControls
Imports Dundas.Charting.WebControl
Imports System.Math

Partial Class CustomerService_MyAccount_Usage
    Inherits System.Web.UI.Page
    Dim billpToken As String = ""
    Dim acctTkn, acctPkgTkn As String
    Dim history As String = ""
    Dim flgUsage As Boolean = False
    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Dim ds As New DataSet
    Dim custAccount As String
    Dim rtnAcctTkns As New System.Data.DataSet
    Public printurl As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Profile.CustomerAccountNumber Is Nothing Then
            Try
                printurl = ResolveUrl("~/print.aspx")
                cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
                encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
                Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)

                If rtnBillPackages Is Nothing Then
                    Dim lbl As New System.Web.UI.WebControls.Label
                    lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                    lbl.ForeColor = Drawing.Color.Red
                    pnlAccount.Controls.Add(lbl)
                Else
                    'Dim billPkgTkn As Array = Split(rtnBillPackages, ",")
                    ''*********************************************************************
                    ''*** uncomment the if statement to bypass displaying dlBillPkg     ***
                    ''*** this is commented out because busy will not fire on page load ***
                    ''*** revisit once live and performance can be measured             ***
                    ''*********************************************************************
                    ''If billPkgTkn.Length > 1 Then
                    'dlBillPkg.DataSource = billPkgTkn
                    'dlBillPkg.DataBind()
                    'dlBillPkg.Visible = True
                    ''Else
                    ''Dim rtnAcctTkns As New System.Data.DataSet
                    ''dlBillPkg.Visible = False
                    ''txtBillPkg = billPkgTkn(0)
                    ''cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
                    ''encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
                    ''rtnAcctTkns = cwservice.GetAccountTokens(encrypt, billPkgTkn(0))
                    ''dgAccountTokens.DataSource = rtnAcctTkns
                    ''dgAccountTokens.DataBind()
                    ''lblAccount.Text = "Select an account to view usage for " & Profile.CustomerAccountNumber & "-" & billPkgTkn(0)
                    ''mvUsage.ActiveViewIndex = 0
                    ''End If

                    Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                    Dim dt As New DataTable()
                    Dim dr As DataRow
                    dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                    dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                    dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                    For i = 0 To billPkgTkn.Length - 1
                        dr = dt.NewRow()
                        dr("custToken") = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
                        dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                        dr("cdToken") = cd.CalculateCheckDigit(cd.ZeroPad(Profile.CustomerAccountNumber, 6) & cd.ZeroPad(billPkgTkn(i), 2))
                        dt.Rows.Add(dr)
                    Next i
                    dlBillPkg.DataSource = dt
                    dlBillPkg.DataBind()

                    history = DdlHistory.SelectedValue
                End If
            Catch
            End Try
        End If
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            'Dim billpToken As String = ""
            Dim cdToken As String = ""
            Dim arTokens As Array = Split(e.CommandArgument, ";")
            billpToken = arTokens(0)
            cdToken = arTokens(1)

            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, billpToken)
            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            lblAccount.Text = "Select an account to view usage for " & Profile.CustomerAccountNumber & "-" & billpToken & "-" & cdToken
            mvUsage.ActiveViewIndex = 0
            pnlAccount.Visible = False
        Catch

        End Try
    End Sub

    Protected Sub DdlHistory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlHistory.SelectedIndexChanged
        Try
            Dim strUnit As String = ""
            Dim lblUnit As WebControls.Label
            If txtServiceType.Text = "Electric" Then
                strUnit = "KWH"
            ElseIf txtServiceType.Text = "Water" Then
                strUnit = "KGAL"
            ElseIf txtServiceType.Text = "Gas" Then
                strUnit = "CCF"
            End If

            testChart()

            Dim gvRow As GridViewRow
            For Each gvRow In gvMeterReads.Rows
                lblUnit = gvRow.Cells(0).FindControl("lblUnit")
                lblUnit.Text = strUnit
            Next

        Catch ex As Exception

        End Try
    End Sub
    Public Sub testChart()
        'Dundas
        Try
            Dim custUsage As Double
            Dim custUsageDate As Date
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            '*** Chart Data ***
            Dim rtnChartData As String = cwservice.GetChartInfo(encrypt, txtAcctTkn.Text, txtAcctPkgTkn.Text, DdlHistory.SelectedValue, txtSPA.Text, txtSPT.Text, txtPT.Text)
            Dim usageFields As Array = Split(rtnChartData, ";")
            Dim customerUsage As Array = Split(usageFields(0), ",")
            Dim customerUsageDate As Array = Split(usageFields(1), ",")
            Dim customerRead As Array = Split(usageFields(2), ",")


            Dim x As Integer
            For x = 0 To customerUsage.Length - 1
                custUsage = CType(customerUsage(x), Double)
                custUsageDate = CType(customerUsageDate(x), Date)
                Chart1.Series("Series 1").Points.AddXY(custUsageDate.Date, custUsage)
            Next
            Chart1.Titles(0).Text = DdlHistory.SelectedItem.Text & " usage from " & customerUsageDate(customerUsageDate.GetLowerBound(0)) & " to " & customerUsageDate(customerUsageDate.GetUpperBound(0))
            Chart1.Series("Series 1").Color = Color.DodgerBlue
            Chart1.Series("Series 1").XValueType = ChartValueTypes.Date
            Chart1.Series("Series 1").XValueIndexed = True
            Chart1.Series("Series 1").Type = SeriesChartType.Column
            Chart1.Series("Series 1").ToolTip = "#VALY" & " on " & "#VALX"

            mvUsage.ActiveViewIndex = 1
            Panel1.Visible = True

            '**** Meter Read Data
            Dim rtnUsage As DataSet = cwservice.GetUsageInfo(encrypt, txtAcctTkn.Text, txtAcctPkgTkn.Text, DdlHistory.SelectedValue, txtSPA.Text, txtSPT.Text, txtPT.Text)
            gvMeterReads.DataSource = rtnUsage
            gvMeterReads.DataBind()
            gvMeterReads.Visible = True

        Catch
            'lblMessage.Text = "Not enough Data to render comparison"
        End Try
    End Sub
    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        'System.Threading.Thread.Sleep(3000)
        Try
            Dim eCmdArg As String = e.CommandArgument
            Dim arTokens As Array = Split(eCmdArg, ",")
            Dim showDemand As Boolean = False

            'txtAcctTkn.Text = e.CommandArgument
            txtAcctTkn.Text = arTokens(0)
            txtSPA.Text = arTokens(1)
            txtAcctPkgTkn.Text = e.CommandName
            txtServiceType.Text = arTokens(2)
            Dim strType As String = arTokens(2) & " usage for service at:"
            Dim strAddress As String = arTokens(3) & " " & arTokens(4) & "," & arTokens(5)
            txtSPT.Text = arTokens(6)
            txtPT.Text = arTokens(7)
            'Dim LinkButton1 As LinkButton = Me.dgAccountTokens.Rows(e.CommandSource).Cells(0).FindControl("LinkButton1")
            lblUsage.Text = strType
            lblUsage.Font.Size = "14"
            lblUsage.ForeColor = Color.Navy
            lblAddress.Text = strAddress
            lblAddress.Font.Size = "14"
            lblAddress.ForeColor = Color.Navy

            Dim strUnit As String = ""
            Dim lblUnit As WebControls.Label
            If arTokens(2) = "Electric" Then
                strUnit = "KWH"
            ElseIf arTokens(2) = "Water" Then
                strUnit = "KGAL"
            ElseIf arTokens(2) = "Gas" Then
                strUnit = "CCF"
            End If

            testChart()

            Dim gvRow As GridViewRow
            For Each gvRow In gvMeterReads.Rows
                lblUnit = gvRow.Cells(0).FindControl("lblUnit")
                lblUnit.Text = strUnit
                If (IsNumeric(gvRow.Cells(4).Text)) And (System.Convert.ToSingle(gvRow.Cells(4).Text) > 0) Then
                    showDemand = True
                End If
                If (IsNumeric(gvRow.Cells(5).Text)) And (System.Convert.ToSingle(gvRow.Cells(5).Text) > 0) Then
                    showDemand = True
                End If
            Next
            If showDemand Then
                'leave the demand read and demand billing columns active
            Else
                'hide the demand read and demand billing columns if data not present
                gvMeterReads.Columns(4).Visible = False
                gvMeterReads.Columns(5).Visible = False
            End If
        Catch

        End Try
    End Sub

    Protected Sub dgAccountTokens_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgAccountTokens.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim LinkButton1 As LinkButton = CType(e.Row.FindControl("LinkButton1"), LinkButton)
            Dim desc As String = LinkButton1.Text.Split(" - ")(0).Trim
            Select Case desc  'disable link if not electric, gas, or water
                Case "Electric"
                Case "Gas"
                Case "Water"
                Case Else
                    LinkButton1.Text = desc & " - Usage not available"
                    LinkButton1.Enabled = False
            End Select
        End If
    End Sub
End Class
