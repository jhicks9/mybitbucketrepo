Imports System.Data
Imports System
Imports System.Web
Imports System.Net
Partial Class CustomerService_MyAccount_Payments
    Inherits System.Web.UI.Page
    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES
    Dim ds As New DataSet
    Dim cd As New checkDigit
    Dim custAccount As String = ""
    Public printurl As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            printurl = ResolveUrl("~/print.aspx")
            If Not Profile.CustomerAccountNumber Is Nothing Then
                showBillPackages()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub showBillPackages()
        Try
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)

            If rtnBillPackages Is Nothing Then
                Dim lbl As New Label
                lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                lbl.ForeColor = Drawing.Color.Red
                vwTokens.Controls.Add(lbl)
            Else
                Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                Dim dt As New DataTable()
                Dim dr As DataRow
                dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                For i = 0 To billPkgTkn.Length - 1
                    dr = dt.NewRow()
                    dr("custToken") = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
                    dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                    dr("cdToken") = cd.CalculateCheckDigit(cd.ZeroPad(Profile.CustomerAccountNumber, 6) & cd.ZeroPad(billPkgTkn(i), 2))
                    dt.Rows.Add(dr)
                Next i
                dlBillPkg.DataSource = dt
                dlBillPkg.DataBind()
            End If
        Catch
        End Try
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim rtnValues As New System.Data.DataSet
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim command As String = e.CommandArgument
            dlBillPkg.Visible = False
            showPayments(command)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub showPayments(ByVal commandname As String)
        Try
            Dim rtnPayment As New System.Data.DataSet
            Dim rtnPaymentDetail As New System.Data.DataSet
            Dim rtnBalance As String

            Dim billpToken As String = ""
            Dim cdToken As String = ""
            Dim arTokens As Array = Split(commandname, ";")
            billpToken = arTokens(0)
            cdToken = arTokens(1)
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            rtnPayment = cwservice.GetPaymentInfo(encrypt, billpToken, "460")

            rtnBalance = cwservice.GetCurrentDue(encrypt, billpToken)
            dgPayment.DataSource = rtnPayment
            dgPayment.DataBind()

            MultiView1.ActiveViewIndex = 1
            lblAccount.Text = "Payment(s) for " & Profile.CustomerAccountNumber & "-" & billpToken & "-" & cdToken
            lblBalanceDue.Text = "Current Balance Due: " & rtnBalance
        Catch

        End Try
    End Sub
End Class
