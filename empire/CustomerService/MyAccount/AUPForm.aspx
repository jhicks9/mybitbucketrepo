﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AUPForm.aspx.vb" Inherits="CustomerService_MyAccount_AUPForm" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlBillPackages" runat="server" Visible="True">
                <asp:DataList id="dlBillPkg" runat="server">
                    <ItemTemplate>
                    <b><asp:LinkButton id="LinkButton1" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                    </ItemTemplate>
                </asp:DataList>
                <asp:HiddenField ID="hdnBillPackage" runat="server" />
            </asp:Panel>

            <asp:Panel ID="pnlForm" runat="server" Visible="false">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top" align="left">Enrolling in Auto-Pay or making any changes to your Auto-Pay information may take at least one billing cycle to go into effect. Customers are responsible for reviewing their bill and ensuring their amount due has been paid. For further information or questions please call Empire District.<br /><br />Electric customers: 800-206-2300<br />Gas customers: 800-424-0427</td>
                        <td valign="top" align="right"><img id='imgService' src='~/images/payments-autopay.jpg' runat='server' alt='' /></td>
                    </tr>
                </table>
                <font style="color:#113962"><b>Auto-Pay Enrollment</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="2">
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblAccount_" Text="Account Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><asp:label ID="lblAccount" runat="server"/><asp:label ID="lblLocation" runat="server" Visible="false" /></td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblEmail_" Text="Email:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><asp:label ID="lblEmail" runat="server" /></td>
                        </tr>
                        <tr valign="top">
                            <td align="right" style="width:170px"><asp:label ID="lblName" Text="* Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" ValidationGroup="avg"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblPhone" Text="* Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPhone" runat="server" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone"
                                    Display="Dynamic" ErrorMessage="Phone Number format xxx-xxx-xxxx " ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                                    ValidationGroup="avg" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblAddress" Text="* Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server" ValidationGroup="avg" />
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblState" Text="* State:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtState" runat="server" ValidationGroup="avg" Width="25px" />
                                <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="txtState" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblZip" Text="* Zip:" runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtZip" runat="server" ValidationGroup="avg" MaxLength="10" Width="75px" />
                                <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                    </table>
                    <br />

                    <asp:Panel ID="pnlNewEnroll" runat="server" Visible="False">
                        <table style="border-collapse:collapse;border-spacing:0;width:100%">
                            <tr>
                                <td><asp:CheckBox ID="chkNewEnrollment" runat="server" TextAlign="Right" Checked="false" Text="" /></td>
                                <td>YES! I want to take advantage of Auto-Pay and hereby authorize Empire District to charge my monthly <asp:Label ID="lblServiceType" runat="server" Text="" /> bill(s)</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    electronically to my
                                    <asp:RadioButtonList ID="rblNEAccountType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                        <asp:ListItem Value="Checking" Selected="True">Checking</asp:ListItem>
                                        <asp:ListItem Value="Savings">Savings</asp:ListItem>
                                    </asp:RadioButtonList>
                                    account at:<br /><br />
                                </td>
                            </tr>
                        </table>                        
                        <table cellpadding="0" cellspacing="2">
                            <tr valign="top">
                                <td align="right" style="width:170px"><asp:label ID="lblNEBankName" Text="* Bank Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:TextBox ID="txtNEBankName" runat="server" ValidationGroup="avg"></asp:TextBox>
                                </td>
                                <td rowspan="4" >
                                    <div id="gallery">
                                        <a href="~/images/payments-autopay-checksample.gif" runat="server" title="Click to enlarge" tabindex="20">
                                        <img id="imgchecksample1" src="~/images/payments-autopay-checksample-thumb.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
                                    </div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right"><asp:label ID="lblNEBankRouting" Text="* Bank Routing Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:TextBox ID="txtNEBankRouting" runat="server" ValidationGroup="avg"></asp:TextBox>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right"><asp:label ID="lblNEBankAccount" Text="* Bank Account Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:TextBox ID="txtNEBankAccount" runat="server" ValidationGroup="avg"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><asp:label ID="lblNEDraftDate" Text="Auto-Pay Draft Date:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:DropDownList ID="cboNEDraftDate" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><div id="gallerylink"><a href="~/images/payments-autopay-checksample.gif" runat="server">example routing and account numbers</a></div></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="font-size:smaller">We reserve the right to select an alternate date if the one you select will not work with your current due date.</td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    
                    <asp:Panel ID="pnlUpdateAUP" runat="server" Visible="False">
                        <asp:CheckBox ID="chkUpdateBank" runat="server" TextAlign="Right" Checked="false" Text="YES! I am currently enrolled in Auto-Pay and wish to change my bank information." /><br />
                        <table cellpadding="0" cellspacing="2">
                            <tr valign="top">
                                <td align="right"></td>
                                <td colspan="2">
                                    <asp:RadioButtonList ID="rblUpdType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                        <asp:ListItem Value="Changing Banks" Selected="True">Changing Banks</asp:ListItem>
                                        <asp:ListItem Value="New account at the same bank">New account at the same bank</asp:ListItem>
                                    </asp:RadioButtonList>                            
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right" style="width:170px"><asp:label ID="lblUpBankName" Text="* Bank Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:TextBox ID="txtUpBankName" runat="server" ValidationGroup="avg" />
                                </td>
                                <td rowspan="4">
                                    <div id="gallery">
                                        <a href="~/images/payments-autopay-checksample.gif" runat="server" title="Click to enlarge">
                                        <img id="imgchecksample2" src="~/images/payments-autopay-checksample-thumb.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
                                        <div><a href="~/images/payments-autopay-checksample.gif" runat="server">example routing and account numbers</a></div>
                                    </div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right"><asp:label ID="lblUpBankRouting" Text="* Bank Routing Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:TextBox ID="txtUpBankRouting" runat="server" ValidationGroup="avg"></asp:TextBox>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right"><asp:label ID="lblUpBankAccount" Text="* Bank Account Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:TextBox ID="txtUpBankAccount" runat="server" ValidationGroup="avg"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br /><br />
                        <asp:CheckBox ID="chkUpdateDraft" runat="server" TextAlign="Right" Checked="false" Text="YES! am currently enrolled in Auto-Pay and wish to change my draft date to:" /><br />
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td align="right" style="width:170px"><asp:label ID="lblUpDraftDate" Text="Auto-Pay Draft Date:&nbsp;" runat="server" Font-Bold="true" /></td>
                                <td>
                                    <asp:DropDownList ID="cboUpDraftDate" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="font-size:smaller">We reserve the right to select an alternate date if the one you select will not work with your current due date.</td>
                            </tr>
                        </table>
                        <br />
                    </asp:Panel>

                </div>
                * (indicates required fields)
                <br /><br />
                <asp:CheckBox ID="ckbAgree" runat="server" AutoPostBack="true" Text="I understand that I may cancel this agreement at any time simply by contacting Empire District." />
                <br /><asp:Label ID="lblStatus" runat="server" Font-Bold="true" ForeColor="Red" /><br />
                <div style="float:left">
                    <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSubmit_Click" Enabled="false"><span>Continue</span></asp:LinkButton>
                    <div style="float:left;width:6px;">&nbsp;</div>
                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" CausesValidation="false" PostBackUrl="~/customerservice/myaccount/default.aspx"><span>Cancel</span></asp:LinkButton>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlFinal" runat="server" Visible="False">
                <div style="padding:15px">
                    <asp:Label ID="lblEmailSent" runat="server" />
                    <br /><br />
                    <asp:LinkButton ID="lbContinue" runat="server" CssClass="ovalbutton" CausesValidation="false" PostBackUrl="~/customerservice/myaccount/default.aspx"><span>Continue</span></asp:LinkButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        //jQuery("#gallery a").slimbox();
        $(function () {
            $('#gallery a').live('mouseover', function () {
                jQuery(this).slimbox();
            });
            $('#gallerylink a').live('mouseover', function () {
                jQuery(this).slimbox();
            });
        });
    </script>
</asp:Content>

