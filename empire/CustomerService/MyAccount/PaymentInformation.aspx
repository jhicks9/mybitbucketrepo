﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="PaymentInformation.aspx.vb" Inherits="CustomerService_MyAccount_PaymentInformation" %>
<%@ Register TagPrefix="makepayment" TagName="uc5" Src="~/MakePayment.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <makepayment:uc5 ID="uc5" runat="server" />
    <div class="landingtext">
        <br />
        <asp:Label ID="txtList" runat="server" Text="" />
    </div>
</asp:Content>

