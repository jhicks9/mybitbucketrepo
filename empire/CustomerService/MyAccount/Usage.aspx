<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="usage.aspx.vb" Inherits="CustomerService_MyAccount_Usage" title="MyUsage" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:TextBox ID="txtAcctTkn" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtAcctPkgTkn" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtSPA" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtServiceType" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtSPT" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtPT" runat="server" Visible="false"></asp:TextBox>
        <asp:panel ID="pnlAccount" runat="server">
            <asp:DataList id="dlBillPkg" runat="server" >
                <ItemTemplate>
                    <b><asp:LinkButton id="LinkButton1" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                </ItemTemplate>
            </asp:DataList>
        </asp:panel>
        
        <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvUsage">
            <asp:View ID="vwAccounts" runat="server">
                <br />
                <asp:Label ID="lblAccount" runat="server"></asp:Label>
                <br /><br />
                
                <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList" AllowPaging="false" >
                    <Columns>
                        <asp:TemplateField>
                           <ItemTemplate>
                                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE") & "," & Eval("service_point_tkn") & "," & Eval("premise_tkn")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                   Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                                   <!--& "&nbsp;&nbsp;" & Left(Eval("POSTAL_CODE"),5) & "-" & Right(Eval("POSTAL_CODE"),4)-->
                           </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No usage found.
                    </EmptyDataTemplate>
                </asp:GridView> 
            </asp:View>
            
            <asp:View ID="vwChart" runat="server"> 

                <div style="padding:0% 2% 0% 0%;width:175px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" ><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                <div id="print_content"><%--begin print content--%>
                <div style="padding:0% 0% 0% 0%;float:left;"><asp:Label ID="lblUsage" runat="server"></asp:Label></div>
                <div style="clear:both;"></div>
                
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
                <asp:Panel ID="Panel1" visible="false" runat="server">
                    <div style="float:left;">
                        <dcwc:chart id="Chart1" runat="server" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#D3DFF0" Width="575px" Height="250px" BorderLineColor="26, 59, 105" Palette="Dundas" borderlinestyle="Solid" backgradienttype="TopBottom" borderlinewidth="2">
		                    <titles>
                                <DCWC:Title Color="26, 59, 105" Font="Trebuchet MS, 14.25pt, style=Bold" Name="Title1"
                                    ShadowColor="32, 0, 0, 0" ShadowOffset="3" Text="Current Usage in KWH">
                                </DCWC:Title>
                                <DCWC:Title Color="26, 59, 105" Font="Trebuchet MS, 14.25pt, style=Bold" Name="Title2"
                                    ShadowColor="32, 0, 0, 0" ShadowOffset="3">
                                </DCWC:Title>
		                    </titles>
		                    <legends>
                                <DCWC:Legend AutoFitText="False" BackColor="Transparent" Enabled="False" Font="Trebuchet MS, 8.25pt, style=Bold"
                                    Name="Default">
                                </DCWC:Legend>
		                    </legends>
		                    <borderskin skinstyle="Emboss"></borderskin>
		                    <series>
			                    <dcwc:series XValueType="DateTime" Name="Series 1" BorderColor="120, 64, 64, 64" Color="220, 65, 140, 240" YValueType="Double" font="Trebuchet MS, 8.25pt, style=Bold"></dcwc:series>
                                <DCWC:Series Name="Series 2">
                                </DCWC:Series>
		                    </series>
		                    <chartareas>
			                    <dcwc:chartarea Name="Default" BorderColor="" BackGradientEndColor="Transparent" BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientType="TopBottom">
				                    <area3dstyle enable3d="True" wallwidth="0" Light="Realistic"></area3dstyle>
				                    <axisy linecolor="64, 64, 64, 64">
					                    <labelstyle font="Trebuchet MS, 8.25pt, style=Bold" Format="N0"></labelstyle>
					                    <majorgrid linecolor="64, 64, 64, 64"></majorgrid>
				                    </axisy>
				                    <axisx linecolor="64, 64, 64, 64">
					                    <labelstyle font="Trebuchet MS, 8.25pt, style=Bold" Format="Y"></labelstyle>
					                    <majorgrid linecolor="64, 64, 64, 64" Enabled="False"></majorgrid>
				                    </axisx>
			                    </dcwc:chartarea>
		                    </chartareas>
	                    </dcwc:chart>
                         <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        <br /><br />
                            &nbsp; &nbsp; &nbsp;Change the comparison period:   
                        <asp:DropDownList ID="DdlHistory" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="396" Selected="True">One year</asp:ListItem>
                            <asp:ListItem Value="781" >Two years</asp:ListItem>
                        </asp:DropDownList>
	                </div>	  
	                <br style="clear:both;" /><br /> 
                </asp:Panel>
                
                <asp:GridView ID="gvMeterReads" runat="server" SkinID="GridView" AllowPaging="false" AllowSorting="false" >
                    <Columns>
                        <asp:BoundField DataField="READ_DATE" HeaderStyle-HorizontalAlign="Left" HeaderText="Read Date" SortExpression="READ_DATE" />
                        <asp:BoundField DataField="USAGE_READ_NUM" HeaderStyle-HorizontalAlign="Left" HeaderText="Meter Read" SortExpression="USAGE_READ_NUM" />
                        <asp:BoundField DataField="USAGE_NUM" HeaderStyle-HorizontalAlign="Left" HeaderText="Usage Amount" SortExpression="USAGE_NUM" />                        
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Unit
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label ID="lblUnit" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DEMAND_READ" HeaderStyle-HorizontalAlign="Left" HeaderText="Demand Read" SortExpression="DEMAND_READ" />
                        <asp:BoundField DataField="DEMAND_BILLED" HeaderStyle-HorizontalAlign="Left" HeaderText="Demand Billed" SortExpression="DEMAND_BILLED" />
                        <asp:BoundField DataField="ESTIMATE_IND" HeaderStyle-HorizontalAlign="Left" HeaderText="Estimate" SortExpression="ESTIMATE_IND" />
                        <asp:BoundField DataField="USAGE_DAYS_CNT" HeaderStyle-HorizontalAlign="Left" HeaderText="Usage Days" SortExpression="USAGE_DAYS_CNT" />                    
                    </Columns>
                </asp:GridView>
                    
                </div><%--end print content--%>
            </asp:View>
        </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Usage","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>