﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PostData.aspx.vb" Inherits="customerservice_MyAccount_PostData" %>
<%@ Register TagPrefix="sidebar" TagName="uc1" Src="~/sidebar.ascx" %>
<%@ Register TagPrefix="footer" TagName="uc2" Src="~/footer.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Empire District</title>
    <link id="stylesheet" href="~/empire.css" rel="stylesheet" runat="server" type="text/css" />
</head>
<body>
    <form id="form1" action="http://c03.apogee.net/calcs/rescalc5x/bi/?utilityID=empiredistrict&hostheader=empiredistrict" method="post">
    
        <div id="pagecontainer" class="pagecontainer">
            <div style="height:10px;background-color:#0D2F57">
                &nbsp;
            </div><%--end header--%>
            
	        <div class="content">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="leftcol" valign="top">
                            <sidebar:uc1 ID="sidebar" runat="server" navMenu="false" ImageUrl="~/images/myaccount-calcsidebar.jpg" />
                        </td>
                        <td class="rightcol" valign="top">
                            <asp:Label ID="lblOutput" runat="server" Text=""></asp:Label><br /><br />
                            <input type="hidden" id="data" runat="server"/>
                            <input type="submit" value="Start Calculator" />
                        </td>
                    </tr>
                </table>
	        </div><%--end content--%>
        </div><%--end pagecontainer--%>
        
        <div class="footer">
            <footer:uc2 ID="footer1" runat="server" />
        </div><%--end footer--%>
    </form>    
</body>
</html>
