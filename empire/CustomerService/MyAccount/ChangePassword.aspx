<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="CustomerService_MyAccount_ChangePassword" title="Change MyPassword" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:ChangePassword ID="ChangePassword1" runat="server" CancelDestinationPageUrl="~/CustomerService/MyAccount/Default.aspx" ChangePasswordTitleText="" ContinueDestinationPageUrl="~/CustomerService/MyAccount/Default.aspx" SuccessTitleText="">
        <ChangePasswordTemplate>
            <br />
            <div style="float:left;width:150px;padding-top:3px;">
                <asp:Label ID="CurrentPasswordLabel" runat="server" width="150px" AssociatedControlID="CurrentPassword">Password:</asp:Label>
            </div>
            <div style="float:left;width:230px;">
                <asp:TextBox ID="CurrentPassword" runat="server" Width="190px" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
            </div>
            <br style="clear:both;"/>
            <br />
            <div style="float:left;width:150px;padding-top:3px;">
                <asp:Label ID="NewPasswordLabel" runat="server" width="150px" AssociatedControlID="NewPassword">New Password:</asp:Label>
            </div>
            <div style="float:left;width:230px;">
                <asp:TextBox ID="NewPassword" runat="server" Width="190px" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                    ErrorMessage="New Password is required." ToolTip="New Password is required."
                    ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
            </div>
            <br style="clear:both;"/>
            <br />
            <div style="float:left;width:150px;padding-top:3px;">
                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" width="150px" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
            </div>
            <div style="float:left;width:230px;">
                <asp:TextBox ID="ConfirmNewPassword" runat="server" Width="190px" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                    ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                    ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
            </div>
            <br style="clear:both;"/>
            <br />
            <div style="color:Red;">
                <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                    ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                    ValidationGroup="ChangePassword1"></asp:CompareValidator>
                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
            </div>    
            <div style="float:left;margin-left:155px;">
                <asp:LinkButton ID="lbChange" runat="server" CssClass="ovalbutton" CommandName="ChangePassword" ValidationGroup="ChangePassword1" ><span>Change Password</span></asp:LinkButton>
                <div style="float:left;width:6px;">&nbsp;</div>
                <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" CausesValidation="false" CommandName="Cancel" ><span>Cancel</span></asp:LinkButton>
            </div>
            <br style="clear:both;"/>
        </ChangePasswordTemplate>
    </asp:ChangePassword>
    <asp:Label ID="lblError" runat="server" Visible="false"></asp:Label>
</asp:Content>

