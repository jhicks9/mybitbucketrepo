﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomerService_MyAccount_AUPForm
    Inherits System.Web.UI.Page

    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Dim email As New clientInfo

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim custToken As String = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
        Dim billPkgTkn As String = cd.ZeroPad(Profile.BillingPackageToken, 2)
        Dim acctpToken As String = ""
        Dim acctToken As String = ""

        If (Not Page.IsPostBack) Then
            lbSubmit.ForeColor = Drawing.Color.Gray
            showBillPackages()
        Else
            If (ckbAgree.Checked) Then
                lbSubmit.ForeColor = Drawing.Color.DarkBlue
                lbSubmit.Enabled = True
            Else
                lbSubmit.ForeColor = Drawing.Color.Gray
                lbSubmit.Enabled = False
            End If
        End If
    End Sub

    Public Sub showBillPackages()
        Try
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)

            If rtnBillPackages Is Nothing Then  'no valid billing packages
                Dim lbl As New Label
                lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                lbl.ForeColor = Drawing.Color.Red
                pnlBillPackages.Controls.Add(lbl)
            Else
                Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                Dim dt As New DataTable()
                Dim dr As DataRow
                dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                For i = 0 To billPkgTkn.Length - 1
                    dr = dt.NewRow()
                    dr("custToken") = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
                    dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                    dr("cdToken") = cd.CalculateCheckDigit(cd.ZeroPad(Profile.CustomerAccountNumber, 6) & cd.ZeroPad(billPkgTkn(i), 2))
                    dt.Rows.Add(dr)
                Next i
                dlBillPkg.DataSource = dt
                dlBillPkg.DataBind()
            End If
        Catch
        End Try
    End Sub

    Public Function isGasCustomer(ByVal custToken As String, ByVal bpToken As String) As Boolean
        Dim result As Boolean = False
        Try
            Dim service As String = ""
            Dim increment As Integer = 0

            Dim rtnAcctTkns As New System.Data.DataSet
            encrypt = tdes.Encrypt(custToken)
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, bpToken)

            For Each dr In rtnAcctTkns.Tables(0).Rows
                service = LCase(rtnAcctTkns.Tables(0).Rows(increment).Item("ACCOUNT_PKG_DESC"))
                Select Case service
                    Case "gas"
                        result = True
                End Select
                increment = increment + 1
            Next
        Catch
        End Try
        Return result
    End Function

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim ci As New clientInfo
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim arTokens As Array = Split(e.CommandArgument, ";")  'arTokens(0) = billpToken

            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            hdnBillPackage.Value = arTokens(0)

            lblAccount.Text = Profile.CustomerAccountNumber & "-" & hdnBillPackage.Value & "-" & cd.CalculateCheckDigit(Profile.CustomerAccountNumber & hdnBillPackage.Value)

            'get email from customer watch
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            lblEmail.Text = cwservice.GetEmailAddress(encrypt, Profile.BillingPackageToken)
            If ci.IsEmail(lblEmail.Text) Then
            Else
                'get email from membership services
                lblEmail.Text = Membership.GetUser(User.Identity.Name).Email
            End If

            'populate due date availability
            If isGasCustomer(Profile.CustomerAccountNumber, hdnBillPackage.Value) Then
                cboUpDraftDate.Items.Add("1st")
                cboUpDraftDate.Items.Add("5th")
                cboUpDraftDate.Items.Add("20th")
                cboUpDraftDate.Items.Add("25th")
                cboNEDraftDate.Items.Add("1st")
                cboNEDraftDate.Items.Add("5th")
                cboNEDraftDate.Items.Add("20th")
                cboNEDraftDate.Items.Add("25th")
                lblServiceType.Text = "Gas"
            Else
                cboUpDraftDate.Items.Add("1st")
                cboUpDraftDate.Items.Add("5th")
                cboUpDraftDate.Items.Add("10th")
                cboUpDraftDate.Items.Add("15th")
                cboUpDraftDate.Items.Add("20th")
                cboUpDraftDate.Items.Add("25th")
                cboNEDraftDate.Items.Add("1st")
                cboNEDraftDate.Items.Add("5th")
                cboNEDraftDate.Items.Add("10th")
                cboNEDraftDate.Items.Add("15th")
                cboNEDraftDate.Items.Add("20th")
                cboNEDraftDate.Items.Add("25th")
                lblServiceType.Text = "Electric"
            End If

            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            Dim onAUP As String = cwservice.GetAutoPayStatus(encrypt, hdnBillPackage.Value)

            If onAUP = "Yes" Then
                pnlForm.Visible = True
                pnlBillPackages.Visible = False
                pnlUpdateAUP.Visible = True
            Else
                pnlForm.Visible = True
                pnlBillPackages.Visible = False
                pnlNewEnroll.Visible = True
            End If

        Catch
        End Try
    End Sub

    Public Sub LogData()
        Dim ci As New clientInfo
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim drLookup As SqlDataReader
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim recordExists As Boolean = False
        Try
            Dim insertquery As String = "insert into log (date,ipaddress,refer,username,web) VALUES (GETDATE(),'" & ci.GetIP4Address() & "','" & lblAccount.Text & "','AUP','enrollment date " & DateTime.Now & "')"
            Dim updatequery As String = "update log set log.date=GETDATE(),log.ipaddress='" & ci.GetIP4Address() & "' where username='AUP' and log.refer='" & lblAccount.Text & "'"
            conn.Open()
            cmdLookup.CommandText = "SELECT count(*) as result from log where username='AUP' and log.refer = '" & lblAccount.Text & "'"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            If recordExists Then  'add record if it doesnt exist, otherwise update record
                cmd.CommandText = updatequery
            Else
                cmd.CommandText = insertquery
            End If
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim emailBody As New StringBuilder
        Dim emailSubject As String = ""
        Try
            If Not chkUpdateBank.Checked And Not chkNewEnrollment.Checked And Not chkUpdateDraft.Checked Then
                chkUpdateBank.ForeColor = System.Drawing.Color.Red
                chkNewEnrollment.ForeColor = System.Drawing.Color.Red
                chkUpdateDraft.ForeColor = System.Drawing.Color.Red
                lblStatus.Text = "Please select an option"
            Else
                If chkNewEnrollment.Checked Then
                    emailSubject = "Auto-Pay New Enrollment"
                    emailBody.Append("Auto-Pay New Enrollment Request Form" & vbCrLf)
                Else
                    emailSubject = "Auto-Pay Update Request"
                    emailBody.Append("Auto-Pay Update Enrollment Request Form" & vbCrLf)
                End If

                emailBody.Append("Name: " & txtName.Text & vbCrLf & "Address: " & txtAddress.Text & vbCrLf)
                emailBody.Append("City, State Zip: " & txtCity.Text & "," & txtState.Text & " " & txtZip.Text & vbCrLf & "Phone: " & txtPhone.Text & vbCrLf)
                emailBody.Append("Account #: " & lblAccount.Text & vbCrLf & "Email: " & lblEmail.Text & vbCrLf & vbCrLf)

                If chkNewEnrollment.Checked Then
                    If String.IsNullOrEmpty(txtNEBankName.Text) Or String.IsNullOrEmpty(txtNEBankAccount.Text) Or String.IsNullOrEmpty(txtNEBankRouting.Text) Then
                        lblNEBankName.ForeColor = Drawing.Color.Red
                        lblNEBankRouting.ForeColor = Drawing.Color.Red
                        lblNEBankAccount.ForeColor = Drawing.Color.Red
                        Throw New System.Exception("Banking information required.")
                    End If
                    emailBody.Append(chkNewEnrollment.Text & " " & lblServiceType.Text & " bill(s) electronically to my " & rblNEAccountType.SelectedValue & " account at:" & vbCrLf)
                    emailBody.Append("Bank Name: " & txtNEBankName.Text & vbCrLf)
                    emailBody.Append("Bank Routing: " & txtNEBankRouting.Text & vbCrLf)
                    emailBody.Append("Bank Account: " & txtNEBankAccount.Text & vbCrLf)
                    emailBody.Append("Auto draft date: " & cboNEDraftDate.SelectedValue & vbCrLf & vbCrLf)
                End If
                If chkUpdateBank.Checked Then
                    If rblUpdType.SelectedIndex = 0 Then 'changing banks
                        If String.IsNullOrEmpty(txtUpBankName.Text) Or String.IsNullOrEmpty(txtUpBankAccount.Text) Or String.IsNullOrEmpty(txtUpBankRouting.Text) Then
                            lblUpBankName.ForeColor = Drawing.Color.Red
                            lblUpBankRouting.ForeColor = Drawing.Color.Red
                            lblUpBankAccount.ForeColor = Drawing.Color.Red
                            Throw New System.Exception("Banking information required.")
                        End If
                    End If
                    If rblUpdType.SelectedIndex = 1 Then 'new account same bank
                        If String.IsNullOrEmpty(txtUpBankAccount.Text) Then
                            lblUpBankAccount.ForeColor = Drawing.Color.Red
                            Throw New System.Exception("Banking information required.")
                        End If
                    End If
                    emailBody.Append(chkUpdateBank.Text & vbCrLf)
                    emailBody.Append("Action: " & rblUpdType.SelectedValue & vbCrLf)
                    emailBody.Append("Bank Name: " & txtUpBankName.Text & vbCrLf)
                    emailBody.Append("Bank Routing: " & txtUpBankRouting.Text & vbCrLf)
                    emailBody.Append("Bank Account: " & txtUpBankAccount.Text & vbCrLf & vbCrLf)
                End If
                If chkUpdateDraft.Checked Then
                    emailBody.Append(chkUpdateDraft.Text & vbCrLf)
                    emailBody.Append("Draft date: " & cboUpDraftDate.SelectedValue & vbCrLf)
                End If

                Dim toList As New ArrayList
                toList.Add("customer.service@empiredistrict.com")
                email.sendEMail(toList, "AUPForm@empiredistrict.com", emailSubject.ToString, emailBody.ToString)

                toList.Clear() 'send confirmation to myAccount user
                toList.Add(Membership.GetUser(User.Identity.Name).Email)
                email.sendEMail(toList, "customer.service@empiredistrict.com", "MyAccount Auto-Pay Enrollment", "Thank you for submitting your Auto-Pay request." & vbCrLf & vbCrLf & "*Reminder* Auto-Pay information may take at least one billing cycle to go into effect.")

                LogData()
                lblEmailSent.Text = "<b>Thank you for submitting your Auto-Pay request.</b><br /><br />*Reminder* Auto-Pay information may take at least one billing cycle to go into effect."
                pnlForm.Visible = False
                pnlFinal.Visible = True
            End If
        Catch ex As Exception
            lblStatus.Text = ex.Message.ToString
        End Try
    End Sub

End Class
