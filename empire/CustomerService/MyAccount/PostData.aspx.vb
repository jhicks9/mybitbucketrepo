﻿Imports System.Xml
Imports System.IO
Imports System.Data

Partial Class CustomerService_MyAccount_PostData
    Inherits System.Web.UI.Page

    Dim ct As String = ""   'custToken
    Dim at As String = ""   'acctToken
    Dim apt As String = ""  'acctpToken
    Dim spat As String = "" 'spaToken
    Dim zc As String = ""   'zipcode
    Dim bt As String = ""   'billing type (gas,water,electric)
    Dim spt As String = ""  'service point token
    Dim pt As String = ""   'premise token
    Dim postData As String = ""

    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'lblDate.Text = FormatDateTime(Now, DateFormat.LongDate)
            ct = Request.QueryString("ct")
            at = Request.QueryString("at")
            apt = Request.QueryString("apt")
            spat = Request.QueryString("spat")
            zc = Request.QueryString("zc")
            bt = Request.QueryString("bt")
            spt = Request.QueryString("spt")
            pt = Request.QueryString("pt")
            BuildXML()  ' build custom XML for Apogee Billing Insights product
            If Len(postData) > 0 Then
                lblOutput.Text = "Personalized data for the calculator processed for " & ct
                data.Value = postData  ' put XML data into an input field labeled 'data' for posting to Apogee
            Else
                lblOutput.Text = "Personalized data for the calculator is not available at this time."
            End If
        Catch
        End Try
    End Sub

    Public Sub BuildXML()
        Try
            Dim tdes As New tripleDES
            Dim encrypt As String = ""
            Dim xmlDoc As String = ""
            Dim ms As New MemoryStream
            Dim xml_text_writer As New XmlTextWriter(ms, System.Text.Encoding.UTF8)

            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(ct)
            Dim rtnUsage As DataSet = cwservice.GetUsageInfo(encrypt, at, apt, "459", spat, spt, pt)

            xml_text_writer.Formatting = Formatting.Indented
            xml_text_writer.Indentation = 4

            ' Write the XML declaration.
            xml_text_writer.WriteStartDocument(True)
            xml_text_writer.WriteStartElement("AuditmationInput20")
            xml_text_writer.WriteAttributeString("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
            xml_text_writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")

            xml_text_writer.WriteStartElement("ClientID")
            xml_text_writer.WriteString(ct)
            xml_text_writer.WriteEndElement() 'ClientID
            xml_text_writer.WriteStartElement("UtilityID")
            xml_text_writer.WriteString("empiredistrict")
            xml_text_writer.WriteEndElement() 'UtilityID
            xml_text_writer.WriteStartElement("ZipCode")
            xml_text_writer.WriteString(zc)
            xml_text_writer.WriteEndElement() 'ZipCode
            xml_text_writer.WriteStartElement("MemberSeparator")
            xml_text_writer.WriteString(ct)
            xml_text_writer.WriteEndElement() 'MemberSeparator
            If bt = "Gas" Then
                xml_text_writer.WriteStartElement("BillingDataGas")
            Else
                xml_text_writer.WriteStartElement("BillingData")
            End If

            Dim dr As DataRow
            Dim increment As Integer = 0
            For Each dr In rtnUsage.Tables(0).Rows
                ' Ensure the latest read has actually been invoiced (has a valid billing_date)
                If Not IsDBNull(rtnUsage.Tables(0).Rows(increment).Item("billing_date")) Then
                    If bt = "Gas" Then
                        xml_text_writer.WriteStartElement("BillingDataRowGas")
                    Else
                        xml_text_writer.WriteStartElement("BillingDataRow")
                    End If
                    xml_text_writer.WriteStartElement("ReadingDate")
                    xml_text_writer.WriteString(rtnUsage.Tables(0).Rows(increment).Item("read_date"))
                    xml_text_writer.WriteEndElement() 'ReadingDate
                    xml_text_writer.WriteStartElement("BillingDate")
                    xml_text_writer.WriteString(rtnUsage.Tables(0).Rows(increment).Item("billing_date"))
                    xml_text_writer.WriteEndElement() 'BillingDate
                    xml_text_writer.WriteStartElement("Usage")
                    If bt = "Gas" Then
                        'CCF Natural Gas to Therms Natural Gas multiply by 1.0250 
                        xml_text_writer.WriteString(((rtnUsage.Tables(0).Rows(increment).Item("usage_num") * 1.025).ToString).Trim)
                    Else
                        xml_text_writer.WriteString(rtnUsage.Tables(0).Rows(increment).Item("usage_num").Trim)
                    End If
                    xml_text_writer.WriteEndElement() 'Usage
                    xml_text_writer.WriteStartElement("UsageUnit")
                    If bt = "Gas" Then
                        xml_text_writer.WriteString("THERMS")
                    Else
                        xml_text_writer.WriteString("KWH")
                    End If
                    xml_text_writer.WriteEndElement() 'UsageUnit
                    xml_text_writer.WriteStartElement("TotalBill")
                    xml_text_writer.WriteString(String.Format("{0:f2}", CType(rtnUsage.Tables(0).Rows(increment).Item("actual_charge"), Decimal)))
                    xml_text_writer.WriteEndElement() 'TotalBill
                    xml_text_writer.WriteStartElement("EnergyCost")
                    xml_text_writer.WriteString(rtnUsage.Tables(0).Rows(increment).Item("usage_charge"))
                    xml_text_writer.WriteEndElement() 'EnergyCost
                    xml_text_writer.WriteStartElement("DaysInCycle")
                    xml_text_writer.WriteString(rtnUsage.Tables(0).Rows(increment).Item("usage_days_cnt"))
                    xml_text_writer.WriteEndElement() 'DaysInCycle
                    xml_text_writer.WriteStartElement("IsEstimate")
                    xml_text_writer.WriteString(rtnUsage.Tables(0).Rows(increment).Item("estimate_ind"))
                    xml_text_writer.WriteEndElement() 'IsEstimate
                    xml_text_writer.WriteEndElement() 'BillingDataRow
                End If
                increment = increment + 1
            Next  'row
            xml_text_writer.WriteEndElement() 'BillingData
            xml_text_writer.WriteEndElement() 'AuditmationInput20

            ' End the document.
            xml_text_writer.WriteEndDocument()
            xml_text_writer.Flush()

            ' Use a StreamReader to display the result.
            Dim sr As New StreamReader(ms)
            ms.Seek(0, SeekOrigin.Begin)
            postData = sr.ReadToEnd
            sr.Close()
            sr.Dispose()

            Dim byteData As Byte() = System.Text.Encoding.ASCII.GetBytes(postData)
            postData = ToBase64(byteData)  'base64 encode the result
            'byteData = FromBase64(postData)
            'txtOutput.Text = System.Text.Encoding.ASCII.GetString(byteData)

            ' Close the XmlTextWriter.
            xml_text_writer.Close()
            ms.Close()
            ms.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Function GetStreamAsByteArray(ByVal stream As System.IO.Stream) As Byte()
        Try
            Dim streamLength As Integer = Convert.ToInt32(stream.Length)
            Dim fileData As Byte() = New Byte(streamLength) {}
            stream.Read(fileData, 0, streamLength)
            stream.Close()
            Return fileData
        Catch
            Return Nothing
        End Try
    End Function

    Public Function ToBase64(ByVal data() As Byte) As String
        Try
            'If data Is Nothing Then Throw New ArgumentNullException("data")
            If data Is Nothing Then
                Return ""
            Else
                Return Convert.ToBase64String(data)
            End If
        Catch
            Return Nothing
        End Try
    End Function
    Public Function FromBase64(ByVal base64 As String) As Byte()
        Try
            'If base64 Is Nothing Then Throw New ArgumentNullException("base64")
            If base64 Is Nothing Then
                Return Nothing
            Else
                Return Convert.FromBase64String(base64)
            End If
        Catch
            Return Nothing
        End Try
    End Function
End Class
