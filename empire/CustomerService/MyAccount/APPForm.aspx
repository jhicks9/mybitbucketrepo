﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="APPForm.aspx.vb" Inherits="CustomerService_MyAccount_APPForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

        <asp:Panel ID="pnlBillPackages" runat="server" Visible="True">
            <asp:DataList id="dlBillPkg" runat="server">
                <ItemTemplate>
                <b><asp:LinkButton id="LinkButton1" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                </ItemTemplate>
            </asp:DataList>
            <asp:HiddenField ID="hdnBillPackage" runat="server" />
            <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList" AllowPaging="false" >
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE") & "," & Eval("service_point_tkn") & "," & Eval("premise_tkn") & "," & Left(Eval("POSTAL_CODE"),5) & "," & Eval("account_pkg_tkn")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No usage found.
                </EmptyDataTemplate>
            </asp:GridView>
        </asp:Panel>

        <asp:Panel ID="pnlForm" runat="server" Visible="False">
            <div style='float:right;width:380px;margin:0px 0 0 5px;'>
                <img id='imgServiceConnect' src='~/images/payments-appgraph.gif' runat='server' alt='' />
            </div>
            <div>
                <p>I understand that the APP monthly amount will be determined by my energy consumption
                for the previous year. This monthly amount may be reasonably adjusted if my usage changes
                during the plan year and/or at the reevaluation month.  Participation in APP requires customers pay their pre-determined average amount every month – even if the status shown on their contract information is a credit. Customers who fail to make monthly installment payments are subject to have their APP contract terminated. Customers who wish to pay more than their monthly installment must contact Empire before doing so.
                    <br /><br />
                    <b><i>To enroll in APP, account must be in good standing with no balance due.</i></b>
                </p>
            </div>
            <font style="color:#113962"><b>Average Payment Plan Enrollment</b></font>
            <div style="border: solid 5px #b0c4de;padding:5px;">
                <table cellpadding="0" cellspacing="2">
                    <tr valign="top">
                        <td align="right" style="width:150px"><asp:label ID="lblName" Text="* Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td align="left" style="width:575px">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="avg"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblPhone" Text="* Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:TextBox ID="txtPhone" runat="server" ValidationGroup="avg" />
                            <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone"
                                Display="Dynamic" ErrorMessage="Phone Number format xxx-xxx-xxxx " ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                                ValidationGroup="avg" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblAddress_" Text="Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td><asp:label ID="lblAddress" runat="server" /></td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblCity_" Text="City:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td><asp:label ID="lblCity" runat="server"/></td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblState_" Text="State:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td><asp:label ID="lblState" runat="server"/></td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblZipCode_" Text="Zip:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td><asp:label ID="lblZipCode" runat="server" /></td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblAccount_" Text="Account Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td><asp:label ID="lblAccount" runat="server"/><asp:label ID="lblService" runat="server" Visible="false" /><asp:label ID="lblLocation" runat="server" Visible="false" /></td>
                    </tr>
                    <tr valign="top">
                        <td align="right"><asp:label ID="lblEmail_" Text="Email:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td><asp:label ID="lblEmail" runat="server" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="width:715px;padding:5px;background-color:#d3d3d3">
                            When you choose APP, you may also elect to choose a new due date.  However, limitations can occur depending on state regulations and customer rate schedules.  Empire will contact a customer if unable to accomodate the change of the date request.
                        </td>
                    </tr>
                    <tr>
                         <td align="right"><asp:label ID="lblDueDate_" Text="Due Date:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:DropDownList ID="cboDueDate" runat="server">
                                <asp:ListItem Selected="True">No change</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;<i>(if possible)</i>
                        </td>
                    </tr>
                </table>
            </div>
            * (indicates required fields)
            <br /><br />
            <asp:CheckBox ID="ckbAgree" runat="server" AutoPostBack="true" Text="I understand that I may cancel this agreement at any time simply by contacting Empire District." />
            <br /><asp:Label ID="lblStatus" runat="server" Font-Bold="true" ForeColor="Red" /><br />
            <div style="float:left">
                <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbSubmit_Click" Enabled="false"><span>Continue</span></asp:LinkButton>
                <div style="float:left;width:6px;">&nbsp;</div>
                <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" CausesValidation="false" PostBackUrl="~/customerservice/myaccount/default.aspx"><span>Cancel</span></asp:LinkButton>
            </div>
        </asp:Panel>
    
        <asp:Panel ID="pnlFinal" runat="server" Visible="False">
            <div style="padding:15px">
                <asp:Label ID="lblEmailSent" runat="server" />
                <br /><br />
                <asp:LinkButton ID="lbContinue" runat="server" CssClass="ovalbutton" CausesValidation="false" PostBackUrl="~/customerservice/myaccount/default.aspx"><span>Continue</span></asp:LinkButton>
            </div>
        </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
