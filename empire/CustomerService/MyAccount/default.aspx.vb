
Partial Class customerservice_MyAccount_default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim psm As New processSiteMap
            txtList.Text = psm.DisplayCurrentNode
            hlChangeUserid.Text = User.Identity.Name

            If Roles.IsUserInRole("Admin") = True Then
                txtAdminList.Text = psm.BuildSingleNode("Admin")
                pnlAdmin.Visible = True
            Else
                pnlAdmin.Visible = False
            End If

        Catch
        End Try
    End Sub
End Class
