
Partial Class CustomerService_MyAccount_ChangePassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ChangePassword1.Focus()
    End Sub

    Protected Sub ChangePassword1_ChangingPassword(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles ChangePassword1.ChangingPassword
        Dim NewPassword As TextBox = ChangePassword1.ChangePasswordTemplateContainer.FindControl("NewPassword")
        'Dim userPassword As New MembershipUser("empireMembershipProvider", User.Identity.Name, Nothing, Membership.GetUser(User.Identity.Name).Email, Nothing, Nothing, True, False, Nothing, Nothing, Nothing, Nothing, Nothing)
        If NewPassword.Text = User.Identity.Name Then
            lblError.Text = "Password cannot be the same as your User Name"
            lblError.ForeColor = Drawing.Color.Red
            lblError.Visible = True
            e.Cancel = True
            'ElseIf NewPassword.Text = userPassword.GetPassword.ToString Then
        Else
            lblError.Visible = False
        End If
    End Sub
    Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Response.Redirect(Request.ServerVariables("URL"))
    End Sub
End Class
