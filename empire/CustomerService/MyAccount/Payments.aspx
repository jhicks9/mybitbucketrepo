<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Payments.aspx.vb" Inherits="CustomerService_MyAccount_Payments" title="MyPayments" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="makepayment" TagName="uc5" Src="~/MakePayment.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

            <asp:MultiView ID="MultiView1" ActiveViewIndex="0" runat="server">
                <asp:View ID="vwTokens" runat="server">
                     <asp:DataList id="dlBillPkg" runat="server">
                         <ItemTemplate>
                            <b><asp:LinkButton id="LinkButton1" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                         </ItemTemplate>
                     </asp:DataList>
                </asp:View>
            
                <asp:View ID="vwPayment" runat="server">
                    <div style="padding:0% 2% 0% 0%;width:175px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" ><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                    <div id="print_content"><%--begin print content--%>
                        <div style="padding:0% 0% 0% 0%;float:left;"><asp:Label ID="lblAccount" runat="server"></asp:Label></div>
                        <div style="clear:both;"></div>                
                        <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                        <br /><br />
                        <makepayment:uc5 ID="uc5" runat="server" />
                        <br />
                        <asp:GridView ID="dgPayment" runat="server" SkinID="GridView" AllowPaging="false" AllowSorting="false">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="150px" DataField="PAYMENTDATE" HeaderStyle-HorizontalAlign="Left" HeaderText="Payment Received" SortExpression="PAYMENTDATE" />
                                <asp:BoundField DataField="PAYMENTAMOUNT" HeaderStyle-HorizontalAlign="Left" HeaderText="Payment Amount" SortExpression="PAYMENTAMOUNT" />
                             </Columns>
                        </asp:GridView>
                    </div><%--end print content--%>                    
                </asp:View>
            </asp:MultiView> 
        </ContentTemplate>
    </asp:UpdatePanel>
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Payments","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>