Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomerService_MyAccount_ChangeEmail
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            txtCurrentEmail.Text = syncEmail() 'Membership.GetUser(User.Identity.Name).Email
            txtNewEmail.Focus()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbChange_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtNewEmail.Text = txtCurrentEmail.Text Then
                lblMessage.Text = "Email not changed. Your current email and the new email entered are the same"
                lblMessage.Font.Size = "12"
                lblMessage.ForeColor = Drawing.Color.Red
            Else
                If txtNewEmail.Text = txtConfirmEmail.Text Then
                    Dim u As MembershipUser
                    u = Membership.GetUser(User.Identity.Name)
                    u.Email = txtNewEmail.Text
                    Membership.UpdateUser(u)
                    updateTable(3) 'build database entry for customerwatch
                    lblMessage.Visible = False
                    lblNewEmailNotify.Text = Membership.GetUser(User.Identity.Name).Email
                    pnlMain.Visible = False
                    pnlEmailChanged.Visible = True
                Else
                    lblMessage.Text = "Reconfirm Email"
                    lblMessage.Font.Size = "12"
                    lblMessage.ForeColor = Drawing.Color.Red
                    lblMessage.Visible = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function syncEmail() As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim cwEmail As String = ""
        Dim msEmail As String = ""
        Dim pendingEmail As String = ""
        Dim email As String = ""
        Try
            'get email from membership services
            msEmail = Membership.GetUser(User.Identity.Name).Email
            email = msEmail  'set display email address

            'get email from customer watch
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            cwEmail = cwservice.GetEmailAddress(encrypt, Profile.BillingPackageToken)

            'get for any "pending" email changes
            conn.Open()
            cmdLookup.CommandText = "SELECT ebilling.email from ebilling where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & Profile.BillingPackageToken & "' and status=1"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    pendingEmail = drLookup("email").ToString
                End While
                email = pendingEmail  'display current email address  -- currently what is in membership services AND pending
            Else 'if there are no pending email changes
                'sync membership services with customer watch if the emails do not match
                If (Len(cwEmail) > 0) Then  'if customer watch has email address
                    If (cwEmail <> msEmail) Then  'if customer watch does not match membership services
                        Dim u As MembershipUser
                        u = Membership.GetUser(User.Identity.Name)
                        u.Email = cwEmail
                        Membership.UpdateUser(u)
                    End If
                    email = cwEmail  'display current email address  -- currently on file in customer watch
                End If
            End If
            drLookup.Close()
            conn.Close()
            Return email
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return email
        End Try
    End Function

    Public Sub updateTable(ByVal actioncode As Integer) 'build table data for customer watch
        Dim recordExists As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim comment As String = ""

        Try
            Select Case actioncode
                Case 1
                    comment = "activate ebilling"
                Case 2
                    comment = "opt-out ebilling"
                Case 3
                    comment = "update email"
            End Select

            Dim insertquery As String = "insert into ebilling(customer_tkn,billing_pkg_tkn,email,status,actioncode,comment,lastupdate) " & _
            "values('" & Profile.CustomerAccountNumber & "','" & Profile.BillingPackageToken & "','" & Membership.GetUser(User.Identity.Name).Email & "'," & 1 & "," & actioncode & ",'" & comment & "','" & Now & "')"
            Dim updatequery As String = "update ebilling set ebilling.status=1,ebilling.actioncode=" & actioncode & ",ebilling.email='" & Membership.GetUser(User.Identity.Name).Email & "',ebilling.lastupdate='" & Now & "',ebilling.comment='" & comment & "' where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & Profile.BillingPackageToken & "'"

            conn.Open()  'add record if it doesnt exist, otherwise update record
            cmdLookup.CommandText = "SELECT count(*) as result from ebilling where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & Profile.BillingPackageToken & "'"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            If recordExists Then
                cmd.CommandText = updatequery
            Else
                cmd.CommandText = insertquery
            End If
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub
End Class
