﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomerService_MyAccount_ChangeUsername
    Inherits System.Web.UI.Page

    Protected Function ChangeUsername(ByVal oldUsername As String, ByVal newUsername As String) As Boolean
        Dim recordExists As Boolean = False
        Dim status As Boolean = False
        Dim userId As System.Guid = Nothing
        Dim applicationId As System.Guid = Nothing
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader

        Try
            conn.Open()
            cmdLookup.CommandText = "select aspnet_Users.UserId, aspnet_Applications.ApplicationId from dbo.aspnet_Users, dbo.aspnet_Applications where aspnet_Users.LoweredUserName = '" & oldUsername.ToLower & "' and aspnet_Users.ApplicationId = aspnet_Applications.ApplicationId"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    userId = drLookup("UserId")
                    applicationId = drLookup("ApplicationId")
                End While
            End If
            drLookup.Close()

            If (Not userId = Nothing) And (Not applicationId = Nothing) Then 'old account exists
                cmdLookup.CommandText = "select count(*) as result from dbo.aspnet_Users, dbo.aspnet_Applications where aspnet_Users.LoweredUserName = '" & newUsername.ToLower & "' and aspnet_Users.ApplicationId = aspnet_Applications.ApplicationId"
                drLookup = cmdLookup.ExecuteReader
                If drLookup.HasRows Then
                    While drLookup.Read
                        recordExists = drLookup("result").ToString
                    End While
                End If
                drLookup.Close()

                If recordExists Then  'new account already used
                    status = False
                Else 'make the change
                    cmd.CommandText = "update dbo.aspnet_Users set aspnet_Users.UserName = '" & newUsername & "', aspnet_Users.LoweredUserName = '" & newUsername.ToLower & _
                    "' WHERE aspnet_Users.UserId = '" & userId.ToString & "' and aspnet_Users.ApplicationId = '" & applicationId.ToString & "'"
                    cmd.ExecuteNonQuery()
                    status = True
                End If
            Else  'old account does not exist
                status = False
            End If

            conn.Close()
            Return status
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return False
        End Try

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lblCurrentUserName.Text = Membership.GetUser().ToString
        lblCurrentUserName.Text = User.Identity.Name
        txtNewUserName.Focus()
    End Sub

    Protected Sub lbChange_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Does this username already exist?
        Dim newUsername As String = txtNewUserName.Text.Trim()
        Dim usr As MembershipUser = Membership.GetUser(newUsername)
        If usr IsNot Nothing Then
            lblMessage.Text = String.Format("User Name {0} is already being used. Please try a different User Name.", newUsername)
            txtNewUserName.Focus()
            Exit Sub
        End If

        Dim success As Boolean = ChangeUsername(User.Identity.Name, newUsername)
        If success Then
            'Sign in user using new username & send them back to homepage
            FormsAuthentication.SetAuthCookie(newUsername, False)
            Response.Redirect("~/CustomerService/MyAccount/Default.aspx")
        Else
            lblMessage.Text = "Unable to change User Name at this time."
        End If
    End Sub
End Class
