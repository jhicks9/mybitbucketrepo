﻿Imports System.Data

Partial Class CustomerService_MyAccount_EnergyCalculator
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Dim rtnAcctTkns As New System.Data.DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Profile.CustomerAccountNumber Is Nothing Then
            Try
                cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
                encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
                Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)

                If rtnBillPackages Is Nothing Then
                    Dim lbl As New System.Web.UI.WebControls.Label
                    lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                    lbl.ForeColor = Drawing.Color.Red
                    pnlAccount.Controls.Add(lbl)
                Else

                    Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                    Dim dt As New DataTable()
                    Dim dr As DataRow
                    dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                    dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                    dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                    For i = 0 To billPkgTkn.Length - 1
                        dr = dt.NewRow()
                        dr("custToken") = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
                        dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                        dr("cdToken") = cd.CalculateCheckDigit(cd.ZeroPad(Profile.CustomerAccountNumber, 6) & cd.ZeroPad(billPkgTkn(i), 2))
                        dt.Rows.Add(dr)
                    Next i
                    dlBillPkg.DataSource = dt
                    dlBillPkg.DataBind()
                End If
            Catch
            End Try
        End If
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim billpToken As String = ""
            Dim cdToken As String = ""
            Dim arTokens As Array = Split(e.CommandArgument, ";")
            billpToken = arTokens(0)
            cdToken = arTokens(1)

            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, billpToken)
            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            lblAccount.Text = "Select an account to use within the calculator for " & Profile.CustomerAccountNumber & "-" & billpToken & "-" & cdToken
            mvUsage.ActiveViewIndex = 0
            pnlAccount.Visible = False
            pnlNote.Visible = True
        Catch
        End Try
    End Sub

    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        Dim eCmdArg As String = e.CommandArgument
        Dim arTokens As Array = Split(eCmdArg, ",")
        Dim ct As String = Profile.CustomerAccountNumber 'custToken
        Dim at As String = arTokens(0)    'acctToken
        Dim apt As String = e.CommandName 'acctpToken
        Dim spat As String = arTokens(1)  'spaToken
        Dim zc As String = arTokens(6)    'zipcode
        Dim bt As String = arTokens(2)    'billing type (gas,water,electric)
        Dim spt As String = arTokens(7)   'service point token
        Dim pt As String = arTokens(8)    'premise token

        Try
            If arTokens(2) = "Water" Then
                lblOutput.Text = "Energy calculator not available for water."
            Else
                lblOutput.Text = "Calculator data being processed for " & ct
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "popup", "window.open('" & ResolveUrl("~/CustomerService/MyAccount/PostData.aspx") & "?ct=" & ct & "&at=" & at & "&apt=" & apt & "&spat=" & spat & "&zc=" & zc & "&bt=" & bt & "&spt=" & spt & "&pt=" & pt & "')", True)
            End If
            mvUsage.ActiveViewIndex = 1
        Catch
        End Try
    End Sub

End Class