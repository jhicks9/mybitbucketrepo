Imports System
Imports System.Web
Imports System.Data

Partial Public Class CustomerService_MyAccount_Statements
    Inherits System.Web.UI.Page
    Dim xdoc As String
    Dim xtemplate As String
    Dim rtnStatement As String
    Dim rtnBillPackages, rtnUsage As String
    Dim custToken As String
    Dim txtBillPkg As String
    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Dim ds As New DataSet
    Dim custAccount As String = ""
    Dim billPkgTkn As Array
    Public printurl As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Profile.CustomerAccountNumber Is Nothing Then
            Try
                printurl = ResolveUrl("~/print.aspx")
                custAccount = Profile.CustomerAccountNumber
                cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
                encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
                rtnBillPackages = cwservice.GetBillPackage(encrypt)

                If rtnBillPackages Is Nothing Then
                    Dim lbl As New Label
                    lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                    lbl.ForeColor = Drawing.Color.Red
                    vwAccounts.Controls.Add(lbl)
                Else
                    Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                    Dim dt As New DataTable()
                    Dim dr As DataRow
                    dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                    dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                    dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                    For i = 0 To billPkgTkn.Length - 1
                        dr = dt.NewRow()
                        dr("custToken") = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
                        dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                        dr("cdToken") = cd.CalculateCheckDigit(cd.ZeroPad(Profile.CustomerAccountNumber, 6) & cd.ZeroPad(billPkgTkn(i), 2))
                        dt.Rows.Add(dr)
                    Next i
                    dlBillPkg.DataSource = dt
                    dlBillPkg.DataBind()
                End If
                Dim cp As ContentPlaceHolder = CType(PreviousPage.Master.FindControl("RightPlaceHolder"), ContentPlaceHolder)
                If Not cp is nothing Then
                    Dim gv As HiddenField = CType(cp.FindControl("hdnStatementToken"), HiddenField)
                End If
            Catch

            End Try
        End If
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim rtnValues As New System.Data.DataSet
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim formatAmount As String = ""

            Dim arTokens As Array = Split(e.CommandArgument, ";")
            Dim billpToken As String = arTokens(0)
            Dim cdToken As String = arTokens(1)

            lblAccount.Font.Size = "10"
            lblAccount.Text = "*Select a date to display the statement for account " & Profile.CustomerAccountNumber & "-" & billpToken & "-" & cdToken & "."
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim encryptcustToken As String = tdes.Encrypt(Profile.CustomerAccountNumber)
            rtnValues = cwservice.GetBillInfo(encryptcustToken, billpToken, "90")

            'add 3 columns (charges,taxes,total) to gridview
            Dim dc As DataColumn
            dc = New DataColumn("amount", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            dc = New DataColumn("duedate", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            Dim dr As DataRow
            Dim increment As Integer = 0
            For Each dr In rtnValues.Tables(0).Rows
                encrypt = tdes.Encrypt(rtnValues.Tables(0).Rows(increment).Item("cust_statement_tkn"))
                rtnStatement = cwservice.GetCustomerChargeInfo(encrypt)
                If Len(rtnStatement) > 0 And InStr(rtnStatement, ";") > 0 Then
                    formatAmount = rtnStatement.Split(";")(0).Trim
                    If Strings.Left(formatAmount, 1) = "<" And Strings.Right(formatAmount, 1) = ">" Then  'clearly show credit on amount
                        formatAmount += " CR"
                    End If
                    rtnValues.Tables(0).Rows(increment).Item("amount") = formatAmount
                    rtnValues.Tables(0).Rows(increment).Item("duedate") = rtnStatement.Split(";")(1).Trim
                Else
                    rtnValues.Tables(0).Rows(increment).Item("amount") = ""
                    rtnValues.Tables(0).Rows(increment).Item("duedate") = ""
                End If
                increment = increment + 1
            Next

            ' Get Bill Inserts
            dgStatements.DataSource = rtnValues
            dgStatements.DataBind()
            lblAccount.Text = "<b>Statements</b> for " & custAccount & "-" & billpToken & "-" & cdToken
            mvSummary.ActiveViewIndex = 1

            rtnAcctTkns = cwservice.GetAccountTokens(encryptcustToken, billpToken)
            Dim sbState As String = ""
            Dim sbDesc As String = ""
            Dim sbRevClass As String = ""
            increment = 0
            For Each dr In rtnAcctTkns.Tables(0).Rows
                If Len(sbDesc) <= 0 Then
                    sbDesc = sbDesc & "'" & rtnAcctTkns.Tables("AccountTokens").Rows(increment).Item("account_pkg_desc") & "'"
                Else
                    sbDesc = sbDesc & ",'" & rtnAcctTkns.Tables("AccountTokens").Rows(increment).Item("account_pkg_desc") & "'"
                End If
                If Len(sbState) <= 0 Then
                    sbState = sbState & "'" & rtnAcctTkns.Tables("AccountTokens").Rows(increment).Item("state_code") & "'"
                Else
                    sbState = sbState & ",'" & rtnAcctTkns.Tables("AccountTokens").Rows(increment).Item("state_code") & "'"
                End If
                If Len(sbRevClass) <= 0 Then
                    sbRevClass = sbRevClass & "'" & rtnAcctTkns.Tables("AccountTokens").Rows(increment).Item("revenue_class_desc") & "'"
                Else
                    sbRevClass = sbRevClass & ",'" & rtnAcctTkns.Tables("AccountTokens").Rows(increment).Item("revenue_class_desc") & "'"
                End If
                increment = increment + 1
            Next
            Dim query As String = "select id, name, begdate from billinsert where (billinsert.begdate <= getdate()) AND (billinsert.enddate >= getdate())"
            query &= " and billinsert.id in (select id from billinsert where state is null"
            If Len(sbState) > 0 Then
                query &= " OR state IN (" & sbState & ")"
            End If
            query &= ")"
            query &= " and billinsert.id in (select id from billinsert where revenueclass is null"
            If Len(sbRevClass) > 0 Then
                query &= " OR revenueclass IN (" & sbRevClass & ")"
            End If
            query &= ")"
            query &= " and billinsert.id in (select id from billinsert where pkgdescription is null"
            If Len(sbDesc) > 0 Then
                query &= " or pkgdescription in (" & sbDesc & ")"
            End If
            query &= ")"
            query &= " order by billinsert.begdate desc"
            sqlDSInserts.SelectCommand = query
            sqlDSInserts.DataBind()
            ' End Bill Inserts
        Catch
        End Try
    End Sub
End Class
