﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ChangeUsername.aspx.vb" Inherits="CustomerService_MyAccount_ChangeUsername" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table cellpadding="5" cellspacing="0">
        <tr>
            <td valign="middle" align="right" style="width:130px">
                <asp:Label ID="lblCurrentUserName1" runat="server" Text="Current User Name: " />
            </td>
            <td valign="middle" align="left">
                <asp:Label ID="lblCurrentUserName" runat="server" Text="" />
            </td>
        </tr>
        <tr>
            <td valign="middle" align="right">
                <asp:Label ID="lblNewUserName1" runat="server" Text="New User Name: " />
            </td>
            <td valign="middle" align="left">
                <asp:TextBox ID="txtNewUserName" runat="server" width="200px" />
                <asp:RequiredFieldValidator ID="rfvNewUserName" ControlToValidate="txtNewUserName" runat="server" ErrorMessage="required" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td valign="middle" align="right">
                <asp:Label ID="lblConfirmUserName" runat="server" Text="Confirm User Name: " />
            </td>
            <td valign="middle" align="left">
                <asp:TextBox ID="txtConfirmUserName" runat="server" width="200px" />
                <asp:RequiredFieldValidator ID="rfvConfirmUserName" ControlToValidate="txtConfirmUserName" runat="server" ErrorMessage="required" Display="Dynamic" />
                <asp:CompareValidator ID="compareUserName" runat="server" ControlToCompare="txtNewUserName"
                    ControlToValidate="txtConfirmUserName" ErrorMessage="User Names must match." Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td valign="middle" align="right"></td>
            <td valign="middle" align="left">
                <asp:Label ID="lblMessage" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="middle" align="right">&nbsp;</td>
            <td valign="middle" align="left">
                <asp:LinkButton ID="lbChange" runat="server" CssClass="ovalbutton" OnClick="lbChange_Click" ><span>Change User Name</span></asp:LinkButton>
                <div style="float:left;width:6px;">&nbsp;</div>
                <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" CausesValidation="false" PostBackUrl="~/customerservice/myaccount/default.aspx"><span>Cancel</span></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
