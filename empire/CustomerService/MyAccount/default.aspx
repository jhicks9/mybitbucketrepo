<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="customerservice_MyAccount_default" title="MyAccount" %>
<%@ Register TagPrefix="makepayment" TagName="uc5" Src="~/MakePayment.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td valign="middle">
                Hello&nbsp;
                <asp:HyperLink ID="hlChangeUserid" runat="server" Font-Bold="true" NavigateUrl="~/CustomerService/MyAccount/ChangeUsername.aspx" Text="" />.
                <br /><br />
                Welcome to the MyAccount section of Empire District's Web site.
            </td>
            <td>
                <asp:Image ID="imgLogin" runat="server" ImageUrl="~/images/landing/landing-myaccount.jpg" />
            </td>
        </tr>
    </table>
    <br />
    <makepayment:uc5 ID="uc5" runat="server" />
    <div class="landingtext">
        <br />
        <asp:Label ID="txtList" runat="server" Text="" />

        <asp:Panel ID="pnlAdmin" runat="server" Visible="false">
            <br /><hr style='color:#113962;height:1px;'/><br />
            <center class='sectionheader'>Administration</center><br />
            <asp:Label ID="txtAdminList" runat="server" Text="" />
        </asp:Panel>
    </div>
</asp:Content>

