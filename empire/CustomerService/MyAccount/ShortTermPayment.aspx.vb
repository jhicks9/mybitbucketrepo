﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net

Partial Class CustomerService_MyAccount_ShortTermPayment
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service
    Dim cd As New checkDigit
    Dim custToken As String = ""
    Dim encrypt As String = ""
    Dim tdes As New tripleDES

    Public Function savePaymentAgreement() As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            conn.Open()
            If hdnInsertRequired.Value = "True" Then
                Dim cmdInsert As New SqlCommand("insert into stpaymentagreement (customer_tkn,billing_pkg_tkn,customeracct_tkn,account_pkg_tkn,name,phone,payment_amt,payment_date,payment_type_location,notes,lastupdate) values (@customer_tkn,@billing_pkg_tkn,@customeracct_tkn,@account_pkg_tkn,@name,@phone,@payment_amt,@payment_date,@payment_type_location,@notes,@lastupdate)", conn)
                cmdInsert.Parameters.AddWithValue("@customer_tkn", custToken)
                cmdInsert.Parameters.AddWithValue("@billing_pkg_tkn", hdnBillPkgTkn.Value)
                cmdInsert.Parameters.AddWithValue("@customeracct_tkn", hdnCustomerAcctTkn.Value)
                cmdInsert.Parameters.AddWithValue("@account_pkg_tkn", hdnAccountPkgTkn.Value)
                cmdInsert.Parameters.AddWithValue("@name", txtName.Text)
                cmdInsert.Parameters.AddWithValue("@phone", txtPhone.Text)
                cmdInsert.Parameters.AddWithValue("@payment_amt", txtPaymentAmount.Text)
                cmdInsert.Parameters.AddWithValue("@payment_date", txtPaymentDate.Text)
                cmdInsert.Parameters.AddWithValue("@payment_type_location", ddlPaymentType.SelectedValue.ToString)
                cmdInsert.Parameters.AddWithValue("@notes", txtNotes.Text)
                cmdInsert.Parameters.AddWithValue("@lastupdate", Now.ToString)
                cmdInsert.ExecuteNonQuery()
            Else
                Dim cmdUpdate As New SqlCommand("update stpaymentagreement set stpaymentagreement.name=@name,phone=@phone,stpaymentagreement.payment_amt=@payment_amt,stpaymentagreement.payment_date=@payment_date,stpaymentagreement.payment_type_location=@payment_type_location,notes=@notes,lastupdate=@lastupdate where stpaymentagreement.customer_tkn = '" & custToken & "' and stpaymentagreement.billing_pkg_tkn = '" & hdnBillPkgTkn.Value & "'", conn)
                cmdUpdate.Parameters.AddWithValue("@name", txtName.Text)
                cmdUpdate.Parameters.AddWithValue("@phone", txtPhone.Text)
                cmdUpdate.Parameters.AddWithValue("@payment_amt", txtPaymentAmount.Text)
                cmdUpdate.Parameters.AddWithValue("@payment_date", txtPaymentDate.Text)
                cmdUpdate.Parameters.AddWithValue("@payment_type_location", ddlPaymentType.SelectedValue.ToString)
                cmdUpdate.Parameters.AddWithValue("@notes", txtNotes.Text)
                cmdUpdate.Parameters.AddWithValue("@lastupdate", Now.ToString)
                cmdUpdate.ExecuteNonQuery()
            End If
            conn.Close()
            Return True
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return False
        End Try
    End Function

    Public Function loadPaymentAgreement() As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            cmd.CommandText = "SELECT stpaymentagreement.name,stpaymentagreement.phone,stpaymentagreement.payment_amt,CONVERT(VARCHAR(10),stpaymentagreement.payment_date,101) AS payment_date,stpaymentagreement.payment_type_location,stpaymentagreement.notes from dbo.stpaymentagreement where stpaymentagreement.customer_tkn = '" & custToken & "' and stpaymentagreement.billing_pkg_tkn = '" & hdnBillPkgTkn.Value & "' and stpaymentagreement.account_pkg_tkn = '" & hdnAccountPkgTkn.Value & "'"
            conn.Open()
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then  'agreement already on file
                While dr.Read
                    txtName.Text = dr("name").ToString
                    txtPhone.Text = dr("phone").ToString
                    txtPaymentAmount.Text = dr("payment_amt").ToString
                    txtPaymentDate.Text = dr("payment_date").ToString
                    ddlPaymentType.SelectedValue = dr("payment_type_location").ToString
                    txtNotes.Text = dr("notes").ToString
                End While
                lblPaymentAgreementButton.Text = "Update Payment Agreement"
            Else 'no rows
                lblPaymentAgreementButton.Text = "Submit Payment Agreement"
                hdnInsertRequired.Value = "True"
            End If
            dr.Close()
            conn.Close()

            Return True
        Catch ex As Exception
            If ex.Message = "Invalid object name 'dbo.stpaymentagreement'." Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.stpaymentagreement(customer_tkn varchar(10) not null,billing_pkg_tkn varchar(10) not null,account_pkg_tkn varchar(10) not null,customeracct_tkn varchar(10) not null,name varchar(64) not null,phone varchar(15) not null,payment_amt varchar(10) not null, payment_date datetime not null, payment_type_location varchar(25) not null,notes varchar(128) null, lastupdate datetime not NULL, CONSTRAINT pk_stpaymentagreement PRIMARY KEY CLUSTERED (customer_tkn ASC, billing_pkg_tkn ASC, account_pkg_tkn ASC))"
                cmd.ExecuteNonQuery()  'create the table
                hdnInsertRequired.Value = "True"
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return False
        End Try
    End Function
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim build As String = ""
        Try
            custToken = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)
            Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)

            If rtnBillPackages Is Nothing Then
                Dim lbl As New System.Web.UI.WebControls.Label
                lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                lbl.ForeColor = Drawing.Color.Red
                pnlBP.Controls.Add(lbl)
            Else
                Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                Dim dt As New DataTable()
                Dim dr As DataRow
                dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                For i = 0 To billPkgTkn.Length - 1
                    dr = dt.NewRow()
                    dr("custToken") = custToken
                    dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                    dr("cdToken") = cd.CalculateCheckDigit(custToken & cd.ZeroPad(billPkgTkn(i), 2))
                    dt.Rows.Add(dr)
                Next i
                dlBillPkg.DataSource = dt
                dlBillPkg.DataBind()
            End If
        Catch
        End Try
    End Sub
    
    Protected Sub lbPaymentAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim keyedDate As DateTime = DateTime.Parse(txtPaymentDate.Text)
            Dim discDate As DateTime
            Dim discDate5 As DateTime
            If IsDate(lblCutDate.Text) Then
                discDate = DateTime.Parse(lblCutDate.Text).AddDays(0)
                discDate5 = DateTime.Parse(lblCutDate.Text).AddDays(5)
            Else
                discDate = DateTime.Now.AddDays(1)  'set disconnect date to tomorrow if it doesnt exist
                discDate5 = DateTime.Now.AddDays(5)
            End If
            If (DateTime.Now.Date >= discDate.Date) Then
                Throw New Exception("payment agreement not allowed on or after " & discDate.ToShortDateString())  'cant enter an agreement on or after disconnect date
            End If
            If (keyedDate.Date > discDate5.Date) Then
                Throw New Exception("payment date must be before " & discDate5.ToShortDateString())  'cant set date after disconnect + 5
            End If
            lblConfirmAmount.Text = txtPaymentAmount.Text
            lblConfirmDate.Text = txtPaymentDate.Text
            savePaymentAgreement()
            pnlForm.Visible = False
            pnlFinal.Visible = True
        Catch ex As Exception
            lblStatus.Text = ex.Message.ToString
        End Try
    End Sub

    Private Function GetDate(ByVal source As String) As DateTime
        Dim converted = DateTime.Parse(source)
        If (converted.Year.Equals(DateTime.Now.Year) And converted.Month.Equals(DateTime.Now.Month)) Then
            GetDate = DateTime.Now
        Else
            GetDate = converted
        End If
    End Function

    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        Try
            Dim eCmdArg As String = e.CommandArgument
            Dim arTokens As Array = Split(eCmdArg, ",")
            hdnCustomerAcctTkn.Value = arTokens(0)
            hdnAccountPkgTkn.Value = e.CommandName
            lbShowAccount.Text = custToken & "-" & hdnCustomerAcctTkn.Value & "-" & hdnAccountPkgTkn.Value
            BuildSummary()
        Catch
        End Try
    End Sub

    Public Sub BuildSummary()
        Try
            Dim build As String = ""

            build = cwservice.GetPaymentDate(encrypt, hdnCustomerAcctTkn.Value, hdnAccountPkgTkn.Value)
            If Len(build) > 1 Then
                lblLastPaymentDate.Text = build.Split(";")(0).Trim
                lblLastPaymentAmount.Text = build.Split(";")(1).Trim
            Else
                lblLastPaymentDate.Text = ""
                lblLastPaymentAmount.Text = ""
            End If
            build = cwservice.GetAPData(encrypt, hdnCustomerAcctTkn.Value, hdnAccountPkgTkn.Value)
            If Len(build) > 1 Then
                lblCutDate.Text = build.Split(";")(1).Trim
                lblDNBalance.Text = build.Split(";")(3).Trim
                If IsNumeric(lblDNBalance.Text) Then
                    txtPaymentAmount.Text = FormatCurrency(lblDNBalance.Text, 2, 0, 0)
                Else
                    txtPaymentAmount.Text = "$0.00"
                End If
            Else
                lblCutDate.Text = ""
                lblDNBalance.Text = ""
            End If
            loadPaymentAgreement()
            pnlAccounts.Visible = False
            pnlForm.Visible = True
        Catch
        End Try
    End Sub

    Protected Sub dlBillPkg_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim rtnAcctTkns As New System.Data.DataSet
            hdnBillPkgTkn.Value = e.CommandArgument.Split(";")(0)
            hdnCheckDigitTkn.Value = e.CommandArgument.Split(";")(1)
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, hdnBillPkgTkn.Value)
            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            lblAccount.Text = "Select an account to make a payment arrangement for " & custToken & "-" & hdnBillPkgTkn.Value & "-" & hdnCheckDigitTkn.Value
            pnlBP.Visible = False
            pnlAccounts.Visible = True
        Catch
        End Try
    End Sub

    Protected Sub lbShowAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowAccount.Click
        pnlForm.Visible = False
        pnlBP.Visible = True
    End Sub
End Class
