﻿<%@ Page Title="MyEbill" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="eBills.aspx.vb" Inherits="CustomerService_MyAccount_eBills" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlStatus" runat="server" Visible="true" >
                <br />
                <div style="float:left;width:70px">Status:</div>
                <div style="float:left;"><asp:Label ID="lbleBillStatus" runat="server" Text="" /></div>
                <br /><br />
                <div style="float:left;width:70px">Email:</div>
                <div style="float:left;"><asp:Label ID="lblEmail" runat="server" Text="" /></div>
                <div style="clear:both"></div>
                <br /><br />
                <asp:HyperLink ID="hlFAQ" runat="server" NavigateUrl="~/CustomerService/MyEbillFAQ.aspx" Text="MyEbill Frequently Asked Questions" />
                <br /><br />
                <div style="clear:both"></div>
                <br />
                <asp:LinkButton ID="btnEnroll" runat="server" CssClass="ovalbutton" OnClick="btnEnroll_Click" Visible="false"><span>Enroll in MyEbills</span></asp:LinkButton>
                <asp:LinkButton ID="btnOptOut" runat="server" CssClass="ovalbutton" OnClick="btnOptOut_Click" Visible="false"><span>Receive paper statements</span></asp:LinkButton>
            </asp:Panel>
            
            <asp:Panel ID="pnlEnroll" runat="server" Visible="false" >
                <div style="width:100%;background-color:#b0c4de;">
                    <div style="padding:15px;">
                        In order to use MyEbills, you must have a valid email address.  Please confirm your current email address:
                        <br />
                        <table cellpadding="5">
                            <tr>
                                <td style="background-color:#ffffaf;"><asp:Label ID="lblEmailConfirm" runat="server" Text="" /></td>
                                <td><asp:LinkButton ID="lbEmailConfirm" runat="server" CausesValidation="false">update email address</asp:LinkButton></td>
                            </tr>
                        </table>
                        <br />
                        <asp:CheckBox ID="ckbEmailConfirm" runat="server" Text="My email address is correct" />&nbsp;&nbsp;<asp:Label ID="lblEmailConfirmStatus" runat="server" ForeColor="Red" Text="" />
                    </div>
                </div>
                <br />
                <div style="width:100%;background-color:#c5c5c5;">
                    <div style="padding:0 10px 5px 0;">
                        <div style="height:350px;width:100%;background-color:#ffffff;color:#000000;font-size:10;text-align:left;border-width:1;border-style:solid;border-color:#cccccc;overflow:scroll">
                            <h3>The Empire District Electric Company – </h3>  
                            <h3>MyEbill Terms and Conditions</h3>
                            <b>Important:  You must read the following and make a selection below in order to proceed</b><br /><br />
                            <b>Qualifications and Restrictions</b><br /><br />
                            <span style="font-size:smaller">
                                Eligible Residential and Commercial customers are permitted to utilize The Empire District Electric Company’s (“Empire”) MyEbill service ("MyEbill") via the Internet with no fee if the following requirements are met:<br /><br />
                                You must have a valid email address and notify Empire of any change of email address, as well as any change in your permanent address, name, or telephone number.<br /><br />
                                Your Empire account must be active.<br /><br />
                                You must comply with the terms and conditions set forth herein.<br /><br />
                            </span>
                            <b>Customer Requirements</b><br /><br />
                            <span style="font-size:smaller">
                                By selecting the "I Accept " button, you are confirming that you accept these Terms and Conditions (and any future modifications thereof).<br /><br />
                                By registering for MyEbill, you will automatically receive email notifications when your Empire bill is issued. These “bill ready” notifications will be sent to your current email address on file for each Empire account you have set up online. (Since these are transaction notices pursuant to the agreement between you and Empire, you may not “opt-out” from communications related to MyEbill.)<br /><br />
                            </span>
                            <b>Electronic Billing</b><br /><br />
                            <span style="font-size:smaller">
                                MyEbill allows you to view bills for the Empire account(s) you have linked to your account.<br /><br />
                                By electing MyEbill, you understand and agree that, until you cancel MyEbill or this Agreement is otherwise terminated, all bills will be delivered electronically to the email address you provide, and will not be sent through the U.S. Postal Service (“U.S.P.S.”).  When you cancel MyEbill, Empire will resume mailing your bills through the U.S.P.S. at no charge to you and will discontinue electronic transmission.  If you wish to obtain a paper copy of any bill sent electronically, contact Empire and a copy will be provided at no charge.  If you wish to mail your payment via U.S.P.S., please include your account number on the check and the remittance portion of your bill, and send it to the remit address on the bill.  You may cancel MyEbill at any time.  You will remain enrolled in MyEbill unless and until you cancel as provided in MyAccount or until this Agreement is otherwise terminated.  To cancel MyEbill and discontinue receiving bill ready email notification(s) you must unlink the MyEbill portion of your bill accounts in MyAccount or contact Empire at 800-206-2300 (electric and water customers) or 800-424-0427 (natural gas customers) any time.<br /><br />
                                By electing MyEbill, you will no longer receive Empire bills  through the mail. You will automatically receive email notification when your Empire bill is  issued.  Empire will forward shut-off notices through U.S.P.S..
                                In the event of any change to your email address, you are obligated to promptly notify Empire by entering this change within the secure profile section of MyAccount.  You must provide Empire your current email address to permit Empire to forward your bill ready notification to your email box.  You are responsible for paying Empire bills regardless of whether you receive email notifications of any bill issued or not.<br /><br />
                                Except as may be necessary to facilitate and complete the online bill payment from a designated financial institution and as otherwise provided in Empire’s Privacy Policy, any information obtained by Empire in connection with your use of MyEbill will not be disclosed to third parties except as required under law.<br /><br />
                            </span>
                            <b>Termination</b><br /><br />
                            <span style="font-size:smaller">
                                Participation in Empire’s MyEbill is completely optional.  Empire reserves the right to suspend or discontinue MyEbill service at any time, for any reason without prior notice. Empire will attempt to provide you with prior notice of suspension or termination of MyEbill, but Empire will not be liable for any failure to do so. Neither termination nor discontinuation of MyEbill shall affect your liability for your Empire bill.<br /><br />
                            </span>
                            <b>Other Terms and Conditions</b><br />
                            <ul style="font-size:smaller">
                                <li>Empire has no responsibility for any failure or error in MyEbill, including without limitation, any interruption, omission, mistake, malfunction or delay related thereto. Empire makes no representation that any data posted is suitable for any particular purpose or use.</li>
                                <li>MyEbill will be operated and used at all times subject to applicable laws, rules, regulations and decisions of federal and state governmental authorities having jurisdiction, as in effect from time to time, including without limitation, the applicable public service commission.</li>
                                <li>MyEbill is proprietary to Empire and is protected by intellectual property laws.  Your access to MyEbill is licensed and not sold.  Subject to timely payment of all bills and these terms and conditions, Empire agrees to provide you with a personal, non-transferable and non-exclusive account enabling you to access MyEbill.</li>
                                <li>Any dispute related to MyEbill shall be resolved pursuant to the laws of the State of Missouri, without reference to its principles on conflicts of laws.</li>
                                <li>Your use of MyEbill does not provide any legal right or interest in any intellectual property used in connection therewith, or derived there from or any text or other content posted in connection therewith, including without limitation any copyright, for which all rights are reserved by Empire.</li>
                                <li>Use of MyEbill is at your sole risk. Although Empire has endeavored to create a secure and reliable service, the confidentiality or security of any communication transmitted or accessible over the internet cannot be guaranteed. Empire does encrypt your personal information on our systems and in transmission and complies with all applicable laws in handling such information. Accordingly, Empire and its affiliates are not responsible for the security of any information transmitted or accessed using the Internet. Actual or attempted unauthorized use of this MyEbill service may result in criminal and/or civil prosecution.</li>
                                <li>When reasonably practical, Empire will attempt to respect your privacy, consistent with its Privacy Policy.</li>
                                <li>Empire shall not be deemed to have waived any of its rights or remedies hereunder unless such waiver is in writing and signed by Empire. No delay or omission on the part of Empire in exercising any rights or remedies shall operate as a waiver of such rights or remedies or any other rights or remedies. A waiver on any one occasion shall not be construed as a bar or wavier of any rights or remedies on future occasions.</li>
                                <li>You shall be solely responsible for providing, maintaining and ensuring compatibility with MyEbill, and all hardware, software, electrical and other requirements for your use of MyEbill, including telecommunications and internet access connections and links, web browsers or other equipment, programs and services required to access and use MyEbill.  MyEbill requires that you have Adobe Reader.</li>
                                <li>You represent that:  i) you are over eighteen (18) years of age and have the power and authority to enter into and perform your obligations under this Agreement; ii) all information you provide is accurate and complete; iii) you are the authorized signatory of the bank account provided to Empire to pay bills; and iv) you will update information you have provided when and if it changes.</li>
                                <li>Empire periodically provides information to customers about programs, services, or other company information through the use of bill inserts.  If you register for Online Services and want to view this information, you can do so by selecting the Customer Service tab and clicking on “Brochures and Advertisements.”</li>
                                <li>MYEBILL SERVICE IS PROVIDED “AS IS,” WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED.</li>
                            </ul>
                            <span style="font-size:smaller">
                                Empire reserves the right to change any terms and conditions of MyEbill at any time.  You will be notified of any such changes by email.  Your use of MyEbill after any changes in such terms and conditions shall constitute your consent to such changes. You agree that Empire shall not be liable to you or to any third party for any modification, suspension, or discontinuance of MyEbill.<br /><br />
                            </span>
                            <b>IF YOU AGREE TO BE BOUND BY THE ABOVE TERMS AND CONDITIONS, SELECT THE “I ACCEPT” BUTTON BELOW; OTHERWISE, SELECT THE “I DO NOT ACCEPT” BUTTON.</b>
                        </div>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr><td colspan="2" align="center"><b>BY SELECTING THE “I ACCEPT” BUTTON, YOU ACKNOWLEDGE HAVING READ THE ABOVE TERMS AND CONDITIONS.</b></td></tr>
                            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <tr>
                                <td align="center">
                                    <asp:CheckBox ID="ckbAgree" runat="server" Text="I ACCEPT" Font-Bold="true" AutoPostBack="true" />&nbsp;&nbsp;<asp:Label ID="lblAgreeStatus" runat="server" ForeColor="Red" Text="" />
                                </td>
                                <td align="center">
                                    <asp:CheckBox ID="ckbNotAgree" runat="server" Text="I DO NOT ACCEPT" Font-Bold="true" AutoPostBack="true" />        
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <br />
                <div style="float:left">
                    <asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" OnClick="lbSubmit_Click"><span>Continue</span></asp:LinkButton>
                    <div style="float:left;width:6px;">&nbsp;</div>
                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" CausesValidation="false" OnClick="lbCancel_Click"><span>Cancel</span></asp:LinkButton>
                </div>
                <div style="clear:both;"></div>                
            </asp:Panel>
            
            <asp:Panel ID="pnlEmailDetailsPopup" runat="server" style="display:none;">
                <asp:UpdatePanel ID="pnlEmailDetails" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                        <asp:Button id="cmdHiddenModalTarget" runat="server" style="display:none" />
                        <cc1:ModalPopupExtender ID="mpeEmailDetailExtender" runat="server" TargetControlID="cmdHiddenModalTarget"
                             PopupControlID="pnlEmailDetailsPopup" BackgroundCssClass="modaloverlay" PopupDragHandleControlID="pnlEmailDetailsPopup" />
                         
                        <div style="background-color:#f5f5f5;width:350px;padding:10px;border:3px solid #113962;">
                            <div style="float:left;width:130px;">
                                <asp:Label ID="lblNewEmail" runat="server" Text="New Email: " />
                            </div>
                            <div style="float:left;width:200px;">
                                <asp:TextBox ID="txtNewEmail" runat="server" width="200px" />
                            </div>
                            <br style="clear:both;"/>
                            <div style="float:left;width:130px;">
                                <asp:Label ID="lblConfirmEmail" runat="server" Text="Confirm email: " />
                            </div>
                            <div style="float:left;width:200px;">
                                <asp:TextBox ID="txtConfirmEmail" runat="server" width="200px" />
                                <asp:Label ID="lblEmailUpdateStatus" runat="server" ForeColor="Red" Font-Bold="true" Text="" />
                            </div>
                            <br style="clear:both;"/>
                            <asp:LinkButton ID="lbEmailUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="false" />&nbsp;
                            <asp:LinkButton ID="lbEmailCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" />
                        </div>
                    </contenttemplate>
                </asp:UpdatePanel>
            </asp:Panel>
                    
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript" >
        // The following snippet works around a problem where FloatingBehavior
        // doesn't allow drops outside the "content area" of the page - where "content
        // area" is a little unusual for our sample web pages due to their use of CSS
        // for layout.
        function setBodyHeightToContentHeight() {
            document.body.style.height = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight) + "px";
        }
        setBodyHeightToContentHeight();
        $addHandler(window, "resize", setBodyHeightToContentHeight);
    </script>
</asp:Content>