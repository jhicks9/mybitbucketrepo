﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ShortTermPayment.aspx.vb" Inherits="CustomerService_MyAccount_ShortTermPayment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div style='float:right;width:350px;margin:0px 0 0 5px;background-color:#b0c4de;padding:15px;'>
                <table cellpadding="3" cellspacing="0">
                    <tr>
                        <td valign="top" align="right">Last Payment Date:</td>
                        <td><asp:Label ID="lblLastPaymentDate" runat="server" /></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">Last Payment Amount:</td>
                        <td><asp:Label ID="lblLastPaymentAmount" runat="server" /></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">Disconnect Amount:</td>
                        <td valign="top"><asp:label ID="lblDNBalance" Text="" runat="server"/></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            Disconnect Date:<br />
                            (on or after)
                        </td>
                        <td valign="top"><asp:Label ID="lblCutDate" runat="server" /></td>
                    </tr>
                </table>                
            </div>
            <div>
                Fill out the form below to arrange for an extension to pay your Empire bill. This agreement and the date we can expect your payment will be noted on your account. If payment is not received by the shut-off date on your disconnect notice, your service may be disrupted.
                <br /><br />
                If you require an extension past the date allowed by the online calendar, please contact Empire at 800-206-2300.
                <br /><br />
                For customers with multiple accounts, please note multiple payment agreements must be made.
                <br /><br />
            </div>
            <asp:HiddenField ID="hdnInsertRequired" runat="server" Value="False" />
            <asp:HiddenField ID="hdnCustomerAcctTkn" runat="server" Value="" />
            <asp:HiddenField ID="hdnAccountPkgTkn" runat="server" Value="" />
            <asp:HiddenField ID="hdnBillPkgTkn" runat="server" Value="" />
            <asp:HiddenField ID="hdnCheckDigitTkn" runat="server" Value="" />

            <asp:Panel ID="pnlBP" runat="server" Visible="true">
                <asp:DataList id="dlBillPkg" runat="server" >
                    <ItemTemplate>
                        <b><asp:LinkButton id="LinkButton3" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%#Eval("custToken")%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                    </ItemTemplate>
                </asp:DataList>
            </asp:Panel>

            <asp:Panel ID="pnlAccounts" runat="server" Visible="false">
                    <br /><asp:Label ID="lblAccount" runat="server"></asp:Label><br /><br />
                    <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList" AllowPaging="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE") & "," & Eval("UNIT_CODE")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                        Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & "&nbsp;" & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No information found.
                        </EmptyDataTemplate>
                    </asp:GridView>
            </asp:Panel>

            <asp:Panel ID="pnlForm" runat="server" Visible="false">
                <table cellpadding="0" cellspacing="0" >
                    <tr>
                        <td valign="top" align="right"><asp:label ID="labelShowAccount" Text="Account:&nbsp;" runat="server" Font-Bold="true" Visible="false" /></td>
                        <td><asp:linkbutton ID="lbShowAccount" runat="server" CausesValidation="False" Text="" /></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right"><asp:label ID="lblName" Text="* Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right"><asp:label ID="lblPhone" Text="* Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:TextBox ID="txtPhone" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone"
                                    Display="Dynamic" ErrorMessage="invalid format" ValidationGroup="avg" ValidationExpression="(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]‌​|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right"><asp:label ID="lblPaymentAmount" Text="* Payment Amount:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:TextBox ID="txtPaymentAmount" runat="server" Width="110px" ValidationGroup="avg" Enabled="false" />
                            <asp:RequiredFieldValidator ID="rfvPaymentAmount" runat="server" ControlToValidate="txtPaymentAmount" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right"><asp:label ID="lblPaymentDate" Text="* Payment Date:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:TextBox ID="txtPaymentDate" runat="server" ValidationGroup="avg" Width="110px"></asp:TextBox>
                            <asp:ImageButton ID="imgPaymentDate" ImageUrl="~/images/icon/icon_calendar.png" runat="server" /> (mm/dd/yyyy)
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtPaymentDate" Format="MM/dd/yyyy" PopupButtonID="imgPaymentDate" PopupPosition="TopRight"></cc1:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvPaymentDate" runat="server" ControlToValidate="txtPaymentDate" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revPaymentDate" runat="server" ControlToValidate="txtPaymentDate"
                                    Display="Dynamic" ErrorMessage="Date format MM/DD/YYYY" ValidationExpression="^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="avg">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right"><asp:label ID="lblPaymentType" Text="* Payment Type:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:DropDownList ID="ddlPaymentType" ValidationGroup="avg" runat="server" AutoPostBack="false" Width="110px" >
                                <asp:ListItem Value="" Text="" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Mail" Text="Mail"></asp:ListItem>
                                <asp:ListItem Value="Office" Text="Empire Office"></asp:ListItem>
                                <asp:ListItem Value="Collector" Text="Outside Payment Location"></asp:ListItem>
                                <asp:ListItem Value="Electronic" Text="Online or Phone Payment"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rvfPaymentType" runat="server" ControlToValidate="ddlPaymentType" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right"><asp:label ID="lblNotes" Text="Notes:&nbsp;" runat="server" Font-Bold="true" /></td>
                        <td>
                            <asp:TextBox ID="txtNotes" runat="server" ValidationGroup="avg" Width="300px" MaxLength="128" TextMode="MultiLine" Height="75px" />
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td><td><asp:Label ID="lblStatus" runat="server" ForeColor="Red" Text="" /></td></tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:LinkButton ID="lbPaymentAgreement" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg" OnClick="lbPaymentAgreement_Click">
                            <asp:label ID="lblPaymentAgreementButton" runat="server" Text="Submit Payment Agreement" /></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <br /><br />* (indicates required fields)
            </asp:Panel>

            <asp:Panel ID="pnlFinal" runat="server" Visible="false">
                <div style="padding:35px">
                    Your short-term payment arrangement has been confirmed for:<br /><br />
                    Payment Date: <asp:label ID="lblConfirmDate" runat="server" Text="" Font-Bold="true" />
                    <br />
                    Payment Amount: <asp:label ID="lblConfirmAmount" runat="server" Text="" Font-Bold="true" />
                    <br /><br />
                    * It may take up to 24 hours before this information is posted to your account.
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>