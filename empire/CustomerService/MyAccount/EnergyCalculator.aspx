﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="EnergyCalculator.aspx.vb" Inherits="CustomerService_MyAccount_EnergyCalculator" title="MyCalculator" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:TextBox ID="txtAcctTkn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtAcctPkgTkn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSPA" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtServiceType" runat="server" Visible="false"></asp:TextBox>
            <asp:panel ID="pnlAccount" runat="server">
                <asp:DataList id="dlBillPkg" skinId="dlistView" width="100%" runat="server" CellPadding="4" ForeColor="#333333">
                    <ItemTemplate>
                        <b><asp:LinkButton id="LinkButton1" font-bold="false" CommandArgument='<%# eval("bpToken") & ";" & eval("cdToken") %>' CommandName="Select" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Eval("bpToken")%>-<%#Eval("cdToken")%></asp:LinkButton></b>
                    </ItemTemplate>
                </asp:DataList>
            </asp:panel>    

            <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvUsage">
                <asp:View ID="vwAccounts" runat="server">
                    <br />
                    <asp:Label ID="lblAccount" runat="server"></asp:Label>
                    <br /><br />
                    
                    <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList" AllowPaging="false" >
                        <Columns>
                            <asp:TemplateField>
                               <ItemTemplate>
                                       <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE") & "," & Left(Eval("POSTAL_CODE"),5) & "," & Eval("service_point_tkn") & "," & Eval("premise_tkn")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                       Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                                       <!--& "&nbsp;&nbsp;" & Left(Eval("POSTAL_CODE"),5) & "-" & Right(Eval("POSTAL_CODE"),4)-->
                               </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No usage found.
                        </EmptyDataTemplate>
                    </asp:GridView> 
                    
                    <br /><br />
                    <asp:Panel ID="pnlNote" runat="server" Visible="false" Width="100%">
                        <div style="background-color:#113962;color:White;text-align:center;Font-weight:bold;border:solid 1px black;">*Note</div>
                        <div style="background-color:#cccccc;color:black;border:solid 1px black;text-align:center;">The calculator will open in a new window. If it does not open you may need to allow pop-ups or disable any pop-up blockers depending on your browser.</div>
                    </asp:Panel>                                        
                    
                </asp:View>
                
                <asp:View ID="vwResults" runat="server">
                    <asp:Label ID="lblOutput" runat="server" Text=""></asp:Label>
                </asp:View>
            </asp:MultiView>  

        </ContentTemplate>
    </asp:UpdatePanel>    
</asp:Content>
