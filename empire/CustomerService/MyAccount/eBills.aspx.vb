﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomerService_MyAccount_eBills
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            getStatus()
            btnOptOut.Attributes.Add("onclick", "return confirm('You will no longer receive MyEbill notifications.  Are you sure you want to return to paper bills?');")
        Else
            If (ckbAgree.Checked Or ckbNotAgree.Checked) Then
                lbSubmit.Enabled = True
            Else
                lbSubmit.Enabled = False
            End If
        End If
    End Sub

    Public Sub getStatus()
        Dim currentStatus As Integer = 0
        Dim currentEmail As String = ""
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader

        Try
            'get any "pending" ebill status changes
            conn.Open()
            cmdLookup.CommandText = "SELECT ebilling.actioncode from ebilling where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & Profile.BillingPackageToken & "' and status=1 and actioncode in (1,2)"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    currentStatus = CInt(drLookup("actioncode"))
                End While
            Else 'if there are no pending status changes
                'get ebill status from customer watch
                cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
                encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
                currentStatus = cwservice.GetEBillingStatus(encrypt, Profile.BillingPackageToken)
                currentStatus += 10  'indicates no pending changes
            End If
            drLookup.Close()
            conn.Close()

            Select Case currentStatus
                Case 1 'currently enrolled -- pending changes
                    lbleBillStatus.Text = "You have selected to receive MyEbill notifications.  Changes could take up to 24 hours to complete."
                    btnEnroll.Visible = False
                    btnOptOut.Visible = False
                Case 2  'not currently enrolled -- pending changes
                    lbleBillStatus.Text = "You have selected to no longer receive MyEbill notifications.  Changes could take up to 24 hours to complete."
                    btnOptOut.Visible = False
                    btnEnroll.Visible = False
                Case 10  'not currently enrolled -- no pending changes
                    lbleBillStatus.Text = "You are not currently enrolled with MyEbill.  You will continue to receive paper bills."
                    btnOptOut.Visible = False
                    btnEnroll.Visible = True
                Case 11  'currently enrolled -- no pending changes
                    lbleBillStatus.Text = "You are currently enrolled with MyEbill."
                    btnOptOut.Visible = True
                    btnEnroll.Visible = False
                Case Else
                    lbleBillStatus.Text = "You are not currently enrolled with MyEbill.  You will continue to receive paper bills."
                    btnOptOut.Visible = False
                    btnEnroll.Visible = True
            End Select
            currentEmail = syncEmail()
            lblEmail.Text = currentEmail 'Membership.GetUser(User.Identity.Name).Email
            lblEmailConfirm.Text = currentEmail 'Membership.GetUser(User.Identity.Name).Email
        Catch
        End Try
    End Sub

    Protected Sub btnOptOut_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        updateTable(2) 'build database entry for customerwatch
        getStatus()
        UpdatePanel1.Update()
    End Sub

    Protected Sub btnEnroll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlStatus.Visible = False
        pnlEnroll.Visible = True
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblAgreeStatus.Text = ""  'reset form fields
        lblEmailConfirmStatus.Text = ""
        ckbAgree.Checked = False
        ckbNotAgree.Checked = False
        ckbEmailConfirm.Checked = False

        pnlEnroll.Visible = False
        pnlStatus.Visible = True
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ckbEmailConfirm.Checked Then
            lblEmailConfirmStatus.Text = "You must confirm your email address"
        ElseIf (Not ckbAgree.Checked And Not ckbNotAgree.Checked) Or (ckbAgree.Checked And ckbNotAgree.Checked) Then
            lblAgreeStatus.Text = "You must accept the terms and conditions"
        ElseIf (Not ckbAgree.Checked And ckbNotAgree.Checked) Then
            ckbAgree.Checked = False  'selecting the "Not Agree" same as using the "Cancel" button
            ckbNotAgree.Checked = False
            ckbEmailConfirm.Checked = False
            pnlEnroll.Visible = False
            pnlStatus.Visible = True
        ElseIf (ckbAgree.Checked And Not ckbNotAgree.Checked) Then
            lblAgreeStatus.Text = ""  'reset form fields
            lblEmailConfirmStatus.Text = ""
            ckbAgree.Checked = False
            ckbEmailConfirm.Checked = False

            pnlEnroll.Visible = False
            pnlStatus.Visible = True
            updateTable(1) 'build database entry for customerwatch
            getStatus()
            UpdatePanel1.Update()
        End If
    End Sub

    Protected Sub lbEmailUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEmailUpdate.Click
        'validate input data (cannot use ajax field validators inside modal popup)
        If Not (Regex.IsMatch(txtNewEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")) Or Not (Regex.IsMatch(txtConfirmEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")) Then
            lblEmailUpdateStatus.Text = "Invalid email Format"
            mpeEmailDetailExtender.Show()
        ElseIf Not txtNewEmail.Text = txtConfirmEmail.Text Then
            lblEmailUpdateStatus.Text = "Email addresses do not match"
            mpeEmailDetailExtender.Show()
        Else
            Dim u As MembershipUser
            u = Membership.GetUser(User.Identity.Name)
            u.Email = txtNewEmail.Text
            Membership.UpdateUser(u)  'update email
            updateTable(3) 'build database entry for customerwatch
            lblEmailUpdateStatus.Text = ""
            lblEmailConfirm.Text = Membership.GetUser(User.Identity.Name).Email
            mpeEmailDetailExtender.Hide()
            UpdatePanel1.Update()
        End If
    End Sub

    Protected Sub lbEmailCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEmailCancel.Click
        lblEmailUpdateStatus.Text = ""
        mpeEmailDetailExtender.Hide()
    End Sub

    Protected Sub lbEmailConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEmailConfirm.Click
        mpeEmailDetailExtender.Show()
        txtNewEmail.Focus()
    End Sub

    Public Function syncEmail() As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim cwEmail As String = ""
        Dim msEmail As String = ""
        Dim pendingEmail As String = ""
        Dim email As String = ""
        Try
            'get email from membership services
            msEmail = Membership.GetUser(User.Identity.Name).Email
            email = msEmail  'set display email address

            'get email from customer watch
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            cwEmail = cwservice.GetEmailAddress(encrypt, Profile.BillingPackageToken)

            'get for any "pending" email changes
            conn.Open()
            cmdLookup.CommandText = "SELECT ebilling.email from ebilling where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & Profile.BillingPackageToken & "' and status=1"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    pendingEmail = drLookup("email").ToString
                End While
                email = pendingEmail  'display current email address  -- currently what is in membership services AND pending
            Else 'if there are no pending email changes
                'sync membership services with customer watch if the emails do not match
                If (Len(cwEmail) > 0) Then  'if customer watch has email address
                    If (cwEmail <> msEmail) Then  'if customer watch does not match membership services
                        Dim u As MembershipUser
                        u = Membership.GetUser(User.Identity.Name)
                        u.Email = cwEmail
                        Membership.UpdateUser(u)
                    End If
                    email = cwEmail  'display current email address  -- currently on file in customer watch
                End If
            End If
            drLookup.Close()
            conn.Close()
            Return email
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return email
        End Try
    End Function

    Public Sub updateTable(ByVal actioncode As Integer) 'build table data for customer watch
        Dim recordExists As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim comment As String = ""
        Dim insertquery As String = ""
        Dim updatequery As String = ""

        Try
            Select Case actioncode
                Case 1
                    comment = "activate ebilling"
                Case 2
                    comment = "opt-out ebilling"
                Case 3
                    comment = "update email"
            End Select

            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            Dim billPackages As String = cwservice.GetBillPackage(encrypt)
            If billPackages Is Nothing Then
            Else
                conn.Open()
                Dim billPkgTkns As Array = Split(billPackages, ",")
                For index As Int32 = 0 To billPkgTkns.Length - 1  'enroll every active billing package for customer_tkn
                    insertquery = "insert into ebilling(customer_tkn,billing_pkg_tkn,email,status,actioncode,comment,lastupdate) " & _
                    "values('" & Profile.CustomerAccountNumber & "','" & billPkgTkns(index) & "','" & Membership.GetUser(User.Identity.Name).Email & "'," & 1 & "," & actioncode & ",'" & comment & "','" & Now & "')"
                    updatequery = "update ebilling set ebilling.status=1,ebilling.actioncode=" & actioncode & ",ebilling.email='" & Membership.GetUser(User.Identity.Name).Email & "',ebilling.lastupdate='" & Now & "',ebilling.comment='" & comment & "' where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & billPkgTkns(index) & "'"
                    cmdLookup.CommandText = "SELECT count(*) as result from ebilling where ebilling.customer_tkn = '" & Profile.CustomerAccountNumber & "' and ebilling.billing_pkg_tkn = '" & billPkgTkns(index) & "'"
                    drLookup = cmdLookup.ExecuteReader
                    If drLookup.HasRows Then
                        While drLookup.Read
                            recordExists = drLookup("result").ToString
                        End While
                    End If
                    drLookup.Close()
                    If recordExists Then  'add record if it doesnt exist, otherwise update record
                        cmd.CommandText = updatequery
                    Else
                        cmd.CommandText = insertquery
                    End If
                    cmd.ExecuteNonQuery()
                Next
                conn.Close()
            End If
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

End Class
