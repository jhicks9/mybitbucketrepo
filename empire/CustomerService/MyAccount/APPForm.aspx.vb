﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomerService_MyAccount_APPForm
    Inherits System.Web.UI.Page

    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Dim email As New clientInfo

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim custToken As String = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
        Dim billPkgTkn As String = cd.ZeroPad(Profile.BillingPackageToken, 2)
        Dim acctpToken As String = ""
        Dim acctToken As String = ""

        If (Not Page.IsPostBack) Then
            lbSubmit.ForeColor = Drawing.Color.Gray
            showBillPackages()
        Else
            If (ckbAgree.Checked) Then
                lbSubmit.ForeColor = Drawing.Color.DarkBlue
                lbSubmit.Enabled = True
            Else
                lbSubmit.ForeColor = Drawing.Color.Gray
                lbSubmit.Enabled = False
            End If
        End If
    End Sub

    Public Sub showBillPackages()
        Try
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)

            If rtnBillPackages Is Nothing Then  'no valid billing packages
                Dim lbl As New Label
                lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                lbl.ForeColor = Drawing.Color.Red
                pnlBillPackages.Controls.Add(lbl)
            Else
                Dim billPkgTkn As Array = Split(cd.ZeroPad(rtnBillPackages, 2), ",")
                Dim dt As New DataTable()
                Dim dr As DataRow
                dt.Columns.Add(New DataColumn("custToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("bpToken", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("cdToken", System.Type.GetType("System.String")))
                For i = 0 To billPkgTkn.Length - 1
                    dr = dt.NewRow()
                    dr("custToken") = cd.ZeroPad(Profile.CustomerAccountNumber, 6)
                    dr("bpToken") = cd.ZeroPad(billPkgTkn(i), 2)
                    dr("cdToken") = cd.CalculateCheckDigit(cd.ZeroPad(Profile.CustomerAccountNumber, 6) & cd.ZeroPad(billPkgTkn(i), 2))
                    dt.Rows.Add(dr)
                Next i
                dlBillPkg.DataSource = dt
                dlBillPkg.DataBind()
            End If
        Catch
        End Try
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim arTokens As Array = Split(e.CommandArgument, ";")  'arTokens(0) = billpToken

            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, arTokens(0))
            hdnBillPackage.Value = arTokens(0)

            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            dlBillPkg.Visible = False
        Catch
        End Try
    End Sub

    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        Try
            Dim ci As New clientInfo
            Dim eCmdArg As String = e.CommandArgument
            Dim arTokens As Array = Split(eCmdArg, ",")

            lblLocation.Text = Profile.CustomerAccountNumber & "-" & arTokens(0) & "-" & arTokens(9)
            lblService.Text = arTokens(2)
            lblAddress.Text = arTokens(3)
            lblCity.Text = arTokens(4)
            lblState.Text = arTokens(5)
            lblZipCode.Text = arTokens(8)
            lblAccount.Text = Profile.CustomerAccountNumber & "-" & hdnBillPackage.Value & "-" & cd.CalculateCheckDigit(Profile.CustomerAccountNumber & hdnBillPackage.Value)

            'get email from customer watch
            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            lblEmail.Text = cwservice.GetEmailAddress(encrypt, Profile.BillingPackageToken)
            If ci.IsEmail(lblEmail.Text) Then
            Else
                'get email from membership services
                lblEmail.Text = Membership.GetUser(User.Identity.Name).Email
            End If

            'populate due date availability
            Select Case arTokens(2).ToString
                Case "Electric"
                    cboDueDate.Items.Add("1st")
                    cboDueDate.Items.Add("5th")
                    cboDueDate.Items.Add("10th")
                    cboDueDate.Items.Add("15th")
                    cboDueDate.Items.Add("20th")
                    cboDueDate.Items.Add("25th")
                Case "Gas"
                    cboDueDate.Items.Add("1st")
                    cboDueDate.Items.Add("5th")
                    cboDueDate.Items.Add("20th")
                    cboDueDate.Items.Add("25th")
            End Select

            encrypt = tdes.Encrypt(Profile.CustomerAccountNumber)
            Dim onAPP As String = cwservice.GetOnAveragePayment(encrypt, arTokens(0), arTokens(9))
            If onAPP = "Yes" Then
                lblEmailSent.Text = "<b>Account is already enrolled in the Average Payment Plan.</b>"
                pnlForm.Visible = False
                pnlBillPackages.Visible = False
                pnlFinal.Visible = True
            Else
                If getAPPStatus() Then
                    lblEmailSent.Text = "<b>Average Payment Plan request currently pending.</b>"
                    pnlForm.Visible = False
                    pnlBillPackages.Visible = False
                    pnlFinal.Visible = True
                Else
                    pnlBillPackages.Visible = False
                    pnlForm.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strEmailBody As String = "Average Payment Enrollment Request"
        Try
            strEmailBody &= vbCrLf & "Name: " & txtName.Text & vbCrLf & "Address: " & lblAddress.Text & vbCrLf
            strEmailBody &= "City, State zip: " & lblCity.Text & "," & lblState.Text & " " & lblZipCode.Text & vbCrLf & "Phone: " & txtPhone.Text & vbCrLf
            strEmailBody &= "Account #: " & lblAccount.Text & vbCrLf
            strEmailBody &= "Location #:" & lblService.Text & " " & lblLocation.Text & vbCrLf
            strEmailBody &= "Email: " & lblEmail.Text & vbCrLf
            strEmailBody &= "Change Due Date to: " & cboDueDate.SelectedItem.ToString & " if possible"

            Dim toList As New ArrayList
            toList.Add("customer.service@empiredistrict.com")
            email.sendEMail(toList, "APPForm@empiredistrict.com", "Average Payment Enrollment Contact Form", strEmailBody.ToString)

            toList.Clear() 'send confirmation to myAccount user
            toList.Add(Membership.GetUser(User.Identity.Name).Email)
            email.sendEMail(toList, "customer.service@empiredistrict.com", "MyAccount Average Payment Enrollment", "Thank you for submitting your Average Payment Plan request." & vbCrLf & vbCrLf)

            LogData()
            lblEmailSent.Text = "<b>Thank you for submitting your APP request.</b>"
            pnlForm.Visible = False
            pnlFinal.Visible = True
        Catch ex As Exception
            lblStatus.Text = ex.Message.ToString
        End Try
    End Sub

    Public Sub LogData()
        Dim ci As New clientInfo
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim drLookup As SqlDataReader
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim recordExists As Boolean = False
        Try
            Dim insertquery As String = "insert into log (date,ipaddress,refer,username,platform,web) VALUES (GETDATE(),'" & ci.GetIP4Address() & "','" & lblLocation.Text & "','APP','" & lblAccount.Text & " " & lblService.Text & "','enrollment date " & DateTime.Now & "')"
            Dim updatequery As String = "update log set log.date=GETDATE(),log.ipaddress='" & ci.GetIP4Address() & "' where username='APP' and log.refer='" & lblLocation.Text & "'"
            conn.Open()
            cmdLookup.CommandText = "SELECT count(*) as result from log where username='APP' and log.refer = '" & lblLocation.Text & "'"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            If recordExists Then  'add record if it doesnt exist, otherwise update record
                cmd.CommandText = updatequery
            Else
                cmd.CommandText = insertquery
            End If
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Public Function getAPPStatus() As Boolean
        Dim recordExists As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim drLookup As SqlDataReader
            Dim cmdLookup As New SqlCommand("", conn)
            conn.Open()
            cmdLookup.CommandText = "SELECT count(*) as result from log where username='APP' and log.refer = '" & lblLocation.Text & "'"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            conn.Close()
        Catch ex As System.Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            recordExists = False
        End Try
        Return recordExists
    End Function
End Class
