<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ChangeEmail.aspx.vb" Inherits="CustomerService_MyAccount_ChangeEmail" title="Change MyEmail" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:Panel id="pnlMain" runat="server">
        <br />
        <div style="float:left;width:130px;padding-top:3px;">
            <asp:Label ID="lblCurrentEmail" runat="server" Text="Current Email: "></asp:Label>
        </div>
        <div style="float:left;width:200px;">
            <asp:TextBox ID="txtCurrentEmail" runat="server" enabled="false" width="200px"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator1" ControlToValidate="txtCurrentEmail" runat="server" ErrorMessage="email required.">*</asp:RequiredFieldValidator>
            <br />
        </div>
        <br style="clear:both;"/>
        <div style="float:left;width:130px;padding-top:3px;">
            <asp:Label ID="lblNewEmail" runat="server" Text="New Email: "></asp:Label>
        </div>
        <div style="float:left;width:200px;">
            <asp:TextBox ID="txtNewEmail" runat="server" width="200px"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" ControlToValidate="txtNewEmail" runat="server" ErrorMessage="email required.">*</asp:RequiredFieldValidator>&nbsp;
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNewEmail"
                ErrorMessage="invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></div>
        <br style="clear:both;"/>
        <div style="float:left;width:130px;padding-top:3px;">
            <asp:Label ID="lblConfirmEmail" runat="server" Text="Confirm New Email: "></asp:Label>
        </div>
        <div style="float:left;width:200px;">
            <asp:TextBox ID="txtConfirmEmail" runat="server" width="200px"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator3" ControlToValidate="txtConfirmEmail" runat="server" ErrorMessage="email required.">*</asp:RequiredFieldValidator>&nbsp;
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtConfirmEmail"
                ErrorMessage="invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></div>
        <br style="clear:both;"/>            
            
        <div style="float:left;margin-left:165px;">
            <asp:LinkButton ID="lbChange" runat="server" CssClass="ovalbutton" OnClick="lbChange_Click" ><span>Change Email</span></asp:LinkButton>
            <div style="float:left;width:6px;">&nbsp;</div>
            <asp:LinkButton ID="lbCancel" runat="server" CssClass="ovalbutton" CausesValidation="false" PostBackUrl="~/customerservice/myaccount/default.aspx"><span>Cancel</span></asp:LinkButton>
        </div>
        <br style="clear:both;"/>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>

    <asp:Panel id="pnlEmailChanged" runat="server" Visible="false">
        <br />Your Email address has been changed to: <asp:Label ID="lblNewEmailNotify" runat="server"></asp:Label>
    </asp:Panel>
</asp:Content>

