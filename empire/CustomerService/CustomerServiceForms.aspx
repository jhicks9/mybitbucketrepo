﻿<%@ Page Title="Customer Service Forms" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="CustomerServiceForms.aspx.vb" Inherits="CustomerService_CustomerServiceForms" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lbResidential" runat="server" CausesValidation="true" Text="Residential" Font-Bold="true"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbCommercial" runat="server" CausesValidation="true" Text="Commercial" Font-Bold="false"/>
            
                <asp:Panel ID="pnlResidential" runat="server" HeaderText="Residential" Visible="true">
                    <ContentTemplate>
                        <br />
                        <table cellpadding="5" cellspacing="0" style="padding-left:5px;">
                            <tr>
                                <td><asp:Image ID="imgDocument1" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceConnectResidential" Text='Start Service' NavigateUrl='~/CustomerService/ServiceConnectResidential.aspx' runat="server"/></td>
                            </tr>
                            <tr>
                                <td><asp:Image ID="imgDocument2" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceTransferResidential" Text='Transfer Service' NavigateUrl='~/CustomerService/ServiceTransfer.aspx' runat="server"/></td>
                            </tr>
                            <tr>
                                <td><asp:Image ID="imgDocument3" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceDisconnectResidential" Text='Disconnect Service' NavigateUrl='~/CustomerService/ServiceDisconnect.aspx' runat="server"/></td>
                            </tr>
                         <%--<tr>
                                <td><asp:Image ID="imgDocument7" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceStandardsResidential" Text='Service Standards' NavigateUrl='~/CustomerService/Electric.aspx?tab=5' runat="server"/></td>
                            </tr>--%>
                        </table>
                        <asp:DataList ID="dlResidential" runat="server" SkinID="DataList" DataSourceID="sqlResidential" Visible="true">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' 
                                    Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlResidential" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 121  and (webdescription like '%electric' or webdescription like 'electric%' or webdescription like '%electric%' or webdescription like '%gas' or webdescription like '%gas%' or webdescription like 'gas%') or ((documents.groupid = 14 AND documents.doctypeid = 70) and (webdescription like '%residential%' or webdescription like '%generator%')) order by documents.webdescription">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlResidential--%>

                <asp:Panel ID="pnlCommercial" runat="server" HeaderText="Commercial" Visible="false">
                    <ContentTemplate>
                        <br />
                        <table cellpadding="5" cellspacing="0" style="padding-left:5px;">
                            <tr>
                                <td><asp:Image ID="imgDocument4" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceConnectCommercial" Text='Start Service' NavigateUrl='~/CustomerService/ServiceConnectCommercial.aspx' runat="server"/></td>
                            </tr>
                            <tr>
                                <td><asp:Image ID="imgDocument5" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceTransferCommercial" Text='Transfer Service' NavigateUrl='~/CustomerService/ServiceTransfer.aspx' runat="server"/></td>
                            </tr>
                            <tr>
                                <td><asp:Image ID="imgDocument6" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceDisconnectCommercial" Text='Disconnect Service' NavigateUrl='~/CustomerService/ServiceDisconnect.aspx' runat="server"/></td>
                            </tr>
                        <%--<tr>
                                <td><asp:Image ID="imgDocument8" ImageAlign="Top" ImageUrl="~/images/icon/icon_bullet.gif" runat="server" /></td>
                                <td><asp:HyperLink ID="hlServiceStandardsCommercial" Text='Service Standards' NavigateUrl='~/CustomerService/Electric.aspx?tab=5' runat="server"/></td>
                            </tr>--%>
                        </table>
                        <asp:DataList ID="dlCommercial" runat="server" SkinID="DataList" DataSourceID="sqlCommercial" Visible="true">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                                    NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' 
                                    Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlCommercial" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 122  and (webdescription like '%electric' or webdescription like 'electric%' or webdescription like '%electric%' or webdescription like '%gas' or webdescription like '%gas%' or webdescription like 'gas%')  or ((documents.groupid = 14 AND documents.doctypeid = 70) and (webdescription like '%commercial%' or webdescription like '%generator%')) order by documents.webdescription">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlCommercial--%>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

