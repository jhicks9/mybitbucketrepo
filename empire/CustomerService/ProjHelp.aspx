<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ProjHelp.aspx.vb" Inherits="CustomerService_ProjHelp" title="Project Help" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div><img src="~/images/projecthelplogo.gif" runat="server" alt="projlogo"/></div>
        <asp:Panel ID="pnlElectric" runat="server" HeaderText="Electric" Visible="true">
            <ContentTemplate>
                <br />
                <div class="sectionheader">Electric</div>
                <hr style="color:#113962;height:1px;"/>
                <b><i>Project Help, a project of the Empire District Electric Company, is neighbors helping neighbors.</i></b>
                <br /><br />
                Helping our neighbors is a tradition in our area, built over the years and based on our common goals and needs. Since 1982, Project Help has been one example of that neighborly spirit at work.
                <br /><br />
                <b><i>What is Project Help?</i></b>
                <br /><br />
                Project Help is an assistance program created to meet emergency energy-related expenses of the elderly and/or disabled residents in Empire's electric service area. For many of these people, protection against extreme heat or cold can be a matter of life or death.
                <br /><br />
                Project Help is funded through voluntary donations.
                <br /><br />
                <b><i>Who is eligible for Project Help Assistance?</i></b>
                <br /><br />
                To be eligible for Project Help, applicants must be a customer of Empire District Electric, be 60 years of age or older, or be disabled to the extent that they cannot be economically self-sufficient, and have no other resource available. Eligibility is verified when making application.
                <br /><br />
                To apply for Project Help assistance, please call Empire District at 800-206-2300 for application requirements, or visit us at 602 S. Joplin Ave., Joplin, MO.
                <br /><br />
                <b><i>What qualifies as "emergency energy-related expenses"?</i></b>
                <br /><br />
                Any energy bill of the truly needy for heating or cooling may qualify for Project Help assistance.
                <br /><br />
                Project Help will determine when emergency needs exist and administer the use of available funds to meet those needs. Project Help is not intended to provide ongoing support, but to provide temporary assistance for those persons in a crisis situation.
                <br /><br />
                <b><i>How customers can make a donation.</i></b>
                <br /><br />
                It's simple! Just complete the online <a id="A1" href="~/CustomerService/ProjHelpForm.aspx?type=e" runat="server" target="_blank" >donation form.</a>
                <br /><br />
                All customer contributions received by Empire will be given monthly to Project Help to be used to help pay emergency energy costs of the truly needy.
                <br /><br />
                Naturally, your contributions are tax deductible.
                <br /><br />
                Your total Project Help contributions for the previous year will be reported on your January bill each year.
                <br /><br />
                <b><i>All money goes to Project Help.</i></b>
                <br /><br />
                Empire's role in Project Help is to encourage donations by offering Empire customers a convenient means of contributing. The full amount of each contribution to Project Help will go to the ultimate beneficiaries. There are no deductions for salaries, administrative costs, or other expenses. And, of course, all the money stays within the Empire District Electric Company electric service area. Project Help is truly neighbors helping neighbors.
            </ContentTemplate>
        </asp:Panel><%--end pnlElectric--%>
</asp:Content>

