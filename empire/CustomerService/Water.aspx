<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Water.aspx.vb" Inherits="EnergySolutions_Water" title="Water Rates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
                
            <asp:LinkButton ID="lbWRate" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Water.aspx" Text="Water Rates" />&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbWReports" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Water.aspx?water=REPORT" Text="Water Reports"/>
            <br />&nbsp

                <asp:Panel ID="pnlRate" runat="server" HeaderText="Water Rates">
                    <ContentTemplate>
                        <b>Bill Explanation</b>
                        <br /><br />
                        <asp:DataList ID="dlBillExplanation" runat="server" SkinID="DataList" DataSourceID="sqlBillExplanation">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlBillExplanation" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 129 ORDER BY documents.webdescription">
                        </asp:SqlDataSource> 
                        <br /><hr style="color:#113962;height:1px;"/>
                        <b>Water Rates</b><br /><br />
                        <asp:DataList ID="dlWaterRates" runat="server" SkinID="DataList" DataSourceID="sqlDSRates">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSRates" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 68 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlRate--%>
                
                <asp:Panel ID="pnlReports" runat="server" HeaderText="Water Reports" Visible="false">
                    <ContentTemplate>
                        <b>Water Reports</b><br /><br />
                        <asp:DataList ID="dlWaterReports" runat="server" SkinID="DataList" DataSourceID="sqlDSReports">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSReports" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 69 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlReports--%>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>