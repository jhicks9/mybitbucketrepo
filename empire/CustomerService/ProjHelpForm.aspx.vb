
Partial Class CustomerService_ProjHelpForm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("type") = "g" Then
            lblType.Text = "Gas"
            lblCheckType.Text = "Gas"
        End If
        If Request.QueryString("type") = "e" Then
            lblType.Text = "Electric"
            lblCheckType.Text = "Electric"
        End If
        lblDate.Text = Now.ToString
    End Sub
End Class
