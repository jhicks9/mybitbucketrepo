﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AvoidScams.aspx.vb" Inherits="CustomerService_AvoidScams" %>

<asp:Content ID="Content4" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <center><b>Empire Warns Customers to Avoid Scams</b></center>
    <br />
    Don’t become a victim of a fraudulent payment or service request. Know the facts about Empire’s payment practices.
    <br /><br />
    <b>Red Flags – End Contact and Report</b>
    <ul>
        <li>Requests for payment over the phone by prepaid credit card.</li>
        <li>Calls on the weekend demanding immediate payment.</li>
        <li>Individuals asking to enter your home to inspect wiring or equipment, unless you have called Empire at 800-206-2300 and requested an appointment with us.</li>
        <li>Individuals requesting payment or offering services that are not in an Empire vehicle, do not have a company identification card and are not wearing an Empire logo/uniform.</li>
        <li>Emails requesting personal, account or financial information.</li>
        <li>Emails including attachments, unless you’ve contacted us to specifically request the information.</li>
    </ul>
    <br />
    <b>Valid Empire Practices</b>
    <ul>
        <li>We provide various payment options including:</li>
        <ul>
            <li>MyEbill – using secure online access through MyAccount</li>
            <li>Credit, debit or ACH - online or by phone using third party vendor Western Union Speedpay.</li>
            <li>Other – Visit www.empiredistrict.com/CustomerService and click on Payment Information for a complete list of payment options.</li>
        </ul>
        <li>We will notify you before a disconnection by mail, door tag and/or by phone during normal business hours. A field representative with proper identification may also notify you in person.</li>
    </ul>
    If you receive a call or visit that appears suspicious, always end the contact immediately and report it to local law enforcement.  You may call Empire anytime at 800-206-2300 to check on your account status and verify whether the contact is legitimate.  Also, if you have family or friends who may be at risk of falling for one of these scams, be sure to discuss this important information with them.
</asp:Content>

