﻿
Partial Class CustomerService_CustomerServiceForms
    Inherits System.Web.UI.Page

    Protected Sub Button_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbResidential.Click, lbCommercial.Click
        Dim btn As LinkButton = CType(sender, LinkButton)
        If btn.ID = "lbResidential" Then
            pnlResidential.Visible = True
            pnlCommercial.Visible = False
            lbResidential.Font.Bold = True
            lbCommercial.Font.Bold = False
        ElseIf btn.ID = "lbCommercial" Then
            pnlCommercial.Visible = True
            pnlResidential.Visible = False
            lbCommercial.Font.Bold = True
            lbResidential.Font.Bold = False
        End If
    End Sub

End Class
