﻿
Partial Class CustomerService_TelevisionCommercials
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Select Case Request.QueryString("v")
            Case 1 'Meeting Energy Challenges
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/F1XlNCDDJ0g"
                pnlYouTube.Visible = True
            Case 2  'The value of Electricity 1
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/LOVVsayzfwg"
                pnlYouTube.Visible = True
            Case 3 'The Value of Electricity 2
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/aePxmveiXR0"
                pnlYouTube.Visible = True
            Case 4  'Our Town
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/YbGG6fLjPq8"
                pnlYouTube.Visible = True
            Case 5 'Century of Service
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/Dfq65y63Z_0"
                pnlYouTube.Visible = True
            Case 6 'Safely Powering Your Life
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/1oLbkV7bo-g"
                pnlYouTube.Visible = True
            Case 7 'Power the Future - Reliability
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/tSj6BsJjS0A"
                pnlYouTube.Visible = True
            Case 8 'Power the Future - Efficiency
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/H_9Zq-D4nNM"
                pnlYouTube.Visible = True
            Case 9 'Power the Future - Capacity
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/fBhpsFGZI38"
                pnlYouTube.Visible = True
            Case 10 'Ice Storm 2007
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/FMTkDhGxx78"
                pnlYouTube.Visible = True
            Case 11 'Natural Gas, the Natural Choice
                ifYouTube.Attributes("src") = "http://www.youtube.com/embed/3hWaViQO-FA"
                pnlYouTube.Visible = True
        End Select
    End Sub

    Protected Sub lbChallenges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbChallenges.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/F1XlNCDDJ0g"
    End Sub

    Protected Sub lbValueElectricty1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbValueElectricty1.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/LOVVsayzfwg"
    End Sub

    Protected Sub lbValueElectricty2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbValueElectricty2.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/aePxmveiXR0"
    End Sub

    Protected Sub lbOurTown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbOurTown.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/YbGG6fLjPq8"
    End Sub

    Protected Sub lbCenturyService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCenturyService.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/Dfq65y63Z_0"
    End Sub

    Protected Sub lbEmpireSafety_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEmpireSafety.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/1oLbkV7bo-g"
    End Sub

    Protected Sub lbPowerFutureReliability_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPowerFutureReliability.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/tSj6BsJjS0A"
    End Sub

    Protected Sub lbPowerFutureEfficiency_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPowerFutureEfficiency.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/H_9Zq-D4nNM"
    End Sub

    Protected Sub lbPowerFutureCapacity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPowerFutureCapacity.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/fBhpsFGZI38"
    End Sub

    Protected Sub lbIceStorm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbIceStorm.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/FMTkDhGxx78"
    End Sub

    Protected Sub lbNaturalGas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbNaturalGas.Click
        If pnlYouTube.Visible = False Then
            pnlYouTube.Visible = True
        End If
        ifYouTube.Attributes("src") = "http://www.youtube.com/embed/3hWaViQO-FA"
    End Sub


End Class
