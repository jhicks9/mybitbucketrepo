﻿
Partial Class CustomerService_Electric
    Inherits System.Web.UI.Page

    Public Sub selectedTab(Optional ByVal tab As String = "")
                pnlState.Visible = False
                pnlFederal.Visible = False
                pnlSS.Visible = False
                lbMO.Font.Bold = False
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
                lbFED.Font.Bold = False
                lbSS.Font.Bold = False
        If Not String.IsNullOrEmpty(tab) Then
            Select Case tab
                Case "AR"
                    sqlBE.SelectParameters("doctypeid").DefaultValue = "106"
                    dlBillExplanation.DataBind
                    sqlResidential.SelectParameters("doctypeid").DefaultValue = "71"
                    dlResidential.DataBind
                    sqlCommercial.SelectParameters("doctypeid").DefaultValue = "72"
                    dlCommercial.DataBind
                    sqlIndustrial.SelectParameters("doctypeid").DefaultValue = "73"
                    dlIndustrial.DataBind

                    pnlState.Visible = True
                    lbAR.Font.Bold = True
                Case "KS"
                    sqlBE.SelectParameters("doctypeid").DefaultValue = "107"
                    dlBillExplanation.DataBind
                    sqlResidential.SelectParameters("doctypeid").DefaultValue = "74"
                    dlResidential.DataBind
                    sqlCommercial.SelectParameters("doctypeid").DefaultValue = "75"
                    dlCommercial.DataBind
                    sqlIndustrial.SelectParameters("doctypeid").DefaultValue = "76"
                    dlIndustrial.DataBind

                    pnlState.Visible = True
                    lbKS.Font.Bold = True
                Case "OK"
                    sqlBE.SelectParameters("doctypeid").DefaultValue = "109"
                    dlBillExplanation.DataBind
                    sqlResidential.SelectParameters("doctypeid").DefaultValue = "80"
                    dlResidential.DataBind
                    sqlCommercial.SelectParameters("doctypeid").DefaultValue = "81"
                    dlCommercial.DataBind
                    sqlIndustrial.SelectParameters("doctypeid").DefaultValue = "82"
                    dlIndustrial.DataBind

                    pnlState.Visible = True
                    lbOK.Font.Bold = True
                Case "FED"
                    pnlFederal.Visible = True
                    lbFED.Font.Bold = True
                Case "SS"
                    pnlSS.Visible = True
                    lbSS.Font.Bold = True
                Case Else 'Missouri
            End Select
        Else 'Missouri
            sqlBE.SelectParameters("doctypeid").DefaultValue = "108"
            dlBillExplanation.DataBind
            sqlResidential.SelectParameters("doctypeid").DefaultValue = "77"
            dlResidential.DataBind
            sqlCommercial.SelectParameters("doctypeid").DefaultValue = "78"
            dlCommercial.DataBind
            sqlIndustrial.SelectParameters("doctypeid").DefaultValue = "79"
            dlIndustrial.DataBind

            pnlState.Visible = True
            lbMO.Font.Bold = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("electric")) Then
                selectedTab(Request.QueryString("electric"))
            Else
                selectedTab()
            End If
        Catch
        End Try
    End Sub
End Class
