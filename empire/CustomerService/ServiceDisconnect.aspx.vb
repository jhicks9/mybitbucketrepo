﻿
Partial Class CustomerService_ServiceDisconnect
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtAccountCT.Attributes("onkeyup") = "autotab(" + txtAccountCT.ClientID + ", " + txtAccountBP.ClientID + ")"
        txtAccountBP.Attributes("onkeyup") = "autotab(" + txtAccountBP.ClientID + ", " + txtAccountCD.ClientID + ")"
        txtAccountCD.Attributes("onkeyup") = "autotab(" + txtAccountCD.ClientID + ", " + txtAccountName.ClientID + ")"
        txtSSN1.Attributes("onkeyup") = "autotab(" + txtSSN1.ClientID + ", " + lbVerifyAccount.ClientID + ")"
        txtPrimaryPhone1.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone1.ClientID + ", " + txtPrimaryPhone2.ClientID + ")"
        txtPrimaryPhone2.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone2.ClientID + ", " + txtPrimaryPhone3.ClientID + ")"
        txtPrimaryPhone3.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone3.ClientID + ", " + txtEmail.ClientID + ")"
        txtAccountCT.Focus()
    End Sub

    Protected Sub lbVerifyAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbVerifyAccount.Click
        Try
            lblVerificationStatus.Text = ""
            encrypt = tdes.Encrypt(txtAccountCT.Text)
            Dim strBillPkg As String = Trim(txtAccountBP.Text)
            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            Dim status As Integer = cwservice.ValidateCustomer(encrypt, strBillPkg, txtAccountName.Text, txtSSN1.Text)

            If status = 1 Then 'move to next panel
                pnlVerification.Visible = False
                pnlCustomerInfo.Visible = True
                txtServiceAddress.Focus()
            ElseIf status = 0 Then
                lblVerificationStatus.ForeColor = Drawing.Color.Red
                lblVerificationStatus.Text = "Cannot find account, please verify account information."
            ElseIf status = 2 Then
                lblVerificationStatus.ForeColor = Drawing.Color.Red
                lblVerificationStatus.Text = "Unable to verify account. Please contact Customer Service at 800-206-2300."
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSubmit.Click
        Dim MailObj As New System.Net.Mail.SmtpClient
        Dim message As New System.Net.Mail.MailMessage()
        Dim fromAddress As New System.Net.Mail.MailAddress("admin@empiredistrict.com")
        Dim strEmailBody As String = "Service Disconnect Request Form" & vbCrLf & vbCrLf
        Try
            lbSubmit.Enabled = False  'eliminate double clicking the submit button
            message.From = fromAddress
            message.To.Add("customer.accounts@empiredistrict.com")
            message.Subject = "Service Disconnect Request Form"
            'MailObj.Host = "" -- obtained from web.config 03/14/2012

            strEmailBody += "Account: " & txtAccountCT.Text & "-" & txtAccountBP.Text & "-" & txtAccountCD.Text & vbCrLf
            strEmailBody += "Name: " & txtAccountName.Text & vbCrLf
            strEmailBody += "Service Address: " & txtServiceAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtServiceAddressAN.Text & " Lot Number: " & txtServiceAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtServiceAddressCity.Text & ", " & txtServiceAddressState.Text & " " & txtServiceAddressZip.Text & vbCrLf
            strEmailBody += "Primary Phone: (" & txtPrimaryPhone1.Text & ")" & txtPrimaryPhone2.Text & "-" & txtPrimaryPhone3.Text & vbCrLf
            strEmailBody += "Email address: " & txtEmail.Text & vbCrLf
            If rblMultipleAccounts.SelectedIndex >= 0 Then
                strEmailBody += "Multiple Accounts w/Empire: " & rblMultipleAccounts.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Multiple Accounts w/Empire: " & vbCrLf
            End If
            strEmailBody += vbCrLf
            strEmailBody += "Services to disconnect: " & vbCrLf
            For i = 0 To cblServices.Items.Count - 1
                If cblServices.Items(i).Selected Then
                    strEmailBody += cblServices.Items(i).Text & vbCrLf
                End If
            Next
            strEmailBody += vbCrLf
            strEmailBody += "Disconnect Date: " & txtDisconnectDate.Text & vbCrLf
            strEmailBody += "Final Address: " & txtFinalAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtFinalAddressAN.Text & " Lot Number: " & txtFinalAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtFinalAddressCity.Text & ", " & txtFinalAddressState.Text & " " & txtFinalAddressZip.Text & vbCrLf
            If rblContactMethod.SelectedIndex >= 0 Then
                strEmailBody += "Contact Method: " & rblContactMethod.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Contact Method: " & vbCrLf
            End If
            strEmailBody += "Additional Information: " & txtAdditionalInformation.Text & vbCrLf

            message.Body = strEmailBody
            MailObj.Send(message)

            lblStatus.Text = "<b>** Email Sent ** <br />Thank you for submitting a service disconnect request.</b><br /><br />You will be contacted within two business days of receipt of your request.<br /><br />"
            pnlCustomerInfo.Visible = False
            pnlFinalize.Visible = True

        Catch ex As Exception
            lblStatus.Text = ex.Message
            lbSubmit.Enabled = True
        End Try
    End Sub

End Class
