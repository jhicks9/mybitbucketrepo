<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AccountLookup.aspx.vb" Inherits="CustomerService_Assistance_AccountLookup" title="Account Lookup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        
            <cc1:TabContainer ID="TabMainContainer" runat="server" CssClass="customTabXP" ActiveTabIndex="0" OnClientActiveTabChanged="changeTab">
                <cc1:TabPanel ID="tabSSN" runat="server" HeaderText="Social Security Number">
                    <ContentTemplate>
                        <br />
                        <asp:Label ID="lblFullSSN" runat="server" Width="200px">Social Security Number:</asp:Label><br />
                        <asp:TextBox ID="txtThree" runat="server" Width="45px" MaxLength="3" ValidationGroup="ssninput"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rvfThree" runat="server" ControlToValidate="txtThree"
                            ErrorMessage="Field required" SetFocusOnError="True" ValidationGroup="ssninput">*</asp:RequiredFieldValidator><b>-</b>&nbsp;
                        <asp:TextBox ID="txtTwo" runat="server" Width="25px" ValidationGroup="ssninput" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rvfTwo" runat="server" ControlToValidate="txtTwo"
                            ErrorMessage="Field required" SetFocusOnError="True" ValidationGroup="ssninput">*</asp:RequiredFieldValidator><b>-</b>&nbsp;
                        <asp:TextBox ID="txtFour" runat="server" Width="55px" ValidationGroup="ssninput" MaxLength="4"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rvfFour" runat="server" ControlToValidate="txtFour"
                            ErrorMessage="Field required" SetFocusOnError="True" ValidationGroup="ssninput">*</asp:RequiredFieldValidator>
                        <%--<asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="ssninput" DisplayMode="List" />--%>
                        <br /><asp:Label ID="lblSSNMessage" runat="server" Visible="false"></asp:Label><br />
                        <div style="float:left;margin-left:110px;">
                            <asp:LinkButton ID="lbSSN" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="ssninput" OnClick="lbSSN_Click"><span>Continue</span></asp:LinkButton>
                        </div>
                        <br /><br />
                    </ContentTemplate>
                </cc1:TabPanel><%--end tabSSN--%>
                
                <cc1:TabPanel ID="tabName" runat="server" HeaderText="Account Number">
                    <ContentTemplate>
                        <br />
                        Information entered should match <b>exactly</b> as it appears on the statement. For example:
                        <div id="gallery">
                            <a id="A1" href="~/images/bill_sample_full_page.jpg" runat="server" title="Click to enlarge">
                            <img id="imgSample" src="~/images/bill_sample_thumbnail.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
                        </div>
                        <br /><hr style="color:#113962;height:1px;" /><br />
                        <asp:Label ID="lblAccount" runat="server" Width="125px">Account Number:</asp:Label><br />
                        <asp:TextBox ID="Textbox1" runat="server" Width="70px" MaxLength="6" ValidationGroup="valAccount"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Textbox1"
                            ErrorMessage="Field cannot be empty" SetFocusOnError="True">*</asp:RequiredFieldValidator>-
                        <asp:TextBox ID="Textbox2" runat="server" Width="25px" ValidationGroup="valAccount" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Textbox2"
                            ErrorMessage="Field cannot be empty" SetFocusOnError="True">*</asp:RequiredFieldValidator>-
                        <asp:TextBox ID="TextBox3" runat="server" Width="25px" ValidationGroup="valAccount" MaxLength="1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="Textbox3"
                            ErrorMessage="Field cannot be empty" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="Textbox1"
                            ErrorMessage="Invalid Entry" Type="Integer" Operator="DataTypeCheck" SetFocusOnError="True">&nbsp;</asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="Textbox2"
                            ErrorMessage="Invalid Entry" Type="Integer" Operator="DataTypeCheck" SetFocusOnError="True">&nbsp;</asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="Textbox3"
                            ErrorMessage="Invalid Entry" Type="Integer" Operator="DataTypeCheck" SetFocusOnError="True">&nbsp;</asp:CompareValidator>
                        <br />
                        <asp:Label ID="lblName" Text="Name:" runat="server" Width="125px"></asp:Label><br />
                        <asp:TextBox ID="txtName" runat="server" Width="155px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Field cannot be empty" SetFocusOnError="True">Name required</asp:RequiredFieldValidator>
                        <br />
                        <asp:Label ID="lblSSN" Text="Last 4 digits of SSN or Tax ID: " runat="server"></asp:Label><br />
                        <asp:TextBox ID="txtSSN" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSSN"
                            ErrorMessage="Field cannot be empty" SetFocusOnError="True">SSN Required</asp:RequiredFieldValidator>
                        <br /><asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label><br />
                        <div style="float:left;margin-left:95px;">
                            <asp:LinkButton ID="lbContinue" runat="server" CssClass="ovalbutton" CausesValidation="true" OnClick="lbContinue_Click"><span>Continue</span></asp:LinkButton>
                        </div>
                        <br /><br />
                    </ContentTemplate>
                </cc1:TabPanel><%--end tabName--%>
            </cc1:TabContainer><%--end TabMainContainer--%>        

        </ContentTemplate>
     </asp:UpdatePanel>
     
    <script type="text/javascript">
        jQuery("#gallery a").slimbox(); jQuery("#gallery2 a").slimbox();

        function changeTab() {
            if ($find("<%=TabMainContainer.ClientID%>").get_activeTabIndex() == 0)
                document.getElementById("<%=txtThree.ClientID%>").focus();
            if ($find("<%=TabMainContainer.ClientID%>").get_activeTabIndex() == 1)
                document.getElementById("<%=Textbox1.ClientID%>").focus();
        }
    </script>
</asp:Content>