Imports System.Data
Imports System
Imports System.Web
Imports System.Net
Imports System.io
Imports System.Data.SqlClient

Partial Class CustomerService_Assistance_AccountSummary
    Inherits System.Web.UI.Page
    Dim custToken As String = ""
    Dim billpToken As String = ""
    Dim acctpToken As String = ""
    Dim acctToken As String = ""
    Dim cdToken As String = ""
    Dim servicetype As String = ""
    Dim custName As String = ""
    Dim custAddress As String = ""
    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Public printurl As String = ""
    Dim _totalPaidDeposit As Decimal = 0.0
    Dim _totalUnpaidDeposit As Decimal = 0.0
    Dim _totalRefund As Decimal = 0.0
    Function AddDeposit(ByVal deposit As Object) As String
        Try
            If IsDBNull(deposit) = False Then
                _totalPaidDeposit += deposit
            Else
                deposit = ""
            End If
            Return deposit
        Catch
            Return ""
        End Try
    End Function
    Function GetDeposit() As String
        Return FormatCurrency(_totalPaidDeposit)
    End Function
    Function GetRefund() As String
        Return FormatCurrency(_totalRefund)
    End Function
    Function GetUnpaid() As String
        Return FormatCurrency(_totalUnpaidDeposit)
    End Function
    Function GetTotal() As String
        Return FormatCurrency(_totalPaidDeposit + _totalUnpaidDeposit)
    End Function
    Function AddRefund(ByVal refund As Object) As String
        Try
            If IsDBNull(refund) = False Then
                _totalRefund += refund
            Else
                refund = ""
            End If
            Return refund
        Catch
            Return ""
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            printurl = ResolveUrl("~/print.aspx")
            If Len(Request.QueryString("c")) < 1 Or Len(Request.QueryString("b")) < 1 Then
                Response.Redirect("~/CustomerService/Assistance/AccountLookup.aspx")
            End If
            custToken = tdes.Decrypt(Request.QueryString("c"))
            billpToken = tdes.Decrypt(Request.QueryString("b"))
            custName = Request.QueryString("n")
            cdToken = cd.CalculateCheckDigit(cd.ZeroPad(custToken & billpToken, 8))

            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)

            Dim rtnAcctTkns As New System.Data.DataSet
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, billpToken)
            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            lblAccount.Text = "Select an account to view for " & custToken & "-" & billpToken & "-" & cdToken
            mvSummary.ActiveViewIndex = 0

            hlMenu1.NavigateUrl = "~/CustomerService/Assistance/Default.aspx?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n"))
            hlMenu2.NavigateUrl = "#top"
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        Try
            Dim eCmdArg As String = e.CommandArgument
            Dim arTokens As Array = Split(eCmdArg, ",")

            acctToken = arTokens(0)
            hdnacctToken.Value = arTokens(0)
            servicetype = arTokens(2)
            acctpToken = e.CommandName
            hdnacctpToken.Value = e.CommandName
            custAddress = arTokens(3) & " " & arTokens(6) & " " & arTokens(4) & "," & arTokens(5)
            BuildSummary()
        Catch
        End Try
    End Sub

    Public Sub BuildSummary()
        Try
            Dim build As String = ""
            Dim altrow As Boolean = True

            lblCustTkns.Text = ZeroPad(6, custToken) & "-" & billpToken & "-" & cdToken
            lblCustName.Text = StrConv(custName, VbStrConv.ProperCase)
            lblCoCustomer.Text = cwservice.GetCoCustomer(encrypt, billpToken)
            lblCustAddress.Text = custAddress
            lblDelinquentDate.Text = cwservice.GetDelinquentDate(encrypt, billpToken)
            lblTotalAccount.Text = String.Format("{0:c}", cwservice.GetAccountTotal(encrypt, acctToken, acctpToken))
            build = cwservice.GetArrearsBreakdown(encrypt, acctToken, acctpToken, billpToken)
            If Len(build) > 1 Then
                lblTotalCharges.Text = build.Split(";")(0).Trim
                lblUnstatemented.Text = build.Split(";")(1).Trim
                lblCurrentCharges.Text = build.Split(";")(2).Trim
                lblPastDue30.Text = build.Split(";")(3).Trim
                lbl30Arrears.Text = build.Split(";")(4).Trim
                lbl60Arrears.Text = build.Split(";")(5).Trim
                lbl90Arrears.Text = build.Split(";")(6).Trim
                lbl120Arrears.Text = build.Split(";")(7).Trim
                lblWriteoff.Text = build.Split(";")(8).Trim
            Else
                lblTotalCharges.Text = ""
                lblUnstatemented.Text = ""
                lblCurrentCharges.Text = ""
                lblPastDue30.Text = ""
                lbl30Arrears.Text = ""
                lbl60Arrears.Text = ""
                lbl90Arrears.Text = ""
                lbl120Arrears.Text = ""
                lblWriteoff.Text = ""
            End If

            build = cwservice.GetPaymentDate(encrypt, acctToken, acctpToken)
            If Len(build) > 1 Then
                lblLastPaymentDate.Text = build.Split(";")(0).Trim
                lblLastPaymentAmount.Text = build.Split(";")(1).Trim
            Else
                lblLastPaymentDate.Text = ""
                lblLastPaymentAmount.Text = ""
            End If
            lblAutoPay.Text = cwservice.GetAutoPayStatus(encrypt, billpToken) 'cwservice.GetOnAutopay(encrypt)
            lblAPP.Text = cwservice.GetOnAveragePayment(encrypt, acctToken, acctpToken)

            build = cwservice.GetAPData(encrypt, acctToken, acctpToken)
            If Len(build) > 1 Then
                lblServiceDate.Text = build.Split(";")(0).Trim
                lblCutDate.Text = build.Split(";")(1).Trim
                lblDNFollupDate.Text = build.Split(";")(2).Trim
                lblDNBalance.Text = build.Split(";")(3).Trim
                lblStatus.Text = build.Split(";")(4).Trim
            Else
                lblServiceDate.Text = ""
                lblCutDate.Text = ""
                lblDNFollupDate.Text = ""
                lblDNBalance.Text = ""
                lblStatus.Text = ""
            End If

            If lblStatus.Text = "Disconnect Non-Pay" Then
                lblStatus.Font.Bold = True
                lblStatus.ForeColor = Drawing.Color.Red
                btnNewPledge.Visible = False
            End If
            build = cwservice.GetInProgressDisconnect(encrypt, acctToken, acctpToken).ToString
            If build > 0 Then  'Pledge information not available if disconnect is in progress
                lblStatus.Font.Bold = True
                lblStatus.ForeColor = Drawing.Color.Red
                lblStatus.Text = "In progress disconnect non pay service order"
                btnNewPledge.Visible = False
                lblPledgeAvailable.ForeColor = Drawing.Color.Red
                lblPledgeAvailable.Visible = True
            Else
                BuildPledgeData()
            End If

            lblCheckAllowed.Text = cwservice.GetCheckAllowed(encrypt)
            lblBadChecks.Text = cwservice.GetReturnedCheck(encrypt)

            lblDNPDate.Text = cwservice.GetDNPDate(encrypt, acctToken, acctpToken)

            build = cwservice.GetCreditHistory(encrypt, acctToken, acctpToken)
            If Len(build) > 1 Then
                If build.Split(";")(0).Trim <> "0" Then
                    If altrow Then  'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Paid On Time<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(0).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Paid On Time<br />"
                        lblHistory.Text += build.Split(";")(0).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(1).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Not Paid On Time<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(1).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Not Paid On Time<br />"
                        lblHistory.Text += build.Split(";")(1).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(2).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Unpaid By Penalty<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(2).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Unpaid By Penalty<br />"
                        lblHistory.Text += build.Split(";")(2).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(3).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Eligible For Disconnect<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(3).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Eligible For Disconnect<br />"
                        lblHistory.Text += build.Split(";")(3).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(4).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Shut-Off Notice<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(4).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Shut-Off Notice<br />"
                        lblHistory.Text += build.Split(";")(4).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(5).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Nonpay Disc Service Order<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(5).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Nonpay Disc Service Order<br />"
                        lblHistory.Text += build.Split(";")(5).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(6).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Disconnect For Non-Pay<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(6).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Disconnect For Non-Pay<br />"
                        lblHistory.Text += build.Split(";")(6).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(7).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Complete NonPayDisconnect<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(7).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Complete NonPayDisconnect<br />"
                        lblHistory.Text += build.Split(";")(7).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(8).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Paid To Avoid Disconnect<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(8).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Paid To Avoid Disconnect<br />"
                        lblHistory.Text += build.Split(";")(8).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(9).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Collection Notice<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(9).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Collection Notice<br />"
                        lblHistory.Text += build.Split(";")(9).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(10).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Eligible For Write-Off<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(10).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Eligible For Write-Off<br />"
                        lblHistory.Text += build.Split(";")(10).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(11).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Write-Off<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(11).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Write-Off<br />"
                        lblHistory.Text += build.Split(";")(11).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(12).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Write-Off Reinstated<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(12).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Write-Off Reinstated<br />"
                        lblHistory.Text += build.Split(";")(12).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(13).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Assign Agency<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(13).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Assign Agency<br />"
                        lblHistory.Text += build.Split(";")(13).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(14).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Bankruptcy<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(14).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Bankruptcy<br />"
                        lblHistory.Text += build.Split(";")(14).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(15).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Bankruptcy Dismissal<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(15).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Bankruptcy Dismissal<br />"
                        lblHistory.Text += build.Split(";")(15).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
            End If

            _totalUnpaidDeposit = CDec(cwservice.GetUnpaidDepositInfo(encrypt, acctToken, acctpToken))
            Dim rtnDataset As DataSet = cwservice.GetDepositInfo(encrypt, acctToken, acctpToken)
            gvDepositInfo.DataSource = rtnDataset
            gvDepositInfo.DataBind()
            gvDepositInfo.Visible = True

            Dim ds As DataSet = cwservice.GetPaymentContractInfo(encrypt, acctToken, acctpToken)
            gvContractInfo.DataSource = ds
            gvContractInfo.DataBind()
            gvContractInfo.Visible = True

            mvSummary.ActiveViewIndex = 1
        Catch
        End Try
    End Sub
    Public Sub BuildPledgeData()
        Try
            'Dim dsPledges As DataSet = cwservice.GetPledgeInfo(tdes.Encrypt("364250"), "9", "154")
            Dim dsPledges As DataSet = cwservice.GetPledgeInfo(encrypt, acctToken, acctpToken)
            ' add status column to CW data
            Dim dc As DataColumn
            dc = New DataColumn("status", System.Type.GetType("System.String"))
            dsPledges.Tables(0).Columns.Add(dc)
            Dim dr As DataRow
            Dim increment As Integer = 0
            For Each dr In dsPledges.Tables(0).Rows
                dsPledges.Tables(0).Rows(increment).Item("status") = "Acknowledged"
                increment = increment + 1
            Next 'end add status
            ' combine current pledge table data to CW dataset
            Dim newRow As DataRow
            Dim query As String = "select id,CONVERT(VARCHAR(10),date,101) AS date,amount,logtkn from pledge where customertkn = '" & custToken & "' and customeraccttkn = '" & acctToken & "' and accountpkgtkn = '" & acctpToken & "' order by date"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd As New SqlCommand(query, conn)
            Dim dr0 As SqlDataReader
            dr0 = cmd.ExecuteReader
            While dr0.Read
                newRow = dsPledges.Tables(0).NewRow()
                If Len(dr0(3).ToString) > 0 Then
                    newRow.Item("status") = "Pending Modification"
                Else
                    newRow.Item("status") = "Pending"
                End If
                newRow.Item("id") = dr0(0).ToString
                newRow.Item("ts") = dr0(1).ToString
                newRow.Item("log_text") = dr0(2).ToString
                dsPledges.Tables(0).Rows.Add(newRow)
            End While
            dr0.Close()
            conn.Close()

            'create a view to change the sort order on the data
            Dim dv As DataView = dsPledges.Tables(0).DefaultView
            dv.Sort = "ts DESC"
            gvLogs.DataSource = dv
            gvLogs.DataBind()
        Catch
        End Try
    End Sub
    Protected Sub btnNewPledge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewPledge.Click
        mvSummary.ActiveViewIndex = 1
        lblAction.Text = "Add New Pledge"
        lblId.Text = ""
        txtAmount.Text = ""
        cboType.SelectedValue = ""
        cboSeason.SelectedValue = ""
        pnlPledge.Visible = True
        pnlData.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        lblValidate.Font.Bold = False
        lblValidate.ForeColor = Drawing.Color.Black
        lblValidate.Text = "Please allow up to 24 hours to acknowledge pledge information."
        mvSummary.ActiveViewIndex = 1
        pnlPledge.Visible = False
        pnlData.Visible = True
    End Sub

    Protected Sub gvLogs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLogs.RowCommand
        Dim rowfound As Boolean = True
        Try
            Select Case e.CommandName
                Case "Select"
                    lblAction.Text = "Edit Pending Pledge"
                    lblId.Text = e.CommandArgument
                    Dim query As String = "select id,amount,type,season from pledge where (id = " & lblId.Text & " or logtkn = '" & lblId.Text & "')"
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    conn.Open()
                    Dim cmd As New SqlCommand(query, conn)
                    Dim dr0 As SqlDataReader
                    dr0 = cmd.ExecuteReader
                    If dr0.HasRows Then  'read the pledge data row for updating
                        While dr0.Read
                            txtAmount.Text = dr0(1).ToString
                            cboType.SelectedValue = dr0(2).ToString
                            cboSeason.SelectedValue = dr0(3).ToString
                        End While
                    Else  'create a new row to edit existing CW log
                        lblAction.Text = "Edit Acknowledged Pledge"
                        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                        Dim build As String = CType(row.Cells(4), DataControlFieldCell).Text
                        txtAmount.Text = build.Split(" ")(0).Trim
                        cboType.SelectedValue = ""
                        cboSeason.SelectedValue = ""
                    End If
                    dr0.Close()
                    conn.Close()

                    mvSummary.ActiveViewIndex = 1
                    pnlPledge.Visible = True
                    pnlData.Visible = False
                Case Else
                    UpdatePanel1.Update()
            End Select
        Catch
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim validate As Boolean = True
        Try
            If cboType.SelectedValue = "" Or cboSeason.SelectedValue = "" Or txtAmount.Text = "" Then
                lblValidate.Font.Bold = True
                lblValidate.ForeColor = Drawing.Color.Red
                lblValidate.Text = "* You must complete the pledge form."
                validate = False
                mvSummary.ActiveViewIndex = 1
            Else
                If Left(Right(txtAmount.Text, 3), 1) <> "." Then  'add ".00" (cents) if not present
                    txtAmount.Text += ".00"
                End If
                If Not isCurrency(txtAmount.Text) Then
                    lblValidate.Font.Bold = True
                    lblValidate.ForeColor = Drawing.Color.Red
                    lblValidate.Text = "* The amount must be in valid currency format."
                    validate = False
                    mvSummary.ActiveViewIndex = 1
                End If
            End If

            If validate Then
                If Left(lblAction.Text, 12) = "Edit Pending" Then
                    Dim query As String = "update pledge set date = '" & Now & "',amount = '" & txtAmount.Text & "', " & _
                    "customertkn = '" & custToken & "', customeraccttkn = '" & hdnacctToken.Value & "',accountpkgtkn = '" & hdnacctpToken.Value & "', " & _
                    "name = '" & lblCustName.Text & "',address = '" & lblCustAddress.Text & "', agency = '" & User.Identity.Name & "', " & _
                    "agent = '" & Membership.GetUser(User.Identity.Name).Email & "', type = '" & cboType.SelectedValue & "', season = '" & cboSeason.SelectedValue & "' where id = " & lblId.Text
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Dim cmd As New SqlCommand()
                    conn.Open()
                    cmd.Connection = conn
                    cmd.CommandText = query
                    cmd.ExecuteNonQuery()
                    conn.Close()
                End If

                If Left(lblAction.Text, 3) = "Add" Then
                    Dim query As String = "INSERT into pledge(date,amount,customertkn,customeraccttkn,accountpkgtkn,name,address,agency,agent,type,season) " & _
                        "VALUES('" & Now & "','" & txtAmount.Text & "','" & custToken & "','" & hdnacctToken.Value & "','" & hdnacctpToken.Value & "','" & Replace(lblCustName.Text, "'", "''") & "','" & Replace(lblCustAddress.Text, "'", "''") & "','" & User.Identity.Name & "','" & Membership.GetUser(User.Identity.Name).Email & "','" & cboType.SelectedValue & "','" & cboSeason.SelectedValue & "')"
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Dim cmd As New SqlCommand()
                    conn.Open()
                    cmd.Connection = conn
                    cmd.CommandText = query
                    cmd.ExecuteNonQuery()
                    conn.Close()
                End If

                If Left(lblAction.Text, 17) = "Edit Acknowledged" Then  'a new row must be added to "edit" an acknowledged CW log
                    Dim query As String = "INSERT into pledge(date,amount,customertkn,customeraccttkn,accountpkgtkn,name,address,agency,agent,type,season,logtkn) " & _
                        "VALUES('" & Now & "','" & txtAmount.Text & "','" & custToken & "','" & hdnacctToken.Value & "','" & hdnacctpToken.Value & "','" & lblCustName.Text & "','" & lblCustAddress.Text & "','" & User.Identity.Name & "','" & Membership.GetUser(User.Identity.Name).Email & "','" & cboType.SelectedValue & "','" & cboSeason.SelectedValue & "','" & lblId.Text & "')"
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Dim cmd As New SqlCommand()
                    conn.Open()
                    cmd.Connection = conn
                    cmd.CommandText = query
                    cmd.ExecuteNonQuery()
                    conn.Close()
                End If

                acctpToken = hdnacctpToken.Value  'restore variable
                acctToken = hdnacctToken.Value  'restore variable
                BuildPledgeData()  'rebuild and rebind datagrid to show updates
                BuildEmail()
                lblValidate.Font.Bold = False
                lblValidate.ForeColor = Drawing.Color.Black
                lblValidate.Text = "Please allow up to 24 hours to acknowledge pledge information."
                mvSummary.ActiveViewIndex = 1
                pnlPledge.Visible = False
                pnlData.Visible = True
            End If
        Catch
        End Try
    End Sub
    Public Sub BuildEmail()
        Try
            Dim buildstr As String = ""
            Dim MailObj As New System.Net.Mail.SmtpClient
            Dim message As New System.Net.Mail.MailMessage()
            Dim fromAddress As New System.Net.Mail.MailAddress("AlertForm@empiredistrict.com")

            message.From = fromAddress
            message.To.Add("websitepledges@empiredistrict.com")
            message.Subject = "website pledge information"
            'MailObj.Host = "" -- obtained from web.config 03/14/2012
            message.IsBodyHtml = False

            message.Body = ""
            If Left(lblAction.Text, 4) = "Edit" Then
                buildstr = "Action: "
                message.Body += buildstr.PadRight(21) & "Pledge Modification" & vbCrLf
            End If
            If Left(lblAction.Text, 3) = "Add" Then
                buildstr = "Action: "
                message.Body += buildstr.PadRight(21) & "New Pledge" & vbCrLf
            End If
            buildstr = "Date: "
            message.Body += buildstr.PadRight(21) & Now.ToString & vbCrLf
            buildstr = "Amount of pledge: "
            message.Body += buildstr.PadRight(21) & txtAmount.Text & vbCrLf
            buildstr = "Customer account: "
            message.Body += buildstr.PadRight(21) & lblCustTkns.Text & vbCrLf
            buildstr = "Customer name: "
            message.Body += buildstr.PadRight(21) & lblCustName.Text & vbCrLf
            buildstr = "Customer address: "
            message.Body += buildstr.PadRight(21) & lblCustAddress.Text & vbCrLf
            buildstr = "Agency name: "
            message.Body += buildstr.PadRight(21) & User.Identity.Name & vbCrLf
            buildstr = "Agent: "
            message.Body += buildstr.PadRight(21) & Membership.GetUser(User.Identity.Name).Email & vbCrLf
            buildstr = "Type of pledge: "
            message.Body += buildstr.PadRight(21) & cboType.SelectedValue & vbCrLf
            buildstr = "Season: "
            message.Body += buildstr.PadRight(21) & cboSeason.SelectedValue & vbCrLf
            MailObj.Send(message)
        Catch
        End Try
    End Sub
    Public Shared Function isCurrency(ByVal textToCheck As String) As Boolean
        If Left(textToCheck, 1) <> "$" Then
            textToCheck = "$" & textToCheck
        End If
        Return Regex.IsMatch(textToCheck, "^\$(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$")
    End Function
    Public Function ZeroPad(ByVal len As Int32, ByVal text As String) As String
        While (text.Length < len)
            text = "0" & text
        End While
        Return text
    End Function
End Class
