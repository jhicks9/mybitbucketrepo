<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="CustomerService_MyAccount_Assistance_default" title="Assistance Agency" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                Welcome&nbsp;
                <asp:Label id="lblUserName" runat="server" Font-Bold="true" Text="" />&nbsp;
                to the Assistance Agency section of Empire District's website.
            </td>
            <td><asp:Image ID="imgLanding" Imageurl="~/images/landing/landing-assistance.jpg" runat="server" /></td>
        </tr>
    </table>
    <div class="landingtext">
        <br />
        <asp:Label ID="txtList" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>

