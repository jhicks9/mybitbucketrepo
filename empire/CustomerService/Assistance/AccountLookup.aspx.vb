Imports System.Net
Imports System.Data
Partial Class CustomerService_Assistance_AccountLookup
    Inherits System.Web.UI.Page
    Dim cwservice As New cw.Service
    Dim cd As New checkDigit
    Dim encrypt_custToken As String
    Dim encrypt_billpToken As String
    Dim tdes As New tripleDES

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim autotab As HtmlGenericControl = New HtmlGenericControl
        autotab.Attributes.Add("type", "text/javascript")
        autotab.TagName = "script"
        autotab.Attributes.Add("src", ResolveUrl("~/js/autotab.js"))
        Page.Header.Controls.Add(autotab)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub lbContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt_custToken = tdes.Encrypt(Textbox1.Text)
            Dim strBillPkg As String = Trim(Textbox2.Text)
            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            Dim status As String = cwservice.ValidateCustomer(encrypt_custToken, strBillPkg, txtName.Text, txtSSN.Text)

            If status = 1 Then
                encrypt_billpToken = tdes.Encrypt(strBillPkg)
                Response.Redirect("~/CustomerService/Assistance/AccountSummary.aspx?c=" & Server.UrlEncode(encrypt_custToken) & "&b=" & Server.UrlEncode(encrypt_billpToken) & "&n=" & Server.UrlDecode(txtName.Text))
            ElseIf status = 0 Then
                lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Text = "Cannot find user. Please verify account information."
                lblMessage.Visible = True
            ElseIf status = 2 Then
                lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Text = "Unable to validate account. Please contact Customer Service at 800-206-2300."
                lblMessage.Visible = True
            End If

        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbSSN_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ssn As String = tdes.Encrypt(txtThree.Text & txtTwo.Text & txtFour.Text)
            cwservice.Credentials = CredentialCache.DefaultCredentials
            Dim ds As DataSet = cwservice.GetCustomerToken(ssn)

            Dim rowCount As Integer = ds.Tables("CustomerTokens").Rows.Count
            If rowCount <> 1 Then
                lblSSNMessage.ForeColor = Drawing.Color.Red
                lblSSNMessage.Text = "Cannot verify account information."
                lblSSNMessage.Visible = True
            Else
                Dim custToken As String = ds.Tables("CustomerTokens").Rows(0).Item(0).ToString
                Dim billpToken As String = cd.ZeroPad(ds.Tables("CustomerTokens").Rows(0).Item(1).ToString, 2)  'always zeropad billing package token to avoid error calculating check digit
                Dim custName As String = ""
                If Len(ds.Tables("CustomerTokens").Rows(0).Item(2).ToString) > 0 Then  'customer title
                    custName += ds.Tables("CustomerTokens").Rows(0).Item(2).ToString & " "
                End If
                If Len(ds.Tables("CustomerTokens").Rows(0).Item(3).ToString) > 0 Then  'customer first name
                    custName += ds.Tables("CustomerTokens").Rows(0).Item(3).ToString & " "
                End If
                If Len(ds.Tables("CustomerTokens").Rows(0).Item(4).ToString) > 0 Then  'customer last name
                    custName += ds.Tables("CustomerTokens").Rows(0).Item(4).ToString & " "
                End If
                If Len(ds.Tables("CustomerTokens").Rows(0).Item(5).ToString()) > 0 Then  'customer suffix
                    custName += ds.Tables("CustomerTokens").Rows(0).Item(5).ToString()
                End If
                encrypt_custToken = tdes.Encrypt(custToken)
                encrypt_billpToken = tdes.Encrypt(billpToken)
                Response.Redirect("~/CustomerService/Assistance/AccountSummary.aspx?c=" & Server.UrlEncode(encrypt_custToken) & "&b=" & Server.UrlEncode(encrypt_billpToken) & "&n=" & Server.UrlDecode(custName))
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            ClientScript.RegisterStartupScript(Me.GetType(), "focus", String.Format("function pageLoad() {{document.getElementById('{0}').focus();}}", txtThree.ClientID), True)
        End If
        txtThree.Attributes("onkeypress") = "return recPreKey(event, this)"
        txtThree.Attributes("onkeyup") = "onFull(event, this)"
        txtTwo.Attributes("onkeypress") = "return recPreKey(event, this)"
        txtTwo.Attributes("onkeyup") = "onFull(event, this)"
        txtFour.Attributes("onkeypress") = "return recPreKey(event, this)"
        txtFour.Attributes("onkeyup") = "onFull(event, this," & lbSSN.ClientID & ")"
        Textbox1.Attributes("onkeypress") = "return recPreKey(event, this)"
        Textbox1.Attributes("onkeyup") = "onFull(event, this)"
        Textbox2.Attributes("onkeypress") = "return recPreKey(event, this)"
        Textbox2.Attributes("onkeyup") = "onFull(event, this)"
        TextBox3.Attributes("onkeypress") = "return recPreKey(event, this)"
        TextBox3.Attributes("onkeyup") = "onFull(event, this)"
        txtSSN.Attributes("onkeypress") = "return recPreKey(event, this)"
        txtSSN.Attributes("onkeyup") = "onFull(event, this," & lbContinue.ClientID & ")"
        'txtThree.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbSSN.UniqueID + "','')")
        'txtTwo.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbSSN.UniqueID + "','')")
        'txtFour.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbSSN.UniqueID + "','')")

        'Textbox1.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
        'Textbox2.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
        'TextBox3.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
        'txtName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
        'txtSSN.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
        txtThree.Focus()
    End Sub
End Class
