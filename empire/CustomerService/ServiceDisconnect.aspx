﻿<%@ Page Title="Service Disconnect Form" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ServiceDisconnect.aspx.vb" Inherits="CustomerService_ServiceDisconnect" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style='float:right;width:350px;margin:0px 0 0 5px;'>
                <img id='imgServiceConnect' src='~/images/customerservice_serviceconnect.jpg' runat='server' alt='' />
            </div>
            <div>
                <b>Thank you for using Empire District's online application.</b><br />
                <br />
                We will respond to your request by telephone or email within two business days of receipt. An account representative may need to verify the service address, schedule a date for a service person to meet you at the property, and discuss any other account requirements.<br />
                <br />
                Twenty-four hour notice, excluding weekends and holidays, is required for service requests.
            </div>
            <asp:Panel ID="pnlFinalize" runat="server" Visible="false">
                <div style="padding:25px 0 0 0">
                    <asp:label ID="lblStatus" Text="" runat="server" Font-Bold="false" />
                </div>
            </asp:Panel>
            <div style="clear:both;"></div>

            <asp:Panel ID="pnlVerification" runat="server" Visible="true">
                <font style="color:#113962"><b>Account Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0" width="100%" >
                        <tr>    
                            <td align="right">
                                <div id="gallery1">
                                    <a href="~/images/bill_sample_full_page.jpg" runat="server" title="Click to enlarge"><img id="imgSample" src="~/images/bill_sample_thumbnail.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border-style:none" /></a>
                                </div>
                            </td>
                            <td valign="top" style="padding:0 5px 0 5px;">
                                <div id="gallery2">
                                You will need to know your account number, your name, and the last four digits of your Social Security Number to proceed.
                                Your name and Empire District account number should match <b>exactly</b> as it appears on your statement.
                                For an example click <a href="~/Images/bill_sample_full_page.jpg" runat="server" target="_blank">here</a>.
                                </div>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr>
                            <td align="right"><asp:label ID="lblAccountNumber" Text="* Account Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtAccountCT" runat="server" Width="50px" ValidationGroup="avg" MaxLength="6" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revAccountCT" runat="server" ControlToValidate="txtAccountCT" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtAccountBP" runat="server" Width="20px" ValidationGroup="avg" MaxLength="2" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revAccountBP" runat="server" ControlToValidate="txtAccountBP" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtAccountCD" runat="server" Width="20px" ValidationGroup="avg" MaxLength="1" />
                                <asp:RegularExpressionValidator ID="revAccountCD" runat="server" ControlToValidate="txtAccountCD" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvAccountCT" runat="server" ControlToValidate="txtAccountCT" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvAccountBP" runat="server" ControlToValidate="txtAccountBP" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvAccountCD" runat="server" ControlToValidate="txtAccountCD" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblAccountName" Text="* Name:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtAccountName" runat="server" Width="300px" ValidationGroup="avg" /><span>(as appears on statement)</span>
                                <asp:RequiredFieldValidator ID="rfvAccountName" runat="server" ControlToValidate="txtAccountName" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><asp:label ID="lblSSN" Text="* Social Security Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtSSN1" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" /><span>(last four digits)</span>
                                <asp:RegularExpressionValidator ID="revSSN1" runat="server" ControlToValidate="txtSSN1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvSSN1" runat="server" ControlToValidate="txtSSN1" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td><td><asp:label ID="lblVerificationStatus" Text="" runat="server" Font-Bold="true" /></td></tr>
                        <tr>
                            <td></td>
                            <td><asp:LinkButton ID="lbVerifyAccount" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg"><span>Verify Account</span></asp:LinkButton></td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>

            <asp:Panel ID="pnlCustomerInfo" runat="server" Visible="false">
                <font style="color:#113962"><b>Customer Information</b></font>
                <div style="border: solid 5px #b0c4de;padding:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr valign="top">
                            <td align="right" style="width:220px"><asp:label ID="lblServiceAddress" Text="* Service Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvServiceAddress" runat="server" ControlToValidate="txtServiceAddress" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblServiceAddressAN" Text="Apartment Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceAddressAN" runat="server" ValidationGroup="avg" Width="115px" ></asp:TextBox>
                                <asp:label ID="lblServiceAddressLN" Text="Lot Number: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtServiceAddressLN" runat="server" ValidationGroup="avg" Width="95px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblServiceAddressCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtServiceAddressCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvfServiceAddressCity" runat="server" ControlToValidate="txtServiceAddressCity" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblServiceAddressState" Text="State: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtServiceAddressState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvServiceAddressState" runat="server" ControlToValidate="txtServiceAddressState" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblServiceAddressZip" Text="Zip: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtServiceAddressZip" runat="server" ValidationGroup="avg" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvServiceAddressZip" runat="server" ControlToValidate="txtServiceAddressZip" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblPrimaryPhone" Text="* Primary Phone:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone1" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone2" runat="server" Width="25px" ValidationGroup="avg" MaxLength="3" /><b>-</b>
                                <asp:RegularExpressionValidator ID="revPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:TextBox ID="txtPrimaryPhone3" runat="server" Width="30px" ValidationGroup="avg" MaxLength="4" />
                                <asp:RegularExpressionValidator ID="revPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ValidationGroup="avg" Display="Dynamic" ErrorMessage="invalid format" ValidationExpression="(^\d+$)" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone1" runat="server" ControlToValidate="txtPrimaryPhone1" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone2" runat="server" ControlToValidate="txtPrimaryPhone2" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RequiredFieldValidator ID="rfvPrimaryPhone3" runat="server" ControlToValidate="txtPrimaryPhone3" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblEmail" Text="* Email address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="avg" Display="Dynamic"
                                    ErrorMessage="Invalid email format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"  style="padding:5px 0 5px 0"><asp:label ID="lblMultipleAccounts" Text="* Do you have multiple accounts<br />with Empire?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="rblMultipleAccounts" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvMultipleAccounts" runat="server" ControlToValidate="rblMultipleAccounts" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblServices" Text="* What services are you requesting<br />to have discontinued?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:CheckBoxList ID="cblServices" runat="server" Width="300px"  RepeatDirection="Horizontal" RepeatColumns="3" >
                                    <asp:ListItem>Electric</asp:ListItem>
                                    <asp:ListItem>Water</asp:ListItem>
                                    <asp:ListItem>Natural Gas</asp:ListItem>
                                    <asp:ListItem>Lighting</asp:ListItem>
                                    <asp:ListItem>All Services</asp:ListItem>
                                </asp:CheckBoxList>
                                <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList" ErrorMessage="required" Display="Dynamic" ValidationGroup="avg" ></asp:CustomValidator> 
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblDisconnectDate" Text="* Service Disconnect Date:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtDisconnectDate" runat="server" ValidationGroup="avg" Width="110px"></asp:TextBox>
                                <asp:ImageButton ID="imgDisconnectDate" ImageUrl="~/images/icon/icon_calendar.png" runat="server" /> (mm/dd/yyyy)
                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtDisconnectDate" Format="MM/dd/yyyy" PopupButtonID="imgDisconnectDate" PopupPosition="TopLeft"></cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfvDisconnectDate" runat="server" ControlToValidate="txtDisconnectDate" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revDisconnectDate" runat="server" ControlToValidate="txtDisconnectDate"
                                        Display="Dynamic" ErrorMessage="Date format MM/DD/YYYY" ValidationExpression="^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="avg">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top"><td colspan="2"><b>Where can we send your final bill?</b></td></tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblFinalAddress" Text="* Address:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtFinalAddress" runat="server" ValidationGroup="avg" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFinalAddress" runat="server" ControlToValidate="txtFinalAddress" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblFinalAddressAN" Text="Apartment Number:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtFinalAddressAN" runat="server" ValidationGroup="avg" Width="115px" ></asp:TextBox>
                                <asp:label ID="lblFinalAddressLN" Text="Lot Number: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtFinalAddressLN" runat="server" ValidationGroup="avg" Width="95px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td align="right"><asp:label ID="lblFinalAddressCity" Text="* City:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <asp:TextBox ID="txtFinalAddressCity" runat="server" ValidationGroup="avg" Width="115px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvfFinalAddressCity" runat="server" ControlToValidate="txtFinalAddressCity" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblFinalAddressState" Text="State: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtFinalAddressState" runat="server" ValidationGroup="avg" Width="25px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvfFinalAddressState" runat="server" ControlToValidate="txtFinalAddressState" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                                <asp:label ID="lblFinalAddressZip" Text="Zip: " runat="server" Font-Bold="true" />
                                <asp:TextBox ID="txtFinalAddressZip" runat="server" ValidationGroup="avg" Width="75px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFinalAddressZip" runat="server" ControlToValidate="txtFinalAddressZip" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td style="text-align:right"><a href="http://webapps.usps.com/zip4/" target="_blank">locate zip code</a></td></tr>
                        <tr valign="top">
                            <td align="right" style="padding:5px 0 5px 0"><asp:label ID="lblContactMethod" Text="* How may we contact you?&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="rblContactMethod" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="Phone">Phone</asp:ListItem>
                                                <asp:ListItem Value="Email">Email</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvContactMethod" runat="server" ControlToValidate="rblContactMethod" ErrorMessage="required" ValidationGroup="avg" Display="Dynamic" />        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top">
                            <td align="right" valign="top"><asp:label ID="lblAdditionalInformation" Text="Additional Information:&nbsp;" runat="server" Font-Bold="true" /></td>
                            <td><asp:TextBox ID="txtAdditionalInformation" runat="server" ValidationGroup="avg" Width="300px" MaxLength="500" TextMode="MultiLine" Height="75px"></asp:TextBox></td>
                        </tr>
                        <tr valign="top"><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr valign="top">
                            <td></td>
                            <td><asp:LinkButton ID="lbSubmit" runat="server" CssClass="ovalbutton" CausesValidation="true" ValidationGroup="avg"><span>Submit Request</span></asp:LinkButton></td>
                        </tr>
                    </table>
                </div>
                * (indicates required fields)
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        jQuery("#gallery1 a, #gallery2 a").slimbox();

        function autotab(original, destination) {
            if (original.getAttribute && original.value.length == original.getAttribute("maxlength"))
                destination.focus();
        }

        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= cblServices.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        } 
 </script>
</asp:Content>

