
Partial Class CustomerService_AssistanceAgencies
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If String.IsNullOrEmpty(Request.QueryString("aagency")) Then
                'If no variable is pass default to MO
                gvMOElectric.DataBind()
                gvMOGas.DataBind()
                pnlMO.Visible = True
                pnlAR.Visible = False
                pnlKS.Visible = False
                pnlOK.Visible = False
                lbMO.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "aagency=AR" Then
                gvAR.DataBind()
                pnlAR.Visible = True
                pnlMO.Visible = False
                pnlKS.Visible = False
                pnlOK.Visible = False
                lbAR.Font.Bold = True
                lbMO.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "aagency=KS" Then
                gvKS.DataBind()
                pnlKS.Visible = True
                pnlMO.Visible = False
                pnlAR.Visible = False
                pnlOK.Visible = False
                lbKS.Font.Bold = True
                lbMO.Font.Bold = False
                lbAR.Font.Bold = False
                lbOK.Font.Bold = False
            ElseIf Request.QueryString.ToString = "aagency=OK" Then
                gvOK.DataBind()
                pnlOK.Visible = True
                pnlMO.Visible = False
                pnlKS.Visible = False
                pnlAR.Visible = False
                lbOK.Font.Bold = True
                lbMO.Font.Bold = False
                lbKS.Font.Bold = False
                lbAR.Font.Bold = False
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class
