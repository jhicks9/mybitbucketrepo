<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Deposit.aspx.vb" Inherits="CustomerService_Deposit" title="Deposits and Fees" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
    The Public Service Commission for each state establishes the deposit and fees policy for that state. They also regulate the fee amounts and deposit interest rate. The following is a description by state of these policies.
    <br /><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lbAR" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Deposit.aspx?deposit=AR" Text="Arkansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbKS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Deposit.aspx?deposit=KS" Text="Kansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbMO" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Deposit.aspx?" Text="Missouri"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbOK" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/Deposit.aspx?deposit=OK" Text="Oklahoma"/>
            <br />&nbsp

                <asp:Panel ID="pnlAR" runat="server" HeaderText="Arkansas" Visible="false">
                    <ContentTemplate>
                        <div class="sectionheader">Residential Deposit</div><br />
                        A deposit may be waived for a new Customer if:
                        <br />
                        <ul>
                            <li>The Customer can supply the Company with a good letter of credit</li>
                            <li>A qualified Guarantor signs for the Customer</li>
                        </ul>
                        If a deposit is required, it will be equal to 2 times the average monthly charge based on the class of service.  Payments for deposits can be made in 2 installments.
                        <br /><br /><hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Commercial Deposit</div><br />
                        <ul>
                            <li>A deposit will be equal to 2 times the average monthly charge for the class of service.</li>
                        </ul>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Miscellaneous Charges</div>
                        <br />
                        <asp:DataList ID="dlARFees" runat="server" SkinID="DataList" DataSourceID="dslDSAR">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="dslDSAR" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 83 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                        <br />
                        <hr style="color:#113962;height:1px;clear:both;margin-top:10px;"/>
                        <div class="sectionheader">Due Date</div><br />
                        All accounts have a 14-day due date.
                        <br /><br />
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Deposit Interest</div><br />
                        <asp:DataList ID="dlARDI" runat="server" DataSourceID="sqlDSARDI">
                        <itemtemplate>
                            <asp:Label ID="lblDI" runat="server" Text='<%#Eval("description")%>' />
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSARDI" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="select top 1 description from messages where type = 'AR Interest'">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlAR--%>
                
                <asp:Panel ID="pnlKS" runat="server" HeaderText="Kansas" Visible="false">
                    <ContentTemplate>
                        <div class="sectionheader">Residential Deposit</div>
                        <br />
                        A deposit may be waived for a new Customer if:
                        <ul>
                            <li>The Customer can supply the Company with a good letter of credit</li>
                            <li>A qualified Guarantor signs for the Customer </li>
                        </ul>
                        If a deposit is required, it will be equal to 2 times the average monthly charge for the specific premise.  Payments for deposit can be made in 4 installments.
                        <br /><br />
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Commercial Deposit</div><br />
                        <ul>
                            <li>A deposit for a Small Commercial will be equal to 2 times the average monthly charge for the specific premise and can be made in 4 installments </li>
                            <li>A deposit for a Large Commercial will be equal to 2 times the highest monthly charge for the specific premise and must be paid at the time service is requested </li>
                        </ul>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Miscellaneous Charges</div><br />
                        <asp:DataList ID="dlKSFees" runat="server" SkinID="DataList" DataSourceID="sqlDSKS">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSKS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 84 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                        <br />
                        <hr style="color:#113962;height:1px;clear:both;margin-top:10px;"/>
                        <div class="sectionheader">Penalties</div><br />
                        <ul>
                            <li>Residential accounts have a 25-day due date and are assessed a 2% penalty if not paid by the due date </li>
                            <li>Commercial accounts have a 15-day due date and are assessed a 2% penalty if not paid by the due date </li>
                        </ul>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Deposit Interest</div><br />
                        <asp:DataList ID="dlKSDI" runat="server" DataSourceID="sqlDSKSDI">
                        <itemtemplate>
                            <asp:Label ID="lblDI" runat="server" Text='<%#Eval("description")%>' />
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSKSDI" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="select top 1 description from messages where type = 'KS Interest'">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlKS--%>
                
                <asp:Panel ID="pnlMO" runat="server" HeaderText="Missouri" Visible="true">
                    <ContentTemplate>
                        <div class="sectionheader">Residential Deposit</div>
                        <br />
                        A deposit may be waived for a new Customer if:

                        <ul>
                            <li>The Customer can supply the Company with a Good letter of credit (no more than 5 late pays in last 12 months)</li>
                            <li>A qualified Guarantor signs for the Customer </li>
                            <li>The Customer has been employed full time for one year and has an adequate regular source of income </li>
                            <li>The Customer owns or is purchasing a home </li>
                        </ul>
                        If a deposit is required, it will be equal to 2 times average monthly charge for the specific premise unless:
                        <br />
                        
                        <ul>
                            <li>The Customer has outstanding debt with Company </li>
                            <li>The Customer had poor previous payment history with Company </li>
                            <li>If the Customer has tampered with service, with our Company or any other utility company in the last 5 years </li>
                        </ul>
                        
                        If any of these apply, the Company may require 2 times highest monthly charge and collect the outstanding bill.
                        <br />
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Commercial Deposit</div>

                        <ul>
                            <li>A deposit will be equal to 2 times the highest monthly charge for the specific premise and must be paid at the time service is requested </li>
                        </ul>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Miscellaneous Charges</div><br />
                        <asp:DataList ID="dlMOFees" runat="server" SkinID="DataList" DataSourceID="sqlDSMO">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSMO" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 85 ORDER BY documents.webdescription">
                        </asp:SqlDataSource>
                        <br />
                        <hr style="color:#113962;height:1px;clear:both;margin-top:10px;"/>
                        <div class="sectionheader">Penalties</div><br />
                        <ul>
                            <li>Residential accounts have a 21-day due date and are assessed a 0.5% penalty if not paid by the due date</li>
                            <li>Small Commercial accounts have a 21-day due date and are assessed a 5% penalty if not paid by the due date</li>
                            <li>Large Commercial accounts have a 14-day due date and are assessed a 5% penalty if not paid by the due date </li>
                        </ul>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Deposit Interest for electric</div><br />
                        <asp:DataList ID="dlMODI" runat="server" DataSourceID="sqlDSMODI">
                        <itemtemplate>
                            <asp:Label ID="lblDI" runat="server" Text='<%#Eval("description")%>' />
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSMODI" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="select top 1 description from messages where type = 'MO Interest'">
                        </asp:SqlDataSource>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Deposit Interest for gas</div><br />
                        <asp:DataList ID="dlDIGAS" runat="server" DataSourceID="sqlDSDIGAS">
                        <itemtemplate>
                            <asp:Label ID="lblDI" runat="server" Text='<%#Eval("description")%>' />
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSDIGAS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="select top 1 description from messages where type = 'Gas Interest'">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlMO--%>

                <asp:Panel ID="pnlOK" runat="server" HeaderText="Oklahoma" Visible="false">
                    <ContentTemplate>
                        <div class="sectionheader">Residential Deposit</div>
                        <br />
                        A deposit may be waived for a new Customer if:
                        <ul>
                            <li>The Customer can supply the Company with a good letter of credit</li> 
                            <li>A qualified Guarantor signs for the Customer</li> 
                        </ul>
                        If a deposit is required, it will be equal to 2 times average monthly charge for the specific premise. Payments for deposit can be made in 2 installments.
                        <br /><br />
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Commercial Deposit</div><br />
                        <ul>
                            <li>A deposit will be equal to 2 times the average monthly charge for the specific premise and must be paid at the time service is requested </li>
                        </ul>
                        <hr style="color:#113962;height:1px;" />
                        <div class="sectionheader">Miscellaneous Charges</div>
                        <br />
                        <asp:DataList ID="dlOKFees" runat="server" SkinID="DataList" DataSourceID="sqlDSOK">
                        <itemtemplate>
                            <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                            <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" runat="server"/> 
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSOK" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 86 ORDER BY documents.webdescription ">                         
                        </asp:SqlDataSource>
                        <br /><hr style="color:#113962;height:1px;clear:both;margin-top:10px;"/>
                        <div class="sectionheader">Penalties</div><br />
                        <ul>
                            <li>Residential accounts have a 20-day due date and are assessed a 1.5% penalty if not paid by the due date</li>
                            <li>Small Commercial accounts have a 20-day due date and are assessed a 1.5% penalty if not paid by the due date</li> 
                            <li>Large Commercial accounts have a 14-day due date and are assessed a 1.5% penalty if not paid by the due date</li> 
                        </ul>
                        <hr style="color:#113962;height:1px;"/>
                        <div class="sectionheader">Deposit Interest</div><br />
                        <asp:DataList ID="dlOKDI" runat="server" DataSourceID="sqlDSOKDI">
                        <itemtemplate>
                            <asp:Label ID="lblDI" runat="server" Text='<%#Eval("description")%>' />
                        </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSOKDI" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                             SelectCommand="select top 1 description from messages where type = 'OK Interest'">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlOK--%>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>