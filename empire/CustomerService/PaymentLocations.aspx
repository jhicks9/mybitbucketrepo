<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="PaymentLocations.aspx.vb" Inherits="CustomerService_PaymentLocations" title="Payment Locations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="RightPlaceHolder">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lbAR" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/PaymentLocations.aspx?plocation=AR" Text="Arkansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbKS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/PaymentLocations.aspx?plocation=KS" Text="Kansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbMO" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/PaymentLocations.aspx?" Text="Missouri"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbOK" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/PaymentLocations.aspx?plocation=OK" Text="Oklahoma"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbGAS" runat="server" CausesValidation="true" PostBackUrl="~/CustomerService/PaymentLocations.aspx?plocation=GAS" Text="Gas"/>
            <br />&nbsp

                <asp:Panel ID="pnlPLocations" runat="server" HeaderText="Payment Locations">
                    <ContentTemplate>
                        <asp:GridView ID="gvPLocations" runat="server" DataSourceID="sqlDSPLocations" SkinID="GridView">
                            <Columns>
                                <asp:BoundField DataField="area" HeaderText="Area" SortExpression="area" />            
                                <asp:TemplateField HeaderText="Name" SortExpression="name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("name") %>'></asp:Label><br />
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("address") %>'></asp:Label><br />
                                        <asp:Label ID="lblCity" runat="server" Text='<%# Bind("city") %>'></asp:Label>,&nbsp
                                        <asp:Label ID="lblState" runat="server" Text='<%# Bind("state") %>'></asp:Label>&nbsp&nbsp
                                        <asp:Label ID="lblZip" runat="server" Text='<%# Bind("zip") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                <asp:TemplateField HeaderText="Hours" SortExpression="hours">
                                    <ItemTemplate>
                                        <asp:Label ID="lblHours" runat="server" Text='<%# Eval("hours").ToString.Replace(chr(13), "<br />") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                                                
                                <asp:TemplateField HeaderText="Accepted Payments" SortExpression="payments">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPayments" runat="server" Text='<%# Eval("payments").ToString.Replace(chr(13), "<br />") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>                    
                        <asp:SqlDataSource ID="sqlDSPLocations" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:Panel><%--end pnlPLocations--%>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>