﻿
Partial Class CustomerService_ServiceTransfer
    Inherits System.Web.UI.Page

    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtAccountCT.Attributes("onkeyup") = "autotab(" + txtAccountCT.ClientID + ", " + txtAccountBP.ClientID + ")"
        txtAccountBP.Attributes("onkeyup") = "autotab(" + txtAccountBP.ClientID + ", " + txtAccountCD.ClientID + ")"
        txtAccountCD.Attributes("onkeyup") = "autotab(" + txtAccountCD.ClientID + ", " + txtAccountName.ClientID + ")"
        txtSSN1.Attributes("onkeyup") = "autotab(" + txtSSN1.ClientID + ", " + lbVerifyAccount.ClientID + ")"
        txtPrimaryPhone1.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone1.ClientID + ", " + txtPrimaryPhone2.ClientID + ")"
        txtPrimaryPhone2.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone2.ClientID + ", " + txtPrimaryPhone3.ClientID + ")"
        txtPrimaryPhone3.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone3.ClientID + ", " + txtEmail.ClientID + ")"
        txtAccountCT.Focus()
    End Sub

    Protected Sub lbVerifyAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbVerifyAccount.Click
        Try
            lblVerificationStatus.Text = ""
            encrypt = tdes.Encrypt(txtAccountCT.Text)
            Dim strBillPkg As String = Trim(txtAccountBP.Text)
            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            Dim status As Integer = cwservice.ValidateCustomer(encrypt, strBillPkg, txtAccountName.Text, txtSSN1.Text)

            If status = 1 Then 'move to next panel
                pnlVerification.Visible = False
                pnlCurrentServiceInfo.Visible = True
                txtCurrentServiceAddress.Focus()
            ElseIf status = 0 Then
                lblVerificationStatus.ForeColor = Drawing.Color.Red
                lblVerificationStatus.Text = "Cannot find account, please verify account information."
            ElseIf status = 2 Then
                lblVerificationStatus.ForeColor = Drawing.Color.Red
                lblVerificationStatus.Text = "Unable to verify account. Please contact Customer Service at 800-206-2300."
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSubmit.Click
        Dim MailObj As New System.Net.Mail.SmtpClient
        Dim message As New System.Net.Mail.MailMessage()
        Dim fromAddress As New System.Net.Mail.MailAddress("admin@empiredistrict.com")
        Dim strEmailBody As String = "Service Transfer Request Form" & vbCrLf & vbCrLf
        Try
            lbSubmit.Enabled = False  'eliminate double clicking the submit button
            message.From = fromAddress
            message.To.Add("customer.accounts@empiredistrict.com")
            message.Subject = "Service Transfer Request Form"
            'MailObj.Host = "" -- obtained from web.config 03/14/2012

            strEmailBody += "Account: " & txtAccountCT.Text & "-" & txtAccountBP.Text & "-" & txtAccountCD.Text & vbCrLf
            strEmailBody += "Name: " & txtAccountName.Text & vbCrLf
            strEmailBody += "Current Service Address: " & txtCurrentServiceAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtCurrentServiceAddressAN.Text & " Lot Number: " & txtCurrentServiceAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtCurrentServiceAddressCity.Text & ", " & txtCurrentServiceAddressState.Text & " " & txtCurrentServiceAddressZip.Text & vbCrLf
            strEmailBody += "Primary Phone: (" & txtPrimaryPhone1.Text & ")" & txtPrimaryPhone2.Text & "-" & txtPrimaryPhone3.Text & vbCrLf
            strEmailBody += "Email address: " & txtEmail.Text & vbCrLf
            strEmailBody += vbCrLf
            strEmailBody += "Services to transfer: " & vbCrLf
            For i = 0 To cblServices.Items.Count - 1
                If cblServices.Items(i).Selected Then
                    strEmailBody += cblServices.Items(i).Text & vbCrLf
                End If
            Next
            strEmailBody += vbCrLf
            strEmailBody += "Disconnect Date: " & txtDisconnectDate.Text & vbCrLf
            strEmailBody += "New Service Address: " & txtNewServiceAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtNewServiceAddressAN.Text & " Lot Number: " & txtNewServiceAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtNewServiceAddressCity.Text & ", " & txtNewServiceAddressState.Text & " " & txtNewServiceAddressZip.Text & vbCrLf
            strEmailBody += "Connect Date: " & txtConnectDate.Text & vbCrLf
            If rblElectricOnly.SelectedIndex >= 0 Then
                strEmailBody += "Electric Only: " & rblElectricOnly.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Electric Only: " & vbCrLf
            End If
            If rblWaterHeater.SelectedIndex >= 0 Then
                strEmailBody += "Water Heater: " & rblWaterHeater.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Water Heater: " & vbCrLf
            End If
            strEmailBody += vbCrLf
            strEmailBody += "Mailing Address: " & txtMailingAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtMailingAddressAN.Text & " Lot Number: " & txtMailingAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtMailingAddressCity.Text & ", " & txtMailingAddressState.Text & " " & txtMailingAddressZip.Text & vbCrLf
            If rblContactMethod.SelectedIndex >= 0 Then
                strEmailBody += "Contact Method: " & rblContactMethod.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Contact Method: " & vbCrLf
            End If
            strEmailBody += "Additional Information: " & txtAdditionalInformation.Text & vbCrLf

            message.Body = strEmailBody
            MailObj.Send(message)

            lblStatus.Text = "<b>** Email Sent ** <br />Thank you for submitting a service transfer request.</b><br /><br />You will be contacted within two business days of receipt of your request.<br /><br />"
            pnlNewServiceInfo.Visible = False
            pnlFinalize.Visible = True

        Catch ex As Exception
            lblStatus.Text = ex.Message
            lbSubmit.Enabled = True
        End Try
    End Sub

    Protected Sub lbNewServiceInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbNewServiceInfoNext.Click
        Try
            Dim electricSelected As Boolean = False
            'only show water heater and electric only questions IF user selected electric as service to transfer
            For i = 0 To cblServices.Items.Count - 1
                If cblServices.Items(i).Selected Then
                    If (cblServices.Items(i).Text = "Electric") Or (cblServices.Items(i).Text = "All Services") Then
                        electricSelected = True
                    End If
                End If
            Next
            If electricSelected Then
                lblElectricOnly.Visible = True
                rblElectricOnly.Visible = True
                lblWaterHeater.Visible = True
                rblWaterHeater.Visible = True
            Else
                lblElectricOnly.Visible = False
                rblElectricOnly.Visible = False
                lblWaterHeater.Visible = False
                rblWaterHeater.Visible = False
            End If
            pnlCurrentServiceInfo.Visible = False
            pnlNewServiceInfo.Visible = True
        Catch
        End Try
    End Sub

    Protected Sub lbCurrentServiceInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCurrentServiceInfoPrev.Click
        pnlNewServiceInfo.Visible = False
        pnlCurrentServiceInfo.Visible = True
    End Sub

    Protected Sub btnDuplicateAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuplicateAddress.Click
        txtMailingAddress.Text = txtNewServiceAddress.Text
        txtMailingAddressAN.Text = txtNewServiceAddressAN.Text
        txtMailingAddressLN.Text = txtNewServiceAddressLN.Text
        txtMailingAddressCity.Text = txtNewServiceAddressCity.Text
        txtMailingAddressState.Text = txtNewServiceAddressState.Text
        txtMailingAddressZip.Text = txtNewServiceAddressZip.Text
    End Sub

End Class
