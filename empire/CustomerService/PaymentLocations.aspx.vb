
Partial Class CustomerService_PaymentLocations
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If String.IsNullOrEmpty(Request.QueryString("plocation")) Then
                'If no variable is pass default to MO
                sqlDSPLocations.SelectCommand = "SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'MO' order by area,city,name"
                gvPLocations.DataBind()
                lbMO.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
                lbGAS.Font.Bold = False
            ElseIf Request.QueryString.ToString = "plocation=AR" Then
                sqlDSPLocations.SelectCommand = "SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'AR' order by area,city,name"
                gvPLocations.DataBind()
                lbAR.Font.Bold = True
                lbMO.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
                lbGAS.Font.Bold = False
            ElseIf Request.QueryString.ToString = "plocation=KS" Then
                sqlDSPLocations.SelectCommand = "SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'KS' order by area,city,name"
                gvPLocations.DataBind()
                lbKS.Font.Bold = True
                lbAR.Font.Bold = False
                lbMO.Font.Bold = False
                lbOK.Font.Bold = False
                lbGAS.Font.Bold = False
            ElseIf Request.QueryString.ToString = "plocation=OK" Then
                sqlDSPLocations.SelectCommand = "SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'OK' order by area,city,name"
                gvPLocations.DataBind()
                lbOK.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbMO.Font.Bold = False
                lbGAS.Font.Bold = False
            ElseIf Request.QueryString.ToString = "plocation=GAS" Then
                sqlDSPLocations.SelectCommand = "SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'Gas' order by city,name"
                gvPLocations.DataBind()
                lbGAS.Font.Bold = True
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbOK.Font.Bold = False
                lbMO.Font.Bold = False
            End If
        Catch
        End Try
    End Sub
End Class
