﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Value.aspx.vb" Inherits="CustomerService_Value" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="background-image:url(../images/landing/landing-value.jpg);background-repeat:no-repeat;background-position:center center;width:750px;height:422px;">
        <tr>
            <td valign="top" style="text-align:center;width:67%;padding:20px">
                <div style="font-size:1.5em;font-weight:bold;color:#E07F48">Electricity.</div>
                <hr />
                <div style="line-height:1.9">You depend on it every minute of every day. In fact, no other single product touches so many aspects of our daily lives or delivers more value than electricity. It keeps our homes comfortable, powers communication and technology, lights our communities, and fuels business and industry. It’s a big job. That’s why we’re always here, working to provide energy that is safe, reliable, environmentally responsible, and a good value.<br /></div>
                <br /><br />
                        <asp:DataList ID="dlBrochure" runat="server" SkinID="" DataSourceID="sqlBrochures">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="hlViewInsert" runat="server" Target="_blank" Text='<%# Eval("name") %>' NavigateUrl='<%# Eval("id", "~/DisplayBinary.aspx?dtype=inserts&id={0}") %>' />
                            </itemtemplate>
                        </asp:DataList>
                <br /><br />
                <asp:Image ID="imgLogo" runat="server" ImageUrl="~/images/navigation/empire-logo.png" />
            </td>
            <td valign="bottom" style="width:33%;text-align:center;padding:20px"></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr><td style="padding:3px 0 0 3px;background-color:#E07F48"><acrobat:uc4 ID="uc1" runat="server" /></td></tr>
    </table>

    <asp:SqlDataSource ID="sqlBrochures" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT billinsert.name, billinsert.id FROM billinsert where billinsert.name like 'value%'">
    </asp:SqlDataSource>
</asp:Content>

