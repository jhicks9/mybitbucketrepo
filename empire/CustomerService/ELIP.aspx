﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ELIP.aspx.vb" Inherits="CustomerService_ELIP" Title="ELIP" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <cc1:TabContainer ID="TabMainContainer" runat="server" ActiveTabIndex="0"  CssClass="customTabXP">
        <%--<cc1:TabPanel ID="tabElectric" runat="server" HeaderText="Electric">
            <ContentTemplate>
                <b>Electric</b><br /><br />
                Empire participates in the Experimental Low-Income Program. This program is facilitated by local community action agencies. Please contact your local agency for program details and eligibility requirements.
            </ContentTemplate>
        </cc1:TabPanel>--%><%--end tabElectric--%>
        
        <cc1:TabPanel ID="tabGas" runat="server" HeaderText="Gas">
            <ContentTemplate>
                <b>Gas</b><br /><br />
                The Experimental Low-Income Program currently is only available in Sedalia, Missouri. For more information, please contact the Missouri Valley Community Action Agency at 660-826-0804 for program details and eligibility requirements.   
            </ContentTemplate>
        </cc1:TabPanel><%--end tabGas--%>
    </cc1:TabContainer><%--end TabMainContainer--%>
</asp:Content>

