<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProjHelpForm.aspx.vb" Inherits="CustomerService_ProjHelpForm" title="Project Help Pledge Form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Project Help Pledge Form</title>
    <style type="text/css" media="print">
        body {color:#000000;background:#ffffff;font-family:"Times New Roman",Times,serif;font-size:12pt; }
        a {text-decoration:underline;color:#0000ff;}
    </style>    
</head>
<body>
    <form id="form1" runat="server">
        <div style="width:100%;background-color:#ffffff;">
            <div style="float:left;width:20%;color:#000000;background-color:#ffffff;">
                <asp:Image id="imgLogo" runat="server" ImageUrl="~/images/navigation/logo_bw.jpg" />
            </div>
            <div style="float:left;width:80%;color:#000000;background-color:#ffffff;font-size:24pt;text-align:center;">
                <img id="Img1" src="~/Images/projecthelplogo.gif" runat="server" alt="projlogo"/>
            </div>
            <div style="float:left;width:80%;color:#000000;background-color:#ffffff;text-align:center;">
                <asp:Label ID="lblDate" runat="server" Text="" ></asp:Label>
            </div> 
            <br style="clear:both;"/>                       
            
            <br /><br />
            To support Project Help, please complete the following pledge form:
            <br /><br />
            <table style="border-collapse:collapse;border-spacing:0">
                <tr>
                    <td style="vertical-align:top"><asp:CheckBox ID="CheckBox1" runat="server"/></td>
                    <td style="vertical-align:top"><label >YES,</label>I pledge my support to Project Help in the amount of $_____________, per month,</td>
                </tr>
                <tr>
                    <td></td>
                    <td>to be paid along with my Empire <asp:Label ID="lblType" runat="server" Text="" /> bill each month.</td>
                </tr>
                <tr>
                    <td></td>
                    <td style="vertical-align:top">(You may cancel your participation at any time).</td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr>
                    <td style="vertical-align:top"><asp:CheckBox ID="CheckBox2" runat="server" /></td>
                    <td style="vertical-align:top">
                        <label >YES,</label>I pledge my support to Project Help with a single donation of:
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top"></td>
                    <td style="vertical-align:top">
                        <br />
                        _____________ $10<br /><br />
                        _____________ $20<br /><br />
                        _____________ $50<br /><br />
                        _____________ $ other
<%--                        <table style="border-collapse:separate;border-spacing:3px">
                            <tr>
                                <td>_____________ $10</td>
                                <td>_____________ $20</td>
                                <td>_____________ $50</td>
                                <td>other $ _____________</td>
                            </tr>
                        </table>--%>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>(Please make check payable to Project Help-<asp:Label ID="lblCheckType" runat="server" Text="" />.)</td>
                </tr>
            </table>
            <br /><br />
            <div style="float:left;width:auto;"><asp:Label ID="Label1" runat="server" Text="Name:" Width="120px" ></asp:Label></div>
            <div style="float:left;width:auto;"><input name="Name" size="35" /></div>
            <br style="clear:both;"/>
            <div style="float:left;width:auto;"><asp:Label ID="Label2" runat="server" Text="Address:" Width="120px"></asp:Label></div>
            <div style="float:left;width:auto;"><input name="Address" size="35" /></div>
            <br style="clear:both;"/>
            <div style="float:left;width:auto;"><asp:Label ID="Label3" runat="server" Text="City:" Width="120px"></asp:Label></div>
            <div style="float:left;width:auto;"><input name="City" size="35" /></div> 
            <br style="clear:both;"/>
            <div style="float:left;width:auto;"><asp:Label ID="Label4" runat="server" Text="State:" Width="120px"></asp:Label></div>
            <div style="float:left;width:auto;"><input name="State" size="35" /></div>
            <br style="clear:both;"/>
            <div style="float:left;width:auto;"><asp:Label ID="Label5" runat="server" Text="Zip Code:" Width="120px"></asp:Label></div>
            <div style="float:left;width:auto;"><input name="Zip" size="8" /></div>
            <br style="clear:both;"/>
            <div style="float:left;width:auto;"><asp:Label ID="Label6" runat="server" Text="Phone:" Width="120px"></asp:Label></div>
            <div style="float:left;width:auto;"><input name="Phone" size="15"/></div>
            <br style="clear:both;"/>
            <br /><br />
            <a href="javascript:void printIt();" runat="server" style="color:blue;font:bold;text-decoration:underline;border-style:none;"><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Please print</a> this pledge form and return with your bill payment.  Thank you for your support!                
        </div>
    </form>
</body>
</html>
<script type="text/javascript">
  function printIt()
  {
     window.print();
  }
</script>
<%--Recommendations for a Print-Friendly Webpage
Change colors to black on white
Change the font to a serif face
Change the font size to 12pt (or larger)
Underline all links
Remove non-essential images,navigation,all advertising, JavaScript, Flash, and animated images
Include a by-line
Include the original URL
Include a copyright notification--%>