﻿
Partial Class CustomerService_ServiceConnectCommercial
    Inherits System.Web.UI.Page

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        If Not ckbAgree.Checked Then
            lblAgreeStatus.Text = "You must accept the terms and conditions"
        Else
            lblAgreeStatus.Text = ""  'reset form fields
            ckbAgree.Checked = False
            pnlTerms.Visible = False
            pnlAgree.Visible = True
        End If
    End Sub
End Class
