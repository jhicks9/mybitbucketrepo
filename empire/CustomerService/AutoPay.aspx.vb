
Partial Class CustomerService_AutoPay
    Inherits System.Web.UI.Page

    Function ResolveSSLUrl(ByVal url As String) As String
        Dim ssl As String = ""
        Try
            ssl = String.Format("https://{0}{1}", Request.ServerVariables("HTTP_HOST"), ResolveUrl(url))
        Catch
        End Try
        Return ssl
    End Function
End Class
