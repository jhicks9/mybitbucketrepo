
Partial Class CustomerService_Payments
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)
    End Sub
End Class
