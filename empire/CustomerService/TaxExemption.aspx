<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="TaxExemption.aspx.vb" Inherits="CustomerService_TaxExemption" title="Tax Exemption" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <b>Non-Residential Customer Tax Exemption</b>
    <br /><br />
    This is available to non-residential tax-exempt customers in all states served by Empire.  To be eligible, customers must provide Empire with a copy of their tax exemption certification.  Empire account numbers need to be included.  Documents can be faxed to 417-625-5169 or emailed to <a id="A1" href="~/About/ContactUs.aspx?type=cs" runat="server">customer.service@empiredistrict.com</a>.
    <br /><br />
    <b>Manufacturing Customers Sales Tax Exemption</b>
    <br /><br />
    This is available to eligible MO electric and natural gas customers of Empire.  Manufacturing customers in the state of Missouri may qualify for the Missouri state sales tax exemption for energy used in manufacturing, effective August 28, 2007.
    <br /><br />
    <b>Empire can not advise customers on how to determine their exemption percentage or if they qualify.</b>  The Missouri Department of Revenue (MDOR) provides information on how to make that determination and provides Form 149.  Empire  needs this form to exempt accounts.  It is available at the <a href="http://www.dor.mo.gov/tax/business/sales/manuexempt/" target="_blank">MDOR Web site</a>.
    <br /><br />
    At the MDOR Web site, there are examples and emergency rules to help customers determine their qualification for the exemption and the exemption percentage.
    <br /><br />
    Missouri Department of Revenue Contact Information<br />
    <a href="http://www.dor.mo.gov" target="_blank">www.dor.mo.gov</a><br />
    573-751-2836<br />
    Missouri Department of Revenue<br />
    P.O. Box 3300<br />
    Jefferson City, MO 65105-3300<br />
    salesuse@dor.mo.gov
    <br /><br />
    Please note that the exempt percentage provided to Empire may not be the exempt percentage applied to the bill.  The MDOR has provided Empire a table for determining the rate to apply.  This table is shown on the MDOR Web site in an emergency rule 
    12 CSR 10-110.601—Electrical, Other Energy and Water as defined in Section 144.054, RSMo. 
    <br /><br />
    After determining the exempt percentage, please complete Part A and section 5b on Form 149, sign and date the form, and fax to 417-625-5169 or e-mail to <a href="~/About/ContactUs.aspx?type=cs" runat="server">customer.service@empiredistrict.com</a>.
    <br /><br />
    When completing the form, please be sure to check the box(es) that apply, provide the exempt percentage and method of calculation, as well as the accounts to which Empire should apply the exempt percentage.
</asp:Content>