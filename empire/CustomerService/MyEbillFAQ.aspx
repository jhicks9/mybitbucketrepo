﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="MyEbillFAQ.aspx.vb" Inherits="CustomerService_MyEbillFAQ" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    Enroll in MyEbill, Empire District’s free service providing you the ease of receiving and viewing your bill online.  In order to enroll for MyEbill, you must first sign up for MyAccount.
    <br /><br />
    <asp:HyperLink ID="hlRegister1" runat="server" NavigateUrl="~/question.aspx" Text="Sign Up for MyAccount" /><br />
    <a href='<%= ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx") %>' >Log in to MyAccount</a>
    <br /><br />

    <b>Why Sign Up for MyEbill?</b>
    <br /><hr style="color:#113962;height:1px;"/>
    <ul>
        <li>A monthly email is sent directly to you when your bill is ready</li>
        <li>Online access is available to see your bills and payments</li>
        <li>View previous three statements</li>
        <li>Usage history going back 24 months</li>
        <li>Ability to print and save your online bill</li>
        <li>Utilize Auto-Pay so you can receive your bill online and pay automatically – never worry about checks and stamps again</li>
    </ul>

    <b>How Can I Sign Up?</b>
    <br /><hr style="color:#113962;height:1px;"/><br />
    To sign up for MyEbill, you need to first create a MyAccount registry. To do this you must have:
    <ul>
        <li>An Empire District account number</li>
        <li>The customer name as it appears on the statement</li>
        <li>The corresponding last four digits of the customer’s Social Security number </li>
    </ul>
    If you are already registered with MyAccount, simply log in and select MyEbill from the menu and follow the easy steps to sign up.
    <br /><br />
    New MyAccount user?<br />
    <asp:HyperLink ID="hlRegister2" runat="server" NavigateUrl="~/question.aspx" Text="Register for MyAccount" /> and sign up for MyEbill.
    <br /><br />
    Current MyAccount user?<br />
    <a href='<%= ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx") %>' >Log in to MyAccount</a> and sign up for MyEbill.
    <br /><br />

    <b>Frequently Asked Questions</b>
    <br /><hr style="color:#113962;height:1px;"/><br />
    <a href="myEbillFAQ.aspx#A1">Is MyEbill secure?</a><br />
    <a href="myEbillFAQ.aspx#A2">What are my payment options?</a><br />
    <a href="myEbillFAQ.aspx#A3">Can any customer enroll?</a><br />
    <a href="myEbillFAQ.aspx#A4">Will I continue to receive my bill by mail?</a><br />
    <a href="myEbillFAQ.aspx#A5">If I transfer my service, will the MyEbill transfer?</a><br />
    <a href="myEbillFAQ.aspx#A6">What if I have questions or concerns about my bill?</a><br />
    <br /><br />
    <a id="A1"></a><strong>Q. Is MyEbill secure?</strong><br />
    <b>A.</b> Yes. For security and privacy, enrollment requires a user name and password to log on to MyAccount. We use the industry standard encryption for Web content.
    <br /><br /><hr style="color:#113962;height:1px;"/><br />
        
    <a id="A2"></a><strong>Q. What are my payment options?</strong><br />
    <b>A.</b> Empire offers the following payment options:<br /><br />
    <b>MyEbill with Auto-Pay</b><br />
    This is the easiest way to pay your Empire bill. Each month the balance due which will be shown on your MyEbill will be deducted from your bank account. When you sign up for Auto-Pay, you can select your payment date. However, if the date you select will not work with your current due date, an Empire representative will contact you to discuss your options.        
    &nbsp;&nbsp;<asp:HyperLink ID="hlAutoPay" runat="server" NavigateUrl="~/CustomerService/AutoPay.aspx">Sign up for Auto-Pay</asp:HyperLink>
    <br /><br />
    <b>Average Payment Plan</b><br />
    For additional convenience, customers can also sign up for the Average Payment Plan (APP). APP calculates the expected annual service charges and divides it into equal payments. At the end of the contract year, the actual usage is renewed and a customer's contract and installment amount are adjusted for the next 12 months. Participation in APP requires customers pay their pre-determined average amount every month – even if the status shown on their contract information is a credit. Customers who fail to make monthly installment payments are subject to have their APP contract terminated. APP customers can also select a new payment due date which can be up to seven days from their current due date.
    &nbsp;&nbsp;<asp:HyperLink ID="hlAveragePay" runat="server" NavigateUrl="~/CustomerService/AvgPayment.aspx">Sign up for Average Payment Plan</asp:HyperLink>
    <br /><br />
    <b>Make a One-Time Payment</b><br />
    Customers can make an immediate payment by credit card or electronic funds transfer either online or by phone. There is a convenience fee for this service. Click <asp:HyperLink ID="hlPayments" runat="server" NavigateUrl="~/CustomerService/Payments.aspx">here</asp:HyperLink> to make a one-time payment.
    <br /><br />
    <b>Utilize a Payment Location</b><br />
    Customers can pay their monthly bill at their local Empire office or through a collection site that can be found nearby. A list of collectors is available <asp:HyperLink ID="hlLocation" runat="server" NavigateUrl="~/CustomerService/PaymentLocations.aspx">here</asp:HyperLink>.
    <br /><br />
    <b>Pay by Mail</b><br />
    Paying by mail remains an option for customers using MyEbill. Customers need to print their bill, remove the remittance portion, and mail it to:<br />
    Empire District<br />
    PO Box 219239<br />
    Kansas City, MO 64121-9239
    <br /><br /><hr style="color:#113962;height:1px;"/><br />

    <a id="A3"></a><strong>Q. Can any customer enroll?</strong><br />
    <b>A.</b> Yes. Existing customers can enroll at anytime. New customer may enroll once they receive their Empire District account number.
    <br /><br /><hr style="color:#113962;height:1px;"/><br />

    <a id="A4"></a><strong>Q. Will I continue to receive my bill by mail?</strong><br />
    <b>A.</b> After you enroll for MyEbill, you will no longer receive your paper bill. Instead you will receive an email each month letting you know your new bill is available online.
    <br /><br />
    If you do not receive your email notification, please verify it has not been sent to your Spam, Junk, or Bulk mail folder. Make sure to add our email address: myebill@empiredistrict.com to your approved senders list so your internet service provider will accept these messages.
    <br /><br />
    If you do not receive your monthly email, you can still view your bill through the MyAccount section of the Empire Web site.
    <br /><br />
    The email sent serves as notification for you to view your bill. Non-receipt of the email does not absolve a customer from their obligation to pay the amount due to Empire District. 
    <br /><br /><hr style="color:#113962;height:1px;"/><br />

    <a id="A5"></a><strong>Q. If I transfer my service, will the MyEbill transfer?</strong><br />
    <b>A.</b> Yes, if your old Empire account was enrolled in MyEbill your new account will be also. However, you may receive a final bill for your old account during the transition.
    <br /><br /><hr style="color:#113962;height:1px;"/><br />

    <a id="A6"></a><strong>Q. What if I have questions or concerns about my bill?</strong><br />
    <b>A.</b> Our Contact Center representatives are available to answer questions about your bill from 7 a.m. to 7 p.m., Monday through Friday. Electric customers should call 800&#8209;206&#8209;2300. Natural gas customers should call 800&#8209;424&#8209;0427.
</asp:Content>
