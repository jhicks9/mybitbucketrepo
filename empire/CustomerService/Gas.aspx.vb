
Partial Class CustomerService_Gas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If String.IsNullOrEmpty(Request.QueryString("gas")) Then
                pnlResidential.Visible = True
                pnlGas.Visible = False
                dlBillExplanation.DataBind()
                dlResidential.DataBind()
                lbRES.Font.Bold = True
                lbCOMM.Font.Bold = False
                lbIND.Font.Bold = False
                lbTS.Font.Bold = False
                lbPGA.Font.Bold = False
            ElseIf Request.QueryString.ToString = "gas=COMM" Then
                pnlResidential.Visible = False
                pnlGas.Visible = True
                lblGas.Text = "Commercial Gas Rates"
                sqlDSGas.SelectCommand = "SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 64 ORDER BY documents.webdescription"
                dlGas.DataBind()
                lbRES.Font.Bold = False
                lbCOMM.Font.Bold = True
                lbIND.Font.Bold = False
                lbTS.Font.Bold = False
                lbPGA.Font.Bold = False
            ElseIf Request.QueryString.ToString = "gas=IND" Then
                pnlResidential.Visible = False
                pnlGas.Visible = True
                lblGas.Text = "Industrial Gas Rates"
                sqlDSGas.SelectCommand = "SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 65 ORDER BY documents.webdescription"
                dlGas.DataBind()
                lbRES.Font.Bold = False
                lbCOMM.Font.Bold = False
                lbIND.Font.Bold = True
                lbTS.Font.Bold = False
                lbPGA.Font.Bold = False
            ElseIf Request.QueryString.ToString = "gas=TS" Then
                pnlResidential.Visible = False
                pnlGas.Visible = True
                lblGas.Text = "Transportation Services"
                sqlDSGas.SelectCommand = "SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 67 ORDER BY documents.webdescription"
                dlGas.DataBind()
                lbRES.Font.Bold = False
                lbCOMM.Font.Bold = False
                lbIND.Font.Bold = False
                lbTS.Font.Bold = True
                lbPGA.Font.Bold = False
            ElseIf Request.QueryString.ToString = "gas=PGA" Then
                pnlResidential.Visible = False
                pnlGas.Visible = True
                lblGas.Text = "Purchased Gas Adjustments"
                sqlDSGas.SelectCommand = "SELECT documents.webdescription AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE documents.groupid = 14 AND documents.doctypeid = 66 ORDER BY documents.webdescription"
                dlGas.DataBind()
                lbRES.Font.Bold = False
                lbCOMM.Font.Bold = False
                lbIND.Font.Bold = False
                lbTS.Font.Bold = False
                lbPGA.Font.Bold = True
            End If
        Catch
        End Try
    End Sub
End Class