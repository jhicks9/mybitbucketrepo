Imports System.Data
Imports System
Imports System.Web
Imports System.Net
Imports System.io
Partial Class CustomerService_Commission_Payments
    Inherits System.Web.UI.Page
    Public custToken As String = ""
    Dim billpToken As String = ""
    Dim custName As String = ""
    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Public printurl As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            printurl = ResolveUrl("~/print.aspx")
            If Len(Request.QueryString("c")) < 1 Or Len(Request.QueryString("b")) < 1 Then
                Response.Redirect("~/CustomerService/Commission/AccountLookup.aspx")
            End If
            custToken = tdes.Decrypt(Request.QueryString("c"))
            billpToken = tdes.Decrypt(Request.QueryString("b"))
            hdnCheckDigit.Value = cd.CalculateCheckDigit(cd.ZeroPad(custToken & billpToken, 8))
            custName = Request.QueryString("n")

            hlMenu1.NavigateUrl = "~/CustomerService/Commission/Default.aspx?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n"))
            hlMenu2.NavigateUrl = "#top"

            showBillPackages()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub showBillPackages()
        Try
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)
            Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)
            Dim billPkgTkn As Array = Split(rtnBillPackages, ",")
            For i = 0 To billPkgTkn.Length - 1  'always show zero padded billing package
                billPkgTkn(i) = cd.ZeroPad(billPkgTkn(i), 2)
            Next
            dlBillPkg.DataSource = billPkgTkn
            dlBillPkg.DataBind()
            dlBillPkg.Visible = True
        Catch
        End Try
    End Sub
    Public Sub showPayments(ByVal commandname As String)
        Try
            Dim rtnPayment As New System.Data.DataSet
            Dim rtnPaymentDetail As New System.Data.DataSet
            Dim rtnBalance As String
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)
            rtnPayment = cwservice.GetPaymentInfo(encrypt, commandname, "460")

            rtnBalance = cwservice.GetCurrentDue(encrypt, commandname)
            dgPayments.DataSource = rtnPayment
            dgPayments.DataBind()

            MultiView1.ActiveViewIndex = 1
            lblCustName.Text = StrConv(custName, VbStrConv.ProperCase)
            lblBalanceDue.Text = "Current Balance Due: " & rtnBalance
            lblAccount.Text = "Payment(s) for " & custToken & "-" & billpToken & "-" & hdnCheckDigit.Value
        Catch
        End Try
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim rtnValues As New System.Data.DataSet
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim command As String = e.CommandName
            dlBillPkg.Visible = False
            showPayments(command)
        Catch ex As Exception
        End Try
    End Sub
End Class
