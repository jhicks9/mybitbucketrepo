<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Statements.aspx.vb" Inherits="CustomerService_Commission_Statements" title="Statements" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <a id="top"></a>
            <acrobat:uc4 ID="uc4" runat="server" />
            <asp:HiddenField ID="hdnCheckDigit" runat="server" />
            <asp:HiddenField ID="hdnStatementToken" runat="server" Value="" />
            <asp:HiddenField ID="hdnPostBackUrl" runat="server" />
            <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvSummary">    
                <asp:View ID="vwAccounts" runat="server">
                    <asp:DataList id="dlBillPkg" skinId="dlistView" runat="server" >
                        <ItemTemplate>
                            <b><asp:LinkButton id="LinkButton3" Font-Bold="false" CommandArgument="statement" CommandName="<%# Container.DataItem %>" runat="server"><%#custToken%>-<%# Container.DataItem %>-<%=hdnCheckDigit.Value%></asp:LinkButton></b>
                        </ItemTemplate>
                    </asp:DataList>
                </asp:View>        

                <asp:View ID="vwStatements" runat="server">        
                    <div style="background-color:#36587a;height:40px;">
                        <div style="background-color:#36587a;color:#ffffff;padding: 10px 0 5px 10px;height:20px;float:left;"><asp:HyperLink ID="hlMenu1" runat="server" style="color:#ffffff;">Lookup additional information</asp:HyperLink></div>
                        <div style="background-color:#36587a;color:#ffffff;padding: 10px 10px 10px 0;height:20px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" style="color:#ffffff;"><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                    </div>                
                    <div id="print_content"><%--begin print content--%>
                        <div style="background-color:#cccccc;padding:5px 0% 0% 5px;">
                            <asp:Label ID="lblCustName" runat="server"></asp:Label><br />
                            <asp:Label ID="lblAccount" runat="server"></asp:Label><br /><br />
                        </div> 
                        
                        <div style="background-color:#cccccc;padding:0% 0% 5px 5px;">
                            <asp:GridView ID="dgStatements" runat="server" SkinID="GridView" AllowSorting="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Statement Date
                                        </HeaderTemplate>                                    
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlStatement" runat="server" Text='<%#Eval("STMT_DATE") %>' NavigateUrl='<%# hdnPostBackUrl.Value & "&data=" & Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Eval("cust_statement_tkn"))) %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="cust_statement_tkn" HeaderText="Statement Token" SortExpression="cust_statement_tkn" Visible="false" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="charges" HeaderText="Current Charges" SortExpression="charges" />
                                    <asp:BoundField ItemStyle-Width="150px" DataField="taxes" HeaderText="Taxes" SortExpression="taxes" />
                                    <asp:BoundField DataField="total" HeaderText="Total" SortExpression="total" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No information found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <br /><asp:Label ID="lblDirections" runat="server" Text="Select a date to display the statement."></asp:Label><br />
                        </div>
                    </div><%--end print content--%>
                    <div style="background-color:#36587a;color:#ffffff;height:40px;width:100%;">
                        <div style="padding:10px 0 10px 10px;"><asp:HyperLink ID="hlMenu2" runat="server" style="color:#ffffff;">Top</asp:HyperLink></div>
                    </div>
                    <br />                    
                </asp:View>        
            </asp:MultiView>                         
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Statements","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>