Imports System
Imports System.Web
Imports System.Data

Partial Class CustomerService_Commission_Statements
    Inherits System.Web.UI.Page
    Public custToken As String = ""
    Dim billpToken As String = ""
    Dim acctpToken As String = ""
    Dim acctToken As String = ""
    Dim custName As String = ""
    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Public printurl As String = ""
    Dim xdoc As String = ""
    Dim xtemplate As String = ""
    Dim rtnStatement As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            printurl = ResolveUrl("~/print.aspx")
            If Len(Request.QueryString("c")) < 1 Or Len(Request.QueryString("b")) < 1 Then
                Response.Redirect("~/CustomerService/Commission/AccountLookup.aspx")
            End If
            custToken = tdes.Decrypt(Request.QueryString("c"))
            billpToken = tdes.Decrypt(Request.QueryString("b"))
            hdnCheckDigit.Value = cd.CalculateCheckDigit(cd.ZeroPad(custToken & billpToken, 8))
            custName = Request.QueryString("n")

            showBillPackages()
            hlMenu1.NavigateUrl = "~/CustomerService/Commission/Default.aspx?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n"))
            hlMenu2.NavigateUrl = "#top"
            hdnPostBackUrl.Value = ResolveUrl("~/CustomerService/Commission/Stmthandler.ashx") & "?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n"))
        Catch ex As Exception
        End Try
    End Sub

    Public Sub showBillPackages()
        Try
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)
            Dim rtnBillPackages As String = cwservice.GetBillPackage(encrypt)
            Dim billPkgTkn As Array = Split(rtnBillPackages, ",")
            For i = 0 To billPkgTkn.Length - 1  'always show zero padded billing package
                billPkgTkn(i) = cd.ZeroPad(billPkgTkn(i), 2)
            Next
            dlBillPkg.DataSource = billPkgTkn
            dlBillPkg.DataBind()
            dlBillPkg.Visible = True
        Catch
        End Try
    End Sub

    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        Try
            Dim rtnValues As New System.Data.DataSet
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim rtnCharge As String = ""

            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)
            rtnValues = cwservice.GetBillInfo(encrypt, e.CommandName, "90")

            'add 3 columns (charges,taxes,total) to gridview
            Dim dc As DataColumn
            dc = New DataColumn("charges", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            dc = New DataColumn("taxes", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            dc = New DataColumn("total", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            Dim dr As DataRow
            Dim increment As Integer = 0
            For Each dr In rtnValues.Tables(0).Rows
                encrypt = tdes.Encrypt(rtnValues.Tables(0).Rows(increment).Item("cust_statement_tkn"))
                rtnStatement = cwservice.GetCommissionChargeInfo(encrypt)
                If Len(rtnStatement) > 0 And InStr(rtnStatement, ";") > 0 Then
                    rtnValues.Tables(0).Rows(increment).Item("charges") = rtnStatement.Split(";")(0).Trim
                    rtnValues.Tables(0).Rows(increment).Item("taxes") = rtnStatement.Split(";")(1).Trim
                    rtnValues.Tables(0).Rows(increment).Item("total") = rtnStatement.Split(";")(2).Trim
                Else
                    rtnValues.Tables(0).Rows(increment).Item("charges") = ""
                    rtnValues.Tables(0).Rows(increment).Item("taxes") = ""
                    rtnValues.Tables(0).Rows(increment).Item("total") = ""
                End If
                increment = increment + 1
            Next

            dgStatements.DataSource = rtnValues
            dgStatements.DataBind()

            lblCustName.Text = StrConv(custName, VbStrConv.ProperCase)
            lblAccount.Text = "Statements for " & custToken & "-" & billpToken & "-" & hdnCheckDigit.Value
            mvSummary.ActiveViewIndex = 1
        Catch
        End Try
    End Sub

End Class
