<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AccountLogs.aspx.vb" Inherits="CustomerService_Commission_AccountLogs" title="Account Logs" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <a id="top"></a>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvSummary">    
            <asp:View ID="vwAccounts" runat="server">
                <asp:Label ID="lblAccount" runat="server"></asp:Label><br /><br />
                <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList">
                    <Columns>
                        <asp:TemplateField>
                           <ItemTemplate>
                                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                   Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                           </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No information found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:View>        
        
            <asp:View ID="vwSummary" runat="server"> 
                <div style="background-color:#36587a;height:40px;">
                    <div style="background-color:#36587a;color:#ffffff;padding: 10px 0 5px 10px;height:20px;float:left;"><asp:HyperLink ID="hlMenu1" runat="server" style="color:#ffffff;">Lookup additional information</asp:HyperLink></div>
                    <div style="background-color:#36587a;color:#ffffff;padding: 10px 10px 10px 0;height:20px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" style="color:#ffffff;"><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                </div>            
                <div style="background-color:#cccccc;line-height:20px;">
                    <div id="print_content"><%--begin print content--%>
                        <div style="background-color:#cccccc;padding:0% 0% 0% 1%;width:99%;">
                            <asp:Label ID="lblCustNumber" runat="server"></asp:Label><br />
                            <asp:Label ID="lblCustName" runat="server"></asp:Label><br />
                            <asp:Label ID="lblCustAddress" runat="server" ></asp:Label><br />
                            <br />
                        </div>                    
                        
                        <div style="clear:both;background-color:#cccccc;padding:0% 0% 0% 1%;width:99%;">    
                            <asp:GridView ID="gvLogs" runat="server" AutoGenerateColumns="True" CellSpacing="0" CellPadding="4" GridLines="None" ShowFooter="False" ShowHeader="True">
                                <EmptyDataTemplate>
                                    No log information found.
                                </EmptyDataTemplate>
                                <RowStyle BackColor="#CCCCCC" />
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <EmptyDataRowStyle BackColor="#cccccc" ForeColor="Maroon" />                            
                                <HeaderStyle BackColor="#AFAFAF" ForeColor="White" />
                            </asp:GridView>  
                        </div>
                        <br />
                    </div><%--end print content--%>
                </div>                
                <div style="background-color:#36587a;color:#ffffff;height:40px;width:100%;">
                    <div style="padding:10px 0 10px 10px;"><asp:HyperLink ID="hlMenu2" runat="server" style="color:#ffffff;">Top</asp:HyperLink></div>
                </div>
            </asp:View>                    
        </asp:MultiView>             
    </ContentTemplate>
    </asp:UpdatePanel>
     
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Account Logs","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>

