Imports System.Data
Imports System
Imports System.Web
Imports System.Net
Imports System.io

Partial Class CustomerService_Commission_MeterReads
    Inherits System.Web.UI.Page
    Dim custToken As String = ""
    Dim billpToken As String = ""
    Dim cdToken As String = ""
    Dim acctpToken As String = ""
    Dim acctToken As String = ""
    Dim servicetype As String = ""
    Dim custName As String = ""
    Dim custAddress As String = ""
    Dim strUnit As String = ""
    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Public printurl As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            printurl = ResolveUrl("~/print.aspx")
            If Len(Request.QueryString("c")) < 1 Or Len(Request.QueryString("b")) < 1 Then
                Response.Redirect("~/CustomerService/Commission/AccountLookup.aspx")
            End If
            custToken = tdes.Decrypt(Request.QueryString("c"))
            billpToken = tdes.Decrypt(Request.QueryString("b"))
            cdToken = cd.CalculateCheckDigit(cd.ZeroPad(custToken & billpToken, 8))
            custName = Request.QueryString("n")

            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)

            Dim rtnAcctTkns As New System.Data.DataSet
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, billpToken)
            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            lblAccount.Text = "Select an account to view for " & custToken & "-" & billpToken & "-" & cdToken
            mvSummary.ActiveViewIndex = 0

            hlMenu1.NavigateUrl = "~/CustomerService/Commission/Default.aspx?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n"))
            hlMenu2.NavigateUrl = "#top"
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        Try
            Dim eCmdArg As String = e.CommandArgument
            Dim arTokens As Array = Split(eCmdArg, ",")
            Dim spa As String = arTokens(1)
            Dim spt As String = arTokens(6)
            Dim pt As String = arTokens(7)

            acctToken = arTokens(0)
            servicetype = arTokens(2)
            acctpToken = e.CommandName
            lblCustAddress.Text = arTokens(3) & " " & arTokens(4) & "," & arTokens(5)
            lblCustName.Text = StrConv(custName, VbStrConv.ProperCase)
            lblCustNumber.Text = custToken & "-" & billpToken & "-" & cdToken

            Select Case servicetype
                Case "Electric"
                    strUnit = "KWH"
                Case "Water"
                    strUnit = "KGAL"
                Case "Gas"
                    strUnit = "CCF"
            End Select

            mvSummary.ActiveViewIndex = 1
            Dim rtnUsage As DataSet = cwservice.GetUsageInfo(encrypt, acctToken, acctpToken, "396", spa, spt, pt)
            gvMeterReads.DataSource = rtnUsage
            gvMeterReads.DataBind()

            Dim lblUnit As WebControls.Label
            Dim gvRow As GridViewRow
            For Each gvRow In gvMeterReads.Rows
                lblUnit = gvRow.Cells(0).FindControl("lblUnit")
                lblUnit.Text = strUnit
            Next

            gvMeterReads.Visible = True
        Catch
        End Try
    End Sub

End Class
