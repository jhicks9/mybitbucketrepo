<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Payments.aspx.vb" Inherits="CustomerService_Commission_Payments" title="Payments" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div><a id="top"></a></div>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnCheckDigit" runat="server" />
            <asp:MultiView ID="MultiView1" ActiveViewIndex="0" runat="server">
                <asp:View ID="vwTokens" runat="server">
                    <asp:DataList id="dlBillPkg" runat="server" skinId="dlistView" visible="False">
                        <ItemTemplate>
                            <b><asp:LinkButton id="LinkButton3"  font-bold="false" CommandArgument="statement" CommandName="<%# Container.DataItem %>" runat="server"><%#custToken%>-<%# Container.DataItem %>-<%=hdnCheckDigit.Value%></asp:LinkButton></b>
                        </ItemTemplate>
                    </asp:DataList>
                </asp:View>

                <asp:View ID="vwPayment" runat="server">
                    <div style="background-color:#36587a;height:40px;">
                        <div style="background-color:#36587a;color:#ffffff;padding: 10px 0 5px 10px;height:20px;float:left;"><asp:HyperLink ID="hlMenu1" runat="server" style="color:#ffffff;">Lookup additional information</asp:HyperLink></div>
                        <div style="background-color:#36587a;color:#ffffff;padding: 10px 10px 10px 0;height:20px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" style="color:#ffffff;"><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                    </div>                
                    <div id="print_content"><%--begin print content--%>
                        <div style="background-color:#cccccc;padding:5px 0% 0% 5px;">
                            <asp:Label ID="lblCustName" runat="server"></asp:Label><br />
                            <asp:Label ID="lblAccount" runat="server"></asp:Label><br />
                            <asp:Label ID="lblBalanceDue" runat="server"></asp:Label><br /><br />
                        </div>                    
                        
                        <div style="background-color:#cccccc;padding:0% 0% 5px 5px;">
                            <asp:GridView ID="dgPayments" runat="server" Width="100%" AutoGenerateColumns="False" CellSpacing="0" CellPadding="4" GridLines="None" ShowFooter="False" ShowHeader="True">
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="150px" DataField="PAYMENTDATE" HeaderStyle-HorizontalAlign="Left" HeaderText="Payment Received" SortExpression="PAYMENTDATE" />
                                    <asp:BoundField DataField="PAYMENTAMOUNT" HeaderStyle-HorizontalAlign="Left" HeaderText="Payment Amount" SortExpression="PAYMENTAMOUNT" />
                                </Columns>                
                                <EmptyDataTemplate>
                                    No information found.
                                </EmptyDataTemplate>
                                <RowStyle BackColor="#CCCCCC"/>
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <EmptyDataRowStyle BackColor="#cccccc" ForeColor="Maroon" />                            
                                <HeaderStyle BackColor="#AFAFAF" ForeColor="White"/>
                            </asp:GridView>
                            <br />
                        </div>
                    </div><%--end print content--%>
                    <div style="background-color:#36587a;color:#ffffff;height:40px;width:100%;">
                        <div style="padding:10px 0 10px 10px;"><asp:HyperLink ID="hlMenu2" runat="server" style="color:#ffffff;">Top</asp:HyperLink></div>
                    </div>

                </asp:View>
            </asp:MultiView> 
        </ContentTemplate>
    </asp:UpdatePanel>
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Payments","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>

