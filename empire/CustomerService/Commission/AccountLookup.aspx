<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AccountLookup.aspx.vb" Inherits="CustomerService_Commission_AccountLookup" title="Account Lookup" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            Information entered should match <b>exactly</b> as it appears on the statement. For example:
            <div id="gallery">
                <a id="A1" href="~/images/bill_sample_full_page.jpg" runat="server" title="Click to enlarge">
                <img id="imgSample" src="~/images/bill_sample_thumbnail.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
            </div>
            <br /><hr style="color:#113962;height:1px;" /><br />                
            <asp:Label ID="Label3" runat="server" Width="125px">Account Number:</asp:Label><br />
            <asp:TextBox ID="Textbox1" runat="server" Width="72px" MaxLength="6" ValidationGroup="valAccount"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Textbox1"
                        ErrorMessage="*" SetFocusOnError="True" Display="Dynamic" />-&nbsp;
            <asp:TextBox ID="Textbox2" runat="server" Width="25px" ValidationGroup="valAccount" MaxLength="2"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Textbox2"
                        ErrorMessage="*" SetFocusOnError="True" Display="Dynamic" />-&nbsp;
            <asp:TextBox ID="TextBox3" runat="server" Width="25px" ValidationGroup="valAccount" MaxLength="1"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="Textbox3"
                        ErrorMessage="*" SetFocusOnError="True" Display="Dynamic" />
                
            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="Textbox1"
                    ErrorMessage="Invalid Entry" Type="Integer" Operator="DataTypeCheck" SetFocusOnError="True">&nbsp;</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="Textbox2"
                    ErrorMessage="Invalid Entry" Type="Integer" Operator="DataTypeCheck" SetFocusOnError="True">&nbsp;</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="Textbox3"
                    ErrorMessage="Invalid Entry" Type="Integer" Operator="DataTypeCheck" SetFocusOnError="True">&nbsp;</asp:CompareValidator>
            <br />
            <asp:Label ID="Label4" Text="Name:" runat="server" Width="125px"></asp:Label><br />
            <asp:TextBox ID="txtName" runat="server" Width="165px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtName"
                        ErrorMessage="Field cannot be empty" SetFocusOnError="True">Name required</asp:RequiredFieldValidator>
            <br /><asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label><br />
            <div style="float:left;margin-left:95px;">
                <asp:LinkButton ID="lbContinue" runat="server" CssClass="ovalbutton" CausesValidation="true" OnClick="lbContinue_Click"><span>Continue</span></asp:LinkButton>
            </div>
            <br /><br />
        </ContentTemplate>
    </asp:UpdatePanel>
     
    <script type="text/javascript">jQuery("#gallery a").slimbox(); jQuery("#gallery2 a").slimbox();</script>
</asp:Content>