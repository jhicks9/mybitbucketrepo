Imports System.Data
Imports System
Imports System.Web
Imports System.Net
Imports System.io

Partial Class CustomerService_Commission_AccountSummary
    Inherits System.Web.UI.Page
    Dim custToken As String = ""
    Dim billpToken As String = ""
    Dim cdToken As String = ""
    Dim acctpToken As String = ""
    Dim acctToken As String = ""
    Dim servicetype As String = ""
    Dim custName As String = ""
    Dim custAddress As String = ""
    Dim encrypt As String
    Dim cwservice As New cw.Service
    Dim tdes As New tripleDES
    Dim cd As New checkDigit
    Public printurl As String = ""
    Dim _totalPaidDeposit As Decimal = 0.0
    Dim _totalUnpaidDeposit As Decimal = 0.0
    Dim _totalRefund As Decimal = 0.0
    Function AddDeposit(ByVal deposit As Object) As String
        Try
            If IsDBNull(deposit) = False Then
                _totalPaidDeposit += deposit
            Else
                deposit = ""
            End If
            Return deposit
        Catch
            Return ""
        End Try
    End Function
    Function GetDeposit() As String
        Return FormatCurrency(_totalPaidDeposit)
    End Function
    Function GetRefund() As String
        Return FormatCurrency(_totalRefund)
    End Function
    Function GetUnpaid() As String
        Return FormatCurrency(_totalUnpaidDeposit)
    End Function
    Function GetTotal() As String
        Return FormatCurrency(_totalPaidDeposit + _totalUnpaidDeposit)
    End Function
    Function AddRefund(ByVal refund As Object) As String
        Try
            If IsDBNull(refund) = False Then
                _totalRefund += refund
            Else
                refund = ""
            End If
            Return refund
        Catch
            Return ""
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            printurl = ResolveUrl("~/print.aspx")
            If Len(Request.QueryString("c")) < 1 Or Len(Request.QueryString("b")) < 1 Then
                Response.Redirect("~/CustomerService/Commission/AccountLookup.aspx")
            End If
            custToken = tdes.Decrypt(Request.QueryString("c"))
            billpToken = tdes.Decrypt(Request.QueryString("b"))
            cdToken = cd.CalculateCheckDigit(cd.ZeroPad(custToken & billpToken, 8))
            custName = Request.QueryString("n")

            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custToken)

            Dim rtnAcctTkns As New System.Data.DataSet
            rtnAcctTkns = cwservice.GetAccountTokens(encrypt, billpToken)
            dgAccountTokens.DataSource = rtnAcctTkns
            dgAccountTokens.DataBind()
            lblAccount.Text = "Select an account to view for " & custToken & "-" & billpToken & "-" & cdToken
            mvSummary.ActiveViewIndex = 0

            hlMenu1.NavigateUrl = "~/CustomerService/Commission/Default.aspx?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n"))
            hlMenu2.NavigateUrl = "#top"
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub dgAccountTokens_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgAccountTokens.RowCommand
        Try
            Dim eCmdArg As String = e.CommandArgument
            Dim arTokens As Array = Split(eCmdArg, ",")

            acctToken = arTokens(0)
            servicetype = arTokens(2)
            acctpToken = e.CommandName
            custAddress = arTokens(3) & " " & arTokens(6) & " " & arTokens(4) & "," & arTokens(5)
            BuildSummary()
        Catch
        End Try
    End Sub

    Public Sub BuildSummary()
        Try
            Dim build As String = ""
            Dim altrow As Boolean = True

            lblCustTkns.Text = cd.ZeroPad(custToken, 6) & "-" & billpToken & "-" & cdToken
            lblCustName.Text = StrConv(custName, VbStrConv.ProperCase)
            lblCoCustomer.Text = cwservice.GetCoCustomer(encrypt, billpToken)
            lblCustAddress.Text = custAddress
            lblDelinquentDate.Text = cwservice.GetDelinquentDate(encrypt, billpToken)
            lblTotalAccount.Text = String.Format("{0:c}", cwservice.GetAccountTotal(encrypt, acctToken, acctpToken))
            build = cwservice.GetArrearsBreakdown(encrypt, acctToken, acctpToken, billpToken)
            If Len(build) > 1 Then
                lblTotalCharges.Text = build.Split(";")(0).Trim
                lblUnstatemented.Text = build.Split(";")(1).Trim
                lblCurrentCharges.Text = build.Split(";")(2).Trim
                lblPastDue30.Text = build.Split(";")(3).Trim
                lbl30Arrears.Text = build.Split(";")(4).Trim
                lbl60Arrears.Text = build.Split(";")(5).Trim
                lbl90Arrears.Text = build.Split(";")(6).Trim
                lbl120Arrears.Text = build.Split(";")(7).Trim
                lblWriteoff.Text = build.Split(";")(8).Trim
            Else
                lblTotalCharges.Text = ""
                lblUnstatemented.Text = ""
                lblCurrentCharges.Text = ""
                lblPastDue30.Text = ""
                lbl30Arrears.Text = ""
                lbl60Arrears.Text = ""
                lbl90Arrears.Text = ""
                lbl120Arrears.Text = ""
                lblWriteoff.Text = ""
            End If

            build = cwservice.GetPaymentDate(encrypt, acctToken, acctpToken)
            If Len(build) > 1 Then
                lblLastPaymentDate.Text = build.Split(";")(0).Trim
                lblLastPaymentAmount.Text = build.Split(";")(1).Trim
            Else
                lblLastPaymentDate.Text = ""
                lblLastPaymentAmount.Text = ""
            End If
            lblAutoPay.Text = cwservice.GetAutoPayStatus(encrypt, billpToken) 'cwservice.GetOnAutopay(encrypt)
            lblAPP.Text = cwservice.GetOnAveragePayment(encrypt, acctToken, acctpToken)

            build = cwservice.GetAPData(encrypt, acctToken, acctpToken)
            If Len(build) > 1 Then
                lblServiceDate.Text = build.Split(";")(0).Trim
                lblCutDate.Text = build.Split(";")(1).Trim
                lblDNFollupDate.Text = build.Split(";")(2).Trim
                lblDNBalance.Text = build.Split(";")(3).Trim
                lblStatus.Text = build.Split(";")(4).Trim
            Else
                lblServiceDate.Text = ""
                lblCutDate.Text = ""
                lblDNFollupDate.Text = ""
                lblDNBalance.Text = ""
                lblStatus.Text = ""
            End If
            lblCheckAllowed.Text = cwservice.GetCheckAllowed(encrypt)
            lblBadChecks.Text = cwservice.GetReturnedCheck(encrypt)

            lblDNPDate.Text = cwservice.GetDNPDate(encrypt, acctToken, acctpToken)

            build = cwservice.GetCreditHistory(encrypt, acctToken, acctpToken)
            If Len(build) > 1 Then
                If build.Split(";")(0).Trim <> "0" Then
                    If altrow Then  'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Paid On Time<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(0).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Paid On Time<br />"
                        lblHistory.Text += build.Split(";")(0).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(1).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Not Paid On Time<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(1).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Not Paid On Time<br />"
                        lblHistory.Text += build.Split(";")(1).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(2).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Unpaid By Penalty<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(2).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Unpaid By Penalty<br />"
                        lblHistory.Text += build.Split(";")(2).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(3).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Eligible For Disconnect<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(3).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Eligible For Disconnect<br />"
                        lblHistory.Text += build.Split(";")(3).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(4).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Shut-Off Notice<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(4).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Shut-Off Notice<br />"
                        lblHistory.Text += build.Split(";")(4).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(5).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Nonpay Disc Service Order<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(5).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Nonpay Disc Service Order<br />"
                        lblHistory.Text += build.Split(";")(5).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(6).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Disconnect For Non-Pay<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(6).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Disconnect For Non-Pay<br />"
                        lblHistory.Text += build.Split(";")(6).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(7).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Complete NonPayDisconnect<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(7).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Complete NonPayDisconnect<br />"
                        lblHistory.Text += build.Split(";")(7).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(8).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Paid To Avoid Disconnect<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(8).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Paid To Avoid Disconnect<br />"
                        lblHistory.Text += build.Split(";")(8).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(9).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Collection Notice<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(9).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Collection Notice<br />"
                        lblHistory.Text += build.Split(";")(9).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(10).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Eligible For Write-Off<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(10).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Eligible For Write-Off<br />"
                        lblHistory.Text += build.Split(";")(10).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(11).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Write-Off<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(11).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Write-Off<br />"
                        lblHistory.Text += build.Split(";")(11).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(12).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Write-Off Reinstated<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(12).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Write-Off Reinstated<br />"
                        lblHistory.Text += build.Split(";")(12).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(13).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Assign Agency<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(13).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Assign Agency<br />"
                        lblHistory.Text += build.Split(";")(13).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(14).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Bankruptcy<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(14).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Bankruptcy<br />"
                        lblHistory.Text += build.Split(";")(14).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
                If build.Split(";")(15).Trim <> "0" Then
                    If altrow Then 'alternate colors
                        lbl_History.Text += "<div style='background-color:#dcdcdc;'>Bankruptcy Dismissal<br /></div>"
                        lblHistory.Text += "<div style='background-color:#dcdcdc;'>" & build.Split(";")(15).Trim & "<br /></div>"
                    Else
                        lbl_History.Text += "Bankruptcy Dismissal<br />"
                        lblHistory.Text += build.Split(";")(15).Trim & "<br />"
                    End If
                    altrow = Not altrow
                End If
            End If

            _totalUnpaidDeposit = CDec(cwservice.GetUnpaidDepositInfo(encrypt, acctToken, acctpToken))
            Dim rtnDataset As DataSet = cwservice.GetDepositInfo(encrypt, acctToken, acctpToken)
            gvDepositInfo.DataSource = rtnDataset
            gvDepositInfo.DataBind()
            gvDepositInfo.Visible = True

            Dim ds As DataSet = cwservice.GetPaymentContractInfo(encrypt, acctToken, acctpToken)
            gvContractInfo.DataSource = ds
            gvContractInfo.DataBind()
            gvContractInfo.Visible = True

            mvSummary.ActiveViewIndex = 1
        Catch
        End Try
    End Sub
End Class
