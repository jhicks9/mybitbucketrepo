<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="MeterReads.aspx.vb" Inherits="CustomerService_Commission_MeterReads" title="Meter Reads" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div><a id="top"></a></div>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvSummary">    
            <asp:View ID="vwAccounts" runat="server">
                <asp:Label ID="lblAccount" runat="server"></asp:Label><br /><br />
                <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList" AllowPaging="false">
                    <Columns>
                        <asp:TemplateField>
                           <ItemTemplate>
                                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE") & "," & Eval("service_point_tkn") & "," & Eval("premise_tkn")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                   Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                           </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No information found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:View>        
        
            <asp:View ID="vwSummary" runat="server"> 
                <div style="background-color:#36587a;height:40px;">
                    <div style="background-color:#36587a;color:#ffffff;padding: 10px 0 5px 10px;height:20px;float:left;"><asp:HyperLink ID="hlMenu1" runat="server" style="color:#ffffff;">Lookup additional information</asp:HyperLink></div>
                    <div style="background-color:#36587a;color:#ffffff;padding: 10px 10px 10px 0;height:20px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" style="color:#ffffff;"><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                </div>                
                <div id="print_content"><%--begin print content--%>
                    <div style="background-color:#cccccc;padding:5px 0% 0% 5px;">
                        <asp:Label ID="lblCustNumber" runat="server"></asp:Label><br />
                        <asp:Label ID="lblCustName" runat="server"></asp:Label><br />
                        <asp:Label ID="lblCustAddress" runat="server" ></asp:Label><br />
                        <br />
                    </div>                    
                    <div style="background-color:#cccccc;padding:0% 0% 5px 5px;">
                        <asp:GridView ID="gvMeterReads" runat="server" AutoGenerateColumns="False" CellSpacing="0" CellPadding="4" GridLines="None" ShowFooter="False" ShowHeader="True" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="READ_DATE" HeaderText="Read Date" SortExpression="READ_DATE" />
                                <asp:BoundField DataField="USAGE_READ_NUM" HeaderText="Meter Read" SortExpression="USAGE_READ_NUM" />
                                <asp:BoundField DataField="ESTIMATE_IND" HeaderText="Estimate" SortExpression="ESTIMATE_IND" />
                                <asp:BoundField DataField="USAGE_NUM" HeaderText="Usage Amount" SortExpression="USAGE_NUM" />
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Unit
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnit" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="USAGE_DAYS_CNT" HeaderText="Usage Days" SortExpression="USAGE_DAYS_CNT" />                            
                            </Columns>
                            <EmptyDataTemplate>
                                No information found.
                            </EmptyDataTemplate>
                            <RowStyle BackColor="#CCCCCC" HorizontalAlign="Left"/>
                            <AlternatingRowStyle BackColor="Gainsboro" HorizontalAlign="Left"/>
                            <EmptyDataRowStyle BackColor="#cccccc" ForeColor="Maroon" />                            
                            <HeaderStyle BackColor="#AFAFAF" ForeColor="White" HorizontalAlign="Left"/>
                        </asp:GridView>  
                    </div>
                </div><%--end print content--%>
                <div style="background-color:#36587a;color:#ffffff;height:40px;width:100%;">
                    <div style="padding:10px 0 10px 10px;"><asp:HyperLink ID="hlMenu2" runat="server" style="color:#ffffff;">Top</asp:HyperLink></div>
                </div>
            </asp:View>                    
        </asp:MultiView>             
    </ContentTemplate>
    </asp:UpdatePanel> 
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Meter Reads","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>