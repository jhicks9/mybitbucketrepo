Imports System.Net
Partial Class CustomerService_Commission_AccountLookup
    Inherits System.Web.UI.Page
    Dim cwservice As New cw.Service
    Dim encrypt_custToken As String
    Dim encrypt_billpToken As String
    Dim tdes As New tripleDES

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        colorbox.Attributes.Add("type", "text/javascript")
        colorbox.TagName = "script"
        colorbox.Attributes.Add("src", ResolveUrl("~/js/slimbox2.js"))
        Page.Header.Controls.Add(colorbox)

        Dim css As HtmlLink = New HtmlLink
        css.Href = ResolveUrl("~/slimbox2.css")
        css.Attributes("rel") = "stylesheet"
        css.Attributes("type") = "text/css"
        css.Attributes("media") = "all"
        Page.Header.Controls.Add(css)

        Dim autotab As HtmlGenericControl = New HtmlGenericControl
        autotab.Attributes.Add("type", "text/javascript")
        autotab.TagName = "script"
        autotab.Attributes.Add("src", ResolveUrl("~/js/autotab.js"))
        Page.Header.Controls.Add(autotab)
    End Sub

    Protected Sub lbContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cwservice.Credentials = CredentialCache.DefaultCredentials
            encrypt_custToken = tdes.Encrypt(Textbox1.Text)
            Dim strBillPkg As String = Trim(Textbox2.Text)
            If strBillPkg.Length = 1 Then
                strBillPkg = "0" & strBillPkg
            End If
            Dim status As String = cwservice.ValidateCustomer(encrypt_custToken, strBillPkg, txtName.Text, "N/A")

            If status = 1 Then
                encrypt_billpToken = tdes.Encrypt(strBillPkg)
                Response.Redirect("~/CustomerService/Commission/AccountSummary.aspx?c=" & Server.UrlEncode(encrypt_custToken) & "&b=" & Server.UrlEncode(encrypt_billpToken) & "&n=" & Server.UrlDecode(txtName.Text))
            ElseIf status = 0 Then
                lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Text = "Cannot find user. Please verify account information."
                lblMessage.Visible = True
            ElseIf status = 2 Then
                lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Text = "Unable to validate account. Please contact Customer Service at 800-206-2300."
                lblMessage.Visible = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Textbox1.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
            Textbox2.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
            TextBox3.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")
            txtName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbContinue.UniqueID + "','')")

            Textbox1.Attributes("onkeypress") = "return recPreKey(event, this)"
            Textbox1.Attributes("onkeyup") = "onFull(event, this)"
            Textbox2.Attributes("onkeypress") = "return recPreKey(event, this)"
            Textbox2.Attributes("onkeyup") = "onFull(event, this)"
            TextBox3.Attributes("onkeypress") = "return recPreKey(event, this)"
            TextBox3.Attributes("onkeyup") = "onFull(event, this)"

            Textbox1.Focus()
        Catch
        End Try
    End Sub
End Class
