<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="AccountSummary.aspx.vb" Inherits="CustomerService_Commission_AccountSummary" title="Account Summary" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div><a id="top"></a></div>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvSummary">
            <asp:View ID="vwAccounts" runat="server">
                <br /><asp:Label ID="lblAccount" runat="server"></asp:Label><br /><br />
                <asp:GridView ID="dgAccountTokens" runat="server" SkinID="GridViewList" AllowPaging="false">
                    <Columns>
                        <asp:TemplateField>
                           <ItemTemplate>
                                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("customer_acct_tkn") & "," & Eval("srvce_pnt_assn_tkn") & "," & eval("account_pkg_desc") & "," & Eval("LN1_ADDR") & "," & Eval("CITY_NAME") & "," & Eval("STATE_CODE") & "," & Eval("UNIT_CODE")%>' CommandName='<%#Eval("account_pkg_tkn") %>'
                                   Text='<%# eval("account_pkg_desc") & " - " & eval("customer_tkn") & "-" & eval("customer_acct_tkn") & "-" & Eval("account_pkg_tkn") &  "&nbsp;&nbsp;&nbsp;For service at " & Eval("LN1_ADDR") & "&nbsp;" & Eval("UNIT_CODE") & "&nbsp;&nbsp;" & Eval("CITY_NAME") & ", " & Eval("STATE_CODE") %>'> </asp:LinkButton>
                           </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No information found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:View>
            
            <asp:View ID="vwSummary" runat="server"> 
                <div style="background-color:#36587a;height:40px;">
                    <div style="background-color:#36587a;color:#ffffff;padding:10px 0 5px 10px;height:20px;float:left;"><asp:HyperLink ID="hlMenu1" runat="server" style="color:#ffffff;">Lookup additional information</asp:HyperLink></div>
                    <div style="background-color:#36587a;color:#ffffff;padding:10px 10px 10px 0;height:20px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" style="color:#ffffff;"><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.gif" runat="server" /> Printer Friendly Version</a></div>
                </div>
                <div style="background-color:#cccccc;line-height:20px;">
                    <div id="print_content"><%--begin print content--%>
                        <div style="float:left;padding:0 0 0 5px;width:225px;line-height:20px;">
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_CustTkns" runat="server" Text="Customer Number"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_CustName" runat="server" Text="Customer"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_CoCustomer" runat="server" Text="CoCustomer"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_CustAddress" runat="server" Text="Address"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_Status" runat="server" Text="Status"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_DelinquentDate" runat="server" Text="Delinquent Date"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_TotalAccount" runat="server" Text="Total of Account"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_TotalCharges" runat="server" Text="Total Charges"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_Unstatemented" runat="server" Text="Unstatemented Charges"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_CurrentCharges" runat="server" Text="Current Charges"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_PastDue30" runat="server" Text="Past Due < 30"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_30Arrears" runat="server" Text="30 Day Arrears"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_60Arrears" runat="server" Text="60 Day Arrears"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_90Arrears" runat="server" Text="90 Day Arrears"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_120Arrears" runat="server" Text="120 Day Arrears"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_Writeoff" runat="server" Text="Write-Off / Bankruptcy"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_LastPaymentDate" runat="server" Text="Last Payment Date"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_LastPaymentAmount" runat="server" Text="Last Payment Amount"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_AutoPay" runat="server" Text="Is On AutoPay"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_AvgPay" runat="server" Text="Is On Average Payment Plan"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_ServiceDate" runat="server" Text="Date Came On Service"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_CutDate" runat="server" Text="Cut Order Date"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_DNFollupDate" runat="server" Text="Disconnect Followup Date"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_DNBalance" runat="server" Text="Disconnect Balance Amt"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl_CheckAllowed" runat="server" Text="Check Allowed"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_BadChecks" runat="server" Text="Returned Checks In Last Year"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl_DNPDate" runat="server" Text="Disconnect order date"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                        </div>
                        <div style="padding:0 0 0 0;margin-left:230px;line-height:20px;">
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblCustTkns" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblCustName" runat="server"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblCoCustomer" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblCustAddress" runat="server"></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblStatus" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblDelinquentDate" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblTotalAccount" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblTotalCharges" runat="server"></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblUnstatemented" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblCurrentCharges" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblPastDue30" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl30Arrears" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl60Arrears" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lbl90Arrears" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lbl120Arrears" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblWriteoff" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblLastPaymentDate" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblLastPaymentAmount" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblAutoPay" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblAPP" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblServiceDate" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblCutDate" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblDNFollupDate" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblDNBalance" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><asp:Label ID="lblCheckAllowed" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblBadChecks" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                            <div style="background-color:#dcdcdc;"><asp:Label ID="lblDNPDate" runat="server" ></asp:Label><br /></div>
                            <div style="background-color:#cccccc;"><br /></div>
                        </div>
                    
                        <div style="clear:both;background-color:#B0C4DE;padding:0% 0% 0% 1%;width:99%">12 Month Credit History</div>
                        <div style="float:left;background-color:#cccccc;padding:0 0 0 5px;width:225px;">
                            <asp:Label ID="lbl_History" runat="server" Text=""></asp:Label>
                        </div>
                        <div style="background-color:#cccccc;padding:0% 0% 0% 0%;margin-left:230px;">
                            <asp:Label ID="lblHistory" runat="server" Text=""></asp:Label>
                        </div>
                        
                        <div style="clear:both;background-color:#cccccc;">&nbsp;</div>
                        <div style="clear:both;background-color:#B0C4DE;padding:0% 0% 0% 1%;width:99%">Deposit Information</div>                                            
                        <div style="clear:both;background-color:#cccccc;">    
                            <asp:GridView ID="gvDepositInfo" runat="server" AutoGenerateColumns="False" Width="100%" CellPadding="4" GridLines="None" ShowFooter="True" ShowHeader="True">
                                <Columns>
                                <%--<asp:BoundField DataField="Type_Desc" HeaderText="Description" SortExpression="Type_Desc" />--%>
                                <asp:TemplateField HeaderText="Description" FooterStyle-Font-Bold="True">
                                    <ItemTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("Type_Desc")%>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPaid" runat="server" Text="Paid"></asp:Label><br />
                                        <asp:Label ID="lblUnpaid" runat="server" Text="Unpaid"></asp:Label><br />
                                        <asp:Label ID="lblTotal" runat="server" Text="Total Deposit"></asp:Label>
                                    </FooterTemplate>                                    
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="ga" HeaderText="Amount" SortExpression="Guaranty_Amt" />--%>
                                <asp:TemplateField HeaderText="Amount" FooterStyle-Font-Bold="True">
                                    <ItemTemplate>
                                    <asp:Label ID="lblDepAmount" runat="server" Text='<%#AddDeposit( DataBinder.Eval(Container.DataItem,"ga") ) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblDepositPaid" runat="server" Text='<%#GetDeposit().ToString%>'></asp:Label><br />
                                        <asp:Label ID="lblDepositUnpaid" runat="server" Text='<%#GetUnpaid().ToString%>'></asp:Label><br />
                                        <asp:Label ID="lblDepositTotal" runat="server" Text='<%#GetTotal().ToString%>'></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="gpd" HeaderText="Date" SortExpression="Guaranty_Pd_Date" />
                                <asp:BoundField DataField="Paid_In_Full_Ind" HeaderText="Paid in Full" SortExpression="Paid_In_Full_Ind" />
                                <asp:BoundField DataField="pfd" HeaderText="Paid in Full Date" SortExpression="Paid_In_Full_Date" />
                                <%--<asp:BoundField DataField="ra" HeaderText="Refund" SortExpression="Refund_Amt" />--%>
                                <asp:TemplateField HeaderText="Refund" FooterStyle-Font-Bold="True">
                                    <ItemTemplate>
                                    <asp:Label ID="lblRefAmount" runat="server" Text='<%# AddRefund(DataBinder.Eval(Container.DataItem,"ra")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblRefundTotal" runat="server" Text='<%#GetRefund().ToString %>'></asp:Label><br />
                                        <asp:Label ID="lblHolder1" runat="server" Text="&nbsp;"></asp:Label><br />
                                        <asp:Label ID="lblHolder2" runat="server" Text="&nbsp;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>                                
                                <asp:BoundField DataField="rd" HeaderText="Refund Date" SortExpression="Refund_Date" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No information found.
                                </EmptyDataTemplate>
                                <RowStyle BackColor="#CCCCCC" />
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <EmptyDataRowStyle BackColor="#cccccc" ForeColor="Maroon" />                            
                                <HeaderStyle BackColor="#AFAFAF" Font-Names="Arial" Font-Bold="False" Font-Size="12px" ForeColor="White"/>
                                <FooterStyle BackColor="#AFAFAF" ForeColor="White" />
                            </asp:GridView>
                            <div style="background-color:#AFAFAF;color:#000000;">
                                <asp:Label ID="lblTotals" runat="server" Text=""></asp:Label></div>
                        </div>
                    
                        <div style="clear:both;background-color:#cccccc;">&nbsp;</div>
                        <div style="clear:both;background-color:#B0C4DE;padding:0% 0% 0% 1%;width:99%">Deferred Payment Contract Information</div>
                        <div style="clear:both;background-color:#cccccc;">                        
                            <asp:GridView ID="gvContractInfo" runat="server" AutoGenerateColumns="False" Width="100%" CellPadding="4" GridLines="None" ShowFooter="False" ShowHeader="True">
                                <Columns>
                                <asp:BoundField DataField="pa" HeaderText="Remaining Amount" SortExpression="principal_amt" />
                                <asp:BoundField DataField="ia" HeaderText="Installment Amount" SortExpression="installment_amt" />
                                <asp:BoundField DataField="installment_qty" HeaderText="Installment Qty" SortExpression="installment_qty" />
                                <asp:BoundField DataField="type_desc" HeaderText="Description" SortExpression="type_desc" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No information found.
                                </EmptyDataTemplate>
                                <RowStyle BackColor="#CCCCCC" />
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <EmptyDataRowStyle BackColor="#cccccc" ForeColor="Maroon" />                            
                                <HeaderStyle BackColor="#AFAFAF" Font-Names="Arial" Font-Bold="False" Font-Size="12px" ForeColor="White"/>
                            </asp:GridView>                    
                        </div>
                    </div><%--end print content--%>
                </div>
                <div style="background-color:#36587a;color:#ffffff;height:40px;width:100%;">
                    <div style="padding:10px 0 10px 10px;"><asp:HyperLink ID="hlMenu2" runat="server" style="color:#ffffff;">Top</asp:HyperLink></div>
                </div>
            </asp:View>
        </asp:MultiView>  
        </ContentTemplate>       
    </asp:UpdatePanel> 
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("<%=printurl %>?t=Account Summary","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>