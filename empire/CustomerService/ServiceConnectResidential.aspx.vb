﻿
Partial Class CustomerService_ServiceConnectResidential
    Inherits System.Web.UI.Page

    Protected Sub lbCustomerInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlService.Visible = False
        pnlCustomerInfo.Visible = True
    End Sub

    Protected Sub lbServicePrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlCustomerInfo.Visible = False
        pnlService.Visible = True
    End Sub

    Protected Sub lbJointCustomerInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlCustomerInfo.Visible = False
        If rblJointCustomer.SelectedItem.Text = "Yes" Then
            lbSecondJointCustomerInfoNext.CausesValidation = True
            pnlJointCustomerInfo.Visible = True
        Else
            pnlServiceInfo.Visible = True
        End If
    End Sub

    Protected Sub lbCustomerInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlJointCustomerInfo.Visible = False
        pnlCustomerInfo.Visible = True
    End Sub

    Protected Sub lbSecondJointCustomerInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlJointCustomerInfo.Visible = False
        If rblSecondJointCustomer.SelectedItem.Text = "Yes" Then
            pnlSecondJointCustomerInfo.Visible = True
        Else
            pnlServiceInfo.Visible = True
        End If
    End Sub

    Protected Sub lbSecondJointCustomerInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlServiceInfo.Visible = False
        If rblSecondJointCustomer.SelectedIndex >= 0 Then
            If rblSecondJointCustomer.SelectedItem.Text = "Yes" Then
                pnlSecondJointCustomerInfo.Visible = True
            Else
                If rblJointCustomer.SelectedIndex >= 0 Then
                    If rblJointCustomer.SelectedItem.Text = "Yes" Then
                        pnlJointCustomerInfo.Visible = True
                    Else
                        pnlCustomerInfo.Visible = True
                    End If
                Else
                    pnlCustomerInfo.Visible = True
                End If
            End If
        Else
            pnlCustomerInfo.Visible = True
        End If
    End Sub

    Protected Sub lbServiceInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlSecondJointCustomerInfo.Visible = False
        pnlServiceInfo.Visible = True
    End Sub

    Protected Sub lbJointCustomerInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlSecondJointCustomerInfo.Visible = False
        pnlJointCustomerInfo.Visible = True
    End Sub

    Protected Sub btnDuplicateAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuplicateAddress.Click
        txtMailingAddress.Text = txtServiceAddress.Text
        txtMailingAddressAN.Text = txtServiceAddressAN.Text
        txtMailingAddressLN.Text = txtServiceAddressLN.Text
        txtMailingAddressCity.Text = txtServiceAddressCity.Text
        txtMailingAddressState.Text = txtServiceAddressState.Text
        txtMailingAddressZip.Text = txtServiceAddressZip.Text
    End Sub

    Protected Sub lbServiceInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlAdditionalInfo.Visible = False
        pnlServiceInfo.Visible = True

    End Sub

    Protected Sub lbContactInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlServiceInfo.Visible = False
        pnlAdditionalInfo.Visible = True
    End Sub

    Protected Sub lbConstructionInfoNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlAdditionalInfo.Visible = False
        If rblNewConstruction.SelectedItem.Text = "Yes" Then
            pnlConstructionInfo.Visible = True
        Else
            pnlFinalize.Visible = True
        End If
    End Sub

    Protected Sub lbAdditionalInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlConstructionInfo.Visible = False
        pnlAdditionalInfo.Visible = True
    End Sub

    Protected Sub lbConstructionInfoPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlFinalize.Visible = False
        If rblNewConstruction.SelectedItem.Text = "Yes" Then
            pnlConstructionInfo.Visible = True
        Else
            pnlAdditionalInfo.Visible = True
        End If
    End Sub

    Protected Sub lbFinalizeNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlConstructionInfo.Visible = False
        pnlFinalize.Visible = True
    End Sub

    Protected Sub lbSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MailObj As New System.Net.Mail.SmtpClient
        Dim message As New System.Net.Mail.MailMessage()
        Dim fromAddress As New System.Net.Mail.MailAddress("admin@empiredistrict.com")
        Dim strEmailBody As String = "Service Connect Request Form" & vbCrLf & vbCrLf
        Try
            lbSubmit.Enabled = False  'eliminate double clicking the submit button
            message.From = fromAddress
            If rblNewConstruction.SelectedItem.Text = "Yes" Then
                message.To.Add("construction.design@empiredistrict.com")
                message.CC.Add("customer.accounts@empiredistrict.com")
            Else
                message.To.Add("customer.accounts@empiredistrict.com")
            End If
            message.Subject = "Service Connect Request Form"
            'MailObj.Host = "" -- obtained from web.config 03/14/2012

            strEmailBody += "Service: " & hdnServiceType.Value.ToString & vbCrLf
            strEmailBody += "First Name: " & txtFirstName.Text & vbCrLf
            strEmailBody += "Middle Name or Initial: " & txtMiddleName.Text & vbCrLf
            strEmailBody += "Last Name: " & txtLastName.Text & vbCrLf
            strEmailBody += "Suffix: " & cboSuffix.SelectedItem.Text & vbCrLf
            strEmailBody += "Date of Birth: " & txtBirthDate.Text & vbCrLf
            strEmailBody += "Primary Phone: (" & txtPrimaryPhone1.Text & ")" & txtPrimaryPhone2.Text & "-" & txtPrimaryPhone3.Text & vbCrLf
            strEmailBody += "Cell Phone: (" & txtCellPhone1.Text & ")" & txtCellPhone2.Text & "-" & txtCellPhone3.Text & vbCrLf
            strEmailBody += "Work Phone: (" & txtWorkPhone1.Text & ")" & txtWorkPhone2.Text & "-" & txtWorkPhone3.Text & vbCrLf
            strEmailBody += "Social Security Number: " & txtSSN1.Text & "-" & txtSSN2.Text & "-" & txtSSN3.Text & vbCrLf
            strEmailBody += "Email address: " & txtEmail.Text & vbCrLf
            strEmailBody += "Occupation: " & txtOccupation.Text & vbCrLf
            strEmailBody += "Employer: " & txtEmployer.Text & vbCrLf
            If rboEmployerTime.SelectedIndex >= 0 Then
                strEmailBody += "Time employed: " & rboEmployerTime.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Time employed: " & vbCrLf
            End If
            strEmailBody += "Driver's License/State Issued ID State: " & ddlState.SelectedItem.Text & vbCrLf
            strEmailBody += "Driver's License/State Issued ID Number: " & txtDL.Text & vbCrLf
            strEmailBody += "Last Residential Address: " & txtLastResidentAddress.Text & vbCrLf
            strEmailBody += "Last Residential City: " & txtLastResidentCity.Text & ", " & txtResidentialAddressState.Text & " " & txtResidentialAddressZip.Text & vbCrLf
            strEmailBody += vbCrLf
            If rblJointCustomerInfo1.SelectedIndex >= 0 Then
                strEmailBody += "Joint Customer Relation: " & rblJointCustomerInfo1.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Joint Customer Relation: " & vbCrLf
            End If
            strEmailBody += "Joint Customer First Name: " & txtFirstName1.Text & vbCrLf
            strEmailBody += "Joint Customer Middle Name or Initial: " & txtMiddleName1.Text & vbCrLf
            strEmailBody += "Joint Customer Last Name: " & txtLastName1.Text & vbCrLf
            strEmailBody += "Joint Customer Suffix: " & cboSuffix1.SelectedItem.Text & vbCrLf
            strEmailBody += "Joint Customer Date of Birth: " & txtBirthDate1.Text & vbCrLf
            strEmailBody += "Joint Customer Primary Phone: (" & txtPrimaryPhone1_1.Text & ")" & txtPrimaryPhone2_1.Text & "-" & txtPrimaryPhone3_1.Text & vbCrLf
            strEmailBody += "Joint Customer Cell Phone: (" & txtCellPhone1_1.Text & ")" & txtCellPhone2_1.Text & "-" & txtCellPhone3_1.Text & vbCrLf
            strEmailBody += "Joint Customer Work Phone: (" & txtWorkPhone1_1.Text & ")" & txtWorkPhone2_1.Text & "-" & txtWorkPhone3_1.Text & vbCrLf
            strEmailBody += "Joint Customer Social Security Number: " & txtSSN1_1.Text & "-" & txtSSN2_1.Text & "-" & txtSSN3_1.Text & vbCrLf
            strEmailBody += "Joint Customer Email address: " & txtEmail1.Text & vbCrLf
            strEmailBody += "Joint Customer Occupation: " & txtOccupation1.Text & vbCrLf
            strEmailBody += "Joint Customer Employer: " & txtEmployer1.Text & vbCrLf
            If rboEmployerTime1.SelectedIndex >= 0 Then
                strEmailBody += "Joint Customer Time employed: " & rboEmployerTime1.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Joint Customer Time employed: " & vbCrLf
            End If
            strEmailBody += "Joint Customer Driver's License/State Issued ID State: " & ddlState1.SelectedItem.Text & vbCrLf
            strEmailBody += "Joint Customer Driver's License/State Issued ID Number: " & txtDL1.Text & vbCrLf
            strEmailBody += "Joint Customer Last Residential Address: " & txtLastResidentAddress1.Text & vbCrLf
            strEmailBody += "Joint Customer Last Residential City: " & txtLastResidentCity1.Text & ", " & txtResidentialAddressState1.Text & " " & txtResidentialAddressZip1.Text & vbCrLf
            strEmailBody += vbCrLf
            If rblJointCustomerInfo2.SelectedIndex >= 0 Then
                strEmailBody += "Second Joint Customer Relation: " & rblJointCustomerInfo2.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Second Joint Customer Relation: " & vbCrLf
            End If
            strEmailBody += "Second Joint Customer First Name: " & txtFirstName2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Middle Name or Initial: " & txtMiddleName2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Last Name: " & txtLastName2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Suffix: " & cboSuffix2.SelectedItem.Text & vbCrLf
            strEmailBody += "Second Joint Customer Date of Birth: " & txtBirthDate2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Primary Phone: (" & txtPrimaryPhone1_2.Text & ")" & txtPrimaryPhone2_2.Text & "-" & txtPrimaryPhone3_2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Cell Phone: (" & txtCellPhone1_2.Text & ")" & txtCellPhone2_2.Text & "-" & txtCellPhone3_2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Work Phone: (" & txtWorkPhone1_2.Text & ")" & txtWorkPhone2_2.Text & "-" & txtWorkPhone3_2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Social Security Number: " & txtSSN1_2.Text & "-" & txtSSN2_2.Text & "-" & txtSSN3_2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Email address: " & txtEmail2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Employee/Occupation: " & txtOccupation2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Employer: " & txtEmployer2.Text & vbCrLf
            If rboEmployerTime2.SelectedIndex >= 0 Then
                strEmailBody += "Second Joint Customer Occupation time: " & rboEmployerTime2.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Second Joint Customer Occupation time: " & vbCrLf
            End If
            strEmailBody += "Second Joint Customer Driver's License/State Issued ID State: " & ddlState2.SelectedItem.Text & vbCrLf
            strEmailBody += "Second Joint Customer Driver's License/State Issued ID Number: " & txtDL2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Last Residential Address: " & txtLastResidentAddress2.Text & vbCrLf
            strEmailBody += "Second Joint Customer Last Residential City: " & txtLastResidentCity2.Text & ", " & txtResidentialAddressState2.Text & " " & txtResidentialAddressZip2.Text & vbCrLf
            strEmailBody += vbCrLf
            If rblPreviousAccount.SelectedIndex >= 0 Then
                strEmailBody += "Previous Empire Account: " & rblPreviousAccount.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Previous Empire Account: " & vbCrLf
            End If
            If rblRentBuy.SelectedIndex >= 0 Then
                strEmailBody += "Rent or Buy: " & rblRentBuy.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Rent or Buy: " & vbCrLf
            End If
            strEmailBody += "Service Address: " & txtServiceAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtServiceAddressAN.Text & " Lot Number: " & txtServiceAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtServiceAddressCity.Text & ", " & txtServiceAddressState.Text & " " & txtServiceAddressZip.Text & vbCrLf
            strEmailBody += "Mailing Address: " & txtMailingAddress.Text & vbCrLf
            strEmailBody += "Apartment Number: " & txtMailingAddressAN.Text & " Lot Number: " & txtMailingAddressLN.Text & vbCrLf
            strEmailBody += "City: " & txtMailingAddressCity.Text & ", " & txtMailingAddressState.Text & " " & txtMailingAddressZip.Text & vbCrLf
            strEmailBody += "Service Start Date: " & txtServiceStartDate.Text & vbCrLf
            strEmailBody += "Emergency Contact Name: " & txtEmergencyContactName.Text & vbCrLf
            strEmailBody += "Emergency Contact Phone: (" & txtEmergencyContactPhone1.Text & ")" & txtEmergencyContactPhone2.Text & "-" & txtEmergencyContactPhone3.Text & vbCrLf
            strEmailBody += "Emergency Contact Relationship: " & txtEmergencyContactRelationship.Text & vbCrLf
            strEmailBody += vbCrLf
            If rblContactMethod.SelectedIndex >= 0 Then
                strEmailBody += "Contact Method: " & rblContactMethod.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Contact Method: " & vbCrLf
            End If
            strEmailBody += "Additional Information: " & txtInformation.Text & vbCrLf & vbCrLf

            strEmailBody += "Construction Contact Name: " & txtCDContactName.Text & vbCrLf
            strEmailBody += "Construction Contact Type: " & ddlCDContactType.SelectedItem.Text & vbCrLf
            strEmailBody += "Construction Contact Phone: " & "(" & txtCDContactPhone1.Text & ")" & txtCDContactPhone2.Text & "-" & txtCDContactPhone3.Text & vbCrLf
            If rblCDServiceType.SelectedIndex >= 0 Then
                strEmailBody += "Construction Service Type: " & rblCDServiceType.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction Service Type: " & vbCrLf
            End If
            If rblServiceSize.SelectedIndex >= 0 Then
                strEmailBody += "Construction Service Size: " & rblServiceSize.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction Service Size: " & vbCrLf
            End If
            strEmailBody += "Construction Home Type: " & ddlCDHomeType.SelectedItem.Text & vbCrLf
            If rblCDElectricHeat.SelectedIndex >= 0 Then
                strEmailBody += "Construction Electric Heat: " & rblCDElectricHeat.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction Electric Heat: " & vbCrLf
            End If
            If rblCDCooled.SelectedIndex >= 0 Then
                strEmailBody += "Construction Cooled: " & rblCDCooled.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction Cooled: " & vbCrLf
            End If
            strEmailBody += "Construction Square Foot: " & txtCDSquareFoot.Text & vbCrLf
            If rblCDBackupGeneration.SelectedIndex >= 0 Then
                strEmailBody += "Construction permanent back-up generation: " & rblCDBackupGeneration.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction permanent back-up generation: " & vbCrLf
            End If
            If rblCDGenerator.SelectedIndex >= 0 Then
                strEmailBody += "Construction standby generator: " & rblCDGenerator.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction standby generator: " & vbCrLf
            End If
            If rblCDTempService.SelectedIndex >= 0 Then
                strEmailBody += "Construction temporary service needed: " & rblCDTempService.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction temporary service needed: " & vbCrLf
            End If
            If rblCDSiteClear.SelectedIndex >= 0 Then
                strEmailBody += "Construction site clearing completed: " & rblCDSiteClear.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction site clearing completed: " & vbCrLf
            End If
            If rblCDFinalGrade.SelectedIndex >= 0 Then
                strEmailBody += "Construction lot within 6in of final grade: " & rblCDFinalGrade.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction lot within 6in of final grade: " & vbCrLf
            End If
            If rblCDBuildingStaked.SelectedIndex >= 0 Then
                strEmailBody += "Construction location of building staked: " & rblCDBuildingStaked.SelectedItem.Text & vbCrLf
            Else
                strEmailBody += "Construction location of building staked: " & vbCrLf
            End If
            strEmailBody += "Construction Additional Information: " & txtCDAdditionalInformation.Text & vbCrLf

            message.Body = strEmailBody
            MailObj.Send(message)

            lblStatus.Text = "<b>** Email Sent ** <br />Thank you for submitting a service connect request.</b>"
            lbConstructionInfoPrev.Visible = False
            lbSubmit.Visible = False
        Catch ex As Exception
            lblStatus.Text = ex.Message
            lbSubmit.Enabled = True
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtPrimaryPhone1.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone1.ClientID + ", " + txtPrimaryPhone2.ClientID + ")"
        txtPrimaryPhone2.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone2.ClientID + ", " + txtPrimaryPhone3.ClientID + ")"
        txtPrimaryPhone3.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone3.ClientID + ", " + txtCellPhone1.ClientID + ")"
        txtCellPhone1.Attributes("onkeyup") = "autotab(" + txtCellPhone1.ClientID + ", " + txtCellPhone2.ClientID + ")"
        txtCellPhone2.Attributes("onkeyup") = "autotab(" + txtCellPhone2.ClientID + ", " + txtCellPhone3.ClientID + ")"
        txtCellPhone3.Attributes("onkeyup") = "autotab(" + txtCellPhone3.ClientID + ", " + txtWorkPhone1.ClientID + ")"
        txtWorkPhone1.Attributes("onkeyup") = "autotab(" + txtWorkPhone1.ClientID + ", " + txtWorkPhone2.ClientID + ")"
        txtWorkPhone2.Attributes("onkeyup") = "autotab(" + txtWorkPhone2.ClientID + ", " + txtWorkPhone3.ClientID + ")"
        txtWorkPhone3.Attributes("onkeyup") = "autotab(" + txtWorkPhone3.ClientID + ", " + txtSSN1.ClientID + ")"
        txtSSN1.Attributes("onkeyup") = "autotab(" + txtSSN1.ClientID + ", " + txtSSN2.ClientID + ")"
        txtSSN2.Attributes("onkeyup") = "autotab(" + txtSSN2.ClientID + ", " + txtSSN3.ClientID + ")"
        txtSSN3.Attributes("onkeyup") = "autotab(" + txtSSN3.ClientID + ", " + txtEmail.ClientID + ")"

        txtPrimaryPhone1_1.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone1_1.ClientID + ", " + txtPrimaryPhone2_1.ClientID + ")"
        txtPrimaryPhone2_1.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone2_1.ClientID + ", " + txtPrimaryPhone3_1.ClientID + ")"
        txtPrimaryPhone3_1.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone3_1.ClientID + ", " + txtCellPhone1_1.ClientID + ")"
        txtCellPhone1_1.Attributes("onkeyup") = "autotab(" + txtCellPhone1_1.ClientID + ", " + txtCellPhone2_1.ClientID + ")"
        txtCellPhone2_1.Attributes("onkeyup") = "autotab(" + txtCellPhone2_1.ClientID + ", " + txtCellPhone3_1.ClientID + ")"
        txtCellPhone3_1.Attributes("onkeyup") = "autotab(" + txtCellPhone3_1.ClientID + ", " + txtWorkPhone1_1.ClientID + ")"
        txtWorkPhone1_1.Attributes("onkeyup") = "autotab(" + txtWorkPhone1_1.ClientID + ", " + txtWorkPhone2_1.ClientID + ")"
        txtWorkPhone2_1.Attributes("onkeyup") = "autotab(" + txtWorkPhone2_1.ClientID + ", " + txtWorkPhone3_1.ClientID + ")"
        txtWorkPhone3_1.Attributes("onkeyup") = "autotab(" + txtWorkPhone3_1.ClientID + ", " + txtSSN1_1.ClientID + ")"
        txtSSN1_1.Attributes("onkeyup") = "autotab(" + txtSSN1_1.ClientID + ", " + txtSSN2_1.ClientID + ")"
        txtSSN2_1.Attributes("onkeyup") = "autotab(" + txtSSN2_1.ClientID + ", " + txtSSN3_1.ClientID + ")"
        txtSSN3_1.Attributes("onkeyup") = "autotab(" + txtSSN3_1.ClientID + ", " + txtEmail1.ClientID + ")"

        txtPrimaryPhone1_2.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone1_2.ClientID + ", " + txtPrimaryPhone2_2.ClientID + ")"
        txtPrimaryPhone2_2.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone2_2.ClientID + ", " + txtPrimaryPhone3_2.ClientID + ")"
        txtPrimaryPhone3_2.Attributes("onkeyup") = "autotab(" + txtPrimaryPhone3_2.ClientID + ", " + txtCellPhone1_2.ClientID + ")"
        txtCellPhone1_2.Attributes("onkeyup") = "autotab(" + txtCellPhone1_2.ClientID + ", " + txtCellPhone2_2.ClientID + ")"
        txtCellPhone2_2.Attributes("onkeyup") = "autotab(" + txtCellPhone2_2.ClientID + ", " + txtCellPhone3_2.ClientID + ")"
        txtCellPhone3_2.Attributes("onkeyup") = "autotab(" + txtCellPhone3_2.ClientID + ", " + txtWorkPhone1_2.ClientID + ")"
        txtWorkPhone1_2.Attributes("onkeyup") = "autotab(" + txtWorkPhone1_2.ClientID + ", " + txtWorkPhone2_2.ClientID + ")"
        txtWorkPhone2_2.Attributes("onkeyup") = "autotab(" + txtWorkPhone2_2.ClientID + ", " + txtWorkPhone3_2.ClientID + ")"
        txtWorkPhone3_2.Attributes("onkeyup") = "autotab(" + txtWorkPhone3_2.ClientID + ", " + txtSSN1_2.ClientID + ")"
        txtSSN1_2.Attributes("onkeyup") = "autotab(" + txtSSN1_2.ClientID + ", " + txtSSN2_2.ClientID + ")"
        txtSSN2_2.Attributes("onkeyup") = "autotab(" + txtSSN2_2.ClientID + ", " + txtSSN3_2.ClientID + ")"
        txtSSN3_2.Attributes("onkeyup") = "autotab(" + txtSSN3_2.ClientID + ", " + txtEmail2.ClientID + ")"

        txtEmergencyContactPhone1.Attributes("onkeyup") = "autotab(" + txtEmergencyContactPhone1.ClientID + ", " + txtEmergencyContactPhone2.ClientID + ")"
        txtEmergencyContactPhone2.Attributes("onkeyup") = "autotab(" + txtEmergencyContactPhone2.ClientID + ", " + txtEmergencyContactPhone3.ClientID + ")"
        txtEmergencyContactPhone3.Attributes("onkeyup") = "autotab(" + txtEmergencyContactPhone3.ClientID + ", " + txtEmergencyContactRelationship.ClientID + ")"

        txtCDContactPhone1.Attributes("onkeyup") = "autotab(" + txtCDContactPhone1.ClientID + ", " + txtCDContactPhone2.ClientID + ")"
        txtCDContactPhone2.Attributes("onkeyup") = "autotab(" + txtCDContactPhone2.ClientID + ", " + txtCDContactPhone3.ClientID + ")"
        txtCDContactPhone3.Attributes("onkeyup") = "autotab(" + txtCDContactPhone3.ClientID + ", " + rblCDServiceType.ClientID + ")"
    End Sub

    Protected Sub ddlCounty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCounty.SelectedIndexChanged
        If ddlCounty.SelectedItem.Text = "Lawrence" Then
            rblService.SelectedIndex = -1
            rblService.Visible = True
        Else
            rblService.Visible = False
            If Right(ddlCounty.SelectedValue, 3) = "Gas" Then
                hdnServiceType.Value = "Gas"
            End If
            If Right(ddlCounty.SelectedValue, 8) = "Electric" Then
                hdnServiceType.Value = "Electric"
            End If
        End If
    End Sub

    Protected Sub rblService_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblService.SelectedIndexChanged
        hdnServiceType.Value = rblService.SelectedValue.ToString
    End Sub
End Class
