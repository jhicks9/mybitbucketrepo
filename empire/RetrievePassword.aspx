<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="RetrievePassword.aspx.vb" Inherits="RetrievePassword" title="MyPassword Reset" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">    
    <div style="width:745px;"> <%--Bug fix for auto-selecting tabs in code-behind--%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
               
                <asp:Panel ID="pnlSelect" runat="server" Visible="true">
                    <br />Please select one of the following methods of recovering your credentials:</center><br /><br />
                    <asp:RadioButton ID="rbAN" GroupName="selection" value="AN" runat="server" /><strong>Reset by Account Number</strong><br />
                    <font size="1">(Note: You will need to know your Empire District Account Number, name, and the last four digits of SSN)</font><br />
                    <asp:RadioButton ID="rbEmail" GroupName="selection" value="Email" runat="server" /><strong>Reset by Email</strong><br />
                    <font size="1">(Note: You will need to know the E-mail address registered with your MyAccount)</font><br />
                    <asp:RadioButton ID="rbUN" GroupName="selection" value="UN" runat="server" /><strong>Recover User Name</strong><br />
                    <font size="1">(Note: You will need to know your Empire District Account Number)</font><br /><br />
                    <div style="">
                         <asp:LinkButton ID="lbSelect" runat="server" CssClass="ovalbutton" CausesValidation="true" OnClick="lbSelect_Click"><span>Select</span></asp:LinkButton>
                    </div>

                </asp:Panel>
            
                <asp:Panel ID="pnlInput" runat="server" Visible="false">
                    You will need to know your account number, your name, and the last four digits of your Social Security Number to reset your password.<br /><br />
                    Your name and Empire District account number should match <b>exactly</b> as it appears on your statement.
                    For example:
                    <div id="gallery">
                    <a id="A1" href="~/images/bill_sample_full_page.jpg" runat="server" title="Click to enlarge">
                         <img id="imgSample" src="~/images/bill_sample_thumbnail.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
                    </div>
                    <br /><hr style="color:#113962;height:1px;" /><br />
                    <asp:Label ID="lblAccount" runat="server" Width="125px">Account Number:</asp:Label><br />
                    <asp:TextBox ID="Textbox1" runat="server" width="70px" MaxLength="6"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAccount" runat="server" ControlToValidate="Textbox1" ErrorMessage="*" SetFocusOnError="true" Display="Dynamic" />
                    -
                    <asp:TextBox ID="Textbox2" runat="server" Width="25px" ValidationGroup="valAccount" MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvBP" runat="server" ControlToValidate="Textbox2" ErrorMessage="*" SetFocusOnError="true" Display="Dynamic" />
                    -
                    <asp:TextBox ID="TextBox3" runat="server" Width="25px" ValidationGroup="valAccount" MaxLength="1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCD" runat="server" ControlToValidate="Textbox3" ErrorMessage="*" SetFocusOnError="true" Display="Dynamic" />
                    <br />
                    <asp:Label ID="lblName" runat="server" Width="125px" Text="Name: "></asp:Label><br />
                    <asp:TextBox ID="txtName" runat="server" Width="155px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtName" ErrorMessage="Name required" SetFocusOnError="true" Display="Dynamic" />
                    <br />
                    <asp:Label ID="lblSSN" Text="Last 4 digits of SSN or Tax ID: " runat="server"></asp:Label><br />
                    <asp:TextBox ID="txtSSN" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSSN" runat="server" ControlToValidate="txtSSN" ErrorMessage="SSN required" SetFocusOnError="True" Display="Dynamic" />
                    <br />

                    <asp:Label ID="lblError" runat="server"></asp:Label>
                    <br /><hr style="color:#113962;height:1px;" /><br />
                            
                    Passwords are required to be at least 8 characters long including one special character (for example: john_123)<br /><br />
                    <asp:Label ID="lblNewPassword" runat="server" Width="125px" Text="New Password:"></asp:Label><br />
                    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Password required" SetFocusOnError="true" Display="Dynamic" />
                    <br />
                    <asp:Label ID="lblConfirmPassword" runat="server" Width="125px" Text="Confirm Password:"></asp:Label><br />
                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Password required" SetFocusOnError="true" Display="Dynamic" />
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" ErrorMessage="Passwords do not match" SetFocusOnError="True" Display="Dynamic" />
                    <br />
                    <div><asp:Label ID="lblMessage" runat="server" visible="false" ></asp:Label></div>
                    <br />
                    <div style="">
                         <asp:LinkButton ID="lbAccount" runat="server" CssClass="ovalbutton" CausesValidation="true" OnClick="lbAccount_Click"><span>Update MyPassword</span></asp:LinkButton>
                         <asp:LinkButton ID="lbReselect" style="float:right" runat="server" CssClass="ovalbutton" CausesValidation="false" OnClick="lbReselect_Click"><span>Select Different Method</span></asp:LinkButton>
                    </div>
                    <br /><br />
                </asp:Panel>
                        
                <asp:Panel ID="pnlDisplay" runat="server" Visible="false" Height="75px">
                    <div style="padding:15px">
                         Password changed.  Please
                         <asp:HyperLink ID="hlLogin" runat="server" NavigateUrl="~/login.aspx" Text="login" />
                         with your new password.
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlEmailInput" runat="server" Visible="false">
                    You will need to know the email address registered with your MyAccount to reset the password.  The email address must be valid.<br /><br />
                    <br />
                    <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label><br />
                    <asp:TextBox ID="txtEmail" runat="server" width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="emailinput" ControlToValidate="txtEmail" runat="server" ErrorMessage="Email address required" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="emailinput" runat="server" ControlToValidate="txtEmail" Display="Dynamic" 
                         ErrorMessage="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                    <br />
                    <asp:Label ID="lblConfirmEmail" runat="server" Text="Confirm Email: "></asp:Label><br />
                    <asp:TextBox ID="txtConfirmEmail" runat="server" width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="emailinput" ControlToValidate="txtConfirmEmail" runat="server" ErrorMessage="Email address required" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="emailinput" runat="server" ControlToValidate="txtConfirmEmail" Display="Dynamic" 
                         ErrorMessage="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="cvEmailAddress" runat="server" ValidationGroup="emailinput" ControlToCompare="txtEmail" ControlToValidate="txtConfirmEmail" ErrorMessage="-Email address does not match" SetFocusOnError="True" Display="Dynamic" />
                    <br /><br />
                    <div style="">
                         <asp:LinkButton ID="lbEmail" runat="server" CssClass="ovalbutton" ValidationGroup="emailinput" OnClick="lbEmail_Click"><span>Email MyPassword</span></asp:LinkButton>
                         <asp:LinkButton ID="lbReselect2" style="float:right" runat="server" CssClass="ovalbutton" CausesValidation="false" OnClick="lbReselect_Click"><span>Select Different Method</span></asp:LinkButton>
                    </div>
                    <br /><br />
                </asp:Panel>
                        
                <asp:Panel ID="pnlEmailSent" runat="server" Visible="false">
                    <div style="padding:15px">
                    An email has been sent to&nbsp;
                    <asp:Label ID="lblEmailSent" runat="server" />.&nbsp;&nbsp;
                    Please follow the instructions in the email to reset MyPassword.
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlUserNameInput" runat="server" Visible="false">

                            Your Account Number should match <b>exactly</b> as it appears on your statement. 
                            For example:
                            <div id="gallery2">
                            <a id="A2" href="~/images/bill_sample_full_page.jpg" runat="server" title="Click to enlarge">
                                <img id="img1" src="~/images/bill_sample_thumbnail.jpg" runat="server" alt="Click to enlarge" title="Click to enlarge" style="border: 2px solid #b0c4de"  /></a>
                            </div>                            
                            <br /><hr style="color:#113962;height:1px;" /><br />
                        
                            <asp:Label ID="lblAN" runat="server" Text="Account Number:" /><br />
                            <asp:TextBox ID="txtUserNameAN" runat="server" Width="72px" MaxLength="6"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUserNameAN" runat="server" ControlToValidate="txtUserNameAN" ErrorMessage="required" SetFocusOnError="true" ValidationGroup="username" Display="dynamic" />
                             -
                            <asp:TextBox ID="txtUserNameBP" runat="server" Width="25px" MaxLength="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUserNameBP" runat="server" ControlToValidate="txtUserNameBP" ErrorMessage="required" SetFocusOnError="true" ValidationGroup="username" Display="dynamic" />
                            -
                            <asp:TextBox ID="txtUserNameCD" runat="server" Width="25px" MaxLength="1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUserNameCD" runat="server" ControlToValidate="txtUserNameCD" ErrorMessage="required" SetFocusOnError="true" ValidationGroup="username" Display="dynamic" />
                            <br />
                            <div><asp:Label ID="lblUserNameMessage" runat="server" visible="false"></asp:Label></div>
                            <br />
                            <div style="">
                                <asp:LinkButton ID="lbUserName" runat="server" CssClass="ovalbutton" ValidationGroup="username" OnClick="lbUserName_Click"><span>Recover User Name</span></asp:LinkButton>
                                <asp:LinkButton ID="lbReselect3" style="float:right" runat="server" CssClass="ovalbutton" CausesValidation="false" OnClick="lbReselect_Click"><span>Select Different Method</span></asp:LinkButton>
                            </div>
                            <br /><br />
                        </asp:Panel>
            
                        <asp:Panel ID="pnlUserNameRecovered" runat="server" Visible="false">
                            <div style="padding:15px">
                                Your user name is:&nbsp;
                                <asp:label ID="lblShowUserName" runat="server" Font-Bold="true"></asp:label>.<br /><br />
                                <asp:HyperLink ID="hlLoginUserName" runat="server" NavigateUrl="~/login.aspx" Text="Login" />
                                with your user name.
                            </div>
                        </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>    
    </div>
    <script type="text/javascript">
        jQuery("#gallery a").slimbox(); jQuery("#gallery2 a").slimbox();
    </script>
</asp:Content>