
Partial Class flash_flashplayer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sm As SiteMapPath = Page.Master.FindControl("SiteMapPath")  'disable sitemap
            sm.Visible = False
        Catch
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            If String.IsNullOrEmpty(Request.QueryString("t")) Then
                lblTitle.Text = ""
            Else
                lblTitle.Text = Request.QueryString("t")
            End If

            'set video properties
            If String.IsNullOrEmpty(Request.QueryString("v")) Then
                flashvideo.Visible = False
            Else
                flashvideo.NavigateUrl = "~/flash/" & Request.QueryString("v")
            End If
            If String.IsNullOrEmpty(Request.QueryString("w")) Then
                flashvideo.Width = "320"
            Else
                flashvideo.Width = Request.QueryString("w")
            End If
            If String.IsNullOrEmpty(Request.QueryString("h")) Then
                flashvideo.Height = "240"
            Else
                flashvideo.Height = Request.QueryString("h")
            End If

            lblFlowPlayerScript.Text = "<script type='text/javascript' language='JavaScript'>"
            lblFlowPlayerScript.Text &= "flowplayer('" & flashvideo.ClientID & "', { src: 'flowplayer.commercial-3.1.5.swf', wmode: 'transparent' }, { key: '$3d11963302d41480993', logo: null"
            If Right(Request.QueryString("v").ToLower, 3) = "swf" Then  'handles swf files
                lblFlowPlayerScript.Text &= ",play: null"
            End If
            If Right(Request.QueryString("v").ToLower(), 3) = "mp3" Then
                lblFlowPlayerScript.Text &= ",playlist: ['" & ResolveUrl("~/images/flashbackground.jpg") & "', '" & Request.QueryString("v") & "']"
            End If
            lblFlowPlayerScript.Text &= " });"
            lblFlowPlayerScript.Text &= "</script>"
        Catch
        End Try
    End Sub

End Class
