<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Cares.aspx.vb" Inherits="Community_Cares" title="Empire Cares" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div id="gallery">
        <table cellpadding="2" cellspacing="0">
            <tr>
                <td align="center" colspan="3"><b>Empire gives back to its local communities in a variety of ways.  Click on a photo to learn more.</b></td>
            </tr>
            <tr>
                <td>
                    <a href="~/images/community/empire_cares11.jpg" runat="server" rel="ecares11" title="Empire donated a bucket truck to Southwest Baptist University to help with campus maintenance.  Retired from Empire�s fleet, the truck will have a second life tackling hard-to-reach projects at SBU.">
                        <img id="Img1" src="~/images/community/empire_cares11_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares9.jpg" runat="server" rel="ecares9" title="A van retired from Empire�s vehicle fleet was donated to Children�s Haven to help transport children to school and other activities.">
                        <img src="~/images/community/empire_cares9_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares10.jpg" runat="server" rel="ecares10" title="Empire�s customer service team frames walls for future Habitat for Humanity homes during the Martin Luther King, Jr. Day of Service.">
                        <img src="~/images/community/empire_cares10_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="~/images/community/empire_cares2.jpg" runat="server" rel="ecares2" title="Local scouts take part in a tree-planting project with the help of Empire�s Vegetation Management staff and trees donated by Empire.  The scouts earned the prestigious Hornaday Unit Award for their efforts to help reforest Joplin.">
                        <img src="~/images/community/empire_cares2_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares12.jpg" runat="server" rel="ecares12" title="">
                        <img src="~/images/community/empire_cares12_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares5.jpg" runat="server" rel="ecares5" title="Two organizations in Pierce City were the recent recipients of land donated by Empire.  The Pierce City Rural Fire Department received about an acre of property near Empire�s local service center.  The property provided the space needed to install a communications tower and will provide room for future expansion.  St. Mary�s Catholic Church received a one-third acre site in the downtown area to provide a space for local community events and parking.">
                        <img src="~/images/community/empire_cares5_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="~/images/community/empire_cares8.jpg" runat="server" rel="ecares8" title="For the United Way Day of Action, Empire employees took part in a game of beeper ball with program recipients of the Joplin Association for the Blind.">
                        <img src="~/images/community/empire_cares8_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares6.jpg" runat="server" rel="ecares6" title="Thank you cards flooded into the Empire office recently after more than $3000 in employee donations were used to purchase school supplies and learning games for students of Jefferson Elementary School.  Empire�s Business Education Partnership Committee has been actively involved with Jefferson Elementary for more than two decades.">
                        <img src="~/images/community/empire_cares6_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares4.jpg" runat="server" rel="ecares4" title="Empire employees organized the donation of used mineral oil from a substation transformer to the Hollister school system.  The used oil will provide fuel for heating the school�s bus barn for about two years.">
                        <img src="~/images/community/empire_cares4_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="~/images/community/empire_cares1.jpg" runat="server" rel="ecares1" title="Rick Stockton, construction design, teaches Jefferson Elementary students how to be safe around electricity.">
                        <img src="~/images/community/empire_cares1_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td>
                    <a href="~/images/community/empire_cares3.jpg" runat="server" rel="ecares3" title="Janice Wright, billing operations, helps a Jefferson student with a craft project during a parent night.">
                        <img src="~/images/community/empire_cares3_thumb.jpg" runat="server" title="Click to enlarge" alt="" style="border: 0px solid transparent;"/>
                    </a>
                </td>
                <td></td>
            </tr>
            <tr><td colspan="3"><div style="height:20px"></div></td></tr>
        </table>

    </div>
    <script type="text/javascript">jQuery("#gallery a").slimbox();</script>
</asp:Content>

