﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="BusComDev.aspx.vb" Inherits="Community_BusComDev" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        /* cellpadding */
        .bcmtable th, .bmctable td { padding:3px;}
        .bcmtable th {margin: 0 auto;background-color:#c5c5c5;font-weight:bold}

        /* cellspacing */
        .bcmtable {border-collapse:separate;border-spacing:3px;width:100%}  /* cellspacing="3" */ 
        /*.bcmtable {border-collapse:collapse;border-spacing:0}  /* cellspacing="0" */

        /* valign */
        .bcmtable th, .bcmtable td { vertical-align: top; }

        /* align (center) */
        /*table { margin: 0 auto; } */
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    The Business & Community Development group was established to increase the level of contact within the communities served by Empire. Members of the group serve as contacts within assigned geographic areas for key leadership within the communities, industries, city government and schools. They also assist with economic development, strategic planning, marketing and business recruitment, retention and expansion.
    <br /><br />
    Our customers’ needs are important to us. From providing reliable electricity to answering your questions in a friendly, knowledgeable manner, we are here for you.
    <br /><br />
    <div style="width:100%;text-align:center"><asp:Image id="imgBusComDev" runat="server" ImageUrl="~/images/community-buscomdev.jpg" /></div>
    <br />To contact your Business & Community Development manager:<br />
    <table class="bcmtable">
        <tr>
            <th colspan="2" style="width:50%">Branson Hollister area</th>
            <th colspan="2" style="width:50%">Webb City area</th>
        </tr>
        <tr>
            <td>Shawn Pingleton</td>
            <td>spingleton@empiredistrict.com</td>
            <td>Stephanie Howard</td>
            <td>showard@empiredistrict.com</td>
        </tr>
        <tr>
            <td colspan="2"><i>senior manager of Business & Community Development</i></td>
            <td>Kelly Chenoweth</td>
            <td>kchenoweth@empiredistrict.com</td>
        </tr>
        <tr>
            <th colspan="2">Joplin area</th>
            <th colspan="2">Neosho area and Gas areas</th>
        </tr>
        <tr>
            <td>Marsha Wallace</td>
            <td>mwallace@empiredistrict.com</td>
            <td>Rick Hendricks</td>
            <td>rhendricks@empiredistrict.com</td>
        </tr>
        <tr>
            <td>Michael Rouse</td>
            <td>mrouse@empiredistrict.com</td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <th colspan="2">Baxter Springs area</th>
            <th colspan="2">Bolivar area</th>
        </tr>
        <tr>
            <td>Anna Lee Alumbaugh</td>
            <td>aalumbaugh@empiredistrict.com</td>
            <td>Travis Jones</td>
            <td>tjones@empiredistrict.com</td>
        </tr>
        <tr>
            <th colspan="2">Ozark area</th>
            <th colspan="2">Aurora area and East end offices</th>
        </tr>
        <tr>
            <td>Mike Hayward</td>
            <td>mhayward@empiredistrict.com</td>
            <td>Marcia Sadler</td>
            <td>msadler@empiredistrict.com</td>
        </tr>
        <tr>
            <th colspan="2">HVAC specialist - all areas</th>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td>Stan Patterson</td>
            <td>spatterson@empiredistrict.com</td>
            <td colspan="2"></td>
        </tr>
    </table> 
</asp:Content>