<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="EconomicDev.aspx.vb" Inherits="Community_EconomicDev" title="Economic Development" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="bottom" style="padding-right:5px;">
                The Empire District is the place to be for successful business and quality living.
                <ul>
                    <li>Outstanding workforce - commitment of local technical schools, community colleges, and four-year institutions dedicated to training</li>
                    <li>Diverse manufacturing and service sector base</li>
                    <li>Available business and industrial parks and available buildings</li>
                    <li>Accessible to major transportation routes including interstate roadways, air and rail systems</li>
                </ul>
                The Empire staff, along with our community partners, will assist you in your business expansion or relocation.
                <div style="padding-top:30px;">Use the links below to access community profiles and available industrial buildings and sites:</div>
            </td>
            <td>
                <asp:Image ID="imgEconomicDev" Imageurl="~/images/landing/landing-economicdev.jpg" runat="server" />    
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <ul>
                    <li style="list-style:none;padding-bottom:0;"><strong><i>All</i></strong></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empire&appsection=localities" target="_blank">Communities / Localities</a></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empire&appsection=buildings" target="_blank">Buildings</a></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empire&appsection=sites" target="_blank">Sites</a></li>
                </ul>
            </td>
            <td>
                <ul>
                    <li style="list-style:none;padding-bottom:0;"><strong><i>Arkansas</i></strong></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empirear&appsection=localities" target="_blank">Communities / Localities</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empirear&appsection=buildings" target="_blank">Buildings</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empirear&appsection=sites" target="_blank">Sites</a></li>
                </ul>
            </td>
            <td>
                <ul>
                    <li style="list-style:none;padding-bottom:0;"><strong><i>Kansas</i></strong></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empireks&appsection=localities" target="_blank">Communities / Localities</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empireks&appsection=buildings" target="_blank">Buildings</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empireks&appsection=sites" target="_blank">Sites</a></li> 
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li style="list-style:none;padding-bottom:0;"><strong><i>Missouri</i></strong></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empiremo&appsection=localities" target="_blank">Communities / Localities</a> </li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empiremo&appsection=buildings" target="_blank">Buildings</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empiremo&appsection=sites" target="_blank">Sites</a></li> 
                </ul>
            </td>
            <td>
                <ul>
                    <li style="list-style:none;padding-bottom:0;"><strong><i>Oklahoma</i></strong></li>
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empireok&appsection=localities" target="_blank">Communities / Localities</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empireok&appsection=buildings" target="_blank">Buildings</a></li> 
                    <li style="padding:0;"><a href="http://www.locationone.com/lois/logon.do?username=empireok&appsection=sites" target="_blank">Sites</a></li> 
                </ul>
            </td>
        </tr>
    </table>

    For more information or to inquire about local and/or state incentives, please contact us:
    <table style="border-collapse:collapse;border-spacing:0">
        <tr>
            <td style="vertical-align:top">Email:</td>
            <td style="vertical-align:top"><a href="~/About/ContactUs.aspx?type=ed" runat="server">economic.development@empiredistrict.com</a></td>
        </tr>
        <tr>
            <td style="vertical-align:top">Phone:</td>
            <td style="vertical-align:top">800-206-2300</td>
        </tr>
    </table>
    <br />    
    To find our Business and Community Development representatives, visit our <a href="~/Community/BusComDev.aspx" runat="server">Business and Community Development</a> page.
    <br /><br />
</asp:Content>