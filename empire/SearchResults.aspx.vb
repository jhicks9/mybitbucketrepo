﻿Imports System.Data

Partial Class sr
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Dim sm As SiteMapPath = Page.Master.FindControl("SiteMapPath")  'disable sitemap
            sm.Visible = False

            'set properties for text box input
            txtSearch.Attributes.Add("onFocus", "if (this.value == 'Search') { this.value = ''; this.style.color = '#000'; }")
            txtSearch.Attributes.Add("onBlur", "if (this.value == '') { this.value = 'Search'; this.style.color = '#888'; }")

            If Not IsPostBack Then  'pass query to search results to have it search without further input
                If Not String.IsNullOrEmpty(Request.QueryString("s")) Then  'if query string search value is passed in
                    hdnSearchString.Value = Request.QueryString("s")
                    txtSearch.Text = Request.QueryString("s")
                    getSearchResults(hdnSearchString.Value)
                End If
            Else
                hdnSearchString.Value = ""
            End If

            If txtSearch.Text = "Search" Then
                txtSearch.ForeColor = System.Drawing.ColorTranslator.FromHtml("#888888")
            Else
                txtSearch.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000")
            End If
        Catch
        End Try
    End Sub

    Public Sub getSearchResults(ByVal searchQuery As String)
        If String.IsNullOrEmpty(txtSearch.Text) Or txtSearch.Text = "Search" Then 'only perform search if criteria entered
        Else
            If SearchResults.runCrawl = True Then 'if last crawl time > 24 hours run a crawl and rebuild search indexes
                SearchResults.createSearchIndex()  'rebuild search indexes with fresh web data
            End If
            rptSearchResults.DataSource = SearchResults.querySearchIndex(searchQuery)
            rptSearchResults.DataBind()
            lblTotalHits.Text = SearchResults.totalHits.ToString & " results found"
        End If
    End Sub

    Protected Sub imgSearch_Click(sender As Object, e As System.EventArgs) Handles imgSearch.Click
        'lblStatus.Text = Request.Form("search").ToString  'read contents of html input in codebehind
        getSearchResults(txtSearch.Text)
    End Sub
End Class
