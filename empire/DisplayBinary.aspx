﻿<%@ Import Namespace="System.Data.SQLClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Page Language="VB" %>

<script runat="server">
    Sub Page_Load()
        Try
            Dim dtype As String = (Request.QueryString("dtype"))
            Dim id As String = (Request.QueryString("id"))
                    
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Select Case dtype
                Case "modules"
                    Dim query As String = "SELECT module_image from modules where module_id = " & id.ToString
                    Dim cmd As New SqlCommand(query, conn)
                    conn.Open()
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    While dr.Read
                        If dr.HasRows Then
                            If Not dr("module_image") Is System.DBNull.Value Then 'do not attempt to write empty fields
                                Response.ContentType = "image/jpeg"
                                Response.BinaryWrite(dr("module_image"))
                            Else
                                DisplayDefault()  'display emtpy if field is empty
                            End If
                        Else
                            DisplayDefault()  'Display empty of query fails
                        End If
                    End While
                    dr.Close()
                    conn.Close()
                Case "inserts"
                    Dim fname As String = ""
                    Dim query As String = "select filename from billinsert where billinsert.id=" & id.ToString
                    Dim cmd As New SqlCommand(query, conn)
                    conn.Open()
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        While dr.Read
                            fname = dr.GetValue(0).ToString
                        End While
                    End If
                    dr.Close()
                    
                    Dim ext As String = System.IO.Path.GetExtension(fname)
                    Select Case ext
                        Case ".htm", ".html"
                            Context.Response.ContentType = "text/HTML"
                        Case ".txt"
                            Context.Response.ContentType = "text/plain"
                        Case ".doc", ".rtf"
                            Context.Response.ContentType = "Application/msword"
                        Case ".ppt"
                            Context.Response.ContentType = "application/vnd.ms-powerpoint"
                        Case ".csv", ".xls"
                            Context.Response.ContentType = "Application/x-msexcel"
                        Case ".pdf"
                            Context.Response.ContentType = "Application/pdf"
                        Case Else
                            Context.Response.ContentType = "text/plain"
                    End Select
                    If Len(fname) > 0 Then
                        cmd.CommandText = "SELECT imagedata from billinsert where id = " & id.ToString
                        Dim file() As Byte = CType(cmd.ExecuteScalar(), Byte())
                        Context.Response.BinaryWrite(file)
                    Else
                        Context.Response.Write("Document Not Found")
                    End If
                    conn.Close()
                Case Else
                    DisplayDefault()
            End Select
        Catch
            DisplayDefault()
        Finally
        End Try
    End Sub
    
    Public Sub DisplayDefault()  'empty graphic
        Try
            Dim path As String = Server.MapPath("~/images/ico_empty.png")
            Dim byteArray As Array = File.ReadAllBytes(path)
            Response.BinaryWrite(byteArray)
        Catch
        End Try
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server"><title></title></head>
    <body><form id="form1" runat="server"><div></div></form></body>
</html>