﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="RetrievePasswordEmail.aspx.vb" Inherits="RetrievePasswordEmail" title="Password Recovery via Email" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
        <ContentTemplate>
            <asp:Panel ID="pnlEmailPassword" runat="server" Visible="true">
                <asp:Label ID="lblDisplayEmail" runat="server" Text=""></asp:Label><br /><br />
                <asp:Label ID="lblDisplayUser" runat="server" Text="Select an account: " ></asp:Label><br />
                <asp:CheckBoxList ID="cblUserName" runat="server">
                </asp:CheckBoxList>
                <hr style="color:#113962;height:1px;" />
                <br />
                <asp:Label ID="lblEmailNewPassword" runat="server" Text="New Password: "></asp:Label><br />
                <asp:TextBox ID="txtEmailNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvfEmailNewPassword" runat="server" ControlToValidate="txtEmailNewPassword"
                    ErrorMessage="Password required" SetFocusOnError="true" Display="Dynamic" />
                <asp:RegularExpressionValidator ID="ValidPassword1" ValidationGroup="password" runat="server" ControlToValidate="txtEmailNewPassword"
                    ErrorMessage="Passwords are required to be at least 8 characters long including one special character (for example: john_123)"
                    ValidationExpression="(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*" Display="Dynamic" />
                <br />
                <asp:Label ID="lblEmailConfirmPassword" runat="server" Text="Confirm Password: "></asp:Label><br />
                <asp:TextBox ID="txtEmailConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvfEmailConfirmPassword" runat="server" ControlToValidate="txtEmailConfirmPassword"
                    ErrorMessage="Password required" SetFocusOnError="true" Display="Dynamic" />
                <asp:RegularExpressionValidator ID="ValidPassword2" ValidationGroup="password" runat="server" ControlToValidate="txtEmailConfirmPassword"
                    ErrorMessage="Passwords are required to be at least 8 characters long including one special character (for example: john_123)"
                    ValidationExpression="(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*" Display="Dynamic" />       
                <asp:CompareValidator ID="ComparePassword" runat="server" ControlToCompare="txtEmailNewPassword" ControlToValidate="txtEmailConfirmPassword" ValidationGroup="password" ErrorMessage="Passwords do not match" SetFocusOnError="True" Display="Dynamic" />
                <br /><asp:Label ID="lblMessage" runat="server" ForeColor="Red" ></asp:Label><br />
                <div style="float:left;margin-left:5px;">
                    <asp:LinkButton ID="lbPassword" runat="server" CssClass="ovalbutton" CausesValidation="true" OnClick="lbPassword_Click"><span>Update MyPassword</span></asp:LinkButton>
                </div>
            </asp:Panel>
            
            <asp:Panel ID="pnlDisplay" runat="server" Visible="false" Height="75px">
                <div style="padding:15px">
                    Password changed.  Please&nbsp;
                    <asp:HyperLink ID="hlLogin" runat="server" NavigateUrl="~/login.aspx" Text="login" />
                    &nbsp;with your new password.
                </div>
            </asp:Panel>
        </ContentTemplate>
     </asp:UpdatePanel>
</asp:Content>

