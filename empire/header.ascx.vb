﻿
Partial Class header
    Inherits System.Web.UI.UserControl

    Private _headerMenu As Boolean = True
    Dim psm As New processSiteMap

    Public Property headerMenu() As Boolean
        Get
            Return _headerMenu
        End Get
        Set(ByVal value As Boolean)
            _headerMenu = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litHeaderMenu.Text = buildMenu()
            If _headerMenu Then
                ' do not show when surfing in the following areas: assistance agency, commission
                Select Case psm.getCurrentNodeKey("resourcekey")
                    Case "Assistance", "Commission"
                        pnlFadeMenu.Visible = False
                        Dim spm As SiteMapPath = Page.Master.FindControl("SiteMapPath")
                        spm.Enabled = False
                    Case Else
                        pnlFadeMenu.Visible = True
                End Select
            Else
                pnlFadeMenu.Visible = False
            End If
        Catch
        End Try
    End Sub

    Function ResolveSSLUrl(ByVal url As String) As String
        Dim ssl As String = ""
        Try
            ssl = String.Format("https://{0}{1}", Request.ServerVariables("HTTP_HOST"), ResolveUrl(url))
        Catch
        End Try
        Return ssl
    End Function

    Protected Sub LoginStatus1_LoggingOut(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs)
        System.Threading.Thread.Sleep(500)
    End Sub

    Private Function buildMenu() As String
        Dim menu As String = ""
        Try
            Dim currentKey As String = psm.getCurrentNodeKey("resourcekey")
            If String.IsNullOrEmpty(currentKey) Then
                currentKey = psm.getCurrentNodeKey("title")
            End If
            Dim parentKey As String = psm.getParentNodeKey("resourcekey")
            Dim icon As String = "<img src='{3}' alt=''/>"
            Menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "notselected", ResolveUrl("~/default.aspx"), "Home")  'never selected
            If currentKey = "News" Or parentKey = "News" Then
                Menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "selected", ResolveUrl("~/newsroom/default.aspx"), "Newsroom")  'display selected
            Else
                Menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "notselected", ResolveUrl("~/newsroom/default.aspx"), "Newsroom")  'not selected
            End If
            If currentKey = "MyAccount" Or parentKey = "MyAccount" Then
                menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "selected", ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx"), "MyAccount")  'display selected
            Else
                menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "notselected", ResolveSSLUrl("~/CustomerService/MyAccount/default.aspx"), "MyAccount")  'not selected
            End If
            If currentKey = "Outages" Or parentKey = "Outages" Then
                Menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "selected", ResolveUrl("~/Outages/default.aspx"), "Outages")  'display selected
            Else
                Menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}</a></li>", "notselected", ResolveUrl("~/Outages/default.aspx"), "Outages")  'not selected
            End If
            If currentKey = "Search" Or parentKey = "Search" Then
                menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}&nbsp;<img src='{3}' alt='Search' style='border:none;padding-top:1px;vertical-align:text-top'/></a></li>", "selected", ResolveUrl("~/SearchResults.aspx"), "Search", ResolveUrl("~/images/icon/icon_search.png"))  'display selected
            Else
                menu &= String.Format("<li class='{0}'><a href='{1}' title='{2}'>{2}&nbsp;<img src='{3}' alt='Search' style='border:none;padding-top:1px;vertical-align:text-top'/></a></li>", "notselected", ResolveUrl("~/SearchResults.aspx"), "Search", ResolveUrl("~/images/icon/icon_search.png"))  'not selected
            End If
        Catch
        End Try
        Return menu
    End Function

End Class
