Imports System.Data
Imports System.Data.SqlClient

Partial Class EnergySolutions_Electric
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            Dim pixelScript As HtmlGenericControl = New HtmlGenericControl
            pixelScript.TagName = "script"
            pixelScript.Attributes("async") = "async"
            pixelScript.InnerHtml = "!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){" & _
                                    "n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};" & _
                                    "if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}" & _
                                    "(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '414704345393477');fbq('track', 'PageView');"
            Page.Header.Controls.Add(pixelScript)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblRES.Visible = False
            lblRESLINKS.Text = ""
            divHr1.Visible = False
            lblBUS.Visible = False
            lblBUSLINKS.Text = ""
            divHr2.Visible = False
            lblEDU.Visible = False
            lblEDULINKS.Text = ""
            If String.IsNullOrEmpty(Request.QueryString("electric")) Then
                sqlDSRES.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 92 order by webdescription"
                dlRES.DataSourceID = "sqlDSRES"
                dlRES.DataBind()
                lblRESLINKS.Text = BuildLinks("R", "MO")
                sqlDSBUS.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 93 order by webdescription"
                dlBUS.DataSourceID = "sqlDSBUS"
                dlBUS.DataBind()
                lblBUSLINKS.Text = BuildLinks("B", "MO")
                If Not dlRES.Items.Count = 0 Or Not lblRESLINKS.Text = "" Then
                    lblRES.Visible = True
                    lblRES.Text = "Residential"
                    dlRES.Visible = True
                    lblRESLINKS.Visible = True
                End If
                If Not dlBUS.Items.Count = 0 Or Not lblBUSLINKS.Text = "" Then
                    lblBUS.Visible = True
                    lblBUS.Text = "Business"
                    divHr1.Visible = True
                    dlBUS.Visible = True
                    lblBUSLINKS.Visible = True
                End If
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbMO.Font.Bold = True
                lbOK.Font.Bold = False
                lbEET.Font.Bold = False
            ElseIf Request.QueryString.ToString = "electric=AR" Then
                sqlDSRES.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 88 order by webdescription"
                dlRES.DataSourceID = "sqlDSRES"
                dlRES.DataBind()
                lblRESLINKS.Text = BuildLinks("R", "AR")
                sqlDSBUS.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 89 order by webdescription"
                dlBUS.DataSourceID = "sqlDSBUS"
                dlBUS.DataBind()
                lblBUSLINKS.Text = BuildLinks("B", "AR")
                sqlDSEDU.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 00 order by webdescription"
                dlEDU.DataSourceID = "sqlDSEDU"
                dlEDU.DataBind()
                lblEDULINKS.Text = BuildLinks("E", "AR")
                If Not dlRES.Items.Count = 0 Or Not lblRESLINKS.Text = "" Then
                    lblRES.Visible = True
                    lblRES.Text = "Residential"
                    dlRES.Visible = True
                    lblRESLINKS.Visible = True
                End If
                If Not dlBUS.Items.Count = 0 Or Not lblBUSLINKS.Text = "" Then
                    lblBUS.Visible = True
                    lblBUS.Text = "Business"
                    divHr1.Visible = True
                    dlBUS.Visible = True
                    lblBUSLINKS.Visible = True
                End If
                If Not dlEDU.Items.Count = 0 Or Not lblEDULINKS.Text = "" Then
                    lblEDU.Visible = True
                    lblEDU.Text = "Education"
                    divHr2.Visible = True
                    dlEDU.Visible = True
                    lblEDULINKS.Visible = True
                End If
                lbAR.Font.Bold = True
                lbKS.Font.Bold = False
                lbMO.Font.Bold = False
                lbOK.Font.Bold = False
                lbEET.Font.Bold = False
            ElseIf Request.QueryString.ToString = "electric=KS" Then
                sqlDSRES.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 90 order by webdescription"
                dlRES.DataSourceID = "sqlDSRES"
                dlRES.DataBind()
                lblRESLINKS.Text = BuildLinks("R", "KS")
                sqlDSBUS.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 91 order by webdescription"
                dlBUS.DataSourceID = "sqlDSBUS"
                dlBUS.DataBind()
                lblBUSLINKS.Text = BuildLinks("B", "KS")
                If Not dlRES.Items.Count = 0 Or Not lblRESLINKS.Text = "" Then
                    lblRES.Visible = True
                    lblRES.Text = "Residential"
                    dlRES.Visible = True
                    lblRESLINKS.Visible = True
                End If
                If Not dlBUS.Items.Count = 0 Or Not lblBUSLINKS.Text = "" Then
                    lblBUS.Visible = True
                    lblBUS.Text = "Business"
                    divHr1.Visible = True
                    dlBUS.Visible = True
                    lblBUSLINKS.Visible = True
                End If
                lbAR.Font.Bold = False
                lbKS.Font.Bold = True
                lbMO.Font.Bold = False
                lbOK.Font.Bold = False
                lbEET.Font.Bold = False
            ElseIf Request.QueryString.ToString = "electric=OK" Then
                sqlDSRES.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 94 order by webdescription"
                dlRES.DataSourceID = "sqlDSRES"
                dlRES.DataBind()
                lblRESLINKS.Text = BuildLinks("R", "OK")
                sqlDSBUS.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 95 order by webdescription"
                dlBUS.DataSourceID = "sqlDSBUS"
                dlBUS.DataBind()
                lblBUSLINKS.Text = BuildLinks("B", "OK")
                If Not dlRES.Items.Count = 0 Or Not lblRESLINKS.Text = "" Then
                    lblRES.Visible = True
                    lblRES.Text = "Residential"
                    dlRES.Visible = True
                    lblRESLINKS.Visible = True
                End If
                If Not dlBUS.Items.Count = 0 Or Not lblBUSLINKS.Text = "" Then
                    lblBUS.Visible = True
                    lblBUS.Text = "Business"
                    divHr1.Visible = True
                    dlBUS.Visible = True
                    lblBUSLINKS.Visible = True
                End If
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbMO.Font.Bold = False
                lbOK.Font.Bold = True
                lbEET.Font.Bold = False
            ElseIf Request.QueryString.ToString = "electric=EET" Then
                sqlDSRES.SelectCommand = "select id,webdescription as DocDescription from documents where groupid = 14 and doctypeid = 00 order by webdescription"
                dlRES.DataSourceID = "sqlDSRES"
                dlRES.DataBind()
                lblRESLINKS.Text = BuildLinks("xx", "EET")
                dlBUS.DataSourceID = ""
                dlBUS.DataBind()
                If Not dlRES.Items.Count = 0 Or Not lblRESLINKS.Text = "" Then
                    lblRES.Visible = True
                    lblRES.Text = "Energy Efficiency Tools"
                    dlRES.Visible = True
                    lblRESLINKS.Visible = True
                End If
                lbAR.Font.Bold = False
                lbKS.Font.Bold = False
                lbMO.Font.Bold = False
                lbOK.Font.Bold = False
                lbEET.Font.Bold = True
            End If
        Catch
        End Try
    End Sub

    Public Function BuildLinks(ByVal type As String, ByVal state As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim links As String = Nothing
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Try
            Select Case state
                Case "AR"
                    Select Case type
                        Case "R"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_r_AR' order by listorder"
                        Case "B"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_b_AR' order by listorder"
                        Case "E"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_e_AR' order by listorder"
                    End Select
                Case "KS"
                    Select Case type
                        Case "R"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_r_KS' order by listorder"
                        Case "B"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_b_KS' order by listorder"
                    End Select
                Case "MO"
                    Select Case type
                        Case "R"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_r_MO' order by listorder"
                        Case "B"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_b_MO' order by listorder"
                    End Select
                Case "OK"
                    Select Case type
                        Case "R"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_r_OK' order by listorder"
                        Case "B"
                            cmd.CommandText = "select title,url,newwindow from links where type = 'energysolutionselectric_b_OK' order by listorder"
                    End Select
                Case "EET"
                    cmd.CommandText = "select title,url,newwindow from links where type = 'energycalculators' order by listorder"
            End Select

            conn.Open()
            cmd.Connection = conn
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                If String.IsNullOrEmpty(links) Then  'start table if its not already started
                    links += "<table cellpadding='4.5' cellspacing='0' style='border-spacing:0;border-collapse:collapse'>"
                End If
                While dr.Read
                    links += "<tr><td>&nbsp&nbsp&nbsp&nbsp</td>"
                    If Left(dr(1).ToString, 2) = "~/" Then  'internal link
                        links += "<td><a href='" & Page.ResolveUrl(dr(1).ToString) & "' runat='server'>" & dr(0).ToString & "</a></td>"
                    Else
                        links += "<td><a href='" & dr(1).ToString & "' target='_blank' runat='server'>" & dr(0).ToString & "</a></td>"
                    End If
                    links += "</tr>"
                End While
            End If
            dr.Close()

            If Not String.IsNullOrEmpty(links) Then  'end table if its already started
                links &= "</table>"
            End If
            conn.Close()
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return links
    End Function

End Class