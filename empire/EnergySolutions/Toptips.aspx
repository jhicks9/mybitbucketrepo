﻿<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Toptips.aspx.vb" Inherits="EnergySolutions_Toptips" title="Top Tips" %>
<%--<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .toptips ul {list-style: outside disc;margin-left: 1em;margin-top:0}
        .toptips ul li {position:relative;left:1em;padding-right:1em}
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style='float:left;width:400px;background:#ffffff;margin:0px 10px 5px 0px;'>
            <asp:Image ID="imgTopTips1" runat="server" ImageUrl="~/images/toptips1.jpg" />
    </div>
    <div class="toptips">
        <h3>Maximize Your Home’s Energy Efficiency While Minimizing Your Energy Bill</h3>
        The way you use electricity at home offers great opportunities for energy savings.  These simple, low- or no-cost tips can assist you in making your energy decisions and in gaining greater control over your electric bill.
        <br /><br />

        <asp:Panel ID="pnlSummer" runat="server" Visible="false">
            <strong><i>Summer Tips</i></strong>
            <ul>
                <li>Direct sunlight falling on a window air conditioning unit increases its workload.  When a choice is possible, locate such units on the north or shady side of the house.</li>
                <li>Set the cooling thermostat as high as comfort will permit. The higher the setting, the more energy you will save.</li>
                <li>Close cooling vents and turn off window air conditioners in unused rooms. Keep doors to unused rooms closed.</li>
                <li>Draw blinds, shades or drapes to block the sunlight during the hottest part of the day, especially on south- and west-facing windows. Exterior shading, trees or awnings, are the most effective, but interior shading will help.</li>
                <li>Don’t position heat-producing devices such as lamps and TV sets beneath a wall-mounted thermostat for a central cooling system. Heat rising from the equipment could cause the thermostat to read a temperature higher than the true room temperature and lead to overcooling.</li>
                <li>Complete heat and/or steam producing activities after 8 p.m. or before 8 a.m. to reduce the load on your AC. This includes laundry, dishwashing, cooking, showers or baths.</li>
            </ul>
        </asp:Panel>
        <asp:Panel ID="pnlWinter" runat="server" Visible="false">
            <strong><i>Winter Tips</i></strong>
            <ul>
                <li>In the winter, the air is normally dry inside your house. This is a disadvantage because, to be comfortable in dry air, people typically require a higher temperature than they would in a humid environment. Therefore, efficient humidifiers are a good investment for energy conservation.</li>
                <li>For holiday lighting, consider using LED lights. Not only will LED lights reduce electric use by more than 90 percent compared to traditional incandescent holiday light bulbs, they will last more than 25,000 hours.</li>
                <li>Locate the heating thermostat on an inside wall and away from windows and doors. Cold drafts will cause the thermostat to keep the system running even when the rest of the house is warm enough.</li>
                <li>Set the heating thermostat as low as comfort permits. For instance, each degree above 68°F can add 3 percent to the amount of energy needed for heating. Turn down your thermostat 5 to 10 degrees at bedtime.</li>
                <li>Raise shades and open curtains during the day. Close shades and curtains at night.</li>
            </ul>
        </asp:Panel>

        <center><u>Energy Efficiency Improvements Anytime</u></center>
        <br />
        <strong><i>Heading & Cooling</i></strong>
        <ul>
            <li>Inspect your heating and cooling equipment. Replace old, outdated appliances with energy efficient models. When buying new appliances compare energy efficiency ratings and annual operating costs. Plus, check to see if your new equipment qualifies for an <asp:hyperlink id="hlRebate" runat="server" NavigateUrl="~/EnergySolutions/Electric.aspx" Text="Empire rebate" />.</li>
            <li>Check to see if both attic and basement have recommended levels of insulation.</li>
            <li>Replace your central heat/air filter once a month. Dirty filters restrict airflow significantly increasing energy use.</li>
            <li>Remove furniture or items which block vents supplying and returning air to the AC or furnace.</li>
            <li>Weather strip and caulk around all entrance doors and windows to limit air leaks. In a typical home, air leaks can be responsible for 25 to 40 percent of the energy used for heating and cooling.</li>
            <li>Consider installing a programmable thermostat. It can be programmed to a preset schedule, so it saves you money and hassle.</li>
            <li>Open windows during the moderate weather of spring and fall for cooling instead of operating air conditioning equipment.</li>
        </ul>
        <br />
        <strong><i>Cooking</i></strong>
        <ul>
            <li>Vacuum clean the condenser coils of refrigerators and freezers (in the back or at the bottom of cabinets) every three months or so. Dust covered coils impair the efficiency of operation and increase energy usage.</li>
            <li>Don’t keep your refrigerator or freezer colder than necessary.  For the food compartments, recommended temperatures are 37-40 degrees for the refrigerator and 5 degrees for the freezer.</li>
            <li>Do not place uncovered liquids in refrigerators. In addition to absorbing undesirable flavors, the liquids give off vapors that add to the compressor workload.</li>
            <li>Allow hot foods or liquids to cool off before placing them in the refrigerator.</li>
            <li>Plan ahead and remove all ingredients for each meal at one time. Each time the door of a refrigerator or freezer is opened, its compressor has to run a bit longer to replace the cold air that spills out.</li>
            <li>Use your microwave whenever possible.  Microwaves draw less than half the power of their conventional counterparts and cook for a much shorter period of time. Using a microwave can reduce your energy used for cooking by more than 50 percent.</li>
            <li>Foods with different cooking temperatures can often be cooked simultaneously at one temperature—variations of 25 degrees in either direction still produce good results and save energy.</li>
            <li>When  roasting  or  baking,  avoid  making  frequent  progress checks that  involve  opening the oven  door.  Each time the door is opened, a considerable portion of the oven’s heat escapes.</li>
            <li>Overall, dishwashers use less water than washing dishes by hand. For a full load of dishes in the dishwasher, washing the same dishes by hand would typically use at least 6 more gallons of hot water.</li>
            <li>Many dishwashers have an option for “air drying” or “heated drying.” The “air drying” setting will use less energy.</li>
        </ul>
        <br />
        <strong><i>Water Heating & Laundry</i></strong>
        <ul>
            <li>It is important to keep a water heater properly maintained. Once or twice a year, drain a bucket of water out of the bottom of the heater tank because it can be full of sediment.  The sediment insulates the water in the tank from the heating element, which wastes energy.</li>
            <li>Lowering your water heater temperature setting from 140°F to 120°F can reduce your water heating energy bill by more than 10 percent.</li>
            <li>Letting the water run while shaving is needless waste. Avoid this by using sink stoppers and dishpans.</li>
            <li>Encourage family members to take showers rather than baths.  The average person will use about half as much hot water in a shower as in a bath.</li>
            <li>Set the wash temperature selector to cold or warm and the rinse temperature to cold as often as possible. Sort laundry and schedule washes so that a complete job can be done with a few cycles of the machine carrying its full capacity rather than a greater number of cycles with light loads.</li>
            <li>Avoid over-drying. This not only wastes energy, but harms fabrics as well.</li>
            <li>Dry towels and heavier cottons in a separate load from clothes with lighter weights.</li>
        </ul>
        <br />
        <strong><i>Lighting</i></strong>
        <ul>
            <li>Select the type of light bulb on the basis of its efficiency. Compact fluorescent bulbs will give an incandescent bulb’s warm soft light, while using 75 percent less electricity. They also last about 8 to 10 times longer.</li>
            <li>If you don’t like the “look” of compact fluorescent lighting, consider high-efficiency halogen lighting.  For example, a 100-watt incandescent bulb can be replaced by a 72-watt or 70-watt halogen bulb.  A 60-watt incandescent bulb can be replaced by a 42-watt or 40-watt halogen bulb.</li>
            <li>Clean lighting fixtures regularly. Dust on lamps and reflectors impair lighting efficiency.</li>
        </ul>
        <br />
    
        <div style="float:right;width:400px;margin:0 0 5px 15px">
            <asp:Image ID="imgTopTips2" runat="server" ImageUrl="~/images/toptips2.jpg" />
        </div>
        <strong><i>Electronics</i></strong>
        <ul>
            <li>The typical U.S. home now has, on average, 25 electronic products, 99 percent of which must be plugged in or recharged. Turn off these products when they’re not in use. Or, use a power strip as a central “turn off” point when you’re finished using equipment. This will help to eliminate the standby power consumption used by many electronics even when they are turned off.</li>
            <li>Unplug any battery chargers or power adapters when electronics are fully charged or disconnected from the charger.</li>
            <li>In the market for a new television? Once you decide on the size, remember that an LCD TV will typically use less electricity than a plasma TV.</li>
            <li>With an LCD TV, turn down the LCD backlight. By turning down the backlight, you’ll lower power consumption and also make the TV less bright.</li>
            <li>Many LCD TVs are set on a “retail” setting, which is the brightest setting for use in retail stores. Most LCD TVs have a “home” setting, which is more appropriate for home use and will save energy.</li>
            <li>Look for the ENERGY STAR® label when shopping for a variety of electronics and appliances, such as dehumidifiers, ceiling fans, battery chargers, DVD players/recorders, Blu-ray players, cordless phones, home stereo systems or cable boxes.</li>
            <li>Computer screen savers may save screens, but they do not save energy. Make sure the screen saver does not deactivate your computer’s sleep mode. You can set the computer to operate the screen saver, then go into the sleep mode.</li>
            <li>If  you  are  not  going  to  use  your  personal computer  for  more  than  20  minutes,  turn  off  your  monitor.  If you are not going to use your computer for more than 2 hours, turn off your monitor and CPU. It takes a lot more energy to have your computer running than the energy it takes to start it.</li>
            <li>Different video game consoles use different amounts of energy.  Consider this factor when using or buying a console. A study by the Electric Power Research Institute showed after an hour of active play, the PlayStation 3 used an average of 84.8 watts, the Xbox 360 used 87.9 watts, and the Nintendo Wii used 13.7 watts.</li>
            <li>It is important to turn off your video game console when it is not in use.    Most game consoles use as much energy in the “idle” mode as in the “active” or “game on” mode. The annual electricity usage for a video game console that is always turned on is nearly 10 times as much as a console that is turned off when not in use.</li>
        </ul>
    </div><%--end top tips--%>
</asp:Content>