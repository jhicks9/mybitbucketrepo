
Partial Class EnergySolutions_Toptips
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim season As Integer = Now.DayOfYear
        '#354 First day of Winter
        '#79  First day of Spring
        '#263 First day of Fall
        '#171 First day of Summer
        If (season < 79) Or (season >= 354) Then  'Winter
            pnlWinter.Visible = true
        End If
        If (season >= 79) And (season < 171) Then  'Spring
            pnlSummer.Visible = true
        End If
        If (season >= 171) And (season < 263) Then  'Summer
            pnlSummer.Visible = true
        End If
        If (season >= 263) And (season < 354) Then  'Fall
            pnlWinter.Visible = true
        End If
    End Sub
End Class
