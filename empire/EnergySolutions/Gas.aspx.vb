Imports System.Data
Imports System.Data.SqlClient

Partial Class EnergySolutions_Gas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lbl_RHeader.Text = ""
            lbl_R.Text = ""
            lbl_BHeader.Text = ""
            lbl_B.Text = ""
            lblcalc.Text = ""
            If String.IsNullOrEmpty(Request.QueryString("gas")) Then
                BuildLinks()
                pnlTools.Visible = False
                pnlMO.Visible = True
                lbMO.Font.Bold = True
                lbEET.Font.Bold = False
            Else
                BuildEETLinks()
                pnlMO.Visible = False
                pnlTools.Visible = True
                lbEET.Font.Bold = True
                lbMO.Font.Bold = False
            End If
        Catch
        End Try
    End Sub

    Public Sub BuildEETLinks()
        Try
            Dim links As String = ""
            Dim sql As String = ""

            links = "<b>Energy Efficiency Tools</b><br /><br /><table cellpadding='3' cellspacing='0'>"
            sql = "select title,url,newwindow from links where type = 'energycalculators_gas' order by listorder"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd0 As New SqlCommand(sql, conn)
            Dim dr0 As SqlDataReader
            dr0 = cmd0.ExecuteReader
            While dr0.Read
                links += "<tr><td>&nbsp;</td>"
                If Left(dr0(1).ToString, 2) = "~/" Then  'if link is "internal"
                    links += "<td><a href='" & Page.ResolveUrl(dr0(1).ToString) & "'"
                Else
                    links += "<td><a href='" & dr0(1).ToString & "'"
                End If

                If dr0(2).ToString Then  'if newwindow is true
                    links += " target='_blank'"
                End If
                links += " runat='server'>"
                links += dr0(0).ToString & "</a></td></tr>"
            End While
            links += "</table>"
            dr0.Close()
            conn.Close()

            lblcalc.Text = links
        Catch
        End Try
    End Sub

    Public Sub BuildLinks()
        Try
            Dim links As String = ""
            Dim sql As String = ""
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd As New SqlCommand
            Dim dr As SqlDataReader
            conn.Open()
            cmd.Connection = conn

            'Build Gas Residential
            lbl_R.Text &= "<table cellpadding='3' cellspacing='0'>"
            sql = "select id,webdescription from documents where groupid = 14 and doctypeid = 96 order by webdescription"
            cmd.CommandText = sql
            dr = cmd.ExecuteReader
            While dr.Read
                links += "<tr>"
                links += "<td><img alt='' src='" & ResolveUrl("~/images/icon/icon_document.gif") & "' /></td>"
                links += "<td><a href='" & Page.ResolveUrl("~/Dochandler.ashx?id=" & dr(0).ToString) & "' target='_blank' runat='server'>" & dr(1).ToString & "</a></td>"
                links += "</tr>"
            End While
            dr.Close()
            sql = "select title,url,newwindow from links where type = 'energysolutionsgas_r_MO' order by listorder"
            cmd.CommandText = sql
            dr = cmd.ExecuteReader
            While dr.Read
                links += "<tr><td>&nbsp</td>"
                If Left(dr(1).ToString, 2) = "~/" Then  'internal link
                    links += "<td><a href='" & Page.ResolveUrl(dr(1).ToString) & "' runat='server'>" & dr(0).ToString & "</a></td>"
                Else
                    links += "<td><a href='" & dr(1).ToString & "' target='_blank' runat='server'>" & dr(0).ToString & "</a></td>"
                End If
                links += "</tr>"
            End While
            dr.Close()
            If Len(links) > 0 Then
                lbl_RHeader.Text = "<b>Residential</b><br /><br />"
                lbl_R.Text &= links
            End If
            lbl_R.Text &= "</table>"

            'Build Gas Business
            lbl_B.Text &= "<table cellpadding='3' cellspacing='0'>"
            links = ""
            sql = "select id,webdescription from documents where groupid = 14 and doctypeid = 97 order by webdescription"
            cmd.CommandText = sql
            dr = cmd.ExecuteReader
            While dr.Read
                links += "<tr>"
                links += "<td><img alt='' src='" & ResolveUrl("~/images/icon/icon_document.gif") & "' /></td>"
                links += "<td><a href='" & Page.ResolveUrl("~/Dochandler.ashx?id=" & dr(0).ToString) & "' target='_blank' runat='server'>" & dr(1).ToString & "</a></td>"
                links += "</tr>"
            End While
            dr.Close()
            sql = "select title,url,newwindow from links where type = 'energysolutionsgas_b_MO' order by listorder"
            cmd.CommandText = sql
            dr = cmd.ExecuteReader
            While dr.Read
                links += "<tr><td>&nbsp</td>"
                If Left(dr(1).ToString, 2) = "~/" Then  'internal link
                    links += "<td><a href='" & Page.ResolveUrl(dr(1).ToString) & "' runat='server'>" & dr(0).ToString & "</a></td>"
                Else
                    links += "<td><a href='" & dr(1).ToString & "' target='_blank' runat='server'>" & dr(0).ToString & "</a></td>"
                End If
                links += "</tr>"
            End While
            dr.Close()
            If Len(links) > 0 Then
                If String.IsNullOrEmpty(lbl_R.Text) Then
                    lbl_BHeader.Text = ""
                Else
                    lbl_BHeader.Text = "<br /><hr style='color:#113962;height:1px;'/>"
                End If
                lbl_BHeader.Text &= "<b>Business</b><br /><br />"
                lbl_B.Text &= links
            End If
            lbl_B.Text &= "</table>"
        Catch
        End Try
    End Sub

End Class
