<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="Electric.aspx.vb" Inherits="EnergySolutions_Electric" title="Smart Energy Solutions Electric" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <acrobat:uc4 ID="uc4" runat="server" />
        
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lbAR" runat="server" CausesValidation="true" PostBackUrl="~/EnergySolutions/Electric.aspx?electric=AR" Text="Arkansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbKS" runat="server" CausesValidation="true" PostBackUrl="~/EnergySolutions/Electric.aspx?electric=KS" Text="Kansas"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbMO" runat="server" CausesValidation="true" PostBackUrl="~/EnergySolutions/Electric.aspx?" Text="Missouri"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbOK" runat="server" CausesValidation="true" PostBackUrl="~/EnergySolutions/Electric.aspx?electric=OK" Text="Oklahoma"/>&nbsp&nbsp|&nbsp
            <asp:LinkButton ID="lbEET" runat="server" CausesValidation="true" PostBackUrl="~/EnergySolutions/Electric.aspx?electric=EET" Text="Energy Efficiency Tools"/>
            <br />&nbsp

                <asp:Panel ID="pnlElectric" runat="server" HeaderText="Electric">
                    <ContentTemplate>
                        <asp:Label ID="lblRES" runat="server" font-bold="true" Visible="false" Text="" /><br />
                        <asp:DataList ID="dlRES" runat="server" SkinID="DataList" Visible="false">
                            <itemtemplate>
                                <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription")%>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}")%>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSRES" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="">
                        </asp:SqlDataSource>
                        <asp:Label ID="lblRESLINKS" runat="server" Text="" Visible="false" />
                        <br />
                        <div id="divHr1" runat="server" visible="false"><hr style="color:#113962;height:1px;"/></div>
                        <asp:Label ID="lblBUS" runat="server" font-bold="true" Visible="false" Text="" /><br />
                        <asp:DataList ID="dlBUS" runat="server" SkinID="DataList" Visible="false">
                            <itemtemplate>
                                <asp:Image ID="Image2" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink2" Text='<%#Eval("DocDescription")%>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}")%>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSBUS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="">
                        </asp:SqlDataSource>
                        <asp:Label ID="lblBUSLINKS" runat="server" Text="" Visible="false" />
                        <br />
                        <div id="divHr2" runat="server" visible="false"><hr style="color:#113962;height:1px;" visible="false"/></div>
                        <asp:Label ID="lblEDU" runat="server" font-bold="true" Visible="false" Text="" /><br />
                        <asp:DataList ID="dlEDU" runat="server" SkinID="DataList" Visible="false">
                            <itemtemplate>
                                <asp:Image ID="Image3" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.gif" runat="server" />&nbsp;
                                <asp:HyperLink ID="HyperLink3" Text='<%#Eval("DocDescription")%>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}")%>' Target="_blank" runat="server"/> 
                            </itemtemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="sqlDSEDU" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand="">
                        </asp:SqlDataSource>
                        <asp:Label ID="lblEDULINKS" runat="server" Text="" Visible="false" />
                </asp:Panel><%--end pnlElectric--%>

            <div style="width:100%;">
                <a href="http://www.energystar.gov" target="_blank"><asp:Image ID="imgEnergyStar" runat="server" ImageUrl="~/images/energystar_logo.jpg" /></a><br />
                ENERGY STAR&reg; qualified products and practices help you save money and reduce greenhouse gas emissions by meeting strict energy efficiency guidelines set by the U.S. EPA and U.S. DOE. The ENERGY STAR label also designates superior energy performance in homes and buildings.  For information on Federal Tax Credits for Energy Efficiency, visit the ENERGY STAR� Web site.<br /><br />
                Empire District proudly promotes <a href="http://www.energystar.gov" target="_blank">ENERGY STAR</a>.
            </div>                                                                                        

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

