﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class RetrievePasswordEmail
    Inherits System.Web.UI.Page

    Dim tdes As New tripleDES

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            If (Len(Request.QueryString("u")) > 0) And (Len(Request.QueryString("e")) > 0) Then
                getUserInfo()
            Else
                pnlEmailPassword.Visible = False
                lblMessage.Text = "Account not found."
            End If
        End If
        txtEmailNewPassword.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbPassword.UniqueID + "','')")
        txtEmailConfirmPassword.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbPassword.UniqueID + "','')")
    End Sub

    Public Sub getUserInfo()
        Try
            Dim user As New MembershipUser("empireMembershipProvider", tdes.Decrypt(Request.QueryString("u")), Nothing, tdes.Decrypt(Request.QueryString("e")), Nothing, Nothing, True, False, Nothing, Nothing, Nothing, Nothing, Nothing)
            Dim userInfo As MembershipUser = Membership.GetUser(tdes.Decrypt(Request.QueryString("u")))
            If Len(userInfo.CreationDate) > 0 Then 'valid user
                lblDisplayEmail.Text += tdes.Decrypt(Request.QueryString("e"))
                txtEmailNewPassword.Focus()

                Dim dt As New System.Data.DataTable("users")
                Dim drow As System.Data.DataRow
                Dim dc As DataColumn
                dc = New DataColumn("id", System.Type.GetType("System.String"))
                dt.Columns.Add(dc)
                dc = New DataColumn("username", System.Type.GetType("System.String"))
                dt.Columns.Add(dc)
                Dim query As String = "select aspnet_users.userid, aspnet_users.username from aspnet_users inner join aspnet_membership on aspnet_users.userid = aspnet_membership.userid where (aspnet_membership.email = '" & tdes.Decrypt(Request.QueryString("e")) & "')"
                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
                conn.Open()
                Dim dr As SqlDataReader
                Dim cmd As New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    drow = dt.NewRow()
                    drow.Item("id") = dr(0).ToString
                    drow.Item("username") = dr(1).ToString
                    dt.Rows.Add(drow)
                End While
                dr.Close()
                conn.Close()
                cblUserName.DataSource = dt
                cblUserName.DataValueField = "id"
                cblUserName.DataTextField = "username"
                cblUserName.DataBind()

            End If
        Catch
            pnlEmailPassword.Visible = False
            lblMessage.Text = "Account not found."
        End Try
    End Sub

    Protected Sub lbPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblMessage.Text = ""
            If EvaluateIsChecked() Then
                If ChangePassword() Then
                    pnlEmailPassword.Visible = False
                    pnlDisplay.Visible = True
                Else
                    pnlEmailPassword.Visible = False
                    lblMessage.Text = "Error changing password."
                End If
            Else
                lblMessage.Text = "No account selected."
            End If
        Catch
            pnlEmailPassword.Visible = False
            lblMessage.Text = "Account not found."
        End Try
    End Sub

    Public Function ChangePassword() As Boolean
        Dim itm As ListItem
        Try
            For Each itm In cblUserName.Items
                If itm.Selected Then
                    Dim userPassword As New MembershipUser("empireMembershipProvider", itm.Text, Nothing, tdes.Decrypt(Request.QueryString("e")), Nothing, Nothing, True, False, Nothing, Nothing, Nothing, Nothing, Nothing)
                    Dim userInfo As MembershipUser = Membership.GetUser(userPassword.UserName.ToString)

                    If userInfo.IsLockedOut Then  'if account is locked out
                        userInfo.UnlockUser()  'if so, unlock account
                    End If
                    userInfo.ChangePassword(userInfo.GetPassword.ToString, txtEmailNewPassword.Text)
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function EvaluateIsChecked() As Boolean
        Try
            Dim itm As ListItem

            For Each itm In cblUserName.Items
                If itm.Selected Then
                    Return True
                End If
            Next
            Return False
        Catch
            Return False
        End Try
    End Function

End Class
