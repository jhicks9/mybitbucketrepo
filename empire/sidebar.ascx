<%@ Control Language="VB" AutoEventWireup="false" CodeFile="sidebar.ascx.vb" Inherits="sidebar" %>

<asp:Image ID="imgSideImage" Imageurl="" runat="server" Visible="false" AlternateText="" />
<asp:Label ID="lblMenu" runat="server" Text="" />

<div style="color:#fff;padding-top:15px;font:bold .9em arial,verdana,sans-serif;text-align:center;">
    <asp:Label ID="lblElectric" runat="server" text="Electric & Water Service:"></asp:Label><br />
    <img id="imgsep1" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
    <asp:Label ID="lblElectricNumber" runat="server" text="800-206-2300"></asp:Label>
    <br /><br />
    <asp:Label ID="lblGas" runat="server" text="Gas Service:"></asp:Label><br />
    <img id="imgsep2" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
    <asp:Label ID="lblGasNumber" runat="server" text="800-424-0427"></asp:Label>
    <br /><br />
    <asp:Label ID="lblGasLeak" runat="server" text="Report a Gas Leak:"></asp:Label><br />
    <img id="imgsep4" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
    <asp:Label ID="lblGasLeakNumber" runat="server" text="800-406-9220"></asp:Label>
    <br /><br />
    <asp:Label ID="lblEmail" runat="server" text="Email:"></asp:Label><br />
    <img id="imgsep3" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
    <a href="~/about/contactus.aspx" runat="server" style="color:#fff">Empire Contacts</a>
    <br /><br />
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //Append a div with hover class to all the LI
        if (!("ontouchstart" in document.documentElement)) {
            $('#sideMenu li').append('<div class="hover"><\/div>');
        }
        $('#sideMenu li').hover(
        //Mouseover, fadeIn the hidden hover class
		function () {
		    $(this).children('div').stop(true, true).fadeIn('xslow');
		},
        //Mouseout, fadeOut the hover class
		function () {
		    $(this).children('div').stop(true, true).fadeOut('xslow');
		});
    });
</script>