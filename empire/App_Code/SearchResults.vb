﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Lucene.Net
Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Collections.Generic
Imports System.Text.RegularExpressions

'-----------------------------------------------------------Crawl website--------------------------------------------------------------------
Public Class SearchResults
    Private Shared _pages As New List(Of Page)()
    Private Shared _externalUrls As New List(Of String)()
    Private Shared _otherUrls As New List(Of String)()
    Private Shared _failedUrls As New List(Of String)()
    Private Shared _exceptions As New List(Of String)()
    Private Shared _classes As New List(Of String)()
    Private Shared _logBuffer As New StringBuilder()
    'Private Shared _searchresults As New SearchResults
    Private Shared _totalHits As Integer = 0
    Private Shared crawlURL As String = "http://www.empiredistrict.com" '_searchresults.FullApplicationPath()

    Public Shared Function runCrawl() As Boolean
        Dim result As Boolean = False  'is it time for full crawl -- default to false
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Try
            conn.Open()
            cmd.CommandText = "SELECT search.date from search where search.controlrecord = 1 and search.title = '-search results-'"
            drLookup = cmd.ExecuteReader  'check for control record
            If drLookup.HasRows Then
                While drLookup.Read
                    If DateDiff(DateInterval.Hour, drLookup("date"), Now) >= 24 Then 'run full crawl once per day
                        result = True
                    Else
                        result = False
                    End If
                End While
            Else
                result = True  'no control record so run full crawl now
            End If
            drLookup.Close()
        Catch ex As Exception
            If ex.Message.Contains("Invalid object name") Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.search(id int IDENTITY(1,1) NOT NULL,controlrecord bit NOT NULL,date datetime NOT NULL,title varchar(128) NULL,url varchar(256) NULL,alttags varchar(max) NULL,html varchar(max) NULL,CONSTRAINT PK_search PRIMARY KEY CLUSTERED (id ASC))"
                cmd.ExecuteNonQuery()
            Else
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return result
    End Function

    Public Function FullApplicationPath() As String  'returns current website
        Dim fpath As String = ""
        Try
            fpath = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, String.Empty) & HttpContext.Current.Request.ApplicationPath
            fpath = fpath.Replace("https://", "http://") 'only return non ssl
        Catch
        End Try
        Return fpath
    End Function

    Public Shared Sub createSearchIndex()
        'create new lucene search indexes based on logged sql data
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            CrawlPage(crawlURL)  'crawl all pages of site placing html data in arrays
            Dim sb As StringBuilder = CreateReport()  'create the detail showing result of crawl
            'WriteReportToDisk(sb.ToString())  'used for debugging

            If Not System.IO.Directory.Exists(System.IO.Path.GetTempPath() & "webIndex") Then 'create directory if it does not exist
                System.IO.Directory.CreateDirectory(System.IO.Path.GetTempPath() & "webIndex")
            Else
            End If
            Dim tempDir As New System.IO.DirectoryInfo(System.IO.Path.GetTempPath() & "webIndex")
            Dim indexDir As Lucene.Net.Store.Directory = Lucene.Net.Store.FSDirectory.Open(tempDir)
            Dim analyzer As Analysis.Analyzer = New Analysis.Standard.StandardAnalyzer(Util.Version.LUCENE_29)
            Dim writer As Index.IndexWriter = New Index.IndexWriter(indexDir, analyzer, True, Index.IndexWriter.MaxFieldLength.UNLIMITED) 'creates *NEW* index (deletes all documents) -- locks file until writer close
            Dim doc As Documents.Document

            'write html data to sql
            logCrawlData(sb.ToString)

            ' populate search index
            Dim dsSearch As New System.Data.DataSet()
            Dim cmd As New SqlDataAdapter("SELECT search.title, search.url, search.alttags, search.html from search where search.controlrecord != 1", conn)
            conn.Open()
            cmd.Fill(dsSearch)
            conn.Close()
            Dim dt As DataTable = dsSearch.Tables(0)
            Dim dr As DataRow
            For Each dr In dt.Rows
                doc = New Documents.Document()
                doc.Add(New Documents.Field("title", dr("title"), Documents.Field.Store.YES, Documents.Field.Index.ANALYZED)) 'index page titles
                doc.Add(New Documents.Field("html", dr("html"), Documents.Field.Store.YES, Documents.Field.Index.ANALYZED))  'index page html text
                doc.Add(New Documents.Field("alttags", dr("alttags"), Documents.Field.Store.YES, Documents.Field.Index.ANALYZED)) 'index image(s) alt tags
                doc.Add(New Documents.Field("url", dr("url"), Documents.Field.Store.YES, Documents.Field.Index.NOT_ANALYZED))  'index actual url of page
                writer.AddDocument(doc)
            Next
            dsSearch.Dispose()
            ' end populate search index

            writer.Optimize()
            writer.Dispose()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Public Shared Function querySearchIndex(ByVal searchQuery As String) As DataSet
        Dim dsResults As New DataSet  'create dataset to return
        Dim dTable As DataTable = New DataTable("searchresults") 'create a table for dataset
        dTable.Columns.Add("title") 'add title to dataset
        dTable.Columns.Add("url")  'add url column to dataset
        dTable.Columns.Add("html")  'add html column to dataset
        Try
            Dim formattedContent As String = ""
            If Not System.IO.Directory.Exists(System.IO.Path.GetTempPath() & "webIndex") Then  'if index directory does not exist
                createSearchIndex() ' create the index
            End If
            Dim tempDir As New System.IO.DirectoryInfo(System.IO.Path.GetTempPath() & "webIndex")
            Dim indexDir As Lucene.Net.Store.Directory = Lucene.Net.Store.FSDirectory.Open(tempDir)
            Dim searcher As Search.IndexSearcher = New Search.IndexSearcher(indexDir, True)
            Dim analyzer As Analysis.Analyzer = New Analysis.Standard.StandardAnalyzer(Util.Version.LUCENE_29)
            Dim booleanQuery As Lucene.Net.Search.BooleanQuery = New Lucene.Net.Search.BooleanQuery

            Dim queryTextParse As Lucene.Net.QueryParsers.MultiFieldQueryParser = New Lucene.Net.QueryParsers.MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, New String() {"html", "title", "alttags"}, analyzer)
            'Dim queryText As Lucene.Net.Search.Query = queryTextParse.Parse("(""provide electric""~10 || (provide || electric))")
            Dim queryText As Lucene.Net.Search.Query = queryTextParse.Parse("""" & searchQuery & """~7")
            booleanQuery.Add(queryText, Search.Occur.MUST)

            'Dim terms As String() = searchQuery.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)  'fuzzy search
            'Dim fzBooleanQuery As Lucene.Net.Search.BooleanQuery = New Lucene.Net.Search.BooleanQuery
            'For Each term As String In terms
            '    fzBooleanQuery.Add(queryTextParse.Parse(term.Replace("~", "") & "~0.5"), Search.Occur.SHOULD)
            'Next
            'booleanQuery.Add(New Lucene.Net.Search.BooleanClause(fzBooleanQuery, Search.Occur.MUST))

            Dim topDocs As Lucene.Net.Search.TopDocs = searcher.Search(booleanQuery, 100)
            _totalHits = topDocs.TotalHits
            Dim scoredocs() = topDocs.ScoreDocs
            For Each scoredoc In scoredocs
                Dim doc As Lucene.Net.Documents.Document = searcher.Doc(scoredoc.Doc)
                If Len(doc.GetField("html").StringValue) >= 256 Then  'append elipses if length of content  > 256
                    formattedContent = Left(doc.GetField("html").StringValue, 256) & "..."
                Else
                    formattedContent = doc.GetField("html").StringValue  'otherwise display full content
                End If
                dTable.Rows.Add(doc.GetField("title").StringValue, doc.GetField("url").StringValue, formattedContent)
            Next
        Catch
            _totalHits = 0
        End Try
        dsResults.Tables.Add(dTable)  'add table to dataset
        Return dsResults
    End Function

    Public Shared Function totalHits() As Integer
        Return _totalHits
    End Function

    Private Shared Sub CrawlPage(url As String)
        If Not PageHasBeenCrawled(url) Then

            Dim htmlText As String = GetWebText(url)
            Dim formattedLink As String = ""

            Dim page As New Page()
            page.Text = htmlText
            page.Url = url
            page.CalculateViewstateSize()
            page.GetTitle()
            _pages.Add(page)

            Dim linkParser As New LinkParser()
            linkParser.ParseLinks(page, url)

            Dim classParser As New CSSClassParser()
            classParser.ParseForCssClasses(page)

            'Add data to main data lists
            AddRangeButNoDuplicates(_externalUrls, linkParser.ExternalUrls)
            AddRangeButNoDuplicates(_otherUrls, linkParser.OtherUrls)
            AddRangeButNoDuplicates(_failedUrls, linkParser.BadUrls)
            AddRangeButNoDuplicates(_classes, classParser.Classes)

            For Each exception As String In linkParser.Exceptions
                _exceptions.Add(exception)
            Next

            'Crawl all the links found on the page.
            For Each link As String In linkParser.GoodUrls
                Try
                    formattedLink = FixPath(url, link)
                    If formattedLink <> [String].Empty Then
                        CrawlPage(formattedLink)
                    End If
                Catch exc As Exception
                    _failedUrls.Add((Convert.ToString(formattedLink & Convert.ToString(" (on page at url ")) & url) + ") - " + exc.Message)
                End Try
            Next
        End If
    End Sub

    Private Shared Function IsExternalUrl(url As String, crawl As String) As Boolean
        Dim evalUrl As String = System.Text.RegularExpressions.Regex.Replace(url, "(?s)http.*?//", "") 'remove http or https
        Dim evalCrawl As String = System.Text.RegularExpressions.Regex.Replace(crawl, "(?s)http.*?//", "") 'remove http or https

        If evalUrl.IndexOf(evalCrawl) > -1 Then 'if url contains same as link being crawled it is not external
            Return False
        ElseIf url.Substring(0, 7) = "http://" OrElse url.Substring(0, 3) = "www" OrElse url.Substring(0, 8) = "https://" Then
            Return True
        End If
        Return False
    End Function

    Private Shared Function PageHasBeenCrawled(url As String) As Boolean
        For Each page As Page In _pages
            If page.Url.ToLower = url.ToLower Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Shared Sub AddRangeButNoDuplicates(targetList As List(Of String), sourceList As List(Of String))
        For Each str As String In sourceList
            If Not targetList.Contains(str) Then
                targetList.Add(str)
            End If
        Next
    End Sub

    Private Shared Function GetWebText(url As String) As String
        Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create(url), HttpWebRequest)
        request.UserAgent = "A .NET Web Crawler"
        request.CachePolicy = New Net.Cache.HttpRequestCachePolicy(Net.Cache.HttpRequestCacheLevel.NoCacheNoStore) 'do not cache

        Dim response As WebResponse = request.GetResponse()
        Dim stream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(stream)
        Dim htmlText As String = reader.ReadToEnd()
        Return htmlText
    End Function

    Public Shared Function FixPath(originatingUrl As String, link As String) As String
        Dim formattedLink As String = [String].Empty
        Dim virtual As String = Microsoft.VisualBasic.Right(crawlURL, (Len(crawlURL) - crawlURL.LastIndexOf("/") - 1))

        If link.IndexOf("http://") <> -1 Or link.IndexOf("https://") <> -1 Then  'no need to process
            If IsExternalUrl(link, crawlURL) Then  'if link is external empty the string so it wont crawl
                formattedLink = String.Empty
            Else
                formattedLink = link
            End If
        ElseIf link.IndexOf("../") > -1 Then
            formattedLink = ResolveRelativePaths(link, originatingUrl)
        ElseIf link.IndexOf("/") = -1 Or Microsoft.VisualBasic.Left(link, 1) <> "/" Then
            ' ** this will not cover 100% of all generated links
            If originatingUrl = crawlURL Or link.IndexOf("/") = -1 Then  'single file name only
                If originatingUrl.LastIndexOf("/") > 7 And originatingUrl <> crawlURL Then  'check for symbol '/' past the 'http://' usually included in most url
                    formattedLink = Microsoft.VisualBasic.Left(originatingUrl, originatingUrl.LastIndexOf("/")) & "/" & link
                Else
                    formattedLink = crawlURL & "/" & link
                End If
            Else
                If originatingUrl.Contains(link) Then
                    formattedLink = Microsoft.VisualBasic.Left(originatingUrl, Len(originatingUrl) - Len(link)) & link
                Else 'otherwise skip as not able to process
                    formattedLink = crawlURL & "/" & link
                End If
            End If
            ' ** end generated links
        ElseIf Microsoft.VisualBasic.Left(link, 1) = "/" Then
            If Microsoft.VisualBasic.Left(link, Len(virtual) + 1) = ("/" + virtual) Then
                formattedLink = crawlURL & Microsoft.VisualBasic.Right(link, Len(link) - Len(virtual) - 1)
            Else
                formattedLink = crawlURL & link
            End If
        Else
            formattedLink = originatingUrl.Substring(0, originatingUrl.LastIndexOf("/")) & link
        End If
        Return formattedLink
    End Function

    Private Shared Function ResolveRelativePaths(relativeUrl As String, originatingUrl As String) As String
        Dim resolvedUrl As String = [String].Empty

        Dim relativeUrlArray As String() = relativeUrl.Split(New Char() {"/"c}, StringSplitOptions.RemoveEmptyEntries)
        Dim originatingUrlElements As String() = originatingUrl.Split(New Char() {"/"c}, StringSplitOptions.RemoveEmptyEntries)
        Dim indexOfFirstNonRelativePathElement As Integer = 0
        For i As Integer = 0 To relativeUrlArray.Length - 1
            If relativeUrlArray(i) <> ".." Then
                indexOfFirstNonRelativePathElement = i
                Exit For
            End If
        Next

        Dim countOfOriginatingUrlElementsToUse As Integer = originatingUrlElements.Length - indexOfFirstNonRelativePathElement - 1
        For i As Integer = 0 To countOfOriginatingUrlElementsToUse - 1
            If originatingUrlElements(i) = "http:" OrElse originatingUrlElements(i) = "https:" Then
                resolvedUrl += originatingUrlElements(i) + "//"
            Else
                resolvedUrl += originatingUrlElements(i) + "/"
            End If
        Next

        For i As Integer = 0 To relativeUrlArray.Length - 1
            If i >= indexOfFirstNonRelativePathElement Then
                resolvedUrl += relativeUrlArray(i)

                If i < relativeUrlArray.Length - 1 Then
                    resolvedUrl += "/"
                End If
            End If
        Next
        Return resolvedUrl
    End Function

    Private Shared Function altTags(ByVal HTML As String) As String
        Dim output As String = ""
        Try
            Dim pattern As String = "(?i)<\s*img.*?alt\s*=\s*(""|')(?<alt>[^""|']*)(""|').*?>"  'img with an alt tag
            Dim rx As Regex = New Regex(pattern)
            Dim matches As MatchCollection = Regex.Matches(HTML, pattern)
            For Each m As Match In matches
                For Each c As Capture In m.Captures
                    output &= " " & rx.Replace(c.Value, "${alt}") 'strip alt text from img
                Next
            Next
        Catch ex As Exception
        End Try
        Return output
    End Function

    Private Shared Function StripTags(ByVal HTML As String) As String
        ' Removes tags from passed HTML
        Dim m As Match  ''process only page content.  no need for navigation headers or footers
        m = Regex.Match(HTML, "(?s)<div class=(""|')content.*?</div><!-- /content -->")  'mobile site
        If m.Success Then
            HTML = m.Groups(0).Value
        Else
            m = Regex.Match(HTML, "(?s)<div class=(""|')content.*?<div class=(""|')footer(""|')>")  'empire site
            If m.Success Then
                HTML = m.Groups(0).Value
                HTML = System.Text.RegularExpressions.Regex.Replace(HTML, "(?s)<td class=(""|')leftcol.*?</td>", "") 'remove sidebar navigation
                HTML = System.Text.RegularExpressions.Regex.Replace(HTML, "(?s)<div class=(""|')breadcrumb.*?</div>", "") 'remove breadcrumb navigation
            Else
                m = Regex.Match(HTML, "(?s)<section id=(""|')content.*?</section>")  'html 5
                If m.Success Then
                    HTML = m.Groups(0).Value
                End If
            End If
        End If
        HTML = HTML.Replace("'", "''")  'single quotes cause sql statements to fail
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, "(?s)<script.*?(/>|</script>)", "") 'remove javascript
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, "<br\s*/>", " ")  'replace returns with a space.
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, "<[^>]*>", "")  'remove html tags but leave content
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, "\s{3,}", " ") 'remove whitespace (with more than two spaces in a row)
        Return HTML
    End Function

    Private Shared Sub logCrawlData(ByVal htmlReport As String)
        Dim crExists As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Try
            conn.Open()
            If _pages.Count > 0 Then
                cmd.CommandText = "delete from search where search.controlrecord=0"
                cmd.ExecuteNonQuery()
            End If
            For Each page As Page In _pages
                cmd.CommandText = "insert into search (controlrecord,date,title,url,alttags,html) VALUES (0,GETDATE(),'" & page.Title & "','" & page.Url & "','" & altTags(page.Text) & "','" & StripTags(page.Text) & "')"
                cmd.ExecuteNonQuery()
            Next

            cmd.CommandText = "SELECT count(*)as total from search where search.controlrecord = 1 and search.title = '-search results-'"
            drLookup = cmd.ExecuteReader  'check for control record
            If drLookup.HasRows Then
                While drLookup.Read
                    crExists = drLookup("total").ToString
                End While
            End If
            drLookup.Close()

            If crExists Then  'if control record exists then update
                cmd.CommandText = " update search set search.date=GETDATE(),search.html='" & htmlReport.Replace("'", "''") & "' where search.controlrecord = 1 and search.title='-search results-'"
            Else  'insert a new control record
                cmd.CommandText = "insert into search (controlrecord,date,title,html) VALUES (1,GETDATE(),'-search results-','" & htmlReport.Replace("'", "''") & "')"
            End If
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Shared Function CreateReport() As StringBuilder
        Dim sb As New StringBuilder()

        sb.Append("<html><head><title>Crawl Report</title><style>")
        sb.Append("table { border: solid 3px black; border-collapse: collapse; }")
        sb.Append("table tr th { font-weight: bold; padding: 3px; padding-left: 10px; padding-right: 10px; }")
        sb.Append("table tr td { border: solid 1px black; padding: 3px;}")
        sb.Append("h1, h2, p { font-family: Rockwell; }")
        sb.Append("p { font-family: Rockwell; font-size: smaller; }")
        sb.Append("h2 { margin-top: 45px; }")
        sb.Append("</style></head><body>")
        sb.Append("<h1>Crawl Report</h1>")
        sb.Append(Now.ToString)
        sb.Append("<h2>Internal Urls - In Order Crawled</h2>")
        sb.Append("<p>These are the pages found within the site. The size is calculated by getting value of the Length of the text of the response text. This is the order in which they were crawled.</p>")
        sb.Append("<table><tr><th>Page Size</th><th>Viewstate Size</th><th>Title</th><th>Url</th></tr>")
        For Each page As Page In _pages
            sb.Append("<tr><td>")
            sb.Append(page.Size.ToString())
            sb.Append("</td><td>")
            sb.Append(page.ViewstateSize.ToString())
            sb.Append("</td><td>")
            sb.Append(page.Title)
            sb.Append("</td><td>")
            sb.Append(page.Url)
            sb.Append("</td></tr>")
        Next
        sb.Append("</table>")

        sb.Append("<h2>External Urls</h2>")
        sb.Append("<p>These are the links to the pages outside the site.</p>")
        sb.Append("<table><tr><th>Url</th></tr>")
        For Each str As String In _externalUrls
            sb.Append("<tr><td>")
            sb.Append(str)
            sb.Append("</td></tr>")
        Next
        sb.Append("</table>")

        sb.Append("<h2>Other Urls</h2>")
        sb.Append("<p>These are the links to things on the site that are not html files (html, aspx, etc.), like images and css files.</p>")
        sb.Append("<table><tr><th>Url</th></tr>")
        For Each str As String In _otherUrls
            sb.Append("<tr><td>")
            sb.Append(str)
            sb.Append("</td></tr>")
        Next
        sb.Append("</table>")

        sb.Append("<h2>Bad Urls</h2>")
        sb.Append("<p>Any bad urls will be listed here.</p>")

        sb.Append("<table><tr><th>Url</th></tr>")
        If _failedUrls.Count > 0 Then
            For Each str As String In _failedUrls
                sb.Append("<tr><td>")
                sb.Append(str)
                sb.Append("</td></tr>")
            Next
        Else
            sb.Append("<tr><td>No bad urls.</td></tr>")
        End If
        sb.Append("</table>")

        sb.Append("<h2>Exceptions</h2>")
        sb.Append("<p>Any exceptions that were thrown would be shown here.</p>")
        sb.Append("<table><tr><th>Exception</th></tr>")
        If _exceptions.Count > 0 Then
            For Each str As String In _exceptions
                sb.Append("<tr><td>")
                sb.Append(str)
                sb.Append("</td></tr>")
            Next
        Else
            sb.Append("<tr><td>No exceptions thrown.</td></tr>")
        End If
        sb.Append("</table>")

        sb.Append("<h2>Css Classes</h2>")
        sb.Append("<p>These are the css classes used on the site, just in case you want to know which ones you're using and compare that with your css...</p>")
        sb.Append("<table><tr><th>Class</th></tr>")
        If _classes.Count > 0 Then
            For Each str As String In _classes
                sb.Append("<tr><td>")
                sb.Append(str)
                sb.Append("</td></tr>")
            Next
        Else
            sb.Append("<tr><td>No classes found.</td></tr>")
        End If
        sb.Append("</table>")
        sb.Append("</body></html>")
        Return sb
    End Function
    Private Shared Sub WriteReportToDisk(contents As String)
        Dim fileName As String = System.IO.Path.GetTempPath() & "crawlResults.htm"
        Dim fStream As FileStream = Nothing
        If File.Exists(fileName) Then
            File.Delete(fileName)
            fStream = File.Create(fileName)
        Else
            fStream = File.OpenWrite(fileName)
        End If

        Using writer As TextWriter = New StreamWriter(fStream)
            writer.WriteLine(contents)
            writer.Flush()
        End Using
        fStream.Dispose()
    End Sub
End Class

Public Class Page
    Public Sub New()
    End Sub

    Private _size As Integer
    Private _text As String
    Private _url As String
    Private _title As String
    Private _viewstateSize As Integer

    Public ReadOnly Property Size() As Integer
        Get
            Return _size
        End Get
    End Property

    Public Property Text() As String
        Get
            Return _text
        End Get
        Set(value As String)
            _text = value
            _size = value.Length
        End Set
    End Property

    Public Property Url() As String
        Get
            Return _url
        End Get
        Set(value As String)
            _url = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(value As String)
            _title = value
        End Set
    End Property

    Public Property ViewstateSize() As Integer
        Get
            Return _viewstateSize
        End Get
        Set(value As Integer)
            _viewstateSize = value
        End Set
    End Property

    Public Sub CalculateViewstateSize()
        Dim startingIndex As Integer = Text.IndexOf("id=""__VIEWSTATE""")
        If startingIndex > -1 Then
            Dim indexOfViewstateValueNode As Integer = Text.IndexOf("value=""", startingIndex)
            Dim indexOfClosingQuotationMark As Integer = Text.IndexOf("""", indexOfViewstateValueNode + 7)
            Dim viewstateValue As String = Text.Substring(indexOfViewstateValueNode + 7, indexOfClosingQuotationMark - (indexOfViewstateValueNode + 7))
            ViewstateSize = viewstateValue.Length
        End If
    End Sub
    Public Sub GetTitle()
        Dim m As Match
        m = Regex.Match(Text, "(?s)<title>.*?</title>")
        If m.Success Then
            Title = m.Groups(0).Value
        Else
            Title = "Unknown"
        End If
        Title = System.Text.RegularExpressions.Regex.Replace(Title, "<[^>]*>", "")  'remove html tags but leave content
        Title = System.Text.RegularExpressions.Regex.Replace(Title, "([^\n]*)\s+\r\n", "")  'remove whitespace
    End Sub
End Class

Public Class LinkParser
    Public Sub New()
    End Sub

    Private Const _LINK_REGEX As String = "href=(""|')[a-zA-Z./:&\d_-]+(""|')"
    Private _goodUrls As New List(Of String)()
    Private _badUrls As New List(Of String)()
    Private _otherUrls As New List(Of String)()
    Private _externalUrls As New List(Of String)()
    Private _exceptions As New List(Of String)()

    Public Property GoodUrls() As List(Of String)
        Get
            Return _goodUrls
        End Get
        Set(value As List(Of String))
            _goodUrls = value
        End Set
    End Property

    Public Property BadUrls() As List(Of String)
        Get
            Return _badUrls
        End Get
        Set(value As List(Of String))
            _badUrls = value
        End Set
    End Property

    Public Property OtherUrls() As List(Of String)
        Get
            Return _otherUrls
        End Get
        Set(value As List(Of String))
            _otherUrls = value
        End Set
    End Property

    Public Property ExternalUrls() As List(Of String)
        Get
            Return _externalUrls
        End Get
        Set(value As List(Of String))
            _externalUrls = value
        End Set
    End Property

    Public Property Exceptions() As List(Of String)
        Get
            Return _exceptions
        End Get
        Set(value As List(Of String))
            _exceptions = value
        End Set
    End Property

    Public Sub ParseLinks(page As Page, sourceUrl As String)
        Dim matches As MatchCollection = Regex.Matches(page.Text, _LINK_REGEX)

        For i As Integer = 0 To matches.Count - 1
            Dim anchorMatch As Match = matches(i)

            If anchorMatch.Value = [String].Empty Then
                BadUrls.Add(Convert.ToString("Blank url value on page ") & sourceUrl)
                Continue For
            End If

            Dim foundHref As String = Nothing
            Try
                foundHref = anchorMatch.Value.Replace("'", """")
                foundHref = foundHref.Replace("href=""", "")
                foundHref = foundHref.Substring(0, foundHref.IndexOf(""""))
            Catch exc As Exception
                Exceptions.Add("Error parsing matched href: " + exc.Message)
            End Try

            If Not GoodUrls.Contains(foundHref) Then
                If IsExternalUrl(foundHref) Then
                    _externalUrls.Add(foundHref)
                ElseIf Not IsAWebPage(foundHref) Then
                    foundHref = SearchResults.FixPath(sourceUrl, foundHref)
                    _otherUrls.Add(foundHref)
                Else
                    GoodUrls.Add(foundHref)
                End If
            End If
        Next
    End Sub

    Public Shared Function IsExternalUrl(url As String) As Boolean
        If url.IndexOf("empiredistrict.com") > -1 Then
            Return False
        ElseIf String.IsNullOrEmpty(url) Or (Len(url) < 8) Then
            Return False
        ElseIf url.Substring(0, 7) = "http://" OrElse url.Substring(0, 3) = "www" OrElse url.Substring(0, 7) = "https://" Then
            Return True
        End If

        Return False
    End Function

    Private Shared Function IsAWebPage(foundHref As String) As Boolean
        If foundHref.IndexOf("javascript:") = 0 Then
            Return False
        End If

        Dim extension As String = foundHref.Substring(foundHref.LastIndexOf(".") + 1, foundHref.Length - foundHref.LastIndexOf(".") - 1)
        Select Case extension
            Case "jpg", "css", "png", "gif", "eps"
                Return False
            Case Else
                Return True
        End Select
    End Function
End Class

Public Class CSSClassParser
    Public Sub New()
    End Sub

    Private Const _CSS_CLASS_REGEX As String = "class=""[a-zA-Z./:&\d_]+"""
    Private _classes As New List(Of String)()

    Public Property Classes() As List(Of String)
        Get
            Return _classes
        End Get
        Set(value As List(Of String))
            _classes = value
        End Set
    End Property

    Public Sub ParseForCssClasses(page As Page)
        Dim matches As MatchCollection = Regex.Matches(page.Text, _CSS_CLASS_REGEX)

        For i As Integer = 0 To matches.Count - 1
            Dim classMatch As Match = matches(i)
            Dim classesArray As String() = classMatch.Value.Substring(classMatch.Value.IndexOf(""""c) + 1, classMatch.Value.LastIndexOf(""""c) - classMatch.Value.IndexOf(""""c) - 1).Split(New Char() {" "c}, StringSplitOptions.RemoveEmptyEntries)

            For Each classValue As String In classesArray
                If Not _classes.Contains(classValue) Then
                    _classes.Add(classValue)
                End If
            Next
        Next
    End Sub
End Class