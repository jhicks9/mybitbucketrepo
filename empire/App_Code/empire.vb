﻿Imports Microsoft.VisualBasic
Imports System.Xml.Linq
Imports System.Linq

Public Class clientInfo
    Function GetIP4Address() As String
        Dim IP4Address As String = String.Empty
        For Each IPA As System.Net.IPAddress In System.Net.Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress)
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next
        If IP4Address <> String.Empty Then
            Return IP4Address
        End If
        For Each IPA As System.Net.IPAddress In System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName())
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next
        Return IP4Address
    End Function
    Function getBrowserInfo(ByVal type As String) As String
        Dim info As String = String.Empty
        Try
            Dim browser As System.Web.HttpBrowserCapabilities = HttpContext.Current.Request.Browser
            Select Case type
                Case "name"
                    info = browser.Browser & " " & browser.Version
                Case "platform"
                    info = browser.Platform
                Case "type"
                    info = browser.Type
                Case "user_agent"
                    info = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT")
                Case Else
                    info = browser.Browser & " " & browser.Version
            End Select
        Catch
        End Try
        Return info
    End Function
    Function IsEmail(ByVal email As String) As Boolean
        Static emailExpression As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        Return emailExpression.IsMatch(email)
    End Function
    Public Function sendEMail(ByVal toList As ArrayList, ByVal strFrom As String, ByVal strSubj As String, ByVal strBody As String, Optional ByVal html As Boolean = false, Optional ByVal fileattachments As ArrayList = Nothing) As Boolean
        Dim status As Boolean = False
        Dim strTo As String = ""
        Dim fAddress As String = ""
        Try
            Dim config = Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
            Dim settings As Net.Configuration.MailSettingsSectionGroup = config.GetSectionGroup("system.net/mailSettings")
            Dim MailObj As New System.Net.Mail.SmtpClient
            Dim message As New System.Net.Mail.MailMessage()

            If html = True then
                message.IsBodyHtml = True 'set HTML as true
            End If
            If String.IsNullOrEmpty(settings.Smtp.Network.UserName) Or String.IsNullOrEmpty(settings.Smtp.Network.Password) Then  'just relay email using supplied name as from address
                If Not strFrom.Contains("@empiredistrict.com") Then 'if only username was passed append @empire
                    strFrom &= "@empiredistrict.com"
                End If
                fAddress = strFrom  'to use "friendly name" use this format: "Your Name <yourname@yourisp.com>"
            Else  'use authentication via userid/password from config file and send email as authenticated user
                MailObj.Credentials = New System.Net.NetworkCredential(settings.Smtp.Network.UserName, settings.Smtp.Network.Password)
                fAddress = settings.Smtp.Network.UserName & "@empiredistrict.com"
            End If
            Dim fromAddress As New System.Net.Mail.MailAddress(fAddress) 'add multiple send to addresses
            For Each strTo In toList
                message.To.Add(strTo)
            Next
            message.From = fromAddress 'add from address
            message.Subject = strSubj
            message.Body = strBody

            If Not fileattachments Is Nothing Then 'file attachments were passed
                For Each attach As HttpPostedFile In fileattachments
                    attach.InputStream.Position = 0
                    message.Attachments.Add(New System.Net.Mail.Attachment(attach.InputStream, System.IO.Path.GetFileName(attach.FileName)))
                    'used to write the file attachment -- for testing
                    'Dim path = System.IO.Path.Combine(System.IO.Path.GetTempPath().ToString, attach.FileName)
		            'Dim data = New Byte(attach.ContentLength - 1) {}
		            'attach.InputStream.Read(data, 0, attach.ContentLength)
		            'Using sw = New system.IO.FileStream(path, System.IO.FileMode.Create)
                        'sw.Write(data, 0, data.Length)
		            'End Using
                Next
            End If
            MailObj.Send(message)
            status = True
        Catch ex As System.Exception
            Throw ex  'send the exception back to ui
        End Try
        Return status
    End Function
End Class

Public Class checkDigit
    Function ZeroPad(ByVal inputStr As String, ByVal reqLength As Integer) As String
        Try
            Dim output As String = ""
            Dim requiredZeros As Integer = 0

            requiredZeros = reqLength - inputStr.Length
            For i = 1 To requiredZeros
                output &= "0"
            Next
            output &= inputStr
            Return output
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Function CalculateCheckDigit(ByVal acct As String) As String
        Try
            Dim sum As Integer = 0
            Dim digit As Integer = 0
            Dim number As Integer = 0
            Dim chrArray() As Char

            chrArray = acct.ToCharArray
            For intCounter = (chrArray.Length - 1) To 0 Step -1 'Iterate through the string in reverse.
                digit = Val(chrArray(intCounter)) 'convert to integer
                number = (((chrArray.Length - 1) - intCounter) Mod 2)
                If number = 0 Then
                    number = 2  'if character position is even
                Else
                    number = 1  'if character position is odd
                End If
                sum += Math.Floor((digit * number) / 10)
                sum += ((digit * number) Mod 10)
            Next

            Dim ckdigit As Integer = sum Mod 10
            If ckdigit = 0 Then
                Return 0
            Else
                Return (10 - ckdigit).ToString
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class

Public Class processSiteMap
    Function getCurrentNodeKey(ByVal keytype As String) As String
        Dim currentNode As String = ""
        Try
            If SiteMap.CurrentNode IsNot Nothing Then
                Select Case keytype
                    Case "title"
                        currentNode = SiteMap.CurrentNode.Title
                    Case "resourcekey"
                        currentNode = SiteMap.CurrentNode.ResourceKey
                    Case "title"
                        currentNode = SiteMap.CurrentNode.Title
                End Select
            End If
        Catch
        End Try
        Return currentNode
    End Function
    Function getParentNodeKey(ByVal keytype As String) As String
        Dim parentNode As String = ""
        Try
            If SiteMap.CurrentNode.ParentNode IsNot Nothing Then
                Select Case keytype
                    Case "title"
                        parentNode = SiteMap.CurrentNode.ParentNode.Title
                    Case "resourcekey"
                        parentNode = SiteMap.CurrentNode.ParentNode.ResourceKey
                End Select
            End If
        Catch
        End Try
        Return parentNode
    End Function

    Function DisplaySiteMap() As String
        Try
            Dim ctrlX As New System.Web.UI.Control
            Dim imgurl As String = ctrlX.ResolveUrl("~/images/icon")
            Dim SiteMapData As SiteMapDataSource = New SiteMapDataSource
            Dim siteMapView As SiteMapDataSourceView = _
                    CType(SiteMapData.GetView(String.Empty), SiteMapDataSourceView)
            Dim nodes As SiteMapNodeCollection = _
                    CType(siteMapView.Select(DataSourceSelectArguments.Empty), SiteMapNodeCollection)
            Dim output As String = String.Empty

            For Each node As SiteMapNode In nodes
                If node.HasChildNodes Then
                    For Each childnode As SiteMapNode In SiteMap.CurrentNode.ChildNodes
                        If childnode("imgUrl") IsNot Nothing Then
                            output &= String.Format("<a href=""{0}""><img class=""icon"" alt="""" src=""{3}/{4}""/>&nbsp;{1}</a><br />{2}<br /><br />", childnode.Url, childnode.Title, childnode.Description, imgurl, childnode("imgUrl"))
                        Else
                            output &= String.Format("<a href=""{0}"">{1}</a><br />{2}<br /><br />", childnode.Url, childnode.Title, childnode.Description)
                        End If
                    Next
                End If
            Next
            Return output
        Catch
            Return Nothing
        End Try
    End Function

    Function BuildSingleNode(ByVal resourceKey As String) As String
        Try
            Dim ctrlX As New System.Web.UI.Control
            Dim output As String = String.Empty
            Dim xelement2 As XElement = XElement.Load(HttpContext.Current.Server.MapPath("~/web.sitemap"))
            Dim urlDescList1 = xelement2.Descendants().Where(Function(sel) CStr(sel.Attribute("resourceKey")) = resourceKey).SelectMany(Function(sel) sel.Elements()).Select(Function(nd) New With {Key .title = nd.Attribute("title").Value, Key .url = nd.Attribute("url").Value, Key .description = nd.Attribute("description").Value})
            For Each s In urlDescList1
                output &= String.Format("<a href=""{0}"">{1}</a>", ctrlX.ResolveUrl(s.url), s.title)
                output &= "<br/>" & s.description & "<br/><br/>"
            Next s
            Return output
        Catch
            Return Nothing
        End Try
    End Function

    Function DisplayCurrentNode(Optional ByVal singleNodeKey As String = "") As String
        Dim SiteMapData As New SiteMapDataSource
        Dim siteMapView As SiteMapDataSourceView = _
                CType(SiteMapData.GetView(String.Empty), SiteMapDataSourceView)
        Dim nodes As SiteMapNodeCollection = _
                CType(siteMapView.Select(DataSourceSelectArguments.Empty), SiteMapNodeCollection)
        Return GetCurrentNode(nodes, singleNodeKey)
    End Function

    Private Function GetCurrentNode(ByVal nodes As SiteMapNodeCollection, Optional ByVal singleNodeKey As String = "") As String
        Dim output As String = String.Empty
        Try
            Dim comparisonKey As String = String.Empty
            If Not String.IsNullOrEmpty(singleNodeKey) Then
                comparisonKey = singleNodeKey
            Else
                comparisonKey = getCurrentNodeKey("resourcekey")
            End If

            For Each node As SiteMapNode In nodes
                If node.ResourceKey = comparisonKey Then  'only show current or specified node items
                    If node.Title = SiteMap.CurrentNode.Title Then
                        'output &= String.Format("{0}", node.Title & "")
                    Else
                        output &= String.Format("<a href=""{0}"">{1}</a>", node.Url, node.Title)
                        output &= "<br/>" & node.Description & "<br/><br/>"
                    End If
                End If
                If node.HasChildNodes Then
                    output &= String.Format("{0}", GetCurrentNode(node.ChildNodes, singleNodeKey))
                End If
            Next
        Catch
        End Try
        Return output
    End Function
End Class