﻿Imports System.Web
Imports System.Runtime.Serialization
Imports System.Globalization
Imports System.IO

Module Weather
    Dim fileName As String = "C:\Temp_Daily.csv"
    'Dim fileName As String = "E:\Pipeline\PROD\DATA\report\Temp_Daily.csv"
    Dim logFile As String = Windows.Forms.Application.StartupPath & "\" & Windows.Forms.Application.ProductName & "log.txt"
    Dim readsVals(24, 1) As Double

    Sub Main()
        Try
            Reads("yesterday")
            Reads("today")
            Using sw As New StreamWriter(File.Open(logFile, FileMode.Append))
                sw.WriteLine("**" & Now.ToString & "** || Success")
            End Using
        Catch ex As Exception
            Using sw As New StreamWriter(File.Open(logFile, FileMode.Append))
                sw.WriteLine("**" & Now.ToString & "** || [Main] " & ex.ToString)
            End Using
        Finally
        End Try
    End Sub

    Private Sub Reads(ByVal whichDay As String)
        Try
            Dim info As ForecastIO.RootObject
            Dim appID As String = "0fe5341d83647b4d81e74c8f0e8c4855"
            'Dim zipcode As String = "64153" KMCI zipcode
            Dim latitude As String = "39.27"
            Dim longitude As String = "-94.74"
            Dim year As String = ""
            Dim month As String = ""
            Dim day As String = ""
            Dim time As String = "" 'year-month-dayThh:mm:ss
            Dim url As String = ""
            Dim json As String = ""
            Dim bytes() As Byte
            Dim ms As MemoryStream
            Dim reader As StreamReader

            If whichDay = "yesterday" Then
                year = Now.AddDays(-1).Year
                month = Now.AddDays(-1).Month
                day = Now.AddDays(-1).Day
                If month.Length = 1 Then
                    month = "0" & month
                End If
                If day.Length = 1 Then
                    day = "0" & day
                End If
                time = year & "-" & month & "-" & day & "T12:00:00"
                url = String.Format("https://api.forecast.io/forecast/{0}/{1},{2},{3}", appID, latitude, longitude, time)
                Using client As New Net.WebClient()
                    json = client.DownloadString(url)
                End Using
                bytes = System.Text.Encoding.ASCII.GetBytes(json)
                ms = New MemoryStream(bytes, 0, bytes.Length)
                ms.Position = 0
                reader = New StreamReader(ms)
                json = reader.ReadToEnd()
                info = (New Script.Serialization.JavaScriptSerializer()).Deserialize(Of ForecastIO.RootObject)(json)
                For i As Integer = 9 To (info.hourly.data.Count - 1) '9AM - 11PM
                    readsVals(i, 0) = info.hourly.data(i).temperature.ToString
                    readsVals(i, 1) = info.hourly.data(i).windSpeed.ToString
                    'Console.WriteLine(readsVals(i, 0).ToString & " " & readsVals(i, 1).ToString)
                Next
                'Console.WriteLine()
            ElseIf whichDay = "today" Then
                year = Now.Year
                'year = Now.AddDays(-1).Year
                month = Now.Month
                'month = Now.AddDays(-1).Month
                day = Now.Day
                'day = Now.AddDays(-1).Day
                If month.Length = 1 Then
                    month = "0" & month
                End If
                If day.Length = 1 Then
                    day = "0" & day
                End If
                time = year & "-" & month & "-" & day & "T12:00:00"
                url = String.Format("https://api.forecast.io/forecast/{0}/{1},{2},{3}", appID, latitude, longitude, time)
                Using client As New Net.WebClient()
                    json = client.DownloadString(url)
                End Using
                bytes = System.Text.Encoding.ASCII.GetBytes(json)
                ms = New MemoryStream(bytes, 0, bytes.Length)
                ms.Position = 0
                reader = New StreamReader(ms)
                json = reader.ReadToEnd()
                info = (New Script.Serialization.JavaScriptSerializer()).Deserialize(Of ForecastIO.RootObject)(json)
                For i As Integer = 0 To 8 '12AM - 8AM
                    readsVals(i, 0) = info.hourly.data(i).temperature.ToString
                    readsVals(i, 1) = info.hourly.data(i).windSpeed.ToString
                    'Console.WriteLine(readsVals(i, 0).ToString & " " & readsVals(i, 1).ToString)
                Next
                Dim hi As Double = Decimal.Round(Max(readsVals), 1, MidpointRounding.AwayFromZero)
                Dim lo As Double = Decimal.Round(Min(readsVals), 1, MidpointRounding.AwayFromZero)
                Dim avgTmp As Double = 0
                Dim avgWS As Double = 0
                For i As Integer = 0 To (info.hourly.data.Count - 1)
                    avgTmp = avgTmp + readsVals(i, 0)
                    avgWS = avgWS + readsVals(i, 1)
                Next
                avgTmp = Decimal.Round((avgTmp / info.hourly.data.Count), 1, MidpointRounding.AwayFromZero)
                avgWS = Decimal.Round((avgWS / info.hourly.data.Count), 1, MidpointRounding.AwayFromZero)
                Using sw As New StreamWriter(File.Open(fileName, FileMode.Append))
                    sw.WriteLine(Now.AddDays(-1).Month & "/" & Now.AddDays(-1).Day & "/" & Now.AddDays(-1).Year & "," & hi.ToString & "," & lo.ToString & "," & avgTmp.ToString & "," & avgWS.ToString)
                End Using
                'Console.WriteLine(Now.AddDays(-1).Month & "/" & Now.AddDays(-1).Day & "/" & Now.AddDays(-1).Year & "," & hi.ToString & "," & lo.ToString & "," & avgTmp.ToString & "," & avgWS.ToString)
                'Console.ReadLine()
            End If
        Catch ex As Exception
            Using sw As New StreamWriter(File.Open(logFile, FileMode.Append))
                sw.WriteLine("**" & Now.ToString & "** || [Reads] " & ex.ToString)
            End Using
        Finally
        End Try
    End Sub

    Private Function Max(ByVal readsArray As Double(,)) As Double
        Try
            Dim maxValue As Double = readsArray(0, 0)
            For i As Integer = 1 To readsArray.GetUpperBound(0)
                If readsArray(i, 0) > maxValue Then
                    maxValue = readsArray(i, 0)
                End If
            Next
            Return maxValue
        Catch ex As Exception
            Using sw As New StreamWriter(File.Open(logFile, FileMode.Append))
                sw.WriteLine("**" & Now.ToString & "** || [Max] " & ex.ToString)
            End Using
        Finally
        End Try
    End Function

    Private Function Min(ByVal readsArray As Double(,)) As Double
        Try
            Dim minValue As Double = readsArray(0, 0)
            For i As Integer = 1 To readsArray.GetUpperBound(0)
                If readsArray(i, 0) < minValue And Not readsArray(i, 0) = 0 Then
                    minValue = readsArray(i, 0)
                End If
            Next
            Return minValue
        Catch ex As Exception
            Using sw As New StreamWriter(File.Open(logFile, FileMode.Append))
                sw.WriteLine("**" & Now.ToString & "** || [Min] " & ex.ToString)
            End Using
        Finally
        End Try
    End Function

End Module

Namespace ForecastIO
    Public Class Currently
        Public Property time() As Integer
            Get
                Return m_time
            End Get
            Set(value As Integer)
                m_time = value
            End Set
        End Property
        Private m_time As Integer
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        Public Property precipIntensity() As Double
            Get
                Return m_precipIntensity
            End Get
            Set(value As Double)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Double
        Public Property precipProbability() As Double
            Get
                Return m_precipProbability
            End Get
            Set(value As Double)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Double
        Public Property temperature() As Double
            Get
                Return m_temperature
            End Get
            Set(value As Double)
                m_temperature = value
            End Set
        End Property
        Private m_temperature As Double
        Public Property apparentTemperature() As Double
            Get
                Return m_apparentTemperature
            End Get
            Set(value As Double)
                m_apparentTemperature = value
            End Set
        End Property
        Private m_apparentTemperature As Double
        Public Property dewPoint() As Double
            Get
                Return m_dewPoint
            End Get
            Set(value As Double)
                m_dewPoint = value
            End Set
        End Property
        Private m_dewPoint As Double
        Public Property humidity() As Double
            Get
                Return m_humidity
            End Get
            Set(value As Double)
                m_humidity = value
            End Set
        End Property
        Private m_humidity As Double
        Public Property windSpeed() As Double
            Get
                Return m_windSpeed
            End Get
            Set(value As Double)
                m_windSpeed = value
            End Set
        End Property
        Private m_windSpeed As Double
        Public Property windBearing() As Integer
            Get
                Return m_windBearing
            End Get
            Set(value As Integer)
                m_windBearing = value
            End Set
        End Property
        Private m_windBearing As Integer
        Public Property visibility() As Double
            Get
                Return m_visibility
            End Get
            Set(value As Double)
                m_visibility = value
            End Set
        End Property
        Private m_visibility As Double
        Public Property cloudCover() As Double
            Get
                Return m_cloudCover
            End Get
            Set(value As Double)
                m_cloudCover = value
            End Set
        End Property
        Private m_cloudCover As Double
        Public Property pressure() As Double
            Get
                Return m_pressure
            End Get
            Set(value As Double)
                m_pressure = value
            End Set
        End Property
        Private m_pressure As Double
        Public Property ozone() As Double
            Get
                Return m_ozone
            End Get
            Set(value As Double)
                m_ozone = value
            End Set
        End Property
        Private m_ozone As Double
    End Class

    Public Class Datum
        Public Property time() As Integer
            Get
                Return m_time
            End Get
            Set(value As Integer)
                m_time = value
            End Set
        End Property
        Private m_time As Integer
        Public Property precipIntensity() As Integer
            Get
                Return m_precipIntensity
            End Get
            Set(value As Integer)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Integer
        Public Property precipProbability() As Integer
            Get
                Return m_precipProbability
            End Get
            Set(value As Integer)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Integer
    End Class

    Public Class Minutely
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        Public Property data() As List(Of Datum)
            Get
                Return m_data
            End Get
            Set(value As List(Of Datum))
                m_data = value
            End Set
        End Property
        Private m_data As List(Of Datum)
    End Class

    Public Class Datum2
        Public Property time() As Integer
            Get
                Return m_time
            End Get
            Set(value As Integer)
                m_time = value
            End Set
        End Property
        Private m_time As Integer
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        Public Property precipIntensity() As Double
            Get
                Return m_precipIntensity
            End Get
            Set(value As Double)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Double
        Public Property precipProbability() As Double
            Get
                Return m_precipProbability
            End Get
            Set(value As Double)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Double
        Public Property temperature() As Double
            Get
                Return m_temperature
            End Get
            Set(value As Double)
                m_temperature = value
            End Set
        End Property
        Private m_temperature As Double
        Public Property apparentTemperature() As Double
            Get
                Return m_apparentTemperature
            End Get
            Set(value As Double)
                m_apparentTemperature = value
            End Set
        End Property
        Private m_apparentTemperature As Double
        Public Property dewPoint() As Double
            Get
                Return m_dewPoint
            End Get
            Set(value As Double)
                m_dewPoint = value
            End Set
        End Property
        Private m_dewPoint As Double
        Public Property humidity() As Double
            Get
                Return m_humidity
            End Get
            Set(value As Double)
                m_humidity = value
            End Set
        End Property
        Private m_humidity As Double
        Public Property windSpeed() As Double
            Get
                Return m_windSpeed
            End Get
            Set(value As Double)
                m_windSpeed = value
            End Set
        End Property
        Private m_windSpeed As Double
        Public Property windBearing() As Integer
            Get
                Return m_windBearing
            End Get
            Set(value As Integer)
                m_windBearing = value
            End Set
        End Property
        Private m_windBearing As Integer
        Public Property visibility() As Double
            Get
                Return m_visibility
            End Get
            Set(value As Double)
                m_visibility = value
            End Set
        End Property
        Private m_visibility As Double
        Public Property cloudCover() As Double
            Get
                Return m_cloudCover
            End Get
            Set(value As Double)
                m_cloudCover = value
            End Set
        End Property
        Private m_cloudCover As Double
        Public Property pressure() As Double
            Get
                Return m_pressure
            End Get
            Set(value As Double)
                m_pressure = value
            End Set
        End Property
        Private m_pressure As Double
        Public Property ozone() As Double
            Get
                Return m_ozone
            End Get
            Set(value As Double)
                m_ozone = value
            End Set
        End Property
        Private m_ozone As Double
        Public Property precipType() As String
            Get
                Return m_precipType
            End Get
            Set(value As String)
                m_precipType = value
            End Set
        End Property
        Private m_precipType As String
        Public Property precipAccumulation() As System.Nullable(Of Double)
            Get
                Return m_precipAccumulation
            End Get
            Set(value As System.Nullable(Of Double))
                m_precipAccumulation = value
            End Set
        End Property
        Private m_precipAccumulation As System.Nullable(Of Double)
    End Class

    Public Class Hourly
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        Public Property data() As List(Of Datum2)
            Get
                Return m_data
            End Get
            Set(value As List(Of Datum2))
                m_data = value
            End Set
        End Property
        Private m_data As List(Of Datum2)
    End Class

    Public Class Datum3
        Public Property time() As Integer
            Get
                Return m_time
            End Get
            Set(value As Integer)
                m_time = value
            End Set
        End Property
        Private m_time As Integer
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        Public Property sunriseTime() As Integer
            Get
                Return m_sunriseTime
            End Get
            Set(value As Integer)
                m_sunriseTime = value
            End Set
        End Property
        Private m_sunriseTime As Integer
        Public Property sunsetTime() As Integer
            Get
                Return m_sunsetTime
            End Get
            Set(value As Integer)
                m_sunsetTime = value
            End Set
        End Property
        Private m_sunsetTime As Integer
        Public Property moonPhase() As Double
            Get
                Return m_moonPhase
            End Get
            Set(value As Double)
                m_moonPhase = value
            End Set
        End Property
        Private m_moonPhase As Double
        Public Property precipIntensity() As Double
            Get
                Return m_precipIntensity
            End Get
            Set(value As Double)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Double
        Public Property precipIntensityMax() As Double
            Get
                Return m_precipIntensityMax
            End Get
            Set(value As Double)
                m_precipIntensityMax = value
            End Set
        End Property
        Private m_precipIntensityMax As Double
        Public Property precipIntensityMaxTime() As Integer
            Get
                Return m_precipIntensityMaxTime
            End Get
            Set(value As Integer)
                m_precipIntensityMaxTime = value
            End Set
        End Property
        Private m_precipIntensityMaxTime As Integer
        Public Property precipProbability() As Double
            Get
                Return m_precipProbability
            End Get
            Set(value As Double)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Double
        Public Property precipType() As String
            Get
                Return m_precipType
            End Get
            Set(value As String)
                m_precipType = value
            End Set
        End Property
        Private m_precipType As String
        Public Property temperatureMin() As Double
            Get
                Return m_temperatureMin
            End Get
            Set(value As Double)
                m_temperatureMin = value
            End Set
        End Property
        Private m_temperatureMin As Double
        Public Property temperatureMinTime() As Integer
            Get
                Return m_temperatureMinTime
            End Get
            Set(value As Integer)
                m_temperatureMinTime = value
            End Set
        End Property
        Private m_temperatureMinTime As Integer
        Public Property temperatureMax() As Double
            Get
                Return m_temperatureMax
            End Get
            Set(value As Double)
                m_temperatureMax = value
            End Set
        End Property
        Private m_temperatureMax As Double
        Public Property temperatureMaxTime() As Integer
            Get
                Return m_temperatureMaxTime
            End Get
            Set(value As Integer)
                m_temperatureMaxTime = value
            End Set
        End Property
        Private m_temperatureMaxTime As Integer
        Public Property apparentTemperatureMin() As Double
            Get
                Return m_apparentTemperatureMin
            End Get
            Set(value As Double)
                m_apparentTemperatureMin = value
            End Set
        End Property
        Private m_apparentTemperatureMin As Double
        Public Property apparentTemperatureMinTime() As Integer
            Get
                Return m_apparentTemperatureMinTime
            End Get
            Set(value As Integer)
                m_apparentTemperatureMinTime = value
            End Set
        End Property
        Private m_apparentTemperatureMinTime As Integer
        Public Property apparentTemperatureMax() As Double
            Get
                Return m_apparentTemperatureMax
            End Get
            Set(value As Double)
                m_apparentTemperatureMax = value
            End Set
        End Property
        Private m_apparentTemperatureMax As Double
        Public Property apparentTemperatureMaxTime() As Integer
            Get
                Return m_apparentTemperatureMaxTime
            End Get
            Set(value As Integer)
                m_apparentTemperatureMaxTime = value
            End Set
        End Property
        Private m_apparentTemperatureMaxTime As Integer
        Public Property dewPoint() As Double
            Get
                Return m_dewPoint
            End Get
            Set(value As Double)
                m_dewPoint = value
            End Set
        End Property
        Private m_dewPoint As Double
        Public Property humidity() As Double
            Get
                Return m_humidity
            End Get
            Set(value As Double)
                m_humidity = value
            End Set
        End Property
        Private m_humidity As Double
        Public Property windSpeed() As Double
            Get
                Return m_windSpeed
            End Get
            Set(value As Double)
                m_windSpeed = value
            End Set
        End Property
        Private m_windSpeed As Double
        Public Property windBearing() As Integer
            Get
                Return m_windBearing
            End Get
            Set(value As Integer)
                m_windBearing = value
            End Set
        End Property
        Private m_windBearing As Integer
        Public Property visibility() As Double
            Get
                Return m_visibility
            End Get
            Set(value As Double)
                m_visibility = value
            End Set
        End Property
        Private m_visibility As Double
        Public Property cloudCover() As Double
            Get
                Return m_cloudCover
            End Get
            Set(value As Double)
                m_cloudCover = value
            End Set
        End Property
        Private m_cloudCover As Double
        Public Property pressure() As Double
            Get
                Return m_pressure
            End Get
            Set(value As Double)
                m_pressure = value
            End Set
        End Property
        Private m_pressure As Double
        Public Property ozone() As Double
            Get
                Return m_ozone
            End Get
            Set(value As Double)
                m_ozone = value
            End Set
        End Property
        Private m_ozone As Double
    End Class

    Public Class Daily
        Public Property data() As List(Of Datum3)
            Get
                Return m_data
            End Get
            Set(value As List(Of Datum3))
                m_data = value
            End Set
        End Property
        Private m_data As List(Of Datum3)
    End Class

    Public Class Flags
        Public Property sources() As List(Of String)
            Get
                Return m_sources
            End Get
            Set(value As List(Of String))
                m_sources = value
            End Set
        End Property
        Private m_sources As List(Of String)
        Public Property darksky_stations() As List(Of String)
            Get
                Return m_darksky_stations
            End Get
            Set(value As List(Of String))
                m_darksky_stations = value
            End Set
        End Property
        Private m_darksky_stations As List(Of String)
        Public Property lamp_stations() As List(Of String)
            Get
                Return m_lamp_stations
            End Get
            Set(value As List(Of String))
                m_lamp_stations = value
            End Set
        End Property
        Private m_lamp_stations As List(Of String)
        Public Property isd_stations() As List(Of String)
            Get
                Return m_isd_stations
            End Get
            Set(value As List(Of String))
                m_isd_stations = value
            End Set
        End Property
        Private m_isd_stations As List(Of String)
        Public Property madis_stations() As List(Of String)
            Get
                Return m_madis_stations
            End Get
            Set(value As List(Of String))
                m_madis_stations = value
            End Set
        End Property
        Private m_madis_stations As List(Of String)
        Public Property units() As String
            Get
                Return m_units
            End Get
            Set(value As String)
                m_units = value
            End Set
        End Property
        Private m_units As String
    End Class

    Public Class RootObject
        Public Property latitude() As Double
            Get
                Return m_latitude
            End Get
            Set(value As Double)
                m_latitude = value
            End Set
        End Property
        Private m_latitude As Double
        Public Property longitude() As Double
            Get
                Return m_longitude
            End Get
            Set(value As Double)
                m_longitude = value
            End Set
        End Property
        Private m_longitude As Double
        Public Property timezone() As String
            Get
                Return m_timezone
            End Get
            Set(value As String)
                m_timezone = value
            End Set
        End Property
        Private m_timezone As String
        Public Property offset() As Integer
            Get
                Return m_offset
            End Get
            Set(value As Integer)
                m_offset = value
            End Set
        End Property
        Private m_offset As Integer
        Public Property currently() As Currently
            Get
                Return m_currently
            End Get
            Set(value As Currently)
                m_currently = value
            End Set
        End Property
        Private m_currently As Currently
        Public Property minutely() As Minutely
            Get
                Return m_minutely
            End Get
            Set(value As Minutely)
                m_minutely = value
            End Set
        End Property
        Private m_minutely As Minutely
        Public Property hourly() As Hourly
            Get
                Return m_hourly
            End Get
            Set(value As Hourly)
                m_hourly = value
            End Set
        End Property
        Private m_hourly As Hourly
        Public Property daily() As Daily
            Get
                Return m_daily
            End Get
            Set(value As Daily)
                m_daily = value
            End Set
        End Property
        Private m_daily As Daily
        Public Property flags() As Flags
            Get
                Return m_flags
            End Get
            Set(value As Flags)
                m_flags = value
            End Set
        End Property
        Private m_flags As Flags
    End Class
End Namespace