<%@ Control Language="VB" AutoEventWireup="false" CodeFile="footer.ascx.vb" Inherits="footer" %>
    <div style ="padding:5px">
        <img id="imgsep1" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
        <asp:Label ID="lbladdress1" runat="server" text="602 S Joplin Avenue"/>
        <img id="imgsep2" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
        <asp:Label ID="lbladdress2" runat="server" text="PO Box 127"/>
        <img id="imgsep3" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
        <asp:Label ID="lblAddress3" runat="server" text="Joplin, MO 64802"/>
        <img id="imgsep4" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>              
        <br /><br /><a href="~/TermsofService.aspx" runat="server">Copyright � 2007-<asp:Label ID="lblEndYear" runat="server" Text="" /> Empire District. All Rights Reserved.</a>
    </div>