﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="forecast.ascx.vb" Inherits="forecast" %>
    <table class="forecasttable">
        <asp:Panel ID='pnlCurrent' runat='server' Visible='false'>
            <tr>
                <td><img id="imgCurrentWeather" runat="server" src="~/images/weatherImages/na.png" alt="" width="80" height="80" /></td>
                <td class="center vcenter" style="font-size:2.25em"><asp:Label ID="lblCurrentTemp" runat="server" Text="" Font-Bold="true" /></td>
                <td colspan="4">
                    <asp:Label ID="lblCurrentDescription" runat="server" Text="" />
                    <asp:Label ID="lblCurrentSummary" runat="server" Text="" />
                    Wind:<asp:Label ID="lblCurrentWind" runat="server" Text="" />
                    <asp:Label ID="lblCurrentWindSpeed" runat="server" Text="" />
                    Humidity:<asp:Label ID="lblCurrentHumidity" runat="server" Text="" />
                    <asp:Label ID="lblCurrentLow" runat="server" Text="" />
                    <asp:Label ID="lblCurrentHigh" runat="server" Text="" />
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlDay0' runat='server' Visible='false'>
            <tr style="background-color:lightgray">
                <td class="right vcenter"><asp:Label ID="lblDay0" runat="server" Text="" /></td>
                <td class="center"><img id="imgDay0" runat='server' src="~/images/weatherImages/na.png" alt="" width="40" height="40"/></td>
                <td><asp:Label ID="lblDay0Desc" runat="server" Text="" /></td>
                <td><asp:Label ID="lblDay0High" runat="server" Text="" ForeColor="Black" /></td>
                <td>&nbsp;|&nbsp;</td>
                <td><asp:Label ID="lblDay0Low" runat="server" Text="" ForeColor="Gray" /></td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlDay1' runat='server' Visible='false'>
            <tr>
                <td class="right vcenter"><asp:Label ID="lblDay1" runat="server" Text="" /></td>
                <td class="center"><img id="imgDay1" runat='server' src="~/images/weatherImages/na.png" alt="" width="40" height="40"/></td>
                <td><asp:Label ID="lblDay1Desc" runat="server" Text="" /></td>
                <td><asp:Label ID="lblDay1High" runat="server" Text="" ForeColor="Black" /></td>
                <td>&nbsp;|&nbsp;</td>
                <td><asp:Label ID="lblDay1Low" runat="server" Text="" ForeColor="Gray" /></td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlDay2' runat='server' Visible='false'>
            <tr style="background-color:lightgray">
                <td class="right vcenter"><asp:Label ID="lblDay2" runat="server" Text="" /></td>
                <td class="center"><img id="imgDay2" runat='server' src="~/images/weatherImages/na.png" alt="" width="40" height="40"/></td>
                <td><asp:Label ID="lblDay2Desc" runat="server" Text="" /></td>
                <td><asp:Label ID="lblDay2High" runat="server" Text="" ForeColor="Black" /></td>
                <td>&nbsp;|&nbsp;</td>
                <td><asp:Label ID="lblDay2Low" runat="server" Text="" ForeColor="Gray" /></td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlDay3' runat='server' Visible='false'>
            <tr>
                <td class="right vcenter"><asp:Label ID="lblDay3" runat="server" Text="" /></td>
                <td class="center"><img id="imgDay3" runat='server' src="~/images/weatherImages/na.png" alt="" width="40" height="40"/></td>
                <td><asp:Label ID="lblDay3Desc" runat="server" Text="" /></td>
                <td><asp:Label ID="lblDay3High" runat="server" Text="" ForeColor="Black" /></td>
                <td>&nbsp;|&nbsp;</td>
                <td><asp:Label ID="lblDay3Low" runat="server" Text="" ForeColor="Gray" /></td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlDay4' runat='server' Visible='false'>
            <tr style="background-color:lightgray">
                <td class="right vcenter"><asp:Label ID="lblDay4" runat="server" Text="" /></td>
                <td class="center"><img id="imgDay4" runat='server' src="~/images/weatherImages/na.png" alt="" width="40" height="40"/></td>
                <td><asp:Label ID="lblDay4Desc" runat="server" Text="" /></td>
                <td><asp:Label ID="lblDay4High" runat="server" Text="" ForeColor="Black" /></td>
                <td>&nbsp;|&nbsp;</td>
                <td><asp:Label ID="lblDay4Low" runat="server" Text="" ForeColor="Gray" /></td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlDay5' runat='server' Visible='false'>
            <tr>
                <td class="right vcenter"><asp:Label ID="lblDay5" runat="server" Text="" /></td>
                <td class="center"><img id="imgDay5" runat='server' src="~/images/weatherImages/na.png" alt="" width="40" height="40"/></td>
                <td><asp:Label ID="lblDay5Desc" runat="server" Text="" /></td>
                <td><asp:Label ID="lblDay5High" runat="server" Text="" ForeColor="Black" /></td>
                <td>&nbsp;|&nbsp;</td>
                <td><asp:Label ID="lblDay5Low" runat="server" Text="" ForeColor="Gray" /></td>
            </tr>
        </asp:Panel>
        <asp:Panel ID='pnlObserved' runat='server' Visible='false'>
            <tr>
                <td colspan="6" class="center"><asp:Label ID="lblCity" runat="server" Text="" Font-Size="X-Small" /></td>
            </tr>
        </asp:Panel>
    </table>