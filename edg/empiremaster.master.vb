﻿
Partial Class empiremaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header
            Dim Header As SiteMapNode
            If SiteMap.CurrentNode IsNot Nothing Then
                Header = SiteMap.CurrentNode
                lblPageHeader.Text = Header.Title
            Else
                lblPageHeader.Text = ""
            End If
        Catch
        End Try
    End Sub
End Class

