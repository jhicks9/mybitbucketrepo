﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="header.ascx.vb" Inherits="header" %>

    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td rowspan="2" align="left" style="width:130px;padding-right:10px;padding-left:10px;">
                <asp:Panel ID="pnlLogo" runat="server" Visible="true">

                    <img id="imgLogo" runat="server" alt="Company Logo" src="~/images/navigation/empire-logo.gif"/>
                    <img id="imgSlogan" runat="server" alt="Services You Count On" src="~/images/navigation/empire-slogan.jpg"/>
                    
                </asp:Panel>
            </td>
            <td align="left" style="vertical-align:middle"><img id="imgTitle" runat="server" alt="Empire District Gas" src="~/images/title.png"/></td>
            <td align="right" style="padding-top:5px;vertical-align:top;">
                <asp:LoginView ID="LoginView1" runat="server">
                    <AnonymousTemplate></AnonymousTemplate>
                    <LoggedInTemplate>
                        <div class="logout">
                            <asp:LoginName ID="LoginName1" runat="server" />
                            <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutPageUrl="~/Default.aspx" LogoutAction="Redirect" OnLoggingOut="LoginStatus1_LoggingOut" onclick="onInvoke()" />
                        </div>
                    </LoggedInTemplate>
                </asp:LoginView>
            </td>
        </tr>
    </table>

<script type="text/javascript">
    $(document).ready(function () {
        //Append a div with hover class to all the LI
        if (!("ontouchstart" in document.documentElement)) {
            $('#fadeMenu li').append('<div class="hover"><\/div>');
        }
        $('#fadeMenu li').hover(
        //Mouseover, fadeIn the hidden hover class
		function () {
		    $(this).children('div').stop(true, true).fadeIn('1000');
		},
        //Mouseout, fadeOut the hover class
		function () {
		    $(this).children('div').stop(true, true).fadeOut('1000');
		});
    });
</script>
