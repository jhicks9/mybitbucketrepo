﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="_default" %>
<%@ Register TagPrefix="sidebar" TagName="uc1" Src="~/sidebar.ascx" %>
<%@ Register TagPrefix="forecast" TagName="uc7" Src="~/forecast.ascx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .slideshow-pager {margin: 2px 0 2px 0;width:100%;text-align:center;font-size:.65em;}
        .slideshow-pager span{margin-right:3px}
        .slideshow-pager a:hover {background-color:#36587a;color:#36587a}
        .slideshow-pager a {border-radius:50%;text-decoration:none;color:#b0c4de;padding:2px 4px 2px 4px;background:#b0c4de}
        .slideshow-pager-active, .slideshow-pager-active a{border-radius:50%;text-decoration:none;color:#36587a;background:#36587a}
        .slideshow {list-style:none;padding:0;margin:0;display:inline-block;font-size:0}
        .slideshow li {list-style:none;}
        .slideshow img {border: 0px solid transparent;}
    </style>
    <script src='<%#ResolveUrl("~/js/jquery.cycle2.min.js")%>' type="text/javascript" charset="utf-8"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftPlaceHolder" Runat="Server">
    <asp:LoginView ID="LoginView1" runat="server">
        <AnonymousTemplate>
            <div style="margin:auto;text-align:center"><sidebar:uc1 ID="sidebar" runat="server" navMenu="false" login="true" ImageUrl="~/images/sidebar-gas.jpg" /></div>
        </AnonymousTemplate>
        <LoggedInTemplate>
            <div style="margin:auto;text-align:center"><sidebar:uc1 ID="sidebar" runat="server" navMenu="true" login="false" ImageUrl="~/images/sidebar-gas.jpg" /></div>
        </LoggedInTemplate>
    </asp:LoginView>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageHeaderPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <table style="border-spacing:0;border-collapse:collapse">
        <tr>
            <td valign="top" style="width:250px">
                <asp:Panel ID="pnlDefault" runat="server" Visible="true">
                    <div style="padding:20px">
                        <br /><br />Based in Joplin, Missouri, <b>The Empire District Gas Company</b> (NYSE: EDE) is an investor-owned, regulated utility providing electric, natural gas (through its wholly owned subsidiary
                        The Empire District Gas Company), and water service, with approximately 217,000 customers in Missouri, Kansas, Oklahoma, and Arkansas. A subsidiary of the company provides fiber optic services.
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlOFO" runat="server" Visible="false">
                    <div style="padding:5px;background-color:#ffffaf">
                    <div class="heading">OFO</div>
                    <asp:GridView ID="gvOFO" runat="server" SkinID="GridViewList" Width="100%" AllowPaging="True" DataSourceID="sqlDSOFO" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                <div style='padding:5px;background-color:#ffffaf'>
                                    <asp:Image ID="imgSepGV" ImageAlign="Top" ImageUrl="~/images/icon/icon_announcement.gif" AlternateText="" runat="server" />&nbsp; 
                                    <asp:Label ID="lblDate" runat="server"  style="white-space:nowrap;vertical-align:top;font-weight:bold;" Text='<%#Eval("Date") & "  " %>'></asp:Label>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>Nothing scheduled at this time.</EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlDSOFO" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT messages.id,messages.type,CONVERT(char(10),messages.date,101) AS date,messages.date AS sortdate,messages.description FROM messages where type='OFO Announcement' and ((getdate() >= messages.begdate and getdate() <= messages.enddate) or (messages.begdate is null and messages.enddate is null)) order by sortdate desc,messages.description ASC">
                    </asp:SqlDataSource>
                    </div>
                </asp:Panel>
            </td>
            <td valign="top" style="width:500px;vertical-align:top;padding:0;margin:0">
                <div class="slideshow" style="z-index:1">
                    <img id="imgSafetyCheck" runat="server" src="~/images/module/module-safetycheck.jpg" alt="" />
                    <img id="imgServes" runat="server" src="~/images/module/module-serves.jpg" alt="" />
                    <img id="imgOneCall" runat="server" src="~/images/module/module-onecall.jpg" alt="" />
                    <img id="imgPipe" runat="server" src="~/images/module/module-pipe.jpg" alt="" />
                </div>
                <div class="slideshow-pager" style="text-align: left"></div>
            </td>
        </tr>
    </table>
                                
    <table style="border-spacing:0;border-collapse:collapse;margin-top:5px">
        <tr>
            <asp:Panel ID='pnlAnnouncements' runat='server' Visible='true'>
                <td valign="top" style="padding:0px;width:400px">
                    <div class="heading">Announcements</div>
                    <asp:GridView ID="gvAnnouncements" runat="server" SkinID="GridViewList" Width="100%" AllowPaging="True" DataSourceID="sqlDSAnnouncements" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                <div style='padding-top:3px;padding-bottom:3px;'>
                                    <asp:Image ID="imgSepGV" ImageAlign="Top" ImageUrl="~/images/icon/icon_announcement.gif" AlternateText="" runat="server" />&nbsp; 
                                    <asp:Label ID="lblDate" runat="server"  style="white-space:nowrap;vertical-align:top;font-weight:bold;" Text='<%#Eval("Date") & "  " %>'></asp:Label>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>Nothing scheduled at this time.</EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlDSAnnouncements" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="SELECT messages.id,messages.type,CONVERT(char(10),messages.date,101) AS date,messages.date AS sortdate,messages.description FROM messages where type='EDG Announcement' and ((getdate() >= messages.begdate and getdate() <= messages.enddate) or (messages.begdate is null and messages.enddate is null)) order by sortdate desc,messages.description ASC">
                    </asp:SqlDataSource>
                </td>
            </asp:Panel>
                
            <asp:Panel ID='pnlWeather' runat='server' Visible='true'>
                <td valign="top" style="padding:0px;width:350px">
                    <asp:Image ID="imgLocalWeather" ImageUrl="~/images/local-weather.jpg" AlternateText="" runat="server" />
                    <forecast:uc7 ID="forecast" runat="server" zipcode="64153" CurrentImageSize="80" ForecastImageSize="40" DisplayCurrentConditions="true" DaysToForecast="2" DisplayObserved="true" />
                </td>
            </asp:Panel>    
        </tr>
    </table>

    <script type="text/javascript">
        $('.slideshow').cycle({ fx: 'fade', speed: 900, timeout: 15500, slides: 'img', pager: '.slideshow-pager', pagerActiveClass: 'slideshow-pager-active', pagerTemplate: '<span><a href=#>{{slideNum}}</a></span>' /*, pagerTemplate: '<span><a href=#>{{slideNum}}</a></span>'*/ });
    </script>
</asp:Content>