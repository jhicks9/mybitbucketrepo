<%@ Control Language="VB" AutoEventWireup="false" CodeFile="sidebar.ascx.vb" Inherits="sidebar" %>

<asp:Panel ID="pnlLogin" runat="server" Visible="false">
    <div style="padding:5px;font:bold .9em arial,verdana,sans-serif;">
        <table width="100%" cellpadding="2" cellspacing="0" style="color:#ffffff">
            <tr>
                <td align="left"><asp:Label ID="lblUserName" runat="server" AssociatedControlID="txtUserName" Text="User Name:" /></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" Width="150px" Font-Size="1em"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="*" ValidationGroup="avg" Display="Dynamic" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td align="left"><asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword" Text="Password:" /></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" Width="150px" Font-Size="1em" TextMode="Password" />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" ValidationGroup="avg" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td >
                    <div style="padding:5px 0 5px 0;color:Red;">
                        <asp:Label ID="lblStatus" runat="server" Visible="true" ForeColor="Red" Text="" />
                    </div>    
                </td>
                </tr>
            <tr>
                <td align="right"><div style="float:left;width:115px;">&nbsp;</div><asp:LinkButton ID="lbLogin" runat="server" CssClass="ovalbutton" CommandName="Login" ValidationGroup="avg" OnClick="lbLogin_Click"><span>Log In</span></asp:LinkButton>    </td>
            </tr>
        </table>
    </div>
</asp:Panel>

<asp:Label ID="lblMenu" runat="server" Text="" Visible="false" />
<asp:Image ID="imgSideImage" Imageurl="" runat="server" Visible="false" AlternateText="" />

<div style="color:#fff;padding-top:15px;font:bold .9em arial,verdana,sans-serif;text-align:center;">
    <asp:Label ID="lblGas" runat="server" text="Gas Service:"></asp:Label><br />
    <img id="imgsep2" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
    <asp:Label ID="lblGasNumber" runat="server" text="800-424-0427"></asp:Label>
    <br /><br />
    <asp:Label ID="lblGasLeak" runat="server" text="Report a Gas Leak:"></asp:Label><br />
    <img id="imgsep4" src="~/images/navigation/separator-yellow.png" runat="server" alt=""/>
    <asp:Label ID="lblGasLeakNumber" runat="server" text="800-406-9220"></asp:Label>
    <br /><br />
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //Append a div with hover class to all the LI
        if (!("ontouchstart" in document.documentElement)) {
            $('#sideMenu li').append('<div class="hover"><\/div>');
        }
        $('#sideMenu li').hover(
        //Mouseover, fadeIn the hidden hover class
		function () {
		    $(this).children('div').stop(true, true).fadeIn('xslow');
		},
        //Mouseout, fadeOut the hover class
		function () {
		    $(this).children('div').stop(true, true).fadeOut('xslow');
		});
    });
</script>

<%--<div class="module">    
    <div class="module-roundright"></div>
    <div class="module-header">
        <asp:Label ID="lblLinks" runat="server" Text=""></asp:Label>
    </div>
    <div class="module-borderleft"> 
      <div class="navmenu">
        <asp:Label ID="txtNavMenu" runat="server" Text="" Visible="False"></asp:Label>
      </div>
      <asp:Image ID="imgSide" Imageurl="" cssclass="navpic" runat="server" Visible="false" /> 
    </div>


</div>--%>