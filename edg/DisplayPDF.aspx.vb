Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO
Imports System.IO.File
Imports System.Xml.XPath
Imports System
Imports System.Web
Imports System.Data
Imports ibex4
Imports ibex4.logging

Partial Class MyAccount_DisplayPDF
    Inherits System.Web.UI.Page

    Dim xdoc As String
    Dim xtemplate As String
    Dim custAccount As String
    Dim rtnStatement As String
    Dim tdes As New tripleDES
    Dim cwservice As New cw.Service
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim statementToken As String = Request.QueryString("x").ToString
        custAccount = Request.QueryString("a")

        cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
        Dim encryptstmtToken As String = tdes.Encrypt(statementToken)
        'production LV Web Template 345
        'online LV Web Template 909
        rtnStatement = cwservice.GetStatement(encryptstmtToken, "345")

        Dim rtnValueArray() As String = Split(rtnStatement, "-,-")
        If rtnValueArray(0) <> "Error" Then
            xdoc = rtnValueArray(0)
            xtemplate = rtnValueArray(1)
            'Response.Write(xtemplate)
            '    'local call to nfop to generate fo and pdf
            GeneratePDF()            
        Else

        End If
    End Sub
    
    Private Sub GeneratePDF()
        Try
            'create the FO using xml/xsl
            Dim rs As XmlReaderSettings = New XmlReaderSettings()
            Dim xslt As XslCompiledTransform = New XslCompiledTransform()

            rs.ProhibitDtd = False
            rs.IgnoreWhitespace = True
            Dim x As XmlDocument = New XmlDocument
            x.LoadXml(xtemplate)

            Dim xmlNode As XmlNodeReader = New XmlNodeReader(x)
            xslt.Load(System.Xml.XmlReader.Create(xmlNode, rs))

            Dim xmldoc As XmlDocument = New XmlDocument
            xmldoc.LoadXml(xdoc)

            'added 6/3            
            Dim xmlNode2 As XmlNodeReader = New XmlNodeReader(xmldoc)
            Dim Fullpathfo As String = System.IO.Path.GetTempPath() & custAccount & ".fo"

            Dim stmFo As System.IO.Stream = System.IO.File.Create(Fullpathfo, System.IO.FileMode.Create)
            xslt.Transform(System.Xml.XmlReader.Create(xmlNode2, rs), Nothing, stmFo)

            'create pdf using generated FO
            stmFo.Close()
            stmFo.Dispose()

            'IBEX *****************

            Dim key As String = "0002008082310A491433269D1E1B5F2ALLR/YAXSSIRD03WS9XZ5AG=="

            ibex4.licensing.Generator.setRuntimeKey(key)
            Dim doc As New FODocument
            'stream for output in memory before sending to browser
            Dim pdfStream As MemoryStream = New MemoryStream
            Dim dataStream As FileStream = New FileStream(Fullpathfo, FileMode.Open, FileAccess.Read)

            Using (dataStream)
                doc.generate(dataStream, pdfStream, False)
            End Using

            System.Threading.Thread.Sleep(1500)
            System.IO.File.Delete(System.IO.Path.GetTempPath() & custAccount & ".fo")
            dataStream.Close()
            Response.Clear()
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-length", System.Convert.ToString(pdfStream.Length))
            Response.BinaryWrite(pdfStream.ToArray())
            Response.End()
            'end Ibex ************************

        Catch
            'lblMessage.Text += Err.Description
        End Try
    End Sub
End Class