﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Runtime.Remoting.Messaging

Partial Class _default
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sm As SiteMapPath = Page.Master.FindControl("SiteMapPath")
            sm.Visible = False
        Catch
        End Try
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            If gvOFO.Rows.Count > 0 Then  'enable OFO announcement if available
                pnlDefault.Visible = False
                pnlOFO.Visible = True
            Else  'otherwise just show default
                pnlOFO.Visible = False
                pnlDefault.Visible = True
            End If
        Catch
        End Try
    End Sub

    Protected Sub gvAnnouncements_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAnnouncements.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim desc As Label = CType(e.Row.FindControl("lblDescription"), Label)

                Dim begtoken As String = "~/"  'process virtual links to actual (if used)
                Dim endtoken As String = Chr(34) 'Chr(34) is the same as the " character
                Dim pos1 As Integer = 0
                Dim pos2 As Integer = 0
                Dim preLink As String = ""
                Dim link As String = ""
                Dim postLink As String = ""

                Do Until pos1 = -1
                    pos1 = LCase(desc.Text).IndexOf(begtoken, pos1)
                    If (pos1 > -1) Then
                        preLink = Left(desc.Text, pos1)
                        pos2 = desc.Text.IndexOf(endtoken, pos1)
                        link = ResolveUrl(desc.Text.Substring(pos1, pos2 - pos1))
                        'link = ResolveUrl(link)
                        postLink = Right(desc.Text, Len(desc.Text) - pos2)
                        pos1 = pos2
                        desc.Text = preLink & link & postLink
                    End If
                Loop
            End If
        Catch
        End Try
    End Sub
End Class

Public Class sqlConnections
    Public Class ConnectionStateTracker
        Private _abandonConnection As Boolean
        Private _run As Boolean
        Private _connStr As String
        Private _sync As Object
        'ctors
        Private Sub New()
            _run = False
            _abandonConnection = False
            _sync = New Object()
        End Sub
        Public Sub New(ByVal connStr As String)
            Me.New()
            _connStr = connStr
        End Sub
        'public properties
        Private Property AbandonConnection() As Boolean
            Get
                Return VolatileRead(_abandonConnection)
            End Get
            Set(ByVal value As Boolean)
                VolatileWrite(_abandonConnection, value)
            End Set
        End Property
        'private methods
        Private Function VolatileRead(Of T)(ByRef Address As T) As T
            VolatileRead = Address
            Threading.Thread.MemoryBarrier()
        End Function
        Private Sub VolatileWrite(Of T)(ByRef Address As T, ByVal Value As T)
            Threading.Thread.MemoryBarrier()
            Address = Value
        End Sub
        Private Sub ConnectCallback(ByVal ar As IAsyncResult)
            'Clean up the connection if it was abandoned
            SyncLock _sync
                If Me.AbandonConnection Then
                    Dim result As AsyncResult = CType(ar, AsyncResult)
                    Dim conn As SqlConnection = CType(ar.AsyncState, SqlConnection)
                    Dim del As Action = CType(result.AsyncDelegate, Action)
                    Try
                        del.EndInvoke(ar)
                    Catch ex As Exception
                        Console.WriteLine("Connection Error: " + ex.Message)
                    End Try
                    conn.Close()
                    conn.Dispose()
                End If
            End SyncLock
        End Sub
        'public methods
        Public Function Connect(ByVal timeout As Integer) As SqlConnection
            If (_run) Then Throw New InvalidOperationException("Connect can only be called once.")
            _run = True

            SyncLock _sync
                Dim conn As New SqlConnection(_connStr)
                Dim del As Action = AddressOf conn.Open
                Dim ar As IAsyncResult = del.BeginInvoke(AddressOf ConnectCallback, conn)
                Dim finished As Boolean = ar.AsyncWaitHandle.WaitOne(timeout)
                If (Not finished) Or (conn.State <> ConnectionState.Open) Then
                    Me.AbandonConnection = True
                    Return Nothing
                Else
                    Me.AbandonConnection = False
                    del.EndInvoke(ar)
                    Return conn
                End If
            End SyncLock

            Return Nothing
        End Function
    End Class
End Class