﻿<%@ Page Title="" Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="GasDocuments.aspx.vb" Inherits="lvg_GasDocuments" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <div style="padding-top:5px;padding-bottom:5px;"><acrobat:uc4 ID="uc1" runat="server" /></div>
    <asp:GridView ID="gvGasDocs" runat="server" DataSourceID="sqlDSGasDocs" SkinID="gridviewlist">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                        <asp:Image ID="imgDocument" ImageAlign="Top" ImageUrl="~/images/icon/icon_document.png" runat="server" />&nbsp; 
                        <asp:HyperLink ID="hlDocument" Text='<%#Eval("DocDescription") %>' NavigateUrl='<%#Eval("id", "~/DocHandler.ashx?id={0}") %>' Target="_blank" ForeColor="#113962" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlDSGasDocs" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT groups.description, documents.lastupdate, documents.filename, documents.filebytes, documents.doctypeid, documents.groupid, doctype.description AS documenttype, documents.description AS DocDescription, documents.id FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE (documents.groupid = 14 AND documents.doctypeid = 115) ORDER BY documents.lastupdate DESC">
    </asp:SqlDataSource>
</asp:Content>


