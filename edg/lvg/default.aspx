<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="ArchiveNom" title="Nominations" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:Panel ID="pnlNominations" runat="server" Visible="false">
        <table>
            <tr>
                <td align="right">
                    <asp:Label ID="Label1" runat="server" Visible="true" Text="Select an account" />
                </td>
                <td>
                    <asp:DropDownList ID="dlsNoms" runat="server" AutoPostBack="false" Visible="false" />&nbsp;&nbsp;
                    <asp:label id="lblMessage" Runat="server" Visible="false" Font-Size="12" ForeColor="#113962" Text="" />
                </td>
            </tr>
            <tr>
                <td align="right">Year</td>
                <td>
                    <asp:DropDownList ID="dlsYear" runat="server" AutoPostBack="false" visibe="false" />&nbsp;&nbsp;
                    Month&nbsp;
                    <asp:DropDownList ID="dlsMonth" runat="server" AutoPostBack="false" visibe="false">
                        <asp:ListItem Text="January" Value="01"></asp:ListItem>
                        <asp:ListItem Text="February" Value="02"></asp:ListItem>
                        <asp:ListItem Text="March" Value="03"></asp:ListItem>
                        <asp:ListItem Text="April" Value="04"></asp:ListItem>
                        <asp:ListItem Text="May" Value="05"></asp:ListItem>
                        <asp:ListItem Text="June" Value="06"></asp:ListItem>
                        <asp:ListItem Text="July" Value="07"></asp:ListItem>
                        <asp:ListItem Text="August" Value="08"></asp:ListItem>
                        <asp:ListItem Text="September" Value="09"></asp:ListItem>
                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:button ID="btnGetArchive" Text="Show Nominations" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table style="width:100%;border-collapse:collapse;border-spacing:5px">
        <tr>
            <td style="text-align:left"><asp:LinkButton ID="lnkAllStatusExcel" runat="server" Visible="false"><asp:image ID="imgExport" class="icon" ImageUrl="~/Images/icon/icon_export.png" runat="server" />&nbsp;Export to Excel</asp:LinkButton></td>
            <td style="text-align:right"><a href="javascript:PrintThisPage();" ><asp:image ID="imgPrint" class="icon" ImageUrl="~/Images/icon/icon_printer.png" runat="server" /> Printer Friendly Version</a></td>
        </tr>
    </table><br />
    
    <asp:Label runat="server" ID="lblError"></asp:Label>
    <div id="print_content"><%--begin print content--%>
        <asp:panel id="pnlHeader" runat="server" Visible="false">
            <asp:Label ID="lblNomSum" runat="server" visible="false" />
            <asp:Label ID="lblCustName" runat="server" visible="false" />
            <asp:Label ID="lblPipleline" runat="server" visible="false" />
            <asp:Label ID="lblBillDmd" runat="server" visible="false" />
            <asp:Label ID="lblAccountNum" runat="server" visible="false"/>
            <asp:Label ID="lblInterconnect" runat="server" visible="false"/>
            <asp:Label ID="lblBillDmdDate" runat="server" visible="false"/>
            <asp:Label ID="lblMonthYear" runat="server" visible="false"/>
            <asp:Label ID="lblMarketer" runat="server" visible="false"/>
            <asp:Label ID="lblPool" runat="server" visible="false"/>

            <asp:GridView ID="gvHeaderInfo" Font-Size=".95em"  Width="100%" ShowHeader="false" RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="center" GridLines="None" CellSpacing="0" CellPadding="1" ShowFooter="false" runat="server" BorderColor="Black">
            </asp:GridView>
        </asp:panel>

        <asp:Panel runat="server" ID="pnlView" >
            <asp:Label ID="lblEstimated" runat="server" Text="Estimated" Visible="false" Font-Italic="true" ForeColor="Blue"></asp:Label>
            <asp:GridView ID="gvNomination" Font-Size=".95em" BackColor="White" Width="100%" HeaderStyle-Font-Size=".95em" HeaderStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" RowStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="center" GridLines="Vertical" HeaderStyle-Height="40" CellSpacing="0" CellPadding="1" ShowFooter="true" runat="server" BorderColor="Black">
            </asp:GridView>
        </asp:Panel>
        <iframe id="iHTML" runat="server" width="100%" height="900" visible="false" frameborder="0" />
    </div><%--end print content--%>

    

    <script type="text/javascript">
        function PrintThisPage() 
        { 
            var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
                    sOption+="scrollbars=yes,width=1024,height=800,left=100,top=25"; 
                   
            var winprint=window.open("<%=printurl %>?t=Imbalance","Print",sOption); 
            winprint.focus(); 
        }
    </script>
</asp:Content>

