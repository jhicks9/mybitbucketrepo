Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.IO.File
Partial Class ArchiveNom
    Inherits System.Web.UI.Page
   
    Dim custAccount As String = ""
    Dim custName As String = ""
    Dim billPkgTkn As Array
    Dim tdes As New tripleDES
    Dim cwservice As New cw.Service
    Public printurl As String = ""
    Dim encrypt As String
    Dim rtnBillPackages As String
    Dim filetoread As String
    Dim strLDOM As String = Nothing
    Dim strArcDate As String = Nothing
    Dim dsNom As New DataSet
    Dim read As Decimal = 0
    Dim McfInclCG As Decimal = 0
    Dim MmBtuCG As Decimal = 0
    Dim McfCG As Decimal = 0
    Dim McfInc As Decimal = 0
    Dim AbsoluteImbalance As Decimal = 0
    Dim OutOfTol As Decimal = 0
    Dim OutOfTolFuel1 As Decimal = 0
    Dim OutOfTolFuel2 As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'can't figure out how to call nom.shownominations - created a button 
        'need to get date and filename somehow
        printurl = ResolveUrl("~/print.aspx")
        If User.IsInRole("lvtransport") Then
            custAccount = Profile.CustomerAccountNumber
            custName = Profile.UserName
            lblMessage.Text = custAccount & " - " & custName            
            pnlNominations.Visible = True
            Label1.Visible = False
            If User.IsInRole("pool") Then
                custAccount = dlsNoms.SelectedValue
                dlsNoms.Visible = True
            End If
        ElseIf User.IsInRole("Admin") Then
            dlsNoms.Visible = True
            dlsYear.Visible = True
            dlsMonth.Visible = True
            pnlNominations.Visible = True
            custAccount = dlsNoms.SelectedValue            
            ElseIf User.IsInRole("Broker") Then
                dlsNoms.Visible = True
                dlsYear.Visible = True
                dlsMonth.Visible = True
                pnlNominations.Visible = True
                custAccount = dlsNoms.SelectedValue               
            End If

            If Not IsPostBack Then
                Try
                    ' Get all profile objects
                    Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
                    dlsNoms.Items.Add("")
                dlsMonth.SelectedIndex = Now.Month() - 1
                    ' Go through the profiles
                    Dim info As ProfileInfo
                    For Each info In profiles
                        ' We need to turn a ProfileInfo into a ProfileCommon 
                        ' to access the properties to do the search
                        Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName)
                        'check for admin
                        If User.IsInRole("Admin") Then
                            'Only return lvtrans profiles
                            If (userProfile.BrokerAssigned.Length < 1) Then
                                If userProfile.CustomerAccountNumber.Length > 1 Then
                                    dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                                End If
                            Else
                                dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                            End If
                        End If

                        'check for broker to only populate with brokercanseestatement

                        If User.IsInRole("pool") And User.IsInRole("lvtransport") Then
                            If userProfile.PoolAssigned = User.Identity.Name Then
                                ' If the pool matches the pool logged in
                                ' add to dropdown list
                                dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                            End If
                        End If
                        If userProfile.BrokerAssigned.ToLower = User.Identity.Name.ToLower Then
                            ' If the broker matches the broker logged in
                            ' add to dropdown list
                            dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                        End If
                    Next
                Catch
                End Try
            Else
            End If
       
    End Sub
    Protected Sub gvNomination_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvNomination.RowCreated
        'replaced dlsMonth.SelectedValue & dlsYear.SelectedValue >= "05/01/10"
        Dim dtFormatChanged As Date = "5/01/10"
        Dim dtSelected As Date = dlsMonth.SelectedValue.ToString & "/01/" & dlsYear.SelectedValue
        Dim intDateCompare As Int32 = dtSelected.CompareTo(dtFormatChanged)

        If intDateCompare = 1 Or intDateCompare = 0 Then
            If e.Row.RowType = DataControlRowType.Header Then

                Dim oGridView As GridView = DirectCast(sender, GridView)
                Dim oGridViewRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)

                Dim oTableCell As New TableCell()
                oTableCell.Text = ""
                oTableCell.ColumnSpan = 1
                oTableCell.BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
                oGridViewRow.Height = "10"
                oGridViewRow.Cells.Add(oTableCell)

                oTableCell = New TableCell()
                oTableCell.Text = "Consumption"
                oTableCell.BorderColor = Drawing.Color.Black
                oTableCell.BackColor = Drawing.Color.WhiteSmoke
                oTableCell.BorderWidth = 1
                oTableCell.HorizontalAlign = HorizontalAlign.Center
                oTableCell.ColumnSpan = 2
                oGridViewRow.Cells.Add(oTableCell)

                oTableCell = New TableCell()
                oTableCell.Text = "Nomination"
                oTableCell.BorderColor = Drawing.Color.Black
                oTableCell.BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
                oTableCell.BorderWidth = 1
                oTableCell.HorizontalAlign = HorizontalAlign.Center
                oTableCell.ColumnSpan = 3
                oGridViewRow.Cells.Add(oTableCell)

                oTableCell = New TableCell()
                oTableCell.Text = "Imbalance"
                oTableCell.BorderColor = Drawing.Color.Black
                oTableCell.BackColor = Drawing.Color.WhiteSmoke
                oTableCell.BorderWidth = 1
                oTableCell.HorizontalAlign = HorizontalAlign.Center
                oTableCell.ColumnSpan = 4
                oGridViewRow.Cells.Add(oTableCell)

                oTableCell = New TableCell()
                oTableCell.Text = "Tolerance"
                oTableCell.BorderColor = Drawing.Color.Black
                oTableCell.BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
                oTableCell.BorderWidth = 1
                oTableCell.HorizontalAlign = HorizontalAlign.Center
                oTableCell.ColumnSpan = 3
                oGridViewRow.Cells.Add(oTableCell)

                oGridView.Controls(0).Controls.AddAt(0, oGridViewRow)
            End If
        End If

    End Sub

    Protected Sub gvHeaderInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvHeaderInfo.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then

        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells.Item(0).Font.Bold = True
            e.Row.Cells.Item(2).Font.Bold = True
            e.Row.Cells.Item(4).Font.Bold = True
        End If
    End Sub

    Protected Sub gvNomination_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvNomination.RowDataBound
        'replaced dlsMonth.SelectedValue & dlsYear.SelectedValue < "05/01/10"
        Dim dtFormatChanged As Date = "5/01/10"
        Dim dtSelected As Date = dlsMonth.SelectedValue.ToString & "/01/" & dlsYear.SelectedValue
        Dim intDateCompare As Int32 = dtSelected.CompareTo(dtFormatChanged)

        If intDateCompare = -1 Then
            gvNomination.GridLines = GridLines.None
            If e.Row.RowType = DataControlRowType.Header Then
                lblEstimated.Visible = False
                e.Row.Cells.Item(0).Text = "Day"
                e.Row.Cells.Item(1).Width = "85"
                e.Row.Cells.Item(1).Text = "Mcf Excl L&U (Burnertip)"
                e.Row.Cells.Item(2).Width = "85"
                e.Row.Cells.Item(2).Text = "Mcf Incl L&U (City Gate)"
                e.Row.Cells.Item(3).Width = "70"
                e.Row.Cells.Item(3).Text = "Mmbtu (City Gate)"
                e.Row.Cells.Item(5).Text = "BTU"
                e.Row.Cells.Item(5).Width = "75"
                e.Row.Cells.Item(4).Width = "95"
                e.Row.Cells.Item(4).Text = "Mcf (City Gate)"
                e.Row.Cells.Item(6).Width = "85"
                e.Row.Cells.Item(6).Text = "Mcf Inclusive of L&U"
                e.Row.Cells.Item(7).Text = "Cumulative Imbalance"
                e.Row.Cells.Item(8).Text = "% Nom"
                e.Row.Cells.Item(9).Text = "Absolute Imbalance"
                e.Row.Cells.Item(10).Width = "85"
                e.Row.Cells.Item(10).Text = "10% of Nominated Mcf"
                e.Row.Cells.Item(11).Text = "Out of Tolerance"
                e.Row.Cells.Item(12).Text = "Out of Tolerance + 2.15% Fuel"
                e.Row.Cells.Item(13).Width = "85"
                e.Row.Cells.Item(13).Text = "$0.2049 applied to Out of Tol. + Fuel"


            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                read += CType(e.Row.Cells.Item(1).Text, Decimal)
                McfInclCG += CType(e.Row.Cells.Item(2).Text, Decimal)
                MmBtuCG += CType(e.Row.Cells.Item(3).Text, Decimal)
                McfCG += CType(e.Row.Cells.Item(4).Text, Decimal)
                McfInc += CType(e.Row.Cells.Item(6).Text, Decimal)
                AbsoluteImbalance += CType(e.Row.Cells.Item(9).Text, Decimal)
                OutOfTol += CType(e.Row.Cells.Item(11).Text, Decimal)
                OutOfTolFuel1 += CType(e.Row.Cells.Item(12).Text, Decimal)
                OutOfTolFuel2 += CType(e.Row.Cells.Item(13).Text, Decimal)
                e.Row.Cells.Item(4).Width = "95"
                e.Row.Cells.Item(5).Width = "75"
                e.Row.Cells.Item(8).Width = "65"
                If e.Row.Cells.Item(20).Text = "Y" Then
                    e.Row.Cells.Item(1).ForeColor = Drawing.Color.Blue
                    e.Row.Cells.Item(1).Font.Italic = True
                    lblEstimated.Visible = True
                End If
                'lblPipleline.Text = e.Row.Cells.Item(14).Text
            Else

                If e.Row.RowType = DataControlRowType.Footer Then
                    e.Row.Cells.Item(0).Text = "Total"
                    e.Row.Font.Bold = True
                    e.Row.Cells.Item(1).Text = read
                    e.Row.Cells.Item(2).Text = McfInclCG
                    e.Row.Cells.Item(3).Text = MmBtuCG
                    e.Row.Cells.Item(4).Text = McfCG
                    e.Row.Cells.Item(6).Text = McfInc
                    e.Row.Cells.Item(9).Text = AbsoluteImbalance
                    e.Row.Cells.Item(11).Text = OutOfTol
                    e.Row.Cells.Item(12).Text = OutOfTolFuel1
                    e.Row.Cells.Item(13).Text = OutOfTolFuel2
                    e.Row.Cells.Item(5).Width = "95"
                End If
            End If
            Select Case lblPipleline.Text
                Case "SSTAR"
                    e.Row.Cells.Item(10).Visible = False
                    e.Row.Cells.Item(11).Visible = False
                    e.Row.Cells.Item(12).Visible = False
                    e.Row.Cells.Item(13).Visible = False
                    e.Row.Cells.Item(9).Visible = True
                Case "PEPL"
                    e.Row.Cells.Item(9).Visible = False
                    e.Row.Cells.Item(10).Visible = True
                    e.Row.Cells.Item(11).Visible = True
                    e.Row.Cells.Item(12).Visible = True
                    e.Row.Cells.Item(13).Visible = True
                Case "ANR"
                    e.Row.Cells.Item(10).Visible = False
                    e.Row.Cells.Item(11).Visible = False
                    e.Row.Cells.Item(12).Visible = False
                    e.Row.Cells.Item(13).Visible = False
                    e.Row.Cells.Item(9).Visible = True
                Case Else

            End Select
            e.Row.Cells.Item(14).Visible = False
            e.Row.Cells.Item(15).Visible = False
            e.Row.Cells.Item(16).Visible = False
            e.Row.Cells.Item(17).Visible = False
            e.Row.Cells.Item(18).Visible = False
            e.Row.Cells.Item(19).Visible = False
            e.Row.Cells.Item(20).Visible = False

        Else
            gvNomination.GridLines = GridLines.Vertical
            If e.Row.RowType = DataControlRowType.Header Then
                lblEstimated.Visible = False
                e.Row.Cells.Item(0).Text = "Day"
                e.Row.Cells.Item(0).Width = "65"
                e.Row.Cells.Item(0).HorizontalAlign = HorizontalAlign.Center

                'Consumption
                e.Row.Cells.Item(1).Width = "65"
                e.Row.Cells.Item(1).BorderWidth = 1
                e.Row.Cells.Item(1).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(1).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(1).Text = "Mcf Burnertip"
                e.Row.Cells.Item(2).Width = "60"
                e.Row.Cells.Item(2).BorderWidth = 1
                e.Row.Cells.Item(2).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(2).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(2).Text = "Mcf<br/>City Gate"

                'Nominations
                e.Row.Cells.Item(3).Width = "65"
                e.Row.Cells.Item(3).BorderWidth = 1
                e.Row.Cells.Item(3).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(3).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(3).Text = "Mmbtu City Gate"
                e.Row.Cells.Item(5).HorizontalAlign = HorizontalAlign.Center
                e.Row.Cells.Item(5).BorderWidth = 1
                e.Row.Cells.Item(5).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(5).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(5).Text = "BTU"
                e.Row.Cells.Item(5).Width = "65"
                e.Row.Cells.Item(4).Width = "60"
                e.Row.Cells.Item(4).BorderWidth = 1
                e.Row.Cells.Item(4).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(4).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(4).Text = "Mcf<br/>City Gate"

                'Imbalance
                e.Row.Cells.Item(6).Width = "60"
                e.Row.Cells.Item(6).BorderWidth = 1
                e.Row.Cells.Item(6).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(6).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(6).Text = "Mcf<br/>City Gate"
                e.Row.Cells.Item(7).Width = "55"
                e.Row.Cells.Item(7).BorderWidth = 1
                e.Row.Cells.Item(7).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(7).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(7).Text = "Cumulative Imbalance"
                e.Row.Cells.Item(8).Text = "% Nom"
                e.Row.Cells.Item(8).BorderWidth = 1
                e.Row.Cells.Item(8).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(8).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(8).HorizontalAlign = HorizontalAlign.Center
                e.Row.Cells.Item(9).Width = "65"
                e.Row.Cells.Item(9).BorderWidth = 1
                e.Row.Cells.Item(9).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(9).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(9).Text = "Absolute Imbalance"

                'Tolerance
                e.Row.Cells.Item(10).Width = "65"
                e.Row.Cells.Item(10).BorderWidth = 1
                e.Row.Cells.Item(10).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(10).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(10).Text = "Tolerance"
                e.Row.Cells.Item(11).Width = "65"
                e.Row.Cells.Item(11).BorderWidth = 1
                e.Row.Cells.Item(11).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(11).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(11).Text = "Out of Tolerance"
                e.Row.Cells.Item(12).Width = "75"
                e.Row.Cells.Item(12).BorderWidth = 1
                e.Row.Cells.Item(12).BorderColor = Drawing.Color.Black
                e.Row.Cells.Item(12).BorderStyle = BorderStyle.Solid
                e.Row.Cells.Item(12).Text = "$1.25<br/> Out of Tol."

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                read += CType(e.Row.Cells.Item(1).Text, Decimal)
                McfInclCG += CType(e.Row.Cells.Item(2).Text, Decimal)
                MmBtuCG += CType(e.Row.Cells.Item(3).Text, Decimal)
                McfCG += CType(e.Row.Cells.Item(4).Text, Decimal)
                McfInc += CType(e.Row.Cells.Item(6).Text, Decimal)
                AbsoluteImbalance += CType(e.Row.Cells.Item(9).Text, Decimal)
                OutOfTol += CType(e.Row.Cells.Item(11).Text, Decimal)
                OutOfTolFuel2 += CType(e.Row.Cells.Item(12).Text, Decimal)
                e.Row.Cells.Item(4).Width = "60"
                e.Row.Cells.Item(5).Width = "65"
                e.Row.Cells.Item(8).Width = "55"
                If e.Row.Cells.Item(20).Text = "Y" Then
                    e.Row.Cells.Item(1).ForeColor = Drawing.Color.Blue
                    e.Row.Cells.Item(1).Font.Italic = True
                    lblEstimated.Visible = True
                End If
            Else
                If e.Row.RowType = DataControlRowType.Footer Then
                    e.Row.Cells.Item(0).Text = "Total"
                    e.Row.Font.Bold = True
                    e.Row.Cells.Item(1).Text = read
                    e.Row.Cells.Item(2).Text = McfInclCG
                    e.Row.Cells.Item(3).Text = MmBtuCG
                    e.Row.Cells.Item(4).Text = McfCG
                    e.Row.Cells.Item(6).Text = McfInc
                    e.Row.Cells.Item(9).Text = AbsoluteImbalance
                    e.Row.Cells.Item(11).Text = OutOfTol
                    e.Row.Cells.Item(12).Text = OutOfTolFuel2
                    e.Row.Cells.Item(5).Width = "65"
                    e.Row.Cells.Item(4).Width = "60"
                    e.Row.Cells.Item(0).BorderWidth = 1
                    e.Row.Cells.Item(0).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(0).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(1).BorderWidth = 1
                    e.Row.Cells.Item(1).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(1).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(2).BorderWidth = 1
                    e.Row.Cells.Item(2).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(2).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(3).BorderWidth = 1
                    e.Row.Cells.Item(3).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(3).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(4).BorderWidth = 1
                    e.Row.Cells.Item(4).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(4).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(5).BorderWidth = 1
                    e.Row.Cells.Item(5).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(5).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(6).BorderWidth = 1
                    e.Row.Cells.Item(6).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(6).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(7).BorderWidth = 1
                    e.Row.Cells.Item(7).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(7).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(8).BorderWidth = 1
                    e.Row.Cells.Item(8).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(8).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(9).BorderWidth = 1
                    e.Row.Cells.Item(9).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(9).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(10).BorderWidth = 1
                    e.Row.Cells.Item(10).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(10).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(11).BorderWidth = 1
                    e.Row.Cells.Item(11).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(11).BorderStyle = BorderStyle.Solid
                    e.Row.Cells.Item(12).BorderWidth = 1
                    e.Row.Cells.Item(12).BorderColor = Drawing.Color.Black
                    e.Row.Cells.Item(12).BorderStyle = BorderStyle.Solid

                End If
            End If

            e.Row.Cells.Item(9).Visible = True
            e.Row.Cells.Item(10).Visible = True
            e.Row.Cells.Item(11).Visible = True
            e.Row.Cells.Item(12).Visible = True
            e.Row.Cells.Item(13).Visible = False
            e.Row.Cells.Item(14).Visible = False
            e.Row.Cells.Item(15).Visible = False
            e.Row.Cells.Item(16).Visible = False
            e.Row.Cells.Item(17).Visible = False
            e.Row.Cells.Item(18).Visible = False
            e.Row.Cells.Item(19).Visible = False
            e.Row.Cells.Item(20).Visible = False
            e.Row.Cells.Item(0).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(1).BackColor = Drawing.Color.WhiteSmoke
            e.Row.Cells.Item(2).BackColor = Drawing.Color.WhiteSmoke
            e.Row.Cells.Item(3).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(4).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(5).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(6).BackColor = Drawing.Color.WhiteSmoke
            e.Row.Cells.Item(7).BackColor = Drawing.Color.WhiteSmoke
            e.Row.Cells.Item(8).BackColor = Drawing.Color.WhiteSmoke
            e.Row.Cells.Item(9).BackColor = Drawing.Color.WhiteSmoke
            e.Row.Cells.Item(10).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(11).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(12).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            e.Row.Cells.Item(13).BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
        End If
    End Sub
    Protected Sub btnGetArchive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetArchive.Click
        Try
            lblNomsum.Text = Nothing
            lblError.Visible = False
            Dim Month As String = dlsMonth.SelectedValue.ToString
            Dim Year As String = dlsYear.SelectedValue.ToString
            lblEstimated.Visible = False
            'custName = dlsNoms.SelectedItem.Text
            If dlsNoms.SelectedItem.Text <> "" Then
                custName = dlsNoms.SelectedItem.Text
            End If
            lblMessage.Text = custAccount & " - " & custName
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custAccount)
            '*****************************************************************************            
            rtnBillPackages = cwservice.GetBillPackage(encrypt)

            If rtnBillPackages Is Nothing Then
                lblNomsum.Text += "bill package not found<br/>"                
            Else
                lblNomsum.Text += rtnBillPackages & "<br/>"
                billPkgTkn = Split(rtnBillPackages, ",")
                Dim dsAcctTkn As New DataSet
                'treeview?

                'For x = 0 To billPkgTkn.Length - 1
                'dsAcctTkn = cwservice.GetAccountTokens(encrypt, billPkgTkn(x))
                'Next
                
            End If
            '*****************************************************************************
            dsNom = cwservice.GetNominationInfo(encrypt, Month, Year)

            If Not dsNom Is Nothing Then
                Dim editDate As Date
                lblCustName.Text = custName
                lblAccountNum.Text = custAccount
                If dsNom.Tables(0).Rows.Count > 0 Then
                    editDate = dsNom.Tables(0).Rows(0).Item("TO_CHAR(IMBALANCE_DATE,'MM/DD/YYYY')").ToString
                    lblMonthYear.Text = editDate.Month.ToString & "/" & editDate.Year
                    lblPipleline.Text = dsNom.Tables(0).Rows(0).Item("pipeline_name").ToString
                    lblInterconnect.Text = dsNom.Tables(0).Rows(0).Item("interconnect").ToString
                    lblMarketer.Text = dsNom.Tables(0).Rows(0).Item("Marketer").ToString
                    lblPool.Text = dsNom.Tables(0).Rows(0).Item("pool").ToString
                    lblBillDmd.Text = dsNom.Tables(0).Rows(0).Item("Demand").ToString
                    If dsNom.Tables(0).Rows(0).Item("Demand_Date").ToString <> "" Then
                        lblBillDmdDate.Text = CType(dsNom.Tables(0).Rows(0).Item("Demand_Date").ToString, Date).ToShortDateString
                    Else
                        lblBillDmdDate.Text = "N/A"
                    End If
                End If
                lnkAllStatusExcel.Visible = True
                pnlHeader.Visible = True
            End If
            gvNomination.DataSource = dsNom
            gvNomination.DataBind()
            gvNomination.Visible = True

            'create headerInfo
            Dim dt As New DataTable()
            Dim dr As DataRow
            dt.Columns.Add(New DataColumn("column1", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("column2", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("column3", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("column4", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("column5", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("column6", System.Type.GetType("System.String")))
            dr = dt.NewRow()
            dr("column1") = "Customer:"
            dr("column2") = lblCustName.Text
            dr("column3") = "PipeLine:"
            dr("column4") = lblPipleline.Text
            dr("column5") = "Billing Demand:"
            dr("column6") = lblBillDmd.Text
            dt.Rows.Add(dr)
            dr = dt.NewRow()
            dr("column1") = "Account:"
            dr("column2") = lblAccountNum.Text
            dr("column3") = "Interconnect:"
            dr("column4") = lblInterconnect.Text
            dr("column5") = "Billing Demand Date:"
            dr("column6") = lblBillDmdDate.Text
            dt.Rows.Add(dr)
            dr = dt.NewRow()
            dr("column1") = "Nomination Date:"
            dr("column2") = lblMonthYear.Text
            dr("column3") = "Marketer:"
            dr("column4") = lblMarketer.Text
            dr("column5") = "Pool:"
            dr("column6") = lblPool.Text
            dt.Rows.Add(dr)
            gvHeaderInfo.DataSource = dt
            gvHeaderInfo.DataBind()


            iHTML.Visible = False
            'lblNomsum.Text += dsNom.Tables(0).Rows.Count.ToString
            If dsNom.Tables(0).Rows.Count = 0 Then
                Try
                    Dim path As String = ResolveUrl("~/lvg/ShowNomArchive.aspx") & "?m=" & Month & "&y=" & Year & "&c=" & custAccount & "&t=" & "Monthly"
                    iHTML.Attributes("src") = path
                    iHTML.Visible = True
                    lnkAllStatusExcel.Visible = False
                    pnlHeader.Visible = False
                    'lblNomsum.Text += "text" '"<a href='" & path & "' target='_blank'>View in own window</a>"
                Catch ex As Exception
                    lblNomSum.Text += "Date not found<br/>"
                    iHTML.Visible = False
                End Try
            End If
        Catch
            lblNomSum.Text += Err.Description
            lblError.Text = "error retrieving data"
            lblError.Visible = True
            pnlHeader.Visible = False
            gvNomination.Visible = False
            lblEstimated.Visible = False
            lnkAllStatusExcel.Visible = False
        End Try
    End Sub

    Protected Sub dlsYear_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlsYear.Load
        If Not IsPostBack Then
            Dim x As Int32 = 2006
            Dim EndYear As Integer
            EndYear = Now.Date.Year
            Do Until x = EndYear + 1
                dlsYear.Items.Insert(0, x.ToString)
                x = x + 1
            Loop
        End If
    End Sub

    Protected Sub lnkAllStatusExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllStatusExcel.Click
        Me.gvNomination.GridLines = GridLines.Both
        Export("Nominations.xls", Me.gvNomination)
    End Sub


    'Public Class GridViewExportUtil
    Public Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"

        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table

        table.GridLines = gv.GridLines
        '  add the header row to the table

        If (Not (gv.HeaderRow) Is Nothing) Then

            Dim headerRow As New TableRow
            Dim headerCell As New TableCell
            headerCell.ColumnSpan = 6
            headerCell.BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            headerCell.Font.Bold = True
            headerCell.Text = lblCustName.Text
            headerRow.Cells.Add(headerCell)
            headerCell = New TableCell
            headerCell.ColumnSpan = 7
            headerCell.BackColor = Drawing.ColorTranslator.FromHtml("#E8E8E8")
            headerCell.Font.Bold = True
            headerCell.Text = "Nomination Date: " & lblMonthYear.Text
            headerRow.Cells.Add(headerCell)
            table.Rows.Add(headerRow)

            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table

        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table

        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)

        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)

            If (i = 0 Or i = 1) AndAlso (current.GetType.ToString = "System.Web.UI.WebControls.DataControlFieldHeaderCell" OrElse current.GetType.ToString = "System.Web.UI.WebControls.DataControlFieldCell") Then
                current.Visible = True
                i += 1
                Continue Do
            End If  'remove edit and history columns
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
    'End Class

End Class