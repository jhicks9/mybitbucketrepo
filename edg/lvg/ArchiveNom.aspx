<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ArchiveNom.aspx.vb" Inherits="ArchiveNom" title="Untitled Page" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:Panel ID="pnlNominations" runat="server" Visible="false">
            <div style="background-color:lightgrey;border:solid 1px #36587a;padding:0px;">
            <br /><b style="font-size:16px;padding:10px;">Monthly Archived Nominations</b><br /><br />
            <b><asp:Label ID="Label1" runat="server" Visible="true" Text="Select an Account to view:"></asp:Label></b>                                                                   
                <asp:DropDownList ID="dlsNoms" runat="server" AutoPostBack="false" Visible="false">            
                </asp:DropDownList>&nbsp;&nbsp;<asp:label id="lblMessage" Runat="server" Visible="false" Font-Size="12" ForeColor="#113962" Text=""></asp:label>                                                                                     
                <asp:DropDownList ID="dlsYear" runat="server" AutoPostBack="false" visibe="false">
                    <asp:ListItem Text="2006" Value="2006"></asp:ListItem>
                    <asp:ListItem Text="2007" Value="2007"></asp:ListItem>
                    <asp:ListItem Text="2008" Value="2008"></asp:ListItem>
                    <asp:ListItem Text="2009" Value="2009"></asp:ListItem>
                    <asp:ListItem Text="2010" Value="2010"></asp:ListItem>
                    <asp:ListItem Text="2011" Value="2011"></asp:ListItem>
                    <asp:ListItem Text="2012" Value="2012"></asp:ListItem>
                    <asp:ListItem Text="2013" Value="2013"></asp:ListItem>
                    <asp:ListItem Text="2014" Value="2014"></asp:ListItem>
                    <asp:ListItem Text="2015" Value="2015"></asp:ListItem>                    
                </asp:DropDownList>
                <asp:DropDownList ID="dlsMonth" runat="server" AutoPostBack="false" visibe="false">
                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                    
                </asp:DropDownList><asp:button ID="btnGetArchive" Text="Retrieve Archive" runat="server" />      
                        <div style="padding:0% 0% 0% 0%;float:left;"><asp:Label ID="lblAccount" runat="server"></asp:Label></div>
                        <br style="clear:both;" /><br />                                                                                                                       
                        <br /><asp:Label ID="lblDirections" runat="server" Visible="true" ></asp:Label><br />                                                                                                              
            </div>
            </asp:Panel>
            <div style="padding:0% 2% 0% 0%;width:175px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" ><asp:image ID="imgPrint" ImageUrl="~/images/icon/icon_printer.png" runat="server" /> Printer Friendly Version</a></div>
                <asp:Label runat="server" ID="lblError"></asp:Label>
                <div id="print_content"><%--begin print content--%>
                    <asp:panel id="pnlHeader" runat="server" Visible="false">
                    <div style="float:left;width:350px;">
                        <asp:Label ID="lblNomSum" runat="server" visible="false"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text="Customer: " Font-Bold="true"></asp:Label><asp:Label ID="lblCustName" runat="server" visible="true"></asp:Label><br />
                        <asp:Label ID="Label5" runat="server" Text="Account: " Font-Bold="true"></asp:Label><asp:Label ID="lblAccountNum" runat="server" visible="true"></asp:Label><br />
                        <asp:Label ID="Label3" runat="server" Text="Nomination Date: " Font-Bold="true"></asp:Label><asp:Label ID="lblMonthYear" runat="server" visible="true"></asp:Label><br />
                    </div>
                    <div style="float:left;width:250px;padding-left:10px;">
                    <asp:Label ID="Label2" runat="server" Text="Pipeline: " Font-Bold="true"></asp:Label><asp:Label ID="lblPipleline" runat="server" visible="true"></asp:Label><br />                    
                    <asp:Label ID="Label6" runat="server" Text="Interconnect: " Font-Bold="true"></asp:Label><asp:Label ID="lblInterconnect" runat="server" visible="true"></asp:Label><br />
                    <asp:Label ID="Label7" runat="server" Text="Marketer: " Font-Bold="true"></asp:Label><asp:Label ID="lblMarketer" runat="server" visible="true"></asp:Label><br />
                    <asp:Label ID="Label8" runat="server" Text="Pool: " Font-Bold="true"></asp:Label><asp:Label ID="lblPool" runat="server" visible="true"></asp:Label><br />
                    </div>
                    <div style="float:left;width:350px;padding-left:10px;">
                    <asp:Label ID="Label9" runat="server" Text="Billing Demand: " Font-Bold="true"></asp:Label><asp:Label ID="lblBillDmd" runat="server" visible="true"></asp:Label><br />
                    <asp:Label ID="Label10" runat="server" Text="Billing Demand Date: " Font-Bold="true"></asp:Label><asp:Label ID="lblBillDmdDate" runat="server" visible="true"></asp:Label><br />
                    </div>
                    <br style="clear:both">
                    </asp:panel>
                    <asp:Panel runat="server" ID="pnlView" >
                    <asp:Label ID="lblEstimated" runat="server" Text="Estimated" Visible="false" Font-Italic="true" ForeColor="Blue"></asp:Label>
                        <%--<asp:GridView ID="gvNomination" BackColor="White" BorderColor="#E7E7FF" BorderStyle="Inset" BorderWidth="1px" FooterStyle-HorizontalAlign="Right" RowStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="50" CellPadding="1" ShowFooter="true" GridLines="none" runat="server">

                        </asp:GridView>--%>
                        <asp:GridView ID="gvNomination" BackColor="White" Width="90%" HeaderStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" RowStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="center" GridLines="Vertical" HeaderStyle-Height="40" CellSpacing="0" CellPadding="1" ShowFooter="true" runat="server" BorderColor="Black">

                        </asp:GridView>
                    </asp:Panel>
                    <iframe id="iHTML" runat="server" width="100%" height="900px" visible="false"></iframe> 
                </div><%--end print content--%> <asp:LinkButton ID="lnkAllStatusExcel" runat="server" Visible="false" Text="Export to Excel"></asp:LinkButton>
                <script type="text/javascript">
                function PrintThisPage() 
                { 
                   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
                         sOption+="scrollbars=yes,width=1024,height=800,left=100,top=25"; 
                   
                   var winprint=window.open("<%=printurl %>?t=Imbalance","Print",sOption); 
                   winprint.focus(); 
                }
            </script>
    
    
</asp:Content>

