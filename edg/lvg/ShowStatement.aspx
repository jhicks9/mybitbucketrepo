<%@ Page Language="VB" MasterPageFile="~/empiremaster.master" AutoEventWireup="false" CodeFile="ShowStatement.aspx.vb" Inherits="ShowStatement" title="Statements" %>
<%@ Register TagPrefix="acrobat" TagName="uc4" Src="~/adobeAcrobat.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="RightPlaceHolder" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div style="padding-top:5px;padding-bottom:5px;"><acrobat:uc4 ID="uc1" runat="server" /></div>
                <asp:Panel ID="pnlStatement" runat="server" Visible="false">
                    <asp:CheckBox ID="ckAllowBrokerStatementAccess" runat="server" AutoPostBack="true" /><asp:Label ID="lblBrokerAssigned" runat="server" Text=""></asp:Label><br />
                    <b><asp:Label ID="Label1" runat="server" Visible="true" Text="Select an Account to view:"></asp:Label></b><br /><br />               
                    <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvSummary">        
                        <asp:View ID="vwAccounts" runat="server">
                            <asp:DataList id="dlBillPkg" skinId="dlistView" runat="server" >
                                <ItemTemplate>
                                    <b>  <asp:LinkButton id="LinkButton3" Font-Bold="false" CommandArgument="statement" CommandName="<%# Container.DataItem %>" runat="server"><%# Profile.CustomerAccountNumber.ToString()%>-<%# Container.DataItem %></asp:LinkButton></b>
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:DataList id="DataList1" skinId="dlistView" runat="server">
                                <ItemTemplate>
                                    <b>  <asp:LinkButton id="LinkButton4" Font-Bold="false" CommandArgument="statement" CommandName="<%# Container.DataItem %>" runat="server" ><%# Profile.CustomerAccountNumber.ToString()%>-<%# Container.DataItem %></asp:LinkButton></b>
                                </ItemTemplate>
                            </asp:DataList>  
                            <asp:DropDownList ID="dlsNoms" runat="server" AutoPostBack="true" Visible="false">
                            </asp:DropDownList><br />
                            <asp:label id="lblMessage" Runat="server" Visible="True" Font-Size="14" forecolor="black" Font-Bold="True" Width="70%" Text=""></asp:label>                    
                        </asp:View>                    
                
                        <asp:View ID="vwStatements" runat="server">
                            <div style="padding:0% 2% 0% 0%;width:175px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" ><asp:image ID="imgPrint" class="icon" ImageUrl="~/Images/icon/icon_printer.png" runat="server" /> Printer Friendly Version</a></div>
                            <asp:HyperLink ID="hlBack" NavigateUrl="~/lvg/ShowStatement.aspx" runat="Server">Back</asp:HyperLink>
                            <div id="print_content"><%--begin print content--%>
                                <div style="padding:0% 0% 0% 0%;float:left;"><asp:Label ID="lblAccount" runat="server"></asp:Label></div>
                                <br style="clear:both;" /><br />
                                <asp:GridView ID="dgStatements" runat="server" width="100%" BackColor="White" AutoGenerateColumns="False" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Statement Date
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="left"/>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbRow" runat="server" CausesValidation="False" CommandArgument='<%#Eval("cust_statement_tkn") %>' CommandName="Select"
                                                    Text='<%#Eval("STMT_DATE") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="cust_statement_tkn" HeaderText="Statement Token" SortExpression="cust_statement_tkn" Visible="false" />
                                        <asp:BoundField ItemStyle-Width="150px" DataField="amount" HeaderStyle-HorizontalAlign="Left" HeaderText="Total Amount Due" SortExpression="amount" />
                                        <asp:BoundField DataField="duedate" HeaderStyle-HorizontalAlign="Left" HeaderText="Delinquent Date" SortExpression="duedate" />
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />                            
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <HeaderStyle BackColor="#113962" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                </asp:GridView> 
                            </div><%--end print content--%>
                            <br /><asp:Label ID="lblDirections" runat="server" Text="Select a date to display the statement."></asp:Label><br />
                        </asp:View>                                        
                    </asp:MultiView>
                </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
    function PrintThisPage() 
    { 
       var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
             sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
       var winprint=window.open("<%=printurl %>?t=Statements","Print",sOption); 
       winprint.focus(); 
    }
    </script>
</asp:Content>

