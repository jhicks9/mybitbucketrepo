Imports System.Data.SqlClient
Partial Class ShowNom
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim PictureID As Integer = Convert.ToInt32(Request.QueryString("id"))
        Dim Title As String = Request.QueryString("Title")
        Dim userId As String = Request.QueryString("userID")
        Try
            'Connect to the database and bring back the image contents & MIME type for the specified picture
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                If userId <> "" Then
                    Dim SQL As String = "SELECT MIMEType, NomData FROM Nominations WHERE cust_token = '" & userId.ToString & "' AND type = 'Daily'"
                    'Response.Write(SQL)
                    Dim myCommand As New SqlCommand(SQL, myConnection)
                    myCommand.Parameters.AddWithValue("@userId", userId.ToString)
                    myConnection.Open()
                    Dim myReader As SqlDataReader = myCommand.ExecuteReader
                    If myReader.Read Then
                        Response.ContentType = myReader("MIMEType").ToString()
                        Response.BinaryWrite(myReader("NomData"))
                    Else
                        Response.Write("Daily Nomination Not Available, try again later")
                    End If
                    myReader.Close()
                Else
                    Const SQL As String = "SELECT MIMEType, NomData FROM Nominations WHERE Title = @Title"
                    Dim myCommand As New SqlCommand(SQL, myConnection)
                    myCommand.Parameters.AddWithValue("@Title", Title)
                    myConnection.Open()
                    Dim myReader As SqlDataReader = myCommand.ExecuteReader
                    If myReader.Read Then
                        Response.ContentType = myReader("MIMEType").ToString()
                        Response.BinaryWrite(myReader("NomData"))
                    End If
                    myReader.Close()
                End If
                myConnection.Close()
            End Using
        Catch
        End Try



    End Sub
End Class
