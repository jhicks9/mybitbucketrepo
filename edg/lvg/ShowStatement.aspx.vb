Imports System.IO
Imports System.IO.File
Imports System.Data
Imports System.Data.SqlClient
Imports System
Imports System.Web
Imports ibex4
Imports ibex4.logging

Partial Class ShowStatement
    Inherits System.Web.UI.Page
    Dim xdoc As String
    Dim xtemplate As String
    Dim rtnStatement As String
    Dim rtnBillPackages, rtnUsage As String
    Dim custToken As String
    Dim txtBillPkg As String    
    Dim cwservice As New cw.Service
    Dim encrypt As String
    Dim tdes As New tripleDES
    Dim ds As New DataSet
    Dim custAccount As String = ""
    Dim custName As String = ""
    Dim billPkgTkn As Array
    Public printurl As String = ""
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.IsInRole("lvtransport") Then
            custAccount = Profile.CustomerAccountNumber
            pnlStatement.Visible = True
            Label1.Visible = False

            If User.IsInRole("pool") Then
                custAccount = dlsNoms.SelectedValue
                pnlStatement.Visible = True
            End If
        ElseIf User.IsInRole("Admin") Then
            dlsNoms.Visible = True
            pnlStatement.Visible = True
            custAccount = dlsNoms.SelectedValue
            ckAllowBrokerStatementAccess.Visible = False
        ElseIf User.IsInRole("Broker") Then
            dlsNoms.Visible = True
            pnlStatement.Visible = True
            ckAllowBrokerStatementAccess.Visible = False
            custAccount = dlsNoms.SelectedValue           
        End If
        If Not IsPostBack Then
            Try
                'Dim FullpathHtm As String = System.IO.Path.GetTempPath()
                'Dim dirInfo As New DirectoryInfo(FullpathHtm)

                ' Get all profile objects
                Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
                dlsNoms.Items.Add("")

                ' Go through the profiles
                Dim info As ProfileInfo
                For Each info In profiles
                    ' We need to turn a ProfileInfo into a ProfileCommon 
                    ' to access the properties to do the search
                    Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName)
                    'check for admin
                    If User.IsInRole("Admin") Then
                        'Only return lvtrans profiles

                        If (userProfile.BrokerAssigned.Length < 1) Then
                            If userProfile.CustomerAccountNumber.Length > 1 Then
                                dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                            End If
                        Else
                            'If userProfile.PoolAssigned.Length > 1 Then

                            'Else
                            'lblMessage.Text = userProfile.PoolAssigned & "<br/>"
                            dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                            'End If
                        End If
                    End If
                    If User.IsInRole("lvtransport") Then
                        lblBrokerAssigned.Text = "Allow " & Profile.BrokerAssigned & " to view my statments."
                        If Profile.UserName = userProfile.UserName Then
                            ckAllowBrokerStatementAccess.Checked = Profile.BrokerCanSeeStatement
                        End If
                    End If
                    'check for broker to only populate with brokercanseestatement

                    If Trim(userProfile.BrokerAssigned.ToLower) = Trim(User.Identity.Name.ToLower) Then

                        If userProfile.BrokerCanSeeStatement = "True" Then
                            'Response.Write("broker can see " & userProfile.BrokerCanSeeStatement)
                            dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                            'ElseIf userProfile.BrokerCanSeeStatement = "False" Then
                            '    'Response.Write("broker can see " & userProfile.BrokerCanSeeStatement)
                            '    dlsNoms.Items.Remove(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))

                        End If
                        'dlsNoms.DataBind()
                    End If
                    If User.IsInRole("pool") And User.IsInRole("lvtransport") Then
                        If userProfile.PoolAssigned = User.Identity.Name Then
                            ' If the pool matches the pool logged in
                            ' add to dropdown list
                            dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                        End If
                    End If
                    'If userProfile.BrokerAssigned.ToLower = User.Identity.Name Then
                    '    ' If the broker matches the broker logged in
                    '    ' add to dropdown list
                    '    dlsNoms.Items.Add(New ListItem(userProfile.UserName, userProfile.CustomerAccountNumber))
                    'End If
                Next

            Catch
            End Try

            Load_grid("dlbillpkg")
        Else
            'If Not Profile.CustomerAccountNumber Is Nothing Then
            Load_grid("datalist1")
        End If
        'Load_grid("dlbillpkg")
        'End If
    End Sub
    Private Sub Load_grid(ByVal grid As String)
        Try
            'Label1.Visible = True

            printurl = ResolveUrl("~/print.aspx")
            'custAccount = Profile.CustomerAccountNumber
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(custAccount)
            rtnBillPackages = cwservice.GetLVGBillPackage(encrypt)

            If rtnBillPackages Is Nothing Then

                Dim lbl As New Label
                If Profile.CustomerAccountNumber = "" Then
                Else
                    'lbl.Text = "Data is currently unavailable.<br /><br />Please try again later or check the Announcement section of our <a href='" & ResolveUrl("~/default.aspx") & "' runat='server'>Home Page</a> for any scheduled maintenance."
                    lbl.ForeColor = Drawing.Color.Red
                    vwAccounts.Controls.Add(lbl)
                End If
            Else

                billPkgTkn = Split(rtnBillPackages, ",")
                If grid = "dlbillpkg" Then
                    dlBillPkg.DataSource = billPkgTkn
                    dlBillPkg.DataBind()
                    dlBillPkg.Visible = True
                    DataList1.Visible = False

                ElseIf grid = "datalist1" Then
                    DataList1.DataSource = billPkgTkn
                    DataList1.DataBind()
                    DataList1.Visible = True
                    dlBillPkg.Visible = False

                End If
            End If
        Catch

        End Try
    End Sub
    Private Sub handleClick(ByVal command As String)
        Try
            Dim rtnValues As New System.Data.DataSet
            Dim rtnAcctTkns As New System.Data.DataSet
            Dim encryptcustToken As String
            lblAccount.Font.Size = "10"
            lblAccount.Text = "*Select a date to display the statement for account " & Profile.CustomerAccountNumber & "-" & command & "."
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encryptcustToken = tdes.Encrypt(custAccount)

            rtnValues = cwservice.GetBillInfo(encryptcustToken, command, "1096")

            'add 3 columns (charges,taxes,total) to gridview
            Dim dc As DataColumn
            dc = New DataColumn("amount", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            dc = New DataColumn("duedate", System.Type.GetType("System.String"))
            rtnValues.Tables(0).Columns.Add(dc)
            Dim dr As DataRow
            Dim increment As Integer = 0
            For Each dr In rtnValues.Tables(0).Rows
                encrypt = tdes.Encrypt(rtnValues.Tables(0).Rows(increment).Item("cust_statement_tkn"))
                rtnStatement = cwservice.GetCustomerChargeInfo(encrypt)
                If Len(rtnStatement) > 0 And InStr(rtnStatement, ";") > 0 Then
                    rtnValues.Tables(0).Rows(increment).Item("amount") = rtnStatement.Split(";")(0).Trim
                    rtnValues.Tables(0).Rows(increment).Item("duedate") = rtnStatement.Split(";")(1).Trim
                Else
                    rtnValues.Tables(0).Rows(increment).Item("amount") = ""
                    rtnValues.Tables(0).Rows(increment).Item("duedate") = ""
                End If
                increment = increment + 1
            Next

            dgStatements.DataSource = rtnValues
            dgStatements.DataBind()

            lblAccount.Text = "Statements for " & dlsNoms.SelectedItem.Text & " " & custAccount & "-" & billPkgTkn(0)
            mvSummary.ActiveViewIndex = 1
        Catch
        End Try
    End Sub
    Protected Sub dlBillPkg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlBillPkg.ItemCommand
        handleClick(e.CommandName)
    End Sub

    Protected Sub datalist1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataList1.ItemCommand
        handleClick(e.CommandName)
    End Sub
   

    Protected Sub dgStatements_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgStatements.RowCommand
        Try
            If e.CommandName = "Select" Then
                Dim statementToken As String

                statementToken = e.CommandArgument
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "popup", "window.open('" & ResolveUrl("~/DisplayPDF.aspx") & "?x=" & statementToken & "&a=" & custAccount & "')", True)
               
           
            End If
        Catch
        End Try
    End Sub
    

    Protected Sub DataList1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DataList1.ItemDataBound
        cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
        encrypt = tdes.Encrypt(custAccount)
        rtnBillPackages = cwservice.GetLVGBillPackage(encrypt)
        billPkgTkn = Split(rtnBillPackages, ",")

        Dim lb As LinkButton = e.Item.FindControl("Linkbutton4")
        lb.Text = custAccount & "-" & billPkgTkn(e.Item.ItemIndex)
    End Sub

    
    Protected Sub ckAllowBrokerStatementAccess_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ckAllowBrokerStatementAccess.CheckedChanged

        If ckAllowBrokerStatementAccess.Checked Then
            Profile.BrokerCanSeeStatement = True
        Else
            Profile.BrokerCanSeeStatement = False
        End If
        
    End Sub
End Class
