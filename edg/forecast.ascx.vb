﻿Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Script.Serialization
Imports System.Runtime.Serialization
Imports System.Globalization

Partial Class forecast
    Inherits System.Web.UI.UserControl

    Private _zipcode As String = ""
    Private _currentconditions As Boolean = False
    Private _currentimagesize As String = "93"
    Private _forecastimagesize As String = "93"
    Private _daystoforecast As Integer = "1"
    Private _observed As Boolean = False

    Public Property zipcode() As String
        Get
            Return _zipcode
        End Get
        Set(ByVal value As String)
            _zipcode = value
        End Set
    End Property

    Public Property DisplayCurrentConditions() As Boolean
        Get
            Return _currentconditions
        End Get
        Set(ByVal value As Boolean)
            _currentconditions = value
        End Set
    End Property

    Public Property CurrentImageSize() As String
        Get
            Return _currentimagesize
        End Get
        Set(ByVal value As String)
            _currentimagesize = value
        End Set
    End Property

    Public Property ForecastImageSize() As String
        Get
            Return _forecastimagesize
        End Get
        Set(ByVal value As String)
            _forecastimagesize = value
        End Set
    End Property

    Public Property DaysToForecast() As Integer
        Get
            Return _daystoforecast
        End Get
        Set(ByVal value As Integer)
            If Not IsNumeric(value) Then
                value = 1
            Else
                If value < 0 Then value = 0
                If value > 5 Then value = 5
            End If
            _daystoforecast = value
        End Set
    End Property

    <ComponentModel.DefaultValue(False)> _
    Public Property DisplayObserved() As Boolean
        Get
            Return _observed
        End Get
        Set(ByVal value As Boolean)
            _observed = value
        End Set
    End Property

    Dim imgurl As String = "~/images/weatherImages/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim info As ForecastIO.ForecastIOResponse = GetWeatherInfo(_zipcode)
            If DisplayCurrentConditions Then
                'imgCurrentWeather.Attributes("class") = getClassName(info.currently.icon.ToString )  'to change class on a span
                imgCurrentWeather.Src = ResolveUrl(imgurl & getIconName(info.currently.icon.ToString))
                imgCurrentWeather.Width = CurrentImageSize
                imgCurrentWeather.Height = CurrentImageSize
                lblCurrentTemp.Text = Math.Round(Val(info.currently.temperature.ToString), 0).ToString & "º"
                lblCurrentDescription.Text = info.currently.summary & " - Feels like " & Math.Round(Val(info.currently.apparentTemperature), 0).ToString & "º"
                lblCurrentHumidity.Text = (Val(info.currently.humidity.ToString) * 100).ToString & "%"
                lblCurrentWind.Text = getWindDirection(Val(info.currently.windBearing.ToString)) 
                lblCurrentWindSpeed.Text = Math.Round(Val(info.currently.windspeed.ToString), 0).ToString & "mph"
                lblCurrentSummary.Text = info.hourly.summary.ToString '(Val(info.daily.data(0).precipProbability.ToString) * 100).ToString & "%"
                lblCurrentHigh.Text = String.Format("High, {1} at {0:htt}",DateAdd(DateInterval.Hour, Val(info.offset.ToString), ConvertFromUnixTimestamp (Val(info.daily.data(0).temperatureMaxTime.ToString))), Math.Round(Val(info.daily.data(0).temperatureMax.ToString), 0).ToString & "º")
                lblCurrentLow.Text = String.Format("Low, {1} at {0:htt}",DateAdd(DateInterval.Hour, Val(info.offset.ToString), ConvertFromUnixTimestamp (Val(info.daily.data(0).temperatureMinTime.ToString))), Math.Round(Val(info.daily.data(0).temperatureMin.ToString), 0).ToString & "º")
                pnlCurrent.Visible = True

                lblDay0.Text = "Today"
                imgDay0.Src = ResolveUrl(imgurl & getIconName(info.daily.data(0).icon.ToString))
                imgDay0.Width = ForecastImageSize
                imgDay0.Height = ForecastImageSize
                lblDay0High.Text = Math.Round(Val(info.daily.data(0).temperatureMax.ToString), 0).ToString & "º"
                lblDay0Low.Text = Math.Round(Val(info.daily.data(0).temperatureMin.ToString), 0).ToString & "º"
                lblDay0Desc.Text = info.daily.data(0).summary.ToString
                pnlDay0.Visible = True
            End If

            If DaysToForecast > 0 Then
                lblDay1.Text = DateAdd(DateInterval.Day,1,DateTime.now).DayOfWeek.ToString
                imgDay1.Src = ResolveUrl(imgurl & getIconName(info.daily.data(1).icon.ToString))
                imgDay1.Width = ForecastImageSize
                imgDay1.Height = ForecastImageSize
                lblDay1High.Text = Math.Round(Val(info.daily.data(1).temperatureMax.ToString), 0).ToString & "º"
                lblDay1Low.Text = Math.Round(Val(info.daily.data(1).temperatureMin.ToString), 0).ToString & "º"
                lblDay1Desc.Text = info.daily.data(1).summary.ToString
                pnlDay1.Visible = True
            End If

            If DaysToForecast > 1 Then
                lblDay2.Text = DateAdd(DateInterval.Day,2,DateTime.now).DayOfWeek.ToString
                imgDay2.Src = ResolveUrl(imgurl & getIconName(info.daily.data(2).icon.ToString))
                imgDay2.Width = ForecastImageSize
                imgDay2.Height = ForecastImageSize
                lblDay2High.Text = Math.Round(Val(info.daily.data(2).temperatureMax.ToString), 0).ToString & "º"
                lblDay2Low.Text = Math.Round(Val(info.daily.data(2).temperatureMin.ToString), 0).ToString & "º"
                lblDay2Desc.Text = info.daily.data(2).summary.ToString
                pnlDay2.Visible = True
            End If

            If DaysToForecast > 2 Then
                lblDay3.Text = DateAdd(DateInterval.Day,3,DateTime.now).DayOfWeek.ToString
                imgDay3.Src = ResolveUrl(imgurl & getIconName(info.daily.data(3).icon.ToString))
                imgDay3.Width = ForecastImageSize
                imgDay3.Height = ForecastImageSize
                lblDay3High.Text = Math.Round(Val(info.daily.data(3).temperatureMax.ToString), 0).ToString & "º"
                lblDay3Low.Text = Math.Round(Val(info.daily.data(3).temperatureMin.ToString), 0).ToString & "º"
                lblDay3Desc.Text = info.daily.data(3).summary.ToString
                pnlDay3.Visible = True
            End If

            If DaysToForecast > 3 Then
                lblDay4.Text = DateAdd(DateInterval.Day,4,DateTime.now).DayOfWeek.ToString
                imgDay4.Src = ResolveUrl(imgurl & getIconName(info.daily.data(4).icon.ToString))
                imgDay4.Width = ForecastImageSize
                imgDay4.Height = ForecastImageSize
                lblDay4High.Text = Math.Round(Val(info.daily.data(4).temperatureMax.ToString), 0).ToString & "º"
                lblDay4Low.Text = Math.Round(Val(info.daily.data(4).temperatureMin.ToString), 0).ToString & "º"
                lblDay4Desc.Text = info.daily.data(4).summary.ToString
                pnlDay4.Visible = True
            End If

            If DaysToForecast > 4 Then
                lblDay5.Text = DateAdd(DateInterval.Day,5,DateTime.now).DayOfWeek.ToString
                imgDay5.Src = ResolveUrl(imgurl & getIconName(info.daily.data(5).icon.ToString))
                imgDay5.Width = ForecastImageSize
                imgDay5.Height = ForecastImageSize
                lblDay5High.Text = Math.Round(Val(info.daily.data(5).temperatureMax.ToString), 0).ToString & "º"
                lblDay5Low.Text = Math.Round(Val(info.daily.data(5).temperatureMin.ToString), 0).ToString & "º"
                lblDay5Desc.Text = info.daily.data(5).summary.ToString
                pnlDay5.Visible = True
            End If

            If DisplayObserved Then
                lblCity.Text = "Weather for " & getCitybyZip(zipcode) & " observed at " &  String.Format("{0:MM/dd/yyyy hh:mmtt}",DateAdd(DateInterval.Hour, Val(info.offset.ToString), ConvertFromUnixTimestamp (Val(info.currently.time.ToString))))
                pnlObserved.Visible = True
            End If
        Catch
        End Try
    End Sub

    Public Shared Function GetWeatherInfo(ByVal zipCode As String) As ForecastIO.ForecastIOResponse
        Dim cwservice As New cw.Service
        Dim weatherInfo As ForecastIO.ForecastIOResponse = New ForecastIO.ForecastIOResponse
        Try
            Dim ms As New System.IO.MemoryStream(cwservice.GetWeatherAPIData(zipCode))
            ms.Position = 0
            Dim reader As New StreamReader(ms)
            Dim json As string = reader.ReadToEnd()
            weatherInfo = (New JavaScriptSerializer()).Deserialize(Of ForecastIO.ForecastIOResponse)(json)
        Catch ex As Exception
        End Try
        Return weatherInfo
    End Function

    Private Function getCitybyZip(ByVal zipCode As String) As String
        Dim locationData As String = ""
        Try
            Dim cwservice As New cw.Service
            locationData = cwservice.getLatLngbyZip(zipcode) 'get latitude/longitude for api
            locationData = String.Format("{0}, {1}", locationData.Split (";")(2).ToString, locationData.Split (";")(3).ToString)
        Catch
            locationData = ""
        End Try
        Return locationData
    End Function

    Private Function ConvertFromUnixTimestamp(ByVal timestamp As Double) As DateTime
	    Dim origin As New DateTime(1970, 1, 1, 0, 0, 0, 0)
	    Return origin.AddSeconds(timestamp)
    End Function

    Private Function getWindDirection(ByVal bearing As single) As String
        Dim direction As String = ""
        Select Case bearing
            Case 0 To 22
                direction = "N"
            Case 23 To 67
                direction = "NE"
            Case 68 To 112
                direction = "E"
            Case 113 To 157
                direction = "SE"
            Case 158 To 202
                direction = "S"
            Case 203 To 247
                direction = "SW"
            Case 248 To 292
                direction = "W"
            Case 293 To 337
                direction = "NW"
            Case 338 To 360
                direction = "N"
            Case Else
                direction = ""
    End Select
        Return direction
    End Function

    Public Function getIconName (ByVal icon As string) As String
        Dim iconname As String = ""
        Select Case icon
            Case "clear-day"
                iconname = "36.png"
            Case "clear-night"
                iconname = "31.png"
            Case "partly-cloudy-day"
                iconname = "28.png"
            Case "partly-cloudy-night"
                iconname = "29.png"
            Case "cloudy"
                iconname = "26.png"
            Case "rain"
                iconname = "12.png"
            Case "sleet"
                iconname = "05.png"
            Case "snow"
                iconname = "14.png"
            Case "wind"
                iconname = "48.png"
            Case "fog"
                iconname = "21.png"
            Case Else
                iconname = "na.png"
        End Select
        Return iconname
    End Function

End Class

Namespace ForecastIO
    Public Class ForecastIOResponse

        <DataMember> _
        Public Property latitude() As Single
            Get
                Return m_latitude
            End Get
            Set(value As Single)
                m_latitude = value
            End Set
        End Property
        Private m_latitude As Single
        <DataMember> _
        Public Property longitude() As Single
            Get
                Return m_longitude
            End Get
            Set(value As Single)
                m_longitude = value
            End Set
        End Property
        Private m_longitude As Single
        <DataMember> _
        Public Property timezone() As String
            Get
                Return m_timezone
            End Get
            Set(value As String)
                m_timezone = value
            End Set
        End Property
        Private m_timezone As String
        <DataMember> _
        Public Property offset() As Single
            Get
                Return m_offset
            End Get
            Set(value As Single)
                m_offset = value
            End Set
        End Property
        Private m_offset As Single
        <DataMember> _
        Public Property currently() As Currently
            Get
                Return m_currently
            End Get
            Set(value As Currently)
                m_currently = value
            End Set
        End Property
        Private m_currently As Currently
        <DataMember> _
        Public Property minutely() As Minutely
            Get
                Return m_minutely
            End Get
            Set(value As Minutely)
                m_minutely = value
            End Set
        End Property
        Private m_minutely As Minutely
        <DataMember> _
        Public Property hourly() As Hourly
            Get
                Return m_hourly
            End Get
            Set(value As Hourly)
                m_hourly = value
            End Set
        End Property
        Private m_hourly As Hourly
        <DataMember> _
        Public Property daily() As Daily
            Get
                Return m_daily
            End Get
            Set(value As Daily)
                m_daily = value
            End Set
        End Property
        Private m_daily As Daily
        <DataMember> _
        Public Property alerts() As List(Of Alert)
            Get
                Return m_alerts
            End Get
            Set(value As List(Of Alert))
                m_alerts = value
            End Set
        End Property
        Private m_alerts As List(Of Alert)
        <DataMember> _
        Public Property flags() As Flags
            Get
                Return m_flags
            End Get
            Set(value As Flags)
                m_flags = value
            End Set
        End Property
        Private m_flags As Flags
    End Class

    <DataContract> _
    Public Class Currently
        <DataMember> _
        Public Property time() As Int64
            Get
                Return m_time
            End Get
            Set(value As Int64)
                m_time = value
            End Set
        End Property
        Private m_time As Int64
        <DataMember> _
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        <DataMember> _
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        <DataMember> _
        Public Property nearestStormDistance() As Single
            Get
                Return m_nearestStormDistance
            End Get
            Set(value As Single)
                m_nearestStormDistance = value
            End Set
        End Property
        Private m_nearestStormDistance As Single
        <DataMember> _
        Public Property nearestStormBearing() As Single
            Get
                Return m_nearestStormBearing
            End Get
            Set(value As Single)
                m_nearestStormBearing = value
            End Set
        End Property
        Private m_nearestStormBearing As Single
        <DataMember> _
        Public Property precipIntensity() As Single
            Get
                Return m_precipIntensity
            End Get
            Set(value As Single)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Single
        <DataMember> _
        Public Property precipProbability() As Single
            Get
                Return m_precipProbability
            End Get
            Set(value As Single)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Single
        <DataMember> _
        Public Property precipType() As String
            Get
                Return m_precipType
            End Get
            Set(value As String)
                m_precipType = value
            End Set
        End Property
        Private m_precipType As String
        <DataMember> _
        Public Property temperature() As Single
            Get
                Return m_temperature
            End Get
            Set(value As Single)
                m_temperature = value
            End Set
        End Property
        Private m_temperature As Single
        <DataMember> _
        Public Property apparentTemperature() As Single
            Get
                Return m_apparentTemperature
            End Get
            Set(value As Single)
                m_apparentTemperature = value
            End Set
        End Property
        Private m_apparentTemperature As Single
        <DataMember> _
        Public Property dewPoint() As Single
            Get
                Return m_dewPoint
            End Get
            Set(value As Single)
                m_dewPoint = value
            End Set
        End Property
        Private m_dewPoint As Single
        <DataMember> _
        Public Property windSpeed() As Single
            Get
                Return m_windSpeed
            End Get
            Set(value As Single)
                m_windSpeed = value
            End Set
        End Property
        Private m_windSpeed As Single
        <DataMember> _
        Public Property windBearing() As Single
            Get
                Return m_windBearing
            End Get
            Set(value As Single)
                m_windBearing = value
            End Set
        End Property
        Private m_windBearing As Single
        <DataMember> _
        Public Property cloudCover() As Single
            Get
                Return m_cloudCover
            End Get
            Set(value As Single)
                m_cloudCover = value
            End Set
        End Property
        Private m_cloudCover As Single
        <DataMember> _
        Public Property humidity() As Single
            Get
                Return m_humidity
            End Get
            Set(value As Single)
                m_humidity = value
            End Set
        End Property
        Private m_humidity As Single
        <DataMember> _
        Public Property pressure() As Single
            Get
                Return m_pressure
            End Get
            Set(value As Single)
                m_pressure = value
            End Set
        End Property
        Private m_pressure As Single
        <DataMember> _
        Public Property visibility() As Single
            Get
                Return m_visibility
            End Get
            Set(value As Single)
                m_visibility = value
            End Set
        End Property
        Private m_visibility As Single
        <DataMember> _
        Public Property ozone() As Single
            Get
                Return m_ozone
            End Get
            Set(value As Single)
                m_ozone = value
            End Set
        End Property
        Private m_ozone As Single
    End Class
    <DataContract> _
    Public Class MinuteForecast
        <DataMember> _
        Public Property time() As Int64
            Get
                Return m_time
            End Get
            Set(value As Int64)
                m_time = value
            End Set
        End Property
        Private m_time As Int64
        <DataMember> _
        Public Property precipIntensity() As Single
            Get
                Return m_precipIntensity
            End Get
            Set(value As Single)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Single
    End Class
    <DataContract> _
    Public Class Minutely
        <DataMember> _
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        <DataMember> _
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        <DataMember> _
        Public Property data() As List(Of MinuteForecast)
            Get
                Return m_data
            End Get
            Set(value As List(Of MinuteForecast))
                m_data = value
            End Set
        End Property
        Private m_data As List(Of MinuteForecast)
    End Class
    <DataContract> _
    Public Class HourForecast
        <DataMember> _
        Public Property time() As Int64
            Get
                Return m_time
            End Get
            Set(value As Int64)
                m_time = value
            End Set
        End Property
        Private m_time As Int64
        <DataMember> _
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        <DataMember> _
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        <DataMember> _
        Public Property precipIntensity() As Single
            Get
                Return m_precipIntensity
            End Get
            Set(value As Single)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Single
        <DataMember> _
        Public Property precipProbability() As Single
            Get
                Return m_precipProbability
            End Get
            Set(value As Single)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Single
        <DataMember> _
        Public Property precipType() As String
            Get
                Return m_precipType
            End Get
            Set(value As String)
                m_precipType = value
            End Set
        End Property
        Private m_precipType As String
        <DataMember> _
        Public Property temperature() As Single
            Get
                Return m_temperature
            End Get
            Set(value As Single)
                m_temperature = value
            End Set
        End Property
        Private m_temperature As Single
        <DataMember> _
        Public Property apparentTemperature() As Single
            Get
                Return m_apparentTemperature
            End Get
            Set(value As Single)
                m_apparentTemperature = value
            End Set
        End Property
        Private m_apparentTemperature As Single
        <DataMember> _
        Public Property dewPoint() As Single
            Get
                Return m_dewPoint
            End Get
            Set(value As Single)
                m_dewPoint = value
            End Set
        End Property
        Private m_dewPoint As Single
        <DataMember> _
        Public Property windSpeed() As Single
            Get
                Return m_windSpeed
            End Get
            Set(value As Single)
                m_windSpeed = value
            End Set
        End Property
        Private m_windSpeed As Single
        <DataMember> _
        Public Property windBearing() As Single
            Get
                Return m_windBearing
            End Get
            Set(value As Single)
                m_windBearing = value
            End Set
        End Property
        Private m_windBearing As Single
        <DataMember> _
        Public Property cloudCover() As Single
            Get
                Return m_cloudCover
            End Get
            Set(value As Single)
                m_cloudCover = value
            End Set
        End Property
        Private m_cloudCover As Single
        <DataMember> _
        Public Property humidity() As Single
            Get
                Return m_humidity
            End Get
            Set(value As Single)
                m_humidity = value
            End Set
        End Property
        Private m_humidity As Single
        <DataMember> _
        Public Property pressure() As Single
            Get
                Return m_pressure
            End Get
            Set(value As Single)
                m_pressure = value
            End Set
        End Property
        Private m_pressure As Single
        <DataMember> _
        Public Property visibility() As Single
            Get
                Return m_visibility
            End Get
            Set(value As Single)
                m_visibility = value
            End Set
        End Property
        Private m_visibility As Single
        <DataMember> _
        Public Property ozone() As Single
            Get
                Return m_ozone
            End Get
            Set(value As Single)
                m_ozone = value
            End Set
        End Property
        Private m_ozone As Single
    End Class
    <DataContract> _
    Public Class Hourly
        <DataMember> _
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        <DataMember> _
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        <DataMember> _
        Public Property data() As List(Of HourForecast)
            Get
                Return m_data
            End Get
            Set(value As List(Of HourForecast))
                m_data = value
            End Set
        End Property
        Private m_data As List(Of HourForecast)
    End Class
    <DataContract> _
    Public Class DailyForecast
        <DataMember> _
        Public Property time() As Int64
            Get
                Return m_time
            End Get
            Set(value As Int64)
                m_time = value
            End Set
        End Property
        Private m_time As Int64
        <DataMember> _
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        <DataMember> _
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        <DataMember> _
        Public Property sunriseTime() As Int64
            Get
                Return m_sunriseTime
            End Get
            Set(value As Int64)
                m_sunriseTime = value
            End Set
        End Property
        Private m_sunriseTime As Int64
        <DataMember> _
        Public Property sunsetTime() As Int64
            Get
                Return m_sunsetTime
            End Get
            Set(value As Int64)
                m_sunsetTime = value
            End Set
        End Property
        Private m_sunsetTime As Int64
        <DataMember> _
        Public Property moonPhase() As Single
            Get
                Return m_moonPhase
            End Get
            Set(value As Single)
                m_moonPhase = value
            End Set
        End Property
        Private m_moonPhase As Single
        <DataMember> _
        Public Property precipAccumulation() As Single
            Get
                Return m_precipAccumulation
            End Get
            Set(value As Single)
                m_precipAccumulation = value
            End Set
        End Property
        Private m_precipAccumulation As Single
        <DataMember> _
        Public Property precipIntensity() As Single
            Get
                Return m_precipIntensity
            End Get
            Set(value As Single)
                m_precipIntensity = value
            End Set
        End Property
        Private m_precipIntensity As Single
        <DataMember> _
        Public Property precipIntensityMax() As Single
            Get
                Return m_precipIntensityMax
            End Get
            Set(value As Single)
                m_precipIntensityMax = value
            End Set
        End Property
        Private m_precipIntensityMax As Single
        <DataMember> _
        Public Property precipIntensityMaxTime() As Int64
            Get
                Return m_precipIntensityMaxTime
            End Get
            Set(value As Int64)
                m_precipIntensityMaxTime = value
            End Set
        End Property
        Private m_precipIntensityMaxTime As Int64
        <DataMember> _
        Public Property precipProbability() As Single
            Get
                Return m_precipProbability
            End Get
            Set(value As Single)
                m_precipProbability = value
            End Set
        End Property
        Private m_precipProbability As Single
        <DataMember> _
        Public Property precipType() As String
            Get
                Return m_precipType
            End Get
            Set(value As String)
                m_precipType = value
            End Set
        End Property
        Private m_precipType As String
        <DataMember> _
        Public Property temperatureMin() As Single
            Get
                Return m_temperatureMin
            End Get
            Set(value As Single)
                m_temperatureMin = value
            End Set
        End Property
        Private m_temperatureMin As Single
        <DataMember> _
        Public Property temperatureMinTime() As Int64
            Get
                Return m_temperatureMinTime
            End Get
            Set(value As Int64)
                m_temperatureMinTime = value
            End Set
        End Property
        Private m_temperatureMinTime As Int64
        <DataMember> _
        Public Property temperatureMax() As Single
            Get
                Return m_temperatureMax
            End Get
            Set(value As Single)
                m_temperatureMax = value
            End Set
        End Property
        Private m_temperatureMax As Single
        <DataMember> _
        Public Property temperatureMaxTime() As Int64
            Get
                Return m_temperatureMaxTime
            End Get
            Set(value As Int64)
                m_temperatureMaxTime = value
            End Set
        End Property
        Private m_temperatureMaxTime As Int64
        <DataMember> _
        Public Property apparentTemperatureMin() As Single
            Get
                Return m_apparentTemperatureMin
            End Get
            Set(value As Single)
                m_apparentTemperatureMin = value
            End Set
        End Property
        Private m_apparentTemperatureMin As Single
        <DataMember> _
        Public Property apparentTemperatureMinTime() As Int64
            Get
                Return m_apparentTemperatureMinTime
            End Get
            Set(value As Int64)
                m_apparentTemperatureMinTime = value
            End Set
        End Property
        Private m_apparentTemperatureMinTime As Int64
        <DataMember> _
        Public Property apparentTemperatureMax() As Single
            Get
                Return m_apparentTemperatureMax
            End Get
            Set(value As Single)
                m_apparentTemperatureMax = value
            End Set
        End Property
        Private m_apparentTemperatureMax As Single
        <DataMember> _
        Public Property apparentTemperatureMaxTime() As Int64
            Get
                Return m_apparentTemperatureMaxTime
            End Get
            Set(value As Int64)
                m_apparentTemperatureMaxTime = value
            End Set
        End Property
        Private m_apparentTemperatureMaxTime As Int64
        <DataMember> _
        Public Property dewPoint() As Single
            Get
                Return m_dewPoint
            End Get
            Set(value As Single)
                m_dewPoint = value
            End Set
        End Property
        Private m_dewPoint As Single
        <DataMember> _
        Public Property windSpeed() As Single
            Get
                Return m_windSpeed
            End Get
            Set(value As Single)
                m_windSpeed = value
            End Set
        End Property
        Private m_windSpeed As Single
        <DataMember> _
        Public Property windBearing() As Single
            Get
                Return m_windBearing
            End Get
            Set(value As Single)
                m_windBearing = value
            End Set
        End Property
        Private m_windBearing As Single
        <DataMember> _
        Public Property cloudCover() As Single
            Get
                Return m_cloudCover
            End Get
            Set(value As Single)
                m_cloudCover = value
            End Set
        End Property
        Private m_cloudCover As Single
        <DataMember> _
        Public Property humidity() As Single
            Get
                Return m_humidity
            End Get
            Set(value As Single)
                m_humidity = value
            End Set
        End Property
        Private m_humidity As Single
        <DataMember> _
        Public Property pressure() As Single
            Get
                Return m_pressure
            End Get
            Set(value As Single)
                m_pressure = value
            End Set
        End Property
        Private m_pressure As Single
        <DataMember> _
        Public Property visibility() As Single
            Get
                Return m_visibility
            End Get
            Set(value As Single)
                m_visibility = value
            End Set
        End Property
        Private m_visibility As Single
        <DataMember> _
        Public Property ozone() As Single
            Get
                Return m_ozone
            End Get
            Set(value As Single)
                m_ozone = value
            End Set
        End Property
        Private m_ozone As Single
    End Class
    <DataContract> _
    Public Class Daily
        <DataMember> _
        Public Property summary() As String
            Get
                Return m_summary
            End Get
            Set(value As String)
                m_summary = value
            End Set
        End Property
        Private m_summary As String
        <DataMember> _
        Public Property icon() As String
            Get
                Return m_icon
            End Get
            Set(value As String)
                m_icon = value
            End Set
        End Property
        Private m_icon As String
        <DataMember> _
        Public Property data() As List(Of DailyForecast)
            Get
                Return m_data
            End Get
            Set(value As List(Of DailyForecast))
                m_data = value
            End Set
        End Property
        Private m_data As List(Of DailyForecast)
    End Class

    <DataContract> _
    Public Class Alert
        <DataMember> _
        Public Property title() As String
            Get
                Return m_title
            End Get
            Set(value As String)
                m_title = value
            End Set
        End Property
        Private m_title As String
        <DataMember> _
        Public Property expires() As Int64
            Get
                Return m_expires
            End Get
            Set(value As Int64)
                m_expires = value
            End Set
        End Property
        Private m_expires As Int64
        <DataMember> _
        Public Property uri() As String
            Get
                Return m_uri
            End Get
            Set(value As String)
                m_uri = value
            End Set
        End Property
        Private m_uri As String
        <DataMember> _
        Public Property description() As String
            Get
                Return m_description
            End Get
            Set(value As String)
                m_description = value
            End Set
        End Property
        Private m_description As String
    End Class

    <DataContract> _
    Public Class Flags
        <DataMember> _
        Public Property sources() As List(Of String)
            Get
                Return m_sources
            End Get
            Set(value As List(Of String))
                m_sources = value
            End Set
        End Property
        Private m_sources As List(Of String)
        <DataMember> _
        Public Property isd_stations() As List(Of String)
            Get
                Return m_isd_stations
            End Get
            Set(value As List(Of String))
                m_isd_stations = value
            End Set
        End Property
        Private m_isd_stations As List(Of String)
        <DataMember> _
        Public Property lamp_stations() As List(Of String)
            Get
                Return m_lamp_stations
            End Get
            Set(value As List(Of String))
                m_lamp_stations = value
            End Set
        End Property
        Private m_lamp_stations As List(Of String)
        <DataMember> _
        Public Property metar_stations() As List(Of String)
            Get
                Return m_metar_stations
            End Get
            Set(value As List(Of String))
                m_metar_stations = value
            End Set
        End Property
        Private m_metar_stations As List(Of String)
        <DataMember> _
        Public Property darksky_stations() As List(Of String)
            Get
                Return m_darksky_stations
            End Get
            Set(value As List(Of String))
                m_darksky_stations = value
            End Set
        End Property
        Private m_darksky_stations As List(Of String)
        <DataMember> _
        Public Property units() As String
            Get
                Return m_units
            End Get
            Set(value As String)
                m_units = value
            End Set
        End Property
        Private m_units As String
    End Class
End Namespace



Namespace forecastIOWeather
Public Class Currently
	Public Property time() As Integer
		Get
			Return m_time
		End Get
		Set
			m_time = Value
		End Set
	End Property
	Private m_time As Integer
	Public Property summary() As String
		Get
			Return m_summary
		End Get
		Set
			m_summary = Value
		End Set
	End Property
	Private m_summary As String
	Public Property icon() As String
		Get
			Return m_icon
		End Get
		Set
			m_icon = Value
		End Set
	End Property
	Private m_icon As String
	Public Property nearestStormDistance() As Integer
		Get
			Return m_nearestStormDistance
		End Get
		Set
			m_nearestStormDistance = Value
		End Set
	End Property
	Private m_nearestStormDistance As Integer
	Public Property nearestStormBearing() As Integer
		Get
			Return m_nearestStormBearing
		End Get
		Set
			m_nearestStormBearing = Value
		End Set
	End Property
	Private m_nearestStormBearing As Integer
	Public Property precipIntensity() As Integer
		Get
			Return m_precipIntensity
		End Get
		Set
			m_precipIntensity = Value
		End Set
	End Property
	Private m_precipIntensity As Integer
	Public Property precipProbability() As Integer
		Get
			Return m_precipProbability
		End Get
		Set
			m_precipProbability = Value
		End Set
	End Property
	Private m_precipProbability As Integer
	Public Property temperature() As Double
		Get
			Return m_temperature
		End Get
		Set
			m_temperature = Value
		End Set
	End Property
	Private m_temperature As Double
	Public Property apparentTemperature() As Double
		Get
			Return m_apparentTemperature
		End Get
		Set
			m_apparentTemperature = Value
		End Set
	End Property
	Private m_apparentTemperature As Double
	Public Property dewPoint() As Double
		Get
			Return m_dewPoint
		End Get
		Set
			m_dewPoint = Value
		End Set
	End Property
	Private m_dewPoint As Double
	Public Property humidity() As Double
		Get
			Return m_humidity
		End Get
		Set
			m_humidity = Value
		End Set
	End Property
	Private m_humidity As Double
	Public Property windSpeed() As Double
		Get
			Return m_windSpeed
		End Get
		Set
			m_windSpeed = Value
		End Set
	End Property
	Private m_windSpeed As Double
	Public Property windBearing() As Integer
		Get
			Return m_windBearing
		End Get
		Set
			m_windBearing = Value
		End Set
	End Property
	Private m_windBearing As Integer
	Public Property visibility() As Double
		Get
			Return m_visibility
		End Get
		Set
			m_visibility = Value
		End Set
	End Property
	Private m_visibility As Double
	Public Property cloudCover() As Double
		Get
			Return m_cloudCover
		End Get
		Set
			m_cloudCover = Value
		End Set
	End Property
	Private m_cloudCover As Double
	Public Property pressure() As Double
		Get
			Return m_pressure
		End Get
		Set
			m_pressure = Value
		End Set
	End Property
	Private m_pressure As Double
	Public Property ozone() As Double
		Get
			Return m_ozone
		End Get
		Set
			m_ozone = Value
		End Set
	End Property
	Private m_ozone As Double
End Class

Public Class Datum
	Public Property time() As Integer
		Get
			Return m_time
		End Get
		Set
			m_time = Value
		End Set
	End Property
	Private m_time As Integer
	Public Property precipIntensity() As Integer
		Get
			Return m_precipIntensity
		End Get
		Set
			m_precipIntensity = Value
		End Set
	End Property
	Private m_precipIntensity As Integer
	Public Property precipProbability() As Integer
		Get
			Return m_precipProbability
		End Get
		Set
			m_precipProbability = Value
		End Set
	End Property
	Private m_precipProbability As Integer
End Class

Public Class Minutely
	Public Property summary() As String
		Get
			Return m_summary
		End Get
		Set
			m_summary = Value
		End Set
	End Property
	Private m_summary As String
	Public Property icon() As String
		Get
			Return m_icon
		End Get
		Set
			m_icon = Value
		End Set
	End Property
	Private m_icon As String
	Public Property data() As List(Of Datum)
		Get
			Return m_data
		End Get
		Set
			m_data = Value
		End Set
	End Property
	Private m_data As List(Of Datum)
End Class

Public Class Datum2
	Public Property time() As Integer
		Get
			Return m_time
		End Get
		Set
			m_time = Value
		End Set
	End Property
	Private m_time As Integer
	Public Property summary() As String
		Get
			Return m_summary
		End Get
		Set
			m_summary = Value
		End Set
	End Property
	Private m_summary As String
	Public Property icon() As String
		Get
			Return m_icon
		End Get
		Set
			m_icon = Value
		End Set
	End Property
	Private m_icon As String
	Public Property precipIntensity() As Double
		Get
			Return m_precipIntensity
		End Get
		Set
			m_precipIntensity = Value
		End Set
	End Property
	Private m_precipIntensity As Double
	Public Property precipProbability() As Double
		Get
			Return m_precipProbability
		End Get
		Set
			m_precipProbability = Value
		End Set
	End Property
	Private m_precipProbability As Double
	Public Property temperature() As Double
		Get
			Return m_temperature
		End Get
		Set
			m_temperature = Value
		End Set
	End Property
	Private m_temperature As Double
	Public Property apparentTemperature() As Double
		Get
			Return m_apparentTemperature
		End Get
		Set
			m_apparentTemperature = Value
		End Set
	End Property
	Private m_apparentTemperature As Double
	Public Property dewPoint() As Double
		Get
			Return m_dewPoint
		End Get
		Set
			m_dewPoint = Value
		End Set
	End Property
	Private m_dewPoint As Double
	Public Property humidity() As Double
		Get
			Return m_humidity
		End Get
		Set
			m_humidity = Value
		End Set
	End Property
	Private m_humidity As Double
	Public Property windSpeed() As Double
		Get
			Return m_windSpeed
		End Get
		Set
			m_windSpeed = Value
		End Set
	End Property
	Private m_windSpeed As Double
	Public Property windBearing() As Integer
		Get
			Return m_windBearing
		End Get
		Set
			m_windBearing = Value
		End Set
	End Property
	Private m_windBearing As Integer
	Public Property visibility() As Double
		Get
			Return m_visibility
		End Get
		Set
			m_visibility = Value
		End Set
	End Property
	Private m_visibility As Double
	Public Property cloudCover() As Double
		Get
			Return m_cloudCover
		End Get
		Set
			m_cloudCover = Value
		End Set
	End Property
	Private m_cloudCover As Double
	Public Property pressure() As Double
		Get
			Return m_pressure
		End Get
		Set
			m_pressure = Value
		End Set
	End Property
	Private m_pressure As Double
	Public Property ozone() As Double
		Get
			Return m_ozone
		End Get
		Set
			m_ozone = Value
		End Set
	End Property
	Private m_ozone As Double
	Public Property precipType() As String
		Get
			Return m_precipType
		End Get
		Set
			m_precipType = Value
		End Set
	End Property
	Private m_precipType As String
End Class

Public Class Hourly
	Public Property summary() As String
		Get
			Return m_summary
		End Get
		Set
			m_summary = Value
		End Set
	End Property
	Private m_summary As String
	Public Property icon() As String
		Get
			Return m_icon
		End Get
		Set
			m_icon = Value
		End Set
	End Property
	Private m_icon As String
	Public Property data() As List(Of Datum2)
		Get
			Return m_data
		End Get
		Set
			m_data = Value
		End Set
	End Property
	Private m_data As List(Of Datum2)
End Class
Public Class Datum3
	Public Property time() As Integer
		Get
			Return m_time
		End Get
		Set
			m_time = Value
		End Set
	End Property
	Private m_time As Integer
	Public Property summary() As String
		Get
			Return m_summary
		End Get
		Set
			m_summary = Value
		End Set
	End Property
	Private m_summary As String
	Public Property icon() As String
		Get
			Return m_icon
		End Get
		Set
			m_icon = Value
		End Set
	End Property
	Private m_icon As String
	Public Property sunriseTime() As Integer
		Get
			Return m_sunriseTime
		End Get
		Set
			m_sunriseTime = Value
		End Set
	End Property
	Private m_sunriseTime As Integer
	Public Property sunsetTime() As Integer
		Get
			Return m_sunsetTime
		End Get
		Set
			m_sunsetTime = Value
		End Set
	End Property
	Private m_sunsetTime As Integer
	Public Property moonPhase() As Double
		Get
			Return m_moonPhase
		End Get
		Set
			m_moonPhase = Value
		End Set
	End Property
	Private m_moonPhase As Double
	Public Property precipIntensity() As Double
		Get
			Return m_precipIntensity
		End Get
		Set
			m_precipIntensity = Value
		End Set
	End Property
	Private m_precipIntensity As Double
	Public Property precipIntensityMax() As Double
		Get
			Return m_precipIntensityMax
		End Get
		Set
			m_precipIntensityMax = Value
		End Set
	End Property
	Private m_precipIntensityMax As Double
	Public Property precipProbability() As Double
		Get
			Return m_precipProbability
		End Get
		Set
			m_precipProbability = Value
		End Set
	End Property
	Private m_precipProbability As Double
	Public Property temperatureMin() As Double
		Get
			Return m_temperatureMin
		End Get
		Set
			m_temperatureMin = Value
		End Set
	End Property
	Private m_temperatureMin As Double
	Public Property temperatureMinTime() As Integer
		Get
			Return m_temperatureMinTime
		End Get
		Set
			m_temperatureMinTime = Value
		End Set
	End Property
	Private m_temperatureMinTime As Integer
	Public Property temperatureMax() As Double
		Get
			Return m_temperatureMax
		End Get
		Set
			m_temperatureMax = Value
		End Set
	End Property
	Private m_temperatureMax As Double
	Public Property temperatureMaxTime() As Integer
		Get
			Return m_temperatureMaxTime
		End Get
		Set
			m_temperatureMaxTime = Value
		End Set
	End Property
	Private m_temperatureMaxTime As Integer
	Public Property apparentTemperatureMin() As Double
		Get
			Return m_apparentTemperatureMin
		End Get
		Set
			m_apparentTemperatureMin = Value
		End Set
	End Property
	Private m_apparentTemperatureMin As Double
	Public Property apparentTemperatureMinTime() As Integer
		Get
			Return m_apparentTemperatureMinTime
		End Get
		Set
			m_apparentTemperatureMinTime = Value
		End Set
	End Property
	Private m_apparentTemperatureMinTime As Integer
	Public Property apparentTemperatureMax() As Double
		Get
			Return m_apparentTemperatureMax
		End Get
		Set
			m_apparentTemperatureMax = Value
		End Set
	End Property
	Private m_apparentTemperatureMax As Double
	Public Property apparentTemperatureMaxTime() As Integer
		Get
			Return m_apparentTemperatureMaxTime
		End Get
		Set
			m_apparentTemperatureMaxTime = Value
		End Set
	End Property
	Private m_apparentTemperatureMaxTime As Integer
	Public Property dewPoint() As Double
		Get
			Return m_dewPoint
		End Get
		Set
			m_dewPoint = Value
		End Set
	End Property
	Private m_dewPoint As Double
	Public Property humidity() As Double
		Get
			Return m_humidity
		End Get
		Set
			m_humidity = Value
		End Set
	End Property
	Private m_humidity As Double
	Public Property windSpeed() As Double
		Get
			Return m_windSpeed
		End Get
		Set
			m_windSpeed = Value
		End Set
	End Property
	Private m_windSpeed As Double
	Public Property windBearing() As Integer
		Get
			Return m_windBearing
		End Get
		Set
			m_windBearing = Value
		End Set
	End Property
	Private m_windBearing As Integer
	Public Property visibility() As Double
		Get
			Return m_visibility
		End Get
		Set
			m_visibility = Value
		End Set
	End Property
	Private m_visibility As Double
	Public Property cloudCover() As Double
		Get
			Return m_cloudCover
		End Get
		Set
			m_cloudCover = Value
		End Set
	End Property
	Private m_cloudCover As Double
	Public Property pressure() As Double
		Get
			Return m_pressure
		End Get
		Set
			m_pressure = Value
		End Set
	End Property
	Private m_pressure As Double
	Public Property ozone() As Double
		Get
			Return m_ozone
		End Get
		Set
			m_ozone = Value
		End Set
	End Property
	Private m_ozone As Double
	Public Property precipIntensityMaxTime() As System.Nullable(Of Integer)
		Get
			Return m_precipIntensityMaxTime
		End Get
		Set
			m_precipIntensityMaxTime = Value
		End Set
	End Property
	Private m_precipIntensityMaxTime As System.Nullable(Of Integer)
	Public Property precipType() As String
		Get
			Return m_precipType
		End Get
		Set
			m_precipType = Value
		End Set
	End Property
	Private m_precipType As String
End Class

Public Class Daily
	Public Property summary() As String
		Get
			Return m_summary
		End Get
		Set
			m_summary = Value
		End Set
	End Property
	Private m_summary As String
	Public Property icon() As String
		Get
			Return m_icon
		End Get
		Set
			m_icon = Value
		End Set
	End Property
	Private m_icon As String
	Public Property data() As List(Of Datum3)
		Get
			Return m_data
		End Get
		Set
			m_data = Value
		End Set
	End Property
	Private m_data As List(Of Datum3)
End Class

Public Class Flags
	Public Property sources() As List(Of String)
		Get
			Return m_sources
		End Get
		Set
			m_sources = Value
		End Set
	End Property
	Private m_sources As List(Of String)
	Public Property isd_stations() As List(Of String)
		Get
			Return m_isd_stations
		End Get
		Set
			m_isd_stations = Value
		End Set
	End Property
	Private m_isd_stations As List(Of String)
	Public Property madis_stations() As List(Of String)
		Get
			Return m_madis_stations
		End Get
		Set
			m_madis_stations = Value
		End Set
	End Property
	Private m_madis_stations As List(Of String)
	Public Property lamp_stations() As List(Of String)
		Get
			Return m_lamp_stations
		End Get
		Set
			m_lamp_stations = Value
		End Set
	End Property
	Private m_lamp_stations As List(Of String)
	Public Property darksky_stations() As List(Of String)
		Get
			Return m_darksky_stations
		End Get
		Set
			m_darksky_stations = Value
		End Set
	End Property
	Private m_darksky_stations As List(Of String)
	Public Property units() As String
		Get
			Return m_units
		End Get
		Set
			m_units = Value
		End Set
	End Property
	Private m_units As String
End Class

Public Class RootObject
	Public Property latitude() As Double
		Get
			Return m_latitude
		End Get
		Set
			m_latitude = Value
		End Set
	End Property
	Private m_latitude As Double
	Public Property longitude() As Double
		Get
			Return m_longitude
		End Get
		Set
			m_longitude = Value
		End Set
	End Property
	Private m_longitude As Double
	Public Property timezone() As String
		Get
			Return m_timezone
		End Get
		Set
			m_timezone = Value
		End Set
	End Property
	Private m_timezone As String
	Public Property offset() As Integer
		Get
			Return m_offset
		End Get
		Set
			m_offset = Value
		End Set
	End Property
	Private m_offset As Integer
	Public Property currently() As Currently
		Get
			Return m_currently
		End Get
		Set
			m_currently = Value
		End Set
	End Property
	Private m_currently As Currently
	Public Property minutely() As Minutely
		Get
			Return m_minutely
		End Get
		Set
			m_minutely = Value
		End Set
	End Property
	Private m_minutely As Minutely
	Public Property hourly() As Hourly
		Get
			Return m_hourly
		End Get
		Set
			m_hourly = Value
		End Set
	End Property
	Private m_hourly As Hourly
	Public Property daily() As Daily
		Get
			Return m_daily
		End Get
		Set
			m_daily = Value
		End Set
	End Property
	Private m_daily As Daily
	Public Property flags() As Flags
		Get
			Return m_flags
		End Get
		Set
			m_flags = Value
		End Set
	End Property
	Private m_flags As Flags
End Class

End Namespace