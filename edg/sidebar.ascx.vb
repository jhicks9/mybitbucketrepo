Imports System.Xml.Linq
Imports System.Linq
Imports AjaxControlToolkit

Partial Class sidebar
    Inherits System.Web.UI.UserControl
    Private _navmenu As Boolean = False
    Private _imgurl As String = ""
    Private _login As Boolean = False
    Dim psm As New processSiteMap

    Public Property navMenu() As Boolean
        Get
            Return _navmenu
        End Get
        Set(ByVal value As Boolean)
            _navmenu = value
        End Set
    End Property

    Public Property ImageUrl() As String
        Get
            Return _imgurl
        End Get
        Set(ByVal value As String)
            _imgurl = value
        End Set
    End Property

    Public Property login() As Boolean
        Get
            Return _login
        End Get
        Set(value As Boolean)
            _login = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'example how to inject javascript
        'Dim colorbox As HtmlGenericControl = New HtmlGenericControl
        'colorbox.Attributes.Add("type", "text/javascript")
        'colorbox.TagName = "script"
        'colorbox.Attributes.Add("src", ResolveUrl("~/js/jquery.superfish.js"))
        'Page.Header.Controls.Add(colorbox)

        'example how to inject stylesheet
        'Dim css As HtmlLink = New HtmlLink
        'css.Href = ResolveUrl("~/slimbox2.css")
        'css.Attributes("rel") = "stylesheet"
        'css.Attributes("type") = "text/css"
        'css.Attributes("media") = "all"
        'Page.Header.Controls.Add(css)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If _imgurl <> "" Then
                imgSideImage.ImageUrl = _imgurl
                imgSideImage.Visible = True
            End If
            If _navmenu Then
                lblMenu.Visible = True
                'lblMenu.Text = BuildSingleNode(psm.getCurrentNodeKey("resourcekey"))
                lblMenu.Text = buildMenu()  'otherwise build normal site-wide menu
            End If
            If _login Then
                pnlLogin.Visible = True
                'Have the ENTER key work as a login click
                txtUserName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLogin.UniqueID + "','')")
                txtPassword.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + lbLogin.UniqueID + "','')")
                txtUserName.Focus()
            Else
                pnlLogin.Visible = False
            End If
        Catch
        End Try
    End Sub

    Private Function BuildSingleNode(ByVal node As String) As String
        Dim classSelected As String = ""
        Try
            Dim output As String = String.Empty
            Dim xelement2 As XElement = XElement.Load(Server.MapPath("~/web.sitemap"))
            Dim urlDescList1 = xelement2.Descendants().Where(Function(sel) CStr(sel.Attribute("resourceKey")) = node).SelectMany(Function(sel) sel.Elements()).Select(Function(nd) New With {Key .title = nd.Attribute("title").Value, Key .url = nd.Attribute("url").Value, Key .resourceKey = nd.Attribute("resourceKey").Value})
            output = "<ul id='sideMenu'>"
            For Each s In urlDescList1
                If s.resourceKey = node Then
                    If psm.getCurrentNodeKey("title") = s.title Or psm.getParentNodeKey("title") = s.title Then
                        classSelected = " class='selected'"
                    Else
                        classSelected = ""
                    End If
                    If Len(Request.QueryString("c")) > 0 And Len(Request.QueryString("b")) > 0 Then
                        'pass encrypted query string information if present (assistance/commission section use only)
                        output &= String.Format("<li{2}><a href=""{0}"">{1}</a></li>", ResolveUrl(s.url) & "?c=" & Server.UrlEncode(Request.QueryString("c")) & "&b=" & Server.UrlEncode(Request.QueryString("b")) & "&n=" & Server.UrlEncode(Request.QueryString("n")), s.title, classSelected)
                    Else
                        output &= String.Format("<li{2}><a href=""{0}"">{1}</a></li>", ResolveUrl(s.url), s.title, classSelected)
                    End If
                End If
            Next s
            output &= "</ul>"
            Return output
        Catch
            Return Nothing
        End Try
    End Function

    Private Function buildMenu() As String
        Try
            ' Get all nodes from only the first two levels of the current site map (first level is empty)
            Dim imgurl As String = Page.ResolveUrl("~/images/icon")
            Dim Menu As String = String.Empty
            Dim rootCollection As SiteMapNodeCollection
            Dim rootNode As SiteMapNode
            'Dim childCollection As SiteMapNodeCollection
            'Dim childNode As SiteMapNode
            Dim classSelected As String = ""

            rootCollection = New SiteMapNodeCollection(SiteMap.RootNode.ChildNodes) 'start with second level
            Menu = "<ul id='sideMenu'>"
            For Each rootNode In rootCollection
                If SiteMap.CurrentNode.ResourceKey = rootNode.ResourceKey Then
                    classSelected = " class='selected'"
                Else
                    classSelected = ""
                End If
                If Not (LCase(rootNode.ResourceKey) = "news" Or LCase(rootNode.ResourceKey) = "outages") Then
                    If Not String.IsNullOrEmpty(rootNode("imgUrl")) Then
                        Menu &= String.Format("<li{2}><a href=""{0}""><img class=""icon"" alt="""" src=""{3}/{4}""/> {1}", rootNode.Url, rootNode.Title, classSelected, imgurl, rootNode("imgurl")) & "</a></li>"
                    Else
                        Menu &= String.Format("<li{2}><a href=""{0}"">{1}", rootNode.Url, rootNode.Title, classSelected) & "</a></li>"
                    End If


                    'If rootNode.HasChildNodes Then
                    '    childCollection = rootNode.ChildNodes
                    '    Menu &= "<ul>"
                    '    For Each childNode In childCollection
                    '        If childNode("imgUrl") IsNot Nothing Then
                    '            Menu &= String.Format("<li><a href=""{0}""><img class=""icon"" alt="""" src=""{3}/{2}""/> {1}</a></li>", _
                    '                                  childNode.Url, childNode.Title, childNode("imgUrl"), imgurl)
                    '        Else
                    '            Menu &= String.Format("<li><a href=""{0}"">{1}</a></li>", _
                    '                                  childNode.Url, childNode.Title)
                    '        End If
                    '    Next
                    '    Menu &= "</ul>"
                    'End If
                    'Menu &= "</li>"
                End If
            Next
            Menu &= "</ul>"
            Return Menu
        Catch
            Return Nothing
        End Try
    End Function


    Protected Sub lbLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            loadScreen(0)  'enable modal processing screen
            Dim authUser As Boolean = True

            Dim userInfo As MembershipUser = Membership.GetUser(txtUserName.Text)  'See if this user exists
            If userInfo Is Nothing Then  'The user entered an invalid username
                lblStatus.Text = "Your login attempt was not successful."
                authUser = False
            End If

            If userInfo.IsLockedOut Then  'See if the user is locked out
                If Now > userInfo.LastLockoutDate.AddMinutes(30) Then  'if > 30 minutes then unlock account
                    lblStatus.Text = ""
                    userInfo.UnlockUser()
                    authUser = True
                Else
                    lblStatus.Text = "Your account has been locked for exceeding the maximum number of login attempts. Please try again in 30 minutes."
                    authUser = False  'account is locked -- do not authenticate
                End If
            End If

            If authUser Then  'account exists and is not locked -- proceed to authenticate user
                If (Membership.ValidateUser(txtUserName.Text, txtPassword.Text)) Then  'valid username/password
                    FormsAuthentication.SetAuthCookie(txtUserName.Text, True)
                    lblStatus.Text = ""
                    Response.Redirect("~/lvg/default.aspx")
                Else  ''The user entered an invalid password
                    lblStatus.Text = "Your login attempt was not successful."
                    loadScreen(0)  'disable modal processing screen
                End If
            Else
                loadScreen(0)  'account doesnt exist or is locked  -- do not authenticate
            End If
        Catch
        End Try
    End Sub

    'Public Sub EvalRoles(ByVal userName As String)
    '    If Roles.IsUserInRole(userName, "Customer") Or Roles.IsUserInRole(userName, "Admin") Then
    '        Response.Redirect("~/CustomerService/MyAccount/default.aspx")
    '    ElseIf Roles.IsUserInRole(userName, "Assistance") = True Then
    '        Response.Redirect("~/CustomerService/Assistance/default.aspx")
    '    ElseIf Roles.IsUserInRole(userName, "Commission") = True Then
    '        Response.Redirect("~/CustomerService/Commission/default.aspx")
    '    Else
    '        Response.Redirect("~/default.aspx")
    '    End If
    'End Sub

    Public Function loadScreen(ByVal toggle As Boolean) As Boolean
        Dim uc As Web.UI.UserControl = CType(Page.Master.FindControl("modal1"), Web.UI.UserControl)
        Dim mp As ModalPopupExtender = CType(uc.FindControl("mpeProgress"), ModalPopupExtender)
        If Not uc Is Nothing Then
            If Not mp Is Nothing Then
                If toggle Then
                    mp.Show()  'enable modal loading screen
                Else
                    mp.Hide()  'disable modal loading screen
                End If
            End If
        End If
        Return True
    End Function
End Class
