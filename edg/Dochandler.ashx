<%@ WebHandler Language="VB" Class="FileHandler" %>

Imports System
Imports System.IO
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient

Public Class FileHandler
    Implements IHttpHandler

    Dim conString As String = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim con As SqlConnection = New SqlConnection(conString)
        Dim fname As String = ""
        Dim id As String
        id = context.Request.QueryString("id")
        If IsNumeric(id) Then
            Dim cmd1 As SqlCommand = New SqlCommand("Select filename FROM documents,doctype WHERE documents.id=" & id & " AND documents.doctypeid = doctype.id AND doctype.restricted = 0", con)
            con.Open()
            Dim reader As SqlDataReader
            reader = cmd1.ExecuteReader
            While reader.Read
                fname = reader.GetValue(0).ToString
            End While
            reader.Close()
        
            Dim ext As String = Path.GetExtension(fname)
            Dim type As String = ""

            If Not IsDBNull(ext) Then
                ext = LCase(ext)
            End If

            Select Case ext
                Case ".htm", ".html"
                    type = "text/HTML"
                Case ".txt"
                    type = "text/plain"
                Case ".doc", ".rtf"
                    type = "Application/msword"
                Case ".ppt"
                    type = "application/vnd.ms-powerpoint"
                Case ".csv", ".xls"
                    type = "Application/x-msexcel"
                Case ".pdf"
                    type = "Application/pdf"
                Case Else
                    type = "text/plain"
            End Select

            If type <> "" Then
                context.Response.ContentType = type
            End If
            
            If fname <> "" Then
                Dim cmd As SqlCommand = New SqlCommand("Select FileBytes FROM documents WHERE id=" & id & " and Filename=@FileName", con)
                cmd.Parameters.AddWithValue("@FileName", fname)
                Using con
                    Dim file() As Byte = CType(cmd.ExecuteScalar(), Byte())
                    context.Response.BinaryWrite(file)
                End Using
                con.Close()
            Else
                context.Response.Write("Document Not Found")
            End If
        Else
            context.Response.Write("Document Not Found")
        End If
    End Sub
    
    Public ReadOnly Property IsReUsable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
End Class