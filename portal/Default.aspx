﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ContentPlaceHolderID="BreadcrumbPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1 class="text-center">Maintenance Portal</h1>
    </div>

    <div class="section">
	    <div class="container">
	        <div class="row">
	        	<div class="col-md-4 col-sm-6">
	        		<div style="background: #FFF;margin: 20px 10px;text-align: center;padding: 30px 20px;-webkit-border-radius: 5px;-webkit-background-clip: padding-box;-moz-border-radius: 5px;-moz-background-clip: padding;border-radius: 5px;background-clip: padding-box;-webkit-box-shadow: 0 0 3px #999;-moz-box-shadow: 0 0 3px #999;box-shadow: 0 0 3px #999;color: 0 0 3px #999;">
		        		<i class="fa fa-briefcase" style="font-size:52px"></i>
		        		<h3>Portal Applications</h3>
		        		<p>&nbsp;</p>
		        		<a href="~/maintenance/portalapps/default.aspx" runat="server" class="btn btn-primary">Continue</a>
		        	</div>
	        	</div>
	        	<div class="col-md-4 col-sm-6">
	        		<div style="background: #FFF;margin: 20px 10px;text-align: center;padding: 30px 20px;-webkit-border-radius: 5px;-webkit-background-clip: padding-box;-moz-border-radius: 5px;-moz-background-clip: padding;border-radius: 5px;background-clip: padding-box;-webkit-box-shadow: 0 0 3px #999;-moz-box-shadow: 0 0 3px #999;box-shadow: 0 0 3px #999;color: 0 0 3px #999;">
		        		<i class="fa fa-cloud" style="font-size:52px"></i>
		        		<h3>Weather</h3>
		        		<p>Local forecast with current radar</p>
		        		<a href="~/weather/weather.aspx" runat="server" class="btn btn-primary">Continue</a>
		        	</div>
	        	</div>
	        	<div class="col-md-4 col-sm-6">
	        		<div style="background: #FFF;margin: 20px 10px;text-align: center;padding: 30px 20px;-webkit-border-radius: 5px;-webkit-background-clip: padding-box;-moz-border-radius: 5px;-moz-background-clip: padding;border-radius: 5px;background-clip: padding-box;-webkit-box-shadow: 0 0 3px #999;-moz-box-shadow: 0 0 3px #999;box-shadow: 0 0 3px #999;color: 0 0 3px #999;">
		        		<i class="fa fa-user" style="font-size:52px"></i>
		        		<h3>eDirectory</h3>
		        		<p>Find employee information</p>
		        		<a href="~/eDirectory.aspx" runat="server" class="btn btn-primary">Continue</a>
		        	</div>
	        	</div>
	        </div>
	    </div>
	</div>
</asp:Content>
