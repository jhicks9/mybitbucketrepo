﻿<%@ Page Language="VB"  MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="eDirectory.aspx.vb" Inherits="eDirectory" title="Peoplesoft Search" %>

<asp:Content ContentPlaceHolderID="BreadcrumbPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:Panel ID="pnlSearch" runat="server" Visible="True" DefaultButton="lbSearch">
        <div class="row">
            <div class="col-lg-12 ">
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td colspan="2" class="field-label col-xs-12 info text-center"><b>eDirectory</b></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label for="txtLName">Last Name</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtLName" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label for="txtLName">First Name</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label for="txtLName">Department</label></td>
                        <td class="col-md-9">
                            <asp:DropDownList ID="ddlDepartment" runat="server" DataSourceID="sqlDepartments" DataTextField="deptname" CssClass="form-control" />
                            <asp:SqlDataSource ID="sqlDepartments" Runat="server" ConnectionString="<%$ ConnectionStrings:PeopleSoft%>" ProviderName="System.Data.OracleClient"
                                SelectCommand="Select DISTINCT EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME From sysadm.EDE_PORTAL_PS_EMPLOYEES_VW order by EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME asc" >
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label for="txtLName">Location</label></td>
                        <td class="col-md-9">
                            <asp:DropDownList ID="ddlLocation" runat="server" DataSourceID="sqlLocations" DataTextField="descr" CssClass="form-control" />
                            <asp:SqlDataSource ID="sqlLocations" Runat="server" ConnectionString="<%$ ConnectionStrings:PeopleSoft%>" ProviderName="System.Data.OracleClient"
                                SelectCommand="select distinct PS_LOCATION_TBL.descr FROM sysadm.PS_LOCATION_TBL order by PS_LOCATION_TBL.descr asc" >
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Search' />
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlSearchResults" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="updateGridview" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvDetails" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" SortExpression="name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbRow" runat="server" CausesValidation="False" CommandName="Select" CommandArgument='<%#Eval("EMPLID") %>' Text='<%#Eval("name") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname" />
                                    <asp:BoundField DataField="jobtitle" HeaderText="Job Title" SortExpression="jobtitle" />
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <asp:LinkButton ID="lbAnotherSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Another Search' />
            </div>
        </div>
    </asp:Panel>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="updateDetails" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-9 col-sm-9">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label for="lblDetailName" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="lblDetailName" runat="server" Text="" CssClass="form-control no-border" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lblDetailTitle" class="col-sm-3 control-label">Job Title</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="lblDetailTitle" runat="server" Text="" CssClass="form-control no-border" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lblDetailDepartment" class="col-sm-3 control-label">Department</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="lblDetailDepartment" runat="server" Text="" CssClass="form-control no-border" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lblDetailLocation" class="col-sm-3 control-label">Location</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="lblDetailLocation" runat="server" Text="" CssClass="form-control no-border" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lblDetailPhone" class="col-sm-3 control-label">Phone</label>
                                            <div class="col-sm-9">
                                                <asp:Label ID="lblDetailPhone" runat="server" Text="" CssClass="form-control no-border" />
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <asp:Image ID="imgPhoto" runat="server" CssClass="img-responsive" Width="112" Height="135" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-right">
                                        <asp:LinkButton ID="lbClose" runat="server" class="btn btn-primary" CausesValidation="False" data-dismiss="modal" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>