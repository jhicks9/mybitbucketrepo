﻿Imports Microsoft.VisualBasic
Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement

Public Class portalAD
    
        Public Function getPropertyValue (dirEntry As DirectoryEntry, propertyValue as String) As String
        Dim result As String = ""
        Try
            If dirEntry isnot Nothing then
                If dirEntry.Properties(propertyValue) IsNot Nothing and dirEntry.Properties(propertyValue).Count > 0 Then 'check for a valid value
                    result = dirEntry.Properties(propertyValue)(0).ToString()
                End If
            End If
        Catch ex As Exception
        End Try
        Return result
    End Function

    Public Function GetManager(ctx As PrincipalContext, user As UserPrincipal) As UserPrincipal
	    Dim result As UserPrincipal = Nothing
	    If user IsNot Nothing Then
		    Dim dirEntryForUser As DirectoryEntry = TryCast(user.GetUnderlyingObject(), DirectoryEntry) 'get the DirectoryEntry behind the UserPrincipal object
		    If dirEntryForUser IsNot Nothing Then
			    If dirEntryForUser.Properties("manager") IsNot Nothing  and dirEntryForUser.Properties("manager").Count > 0 Then 'check for a manager name
				    Dim managerDN As String = dirEntryForUser.Properties("manager")(0).ToString()
				    result = UserPrincipal.FindByIdentity(ctx, managerDN) ' find the manager UserPrincipal via the managerDN 
			    End If
		    End If
	    End If
	    Return result
    End Function

End Class
