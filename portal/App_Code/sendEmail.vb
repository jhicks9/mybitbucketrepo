﻿Imports Microsoft.VisualBasic

Public Class sendEmail
        Public Function sendEMail(ByVal toList As ArrayList, ByVal strFrom As String, ByVal strSubj As String, ByVal strBody As String, Optional ByVal html As Boolean = false, Optional ByVal fileattachments As ArrayList = Nothing) As Boolean
        Dim status As Boolean = False
        Dim strTo As String = ""
        Dim fAddress As String = ""
        Try
            Dim config = Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
            Dim settings As Net.Configuration.MailSettingsSectionGroup = config.GetSectionGroup("system.net/mailSettings")
            Dim MailObj As New System.Net.Mail.SmtpClient
            Dim message As New System.Net.Mail.MailMessage()

            If html = True then
                message.IsBodyHtml = True 'set HTML as true
            End If
            If String.IsNullOrEmpty(settings.Smtp.Network.UserName) Or String.IsNullOrEmpty(settings.Smtp.Network.Password) Then  'just relay email using supplied name as from address
                If Not strFrom.Contains("@empiredistrict.com") Then 'if only username was passed append @empire
                    strFrom &= "@empiredistrict.com"
                End If
                fAddress = strFrom  'to use "friendly name" use this format: "Your Name <yourname@yourisp.com>"
            Else  'use authentication via userid/password from config file and send email as authenticated user
                MailObj.Credentials = New System.Net.NetworkCredential(settings.Smtp.Network.UserName, settings.Smtp.Network.Password)
                fAddress = settings.Smtp.Network.UserName & "@empiredistrict.com"
            End If
            Dim fromAddress As New System.Net.Mail.MailAddress(fAddress) 'add multiple send to addresses
            For Each strTo In toList
                message.To.Add(strTo)
            Next
            message.From = fromAddress 'add from address
            message.Subject = strSubj
            message.Body = strBody

            If Not fileattachments Is Nothing Then 'file attachments were passed
                For Each attach As HttpPostedFile In fileattachments
                    attach.InputStream.Position = 0
                    message.Attachments.Add(New System.Net.Mail.Attachment(attach.InputStream, System.IO.Path.GetFileName(attach.FileName)))
                    'used to write the file attachment -- for testing
                    'Dim path = System.IO.Path.Combine(System.IO.Path.GetTempPath().ToString, attach.FileName)
		            'Dim data = New Byte(attach.ContentLength - 1) {}
		            'attach.InputStream.Read(data, 0, attach.ContentLength)
		            'Using sw = New system.IO.FileStream(path, System.IO.FileMode.Create)
                        'sw.Write(data, 0, data.Length)
		            'End Using
                Next
            End If
            MailObj.Send(message)
            status = True
        Catch ex As System.Exception
            Throw ex  'send the exception back to ui
        End Try
        Return status
    End Function
End Class
