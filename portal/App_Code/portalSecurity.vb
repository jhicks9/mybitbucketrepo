﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
'Imports System.Web.UI.UserControl

Public Class portalSecurity

    Public Function isAuthorized(ByVal pagename As String) As Boolean
        Dim authorized As Boolean = False
        Try 
            Dim securitydefined As Boolean = False
            'Dim query As String = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_pages.name = '" & pagename & "') and (ps_security.usertype <> 'admin' or ps_security.usertype IS NULL)"
            Dim query As String = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_pages.name = @pagename or left(ps_pages.name, @namelength) = @pagename) and (ps_security.usertype <> 'admin' or ps_security.usertype IS NULL)"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd0 As New SqlCommand(query, conn)
            cmd0.Parameters.AddWithValue("@pagename", pagename)
            cmd0.Parameters.AddWithValue("@namelength", pagename.Length)
            cmd0.Parameters.AddWithValue("username", getUsername())
            Dim dr0 As SqlDataReader
            conn.Open()
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    securitydefined = dr0(0)  'does page/module have any security
                End While
            End If
            dr0.Close()
            If securitydefined Then  'security is defined so lookup security for user
                cmd0.CommandText = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_pages.name = @pagename or left(ps_pages.name, @namelength) = @pagename) and (ps_security.username = @username)"
                dr0 = cmd0.ExecuteReader
                If dr0.HasRows Then
                    While dr0.Read
                        authorized = dr0(0)
                    End While
                End If
                dr0.Close()
            Else
                authorized = True    'no security defined so user is authorized
            End If
            conn.Close()
        Catch
            authorized = False
        End Try
        Return authorized
    End Function
    Public Function isAdmin(ByVal pagename As String) As Boolean
        Dim admin As Boolean = False
        Try
            Dim query As String = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_security.usertype = 'admin') and (ps_security.username = @username) and (ps_pages.name = @pagename or left(ps_pages.name, @namelength) = @pagename)"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd0 As New SqlCommand(query, conn)
            cmd0.Parameters.AddWithValue("@pagename", pagename)
            cmd0.Parameters.AddWithValue("@namelength", pagename.Length)
            cmd0.Parameters.AddWithValue("username", getUsername())
            Dim dr0 As SqlDataReader
            conn.Open()
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    admin = dr0(0)
                End While
            End If
            dr0.Close()
            conn.Close()
        Catch
            admin = false
        End Try
        Return admin
    End Function
    Public Function canInsert(ByVal pagename As String) As Boolean
        Try
            Dim admin As Boolean = False
            Dim query As String = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_security.insertrecord = 'True') and (ps_security.username = @username) and (ps_pages.name = @pagename or left(ps_pages.name, @namelength) = @pagename)"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd0 As New SqlCommand(query, conn)
            cmd0.Parameters.AddWithValue("@pagename", pagename)
            cmd0.Parameters.AddWithValue("@namelength", pagename.Length)
            cmd0.Parameters.AddWithValue("username", getUsername())
            Dim dr0 As SqlDataReader
            conn.Open()
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    admin = dr0(0)
                End While
            End If
            dr0.Close()
            conn.Close()
            Return admin
        Catch
            Return False
        End Try
    End Function
    Public Function canUpdate(ByVal pagename As String) As Boolean
        Try
            Dim admin As Boolean = False
            Dim query As String = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_security.updaterecord = 'True') and (ps_security.username = @username) and (ps_pages.name = @pagename or left(ps_pages.name, @namelength) = @pagename)"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd0 As New SqlCommand(query, conn)
            cmd0.Parameters.AddWithValue("@pagename", pagename)
            cmd0.Parameters.AddWithValue("@namelength", pagename.Length)
            cmd0.Parameters.AddWithValue("username", getUsername())
            Dim dr0 As SqlDataReader
            conn.Open()
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    admin = dr0(0)
                End While
            End If
            dr0.Close()
            conn.Close()
            Return admin
        Catch
            Return False
        End Try
    End Function
    Public Function canDelete(ByVal pagename As String) As Boolean
        Try
            Dim admin As Boolean = False
            Dim query As String = "select count(*) as t1 from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id where (ps_security.deleterecord = 'True') and (ps_security.username = @username) and (ps_pages.name = @pagename or left(ps_pages.name, @namelength) = @pagename)"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd0 As New SqlCommand(query, conn)
            cmd0.Parameters.AddWithValue("@pagename", pagename)
            cmd0.Parameters.AddWithValue("@namelength", pagename.Length)
            cmd0.Parameters.AddWithValue("username", getUsername())
            Dim dr0 As SqlDataReader
            conn.Open()
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    admin = dr0(0)
                End While
            End If
            dr0.Close()
            conn.Close()
            Return admin
        Catch
            Return False
        End Try
    End Function
    Public Function isSiteAdmin() As Boolean
        Dim admin As Boolean = False
        Try 
            Dim query As String = "select count(*) as t1 from ps_security where (ps_security.usertype = 'site') and (ps_security.pageid in (SELECT ps_pages.id FROM ps_pages WHERE ps_pages.name = 'SiteAdmin.ascx')) and (ps_security.username = @username)"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim cmd0 As New SqlCommand(query, conn)
            cmd0.Parameters.AddWithValue("username", getUsername())
            Dim dr0 As SqlDataReader
            conn.Open()
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    admin = dr0(0)
                End While
            End If
            dr0.Close()
            conn.Close()
        Catch
            admin = false
        End Try
         Return admin
    End Function

    Public Function getPageName() As String
        Dim pagename As String = ""
        Dim context As HttpContext = HttpContext.Current
        Try
            Dim apath As Array
            apath = Split("/" & context.Request.ServerVariables("PATH_INFO"), "/")
            pagename = apath(UBound(apath))
        Catch
            pagename =  ""
        End Try
        Return pagename
    End Function

    Public Function getUsername() As String
        Dim getuser As String = ""
        Try
            Dim context As HttpContext = HttpContext.Current 
            getuser = context.Request.ServerVariables("LOGON_USER")
            getuser = Right(getuser, Len(getuser) - InStr(getuser, "\"))
        Catch
            getuser =  ""
        End Try
        Return getuser.ToLower
    End Function

End Class
