﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Optimization
Imports System.Web.UI

Public Module BundleConfig
    ' For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
    Public Sub RegisterBundles(bundles As BundleCollection)

        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-2.1.4.min.js",
            "~/Scripts/moment-with-locales.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/bootstrap").Include(
            "~/Scripts/bootstrap.js",
            "~/Scripts/bootstrap-datetimepicker.min.js"))

        bundles.Add(New StyleBundle("~/Content/css").Include(
            "~/Content/bootstrap.css",
            "~/Content/bootstrap-theme.css",
            "~/Content/font-awesome.min.css",
            "~/Content/bootstrap-datetimepicker.min.css",
            "~/Content/site.css"))
    End Sub
End Module
