﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="modal.aspx.vb" Inherits="modal" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=100"><%--force IE to not use compatibility mode--%>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><%: Page.Title %></title>
    <%: System.Web.Optimization.Scripts.Render("~/Content/css") %>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/Content/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 90%; margin-right: 5%; margin-left: 5%; text-align: center">
            <asp:ScriptManager runat="server" ID="ScriptManager1" />
            <h3 style="text-align: center;">ASP.NET GridVIew: CRUD using Twitter Bootstrap Modal Popup</h3>
            <p style="text-align: center;">Demo by Priya Darshini - Tutorial @ <a href="http://www.programming-free.com/2013/09/gridview-crud-bootstrap-modal-popup.html">Programmingfree</a></p>
            <!-- Placing GridView in UpdatePanel-->
            <asp:UpdatePanel ID="upCrudGrid" runat="server">
                <ContentTemplate>
<%--                    <asp:GridView ID="GridView1" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false" AllowPaging="true"
                        DataKeyNames="Code" CssClass="table table-hover table-striped">
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="Code" HeaderText="Code" />
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="Continent" HeaderText="Continent" />
                            <asp:BoundField DataField="Region" HeaderText="Region" />
                            <asp:BoundField DataField="Population" HeaderText="Population" />
                            <asp:BoundField DataField="IndepYear" HeaderText="Independence Year" />
                        </Columns>
                    </asp:GridView>--%>
                    <asp:Button ID="btnAdd" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAdd_Click" />

                    <asp:GridView ID="gvPageAdminUsers" runat="server" DataSourceID="sqlDSUsers" DataKeyNames="id,pageid" AllowPaging="true" OnRowCommand="gvPageAdminUsers_RowCommand" Width="100%" 
                        CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                        <Columns>

                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>

                            <asp:TemplateField ShowHeader="False">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbAdd" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                        CommandName="AddRecord" CausesValidation="False" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Add User</asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="false" CommandName="Select" CssClass="btn btn-default"
                                        CommandArgument='<%# Eval("pageid") %>'><span class="fa fa-edit"></span> Edit User</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>   
                            <asp:BoundField DataField="username" HeaderText="User name" SortExpression="username" />
                            <asp:BoundField DataField="usertype" HeaderText="Type" SortExpression="usertype" />
                            <asp:CheckBoxField DataField="insertrecord" HeaderText="insert" SortExpression="insertrecord" />
                            <asp:CheckBoxField DataField="updaterecord" HeaderText="update" SortExpression="updaterecord" />
                            <asp:CheckBoxField DataField="deleterecord" HeaderText="delete" SortExpression="deleterecord" />
                        </Columns>
                        <EmptyDataTemplate>
                            <asp:LinkButton ID="lbAddEmpty" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                CommandName="AddRecord" CausesValidation="False" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Add new user</asp:LinkButton>
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlDSUsers" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                        SelectCommand="select * from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id AND ps_pages.name = @parentusercontrol">
                        <SelectParameters>
                            <asp:Parameter Name="parentusercontrol" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel1">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


            <!-- Detail Modal Starts here-->
            <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Detailed View</h3>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblTest" runat="server" Text="Test" />
                            <%--<asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                                <Fields>
                                    <asp:BoundField DataField="Code" HeaderText="Code" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="Continent" HeaderText="Continent" />
                                    <asp:BoundField DataField="Population" HeaderText="Population" />
                                    <asp:BoundField DataField="IndepYear" HeaderText="Independence Year" />
                                </Fields>
                            </asp:DetailsView>--%>
                        </ContentTemplate>
                        <%--<Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                        </Triggers>--%>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="editModalLabel">Edit Record</h3>
                </div>
                <asp:UpdatePanel ID="upEdit" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                                <tr>
                                    <td>Country Code : 
                            <asp:Label ID="lblCountryCode" runat="server" Text="Country Code"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Population : 
                            <asp:TextBox ID="txtPopulation" runat="server"></asp:TextBox>
                                        <asp:Label runat="server" Text="Type Integer Value!" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Country Name:
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Continent:
                            <asp:TextBox ID="txtContinent1" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            something here
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnSave_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    </Triggers>--%>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <%--<div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="addModalLabel">Add New Record</h3>
                </div>
                <asp:UpdatePanel ID="upAdd" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td>Country Code : 
                                <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Country Name : 
                                <asp:TextBox ID="txtCountryName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Continent Name:
                                <asp:TextBox ID="txtContinent" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Region:
                                <asp:TextBox ID="txtRegion" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Population:
                                    <asp:TextBox ID="txtTotalPopulation" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label1" runat="server" Text="Type Integer Value!" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Year of Independence
                                        <asp:TextBox ID="txtIndYear" runat="server"></asp:TextBox>
                                        <asp:Label ID="Label2" runat="server" Text="Type Integer Value!" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddRecord" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>--%>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <%--<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="delModalLabel">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="upDel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>--%>
            <!--Delete Record Modal Ends here -->
        </div>
    </form>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/bootstrap") %>
</body>
</html>
