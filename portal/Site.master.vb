﻿Imports System.Web
Imports System.Web.UI

Public Partial Class SiteMaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs)
        'Optimization.BundleTable.EnableOptimizations = True
        Page.Header.DataBind()  'renders the relative path for javascript and stylesheets in HTML header
    End Sub

    Protected Sub SiteMapPath_ItemCreated(ByVal sender As Object, ByVal e As SiteMapNodeItemEventArgs)
        If e.Item.ItemType = SiteMapNodeItemType.Root OrElse (e.Item.ItemType = SiteMapNodeItemType.PathSeparator AndAlso e.Item.ItemIndex = 1) Then
            e.Item.Visible = False
        End If
    End Sub

End Class
