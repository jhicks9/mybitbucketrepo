﻿<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="ConvertPLY.aspx.vb" Inherits="Maintenance_web_ConvertPLY" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:HiddenField runat="server" ID="HFCurrTabIndex" />
    <asp:HiddenField runat="server" ID="hdnFileName" />

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container_1">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Convert Poly</a></li>
                <li><a href="#tab_b" data-toggle="pill">ESZ Description</a></li>
                <li><a href="#tab_c" data-toggle="pill">ESZ Differences</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <table class="table table-condensed table-bordered">
                        <tr><td colspan="3"><div class="well-sm bg-info text-center"><strong>Geographic/UTM Coordinate Converter</strong></div></td></tr>
                        <tr>
                            <td class="field-label active">Step Number</td>
                            <td class="field-label active">Selection Criteria</td>
                            <td class="field-label active">Action</td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Step 1:</td>
                            <td><asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" /></td>
                            <td><asp:Button ID="cmdLoad" runat="server" Text="Load" class="btn btn-default" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Step 2:</td>
                            <td style="text-align:center"><asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" /></td>
                            <td><asp:Button id="cmdProcess" type="button" value="Process" runat="server" Text="Process" class="btn btn-default" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Step 3:</td>
                            <td><asp:TextBox ID="txtCoord" runat="server" Height="50px" TextMode="MultiLine" Text="" CssClass="form-control" /></td>
                            <td>
                                <asp:Button ID="cmdUpdate" runat="server" Text="Update" UseSubmitBehavior="false" class="btn btn-default" OnClientClick="if(!confirm('This will replace all current coordinates in the database. Continue?'))return false;" />
                                <asp:Label ID="lblStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </td>
                        </tr>
                    </table>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <asp:UpdatePanel ID="updateESZDesc" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvESZDesc" runat="server" DataSourceID="sqlDSESZDesc" DataKeyNames="id" AllowSorting="true" AllowPaging="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="lbAddRecord" runat="server" CommandName="AddRecord" CausesValidation="false"
                                                            CssClass="btn btn-primary" ToolTip="Add new entry"><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:linkButton ID="btnSelect" runat="server" CommandName="Select" CausesValidation="false" CommandArgument='<%#Eval("esz") %>'
                                                            CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" Visible="false" />
                                                <asp:BoundField DataField="esz" HeaderText="esz" SortExpression="esz" Visible="true" />
                                                <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" Visible="true" />
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <EmptyDataTemplate>
                                                <asp:LinkButton ID="lbAddRecord" CommandName="AddRecord" runat="server" CausesValidation="false"><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="sqlDSESZDesc" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                                            SelectCommand="SELECT esz_description.id,esz_description.esz,esz_description.description from dbo.esz_description order by esz_description.esz">
                                        </asp:SqlDataSource>
                                    </ContentTemplate> 
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div><!-- tab-B -->
                <div class="tab-pane" id="tab_c">
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:UpdatePanel ID="updateDifferences" runat="server">
                                <ContentTemplate>
                                    <div class="table-responsive">
                                        <asp:GridView ID="gvNoESZ" runat="server" DataSourceID="sqlNoESZ" AllowPaging="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:templatefield>
                                                    <ItemTemplate>No valid esz</ItemTemplate>
                                                </asp:templatefield>
                                                <asp:BoundField DataField="id" HeaderText="id" SortExpression="" Visible="true" />
                                                <asp:BoundField DataField="esz" HeaderText="esz" SortExpression="" Visible="true" />
                                                <asp:BoundField DataField="description" HeaderText="description" SortExpression="" Visible="true" />
                                            </Columns>
                                            <EmptyDataTemplate>No zone/description differences</EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="sqlNoESZ" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                                            SelectCommand="select esz_description.id,esz_description.esz,esz_description.description from dbo.esz_description where esz not in (select distinct esz_boundary.esz from esz_boundary)">
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="table-responsive">
                                        <asp:GridView ID="gvNoDesc" runat="server" DataSourceID="sqlNoDesc" AllowPaging="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:templatefield>
                                                    <ItemTemplate>No valid description</ItemTemplate>
                                                </asp:templatefield>
                                                <asp:BoundField DataField="esz" HeaderText="esz" SortExpression="" Visible="true" />
                                            </Columns>
                                            <EmptyDataTemplate>No zone/description differences</EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="sqlNoDesc" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                                            SelectCommand="select distinct esz_boundary.esz from dbo.esz_boundary where esz_boundary.esz not in(select distinct esz_description.esz from dbo.esz_description)">
                                        </asp:SqlDataSource>
                                    </div>
                                </ContentTemplate> 
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div><!-- tab-C -->
            </div><!-- tab-content -->
        </div><!-- tab container -->

        <div class="modal fade" id="ESZDescDetail" tabindex="-1" role="dialog" aria-labelledby="ESZDescDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateESZDescDetail" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="dvESZDesc" runat="server" DataKeyNames="id" DataSourceID="sqlDSESZDescDetail" CssClass="table table-striped table-bordered table-hover"
                                     AutoGenerateRows="false">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="ESZ" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <ItemTemplate>
                                            <asp:Label ID="lblESZ" runat="server" Text='<%# Bind("esz") %>' CssClass="col-md-9 form-control no-border"></asp:Label>                                    
                                            </ItemTemplate>                                                                
                                            <edititemtemplate>
                                            <asp:TextBox ID="txtESZ" runat="server" Text='<%# Bind("esz") %>' CssClass="col-md-9 form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvESZ" runat="server" ControlToValidate="txtESZ" ErrorMessage="required" SetFocusOnError="True" />
                                            </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Description" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <ItemTemplate>
                                            <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control no-border"></asp:Label>
                                            </ItemTemplate>
                                            <edititemtemplate>
                                            <asp:TextBox ID="txtDesc" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDesc" ErrorMessage="required" SetFocusOnError="True" />
                                            </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="lbDescItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary"/>
                                                <asp:LinkButton ID="lbDescInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="lbDescEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary"/>&nbsp;
                                                <asp:LinkButton ID="lbDescEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>&nbsp;
                                                <asp:LinkButton ID="lbDescEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblESZDescDetailStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                <asp:SqlDataSource ID="sqlDSESZDescDetail" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                                    SelectCommand="SELECT esz_description.id,esz_description.esz,esz_description.description from dbo.esz_description WHERE (esz_description.id = @id)"
                                    InsertCommand="INSERT INTO dbo.esz_description (esz,description) VALUES (@esz,@description)"
                                    UpdateCommand="UPDATE dbo.esz_description SET esz=@esz,description=@description WHERE (esz_description.id = @id)"
                                    DeleteCommand="DELETE FROM dbo.esz_description WHERE id = @id">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="gvESZDesc" Type="Int32" Name="id" PropertyName="SelectedValue"/>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel><!-- end pnlAuthorized -->
</asp:Content>