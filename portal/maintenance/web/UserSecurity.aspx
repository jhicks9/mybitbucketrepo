<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="UserSecurity.aspx.vb" Inherits="maintenance_web_UserSecurity" title="BOD Document Security" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-12 ">
                <ul class="nav nav-pills nav-justified">
                    <li id="liUserSecurity" runat="server"><asp:LinkButton ID="lbUserSecurityPanel" runat="server" Text="User Security" /></li>
                    <li id="liGroupSecurity" runat="server"><asp:LinkButton ID="lbGroupSecurityPanel" runat="server" Text="Group Security" /></li>
                    <li id="liDocumentType" runat="server"><asp:LinkButton ID="lbDocumentTypePanel" runat="server" Text="Document Type" /></li>
                    <li id="liGroups" runat="server"><asp:LinkButton ID="lbGroupsPanel" runat="server" Text="Groups" /></li>
                </ul>                                
            </div>
        </div>
        <div class="row"><div class="col-lg-12"><hr /></div></div>

        <asp:Panel ID="pnlUserSecurityPanel" runat="server" Visible="true" >
            <asp:UpdatePanel ID="pnlUsers" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-3 ">
                            <div class="table-responsive">
                                <asp:GridView ID="gvUsers" runat="server" DataKeyNames="name" AllowPaging="false"  CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="True">
                                            <HeaderTemplate >
                                                <asp:Label ID="lblgvUsersName" runat="server" Text="Username"></asp:Label>  
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbgvUsersName" runat="server" Text='<%# Bind("name") %>' CommandArgument='<%# Bind("name") %>' CommandName="Select"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>No users</EmptyDataTemplate>
                                    <SelectedRowStyle CssClass="info"  Font-Bold="True" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="col-md-9 ">
                            <div class="table-responsive">
                                <asp:GridView ID="gvSummary" runat="server" AllowPaging="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="group" HeaderText="Group" />
                                        <asp:BoundField DataField="doctype" HeaderText="Type" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>

        <asp:Panel ID="pnlGroupSecurityPanel" runat="server" Visible="false" >
            <div class="row">
                <div class="col-md-3 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gvGroups" runat="server" DataSourceID="sqlDSGroups" DataKeyNames="id" AllowPaging="true" PageSize="15" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>                    
                                <asp:TemplateField ShowHeader="True">
                                    <HeaderTemplate >
                                        <asp:Label ID="lblgvGroupsDescHeader" runat="server" Text="Group"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbgvGroupsDesc" runat="server" Text='<%# Bind("description") %>' CommandArgument='<%# Bind("id") %>' CommandName="Select"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <SelectedRowStyle CssClass="info"  Font-Bold="True" />
                            <EmptyDataTemplate>No groups</EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div class="col-md-9 ">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="table-responsive">
                                <asp:UpdatePanel ID="updateGroupsGridview" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvGroupUsers" runat="server" DataSourceID="sqlDSUsers" DataKeyNames="id" AllowPaging="False" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false" Visible="false">
                                            <Columns>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="lbAddUser" runat="server" CommandName="AddRecord" CausesValidation="false" ToolTip="Add user"><span class="fa fa-user-plus"></span></asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbSelectUser" runat="server" CausesValidation="false" CommandName="Select" ToolTip="Edit user"
                                                            CommandArgument="EditRecord"><span class="fa fa-user"></span></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>   
                                                <asp:TemplateField ShowHeader="True">
                                                    <HeaderTemplate >
                                                        <asp:Label ID="lblgvGroupsUsersNameHeader" runat="server" Text="Username"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbgvGroupsUsersName" runat="server" ToolTip="Edit documents" Text='<%# Bind("userid") %>' CommandArgument='<%# Bind("id") %>' CommandName="Select"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="info"  Font-Bold="True" />
                                            <EmptyDataTemplate>
                                                <asp:LinkButton ID="lbAddEmpty" runat="server" CommandName="AddRecord" CausesValidation="False" ToolTip="Add user" Text="Add"><span class="fa fa-user-plus"></span></asp:LinkButton>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Panel ID="pDocumentTypes" runat="server" Visible="False">
                                        <p class="well-sm bg-info text-center">Document Types</p>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="cblDetails" runat="server" CssClass="form-field">
                                        </asp:CheckBoxList>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" Visible="false" CssClass="btn btn-default" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="userDetail" tabindex="-1" role="dialog" aria-labelledby="userDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="pnlGroupUsersDetails" runat="server">
                                <contenttemplate>
                                    <asp:DetailsView ID="UsersDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSUsers2" CssClass="table table-striped table-bordered table-hover"
                                        AutoGenerateRows="False">
                                        <Fields>
                                            <asp:templatefield headertext="Username" HeaderStyle-CssClass="field-label col-xs-3 active">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("userid") %>' CssClass="col-md-9 form-control"></asp:Label>                                    
                                                </ItemTemplate>
                                                <edititemtemplate>
                                                    <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("userid") %>' CssClass="col-md-9 form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                                                </edititemtemplate>                                                                   
                                            </asp:templatefield>                                                                
                                            <asp:TemplateField headertext="" HeaderStyle-CssClass="field-label col-xs-3 active">
                                                <InsertItemTemplate>
                                                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                    <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                                    <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                                </InsertItemTemplate>                                
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                    <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                                    <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                                    <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                        <FooterTemplate></FooterTemplate>
                                    </asp:DetailsView>
                                    <asp:Label ID="lblGroupUsersStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </contenttemplate>
                                <triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvGroupUsers" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                </triggers>                
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel><%--pnlGroupSecurityPanel--%>

        <asp:Panel ID="pnlDocumentTypePanel" runat="server" Visible="false" >
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                    <asp:UpdatePanel ID="updateDocTypeGridview" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvDocTypes" runat="server" DataSourceID="sqlDSDocTypes" DataKeyNames="id" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false" AllowPaging="true">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbAddDocument" runat="server" CausesValidation="false" CommandName="AddRecord" CssClass="btn btn-primary"
                                                ToolTip="Add document type"><span class="fa fa-plus">&nbsp;Add document</span></asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbSelectDocument" runat="server" CausesValidation="false" CommandName="Select" CssClass="btn btn-default"
                                                ToolTip="Edit document type"><span class="fa fa-file-text-o">&nbsp;Edit document</span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="description" HeaderText="Document Type" SortExpression="description" />
                                    <asp:BoundField DataField="multiple" HeaderText="Allow Multiple" SortExpression="multiple" />
                                    <asp:BoundField DataField="defaultexp" HeaderText="Expiration Days" SortExpression="defaultexp" />
                                    <asp:BoundField DataField="restricted" HeaderText="Restricted Access" SortExpression="restricted" />
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                                <EmptyDataTemplate>
                                    <asp:LinkButton ID="lbAddEmpty" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord" Text="Add" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add document</span></asp:LinkButton>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="docTypeDetail" tabindex="-1" role="dialog" aria-labelledby="docTypeDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updateDocTypesDetails" runat="server">
                                <contenttemplate>
                                    <asp:DetailsView ID="DocTypesDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSDocTypes2" CssClass="table table-striped table-bordered table-hover"
                                        AutoGenerateRows="False">
                                        <Fields>
                                            <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                            <asp:templatefield headertext="Document Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                              <ItemTemplate>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control"></asp:Label>
                                              </ItemTemplate>                                                                
                                              <edititemtemplate>
                                                <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                                              </edititemtemplate>                                  
                                            </asp:templatefield>
                                            <asp:templatefield headertext="Allow Multiple" HeaderStyle-CssClass="field-label col-xs-3 active">
                                              <ItemTemplate>
                                                <asp:Label ID="lblMultiple" runat="server" Text='<%# Bind("multiple") %>'></asp:Label>
                                              </ItemTemplate>                                  
                                              <EditItemTemplate>
                                                <asp:DropDownList ID="ddlMultiple" runat="server" SelectedValue='<%# Bind("multiple") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"multiple") ) %>' CssClass="col-md-9 form-control">
                                                  <asp:ListItem>False</asp:ListItem>
                                                  <asp:ListItem>True</asp:ListItem>
                                                </asp:DropDownList>                                                                
                                              </EditItemTemplate>
                                            </asp:templatefield>
                                            <asp:templatefield headertext="Default Expiration (days)" HeaderStyle-CssClass="field-label col-xs-3 active">
                                              <ItemTemplate>
                                                <asp:Label ID="lblDefaultExp" runat="server" Text='<%# Bind("defaultexp") %>' CssClass="col-md-9 form-control"></asp:Label>
                                              </ItemTemplate>                                                                
                                              <edititemtemplate>
                                                <asp:TextBox ID="txtDefaultExp" runat="server" Text='<%# Bind("defaultexp") %>' CssClass="col-md-9 form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDefaultExp" runat="server" ControlToValidate="txtDefaultExp" ErrorMessage="required.  Enter a zero for no default expiration." SetFocusOnError="True" Display="Dynamic" />
                                              </edititemtemplate>                                  
                                            </asp:templatefield>
                                            <asp:templatefield headertext="Restricted Access" HeaderStyle-CssClass="field-label col-xs-3 active">
                                              <ItemTemplate>
                                                <asp:Label ID="lblRestricted" runat="server" Text='<%# Bind("restricted") %>' CssClass="col-md-9 form-control"></asp:Label>
                                              </ItemTemplate>                                  
                                              <EditItemTemplate>
                                                <asp:DropDownList ID="ddlRestricted" runat="server" SelectedValue='<%# Bind("restricted") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"restricted") ) %>' CssClass="col-md-9 form-control">
                                                  <asp:ListItem>False</asp:ListItem>
                                                  <asp:ListItem>True</asp:ListItem>
                                                </asp:DropDownList>                                                                
                                              </EditItemTemplate>
                                            </asp:templatefield>
                                            <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                                <InsertItemTemplate>
                                                  <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                  <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                                  <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                                </InsertItemTemplate>                                
                                                <EditItemTemplate>
                                                  <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                  <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true"  CssClass="btn btn-primary"/>
                                                  <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                                  <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false"  CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:Label ID="lblDocTypesStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" BackColor="#c0c0c0"></asp:Label>
                                </contenttemplate>
                                <triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvDocTypes" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                </triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel><%--pnlDocumentTypePanel--%>

        <asp:Panel ID="pnlGroupsPanel" runat="server" Visible="false" >
            <div class="row">
                <div class="col-md-6 ">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateGroupGridview" runat="server">
                            <ContentTemplate>            
                                <asp:GridView ID="gvGroup" runat="server" DataSourceID="sqlDSGroups" DataKeyNames="id" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false" AllowPaging="true">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddUser" runat="server" CommandName="AddRecord" CausesValidation="false" ToolTip="Add new group"><span class="fa fa-user-plus"></span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectUser" runat="server" CausesValidation="false" CommandName="Select" ToolTip="Edit group"
                                                    CommandArgument="EditRecord"><span class="fa fa-user"></span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="True">
                                            <HeaderTemplate >
                                                <asp:Label ID="lblgvGroupDescription" runat="server" Text="Group"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbgvGroupDescription" runat="server" ToolTip="Edit documents" Text='<%# Bind("description") %>' CommandArgument='<%# Bind("id") %>' CommandName="Select"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmpty" runat="server" CausesValidation="False" ToolTip="Add group" CommandName="AddRecord" Text="Add"><span class="fa fa-user-plus"></span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle CssClass="info"  Font-Bold="True" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:CheckBoxList ID="cblGroupDetails" runat="server" DataSourceID="sqlDSDocTypes" DataTextField="description"
                                DataValueField="id" CssClass="form-field">
                            </asp:CheckBoxList>
                            <asp:Button ID="btnUpdateGroup" runat="server" Text="Update" Visible="false" OnClick="btnUpdateGroup_Click" CssClass="btn btn-default" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="modal fade" id="GroupDetail" tabindex="-1" role="dialog" aria-labelledby="GroupDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updatePanelGroupDetails" runat="server">
                                <contenttemplate>
                                    <asp:DetailsView ID="GroupDetails" runat="server" DataKeyNames="id" SkinID="DetailsView" DataSourceID="sqlDSGroups2" CssClass="table table-striped table-bordered table-hover"
                                         AutoGenerateRows="false">
                                        <Fields>
                                            <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                            <asp:templatefield headertext="Group" HeaderStyle-CssClass="field-label col-xs-3 active">
                                              <ItemTemplate>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control"></asp:Label>
                                              </ItemTemplate>                                                                
                                              <edititemtemplate>
                                                <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" />
                                              </edititemtemplate>                                  
                                            </asp:templatefield>
                                            <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                                <InsertItemTemplate>
                                                  <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                  <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                                  <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                                </InsertItemTemplate>                                
                                                <EditItemTemplate>
                                                  <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                  <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary"/>
                                                  <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                                  <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:Label ID="lblGroupStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" BackColor="#c0c0c0"></asp:Label>
                                </contenttemplate>
                                <triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvGroup" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                </triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel><%--pnlGroupsPanel--%>
    </asp:Panel><%--end pnlAuthorized--%>
       
    <asp:SqlDataSource ID="sqlDSGroups" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select id,description from groups order by description">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSGroups2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,description FROM groups where (groups.id = @id)"
        InsertCommand="insert into groups (description) VALUES (@description)"
        UpdateCommand="update groups SET description=@description where groups.id = @id"
        DeleteCommand="delete from groups where id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvGroup" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="description" Type="String" />
        </InsertParameters>                
    </asp:SqlDataSource>    
    <asp:SqlDataSource ID="sqlDSUsers" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select id,userid,groupid from security where groupid = @groupid order by userid asc">
        <SelectParameters>
            <asp:Parameter Name="groupid" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>         
    <asp:SqlDataSource ID="sqlDSUsers2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select security.id, security.userid, groups.description from security inner join groups ON security.groupid = groups.id where (security.id = @id)"
        InsertCommand="insert into security (userid,groupid,doctypeid) VALUES (@userid,@groupid,@doctypeid)"
        UpdateCommand="update security SET userid=@userid where id = @id"
        DeleteCommand="delete from security where id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvGroupUsers" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="gvGroups" Type="String" Name="groupid" PropertyName="SelectedDataKey.Value"/>
            <asp:Parameter Name="doctypeid" Type="String" DefaultValue="0"/>
            <asp:Parameter Name="userid" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="userid" Type="String" />
        </UpdateParameters>         
        <DeleteParameters>
            <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue" ControlID="gvGroupUsers"></asp:ControlParameter>
        </DeleteParameters>                       
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSDocTypes" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,description,multiple,defaultexp,restricted FROM doctype order by description">
    </asp:SqlDataSource>                        
    <asp:SqlDataSource ID="sqlDSDocTypes2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,description,multiple,defaultexp,restricted FROM doctype where (doctype.id = @id)"
        InsertCommand="INSERT INTO doctype (description,multiple,defaultexp,restricted) VALUES (@description,@multiple,@defaultexp,@restricted)"
        UpdateCommand="UPDATE doctype SET description=@description,multiple=@multiple,defaultexp=@defaultexp,restricted=@restricted WHERE (doctype.id = @id)"
        DeleteCommand="DELETE FROM doctype WHERE doctype.id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvDocTypes" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="multiple" Type="Boolean" />
            <asp:Parameter Name="defaultexp" Type="String" />
            <asp:Parameter Name="restricted" Type="Boolean" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="multiple" Type="Boolean" />
            <asp:Parameter Name="defaultexp" Type="String" />
            <asp:Parameter Name="restricted" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>