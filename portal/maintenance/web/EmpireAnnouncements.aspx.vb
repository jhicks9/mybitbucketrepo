﻿Imports System.Data.SqlClient

Partial Class Maintenance_web_EmpireAnnouncements
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    '============================== Announcement Maintenance ===========================================================================
    Protected Sub gvAnnouncements_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAnnouncements.RowCommand
        Select Case e.CommandName
            Case "Select"
                AnnouncementDetails.ChangeMode(DetailsViewMode.Edit)
                AnnouncementDetails.HeaderText = "Edit"
                pnlAnnouncementDetail.Visible = true
                pnlAnnouncement.Visible = false
            Case "AddRecord"
                gvAnnouncements.SelectedIndex = -1
                AnnouncementDetails.HeaderText = "Add"
                AnnouncementDetails.ChangeMode(DetailsViewMode.Insert)
                pnlAnnouncementDetail.Visible = true
                pnlAnnouncement.Visible = false
            Case Else
                AnnouncementDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub AnnouncementDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles AnnouncementDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
                pnlAnnouncementDetail.Visible = false
                pnlAnnouncement.Visible = true
            Case "Update"
            Case "Insert"
        End Select
        gvAnnouncements.SelectedIndex = -1
        lblAnnouncementStatus.Text = ""        
    End Sub

    Protected Sub AnnouncementDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles AnnouncementDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblAnnouncementStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvAnnouncements.DataBind()
            pnlAnnouncementDetail.Visible = false
            pnlAnnouncement.Visible = true
        End If
    End Sub

    Protected Sub AnnouncementDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles AnnouncementDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(AnnouncementDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvAnnouncements.DataBind()
            pnlAnnouncementDetail.Visible = false
            pnlAnnouncement.Visible = true
        End If
    End Sub

    Protected Sub AnnouncementDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles AnnouncementDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(AnnouncementDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvAnnouncements.DataBind()
            pnlAnnouncementDetail.Visible = false
            pnlAnnouncement.Visible = true
        End If
    End Sub

    '============================== End Announcement Maintenance =======================================================================
    '============================== Outage Maintenance =================================================================================
    Protected Sub gvOutage_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOutage.RowCommand
        Select Case e.CommandName
            Case "Select"
                OutageDetails.HeaderText = "Edit"
                OutageDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#outageDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvOutage.SelectedIndex = -1
                OutageDetails.HeaderText = "Add"
                OutageDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#outageDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
            Case Else
                OutageDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub OutageDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles OutageDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvOutage.SelectedIndex = -1
    End Sub

    Protected Sub OutageDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles OutageDetails.ItemDeleted
        gvOutage.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#outageDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub OutageDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles OutageDetails.ItemInserted
        gvOutage.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#outageDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub OutageDetails_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles OutageDetails.ItemInserting
        sqlOutage2.InsertParameters("date").DefaultValue = Date.Now
    End Sub

    Protected Sub OutageDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles OutageDetails.ItemUpdated
        gvOutage.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#outageDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
    End Sub
    '============================== End Outage Maintenance =======================================================================
End Class
