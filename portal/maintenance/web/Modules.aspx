﻿<%@ Page Title="" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="Modules.aspx.vb" Inherits="maintenance_web_modules" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container_1">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Center Modules</a></li>
                <li><a href="#tab_b" data-toggle="pill">Static Modules</a></li>
                <li><a href="#tab_c" data-toggle="pill">Admin Accounts</a></li>
            </ul>
            <div class="row"><div class="col-lg-12"><hr /></div></div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:Panel ID="pnlCenter" runat="server" Visible="true">
                            <asp:GridView ID="gvCenterModules" runat="server" DataSourceID="sqlDSCenterModules" DataKeyNames="module_id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbAdd" runat="server" ToolTip="Add new module" CausesValidation="false" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add</span></asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" ToolTip="Edit module" CommandName="Edit" CommandArgument="EditRecord" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="true" Text="Update" ValidationGroup="updateCenter"></asp:LinkButton>
                                            <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel"></asp:LinkButton>
                                            <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" CausesValidation="false" Text="Delete" OnClientClick='<%# "return confirm(""Are you sure you want to delete the page/module?"")" %>'></asp:LinkButton>
                                        </EditItemTemplate>                        
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Module_ID" HeaderText="Module_ID" Visible="False" ReadOnly="True" />
                                    <asp:BoundField DataField="Module_Image" HeaderText="Module_Image" Visible="False" />
                                    <asp:CheckBoxField DataField="module_visible" HeaderText="Visible" SortExpression="Module_Visible" />
                                    <asp:BoundField DataField="Module_Order" HeaderText="Order" SortExpression="Module_Order" />
                                    <asp:TemplateField HeaderText="Name" SortExpression="Module_Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCMName" runat="server" Text='<%# Bind("Module_Name") %>'></asp:TextBox>
                                            <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvCMName" runat="server" ControlToValidate="txtCMName"
                                            Display="Dynamic" ErrorMessage="Name is required." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("Module_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Link" SortExpression="Module_Link">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCMLink" runat="server" Text='<%# Bind("Module_Link") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCMLink" runat="server" Text='<%# Bind("Module_Link") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:templatefield headertext="Image" >
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlViewImage" runat="server" Target="_blank" NavigateUrl='<%# Eval("module_id", "~/controls/FHandler.ashx?dtype=modules&id={0}&view=1") %>'>View Image</asp:HyperLink>
                                        </ItemTemplate>
                                        <edititemtemplate>
                                        <asp:FileUpload ID="UpdateUploadedFile" runat="server" />
                                        <asp:Label ID="lblFileValidation" runat="server" Text="" Font-Bold="true" ForeColor="red" />
                                        <%--<asp:Label ID="lblModuleFilename" runat="server" Text='<%# Bind("Module_FileName") %>' Visible="true" />--%>
                                        </edititemtemplate>
                                    </asp:templatefield>
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                                <EmptyDataTemplate>
                                    <asp:LinkButton ID="lbAddEmpty" runat="server" ToolTip="Add new module" CausesValidation="false" CommandArgument="AddRecord" CommandName="AddRecord" Text="Add"></asp:LinkButton>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </asp:Panel><!-- pnlCenter -->
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateStaticModules" runat="server">
                            <ContentTemplate>
                               <asp:GridView ID="gvStaticModules" runat="server" DataSourceID="sqlDSStaticModules" DataKeyNames="module_id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAdd" runat="server" ToolTip="Add new module" CausesValidation="false" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" ToolTip="Edit module" CommandName="Edit" CommandArgument="EditRecord" Text="Edit" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="true" Text="Update" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" CausesValidation="false" Text="Delete" OnClientClick='<%# "return confirm(""Are you sure you want to delete the page/module?"")" %>' />
                                            </EditItemTemplate>                        
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Module_ID" HeaderText="Module_ID" Visible="False" ReadOnly="True" />
                                        <asp:CheckBoxField DataField="module_visible" HeaderText="Visible" SortExpression="Module_Visible" />
                                        <asp:TemplateField HeaderText="Name" SortExpression="Module_Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtSMName" runat="server" Text='<%# Bind("Module_Name") %>' />
                                                <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red" Visible="false" />
                                                <asp:RequiredFieldValidator ID="rfvSMName" runat="server" ControlToValidate="txtSMName"
                                                Display="Dynamic" ErrorMessage="Name is required." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("Module_Name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Link" SortExpression="Module_Link">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSMLink" runat="server" Text='<%# Bind("Module_Link") %>'/>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtSMLink" runat="server" Text='<%# Bind("Module_Link") %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmpty" runat="server" ToolTip="Add new module" CausesValidation="false" CommandArgument="AddRecord" CommandName="AddRecord" Text="Add"></asp:LinkButton>
                                    </EmptyDataTemplate>           
                               </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->
                <div class="tab-pane" id="tab_c">
                    <asp:UpdatePanel ID="updateAccounts" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:LinkButton ID="lbAccountAdd" runat="server" Text="Add Account" CssClass="btn btn-primary" />
                                    <asp:LinkButton ID="lbAccountDelete" runat="server" Text="Delete Account" CssClass="btn btn-warning" />
                                    <asp:LinkButton ID="lbAccountEdit" runat="server" Text="Edit Account" CssClass="btn btn-default" />
                                </div>
                            </div>
                            <div class="table-responsive">
                                <asp:GridView ID="gvAccounts" runat="server" DataSourceID="sqlDSAccounts" DataKeyNames="UserId" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Admin Accounts" />
                                        <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
                                        <asp:BoundField DataField="CreateDate" SortExpression="" HeaderText="Created" />
                                        <asp:BoundField DataField="LastLoginDate" SortExpression="" HeaderText="Login" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>No accounts found</EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div><!-- tab-C -->
            </div><!-- tab-content -->
        </div><!-- tab container -->

        <div class="modal fade" id="imageDetail" tabindex="-1" role="dialog" aria-labelledby="imageDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateImage" runat="server">
                            <ContentTemplate>
                                <asp:Image ID="imgStaticModule" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div><!-- imageDetail -->

        <asp:Panel ID="pnlCenterDetail" runat="server" Visible="false">
                <div class="row"><div class="col-lg-12"><p class="well-sm bg-info text-center"><strong>Add Center Module</strong></p></div></div>
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="field-label col-xs-3 active">Name</td>
                        <td>
                            <asp:TextBox ID="txtCenterModuleName" runat="server" Text="" CssClass="col-md-9 form-control" />
                            <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red" />
                            <asp:RequiredFieldValidator ID="rfvCenterModuleName" runat="server" ControlToValidate="txtCenterModuleName"
                                Display="Dynamic" ErrorMessage="Name is required." SetFocusOnError="True" ValidationGroup="addCenter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active">Visible</td>
                        <td><asp:CheckBox ID="ckbCenterModuleVisible" runat="server" CssClass="col-md-9 form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active">Order</td>
                        <td><asp:TextBox ID="txtCenterModuleOrder" runat="server" Text="" CssClass="col-md-9 form-control" />
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active">Link</td>
                        <td><asp:TextBox ID="txtCenterModuleLink" runat="server" Text="" CssClass="col-md-9 form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active">Image</td>
                        <td>
                            <asp:FileUpload ID="fluCenterModuleImage" runat="server" CssClass="col-md-9 form-control" />
                            <%--<asp:RequiredFieldValidator ID="rfvCenterModuleImage" runat="server" ControlToValidate="fluCenterModuleImage"
                                ErrorMessage="required" Display="Dynamic" ValidationGroup="addCenter" />
                            <asp:RegularExpressionValidator ID="revCenterModuleImage" runat="server" ControlToValidate="fluCenterModuleImage"
                                ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$" 
                                ErrorMessage="Invalid file type" Display="Dynamic" ValidationGroup="addCenter"  />--%>
                            <asp:Label ID="lblFileValidation" runat="server" Text="" Font-Bold="true" ForeColor="Red" />
                        </td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"></td>
                        <td>
                            <asp:LinkButton ID="lbCenterModuleInsert" CommandName="Update" runat="server" CausesValidation="true" ValidationGroup="addCenter" Text="Update" CssClass="btn btn-primary" />
                            <asp:LinkButton ID="lbCenterModuleCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="btn btn-default" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div class="modal fade" id="staticDetail" tabindex="-1" role="dialog" aria-labelledby="staticDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updateStatic" runat="server">
                                <ContentTemplate>
                                    <div class="row"><div class="col-lg-12"><p class="well-sm bg-info text-center"><strong>Add Static Module</strong></p></div></div>
                                    <table class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <td class="field-label col-xs-3 active">Name</td>
                                            <td>
                                                <asp:TextBox ID="txtInsertStaticModuleName" runat="server" Text="" CssClass="col-md-9 form-control" />
                                                <asp:Label ID="lblInsertStaticValidation" runat="server" Text="" ForeColor="Red" />
                                                <asp:RequiredFieldValidator ID="rvfInsertStaticModuleName" runat="server" ControlToValidate="txtInsertStaticModuleName"
                                                    Display="Dynamic" ErrorMessage="Name is required." SetFocusOnError="True" ValidationGroup="insert" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active">Visible</td>
                                            <td><asp:CheckBox ID="ckbInsertStaticModuleVisible" runat="server" CssClass="col-md-9 form-control" /></td>
                                        </tr>
                                        <tr>
                                            <td  class="field-label col-xs-3 active">Link</td>
                                            <td><asp:TextBox ID="txtInsertStaticModuleLink" runat="server" Text="" CssClass="col-md-9 form-control" />
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active"></td>
                                            <td class="col-md-9">
                                                <asp:LinkButton ID="lbInsertStaticModuleInsert" CommandName="Insert" runat="server" CausesValidation="true" ValidationGroup="insert" Text="Add" CssClass="btn btn-primary" />
                                                <asp:LinkButton ID="lbInsertStaticModuleCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="btn btn-default" data-dismiss="modal" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div><!-- staticDetail -->

            <div class="modal fade" id="AddAccountDetail" tabindex="-1" role="dialog" aria-labelledby="AddAccountDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="pnlAddAccount" runat="server">
                                <contenttemplate>
                                    <div class="row">
                                        <div class="col-lg-12"><p class="well-sm bg-info text-center"><strong><asp:Label ID="lblType" runat="server" Text="" /> Account Creation</strong></p></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" MembershipProvider="empireMembershipProvider" LoginCreatedUser="False" DisplayCancelButton="true">
                                                <WizardSteps>
                                                    <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                                    </asp:CreateUserWizardStep>
                                                    <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                                                    </asp:CompleteWizardStep>
                                                </WizardSteps>
                                            </asp:CreateUserWizard>
                                            <asp:Label ID="lblAccountAddStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                        </div>
                                    </div>
                                </contenttemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div><!-- AddAccountDetail -->

            <div class="modal fade" id="EditAccountDetail" tabindex="-1" role="dialog" aria-labelledby="EditAccountDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updateEditAccount" runat="server">
                                <contenttemplate>
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <td class="field-label col-xs-3 active">UserName</td>
                                            <td>
                                                <asp:TextBox ID="txtEUserName" runat="server" Text="" CssClass="col-md-9 form-control" />
                                                <asp:RequiredFieldValidator ID="rfvEUserName" runat="server" ControlToValidate="txtEUserName"
                                                Display="Dynamic" ErrorMessage="required" SetFocusOnError="True" ValidationGroup="EditAccount" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active"></td>
                                            <td class="col-md-9">
                                            <div class="table-responsive">
                                                <asp:GridView ID="gvRoles" runat="server" AllowPaging="false" AllowSorting="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField  HeaderText="Role Name">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkRole" runat="server"/>
                                                                <asp:Label ID="lblRole" runat="server" Text="<%#Container.DataItem %>"/>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active"></td>
                                            <td class="col-md-9">
                                                <asp:LinkButton ID="lbEditLookup" runat="server" Text="Lookup Account" CausesValidation="true" ValidationGroup="EditAccount" CssClass="btn btn-primary" />
                                                <asp:LinkButton ID="lbEditCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                                            </td>
                                        </tr>
                                    </table>
                                </contenttemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div><!-- EditAccountDetail -->

            <div class="modal fade" id="DeleteAccountDetail" tabindex="-1" role="dialog" aria-labelledby="DeleteAccountDetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updateDeleteAccount" runat="server">
                                <contenttemplate>
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <td class="field-label col-xs-3 active">UserName</td>
                                            <td>
                                                <asp:TextBox ID="txtDUserName" runat="server" Text="" CssClass="col-md-9 form-control" />
                                                <asp:RequiredFieldValidator ID="rfvDUserName" runat="server" ControlToValidate="txtDUserName"
                                                    Display="Dynamic" ErrorMessage="required" SetFocusOnError="True"  ValidationGroup="DeleteAccount"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active"></td>
                                            <td><asp:Label ID="lblDUserID" runat="server" Text="" CssClass="col-md-9 form-control no-border"/></td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active"></td>
                                            <td>
                                                <asp:LinkButton ID="lbDeleteAccount" runat="server" Text="Delete User" CausesValidation="true" ValidationGroup="DeleteAccount" CssClass="btn btn-warning" />
                                                <asp:LinkButton ID="lbDeleteCancel" runat="server" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lblDeleteStatus" runat="server" Text="" ForeColor="Red" />
                                </contenttemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div><!-- DeleteAccountDetail -->
    </asp:Panel><!-- pnlAuthorized -->

    <asp:SqlDataSource ID="sqlDSCenterModules" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select module_id,module_name,module_filename,module_image,module_visible,module_link,module_order from modules where module_columnonpage = 'Middle' order by module_order,module_name"
        UpdateCommand="update modules SET module_name=@module_name,module_filename=@module_filename,module_visible=@module_visible,module_image=@imagedata,module_link=@module_link,module_order=@module_order where module_id = @module_id"
        InsertCommand=""
        DeleteCommand="delete from modules where module_id = @module_id">
        <UpdateParameters>
            <asp:Parameter Name="imagedata" DbType="Binary"  />
        </UpdateParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="sqlDSStaticModules" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select modules.module_id,modules.module_name,modules.module_visible,modules.module_link from modules where modules.module_columnonpage = 'Static' order by modules.module_name"
        UpdateCommand="update modules SET module_name=@module_name,module_visible=@module_visible,module_link=@module_link where module_id = @module_id"
        InsertCommand=""
        DeleteCommand="delete from modules where module_id = @module_id">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSAccounts" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT u.UserId,u.Username,m.Email,m.IsLockedOut,m.CreateDate,m.LastPasswordChangedDate,m.LastLoginDate from aspnet_Users u inner join aspnet_UsersInRoles ur on u.Userid = ur.UserId join aspnet_Roles r on ur.roleId = r.RoleId join aspnet_Membership m on u.Userid = m.UserId where r.RoleName = 'Admin'"
        UpdateCommand=""
        InsertCommand=""
        DeleteCommand="">
    </asp:SqlDataSource>
</asp:Content>

