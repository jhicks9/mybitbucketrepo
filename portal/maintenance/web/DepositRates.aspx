<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="DepositRates.aspx.vb" Inherits="Maintenance_web_DepositRates" title="Deposit Interest Rates" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="updateInterest" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvInterest" runat="server" DataSourceID="sqlDS1" DataKeyNames="id" AllowPaging="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbAdd" runat="server" CommandName="AddRecord" CausesValidation="false" CssClass="btn btn-primary"
                                            ToolTip="Add new entry"><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="false" CommandName="Select"
                                            ToolTip="Edit entry" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="type" ItemStyle-Wrap="false" HeaderText="Type" SortExpression="type" />
                                    <asp:BoundField DataField="description" HeaderText="Description" SortExpression="description" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:LinkButton ID="lbAddEmpty" runat="server" CausesValidation="False" ToolTip="Add entry" CommandName="AddRecord" CssClass="btn btn-primary"><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </ContentTemplate> 
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="modal fade" id="interestDetail" tabindex="-1" role="dialog" aria-labelledby="interestDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateInterestDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="InterestDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDS2" CssClass="table table-striped table-bordered table-hover"
                                     AutoGenerateRows="false">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="UserId" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("type") %>' CssClass="col-md-9 form-control no-border"></asp:Label>                                    
                                          </ItemTemplate>
                                          <edititemtemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("type") %>' CssClass="col-md-9 form-control no-border"></asp:Label>
                                          </edititemtemplate>
                                          <InsertItemTemplate >
                                              <asp:DropDownList ID="ddlType" runat="server" DataSourceID="sqlDS2" DataValueField="type" SelectedValue='<%#Bind("type") %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem Text="AR Interest" Value="AR Interest" Selected="True">AR Interest</asp:ListItem>
                                                <asp:ListItem Text="KS Interest" Value="KS Interest">KS Interest</asp:ListItem>
                                                <asp:ListItem Text="MO Interest" Value="MO Interest">MO Interest</asp:ListItem>
                                                <asp:ListItem Text="OK Interest" Value="OK Interest">OK Interest</asp:ListItem>
                                                <asp:ListItem Text="GAS Interest" Value="Gas Interest">Gas Interest</asp:ListItem>
                                              </asp:DropDownList>
                                          </InsertItemTemplate>
                                        </asp:templatefield>                                                                 
                                        <asp:templatefield headertext="Description" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control no-border"></asp:Label>                                    
                                          </ItemTemplate>                                                                
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Height="200" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control"></asp:TextBox>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                 
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblInterestStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel><%--end pnlAuthorized--%>
    
    <asp:SqlDataSource ID="sqlDS1" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,description FROM messages where type in ('MO Interest','AR Interest','KS Interest','OK Interest','Gas Interest') order by type">
    </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqlDS2" runat="server" SelectCommand="SELECT id,type,description FROM messages WHERE (id = @id)"
        ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" UpdateCommand="update messages SET description=@description WHERE id = @id"
         InsertCommand="INSERT INTO messages (type,date,description,begdate,enddate) VALUES (@type,ISNULL(@date, GETDATE()),@description,ISNULL(@begdate, GETDATE()),ISNULL(@enddate, GETDATE()))"
         DeleteCommand="DELETE FROM messages WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvInterest" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="description" Type="String" />
        </UpdateParameters> 
        <InsertParameters>
            <asp:Parameter Type="String" Name="type"></asp:Parameter>
            <asp:Parameter Type="String" Name="description"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="date"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="begdate"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="enddate"></asp:Parameter>                
        </InsertParameters>                       
        <DeleteParameters>
            <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue"
                ControlID="gvInterest"></asp:ControlParameter>
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>
