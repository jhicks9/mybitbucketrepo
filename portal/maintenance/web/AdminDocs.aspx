<%@ Page Language="VB" Debug="true" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="AdminDocs.aspx.vb" Inherits="AdminDocs" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <asp:Panel ID="pnlDocuments" runat="server" Visible="true" >
            <div class="row">
                <div class="col-lg-6">
                    <asp:DropDownList ID="cboGroupid" runat="server" DataSourceID="SqlDataSourceDefault" DataTextField="description" DataValueField="id" AutoPostBack="True" OnSelectedIndexChanged="cboGroupid_SelectedIndexChanged" CssClass="form-control" />
                </div>
                <div class="col-lg-6">
                    <asp:DropDownList ID="cboDocumentType" runat="server" DataTextField="description" DataValueField="id" AutoPostBack="True" OnSelectedIndexChanged="cboDocumentType_SelectedIndexChanged" CssClass="form-control" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gvDocuments" runat="server" DataSourceID="SqlDataSourceDoc" AllowPaging="true" PageSize="15" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbAddRecord" runat="server" CommandName="AddRecord" CausesValidation="false" CssClass="btn btn-primary"><span class="fa fa-plus"></span>&nbsp;Add document</asp:LinkButton>
                                    </HeaderTemplate>
                                <ItemStyle wrap="false" />
                                    <ItemTemplate>
                                        <asp:linkButton ID="btnSelect" runat="server" CommandName="Select" CausesValidation="false" CommandArgument='<%#Eval("id") %>' CssClass="btn btn-info"><span class="fa fa-pencil-square-o"></span>&nbsp;Edit Document</asp:linkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document Description" SortExpression="DocDescription" HeaderStyle-CssClass="form-label">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" Text='<%#Eval("DocDescription") %>'
                                                        NavigateUrl='<%#Eval("id", "~/controls/FHandler.ashx?id={0}") %>' 
                                                        runat="server" /> 
                                        <asp:HyperLink ID="HyperLink2" Text='<%#Eval("WebDescription") %>'
                                                        NavigateUrl='#' Target="_blank" runat="server" Visible="False"/>                                            
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="form-label">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlView" Text='<%#Eval("id") %>'
                                                        NavigateUrl='<%#Eval("id", "~/controls/FHandler.ashx?id={0}&view=1") %>' 
                                                        Target="_blank" runat="server" /> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="lastupdate" HeaderText="Modified" SortExpression="lastupdate" DataFormatString="{0:d}" HtmlEncode="false" />
                                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" Visible="false" />
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <EmptyDataTemplate>
                                <asp:LinkButton ID="lbAddRecord" CommandName="AddRecord" runat="server" CausesValidation="false" CssClass="btn btn-primary"><span class="fa fa-plus"></span>&nbsp;Add document</asp:LinkButton>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12 ">
                        <asp:Label ID="lblStatus" visible="true" runat="server" Text="" />
                    </div>
                </div>
        </asp:Panel><%--pnlDocuments--%>

        <asp:Panel ID="pnlDetail" runat="server" Visible="false" >
            <asp:HiddenField ID="hdnID" runat="server" Value="" />

            <div class="row">
                <div class="col-lg-12 ">
                    <p class="well-sm bg-info text-center"><strong><asp:Label ID="lblFunction" runat="server" Text="" /></strong></p>  
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 ">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="lblID">ID</label></td>
                            <td class="col-md-9"><asp:Label ID="lblID" runat="server" Text="" CssClass="form-control no-border" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtBODDescription">BOD Description</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtBODDescription" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtWEBDescription">Web Description</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtWEBDescription" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="upfile">Document</label></td>
                            <td class="col-md-9"><asp:FileUpload ID="upFile" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtExpires">Default Expiration Date</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtExpires" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><asp:CheckBox ID="chkOverrideLastUpdate" runat="server" Checked="false" AutoPostBack="true" Text=" Last updated" class="btn btn-default" /></td>
                            <td><asp:TextBox ID="txtLastUpdate" runat="server" Enabled="false" CssClass="form-control"/></td>
                        </tr>            
                        <tr>
                            <td class="field-label col-xs-3"></td>
                            <td class="col-md-9">
                                <asp:LinkButton ID="lbUpdate" CommandName="Update" runat="server" CausesValidation="false" Text="Add Document" CssClass="btn btn-primary" />
                                <asp:LinkButton ID="lbCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="btn btn-default" />
                                <asp:LinkButton ID="lbDelete" CommandName="Delete" runat="server" CausesValidation="False" Text="" CssClass="btn btn-warning" ><span class="fa fa-times"></span>&nbsp;Delete</asp:LinkButton>
                            </td>
                        </tr>    
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 ">
                    <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Bold="true" />
                </div>
            </div>
        </asp:Panel><%--pnlDetails--%>
    </asp:Panel><%--pnlAuthorized--%>

    <asp:SqlDataSource ID="SqlDataSourceDefault" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
            SelectCommand="SELECT groups.id, groups.description FROM groups INNER JOIN security ON groups.id = security.groupid WHERE (security.userid = @login) order by groups.description">
        <InsertParameters>
            <asp:ControlParameter ControlID="upFile" Name="FileName" PropertyName="FileName" />
            <asp:ControlParameter ControlID="upFile" Name="FileBytes" PropertyName="FileBytes" />
            <asp:ControlParameter Name="description" ControlID="txtBODDescription"/>
            <asp:ControlParameter Name="Groupid" ControlID="cboGroupid"  PropertyName="SelectedValue"/>
            <asp:ControlParameter Name="DocTypeId" ControlID="cboDocumentType"  PropertyName="SelectedValue"/>
            <asp:ControlParameter Name="Now" ControlID="txtNow"/>
            <asp:ControlParameter Name="Login" ControlID="lblLogin" />
        </InsertParameters>
        <SelectParameters>
            <asp:Parameter Name="login" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceDoc" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT documents.id as id,groups.description, documents.lastupdate, documents.filename, documents.filebytes, documents.doctypeid, documents.groupid, doctype.description AS documenttype, documents.description AS DocDescription, documents.webdescription AS WebDescription FROM documents INNER JOIN doctype ON documents.doctypeid = doctype.id INNER JOIN groups ON documents.groupid = groups.id WHERE (documents.groupid = @groupid AND documents.doctypeid = @doctypeid) ORDER BY lastupdate DESC" 
        DeleteCommand="DELETE FROM documents WHERE (id = @id)">
    <SelectParameters>
        <asp:ControlParameter Name="groupid" ControlID="cboGroupid"  PropertyName="SelectedValue"/>
        <asp:ControlParameter Name="doctypeid" ControlID="cboDocumentType"  PropertyName="SelectedValue"/>
    </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceEdit" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        InsertCommand="INSERT documents (FileName,FileBytes,groupid,doctypeid,description,webdescription,lastupdate,expdate) VALUES (@FileName,@FileBytes,@Groupid,@DocTypeId,@description,@webdescription,@lastupdate,@expdate)"
        DeleteCommand="DELETE FROM documents WHERE (id = @id)">
        <InsertParameters>
            <asp:ControlParameter ControlID="upFile" Name="FileName" PropertyName="FileName" />
            <asp:ControlParameter ControlID="upFile" Name="FileBytes" PropertyName="FileBytes" />
            <asp:ControlParameter ControlID="cboGroupid" Name="Groupid" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="cboDocumentType" Name="DocTypeId" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="description" ControlID="txtBODDescription"/>
            <asp:ControlParameter Name="webdescription" ControlID="txtWEBDescription"/>
            <asp:ControlParameter ControlID="txtExpires" Name="expdate"/>
            <asp:Parameter DbType="DateTime" Name="lastupdate" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter DbType="Int32" Name="id" />
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {
                var txtExpires = $("#<%=txtExpires.ClientId %>");
                txtExpires.parent().css("position", "relative");
                txtExpires.datetimepicker();
                var txtLastUpdate = $("#<%=txtLastUpdate.ClientId %>");
                txtLastUpdate.parent().css("position", "relative");
                txtLastUpdate.datetimepicker();
            });
        </script>
</asp:Content>