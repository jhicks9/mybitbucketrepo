﻿<%@ Page Title="Billing Inserts" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="BillingInserts.aspx.vb" Inherits="Maintenance_web_BillingInserts" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <asp:Panel ID="pnlBillInsert" runat="server" Visible="true">
            <div class="table-responsive">
                <asp:GridView ID="gvBillInserts" runat="server" DataSourceID="sqlDSInserts" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbAdd" runat="server" ToolTip="Add new record" CausesValidation="false" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add</span></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" ToolTip="Edit module" CommandName="Edit" CommandArgument="EditRecord" Text="Edit"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="true" Text="Update"></asp:LinkButton>
                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel"></asp:LinkButton>
                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" CausesValidation="false" Text="Delete" OnClientClick='<%# "return confirm(""Are you sure you want to delete the record?"")" %>'></asp:LinkButton>
                            </EditItemTemplate>                        
                        </asp:TemplateField>
                        <asp:BoundField DataField="id" HeaderText="ID" Visible="False" ReadOnly="True" />                    
                        <asp:TemplateField HeaderText="Name" SortExpression="name">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtName" runat="server" MaxLength="100" Text='<%# Bind("name") %>' />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                    Display="Dynamic" ErrorMessage="Name required." SetFocusOnError="True" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Beginning Date" SortExpression="begdate">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtBegDate" runat="server" Text='<%# Bind("begdate","{0:MM/dd/yyyy}") %>' CssClass="datetimepickerclass" />
                                <asp:RequiredFieldValidator ID="rfvBegDate" runat="server" ControlToValidate="txtBegDate"
                                    Display="Dynamic" ErrorMessage="Date required." SetFocusOnError="True" />
                                <asp:RegularExpressionValidator ID="revBegDate" runat="server" ErrorMessage="MM/DD/YYYY"  ControlToValidate="txtBegDate"
                                     Display="Dynamic" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBegDate" runat="server" Text='<%# Bind("begdate","{0:MM/dd/yyyy}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ending Date" SortExpression="enddate">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEndDate" runat="server" Text='<%# Bind("enddate","{0:MM/dd/yyyy}") %>' CssClass="datetimepickerclass" />
                                <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtEndDate"
                                    Display="Dynamic" ErrorMessage="Date required." SetFocusOnError="True" />
                                <asp:RegularExpressionValidator ID="revEndDate" runat="server" ErrorMessage="MM/DD/YYYY"  ControlToValidate="txtEndDate"
                                     Display="Dynamic" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("enddate","{0:MM/dd/yyyy}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" SortExpression="state">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlState" runat="server" SelectedValue='<%# Bind("state") %>' >
                                    <asp:ListItem Text="All"   Value="" />
                                    <asp:ListItem Text="AR" Value="AR" />
                                    <asp:ListItem Text="KS" Value="KS" />
                                    <asp:ListItem Text="MO" Value="MO" />
                                    <asp:ListItem Text="OK" Value="OK" />
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblState" runat="server" Text='<%# eval("state") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Revenue Class" SortExpression="revenueclass">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlRevenueClass" runat="server" SelectedValue='<%# Bind("revenueclass") %>'>
                                    <asp:ListItem Text="All" Value="" />
                                    <asp:ListItem Text="Commercial" Value="Commercial" />
                                    <asp:ListItem Text="Industrial" Value="Industrial" />
                                    <asp:ListItem Text="Residential" Value="Residential" />
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRevenueClass" runat="server" Text='<%# eval("revenueclass") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" SortExpression="pkgdescription">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlPkgDescription" runat="server" SelectedValue='<%# Bind("pkgdescription") %>'>
                                    <asp:ListItem Text="All" Value="" />
                                    <asp:ListItem Text="Electric" Value="Electric" />
                                    <asp:ListItem Text="Gas" Value="Gas" />
                                    <asp:ListItem Text="Water" Value="Water" />
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPkgDescription" runat="server" Text='<%# eval("pkgdescription") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="required" HeaderText="Required" SortExpression="required" />                    
                        <asp:templatefield ShowHeader="False">
                          <ItemTemplate>
                              <asp:HyperLink ID="hlViewInsert" runat="server" Target="_blank" NavigateUrl='<%# Eval("id", "~/controls/FHandler.ashx?dtype=inserts&id={0}&view=1") %>'>View</asp:HyperLink>
                          </ItemTemplate>
                          <edititemtemplate>
                            <asp:FileUpload ID="UpdateUploadedFile" runat="server" />
                            <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red" />
                          </edititemtemplate>
                        </asp:templatefield>
                    </Columns>
                    <PagerStyle CssClass="pagination-ys" />
                    <EmptyDataTemplate>
                        <asp:LinkButton ID="lbAddEmpty" runat="server" ToolTip="Add new record" CausesValidation="false" CommandArgument="AddRecord" CommandName="AddRecord" Text="Add"></asp:LinkButton>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </asp:Panel><!-- pnlBillInsert -->


        <asp:Panel ID="pnlAddInsert" runat="server" Visible="false">
            <div class="row"><div class="col-lg-12"><p class="well-sm bg-info text-center"><strong>Add Bill Insert</strong></p></div></div>
            <table class="table table-striped table-bordered">
                <tr>
                    <td class="field-label col-xs-3 active">Name</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="100" Text="" CssClass="col-md-9 form-control" />
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ValidationGroup="addInsert"
                            Display="Dynamic" ErrorMessage="Name required." SetFocusOnError="True" />
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">Beginning Date</td>
                    <td>
                        <asp:TextBox ID="txtBegDate" runat="server" Text="" CssClass="col-md-9 form-control datetimepickerclass" />
                        <asp:RequiredFieldValidator ID="rfvBegDate" runat="server" ControlToValidate="txtBegDate" ValidationGroup="addInsert"
                            Display="Dynamic" ErrorMessage="Date required." SetFocusOnError="True" />
                        <asp:RegularExpressionValidator ID="revBegDate" runat="server" ErrorMessage="MM/DD/YYYY" ControlToValidate="txtBegDate" ValidationGroup="addInsert"
                            Display="Dynamic" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" />
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">Ending Date</td>
                    <td>
                        <asp:TextBox ID="txtEndDate" runat="server" Text="" CssClass="col-md-9 form-control datetimepickerclass" />
                        <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtEndDate" ValidationGroup="addInsert"
                            Display="Dynamic" ErrorMessage="Date required." SetFocusOnError="True" />
                        <asp:RegularExpressionValidator ID="revEndDate" runat="server" ErrorMessage="MM/DD/YYYY"  ControlToValidate="txtEndDate" ValidationGroup="addInsert"
                            Display="Dynamic" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" />
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">State</td>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server" CssClass="col-md-9 form-control">
                            <asp:ListItem Text="All" Value="" Selected="True" />
                            <asp:ListItem Text="AR" Value="AR" />
                            <asp:ListItem Text="KS" Value="KS" />
                            <asp:ListItem Text="MO" Value="MO" />
                            <asp:ListItem Text="OK" Value="OK" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">Revenue Class</td>
                    <td>
                        <asp:DropDownList ID="ddlRevenueClass" runat="server" CssClass="col-md-9 form-control">
                            <asp:ListItem Text="All" Value="" Selected="True" />
                            <asp:ListItem Text="Commercial" Value="Commercial" />
                            <asp:ListItem Text="Industrial" Value="Industrial" />
                            <asp:ListItem Text="Residential" Value="Residential" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">Type</td>
                    <td>
                        <asp:DropDownList ID="ddlPkgDescription" runat="server" CssClass="col-md-9 form-control">
                            <asp:ListItem Text="All" Value="" Selected="True"/>
                            <asp:ListItem Text="Electric" Value="Electric" />
                            <asp:ListItem Text="Gas" Value="Gas" />
                            <asp:ListItem Text="Water" Value="Water" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">Required</td>
                    <td><asp:CheckBox ID="ckbRequired" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active">File</td>
                    <td><asp:FileUpload ID="fluBillInsert" runat="server" CssClass="col-md-9 form-control" />
                        <asp:Label ID="lblFileValidation" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"></td>
                    <td class="col-md-9">
                        <asp:LinkButton ID="lbInsert" CommandName="Update" runat="server" CausesValidation="true" Text="Update" ValidationGroup="addInsert" CssClass="btn btn-primary" />
                        <asp:LinkButton ID="lbCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="btn btn-default" />
                    </td>
                </tr>
            </table>
        </asp:Panel><!-- pnlAddInsert -->

    </asp:Panel><!-- pnlAuthorized -->
    
    <asp:SqlDataSource ID="sqlDSInserts" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="select * from billinsert order by billinsert.pkgdescription,billinsert.state,billinsert.revenueclass,billinsert.begdate"
        UpdateCommand="update billinsert SET name=@name,begdate=@begdate,enddate=@enddate,state=@state,revenueclass=@revenueclass,pkgdescription=@pkgdescription,required=@required,filename=@filename,imagedata=@imagedata where id = @id"
        InsertCommand=""
        DeleteCommand="delete from billinsert where id = @id">
        <UpdateParameters>
            <asp:Parameter Name="imagedata" />
        </UpdateParameters>
    </asp:SqlDataSource>            
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {   
                var txtBegDate = $("#<%=txtBegDate.ClientId %>");
                txtBegDate.parent().css("position", "relative");
                txtBegDate.datetimepicker({ format: 'MM/DD/YYYY' });
                var txtEndDate = $("#<%=txtEndDate.ClientId %>");
                txtEndDate.parent().css("position", "relative");
                txtEndDate.datetimepicker({ format: 'MM/DD/YYYY' });
            });
        </script>
</asp:Content>