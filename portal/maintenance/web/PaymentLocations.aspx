<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="PaymentLocations.aspx.vb" Inherits="Maintenance_web_PaymentLocations" title="Payment Locations" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Arkansas</a></li>
                <li><a href="#tab_b" data-toggle="pill">Kansas</a></li>
                <li><a href="#tab_c" data-toggle="pill">Missouri</a></li>
                <li><a href="#tab_d" data-toggle="pill">Oklahoma</a></li>
                <li><a href="#tab_e" data-toggle="pill">Missouri Gas</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateArkansas" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvArkansas" runat="server" DataSourceID="sqlDSAR" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddArkansas" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new location" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectArkansas" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit location" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name" />
                                        <asp:BoundField DataField="area" HeaderText="Area" SortExpression="Area" />                                
                                        <asp:BoundField DataField="address" HeaderText="Address" SortExpression="Address" />
                                        <asp:BoundField DataField="city" HeaderText="City" SortExpression="City" />
                                        <asp:BoundField DataField="state" HeaderText="State" SortExpression="State" />
                                        <asp:BoundField DataField="zip" HeaderText="Zip" SortExpression="Zip" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="Phone" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddAREmpty" runat="server" CausesValidation="False" ToolTip="Add new location" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel><!-- updateArkansas -->
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateKansas" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvKansas" runat="server" DataSourceID="sqlDSKS" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddKansas" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new location" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectKansas" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit location" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name" />
                                        <asp:BoundField DataField="area" HeaderText="Area" SortExpression="Area" />                                
                                        <asp:BoundField DataField="address" HeaderText="Address" SortExpression="Address" />
                                        <asp:BoundField DataField="city" HeaderText="City" SortExpression="City" />
                                        <asp:BoundField DataField="state" HeaderText="State" SortExpression="State" />
                                        <asp:BoundField DataField="zip" HeaderText="Zip" SortExpression="Zip" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="Phone" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddKSEmpty" runat="server" CausesValidation="False" ToolTip="Add new location" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel><!-- updateKansas -->
                    </div>
                </div><!-- tab-B -->

                <div class="tab-pane" id="tab_c">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateMissouri" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvMissouri" runat="server" DataSourceID="sqlDSMO" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddMissouri" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new location" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectMissouri" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit location" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name" />
                                        <asp:BoundField DataField="area" HeaderText="Area" SortExpression="Area" />                                
                                        <asp:BoundField DataField="address" HeaderText="Address" SortExpression="Address" />
                                        <asp:BoundField DataField="city" HeaderText="City" SortExpression="City" />
                                        <asp:BoundField DataField="state" HeaderText="State" SortExpression="State" />
                                        <asp:BoundField DataField="zip" HeaderText="Zip" SortExpression="Zip" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="Phone" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddMOEmpty" runat="server" CausesValidation="False" ToolTip="Add new location" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel><!-- updateMissouri -->
                    </div>
                </div><!-- tab-C -->

                <div class="tab-pane" id="tab_d">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateOklahoma" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvOKlahoma" runat="server" DataSourceID="sqlDSOK" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddOklahoma" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new location" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectOklahoma" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit location" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name" />
                                        <asp:BoundField DataField="area" HeaderText="Area" SortExpression="Area" />                                
                                        <asp:BoundField DataField="address" HeaderText="Address" SortExpression="Address" />
                                        <asp:BoundField DataField="city" HeaderText="City" SortExpression="City" />
                                        <asp:BoundField DataField="state" HeaderText="State" SortExpression="State" />
                                        <asp:BoundField DataField="zip" HeaderText="Zip" SortExpression="Zip" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="Phone" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddOKEmpty" runat="server" CausesValidation="False" ToolTip="Add new location" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel><!-- updateOklahoma -->
                    </div>
                </div><!-- tab-D -->

                <div class="tab-pane" id="tab_e">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateGas" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvGas" runat="server" DataSourceID="sqlDSGAS" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddGas" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new location" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectGas" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit location" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name" />
                                        <asp:BoundField DataField="area" HeaderText="Area" SortExpression="Area" />                                
                                        <asp:BoundField DataField="address" HeaderText="Address" SortExpression="Address" />
                                        <asp:BoundField DataField="city" HeaderText="City" SortExpression="City" />
                                        <asp:BoundField DataField="state" HeaderText="State" SortExpression="State" />
                                        <asp:BoundField DataField="zip" HeaderText="Zip" SortExpression="Zip" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="Phone" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddGasEmpty" runat="server" CausesValidation="False" ToolTip="Add new location" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add location</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel><!-- updateGas -->
                    </div>
                </div><!-- tab-E -->
            </div><!-- tab-content -->
        </div><!-- tab container -->
        
        <div class="modal fade" id="ARDetail" tabindex="-1" role="dialog" aria-labelledby="ARDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateARDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="ARDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSAR2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False" />
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Area" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("area") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Address" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("address") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Address2" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("address2") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="City" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                                                                                                
                                        <asp:templatefield headertext="State" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:DropDownList ID="ddlState" runat="server" SelectedValue='<%# Bind("state") %>' SelectedIndex='<%#GetSelectedIndex(DataBinder.Eval(Container.DataItem,"state"), "AR") %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>AR</asp:ListItem>
                                                <asp:ListItem>KS</asp:ListItem>
                                                <asp:ListItem>MO</asp:ListItem>
                                                <asp:ListItem>OK</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Zip" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtZip" runat="server" Text='<%# Bind("zip") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip Code is required" SetFocusOnError="True" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="revZip" runat="server" ControlToValidate="txtZip" ErrorMessage="5 digit or zip + 4" SetFocusOnError="True" ValidationExpression="\d{5}(-\d{4})?"  Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone Number format xxx-xxx-xxxx" SetFocusOnError="True" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Hours" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtHours" runat="server" TextMode="MultiLine" Text='<%# Bind("hours") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Payment Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPayments" runat="server" TextMode="MultiLine" Text='<%# Bind("payments") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><!-- ARDetails -->
                                <asp:Label ID="lblARStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                            </contenttemplate>
                        </asp:UpdatePanel><!-- updateARDetails -->
                    </div>
                </div>
            </div>
        </div><%--ARDetail--%>

        <div class="modal fade" id="KSDetail" tabindex="-1" role="dialog" aria-labelledby="KSDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateKSDetails" runat="server">
                            <contenttemplate>	                                 
                                <asp:DetailsView ID="KSDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSKS2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Area" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("area") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Address" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("address") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Address2" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("address2") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="City" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                                                                                                
                                        <asp:templatefield headertext="State" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:DropDownList ID="ddlState" runat="server" SelectedValue='<%# Bind("state") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"state"), "KS") %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>AR</asp:ListItem>
                                                <asp:ListItem>KS</asp:ListItem>
                                                <asp:ListItem>MO</asp:ListItem>
                                                <asp:ListItem>OK</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Zip" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtZip" runat="server" Text='<%# Bind("zip") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip Code is required" SetFocusOnError="True" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="revZip" runat="server" ControlToValidate="txtZip" ErrorMessage="5 digit or zip + 4" SetFocusOnError="True" ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone Number format xxx-xxx-xxxx" SetFocusOnError="True" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"  Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Hours" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtHours" runat="server" TextMode="MultiLine" Text='<%# Bind("hours") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Payment Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPayments" runat="server" TextMode="MultiLine" Text='<%# Bind("payments") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><!-- KSDetails -->
                                <asp:Label ID="lblKSStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><!-- updateKSDetails -->
                    </div>
                </div>
            </div>
        </div><%--KSDetail--%>

        <div class="modal fade" id="MODetail" tabindex="-1" role="dialog" aria-labelledby="MODetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateMODetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="MODetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSMO2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False" />
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Area" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("area") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Address" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("address") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Address2" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("address2") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="City" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                                                                                                
                                        <asp:templatefield headertext="State" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:DropDownList ID="ddlState" runat="server" SelectedValue='<%# Bind("state") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"state"), "MO") %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>AR</asp:ListItem>
                                                <asp:ListItem>KS</asp:ListItem>
                                                <asp:ListItem>MO</asp:ListItem>
                                                <asp:ListItem>OK</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Zip" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtZip" runat="server" Text='<%# Bind("zip") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip Code is required" SetFocusOnError="True" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="revZip" runat="server" ControlToValidate="txtZip" ErrorMessage="5 digit or zip + 4" SetFocusOnError="True" ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone Number format xxx-xxx-xxxx" SetFocusOnError="True" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Hours" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtHours" runat="server" TextMode="MultiLine" Text='<%# Bind("hours") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Payment Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPayments" runat="server" TextMode="MultiLine" Text='<%# Bind("payments") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField >
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary"/>
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><!-- MODetails -->
                                <asp:Label ID="lblMOStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><!-- updateMODetails -->
                    </div>
                </div>
            </div>
        </div><!-- MODetail -->

        <div class="modal fade" id="OKDetail" tabindex="-1" role="dialog" aria-labelledby="OKDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateOKDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="OKDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSOK2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False" />
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Area" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("area") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Address" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("address") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Address2" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("address2") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="City" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                                                                                                
                                        <asp:templatefield headertext="State" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:DropDownList ID="ddlState" runat="server" SelectedValue='<%# Bind("state") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"state"), "OK") %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>AR</asp:ListItem>
                                                <asp:ListItem>KS</asp:ListItem>
                                                <asp:ListItem>MO</asp:ListItem>
                                                <asp:ListItem>OK</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Zip" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtZip" runat="server" Text='<%# Bind("zip") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip Code is required" SetFocusOnError="True" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="revZip" runat="server" ControlToValidate="txtZip" ErrorMessage="5 digit or zip + 4" SetFocusOnError="True" ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone Number format xxx-xxx-xxxx" SetFocusOnError="True" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Hours" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtHours" runat="server" TextMode="MultiLine" Text='<%# Bind("hours") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Payment Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPayments" runat="server" TextMode="MultiLine" Text='<%# Bind("payments") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary"/>
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><!-- OKDetails -->
                                <asp:Label ID="lblOKStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><!-- updateOKDetails -->
                    </div>
                </div>
            </div>
        </div><%--OKDetail--%>

        <div class="modal fade" id="GASDetail" tabindex="-1" role="dialog" aria-labelledby="GASDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateGASDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="GASDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSGAS2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False" />
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Area" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("area") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Address" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("address") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Address2" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("address2") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="City" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                                                                                                
                                        <asp:templatefield headertext="State" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:DropDownList ID="ddlState" runat="server" SelectedValue='<%# Bind("state") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"state"), "MO") %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>AR</asp:ListItem>
                                                <asp:ListItem>KS</asp:ListItem>
                                                <asp:ListItem>MO</asp:ListItem>
                                                <asp:ListItem>OK</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Zip" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtZip" runat="server" Text='<%# Bind("zip") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip Code is required" SetFocusOnError="True" Display="Dynamic" />
                                            <asp:RegularExpressionValidator ID="revZip" runat="server" ControlToValidate="txtZip" ErrorMessage="5 digit or zip + 4" SetFocusOnError="True" ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone Number format xxx-xxx-xxxx" SetFocusOnError="True" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" Display="Dynamic" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Hours" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtHours" runat="server" TextMode="MultiLine" Text='<%# Bind("hours") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Payment Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPayments" runat="server" TextMode="MultiLine" Text='<%# Bind("payments") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><!-- GasDetails -->
                                <asp:Label ID="lblGASStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><!-- updateGASDetails -->
                    </div>
                </div>
            </div>
        </div><%--GASDetail--%>
    </asp:Panel>


    <asp:SqlDataSource ID="sqlDSAR" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
        SelectCommand="SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'AR' order by area,city,name">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSKS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
        SelectCommand="SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'KS' order by area,city,name">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSMO" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
        SelectCommand="SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'MO' order by area,city,name">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSOK" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
        SelectCommand="SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'electric' and state = 'OK' order by area,city,name">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSGAS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
        SelectCommand="SELECT id,type,area,name,address,city,state,zip,phone,hours,payments from plocations where type = 'gas' order by area,city,name">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="sqlDSAR2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,area,name,address,address2,city,state,zip,phone,hours,payments from plocations WHERE (plocations.id = @id)"
        InsertCommand="INSERT INTO plocations (type,area,name,address,address2,city,state,zip,phone,hours,payments,latitude,longitude) VALUES (@type,@area,@name,@address,@address2,@city,@state,@zip,@phone,@hours,@payments,@latitude,@longitude)"
        UpdateCommand="UPDATE plocations SET name=@name,area=@area,address=@address,address2=@address2,city=@city,state=@state,zip=@zip,phone=@phone,hours=@hours,payments=@payments,latitude=@latitude,longitude=@longitude WHERE (plocations.id = @id)"
        DeleteCommand="DELETE FROM plocations WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvArkansas" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="area" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="city" Type="String" />
            <asp:Parameter Name="state" Type="String" />
            <asp:Parameter Name="zip" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="hours" Type="String" />
            <asp:Parameter Name="payments" Type="String" />
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric"/>
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSKS2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,area,name,address,address2,city,state,zip,phone,hours,payments from plocations WHERE (plocations.id = @id)"
        InsertCommand="INSERT INTO plocations (type,area,name,address,address2,city,state,zip,phone,hours,payments,latitude,longitude) VALUES (@type,@area,@name,@address,@address2,@city,@state,@zip,@phone,@hours,@payments,@latitude,@longitude)"
        UpdateCommand="UPDATE plocations SET name=@name,area=@area,address=@address,address2=@address2,city=@city,state=@state,zip=@zip,phone=@phone,hours=@hours,payments=@payments,latitude=@latitude,longitude=@longitude WHERE (plocations.id = @id)"
        DeleteCommand="DELETE FROM plocations WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvKansas" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="area" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="city" Type="String" />
            <asp:Parameter Name="state" Type="String" />
            <asp:Parameter Name="zip" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="hours" Type="String" />
            <asp:Parameter Name="payments" Type="String" />
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric"/>
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </InsertParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="sqlDSMO2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,area,name,address,address2,city,state,zip,phone,hours,payments from plocations WHERE (plocations.id = @id)"
        InsertCommand="INSERT INTO plocations (type,area,name,address,address2,city,state,zip,phone,hours,payments,latitude,longitude) VALUES (@type,@area,@name,@address,@address2,@city,@state,@zip,@phone,@hours,@payments,@latitude,@longitude)"
        UpdateCommand="UPDATE plocations SET name=@name,area=@area,address=@address,address2=@address2,city=@city,state=@state,zip=@zip,phone=@phone,hours=@hours,payments=@payments,latitude=@latitude,longitude=@longitude WHERE (plocations.id = @id)"
        DeleteCommand="DELETE FROM plocations WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvMissouri" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="area" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="city" Type="String" />
            <asp:Parameter Name="state" Type="String" />
            <asp:Parameter Name="zip" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="hours" Type="String" />
            <asp:Parameter Name="payments" Type="String" />
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric"/>
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSOK2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,area,name,address,address2,city,state,zip,phone,hours,payments from plocations WHERE (plocations.id = @id)"
        InsertCommand="INSERT INTO plocations (type,area,name,address,address2,city,state,zip,phone,hours,payments,latitude,longitude) VALUES (@type,@area,@name,@address,@address2,@city,@state,@zip,@phone,@hours,@payments,@latitude,@longitude)"
        UpdateCommand="UPDATE plocations SET name=@name,area=@area,address=@address,address2=@address2,city=@city,state=@state,zip=@zip,phone=@phone,hours=@hours,payments=@payments,latitude=@latitude,longitude=@longitude WHERE (plocations.id = @id)"
        DeleteCommand="DELETE FROM plocations WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvOklahoma" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="area" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="city" Type="String" />
            <asp:Parameter Name="state" Type="String" />
            <asp:Parameter Name="zip" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="hours" Type="String" />
            <asp:Parameter Name="payments" Type="String" />
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric"/>
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSGAS2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,area,name,address,address2,city,state,zip,phone,hours,payments from plocations WHERE (plocations.id = @id)"
        InsertCommand="INSERT INTO plocations (type,area,name,address,address2,city,state,zip,phone,hours,payments,latitude,longitude) VALUES (@type,@area,@name,@address,@address2,@city,@state,@zip,@phone,@hours,@payments,@latitude,@longitude)"
        UpdateCommand="UPDATE plocations SET name=@name,area=@area,address=@address,address2=@address2,city=@city,state=@state,zip=@zip,phone=@phone,hours=@hours,payments=@payments,latitude=@latitude,longitude=@longitude WHERE (plocations.id = @id)"
        DeleteCommand="DELETE FROM plocations WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvGas" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="area" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="city" Type="String" />
            <asp:Parameter Name="state" Type="String" />
            <asp:Parameter Name="zip" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="hours" Type="String" />
            <asp:Parameter Name="payments" Type="String" />
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Gas"/>
            <asp:Parameter Name="latitude" Type="Decimal" DefaultValue="0"/>
            <asp:Parameter Name="longitude" Type="Decimal" DefaultValue="0"/>
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
