Imports System.Data.SqlClient
Imports System.Data

Partial Class maintenance_web_CommAsstAccounts
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub
    '============================== Assistance Maintenance ===========================================================================

    Protected Sub gvAccountsA_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsA.RowCommand
        Select Case e.CommandName
            Case "Select"
                AccountsADetails.HeaderText = "Edit"
                AccountsADetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ADetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ADetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                lblType.Text = "Assistance"
                gvAccountsA.SelectedIndex = -1
                AccountsADetails.HeaderText = "Add"
                AccountsADetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AddDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddDetailModalScript", sb.ToString(), False)
            Case Else
                AccountsADetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub AccountsADetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles AccountsADetails.ItemCommand
        Select Case e.CommandName
            Case "Del"
                DeleteAccount(AccountsADetails.DataKey.Item(0).ToString)
                gvAccountsA.SelectedIndex = -1
                gvAccountsA.DataBind()
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ADetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ADetailModalScript", sb.ToString(), False)
            Case "Can"
                gvAccountsA.SelectedIndex = -1
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ADetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ADetailModalScript", sb.ToString(), False)
            Case "Upd"
                If UpdateAccountA(AccountsADetails.DataKey.Item(0).ToString) Then
                    gvAccountsA.SelectedIndex = -1
                    gvAccountsA.DataBind()
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#ADetail').modal('hide');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ADetailModalScript", sb.ToString(), False)
                Else
                End If
        End Select
    End Sub

    Public Function UpdateAccountA(ByVal id As String) As Boolean
        Try
            Dim status As Boolean = True
            Dim mbr As MembershipProvider
            Dim u As MembershipUser
            Dim newEmail As TextBox = (CType(AccountsADetails.FindControl("txtEmail"), TextBox))
            Dim username As Label = (CType(AccountsADetails.FindControl("lblUserName"), Label))
            Dim lblStatus As Label = (CType(AccountsADetails.FindControl("lblStatus"), Label))
            Dim pwd As TextBox = (CType(AccountsADetails.FindControl("txtPassword1"), TextBox))
            Dim pwdconfirm As TextBox = (CType(AccountsADetails.FindControl("txtPassword2"), TextBox))
            mbr = Membership.Providers.Item("empireMembershipProvider")
            Dim oldpassword As String = mbr.GetPassword(username.Text, vbNull)

            'update email address if changed
            u = mbr.GetUser(username.Text, False)
            If u.Email <> newEmail.Text Then
                If Not Regex.IsMatch(newEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") Then
                    lblStatus.Text += "Invalid Email Format"
                    status = False
                Else
                    u.Email = newEmail.Text
                    mbr.UpdateUser(u)
                End If
            End If

            'update password if necessary
            If Len(pwd.Text) > 0 Then  'a password was keyed
                If pwd.Text = pwdconfirm.Text Then  'verify password confirmation matches
                    If Not Regex.IsMatch(pwd.Text, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*") Then  'ensure password meets criteria
                        lblStatus.Text += "Passwords are required to be at least 8 characters long including one special character (for example: john_123)."
                        status = False
                    Else
                        mbr.ChangePassword(username.Text, oldpassword, pwd.Text)
                    End If
                Else
                    lblStatus.Text = "Passwords do not match."
                    status = False
                End If
            End If
            Return status
        Catch
            Return False
        End Try
    End Function
    '============================== End Assistance Maintenance =======================================================================
    '============================== Commission Maintenance ===========================================================================

    Protected Sub gvAccountsC_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsC.RowCommand
        Select Case e.CommandName
            Case "Select"
                AccountsCDetails.HeaderText = "Edit"
                AccountsCDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#CDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "CDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                lblType.Text = "Commission"
                gvAccountsC.SelectedIndex = -1
                AccountsCDetails.HeaderText = "Add"
                AccountsCDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AddDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddDetailModalScript", sb.ToString(), False)
            Case Else
                AccountsCDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub AccountsCDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles AccountsCDetails.ItemCommand
        Select Case e.CommandName
            Case "Del"
                DeleteAccount(AccountsCDetails.DataKey.Item(0).ToString)
                gvAccountsC.SelectedIndex = -1
                gvAccountsC.DataBind()
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#CDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "CDetailModalScript", sb.ToString(), False)
            Case "Can"
                gvAccountsC.SelectedIndex = -1
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#CDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "CDetailModalScript", sb.ToString(), False)
            Case "Upd"
                If UpdateAccountC(AccountsCDetails.DataKey.Item(0).ToString) Then
                    gvAccountsC.SelectedIndex = -1
                    gvAccountsC.DataBind()
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#CDetail').modal('hide');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "CDetailModalScript", sb.ToString(), False)
                Else
                End If
        End Select
    End Sub

    Public Function UpdateAccountC(ByVal id As String) As Boolean
        Try
            Dim status As Boolean = True
            Dim mbr As MembershipProvider
            Dim u As MembershipUser
            Dim newEmail As TextBox = (CType(AccountsCDetails.FindControl("txtEmail"), TextBox))
            Dim username As Label = (CType(AccountsCDetails.FindControl("lblUserName"), Label))
            Dim lblStatus As Label = (CType(AccountsCDetails.FindControl("lblStatus"), Label))
            Dim pwd As TextBox = (CType(AccountsCDetails.FindControl("txtPassword1"), TextBox))
            Dim pwdconfirm As TextBox = (CType(AccountsCDetails.FindControl("txtPassword2"), TextBox))
            mbr = Membership.Providers.Item("empireMembershipProvider")
            Dim oldpassword As String = mbr.GetPassword(username.Text, vbNull)

            'update email address if changed
            u = mbr.GetUser(username.Text, False)
            If u.Email <> newEmail.Text Then
                If Not Regex.IsMatch(newEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") Then
                    lblStatus.Text += "Invalid Email Format"
                    status = False
                Else
                    u.Email = newEmail.Text
                    mbr.UpdateUser(u)
                End If
            End If

            'update password if necessary
            If Len(pwd.Text) > 0 Then  'a password was keyed
                If pwd.Text = pwdconfirm.Text Then  'verify password confirmation matches
                    If Not Regex.IsMatch(pwd.Text, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*") Then  'ensure password meets criteria
                        lblStatus.Text += "Passwords are required to be at least 8 characters long including one special character (for example: john_123)."
                        status = False
                    Else
                        mbr.ChangePassword(username.Text, oldpassword, pwd.Text)
                    End If
                Else
                    lblStatus.Text = "Passwords do not match."
                    status = False
                End If
            End If
            Return status
        Catch
            Return False
        End Try
    End Function

    Protected Sub CreateUserWizard1_CancelButtonClick(sender As Object, e As EventArgs) Handles CreateUserWizard1.CancelButtonClick
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#AddDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub CreateUserWizard1_ContinueButtonClick(sender As Object, e As EventArgs) Handles CreateUserWizard1.ContinueButtonClick
        CreateUserWizard1.ActiveStepIndex = 0
    End Sub
    '============================== End Commission Maintenance =======================================================================

    Protected Sub CreateUserWizard1_CreatedUser(ByVal sender As Object, ByVal e As System.EventArgs) Handles CreateUserWizard1.CreatedUser
        Try
            ' Create new user.
            If Not CreateUserWizard1.UserName Is Nothing Then
                If Not Roles.IsUserInRole(CreateUserWizard1.UserName, lblType.Text) Then
                    Dim arUserName(0) As String
                    arUserName(0) = CreateUserWizard1.UserName
                    Dim arRoleName(0) As String
                    arRoleName(0) = lblType.Text
                    Roles.Providers("empireRoleSqlProvider").AddUsersToRoles(arUserName, arRoleName)

                    gvAccountsA.DataBind()
                    gvAccountsC.DataBind()
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#AddDetail').modal('hide');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddDetailModalScript", sb.ToString(), False)
                End If
            End If
        Catch er As MembershipCreateUserException
        End Try
    End Sub

    Public Sub DeleteAccount(ByVal id As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Try 
            'deletes userid entry from aspnet_profile, aspnet_usersinroles, aspnet_membership, aspnet_users
            Dim query1 As String = "delete from aspnet_Profile WHERE UserID = @id"
            Dim query2 As String = "delete from aspnet_UsersInRoles WHERE UserID = @id"
            Dim query3 As String = "delete from aspnet_membership where userID = @id"
            Dim query4 As String = "delete from aspnet_users where userID = @id"
            conn.Open()
            Dim cmd As New SqlCommand(query1, conn)
            cmd.Parameters.AddWithValue("@id", id)
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            cmd.ExecuteNonQuery()
            cmd.CommandText = query3
            cmd.ExecuteNonQuery()
            cmd.CommandText = query4
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
        End Try
    End Sub

End Class
