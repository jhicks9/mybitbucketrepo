<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="CommAsstAccounts.aspx.vb" Inherits="maintenance_web_CommAsstAccounts" title="Comm/Agency Accounts" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Assistance Agency</a></li>
                <li><a href="#tab_b" data-toggle="pill">Commission</a></li>
            </ul>
            <div class="row"><div class="col-lg-12"><hr /></div></div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="pnlAssistance" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAccountsA" runat="server" DataSourceID="sqlDSA" DataKeyNames="UserId" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddA" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new account" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectA" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit account" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UserName" HeaderText="Username" SortExpression="UserName" />
                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                        <asp:BoundField DataField="CreateDate" HeaderText="Created" SortExpression="CreateDate" />
                                        <asp:BoundField DataField="LastLoginDate" HeaderText="Last Accessed" SortExpression="LastLoginDate" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddAEmpty" runat="server" CausesValidation="False" ToolTip="Add new account" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="pnlCommission" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAccountsC" runat="server" DataSourceID="sqlDSC" DataKeyNames="UserId" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddC" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new account" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectA" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit account" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UserName" HeaderText="Username" SortExpression="UserName" />
                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                        <asp:BoundField DataField="CreateDate" HeaderText="Created" SortExpression="CreateDate" />
                                        <asp:BoundField DataField="LastLoginDate" HeaderText="Last Accessed" SortExpression="LastLoginDate" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddCEmpty" runat="server" CausesValidation="False" ToolTip="Add new account" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->
            </div><!-- tab-content -->
        </div><!-- tab container -->

        <div class="modal fade" id="ADetail" tabindex="-1" role="dialog" aria-labelledby="ADetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlAccountsADetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="AccountsADetails" runat="server" DataKeyNames="UserId" DataSourceID="sqlDSA2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="UserId" SortExpression="UserId" HeaderText="UserId" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="UserName" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>' CssClass="col-md-9 no-border"/>
                                          </ItemTemplate>                                                                
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Confirm Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control"/>
                                            <asp:CompareValidator id="ConfirmPassword" runat="server" ControlToValidate="txtPassword2" ForeColor="red"
                                                Display="Static" ControlToCompare="txtPassword1" ErrorMessage="Passwords must match." />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Email" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("email") %>' CssClass="col-md-9 form-control"/>
                                          </ItemTemplate>                                                                
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("email") %>' CssClass="col-md-9 form-control"/>
                                                <asp:RegularExpressionValidator ID="ValidEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">Invalid Email Format</asp:RegularExpressionValidator>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Status" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Text="" CssClass="col-md-9 no-border"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary"/>
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default"/>
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Upd" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary"/>
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Can" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Del" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>	        	                                                                           
                                <asp:Label ID="lblAccountsAStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div><!-- ADetail -->

        <div class="modal fade" id="CDetail" tabindex="-1" role="dialog" aria-labelledby="CDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlAccountsCDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="AccountsCDetails" runat="server" DataKeyNames="UserId" DataSourceID="sqlDSC2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="UserId" SortExpression="UserId" HeaderText="UserId" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="UserName" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <ItemTemplate>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>' CssClass="col-md-9 no-border" />
                                            </ItemTemplate>                                                                
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <edititemtemplate>
                                            <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control" />
                                            </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Confirm Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <edititemtemplate>
                                            <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control"/>
                                            <asp:CompareValidator id="ConfirmPassword" runat="server" ControlToValidate="txtPassword2" ForeColor="red"
                                                Display="Static" ControlToCompare="txtPassword1" ErrorMessage="Passwords must match." />
                                            </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Email" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("email") %>' CssClass="col-md-9 form-control"/>
                                            </ItemTemplate>                                                                
                                            <edititemtemplate>
                                            <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("email") %>' CssClass="col-md-9 form-control"/>
                                                <asp:RegularExpressionValidator ID="ValidEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">Invalid Email Format</asp:RegularExpressionValidator>
                                            </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Status" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <edititemtemplate>
                                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Text="" CssClass="col-md-9 no-border" />
                                            </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                                <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary"/>
                                                <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default"/>
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="lbEditUpdate" CommandName="Upd" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary"/>
                                                <asp:LinkButton ID="lbEditCancel" CommandName="Can" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default"/>
                                                <asp:LinkButton ID="lbEditDelete" CommandName="Del" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblAccountsCStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div><!-- CDetail -->

        <div class="modal fade" id="AddDetail" tabindex="-1" role="dialog" aria-labelledby="AddDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlAddAccount" runat="server">
                            <contenttemplate>
                                <div class="row">
                                    <div class="col-lg-12"><p class="well-sm bg-info text-center"><strong><asp:Label ID="lblType" runat="server" Text="" /> Account Creation</strong></p></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" MembershipProvider="empireMembershipProvider" LoginCreatedUser="False" DisplayCancelButton="true" EnableViewState="false">
                                            <WizardSteps>
                                                <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                                </asp:CreateUserWizardStep>
                                                <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                                                </asp:CompleteWizardStep>
                                            </WizardSteps>
                                        </asp:CreateUserWizard>
                                        <asp:Label ID="lblAddUserStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                    </div>
                                </div>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div><!-- AddDetail -->
    </asp:Panel><%--end pnlAuthorized--%>

    <asp:SqlDataSource ID="sqlDSA" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate, aspnet_Roles.RoleName FROM aspnet_Users INNER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId INNER JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Roles.RoleName = 'assistance')">
    </asp:SqlDataSource>     
    <asp:SqlDataSource ID="sqlDSA2" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email FROM aspnet_Users INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvAccountsA" Name="UserId" PropertyName="SelectedValue"/>
        </SelectParameters>        
    </asp:SqlDataSource>          
    <asp:SqlDataSource ID="sqlDSC" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate, aspnet_Roles.RoleName FROM aspnet_Users INNER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId INNER JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Roles.RoleName = 'commission')">
    </asp:SqlDataSource>  
    <asp:SqlDataSource ID="sqlDSC2" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email FROM aspnet_Users INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvAccountsC" Name="UserId" PropertyName="SelectedValue"/>
        </SelectParameters>        
    </asp:SqlDataSource>                
</asp:Content>