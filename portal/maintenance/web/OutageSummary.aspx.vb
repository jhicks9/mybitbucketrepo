﻿Imports System.IO
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.DataVisualization.Charting

Partial Class maintenance_web_OutageSummary
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isAdmin(ps.getPageName) Then
            pnlAuthorized.Visible = True
            pnlNotAuthorized.Visible = False
            pnlAdministration.Visible = True
        Else
            pnlAdministration.Visible = False
            If Not ps.isAuthorized(ps.getPageName) Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If
        End If

        getOutageLog()
    End Sub

    Private Sub getOutageLog()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim dtOutageData As New DataTable()
            Dim dr As DataRow
            dtOutageData.Columns.Add(New DataColumn("date", System.Type.GetType("System.DateTime")))
            dtOutageData.Columns.Add(New DataColumn("zone", System.Type.GetType("System.String")))
            dtOutageData.Columns.Add(New DataColumn("description", System.Type.GetType("System.String")))
            dtOutageData.Columns.Add(New DataColumn("outage", System.Type.GetType("System.String")))
            dtOutageData.Columns.Add(New DataColumn("data", System.Type.GetType("System.String")))
            dtOutageData.Columns.Add(New DataColumn("logid", System.Type.GetType("System.String")))

            Dim outageInfo As String = ""            
            'Dim uniqueZones As new list(of String)
            Dim dtOutageReadData As DataTable = getOutageReadData
            For Each row As DataRow In dtOutageReadData.Rows
                outageInfo = row.Item("refer")
                outageInfo = outageInfo.Replace("[","")
                outageInfo = outageInfo.Replace("]","")
                If InStr(outageInfo, ";") > 0 Then
                    Dim outages As String() = outageInfo.Split(";") 'process multiple outages in read data
                    For Each outage As String In outages
                        If Not String.IsNullOrEmpty (outage) Then

                            If CInt(outage.Split("-")(1).Trim) >= 10 Then 'only process outage > 10 (same as production map)
                                dr = dtOutageData.NewRow()
                                dr("data") = row.Item("refer").ToString
                                dr("date") = row.Item("date")
                                dr("logid") = row.Item("id").ToString 
                                dr("zone") = outage.Split("-")(0).Trim
                                    
                                ' Lookup Description
                                Using connLookupZone As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                    connLookupZone.Open()
                                    Using cmdLookupZone As New SqlCommand("select esz_description.description from esz_description where esz_description.esz=@esz", connLookupZone)
                                        cmdLookupZone.Parameters.AddWithValue("@esz", outage.Split("-")(0).Trim)
                                        Using drLookupZone As SqlDataReader = cmdLookupZone.ExecuteReader
                                            If drLookupZone.HasRows Then
                                                While drLookupZone.Read
                                                    dr("description") = drLookupZone("description").ToString
                                                End While
                                            End If
                                        End Using 'drLookupZone
                                    End Using 'cmdLookupZone
                                    connLookupZone.Close()
                                End Using 'connLookupZone
                                ' End Lookup Description

                                dr("outage") = outage.Split("-")(1).Trim
                                dtOutageData.Rows.Add(dr)

                            End If 'outage > 10
                        End If
                    Next 'process multiple outage data in read (separated by ;)
                Else 'empty read (no outages)
                End If 'valid read data 
            Next row

            gvData.DataSource = dtOutageData
            gvData.DataBind
        Catch ex As Exception
            lblStatus.Text = Err.Description
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Public Function getOutageReadData As DataTable
        Dim dt As New DataTable
        Try
            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                conn.Open()
                Using da As New System.Data.SqlClient.SqlDataAdapter("select log.id,log.date,log.refer from log where log.web like '%oms successful%' and log.username='outagemap' order by log.date desc", conn)
                    If da.SelectCommand.Connection.State <> ConnectionState.Open Then da.SelectCommand.Connection.Open()
                    da.Fill(dt)
                End Using
                conn.Close()
            End Using 'conn
        Catch ex As Exception
        End Try
        Return dt
    End Function

    Public Function buildZones (ByVal zones As List(Of String), addZone As String) As List(Of String)
        Try
            Dim unique As Boolean = True 'determine if addZone should be included as unique
            For Each z As String In zones 'loop through current list of unique zones
                If z = addZone Then 'if addZone already in list
                    unique = False 'set unique to false
                End If
            Next
            If unique = True Then 'if addZone is new to the list (unique)
                zones.Add(addZone) 'add addzone to unique list
            End If
        Catch
        End Try
        Return zones 'return modified list of unique zones
    End Function

    Public Function getZoneData (ByVal outageDate As String, ByVal esz As String) As DataTable
        Dim dtOutageData As New DataTable
        Dim dr As DataRow
        dtOutageData.Columns.Add(New DataColumn("date", System.Type.GetType("System.DateTime")))
        dtOutageData.Columns.Add(New DataColumn("zone", System.Type.GetType("System.String")))
        dtOutageData.Columns.Add(New DataColumn("outage", System.Type.GetType("System.String")))
        Dim outageInfo As String = ""

        Try
            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                conn.Open()
                    Using cmdLookupDate As New SqlCommand("select log.id,log.date,log.refer from log where log.web like '%oms successful%' and log.username='outagemap' and log.date between '" & FormatDateTime(outageDate, DateFormat.ShortDate) & " 00:00' and '" & FormatDateTime(outageDate, DateFormat.ShortDate) & " 23:59' order by log.date", conn)
                        Using drLookupZone As SqlDataReader = cmdLookupDate.ExecuteReader
                            If drLookupZone.HasRows Then
                                While drLookupZone.Read

                                    dr = dtOutageData.NewRow() 'create new datarow
                                    dr("date") = drLookupZone("date") 'set date in daterow as log date
                                    dr("zone") = esz 'set zone to selected zone
                                    dr("outage") = "0" 'set outage value to zero

                                    outageInfo = drLookupZone("refer")
                                    outageInfo = outageInfo.Replace("[","")
                                    outageInfo = outageInfo.Replace("]","")
                                    If InStr(outageInfo, ";") > 0 Then
                                        Dim outages As String() = outageInfo.Split(";") 'process multiple outages in read data
                                        For Each outage As String In outages
                                            If Not String.IsNullOrEmpty (outage) Then
                                                If CInt(outage.Split("-")(1).Trim) >= 10 Then 'only process outage > 10 (same as production map)
                                                    If outage.Split("-")(0).Trim = esz Then 'only process selected zone
                                                        dr("outage") = outage.Split("-")(1).Trim 'override default row value of zero
                                                    Else
                                                    End If ' process only selected esz
                                                End If 'outage > 10
                                            End If
                                        Next 'process multiple outage data in read (separated by ;)
                                    Else 'empty read (no outages)
                                    End If 'valid read data 
                                    dtOutageData.Rows.Add(dr) 'add datarow

                                End While
                            End If
                        End Using 'drLookupZone
                    End Using 'cmdLookupZone
                conn.Close()
            End Using 'conn
        Catch ex As Exception
        End Try
        Return dtOutageData
    End Function

    Protected Sub gvData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvData.PageIndexChanging
        gvData.PageIndex = e.NewPageIndex
        gvData.DataBind
    End Sub

    Protected Sub gvData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvData.RowCommand
        Try

        Select Case e.CommandName
            Case "Select"            
                Dim dtOutageData As New datatable
                dtOutageData = getZoneData(e.CommandArgument.Split(";")(0).Trim, e.CommandArgument.Split(";")(1).Trim)

                'clear series for chart
                for each series in chart1.Series
                    series.Points.Clear()
                Next

                Chart1.Titles(0).Docking = Docking.Top
                Chart1.Titles(0).Alignment = Drawing.ContentAlignment.TopCenter
                Chart1.Titles(0).Text = e.CommandArgument.Split(";")(2).Trim
                Chart1.Titles(1).Text = FormatDateTime(e.CommandArgument.Split(";")(0).Trim, DateFormat.ShortDate)
	            Chart1.Titles(0).ShadowColor = Drawing.Color.FromArgb(32, 0, 0, 0)
	            Chart1.Titles(0).Font = New Drawing.Font("Trebuchet MS", 14F, Drawing.FontStyle.Bold)
	            Chart1.Titles(0).ShadowOffset = 3
	            Chart1.Titles(0).ForeColor = Drawing.Color.FromArgb(26, 59, 105) '1A3B69

	            Chart1.Legends(0).ShadowColor = Drawing.Color.FromArgb(32, 0, 0, 0)
	            Chart1.Legends(0).Font = New Drawing.Font("Trebuchet MS", 10F, Drawing.FontStyle.Bold)
	            Chart1.Legends(0).ShadowOffset = 3
	            Chart1.Legends(0).ForeColor = Drawing.Color.FromArgb(26, 59, 105)
                Chart1.Legends(0).TitleFont = New Drawing.Font("Trebuchet MS", 10F, Drawing.FontStyle.Bold)
                Chart1.Legends(0).Docking = Docking.Right
                Chart1.Legends(0).Alignment = Drawing.StringAlignment.Near

                Dim seriesDetail As New Series()
                seriesDetail.BorderWidth = 5
	            seriesDetail.Name = "outages"
                seriesDetail.XValueType = ChartValueType.Time
                seriesDetail.BorderColor = Drawing.Color.FromArgb(120, 64, 64, 64)
                seriesDetail.YValueType = ChartValueType.Int32 
	            seriesDetail.IsValueShownAsLabel = False
	            seriesDetail.Color = drawing.color.DodgerBlue
	            seriesDetail.ChartType = SeriesChartType.Line

                For Each row As DataRow In dtOutageData.Rows
                    seriesDetail.Points.AddXY(row.Item("date"), row.Item("outage"))
                Next row
	            seriesDetail.ChartArea = "ChartArea1"
	            chart1.Series.Add(seriesDetail)

                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#chartDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "chartDetailModalScript", sb.ToString(), False)
        End Select
        Catch
        End Try
    End Sub
End Class
