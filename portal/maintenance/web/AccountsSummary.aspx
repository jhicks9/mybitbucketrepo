<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="AccountsSummary.aspx.vb" Inherits="Maintenance_web_AccountsSummary" title="Accounts Summary" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Summary</a></li>
                <li><a href="#tab_b" data-toggle="pill">Account Lookup</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active">Total Accounts</td>
                            <td class="col-md-9"><asp:Label ID="lblAccountsTotal" runat="server" Text="" CssClass="form-control no-border" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Last</td>
                            <td class="col-md-9">
                                <asp:DropDownList ID="ddlInactive" runat="server" AutoPostBack="True" CssClass="form-control">
                                    <asp:ListItem Text="30 days" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="60 days" Value="2"></asp:ListItem>
                                    <asp:ListItem Selected="True" Text="90 days" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="1 year" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Inactive accounts</td>
                            <td class="col-md-9"><asp:Label ID="lblAccountsInactive" runat="server" Text="" CssClass="form-control no-border" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">&nbsp;</td>
                            <td class="col-md-9">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Accounts created</td>
                            <td class="col-md-9">
                                <asp:Label ID="lblAccountsCreated" runat="server" Text="" CssClass="form-control no-border"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Accounts accessed</td>
                            <td class="col-md-9">
                                <asp:Label ID="lblAccountsAccessed" runat="server" Text="" CssClass="form-control no-border" />
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">From</td>
                            <td class="col-md-9">
                                <asp:TextBox ID="txtBegDate" runat="server" CssClass="form-control" />
                                <asp:CompareValidator id="begdateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtBegDate" ErrorMessage="mm/dd/yyyy" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">To</td>
                            <td class="col-md-9">
                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" />
                                <asp:CompareValidator id="enddateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtEndDate" ErrorMessage="mm/dd/yyyy" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"></td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CausesValidation="true" UseSubmitBehavior="False" CssClass="btn btn-default" />
                            </td>
                        </tr>
                    </table>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="pnlAccounts" runat="server">
                            <ContentTemplate>

                                <table class="table table-condensed table-bordered">
                                    <tr>                                    
                                        <td class="field-label col-xs-3 active">Account</td>
                                        <td class="col-md-9 form-inline">
                                            <div class="form-group">
                                                <label class="sr-only" for="txtAN">Account Number</label>
                                                <asp:TextBox ID="txtAN" runat="server" Text="" MaxLength="6" placeholder="account" CssClass="form-control"/>-
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="txtBP">Billing Package</label>
                                                <asp:TextBox ID="txtBP" runat="server" Text="" MaxLength="2" placeholder="billing package" CssClass="form-control" />-
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="txtCD">Check Digit</label>
                                                <asp:TextBox ID="txtCD" runat="server" Text="" MaxLength="1" placeholder="check digit" CssClass="form-control" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field-label col-xs-3 active">Username</td>
                                        <td class="col-md-9"><asp:TextBox ID="txtName" runat="server" Text="" MaxLength="40" CssClass="form-control" />
                                    </tr>
                                    <tr>
                                        <td class="field-label col-xs-3 active">Email</td>
                                        <td class="col-md-9">
                                            <asp:TextBox ID="txtEmail" runat="server" Text="" MaxLength="40" CssClass="form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field-label col-xs-3 active"></td>
                                        <td class="col-md-9">
                                            <asp:Button ID="btnLookup" runat="server" Text="Submit" CausesValidation="false" UseSubmitBehavior="False" CssClass="btn btn-default" />
                                        </td>
                                    </tr>
                                </table>
                        
                                <asp:GridView ID="gvAccounts" runat="server" DataSourceID="dsAccounts" AllowPaging="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="UserName" SortExpression="" HeaderText="UserName" />
                                        <asp:BoundField DataField="PropertyValuesString" SortExpression="" HeaderText="Account" />
                                        <asp:BoundField DataField="Email" SortExpression="" HeaderText="Email" />
                                        <asp:CheckBoxField DataField="IsLockedOut" SortExpression="" HeaderText="Locked" />
                                        <asp:BoundField DataField="CreateDate" SortExpression="" HeaderText="Created" />
                                        <asp:BoundField DataField="LastPasswordChangedDate" SortExpression="" HeaderText="Password Changed" />
                                        <asp:BoundField DataField="LastLoginDate" SortExpression="" HeaderText="Last Login" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate></EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->
            </div><!-- tab-content -->
        </div><!-- tab container -->
    </asp:Panel><!-- pnlAuthorized -->
    
    <asp:SqlDataSource ID="dsAccounts" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsAccountsInactive" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT count(*) as accounts_inactive FROM aspnet_Membership WHERE DATEDIFF(mm, LastLoginDate, GETUTCDATE()) > @months">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlInactive" Type="Int32" Name="months" PropertyName="SelectedValue"/>
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsAccountsTotal" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT count(*) as accounts_total FROM aspnet_Membership">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsAccountsAccessed" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT count(*) as accounts_accessed, CONVERT(VARCHAR(10),DATEADD(mm, - 1, GETDATE()),101) as begdate, CONVERT(VARCHAR(10),DATEADD(day, 0, getutcdate()),101) as enddate FROM aspnet_Membership WHERE LastLoginDate >= DATEADD(mm, - 1, GETDATE()) AND LastLoginDate <= DATEADD(day, 0, getutcdate())">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsAccountsCreated" runat="server" ConnectionString="<%$ ConnectionStrings:empiremembershipConnectionString %>"
        SelectCommand="SELECT count(*) as accounts_created FROM aspnet_Membership WHERE CreateDate >= @begDate AND CreateDate <= @endDate">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtBegDate" Type="String" Name="begDate" PropertyName="Text"/>
            <asp:ControlParameter ControlID="txtEndDate" Type="String" Name="endDate" PropertyName="Text"/>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {
                var txtBegDate = $("#<%=txtBegDate.ClientId %>");
                txtBegDate.parent().css("position", "relative");
                txtBegDate.datetimepicker({format: 'MM/DD/YYYY'});
                var txtEndDate = $("#<%=txtEndDate.ClientId %>");
                txtEndDate.parent().css("position", "relative");
                txtEndDate.datetimepicker({format: 'MM/DD/YYYY'});
            });
        </script>
</asp:Content>