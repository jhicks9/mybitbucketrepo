﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Partial Class maintenance_web_modules
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isAdmin(ps.getPageName) Then
            pnlAuthorized.Visible = True
            pnlNotAuthorized.Visible = False
            pnlAdministration.Visible = True
        Else
            pnlAdministration.Visible = False
            If Not ps.isAuthorized(ps.getPageName) Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If
        End If
    End Sub

    '============================== Center Modules ===================================================================================
    
    Protected Sub gvCenterModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCenterModules.RowCommand
        Select Case e.CommandName
            Case "Select"
            Case "AddRecord"
                pnlCenter.Visible = false
                pnlCenterDetail.Visible = true
        End Select
    End Sub

    Protected Sub gvCenterModules_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvCenterModules.RowUpdating
        Try
            'Dim index As Integer = Convert.ToInt32(e.RowIndex)
            Dim row As GridViewRow = gvCenterModules.Rows(e.RowIndex)
            Dim lbl As Label = CType(row.FindControl("lblFileValidation"), Label)
            Dim UploadedFile As FileUpload = CType(row.FindControl("UpdateUploadedFile"), FileUpload)

            'Make sure a file has been uploaded
            'If UploadedFile.PostedFile Is Nothing OrElse String.IsNullOrEmpty(UploadedFile.PostedFile.FileName) OrElse UploadedFile.PostedFile.InputStream Is Nothing Then
            '    lbl.Text = "File Required"
            '    lbl.Visible = True
            '    e.Cancel = True
            '    Exit Sub
            'End If

            'If file is uploaded make sure its JPG or GIF or PNG file
            If UploadedFile.PostedFile Is Nothing OrElse String.IsNullOrEmpty(UploadedFile.PostedFile.FileName) OrElse UploadedFile.PostedFile.InputStream Is Nothing Then
                sqlDSCenterModules.UpdateCommand = "update modules SET module_name=@module_name,module_visible=@module_visible,module_link=@module_link,module_order=@module_order where module_id = @module_id"
            Else
                Dim extension As String = System.IO.Path.GetExtension(UploadedFile.PostedFile.FileName).ToLower()
                Dim MIMEType As String = Nothing
                Select Case extension
                    Case ".jpg", ".jpeg", ".jpe"
                        MIMEType = "image/jpeg"
                    Case ".png"
                        MIMEType = "image/png"
                    Case ".gif"
                        MIMEType = "image/gif"
                    Case Else
                        lbl.Text = "Invalid file type uploaded"
                        lbl.Visible = True
                        e.Cancel = True
                        Exit Sub
                End Select

                ' Make sure resolution is less than 800px wide and equal to 300px tall
                If Not ValidateUserImageDimensions(UploadedFile.PostedFile) Then
                    lbl.Text = "Height must = 300px and width < 800px."
                    lbl.Visible = True
                    e.Cancel = True
                    Exit Sub
                End If

                'Specify the values for MIMEType and Image
                'e.Values("MIMEType") = MIMEType
                UploadedFile.PostedFile.InputStream.Position = 0
                Dim imageBytes(UploadedFile.PostedFile.InputStream.Length) As Byte
                UploadedFile.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                e.NewValues("imagedata") = imageBytes
                e.NewValues("module_filename") = System.IO.Path.GetFileName(UploadedFile.PostedFile.FileName)
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbCenterModuleCancel_Click(sender As Object, e As EventArgs) Handles lbCenterModuleCancel.Click
        pnlCenterDetail.Visible = False
        pnlCenter.Visible = true
    End Sub

    Protected Sub lbCenterModuleInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCenterModuleInsert.Click
        Try
            If fluCenterModuleImage.PostedFile Is Nothing OrElse String.IsNullOrEmpty(fluCenterModuleImage.PostedFile.FileName) OrElse fluCenterModuleImage.PostedFile.InputStream Is Nothing Then
                lblFileValidation.Text = "Image File Required"
                lblFileValidation.Visible = True
                Exit Sub
            Else
                Dim extension As String = System.IO.Path.GetExtension(fluCenterModuleImage.PostedFile.FileName).ToLower()
                Dim MIMEType As String = Nothing
                Select Case extension
                    Case ".jpg", ".jpeg", ".jpe"
                        MIMEType = "image/jpeg"
                    Case ".png"
                        MIMEType = "image/png"
                    Case ".gif"
                        MIMEType = "image/gif"
                    Case Else
                        lblFileValidation.Text = "Invalid file type uploaded"
                        lblFileValidation.Visible = True
                        Exit Sub
                End Select

                ' Make sure resolution is less than 800px wide and equal to 300px tall
                If Not ValidateUserImageDimensions(fluCenterModuleImage.PostedFile) Then
                    lblFileValidation.Text = "Height must = 300px and width < 800px."
                    'e.Cancel = True
                    Exit Sub
                End If

                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Dim query As String = "insert into modules (module_name,module_filename,module_visible,module_order,module_page,module_columnonpage,module_image,module_link) values (@module_name,@module_filename,@module_visible,@module_order,@module_page,@module_columnonpage,@imagedata,@module_link)"
                Dim cmd As New SqlCommand(query, conn)
                cmd.Parameters.AddWithValue("@module_name", txtCenterModuleName.Text)
                cmd.Parameters.AddWithValue("@module_filename", System.IO.Path.GetFileName(fluCenterModuleImage.PostedFile.FileName))
                cmd.Parameters.AddWithValue("@module_visible", ckbCenterModuleVisible.Checked)
                cmd.Parameters.AddWithValue("@module_order", txtCenterModuleOrder.Text)
                cmd.Parameters.AddWithValue("@module_link", txtCenterModuleLink.Text)
                cmd.Parameters.AddWithValue("@module_page", "Landing")
                cmd.Parameters.AddWithValue("@module_columnonpage", "Middle")

                'Load FileUpload's InputStream into Byte array
                fluCenterModuleImage.PostedFile.InputStream.Position = 0
                Dim imageBytes(fluCenterModuleImage.PostedFile.InputStream.Length) As Byte
                fluCenterModuleImage.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                cmd.Parameters.AddWithValue("@imagedata", imageBytes)

                conn.Open()
                cmd.ExecuteNonQuery()
                conn.Close()
                cmd.Dispose()
                conn.Dispose()

                txtCenterModuleName.Text = ""
                ckbCenterModuleVisible.Checked = False
                txtCenterModuleLink.Text = ""
                lblFileValidation.Text = ""

                sqlDSCenterModules.DataBind()
                gvCenterModules.DataBind()
                pnlCenterDetail.Visible = False
                pnlCenter.Visible = true
            End If
        Catch ex As Exception
        End Try
    End Sub

    '============================== End Center Modules ===============================================================================
    '============================== Static Modules ===================================================================================

    Protected Sub gvStaticModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStaticModules.RowCommand
        Select Case e.CommandName
            Case "Select"
            Case "AddRecord"
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#staticDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "staticDetailModalScript", sb.ToString(), False)
        End Select
    End Sub

    Protected Sub lbInsertStaticModuleInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInsertStaticModuleInsert.Click
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim query As String = "insert into modules (module_name,module_visible,module_page,module_columnonpage,module_link) values (@module_name,@module_visible,@module_page,@module_columnonpage,@module_link)"
            Dim cmd As New SqlCommand(query, conn)
            cmd.Parameters.AddWithValue("@module_name", txtInsertStaticModuleName.Text)
            cmd.Parameters.AddWithValue("@module_visible", ckbInsertStaticModuleVisible.Checked)
            cmd.Parameters.AddWithValue("@module_link", txtInsertStaticModuleLink.Text)
            cmd.Parameters.AddWithValue("@module_page", "Landing")
            cmd.Parameters.AddWithValue("@module_columnonpage", "Static")

            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            cmd.Dispose()
            conn.Dispose()

            txtInsertStaticModuleName.Text = ""
            ckbInsertStaticModuleVisible.Checked = False
            txtInsertStaticModuleLink.Text = ""
            sqlDSStaticModules.DataBind()
            gvStaticModules.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#staticDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "staticDetailModalScript", sb.ToString(), False)
        Catch ex As Exception
            lblInsertStaticValidation.Text = ex.Message
        End Try
    End Sub

    '============================== End Static Modules ===============================================================================

    Protected Sub lbAccountAdd_Click(sender As Object, e As EventArgs) Handles lbAccountAdd.Click
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#AddAccountDetail').modal('show');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddAccountDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub CreateUserWizard1_Cancel(ByVal sender As Object, ByVal e As System.EventArgs) Handles CreateUserWizard1.CancelButtonClick
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#AddAccountDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddAccountDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub CreateUserWizard1_CreatedUser(ByVal sender As Object, ByVal e As System.EventArgs) Handles CreateUserWizard1.CreatedUser
        Try
            Dim arUserName(0) As String
            arUserName(0) = CreateUserWizard1.UserName
            Dim arRoleName(0) As String
            arRoleName(0) = "Customer"
            Roles.Providers("empireRoleSqlProvider").AddUsersToRoles(arUserName, arRoleName)
            arRoleName(0) = "Admin"
            Roles.Providers("empireRoleSqlProvider").AddUsersToRoles(arUserName, arRoleName)
            gvAccounts.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#AddAccountDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddAccountDetailModalScript", sb.ToString(), False)
        Catch er As MembershipCreateUserException
            lblAccountAddStatus.Text = er.Message
        End Try
    End Sub

    Protected Sub lbAccountDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAccountDelete.Click
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#DeleteAccountDetail').modal('show');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DeleteAccountDetailModalScript", sb.ToString(), False)
    End Sub
    Protected Sub lbDeleteCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDeleteCancel.Click
        lblDUserID.Text = ""
        txtDUserName.Text = ""
        lbDeleteAccount.Text = "Delete user"
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#DeleteAccountDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DeleteAccountDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub lbDeleteAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDeleteAccount.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Try
            If lblDUserID.Text = "" Then
                Dim cmd As New SqlCommand("select aspnet_Users.UserID from aspnet_Users where aspnet_Users.UserName = '" & txtDUserName.Text & "'", conn)
                conn.Open()
                Dim dr As SqlDataReader
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        lblDUserID.Text = dr("UserID").ToString
                        lbDeleteAccount.Text = "Delete " & txtDUserName.Text
                    End While
                Else
                End If
                dr.Close()
                conn.Close()                
            Else
                DeleteAccount(lblDUserID.Text)
                lblDeleteStatus.Text = ""
                gvAccounts.DataBind()
                lblDUserID.Text = ""
                txtDUserName.Text = ""
                lbDeleteAccount.Text = "Delete user"
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#DeleteAccountDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DeleteAccountDetailModalScript", sb.ToString(), False)
            End If
        Catch ex As Exception
            lblDeleteStatus.Text = ex.Message.ToString
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Public Sub DeleteAccount(ByVal id As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Try
            'deletes userid entry from aspnet_profile, aspnet_usersinroles, aspnet_membership, aspnet_users
            Dim query1 As String = "delete from aspnet_Profile WHERE UserID = @id"
            Dim query2 As String = "delete from aspnet_UsersInRoles WHERE UserID = @id"
            Dim query3 As String = "delete from aspnet_membership where userID = @id"
            Dim query4 As String = "delete from aspnet_users where userID = @id"
            conn.Open()
            Dim cmd As New SqlCommand(query1, conn)
            cmd.Parameters.AddWithValue("@id", id)
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            cmd.ExecuteNonQuery()
            cmd.CommandText = query3
            cmd.ExecuteNonQuery()
            cmd.CommandText = query4
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    Protected Sub lbAccountEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAccountEdit.Click
        txtEUserName.Enabled = True
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#EditAccountDetail').modal('show');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "EditAccountDetailModalScript", sb.ToString(), False)
    End Sub
    Protected Sub lbEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEditCancel.Click
        txtEUserName.Text = ""
        BindRoles(True)
        lbEditLookup.Text = "Lookup Account"
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#EditAccountDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "EditAccountDetailModalScript", sb.ToString(), False)
    End Sub

    'Bind Roles to Gridview
    Protected Sub BindRoles(Optional ByVal empty As Boolean = False)
        If empty Then
            gvRoles.DataSource = Nothing
        Else
            gvRoles.DataSource = Roles.Providers("empireRoleSqlProvider").GetAllRoles()
        End If
        gvRoles.DataBind()
    End Sub

    Protected Sub lbEditAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEditLookup.Click
        Try
            Dim userRoles As String() = Roles.Providers("empireRoleSqlProvider").GetRolesForUser(txtEUserName.Text)
            Dim arUserName(0) As String
            Dim arRoleName(0) As String
            Dim index As Integer = -1
            arUserName(0) = txtEUserName.Text

            If lbEditLookup.Text = "Lookup Account" Then  'display current roles
                txtEUserName.Enabled = False
                BindRoles()
                For Each gvrow As GridViewRow In gvRoles.Rows
                    Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkRole"), CheckBox)
                    Dim roleName As Label = DirectCast(gvrow.FindControl("lblRole"), Label)
                    index = Array.IndexOf(userRoles, roleName.Text)
                    If index > -1 Then
                        chk.Checked = True
                    Else
                        chk.Checked = False
                    End If
                Next
                lbEditLookup.Text = "Update Account"
            Else  'make updates to roles based on selection
                For Each gvrow As GridViewRow In gvRoles.Rows
                    Dim roleCheck As CheckBox = DirectCast(gvrow.FindControl("chkRole"), CheckBox)
                    Dim roleName As Label = DirectCast(gvrow.FindControl("lblRole"), Label)
                    'roleName = roleLabel.Text
                    If roleCheck.Checked Then
                        index = Array.IndexOf(userRoles, roleName.Text)
                        If index = -1 Then
                            arRoleName(0) = roleName.Text
                            Roles.Providers("empireRoleSqlProvider").AddUsersToRoles(arUserName, arRoleName)  'add user to role
                        End If
                    Else
                        index = Array.IndexOf(userRoles, roleName.Text)
                        If index > -1 Then
                            arRoleName(0) = roleName.Text
                            Roles.Providers("empireRoleSqlProvider").RemoveUsersFromRoles(arUserName, arRoleName)
                        End If
                    End If
                Next
                txtEUserName.Text = ""
                BindRoles(True)
                lbEditLookup.Text = "Lookup Account"
                gvAccounts.DataBind()
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#EditAccountDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "EditAccountDetailModalScript", sb.ToString(), False)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Function ValidateUserImageDimensions(ByVal file As HttpPostedFile) As Boolean
        Dim _MaxWidth As Integer = 800
        Dim _MaxHeight As Integer = 300
        Dim validimagedimension As Boolean = False
        Dim bitmap As System.Drawing.Bitmap = New System.Drawing.Bitmap(File.InputStream, False)

        If (bitmap.Width <= _MaxWidth And bitmap.Height = _MaxHeight) Then
            validimagedimension = True
        End If
        Return validimagedimension
    End Function
End Class
