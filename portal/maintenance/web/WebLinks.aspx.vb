Imports System.Data.SqlClient

Partial Class Maintenance_web_WebLinks
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub gvLinks_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLinks.RowCommand
        Select Case e.CommandName
            Case "Select"
                LinksDetails.HeaderText = "Edit"
                LinksDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#linkDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "linkDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvLinks.SelectedIndex = -1
                LinksDetails.HeaderText = "Add"
                LinksDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#linkDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "linkDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub
    Protected Sub LinksDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles LinksDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvLinks.SelectedIndex = -1
        lblLinksStatus.Text = ""
    End Sub

    Function GetSelectedIndex(ByVal state As Boolean) As Integer
        Dim i As Integer = 0
        If LinksDetails.CurrentMode = DetailsViewMode.Insert Then
            i = 1  'default to true for all new entries
        Else
            Select Case state
                Case True
                    i = 1
                Case False
                    i = 0
            End Select
        End If

        Return i
    End Function

    Protected Sub LinksDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles LinksDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblLinksStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvLinks.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#linkDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "linkDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub LinksDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles LinksDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(LinksDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvLinks.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#linkDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "linkDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub LinksDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles LinksDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(LinksDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvLinks.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#linkDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "linkDetailModalScript", sb.ToString(), False)
        End If
    End Sub
End Class
