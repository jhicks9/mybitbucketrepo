Imports System.Data.SqlClient
Imports <xmlns="http://schemas.microsoft.com/search/local/ws/rest/v1">
Imports System.Globalization

Partial Class Maintenance_web_PaymentLocations
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Function GetSelectedIndex(ByVal selectedState As String, ByVal newState As String) As Integer
        Dim i As Integer = 0
        Select Case selectedState
            Case "AR"
                i = 0
            Case "KS"
                i = 1
            Case "MO"
                i = 2
            Case "OK"
                i = 3
            Case Else
                Select Case newState
                Case "AR"
                    i = 0
                Case "KS"
                    i = 1
                Case "MO"
                    i = 2
                Case "OK"
                    i = 3
                End Select
        End Select
        Return i
    End Function

    Function IsEven(ByVal numberToCheck As integer) As Boolean
        Dim even As Boolean = false
        If numberToCheck Mod 2 = 0 Then
            even = True
        Else
            even = False
        End if
        Return even
    End Function 

    Function GoogleGetGeocodingSearchResults(ByVal address As String) As XElement
        Dim url = String.Format("http://maps.google.com/maps/api/geocode/xml?address={0}&sensor=false", HttpContext.Current.Server.UrlEncode(address))
        Dim results As XElement = XElement.Load(url)
        Dim status = results.Element("status").Value
        If status <> "OK" AndAlso status <> "ZERO_RESULTS" Then 'Check the status
            Throw New ApplicationException("error: " & status) 'bad request
        End If
        Return results
    End Function

    Function BingGetGeocodingSearchResults(ByVal address As String) As XElement
        Dim url as string = String.Format("http://dev.virtualearth.net/REST/v1/Locations?q={0}&o=xml&key=AkPdw_MFymEO655WUG8sdWPRE8pnFPDVM0hS0DZOdHrdtn4_-_Z1ReWxKgYSGuzs", HttpContext.Current.Server.UrlEncode(address))
        Dim results As XElement = XElement.Load(url)
        Dim statusCode As String = results.<StatusCode>.Value
        Dim statusDescription = results.<StatusDescription>.Value
        If statusCode <> "200" AndAlso statusDescription <> "OK" Then 'Check the status
            Throw New ApplicationException("error: " & statusCode & "-" & statusDescription) 'bad request
        End If
        Return results
    End Function

    Public Sub bindGridViews()
        gvArkansas.DataBind()
        gvKansas.DataBind()
        gvMissouri.DataBind()
        gvOklahoma.DataBind()
        gvGas.DataBind()
    End Sub

    '============================== Arkansas Maintenance ===========================================================================
    Protected Sub gvArkansas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvArkansas.RowCommand
        Select Case e.CommandName
            Case "Select"
                ARDetails.HeaderText = "Edit Arkansas Location"
                ARDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ARDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ARDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvArkansas.SelectedIndex = -1
                ARDetails.HeaderText = "Add Arkansas Location"
                ARDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ARDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ARDetailModalScript", sb.ToString(), False)
            Case Else
                ARDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub ARDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles ARDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvArkansas.SelectedIndex = -1
        lblARStatus.Text = ""
    End Sub

    Protected Sub ARDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles ARDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblARStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#ARDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ARDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub ARDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles ARDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(ARDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#ARDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ARDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub ARDetails_ItemInserting(sender As Object, e As DetailsViewInsertEventArgs) Handles ARDetails.ItemInserting
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.Values("address").ToString & ", " & e.Values("city") & ", " & e.Values("state")

        If not IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSAR2.InsertParameters("latitude").DefaultValue = lat
        sqlDSAR2.InsertParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub ARDetails_ItemUpdating(sender As Object, e As DetailsViewUpdateEventArgs) Handles ARDetails.ItemUpdating
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.NewValues("address").ToString & ", " & e.newValues("city") & ", " & e.newValues("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSAR2.UpdateParameters("latitude").DefaultValue = lat
        sqlDSAR2.UpdateParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub ARDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles ARDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(ARDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#ARDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ARDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    '============================== End Arkansas Maintenance =======================================================================

    '============================== Missouri Maintenance ===========================================================================
    Protected Sub gvMissouri_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMissouri.RowCommand
        Select Case e.CommandName
            Case "Select"
                MODetails.HeaderText = "Edit Missouri Location"
                MODetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#MODetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "MODetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvMissouri.SelectedIndex = -1
                MODetails.HeaderText = "Add Missouri Location"
                MODetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#MODetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "MODetailModalScript", sb.ToString(), False)
            Case Else
                MODetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub MODetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles MODetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvMissouri.SelectedIndex = -1
        lblMOStatus.Text = ""
    End Sub

    Protected Sub MODetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles MODetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblMOStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#MODetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "MODetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub MODetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles MODetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(MODetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#MODetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "MODetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub MODetails_ItemInserting(sender As Object, e As DetailsViewInsertEventArgs) Handles MODetails.ItemInserting
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.Values("address").ToString & ", " & e.Values("city") & ", " & e.Values("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSMO2.InsertParameters("latitude").DefaultValue = lat
        sqlDSMO2.InsertParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub MODetails_ItemUpdating(sender As Object, e As DetailsViewUpdateEventArgs) Handles MODetails.ItemUpdating
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.newValues("address").ToString & ", " & e.newValues("city") & ", " & e.newValues("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSMO2.UpdateParameters("latitude").DefaultValue = lat
        sqlDSMO2.UpdateParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub MODetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles MODetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(MODetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#MODetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "MODetailModalScript", sb.ToString(), False)
        End If
    End Sub
    '============================== End Missouri Maintenance =======================================================================

    '============================== Kansas Maintenance =============================================================================
    Protected Sub gvKansas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvKansas.RowCommand
        Select Case e.CommandName
            Case "Select"
                KSDetails.HeaderText = "Edit Kansas Location"
                KSDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#KSDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "KSDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvKansas.SelectedIndex = -1
                KSDetails.HeaderText = "Add Kansas Location"
                KSDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#KSDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "KSDetailModalScript", sb.ToString(), False)
            Case Else
                KSDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub KSDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles KSDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvKansas.SelectedIndex = -1
        lblKSStatus.Text = ""
    End Sub

    Protected Sub KSDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles KSDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblKSStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#KSDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "KSDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub KSDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles KSDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(KSDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#KSDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "KSDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub KSDetails_ItemInserting(sender As Object, e As DetailsViewInsertEventArgs) Handles KSDetails.ItemInserting
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.Values("address").ToString & ", " & e.Values("city") & ", " & e.Values("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSKS2.InsertParameters("latitude").DefaultValue = lat
        sqlDSKS2.InsertParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub KSDetails_ItemUpdating(sender As Object, e As DetailsViewUpdateEventArgs) Handles KSDetails.ItemUpdating
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.newValues("address").ToString & ", " & e.newValues("city") & ", " & e.newValues("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSKS2.UpdateParameters("latitude").DefaultValue = lat
        sqlDSKS2.UpdateParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub KSDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles KSDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(KSDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#KSDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "KSDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    '============================== End Kansas Maintenance =========================================================================

    '============================== Oklahoma Maintenance ===========================================================================
    Protected Sub gvOKlahoma_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOKlahoma.RowCommand
        Select Case e.CommandName
            Case "Select"
                OKDetails.HeaderText = "Edit Oklahoma Location"
                OKDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#OKDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OKDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvOKlahoma.SelectedIndex = -1
                OKDetails.HeaderText = "Add Oklahoma Location"
                OKDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#OKDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OKDetailModalScript", sb.ToString(), False)
            Case Else
                OKDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub OKDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles OKDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvOKlahoma.SelectedIndex = -1
        lblOKStatus.Text = ""
    End Sub

    Protected Sub OKDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles OKDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblOKStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#OKDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OKDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub OKDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles OKDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(OKDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#OKDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OKDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub OKDetails_ItemInserting(sender As Object, e As DetailsViewInsertEventArgs) Handles OKDetails.ItemInserting
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.Values("address").ToString & ", " & e.Values("city") & ", " & e.Values("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSOK2.InsertParameters("latitude").DefaultValue = lat
        sqlDSOK2.InsertParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub OKDetails_ItemUpdating(sender As Object, e As DetailsViewUpdateEventArgs) Handles OKDetails.ItemUpdating
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.newValues("address").ToString & ", " & e.newValues("city") & ", " & e.newValues("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSOK2.UpdateParameters("latitude").DefaultValue = lat
        sqlDSOK2.UpdateParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub OKDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles OKDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(OKDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#OKDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OKDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    '============================== End Oklahoma Maintenance =======================================================================

    '============================== Gas Maintenance ================================================================================
    Protected Sub gvGas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGas.RowCommand
        Select Case e.CommandName
            Case "Select"
                GASDetails.HeaderText = "Edit Gas Location"
                GASDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#GASDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GASDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvGas.SelectedIndex = -1
                GASDetails.HeaderText = "Add Gas Location"
                GASDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#GASDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GASDetailModalScript", sb.ToString(), False)
            Case Else
                GASDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub GASDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles GASDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvGas.SelectedIndex = -1
        lblGASStatus.Text = ""
    End Sub

    Protected Sub GASDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles GASDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblGASStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#GASDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GASDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub GASDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles GASDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(GASDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#GASDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GASDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub GASDetails_ItemInserting(sender As Object, e As DetailsViewInsertEventArgs) Handles GASDetails.ItemInserting
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.Values("address").ToString & ", " & e.Values("city") & ", " & e.Values("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSGAS2.InsertParameters("latitude").DefaultValue = lat
        sqlDSGAS2.InsertParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub GASDetails_ItemUpdating(sender As Object, e As DetailsViewUpdateEventArgs) Handles GASDetails.ItemUpdating
        Dim currentDate As DateTime = DateTime.Now
        Dim lat as Decimal = 0.0
        Dim lng as Decimal = 0.0
        Dim address As String = e.newValues("address").ToString & ", " & e.newValues("city") & ", " & e.newValues("state")

        If IsEven(currentDate.Day) Then 'use Bing
                Dim bingResults = BingGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Latitude>.Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(bingResults.<ResourceSets>.<ResourceSet>.<Resources>.<Location>.<Point>.<Longitude>.Value, NumberFormatInfo.InvariantInfo)
            Else 'Use Google
                Dim googleResults = GoogleGetGeocodingSearchResults(address) 'read latitude and longitude
                lat = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lat").Value, NumberFormatInfo.InvariantInfo)
                lng = Convert.ToDecimal(googleResults.Element("result").Element("geometry").Element("location").Element("lng").Value, NumberFormatInfo.InvariantInfo)
            End If
        sqlDSGAS2.UpdateParameters("latitude").DefaultValue = lat
        sqlDSGAS2.UpdateParameters("longitude").DefaultValue = lng
    End Sub

    Protected Sub GASDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles GASDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(GASDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            bindGridViews()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#GASDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GASDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    '============================== End Gas Maintenance ===========================================================================-

End Class