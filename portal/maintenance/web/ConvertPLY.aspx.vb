﻿Imports System.IO
Imports System.Data
Imports System.IO.File
Imports System.Data.SqlClient

Partial Class Maintenance_web_ConvertPLY
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    '============================== Beg Description Maintenance ====================================================================
    Protected Sub gvESZDesc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvESZDesc.RowCommand
        Select Case e.CommandName
            Case "Select"
                dvESZDesc.HeaderText = "Edit"
                dvESZDesc.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ESZDescDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ESZDescDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                dvESZDesc.HeaderText = "Add"
                dvESZDesc.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#ESZDescDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ESZDescDetailModalScript", sb.ToString(), False)
            Case Else
                dvESZDesc.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub dvESZDesc_ItemDeleted(sender As Object, e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles dvESZDesc.ItemDeleted
        gvESZDesc.DataBind()
        gvNoESZ.DataBind()
        gvNoDesc.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#ESZDescDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ESZDescDetailModalScript", sb.ToString(), False)
    End Sub
    Protected Sub dvESZDesc_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvESZDesc.ItemInserted
        gvESZDesc.DataBind()
        gvNoESZ.DataBind()
        gvNoDesc.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#ESZDescDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ESZDescDetailModalScript", sb.ToString(), False)
    End Sub
    Protected Sub dvESZDesc_ItemUpdated(sender As Object, e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvESZDesc.ItemUpdated
        gvESZDesc.DataBind()
        gvNoESZ.DataBind()
        gvNoDesc.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#ESZDescDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "ESZDescDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub sqlDSESZDesc_Selected(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqlDSESZDesc.Selected
        Try
            Dim exceptMessage As String = e.Exception.Message
            Dim Rec As Integer = e.AffectedRows
            If exceptMessage = "Invalid object name 'dbo.esz_description'." Then  'table doesnt exist
                createTable()
            End If

            If Rec = 0 Then
            End If
        Catch ex As Exception
        End Try
    End Sub
    '============================== End Description Maintenance ====================================================================

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try            
            If Not IsPostBack Then
                cmdProcess.Enabled = False

                If ps.isAdmin(ps.getPageName) Then
                    pnlAuthorized.Visible = True
                    pnlNotAuthorized.Visible = False
                    pnlAdministration.Visible = True
                Else
                    pnlAdministration.Visible = False
                    If Not ps.isAuthorized(ps.getPageName) Then
                        pnlAuthorized.Visible = False
                        pnlNotAuthorized.Visible = True
                    End If
                End If
            End If
        Catch
        End Try
    End Sub

    Private Sub createTable()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            cmd.CommandText = "CREATE TABLE dbo.esz_description(id int IDENTITY(1,1) NOT NULL,esz varchar(12) not null,description varchar(50) not null, CONSTRAINT pk_esz_description PRIMARY KEY CLUSTERED (id ASC))"
            conn.Open()
            cmd.ExecuteNonQuery()  'create the table
            conn.Close()
            gvESZDesc.DataSourceID = "sqlDSESZDesc"
            gvESZDesc.DataBind()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub cmdLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLoad.Click
        Try
            If FileUpload1.PostedFile Is Nothing OrElse String.IsNullOrEmpty(FileUpload1.PostedFile.FileName) OrElse FileUpload1.PostedFile.InputStream Is Nothing Then
                lblError.Text = "File is required"
            Else
                'save the file to the server
                If File.Exists(System.IO.Path.GetTempPath() & FileUpload1.FileName) Then
                    File.Delete(System.IO.Path.GetTempPath() & FileUpload1.FileName)  'remove previous file if exists
                End If
                FileUpload1.PostedFile.SaveAs(System.IO.Path.GetTempPath() & FileUpload1.FileName)
                lblError.Text = "File Saved as " & System.IO.Path.GetTempPath() & FileUpload1.FileName
                hdnFileName.Value = System.IO.Path.GetTempPath() & FileUpload1.FileName  'save uploaded filename to process
                cmdProcess.Enabled = True  'enable button
            End If
        Catch
        End Try
    End Sub

    Protected Sub cmdProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdProcess.Click
        convertUTM()
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Dim lineinfo As String = ""
        Dim i As Integer = 0
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim linecount As Integer = txtCoord.Text.ToString.Split(vbCrLf).Length()
            If (linecount > 0) And (Len(txtCoord.Text) > 0) Then  'check for any data
                Dim cmd = New SqlCommand("delete from esz_boundary", conn)
                conn.Open()
                cmd.ExecuteNonQuery()  'remove all preview rows
                For i = 0 To linecount - 1
                    If validateLine(i) Then  'ensure a valid line of data
                        lineinfo = txtCoord.Text.Split(vbCrLf)(i).Trim & vbCrLf
                        cmd.CommandText = "insert into esz_boundary (esz,num,latitude,longitude) values ('" & lineinfo.Split(",")(0).Trim & "','" & lineinfo.Split(",")(1).Trim & "','" & lineinfo.Split(",")(2).Trim & "','" & lineinfo.Split(",")(3).Trim & "')"
                        cmd.ExecuteNonQuery()
                    End If
                Next
                If File.Exists(System.IO.Path.GetTempPath() & hdnFileName.Value) Then
                    File.Delete(System.IO.Path.GetTempPath() & hdnFileName.Value)  'remove uploaded ply file when finished
                End If
                txtCoord.Text = ""
                lblStatus.Text = "Update Complete"
            Else
                lblStatus.Text = "Data not found"
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            lblStatus.Text = ex.Message.ToString & " [line# " & i & "]"
        End Try
    End Sub

    Public Sub convertUTM()
        Try
            Dim filename As String = hdnFileName.Value

            Dim coords() As String
            Dim inputLine As String
            Dim sr As IO.StreamReader
            Dim esz As String = ""
            Dim num As String = ""
            Dim latlon() As String

            sr = OpenText(filename)
            While sr.Peek() <> -1
                inputLine = sr.ReadLine()
                If inputLine <> "END" Then
                    coords = inputLine.Split("	")
                    If coords(0) = "ESZ:" Then
                        esz = coords(1)
                    ElseIf coords(0) = "NUM:" Then
                        num = coords(1)
                    ElseIf coords(0) = "XY:" Then
                        latlon = convertUTMLatLon((coords(2) * 0.01), (coords(1) * 0.01)).Split(";")
                        txtCoord.Text &= esz & "," & num & "," & latlon(0) & "," & latlon(1) & vbCrLf
                    End If
                Else
                End If
            End While
            sr.Close()
        Catch
        End Try
    End Sub

    Function convertUTMLatLon(ByVal northing As Double, ByVal easting As Double) As String
        Dim lat As String = ""
        Dim lon As String = ""
        Try
            Dim utm As New GPS.UTM(northing, easting, 15)
            utm.UTMLatitudeHemisphere = GPS.UTMLat.North
            lat = Decimal.Round(utm.Latitude.GetDecimalCoordinate, 6, MidpointRounding.AwayFromZero).ToString
            lon = Decimal.Round(utm.Longitude.GetDecimalCoordinate, 6, MidpointRounding.AwayFromZero).ToString
        Catch
        End Try
        Return lat & ";" & lon
    End Function

    Function validateLine(ByVal linenumber As Integer) As Boolean
        Try
            Dim lineinfo As String = txtCoord.Text.Split(vbCrLf)(linenumber).Trim & vbCrLf
            If validateData(lineinfo) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Function validateData(ByVal lineinfo As String) As Boolean
        Try
            Dim esz As String = lineinfo.Split(",")(0).Trim
            Dim num As String = lineinfo.Split(",")(1).Trim
            Dim longitude As String = lineinfo.Split(",")(2).Trim
            Dim latitude As String = lineinfo.Split(",")(3).Trim
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class


Public Class GPS

#Region "Enums"
    Public Enum CardinalDirection
        North
        East
        South
        West
    End Enum

    Public Enum UTMLat
        North = CardinalDirection.North
        South = CardinalDirection.South
    End Enum

    Public Enum CoordinateType
        Latitude
        Longitude
    End Enum

    Public Enum UTMDatum
        WGS84
        NAD83
        GRS80
        WGS72
        Australian1965
        Krasovsky1940
        NorthAmerican1927
        International1924
        Hayford1909
        Clarke1880
        Clarke1866
        Airy1830
        Bessel1841
        Everest1830
    End Enum
#End Region

#Region "Structures"
    Public Structure Coordinate
        Public Degrees As Int32
        Public Minutes As Int32
        Public Seconds As Decimal
        Public Direction As CardinalDirection

        Public Sub New(ByVal Degrees As Int32, ByVal Minutes As Int32, ByVal seconds As Decimal, ByVal Direction As CardinalDirection)
            Me.Degrees = Degrees
            Me.Minutes = Minutes
            Me.Seconds = seconds
            Me.Direction = Direction
        End Sub
        Public Sub New(ByVal Degrees As Decimal, ByVal Type As CoordinateType)
            If Type = CoordinateType.Latitude Then
                If Degrees > 90 OrElse Degrees < -90 Then Exit Sub
                Me.Direction = If(Degrees < 0, CardinalDirection.South, CardinalDirection.North)
            Else
                If Degrees > 180 OrElse Degrees < -180 Then Exit Sub
                Me.Direction = If(Degrees < 0, CardinalDirection.West, CardinalDirection.East)
            End If

            Degrees = Math.Abs(Degrees)
            Me.Degrees = System.Convert.ToInt32(Math.Truncate(Degrees))
            Me.Minutes = System.Convert.ToInt32(Math.Truncate((Degrees - Math.Truncate(Degrees)) * 60D))
            Me.Seconds = (Degrees - Math.Truncate(Degrees) - System.Convert.ToDecimal(Me.Minutes) / 60) * 3600
        End Sub

        Public Overrides Function ToString() As String
            Return String.Format("{0}° {1}' {2:f3}"" {3}", Degrees, Minutes, Seconds, Direction)
        End Function

        Private Function FlipDirection(ByVal Direction As CardinalDirection) As CardinalDirection
            Select Case Direction
                Case CardinalDirection.North
                    Return CardinalDirection.South
                Case CardinalDirection.East
                    Return CardinalDirection.West
                Case CardinalDirection.South
                    Return CardinalDirection.North
                Case CardinalDirection.West
                    Return CardinalDirection.East
            End Select
        End Function

        Public Function GetAbsoluteDecimalCoordinate() As Decimal
            Return System.Convert.ToDecimal(Degrees) + System.Convert.ToDecimal(Minutes) / 60D + Seconds / 3600D
        End Function

        Public Function GetDecimalCoordinate() As Decimal
            Dim dec As Decimal = System.Convert.ToDecimal(Degrees) + System.Convert.ToDecimal(Minutes) / 60D + Seconds / 3600D
            Return If((Direction = CardinalDirection.North OrElse Direction = CardinalDirection.East), dec, -dec)
        End Function
    End Structure
#End Region

#Region "UTM Object"
    Public Class UTM
        Private Const k0 As Double = 0.9996

        Private cooLat As New Coordinate(0, CoordinateType.Latitude)
        Private cooLong As New Coordinate(0, CoordinateType.Longitude)
        Private utmDat As UTMDatum = UTMDatum.WGS84
        Private dblNorthing As Double = 0
        Private dblEasting As Double = 0
        Private utmL As UTMLat = UTMLat.North
        Private intZone As Int32

#Region "Properties"

        Public Property UTMLatitudeHemisphere() As UTMLat
            Get
                Return utmL
            End Get
            Set(ByVal value As UTMLat)
                utmL = value
                GetLatLong()
            End Set
        End Property

        Public Property Datum() As UTMDatum
            Get
                Return utmDat
            End Get
            Set(ByVal value As UTMDatum)
                utmDat = value
            End Set
        End Property

        Public Property Latitude() As Coordinate
            Get
                Return cooLat
            End Get
            Set(ByVal value As Coordinate)
                If value.Direction = CardinalDirection.North OrElse value.Direction = CardinalDirection.South Then
                    cooLat = value
                    utmL = DirectCast(cooLat.Direction, UTMLat)
                    GetUTM()
                End If
            End Set
        End Property

        Public Property Longitude() As Coordinate
            Get
                Return cooLong
            End Get
            Set(ByVal value As Coordinate)
                If value.Direction = CardinalDirection.East OrElse value.Direction = CardinalDirection.West Then
                    cooLong = value
                    GetUTM()
                End If
            End Set
        End Property

        Public Property Zone() As Int32
            Get
                Return intZone
            End Get
            Set(ByVal value As Int32)
                intZone = value
                GetLatLong()
            End Set
        End Property

        Public Property Easting() As Double
            Get
                Return dblEasting
            End Get
            Set(ByVal value As Double)
                dblEasting = value
                GetLatLong()
            End Set
        End Property

        Public Property Northing() As Double
            Get
                Return dblNorthing
            End Get
            Set(ByVal value As Double)
                dblNorthing = value
                GetLatLong()
            End Set
        End Property

#End Region

#Region "Constructors"
        Public Sub New(ByVal Latitude As Coordinate, ByVal Longitude As Coordinate)
            cooLat = Latitude
            cooLong = Longitude
            intZone = GetZone()
            utmL = DirectCast(cooLat.Direction, UTMLat)
            GetUTM()
        End Sub

        Public Sub New(ByVal Northing As Double, ByVal Easting As Double, ByVal Zone As Int32)
            dblNorthing = Northing
            dblEasting = Easting
            intZone = Zone
            GetLatLong()
        End Sub
#End Region

        Public Function GetZone() As Int32
            Dim decLongAbs As Decimal = cooLong.GetDecimalCoordinate
            If cooLong.Direction = CardinalDirection.West Then
                Return System.Convert.ToInt32(Math.Ceiling((180 + decLongAbs) / 6))
            Else
                Return System.Convert.ToInt32(Math.Ceiling(decLongAbs / 6) + 30)
            End If

        End Function

        Public Function GetZoneCM() As Int32
            Return 6 * intZone - 183
        End Function

        Public Sub GetUTM()
            Dim a As Double = LookupA()
            Dim b As Double = LookupB()
            Dim f As Double = (a - b) / a
            Dim invf As Double = 1 / f
            Dim rm As Double = (a * b) ^ 0.5
            Dim e As Double = Math.Sqrt(1 - (b / a) ^ 2)
            Dim e1sq As Double = e ^ 2 / (1 - e ^ 2)
            Dim n As Double = (a - b) / (a + b)
            Dim latRad As Double = cooLat.GetDecimalCoordinate * Math.PI / 180
            Dim rho As Double = a * (1 - e ^ 2) / ((1 - (e * Math.Sin(latRad)) ^ 2) ^ (3 / 2))
            Dim nu As Double = a / ((1 - (e * Math.Sin(latRad)) ^ 2) ^ (1 / 2))

            Dim a0 As Double = a * (1 - n + (5 * n * n / 4) * (1 - n) + (81 * n ^ 4 / 64) * (1 - n))
            Dim b0 As Double = (3 * a * n / 2) * (1 - n - (7 * n * n / 8) * (1 - n) + 55 * n ^ 4 / 64)
            Dim c0 As Double = (15 * a * n * n / 16) * (1 - n + (3 * n * n / 4) * (1 - n))
            Dim d0 As Double = (35 * a * n ^ 3 / 48) * (1 - n + 11 * n * n / 16)
            Dim e0 As Double = (315 * a * n ^ 4 / 51) * (1 - n)
            Dim s As Double = a0 * latRad - b0 * Math.Sin(2 * latRad) + c0 * Math.Sin(4 * latRad) - d0 * Math.Sin(6 * latRad) + e0 * Math.Sin(8 * latRad)

            Dim p As Double = (cooLong.GetDecimalCoordinate - GetZoneCM()) * 3600 / 10000
            Dim sin1 As Double = Math.PI / (180 * 3600)

            Dim ki As Double = s * k0
            Dim kii As Double = nu * Math.Sin(latRad) * Math.Cos(latRad) * sin1 ^ 2 * k0 * (100000000) / 2
            Dim kiii As Double = ((sin1 ^ 4 * nu * Math.Sin(latRad) * Math.Cos(latRad) ^ 3) / 24) * (5 - Math.Tan(latRad) ^ 2 + 9 * e1sq * Math.Cos(latRad) ^ 2 + 4 * e1sq ^ 2 * Math.Cos(latRad) ^ 4) * k0 * (10000000000000000)
            Dim kiv As Double = nu * Math.Cos(latRad) * sin1 * k0 * 10000
            Dim kv As Double = (sin1 * Math.Cos(latRad)) ^ 3 * (nu / 6) * (1 - Math.Tan(latRad) ^ 2 + e1sq * Math.Cos(latRad) ^ 2) * k0 * (1000000000000)
            Dim a6 As Double = ((p * sin1) ^ 6 * nu * Math.Sin(latRad) * Math.Cos(latRad) ^ 5 / 720) * (61 - 58 * Math.Tan(latRad) ^ 2 + Math.Tan(latRad) ^ 4 + 270 * e1sq * Math.Cos(latRad) ^ 2 - 330 * e1sq * Math.Sin(latRad) ^ 2) * k0 * (1.0E+24)

            dblEasting = 500000 + (kiv * p + kv * p ^ 3)
            dblNorthing = (ki + kii * p * p + kiii * p ^ 4)
            If cooLat.Direction = CardinalDirection.South Then dblNorthing = 10000000 + dblNorthing
        End Sub

        Public Sub GetLatLong()
            Dim a As Double = LookupA()
            Dim b As Double = LookupB()
            Dim f As Double = (a - b) / a
            Dim invf As Double = 1 / f
            Dim rm As Double = (a * b) ^ 0.5
            Dim ec As Double = Math.Sqrt(1 - (b / a) ^ 2)
            Dim eisq As Double = ec ^ 2 / (1 - ec ^ 2)

            dblNorthing = If(utmL = UTMLat.North, dblNorthing, 10000000 - dblNorthing)

            Dim arc As Double = dblNorthing / k0
            Dim mu As Double = arc / (a * (1 - ec ^ 2 / 4 - 3 * ec ^ 4 / 64 - 5 * ec ^ 6 / 256))
            Dim ei As Double = (1 - (1 - ec * ec) ^ (1 / 2)) / (1 + (1 - ec * ec) ^ (1 / 2))
            Dim ca As Double = 3 * ei / 2 - 27 * ei ^ 3 / 32
            Dim cb As Double = 21 * ei ^ 2 / 16 - 55 * ei ^ 4 / 32
            Dim ccc As Double = 151 * ei ^ 3 / 96
            Dim cd As Double = 1097 * ei ^ 4 / 512
            Dim phi1 As Double = mu + ca * Math.Sin(2 * mu) + cb * Math.Sin(4 * mu) + ccc * Math.Sin(6 * mu) + cd * Math.Sin(8 * mu)

            Dim sin1 As Double = Math.PI / (180 * 3600)
            Dim q0 As Double = eisq * Math.Cos(phi1) ^ 2
            Dim t0 As Double = Math.Tan(phi1) ^ 2
            Dim n0 As Double = a / (1 - (ec * Math.Sin(phi1)) ^ 2) ^ (1 / 2)
            Dim r0 As Double = a * (1 - ec * ec) / (1 - (ec * Math.Sin(phi1)) ^ 2) ^ (3 / 2)
            Dim dd0 As Double = (500000 - dblEasting) / (n0 * k0)

            Dim fact1 As Double = n0 * Math.Tan(phi1) / r0
            Dim fact2 As Double = dd0 * dd0 / 2
            Dim fact3 As Double = (5 + 3 * t0 + 10 * q0 - 4 * q0 * q0 - 9 * eisq) * dd0 ^ 4 / 24
            Dim fact4 As Double = (61 + 90 * t0 + 298 * q0 + 45 * t0 * t0 - 252 * eisq - 3 * q0 * q0) * dd0 ^ 6 / 720

            Dim lof1 As Double = dd0
            Dim lof2 As Double = (1 + 2 * t0 + q0) * dd0 ^ 3 / 6
            Dim lof3 As Double = (5 - 2 * q0 + 28 * t0 - 3 * q0 ^ 2 + 8 * eisq + 24 * t0 ^ 2) * dd0 ^ 5 / 120

            Dim lat As Double = 180 * (phi1 - fact1 * (fact2 + fact3 + fact4)) / Math.PI
            If utmL = UTMLat.South Then lat = -lat
            cooLat = New Coordinate(System.Convert.ToDecimal(lat), CoordinateType.Latitude)

            Dim lon As Double = GetZoneCM() - ((lof1 - lof2 + lof3) / Math.Cos(phi1)) * 180 / Math.PI

            cooLong = New Coordinate(System.Convert.ToDecimal(lon), CoordinateType.Longitude)
        End Sub

        Private Function LookupA() As Double
            Select Case utmDat
                Case UTMDatum.NAD83, UTMDatum.WGS84, UTMDatum.GRS80
                    Return 6378137
                Case UTMDatum.WGS72
                    Return 6378135
                Case UTMDatum.Australian1965
                    Return 6378160
                Case UTMDatum.Krasovsky1940
                    Return 6378245
                Case UTMDatum.NorthAmerican1927
                    Return 6378206.4
                Case UTMDatum.International1924, UTMDatum.Hayford1909
                    Return 6378388
                Case UTMDatum.Clarke1880
                    Return 6378249.1
                Case UTMDatum.Clarke1866
                    Return 6378206.4
                Case UTMDatum.Bessel1841
                    Return 6377397.2
                Case UTMDatum.Airy1830
                    Return 6377563.4
                Case UTMDatum.Everest1830
                    Return 6377276.3
            End Select
        End Function

        Private Function LookupB() As Double
            Select Case utmDat
                Case UTMDatum.NAD83, UTMDatum.WGS84
                    Return 6356752.3142
                Case UTMDatum.GRS80
                    Return 6356752.3141
                Case UTMDatum.WGS72
                    Return 6356750.5
                Case UTMDatum.Australian1965
                    Return 6356774.7
                Case UTMDatum.Krasovsky1940
                    Return 6356863
                Case UTMDatum.NorthAmerican1927
                    Return 6356583.8
                Case UTMDatum.International1924, UTMDatum.Hayford1909
                    Return 6356911.9
                Case UTMDatum.Clarke1880
                    Return 6356514.9
                Case UTMDatum.Clarke1866
                    Return 6356583.8
                Case UTMDatum.Bessel1841
                    Return 6356079
                Case UTMDatum.Airy1830
                    Return 6356256.9
                Case UTMDatum.Everest1830
                    Return 6356075.4
            End Select
        End Function

        Public Function GetZoneChar() As Char
            Select Case cooLat.GetDecimalCoordinate
                Case -90 To -84
                    Return "A"c
                Case -84 To -72
                    Return "C"c
                Case -72 To -64
                    Return "D"c
                Case -64 To -56
                    Return "E"c
                Case -56 To -48
                    Return "F"c
                Case -48 To -40
                    Return "G"c
                Case -40 To -32
                    Return "H"c
                Case -32 To -24
                    Return "J"c
                Case -24 To -16
                    Return "K"c
                Case -16 To -8
                    Return "L"c
                Case -8 To 0
                    Return "M"c
                Case 0 To 8
                    Return "N"c
                Case 8 To 16
                    Return "P"c
                Case 16 To 24
                    Return "Q"c
                Case 24 To 32
                    Return "R"c
                Case 32 To 40
                    Return "S"c
                Case 40 To 48
                    Return "T"c
                Case 48 To 56
                    Return "U"c
                Case 56 To 64
                    Return "V"c
                Case 64 To 72
                    Return "W"c
                Case 72 To 84
                    Return "X"c
                Case Else
                    Return "Z"c
            End Select
        End Function

    End Class
#End Region

End Class
