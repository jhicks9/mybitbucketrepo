<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="AssistanceAgencyList.aspx.vb" Inherits="Maintenance_web_AssistanceAgencyList" title="Assistance Agencies List" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container_1">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Arkansas</a></li>
                <li><a href="#tab_b" data-toggle="pill">Kansas</a></li>
                <li><a href="#tab_c" data-toggle="pill">Missouri</a></li>
                <li><a href="#tab_d" data-toggle="pill">Missouri Gas</a></li>
                <li><a href="#tab_e" data-toggle="pill">Oklahoma</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateArkansas" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvArkansas" runat="server" DataSourceID="sqlDSAR" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddArkansas" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new agency" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectArkansas" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                        <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                                        <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                        <asp:BoundField DataField="familyservices" HeaderText="Family Services" SortExpression="familyservices" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmptyArkansas" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateKansas" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvKansas" runat="server" DataSourceID="sqlDSKS" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddKansas" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new agency" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectKansas" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                        <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                                        <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                        <asp:BoundField DataField="familyservices" HeaderText="Family Services" SortExpression="familyservices" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmptyKansas" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->

                <div class="tab-pane" id="tab_c">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateMissouri" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvMissouri" runat="server" DataSourceID="sqlDSMO" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddMissouri" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new agency" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectMissouri" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                        <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                                        <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                        <asp:BoundField DataField="familyservices" HeaderText="Family Services" SortExpression="familyservices" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmptyMissouri" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-C -->

                <div class="tab-pane" id="tab_d">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateGas" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvGAS" runat="server" DataSourceID="sqlDSGAS" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddGas" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new agency" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectGas" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                        <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                                        <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                        <asp:BoundField DataField="familyservices" HeaderText="Family Services" SortExpression="familyservices" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmptyGas" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-D -->

                <div class="tab-pane" id="tab_e">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateOklahoma" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvOklahoma" runat="server" DataSourceID="sqlDSOK" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddOklahoma" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new agency" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectOklahoma" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                                        <asp:BoundField DataField="location" HeaderText="Location" SortExpression="location" />
                                        <asp:BoundField DataField="agency" HeaderText="Agency" SortExpression="agency" />
                                        <asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone" />
                                        <asp:BoundField DataField="familyservices" HeaderText="Family Services" SortExpression="familyservices" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmptyOklahoma" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add agency</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-E -->
            </div><!-- tab-content -->
        </div><!-- tab container -->


        <div class="modal fade" id="ARDetail" tabindex="-1" role="dialog" aria-labelledby="ARDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateARDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="ARDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSAR2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" Visible="False"></asp:BoundField>                    
                                        <asp:templatefield headertext="County" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCounty" runat="server" Text='<%# Bind("county") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvCounty" runat="server" ControlToValidate="txtCounty" ErrorMessage="County is required" SetFocusOnError="True" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Location" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtLocation" runat="server" Text='<%# Bind("location") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Agency" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAgency" runat="server" Text='<%# Bind("agency") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                     
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control"/>
                                          </EditItemTemplate>
                                        </asp:templatefield>   
                                        <asp:templatefield headertext="Family Services" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtFamilyServices" runat="server" Text='<%# Bind("familyservices") %>' CssClass="col-md-9 form-control"/>
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Energy Crisis" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtEnergyCrisis" runat="server" Text='<%# Bind("energycrisis") %>' CssClass="col-md-9 form-control"/>
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Project Help" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtProjectHelp" runat="server" Text='<%# Bind("projectHelp") %>' CssClass="col-md-9 form-control"/>
                                          </EditItemTemplate>
                                        </asp:templatefield>                                                                         
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblARStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><%--updateARDetails--%>
                    </div>
                </div>
            </div>
        </div><%--ARDetail--%>

        <div class="modal fade" id="KSDetail" tabindex="-1" role="dialog" aria-labelledby="KSDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateKSDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="KSDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSKS2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" Visible="False"></asp:BoundField>                    
                                        <asp:templatefield headertext="County" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCounty" runat="server" Text='<%# Bind("county") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvCounty" runat="server" ControlToValidate="txtCounty" ErrorMessage="County is required" SetFocusOnError="True" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Location" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtLocation" runat="server" Text='<%# Bind("location") %>' CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Agency" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAgency" runat="server" Text='<%# Bind("agency") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                     
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>   
                                        <asp:templatefield headertext="Family Services" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtFamilyServices" runat="server" Text='<%# Bind("familyservices") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Energy Crisis" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtEnergyCrisis" runat="server" Text='<%# Bind("energycrisis") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Project Help" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtProjectHelp" runat="server" Text='<%# Bind("projectHelp") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>                                                                         
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblKSStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><%--updateKSDetails--%>
                    </div>
                </div>
            </div>
        </div><%--KSDetail--%>

        <div class="modal fade" id="MODetail" tabindex="-1" role="dialog" aria-labelledby="MODetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlMODetails" runat="server">
                            <contenttemplate>	                                 
                                <asp:DetailsView ID="MODetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSMO2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" Visible="False"></asp:BoundField>                    
                                        <asp:templatefield headertext="County" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCounty" runat="server" Text='<%# Bind("county") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvCounty" runat="server" ControlToValidate="txtCounty" ErrorMessage="County is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Location" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtLocation" runat="server" Text='<%# Bind("location") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Agency" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAgency" runat="server" Text='<%# Bind("agency") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                     
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>   
                                        <asp:templatefield headertext="Family Services" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtFamilyServices" runat="server" Text='<%# Bind("familyservices") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Energy Crisis" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtEnergyCrisis" runat="server" Text='<%# Bind("energycrisis") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Project Help" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtProjectHelp" runat="server" Text='<%# Bind("projectHelp") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>                                                                         
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblMOStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="GASDetail" tabindex="-1" role="dialog" aria-labelledby="GASDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateGASDetails" runat="server">
                            <contenttemplate>	                                 
                                <asp:DetailsView ID="GASDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSGAS2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" Visible="False"></asp:BoundField>                    
                                        <asp:templatefield headertext="County" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCounty" runat="server" Text='<%# Bind("county") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvCounty" runat="server" ControlToValidate="txtCounty" ErrorMessage="County is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Location" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtLocation" runat="server" Text='<%# Bind("location") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Agency" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAgency" runat="server" Text='<%# Bind("agency") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                     
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>   
                                        <asp:templatefield headertext="Family Services" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtFamilyServices" runat="server" Text='<%# Bind("familyservices") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Energy Crisis" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtEnergyCrisis" runat="server" Text='<%# Bind("energycrisis") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Project Help" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtProjectHelp" runat="server" Text='<%# Bind("projectHelp") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>                                                                         
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><%--GASDetails--%>
                                <asp:Label ID="lblGASStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><%--updateGASDetails--%>
                    </div>
                </div>
            </div>
        </div><%--GASDetail--%>

        <div class="modal fade" id="OKDetail" tabindex="-1" role="dialog" aria-labelledby="OKDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlOKDetails" runat="server">
                            <contenttemplate>	                                 
                                <asp:DetailsView ID="OKDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSOK2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" Visible="False"></asp:BoundField>                    
                                        <asp:templatefield headertext="County" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtCounty" runat="server" Text='<%# Bind("county") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvCounty" runat="server" ControlToValidate="txtCounty" ErrorMessage="County is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Location" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtLocation" runat="server" Text='<%# Bind("location") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Agency" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtAgency" runat="server" Text='<%# Bind("agency") %>' CssClass="col-md-9 form-control" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                     
                                        <asp:templatefield headertext="Phone" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("phone") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>   
                                        <asp:templatefield headertext="Family Services" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtFamilyServices" runat="server" Text='<%# Bind("familyservices") %>' CssClass="col-md-9 form-control"/>
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Energy Crisis" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtEnergyCrisis" runat="server" Text='<%# Bind("energycrisis") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Project Help" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:TextBox ID="txtProjectHelp" runat="server" Text='<%# Bind("projectHelp") %>' CssClass="col-md-9 form-control" />
                                          </EditItemTemplate>
                                        </asp:templatefield>                                                                         
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView><%--OKDetails--%>
                                <asp:Label ID="lblOKStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel><%--pnlOKDetails--%>
                    </div>
                </div>
            </div>
        </div><%--OKDetail--%>
    </asp:Panel>

    <asp:SqlDataSource ID="sqlDSAR" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
         SelectCommand="SELECT id,county,location,agency,phone,familyservices from agencies where type = 'electric' and state = 'Arkansas' order by county,location">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSAR2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp from agencies where (agencies.id = @id)"
        InsertCommand="INSERT INTO agencies (type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp) VALUES (@type,@state,@county,@location,@agency,@phone,@familyservices,@energycrisis,@projecthelp)"
        UpdateCommand="UPDATE agencies SET county=@county,location=@location,agency=@agency,phone=@phone,familyservices=@familyservices,energycrisis=@energycrisis,projecthelp=@projecthelp WHERE (agencies.id = @id)"
        DeleteCommand="DELETE FROM agencies WHERE agencies.id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvArkansas" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric" />
            <asp:Parameter Name="state" Type="String" DefaultValue="Arkansas" />
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />            
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSKS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
         SelectCommand="SELECT id,county,location,agency,phone,familyservices from agencies where type = 'electric' and state = 'Kansas' order by county,location">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSKS2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp from agencies where (agencies.id = @id)"
        InsertCommand="INSERT INTO agencies (type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp) VALUES (@type,@state,@county,@location,@agency,@phone,@familyservices,@energycrisis,@projecthelp)"
        UpdateCommand="UPDATE agencies SET county=@county,location=@location,agency=@agency,phone=@phone,familyservices=@familyservices,energycrisis=@energycrisis,projecthelp=@projecthelp WHERE (agencies.id = @id)"
        DeleteCommand="DELETE FROM agencies WHERE agencies.id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvKansas" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric" />
            <asp:Parameter Name="state" Type="String" DefaultValue="Kansas" />
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />            
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSMO" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
         SelectCommand="SELECT id,county,location,agency,phone,familyservices from agencies where type = 'electric' and state = 'Missouri' order by county,location">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSMO2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp from agencies where (agencies.id = @id)"
        InsertCommand="INSERT INTO agencies (type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp) VALUES (@type,@state,@county,@location,@agency,@phone,@familyservices,@energycrisis,@projecthelp)"
        UpdateCommand="UPDATE agencies SET county=@county,location=@location,agency=@agency,phone=@phone,familyservices=@familyservices,energycrisis=@energycrisis,projecthelp=@projecthelp WHERE (agencies.id = @id)"
        DeleteCommand="DELETE FROM agencies WHERE agencies.id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvMissouri" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric" />
            <asp:Parameter Name="state" Type="String" DefaultValue="Missouri" />
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />            
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSGAS" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
         SelectCommand="SELECT id,county,location,agency,phone,familyservices from agencies where type = 'gas' and state = 'Missouri' order by county,location">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSGAS2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp from agencies where (agencies.id = @id)"
        InsertCommand="INSERT INTO agencies (type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp) VALUES (@type,@state,@county,@location,@agency,@phone,@familyservices,@energycrisis,@projecthelp)"
        UpdateCommand="UPDATE agencies SET county=@county,location=@location,agency=@agency,phone=@phone,familyservices=@familyservices,energycrisis=@energycrisis,projecthelp=@projecthelp WHERE (agencies.id = @id)"
        DeleteCommand="DELETE FROM agencies WHERE agencies.id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvGAS" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Gas" />
            <asp:Parameter Name="state" Type="String" DefaultValue="Missouri" />
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />            
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlDSOK" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
         SelectCommand="SELECT id,county,location,agency,phone,familyservices from agencies where type = 'electric' and state = 'Oklahoma' order by county,location">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDSOK2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp from agencies where (agencies.id = @id)"
        InsertCommand="INSERT INTO agencies (type,state,county,location,agency,phone,familyservices,energycrisis,projecthelp) VALUES (@type,@state,@county,@location,@agency,@phone,@familyservices,@energycrisis,@projecthelp)"
        UpdateCommand="UPDATE agencies SET county=@county,location=@location,agency=@agency,phone=@phone,familyservices=@familyservices,energycrisis=@energycrisis,projecthelp=@projecthelp WHERE (agencies.id = @id)"
        DeleteCommand="DELETE FROM agencies WHERE agencies.id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvOklahoma" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:Parameter Name="type" Type="String" DefaultValue="Electric" />
            <asp:Parameter Name="state" Type="String" DefaultValue="Oklahoma" />
            <asp:Parameter Name="county" Type="String" />
            <asp:Parameter Name="location" Type="String" />
            <asp:Parameter Name="agency" Type="String" />
            <asp:Parameter Name="phone" Type="String" />            
            <asp:Parameter Name="familyservices" Type="String" />
            <asp:Parameter Name="energycrisis" Type="String" />
            <asp:Parameter Name="projecthelp" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>