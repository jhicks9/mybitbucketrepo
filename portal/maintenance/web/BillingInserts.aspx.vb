﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Partial Class Maintenance_web_BillingInserts
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isAdmin(ps.getPageName) Then
            pnlAuthorized.Visible = True
            pnlNotAuthorized.Visible = False
            pnlAdministration.Visible = True
        Else
            pnlAdministration.Visible = False
            If Not ps.isAuthorized(ps.getPageName) Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If
        End If
    End Sub

    Protected Sub gvBillInserts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBillInserts.RowCommand
        Select Case e.CommandName
            Case "Select"
            Case "AddRecord"
                pnlBillInsert.Visible = False
                pnlAddInsert.Visible = True
                txtName.Focus()
        End Select
    End Sub
    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCancel.Click
        pnlAddInsert.Visible = False
        pnlBillInsert.Visible = True
    End Sub
    Protected Sub lbInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInsert.Click
        'Make sure a file has been uploaded
        If fluBillInsert.PostedFile Is Nothing OrElse String.IsNullOrEmpty(fluBillInsert.PostedFile.FileName) OrElse fluBillInsert.PostedFile.InputStream Is Nothing Then
            lblFileValidation.Text = "File Required"
            lblFileValidation.Visible = True
            Exit Sub
        Else
            Dim extension As String = System.IO.Path.GetExtension(fluBillInsert.PostedFile.FileName).ToLower()
            Dim MIMEType As String = Nothing
            Select Case extension
                Case ".pdf"
                    MIMEType = "Application/pdf"
                Case Else
                    lblFileValidation.Text = "Invalid file type uploaded"
                    lblFileValidation.Visible = True
                    Exit Sub
            End Select

            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim query As String = "insert into billinsert (name,begdate,enddate,state,revenueclass,pkgdescription,required,filename,imagedata) values (@name,@begdate,@enddate,@state,@revenueclass,@pkgdescription,@required,@filename,@imagedata)"
            Dim cmd As New SqlCommand(query, conn)
            cmd.Parameters.AddWithValue("@name", txtName.Text)
            cmd.Parameters.AddWithValue("@begdate", txtBegDate.Text)
            cmd.Parameters.AddWithValue("@enddate", txtEndDate.Text)
            If ddlState.SelectedValue = "" Then
                cmd.Parameters.AddWithValue("@state", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@state", ddlState.SelectedValue)
            End If
            If ddlRevenueClass.SelectedValue = "" Then
                cmd.Parameters.AddWithValue("@revenueclass", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@revenueclass", ddlRevenueClass.SelectedValue)
            End If
            If ddlPkgDescription.SelectedValue = "" Then
                cmd.Parameters.AddWithValue("@pkgdescription", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@pkgdescription", ddlPkgDescription.SelectedValue)
            End If
            cmd.Parameters.AddWithValue("@required", ckbRequired.Checked)
            cmd.Parameters.AddWithValue("@filename", System.IO.Path.GetFileName(fluBillInsert.PostedFile.FileName))

            'Load FileUpload's InputStream into Byte array
            Dim imageBytes(fluBillInsert.PostedFile.InputStream.Length) As Byte
            fluBillInsert.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
            cmd.Parameters.AddWithValue("@imagedata", imageBytes)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            cmd.Dispose()
            conn.Dispose()

            txtName.Text = ""
            txtBegDate.Text = ""
            txtEndDate.Text = ""
            ddlState.SelectedIndex = 0
            ddlRevenueClass.SelectedIndex = 0
            ckbRequired.Checked = False
            lblFileValidation.Text = ""
            lblFileValidation.Visible = False

            sqlDSInserts.DataBind()
            gvBillInserts.DataBind()

            pnlAddInsert.Visible = False
            pnlBillInsert.Visible = True
        End If
    End Sub

    Protected Sub gvBillInserts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBillInserts.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim state As Label = CType(e.Row.FindControl("lblState"), Label)
                If Len(state.Text) <= 0 Then
                    state.Text = "All"
                End If
                Dim type As Label = CType(e.Row.FindControl("lblPkgDescription"), Label)
                If Len(type.Text) <= 0 Then
                    type.Text = "All"
                End If
            End If
            Dim rc As Label = CType(e.Row.FindControl("lblRevenueClass"), Label)
            If Len(rc.Text) <= 0 Then
                rc.Text = "All"
            End If
        Catch
        End Try
    End Sub

    Protected Sub gvBillInserts_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvBillInserts.RowUpdating
        Try
            'Dim index As Integer = Convert.ToInt32(e.RowIndex)
            Dim row As GridViewRow = gvBillInserts.Rows(e.RowIndex)
            Dim lbl As Label = CType(row.FindControl("lblValidation"), Label)
            Dim UploadedFile As FileUpload = CType(row.FindControl("UpdateUploadedFile"), FileUpload)

            'If file is uploaded make sure its pdf file
            If UploadedFile.PostedFile Is Nothing OrElse String.IsNullOrEmpty(UploadedFile.PostedFile.FileName) OrElse UploadedFile.PostedFile.InputStream Is Nothing Then
                sqlDSInserts.UpdateCommand = "update billinsert SET name=@name,begdate=@begdate,enddate=@enddate,state=@state,revenueclass=@revenueclass,pkgdescription=@pkgdescription,required=@required where id = @id"
            Else
                Dim extension As String = System.IO.Path.GetExtension(UploadedFile.PostedFile.FileName).ToLower()
                Dim MIMEType As String = Nothing
                Select Case extension
                    Case ".pdf"
                        MIMEType = "Application/pdf"
                    Case Else
                        lbl.Text = "Invalid file type uploaded"
                        lbl.Visible = True
                        e.Cancel = True
                        Exit Sub
                End Select

                'Specify the values for MIMEType and Image
                'e.Values("MIMEType") = MIMEType
                UploadedFile.PostedFile.InputStream.Position = 0
                Dim imageBytes(UploadedFile.PostedFile.InputStream.Length) As Byte
                UploadedFile.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                e.NewValues("imagedata") = imageBytes
                e.NewValues("filename") = System.IO.Path.GetFileName(UploadedFile.PostedFile.FileName)
            End If
        Catch
        End Try
    End Sub
End Class
