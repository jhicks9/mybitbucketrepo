Imports System
Imports System.IO
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient

Partial Class AdminDocs
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If

            ' Ensure user can actually access any documents
            Dim securityCount As Integer
            securityCount = getsecurityCount()
            If securityCount = 0 Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If

            If Not Page.IsPostBack Then
                SqlDataSourceDefault.SelectParameters("Login").DefaultValue = ps.getUsername
            End If
            lbDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this document?');")
        Catch ex As Exception
        End Try
    End Sub

    Public Sub prepareForm()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim defaultexp As String = ""
        Dim qryType As String = "exp"
        Try
            lblmsg.Text = ""
            defaultexp = getData("exp")
            lblStatus.Text = ""

            If lblFunction.Text = "Add Document" Then
                lblID.Text = ""
                txtBODDescription.Text = ""
                txtWEBDescription.Text = ""
                txtLastUpdate.Text = ""
                'disable expiration date if not required
                If defaultexp <> 0 Then
                    txtExpires.Enabled = True
                    txtExpires.Text = FormatDateTime(Now.AddDays(defaultexp), DateFormat.ShortDate)
                Else
                    txtExpires.Enabled = False
                    txtExpires.Text = ""
                End If
                ' disable editing last update when adding a document
                chkOverrideLastUpdate.Enabled = False
                chkOverrideLastUpdate.Checked = False
                txtLastUpdate.Enabled = False
                txtLastUpdate.Text = ""
                lbUpdate.Text = "Add Document"
            End If

            If lblFunction.Text = "Edit Document" Then
                'load form values
                lblID.Text = hdnID.Value
                chkOverrideLastUpdate.Enabled = True
                chkOverrideLastUpdate.Checked = False
                Dim sql As String = "select documents.description,documents.webdescription,documents.lastupdate,documents.expdate from dbo.documents where documents.id = @docid"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.Parameters.AddWithValue("@docid", hdnID.Value)
                conn.Open()
                Dim dr As SqlDataReader
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        txtBODDescription.Text = dr("description").ToString
                        txtWEBDescription.Text = dr("webdescription").ToString
                        If IsDBNull(dr("expdate")) Then  'dont format date if it doesnt exist
                            txtExpires.Text = ""
                        Else
                            txtExpires.Text = FormatDateTime(dr("expdate").ToString, DateFormat.ShortDate)
                        End If
                        txtLastUpdate.Text = dr("lastupdate").ToString
                    End While
                Else
                End If
                dr.Close()
                conn.Close()

                'disable expiration date if not required
                If defaultexp <> 0 Then
                    txtExpires.Enabled = True
                Else
                    txtExpires.Enabled = False
                    txtExpires.Text = ""
                End If
                'disable last updated date.  can be changed by clicking override checkbox
                txtLastUpdate.Enabled = False
                lbUpdate.Text = "Update Document"
            End If
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub
    Public Function getsecurityCount() As Integer
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand

        Dim datacount As Integer
        Dim strSelect = "select count(*) from groups,security where groups.id = security.groupid and security.userid = @userid"

        mySqlCommand = New SqlCommand(strSelect, conn)
        mySqlCommand.Parameters.AddWithValue("@userid", ps.getUsername)
        conn.Open()
        myDataReader = mySqlCommand.ExecuteReader(CommandBehavior.CloseConnection)
        If myDataReader.HasRows Then
            While myDataReader.Read
                datacount = myDataReader.GetValue(0)
            End While
        End If

        myDataReader.Close()
        conn.Close()
        Return datacount
    End Function
    Protected Sub chkOverrideLastUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOverrideLastUpdate.CheckedChanged
        If chkOverrideLastUpdate.Checked Then
            txtLastUpdate.Enabled = True
        Else
            txtLastUpdate.Enabled = False
        End If
    End Sub

    Function getData(ByVal qryType As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim strSelect As String = "SELECT multiple,defaultexp FROM doctype WHERE id = @id"
            Dim multiple As String = ""
            Dim defaultexp As String = ""
            Dim myDataReader As SqlDataReader
            Dim mySqlCommand As SqlCommand
            Dim rtnValue As String

            mySqlCommand = New SqlCommand(strSelect, conn)
            mySqlCommand.Parameters.AddWithValue("@id", cboDocumentType.SelectedValue)

            conn.Open()
            myDataReader = mySqlCommand.ExecuteReader(CommandBehavior.CloseConnection)
            If myDataReader.HasRows Then
                While myDataReader.Read
                    multiple = myDataReader.GetValue(0).ToString
                    defaultexp = myDataReader.GetValue(1).ToString
                End While
            End If

            myDataReader.Close()
            conn.Close()
            If qryType = "mlt" Then
                rtnValue = multiple
            Else
                rtnValue = defaultexp
            End If
            Return rtnValue
        Catch ex As Exception
            lblmsg.Text = ex.Message.ToString
            Return ""
        End Try
    End Function

    Function CheckFileType(ByVal fileName As String) As Boolean
        Dim ext As String = Path.GetExtension(fileName)
        Select Case ext.ToLower()
            Case ".htm", ".html"
                Return True
            Case ".txt"
                Return True
            Case ".doc", ".rtf"
                Return True
            Case ".ppt"
                Return True
            Case ".csv", ".xls"
                Return True
            Case ".pdf", ".mag"
                Return True
            Case ".mp3"
                Return True
            Case ".xml", ".xsd"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Protected Sub gvDocuments_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDocuments.RowCommand
        Try
            Select Case e.CommandName
                Case "Select"
                    lblFunction.Text = "Edit Document"
                    hdnID.Value = e.CommandArgument
                    prepareForm()
                    pnlDocuments.Visible = False
                    pnlDetail.Visible = True
                Case "AddRecord"
                    lblFunction.Text = "Add Document"
                    prepareForm()
                    pnlDocuments.Visible = False
                    pnlDetail.Visible = True
                Case "Delete"
                    SqlDataSourceEdit.DeleteParameters("id").DefaultValue = e.CommandArgument
                    SqlDataSourceEdit.Delete()
                    gvDocuments.DataSourceID = SqlDataSourceEdit.ID
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lbUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUpdate.Click
        Try
            Dim multiple As String = ""
            Dim StrNotify As String = ""
            Dim query As String = ""

            SqlDataSourceEdit.InsertParameters("lastupdate").DefaultValue = DateTime.Now  'lastupdated timestamp
            multiple = getData("mlt")

            If txtBODDescription.Text = "" Then  'require BOD description
                Throw New Exception("BOD Description Required")
            Else
                If txtWEBDescription.Text = "" Then  'if WEB description not given, use BOD description
                    txtWEBDescription.Text = txtBODDescription.Text
                End If
            End If

            If lblFunction.Text = "Add Document" Then  '** Add Document **
                If upFile.HasFile Then  'require a file to store
                    If Not CheckFileType(upFile.FileName) Then  'only accept certain document types
                        Throw New Exception("not a valid file type")
                    End If
                    If (multiple = "False") And (gvDocuments.Rows.Count > 0) Then  'prohibit multiple documents from being added if the doctype isnt marked for multiple
                        Throw New Exception("Multiple documents are not allowed")
                    End If
                Else
                    Throw New Exception("Document required")  'file required
                End If
                SqlDataSourceEdit.Insert()
                lblStatus.Text = "Document added"
            Else  '** Edit Document **
                If upFile.HasFile Then  'attempting to update a document
                    If Not CheckFileType(upFile.FileName) Then  'only accept certain document types
                        Throw New Exception("not a valid file type")
                    End If
                End If
                Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Dim cmd As New SqlCommand("", conn)
                    query = "update documents set description=@description,webdescription=@webdescription,expdate=@expdate,lastupdate=@lastupdate"
                    If upFile.HasFile Then
                        query &= ",@filesize=@filesize,filebytes=@imagedata,filename=@filename"
                        cmd.Parameters.AddWithValue("@fileSize", upFile.PostedFile.ContentLength)
                        Dim imageBytes(upFile.PostedFile.InputStream.Length) As Byte  'Load FileUpload's InputStream into Byte array
                        upFile.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                        cmd.Parameters.AddWithValue("@imagedata", imageBytes)
                        cmd.Parameters.AddWithValue("@filename", upFile.FileName)
                    End If
                    query &= " where id=" & hdnID.Value

                    cmd.CommandText = query
                    cmd.CommandTimeout = 90
                    cmd.Parameters.AddWithValue("@description", txtBODDescription.Text)
                    cmd.Parameters.AddWithValue("@webdescription", txtWEBDescription.Text)
                    If (txtExpires.Enabled) And (IsDate(txtLastUpdate.Text)) Then
                        cmd.Parameters.AddWithValue("@expdate", txtExpires.Text)
                    Else
                        cmd.Parameters.AddWithValue("@expdate", DBNull.Value)
                    End If
                    If (txtLastUpdate.Enabled) And (IsDate(txtLastUpdate.Text)) Then
                        cmd.Parameters.AddWithValue("@lastupdate", txtLastUpdate.Text)
                    Else
                        cmd.Parameters.AddWithValue("@lastupdate", Now.ToString)
                    End If

                    conn.Open()
                    cmd.ExecuteNonQuery()
                    conn.Close()
                    lblStatus.Text = "Document updated"
                End Using
            End If

            gvDocuments.DataBind
            gvDocuments.SelectedIndex = -1
            pnlDetail.Visible = False
            pnlDocuments.Visible = True
        Catch ex As Exception
            lblmsg.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub lbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDelete.Click
        SqlDataSourceEdit.DeleteParameters("id").DefaultValue = hdnID.Value
        SqlDataSourceEdit.Delete()
        gvDocuments.DataBind
        gvDocuments.SelectedIndex = -1
        pnlDetail.Visible = False
        pnlDocuments.Visible = True
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCancel.Click
        pnlDetail.Visible = False
        pnlDocuments.Visible = True
    End Sub

    Protected Sub cboGroupid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            SqlDataSourceEdit.SelectCommand = "sp_getSecurity @username='" & ps.getUsername & "', @groupid=" & cboGroupid.SelectedValue
            cboDocumentType.DataSourceID = "SqlDataSourceEdit"
            cboDocumentType.DataTextField = "description"
            cboDocumentType.DataValueField = "id"
            gvDocuments.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cboDocumentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            SqlDataSourceDoc.SelectParameters("groupid").DefaultValue = cboGroupid.SelectedValue
            SqlDataSourceDoc.SelectParameters("doctypeid").DefaultValue = cboDocumentType.SelectedValue
            SqlDataSourceDoc.DataBind
            gvDocuments.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cboGroupid_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGroupid.DataBound
        Try
            SqlDataSourceEdit.SelectCommand = "sp_getSecurity @username='" & ps.getUsername & "', @groupid=" & cboGroupid.SelectedValue
            cboDocumentType.DataSourceID = "SqlDataSourceEdit"
            cboDocumentType.DataTextField = "description"
            cboDocumentType.DataValueField = "id"
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub SqlDataSourceEdit_Inserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles SqlDataSourceEdit.Inserting
        e.Command.CommandTimeout = 90  'override timeout -- default is 30 seconds
    End Sub

    Protected Sub SqlDataSourceEdit_Updating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles SqlDataSourceEdit.Updating
        e.Command.CommandTimeout = 90  'override timeout -- default is 30 seconds
    End Sub
End Class