﻿<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="EmpireAnnouncements.aspx.vb" Inherits="Maintenance_web_EmpireAnnouncements" title="Announcements" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container_1">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Announcements</a></li>
                <li><a href="#tab_b" data-toggle="pill">Outage Number</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:Panel ID="pnlAnnouncementDetail" runat="server" Visible="false">
                        <asp:DetailsView ID="AnnouncementDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDS2"
                            CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False" >
                            <Fields>
                                <asp:BoundField DataField="id" SortExpression="UserId" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                <asp:templatefield headertext="Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text="Announcement" CssClass="col-md-9 form-control no-border" />
                                    </ItemTemplate>
                                    <edititemtemplate>
                                        <asp:Label ID="lblType" runat="server" Text="Announcement" CssClass="col-md-9 form-control no-border" />
                                    </edititemtemplate>
                                </asp:templatefield>                                                                 
                                <asp:templatefield headertext="Date" SortExpression="date" HeaderStyle-CssClass="field-label col-xs-3 active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("date") %>' CssClass="col-md-9 form-control"></asp:Label>                                    
                                    </ItemTemplate>                                
                                    <edititemtemplate>
                                        <div class="input-group date datetimepickerclass">
                                            <asp:TextBox ID="txtDate" runat="server" Text='<%# Bind("date") %>' CssClass="col-md-9 form-control" />
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </edititemtemplate>
                                </asp:templatefield>                                 
                                <asp:templatefield headertext="Description" SortExpression="description" HeaderStyle-CssClass="field-label col-xs-3 active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("description") %>' HtmlEncode="false" CssClass="col-md-9 form-control"></asp:Label>
                                    </ItemTemplate>                                                                
                                    <edititemtemplate>
                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="150" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control" />
                                        <ajaxToolKit:HtmlEditorExtender ID="htmlEdit1" runat="server" TargetControlID="txtDescription" EnableSanitization="false" DisplaySourceTab="true" />
                                    </edititemtemplate>
                                </asp:templatefield>                                                                 
                                <asp:templatefield headertext="Beginning Date" SortExpression="begdate" HeaderStyle-CssClass="field-label col-xs-3 active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBegDate" runat="server" Text='<%# Bind("begdate") %>' CssClass="col-md-9 form-control calendar"></asp:Label>
                                    </ItemTemplate>                                                                
                                    <edititemtemplate>
                                        <div class="input-group date datetimepickerclass">
                                            <asp:TextBox ID="txtBegDate" runat="server" Text='<%# Bind("begdate") %>' CssClass="col-md-9 form-control" />
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </edititemtemplate>
                                </asp:templatefield>                                                                 
                                <asp:templatefield headertext="Ending Date" SortExpression="enddate" HeaderStyle-CssClass="field-label col-xs-3 active">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEndDate" runat="server" Text='<%# Bind("enddate") %>' CssClass="col-md-9 form-control"></asp:Label>                                    
                                    </ItemTemplate>                                                                
                                    <edititemtemplate>
                                        <div class="input-group date datetimepickerclass">
                                            <asp:TextBox ID="txtEndDate" runat="server" Text='<%# Bind("enddate") %>' CssClass="col-md-9 form-control" />
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </edititemtemplate>
                                </asp:templatefield> 
                                <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                    <InsertItemTemplate>
                                        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                        <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                        <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                    </InsertItemTemplate>                                
                                    <EditItemTemplate>
                                        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                        <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                        <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                        <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:Label ID="lblAnnouncementStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                        </asp:Panel>

                        <asp:Panel ID="pnlAnnouncement" runat="server" Visible="true">
                        <asp:GridView ID="gvAnnouncements" runat="server" DataSourceID="sqlDS1" DataKeyNames="id" AllowPaging="True" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true"
                            AutoGenerateColumns="false" OnRowCommand="gvAnnouncements_RowCommand">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbAdd" runat="server" CommandName="AddRecord" CausesValidation="false" CssClass="btn btn-primary"
                                            ToolTip="Add new announcement"><span class="fa fa-plus">&nbsp;Add</span></asp:LinkButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbSelectAnnouncement" runat="server" CausesValidation="false" CommandName="Select"
                                            ToolTip="Edit announcement" CssClass="btn btn-default"><span class="fa fa-pencil-square-o">&nbsp;Edit</span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="date" HeaderText="Date" SortExpression="date" />
                                <asp:BoundField DataField="description" HeaderText="Description" SortExpression="description" HtmlEncode="false" />
                                <asp:BoundField DataField="begdate" HeaderText="Beginning Date" SortExpression="begdate" />
                                <asp:BoundField DataField="enddate" HeaderText="Ending Date" SortExpression="enddate" />                                
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:LinkButton ID="lbAddEmpty" runat="server" CommandName="AddRecord" CausesValidation="false" CssClass="btn btn-primary"
                                    ToolTip="Add new announcement"><span class="fa fa-plus">&nbsp;Add Announcement</span></asp:LinkButton>
                            </EmptyDataTemplate>
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                        </asp:Panel>
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="pnlOutageNumber" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvOutage" runat="server" DataSourceID="sqlOutage1" DataKeyNames="id" AllowPaging="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectAnnouncement" runat="server" CausesValidation="false" CommandName="Select"
                                                    ToolTip="Edit" CssClass="btn btn-default"><span class="fa fa-pencil-square-o">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="date" HeaderText="Date" SortExpression="date" />
                                        <asp:BoundField DataField="description" HeaderText="Outage Number" SortExpression="description" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmptyOutage" runat="server" CausesValidation="False" ToolTip="Add Record" CommandName="AddRecord"
                                            CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->
            </div><!-- tab-content -->
        </div><!-- tab container -->


        <div class="modal fade" id="outageDetail" tabindex="-1" role="dialog" aria-labelledby="outageDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlOutageDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="OutageDetails" runat="server" DataKeyNames="id" DataSourceID="sqlOutage2" CssClass="table table-striped table-bordered table-hover"
                                        AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="UserId" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="date" SortExpression="date" HeaderText="Date" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:BoundField DataField="type" SortExpression="type" HeaderText="Type" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="Outage Number" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>'  CssClass="col-md-9 form-control"></asp:Label>                                    
                                            </ItemTemplate>                                                                
                                            <edititemtemplate>
                                            <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("description") %>'  CssClass="col-md-9 form-control"></asp:TextBox>
                                            </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                                <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                                <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />&nbsp;
                                                <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                            </contenttemplate>
                            <triggers>
                                <asp:AsyncPostBackTrigger ControlID="gvOutage" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                            </triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel><%--end pnlAuthorized--%>
                    
    <asp:SqlDataSource ID="sqlDS1" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,CONVERT(char(10),date,101) AS date,description,CONVERT(char(10),begdate,101) AS begdate,CONVERT(CHAR(10),enddate,101) AS enddate FROM messages where type='Announcement' order by date">
    </asp:SqlDataSource>                    
    <asp:SqlDataSource ID="sqlDS2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,CONVERT(char(10),date,101) AS date,type,description,CONVERT(char(10),begdate,101) AS begdate,CONVERT(CHAR(10),enddate,101) AS enddate FROM messages WHERE (id = @id)"
        UpdateCommand="update messages SET description=@description,begdate=@begdate,enddate=@enddate,date=@date WHERE id = @id"
        InsertCommand="INSERT INTO messages (type,date,description,begdate,enddate) VALUES ('Announcement',@date,@description,@begdate,@enddate)"
        DeleteCommand="DELETE FROM messages WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvAnnouncements" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="date" Type="DateTime" />
            <asp:Parameter Name="begdate" Type="DateTime" />
            <asp:Parameter Name="enddate" Type="DateTime" />
        </UpdateParameters> 
        <InsertParameters>
            <asp:Parameter Type="String" Name="type" DefaultValue="Announcement"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="date"></asp:Parameter>
            <asp:Parameter Type="String" Name="description"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="begdate"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="enddate"></asp:Parameter>    
        </InsertParameters>                       
        <DeleteParameters>
            <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue"
                ControlID="gvAnnouncements"></asp:ControlParameter>
        </DeleteParameters>        
    </asp:SqlDataSource>  
    <asp:SqlDataSource ID="sqlOutage1" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT top 1 id,type,date,description FROM messages where type='Outage'">
    </asp:SqlDataSource>                              
    <asp:SqlDataSource ID="sqlOutage2" runat="server" SelectCommand="SELECT id,type,date,type,description FROM messages WHERE (id = @id)"
        ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        UpdateCommand="update messages SET description=@description,date=GETDATE() WHERE id = @id"
        InsertCommand="INSERT INTO messages (type,date,description,begdate,enddate) VALUES ('Outage',@date,@description,@begdate,@enddate)"
        DeleteCommand="DELETE FROM messages WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvOutage" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="date" Type="DateTime" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue"
                ControlID="gvOutage"></asp:ControlParameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Type="DateTime" Name="date"></asp:Parameter>
            <asp:Parameter Type="String" Name="description"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="begdate"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="enddate"></asp:Parameter>    
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {   
                $(".datetimepickerclass").datetimepicker();
            });
        </script>
</asp:Content>