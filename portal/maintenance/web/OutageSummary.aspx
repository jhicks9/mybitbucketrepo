﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="OutageSummary.aspx.vb" Inherits="maintenance_web_OutageSummary" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-12">
                <asp:label runat="server" id="lblStatus" />
                <asp:UpdatePanel ID="updateOutageLog" runat="server">
                    <ContentTemplate>
                <asp:GridView ID="gvData" runat="server" AllowPaging="true" AllowSorting="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="date" HeaderText="Date" SortExpression="date" />
                        <asp:TemplateField HeaderText="Zone" SortExpression="zone">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="false" CommandName="Select" CommandArgument='<%# Eval("date")& ";" & Eval("zone") & ";" & Eval("description") %>'
                                ToolTip="Edit entry" Text='<%# Bind("zone") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="description" HeaderText="Description" SortExpression="description" />
                        <asp:BoundField DataField="outage" HeaderText="Outage" SortExpression="outage" />
                        <asp:BoundField DataField="logid" HeaderText="Log ID" />
                        <asp:BoundField DataField="data" HeaderText="Data" Visible="false" />
                    </Columns>
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
                    </ContentTemplate> 
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="modal modal-wide fade" id="chartDetail" tabindex="-1" role="dialog" aria-labelledby="chartDetailLabel">
            <div class="modal-dialog" style="width:1285px !important" role="document">
                <div class="modal-content" >
                    <div class="modal-body" style="width:1285px !important">
                        <asp:UpdatePanel ID="updateChartDetails" runat="server">
                            <contenttemplate>
                                <asp:Chart ID="Chart1" runat="server" class="img-responsive form-control" Width="1280px" Height="450px" TextAntiAliasingQuality="Normal" 
                                     AntiAliasing="All"  BorderSkin-SkinStyle="Emboss" Palette="BrightPastel" BorderlineWidth="1" BackGradientStyle="TopBottom" BorderlineDashStyle="Solid"
                                     BorderSkin-BackColor="#FFFFFF" BorderSkin-PageColor="#FFFFFF" BackSecondaryColor="White" BorderlineColor="#1A3b69" BackColor="#D3DFF0">
                                    <Legends>
                                        <asp:Legend Name="Default"></asp:Legend>
                                    </Legends>
                                    <Titles>
                                        <asp:Title Name="Default Title" Text="">
                                        </asp:Title>
                                        <asp:Title Name="sub Title" Text="">
                                        </asp:Title>
                                    </Titles>
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                            <Area3DStyle Enable3D="False" LightStyle="Realistic" />
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span>&nbsp;<b>Close</b></button>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel><!-- pnlAuthorized -->
</asp:Content>

