Imports System.Data.SqlClient
Imports System.data

Partial Class maintenance_web_UserSecurity
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
            gvUsers.DataSource = GetUsers()
            gvUsers.DataBind()
            If Not Page.IsPostBack Then
                liUserSecurity.Attributes.Add("class","active")
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbUserSecurityPanel_Click(sender As Object, e As EventArgs) Handles lbUserSecurityPanel.Click
        hidePanels()
        pnlUserSecurityPanel.Visible = True
        liUserSecurity.Attributes.Add("class","active")
    End Sub
    Protected Sub lbGroupSecurityPanel_Click(sender As Object, e As EventArgs) Handles lbGroupSecurityPanel.Click
        hidePanels()
        pnlGroupSecurityPanel.Visible = True
        liGroupSecurity.Attributes.Add("class","active")
    End Sub
    Protected Sub lbDocumentTypePanel_Click(sender As Object, e As EventArgs) Handles lbDocumentTypePanel.Click
        hidePanels()
        pnlDocumentTypePanel.Visible = True
        liDocumentType.Attributes.Add("class", "active")
    End Sub
    Protected Sub lbGroupsPanel_Click(sender As Object, e As EventArgs) Handles lbGroupsPanel.Click
        hidePanels()
        pnlGroupsPanel.Visible = True
        liGroups.Attributes.Add("class", "active")
    End Sub
    Sub hidePanels ()
        pnlUserSecurityPanel.Visible = False
        pnlGroupSecurityPanel.Visible = False
        pnlDocumentTypePanel.Visible = False
        pnlGroupsPanel.Visible = False
        liUserSecurity.Attributes.Remove("class")
        liGroupSecurity.Attributes.Remove("class")
        liDocumentType.Attributes.Remove("class")
        liGroups.Attributes.Remove("class")
    End Sub

    '============================== Group Security Maintenance ===========================================================================
    Protected Sub gvGroups_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroups.PageIndexChanging
        gvGroups.SelectedIndex = -1
        gvGroupUsers.DataSource = Nothing
        gvGroupUsers.DataBind()
        cblDetails.DataSource = GetDocTypebyGroup("")
        cblDetails.DataTextField = "description"
        cblDetails.DataValueField = "id"
        cblDetails.DataBind()
        btnUpdate.Visible = False
        pDocumentTypes.Visible = False
        gvGroupUsers.Visible = False
        'pnlGroups.Update()
    End Sub

    Protected Sub gvGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroups.RowCommand
        Select Case e.CommandName
            Case "Select"
                sqlDSUsers.SelectParameters("groupid").DefaultValue = e.CommandArgument
                sqlDSUsers2.InsertParameters(0).DefaultValue = e.CommandArgument
                gvGroupUsers.DataBind()
                gvGroupUsers.SelectedIndex = -1
                cblDetails.DataSource = GetDocTypebyGroup(e.CommandArgument)  'populate full list of group's document types
                cblDetails.DataTextField = "description"
                cblDetails.DataValueField = "id"
                cblDetails.DataBind()
                btnUpdate.Visible = False
                pDocumentTypes.Visible = True
                gvGroupUsers.Visible = True
            Case Else
        End Select
    End Sub

    Protected Sub gvGroupUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroupUsers.RowCommand
        Select Case e.CommandName
            Case "Select"
                If e.CommandArgument = "EditRecord" Then  'Edit the username
                    UsersDetails.HeaderText = "Edit user"
                    UsersDetails.ChangeMode(DetailsViewMode.Edit)
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#userDetail').modal('show');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "UserDetailModalScript", sb.ToString(), False)
                Else  ' Select the username to edit the document types
                    populateDetails(e.CommandArgument)
                    btnUpdate.Visible = True
                    btnUpdate.CommandName = e.CommandArgument
                End If
            Case "AddRecord"
                gvGroupUsers.SelectedIndex = -1
                UsersDetails.HeaderText = "Add"
                UsersDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#userDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "UserDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub UsersDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles UsersDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                gvGroupUsers.SelectedIndex = -1
                sqlDSUsers.SelectParameters("groupid").DefaultValue = gvGroups.SelectedDataKey.Value
                sqlDSUsers2.InsertParameters(0).DefaultValue = gvGroups.SelectedDataKey.Value
                btnUpdate.Visible = False
            Case "Cancel"
                gvGroupUsers.SelectedIndex = -1
                btnUpdate.Visible = False
            Case "Update"
                gvGroupUsers.SelectedIndex = -1
                sqlDSUsers.SelectParameters("groupid").DefaultValue = gvGroups.SelectedDataKey.Value
                sqlDSUsers2.InsertParameters(0).DefaultValue = gvGroups.SelectedDataKey.Value
                sqlDSUsers.DataBind()
                btnUpdate.Visible = False
            Case "Insert"
                gvGroupUsers.SelectedIndex = -1
                sqlDSUsers.SelectParameters("groupid").DefaultValue = gvGroups.SelectedDataKey.Value
                sqlDSUsers2.InsertParameters(0).DefaultValue = gvGroups.SelectedDataKey.Value
                sqlDSUsers.DataBind()
        End Select
    End Sub

    Protected Sub UsersDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles UsersDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblGroupUsersStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvGroupUsers.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#userDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "UserDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub UsersDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles UsersDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(UsersDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvGroupUsers.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#userDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "UserDetailModalScript", sb.ToString(), False)
        End If

    End Sub

    Protected Sub UsersDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles UsersDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(UsersDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvGroupUsers.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#userDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "UserDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        updateDetails()
    End Sub
    '============================== End Group Security Maintenance ==================================================================

    '============================== Doc Types Maintenance ===========================================================================
    Protected Sub gvDocTypes_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDocTypes.RowCommand
        Select Case e.CommandName
            Case "Select"
                DocTypesDetails.HeaderText = "Edit"
                DocTypesDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#docTypeDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "docTypeDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvDocTypes.SelectedIndex = -1
                DocTypesDetails.HeaderText = "Add"
                DocTypesDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#docTypeDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "docTypeDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub
    Protected Sub DocTypesDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles DocTypesDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvDocTypes.SelectedIndex = -1
        lblDocTypesStatus.Text = ""
    End Sub
    Protected Sub DocTypesDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles DocTypesDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblDocTypesStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvDocTypes.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#docTypeDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "docTypeDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    Protected Sub DocTypesDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles DocTypesDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(DocTypesDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvDocTypes.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#docTypeDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "docTypeDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    Protected Sub DocTypesDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles DocTypesDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(DocTypesDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvDocTypes.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#docTypeDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "docTypeDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    '============================== End Doc Types Maintenance =======================================================================

    '============================== Group Maintenance ===============================================================================
    Protected Sub gvGroup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup.RowCommand
        Select Case e.CommandName
            Case "Select"
                If e.CommandArgument = "EditRecord" Then  'Edit the group
                    GroupDetails.HeaderText = "Edit group"
                    GroupDetails.ChangeMode(DetailsViewMode.Edit)
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#GroupDetail').modal('show');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GroupDetailModalScript", sb.ToString(), False)
                Else  ' Select the group to edit the document types
                    populateGroupDetails(e.CommandArgument)
                    btnUpdateGroup.Visible = True
                    btnUpdateGroup.CommandName = e.CommandArgument
                End If
            Case "AddRecord"
                gvGroup.SelectedIndex = -1
                GroupDetails.HeaderText = "Add"
                GroupDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#GroupDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GroupDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub
    Protected Sub GroupDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles GroupDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvGroup.SelectedIndex = -1
        lblGroupStatus.Text = ""
    End Sub

    Protected Sub GroupDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles GroupDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(GroupDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvGroup.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#GroupDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GroupDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub GroupDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles GroupDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblGroupStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvGroup.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#GroupDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GroupDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub GroupDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles GroupDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(GroupDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvGroup.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#GroupDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "GroupDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub btnUpdateGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateGroup.Click
        updateGroupDetails()
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.SelectedIndex = -1
        Dim itm As ListItem
        For Each itm In cblGroupDetails.Items  'clear old entries
            itm.Selected = False
        Next
        btnUpdateGroup.Visible = False
    End Sub
    '============================== End Group Maintenance ===========================================================================

    Protected Sub gvUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvUsers.RowCommand
        Select Case e.CommandName
            Case "Select"
                gvSummary.DataSource = GetSummary(e.CommandArgument)
                gvSummary.DataBind()
                pnlUsers.Update()
            Case Else
        End Select
    End Sub
    Protected Sub gvUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUsers.PageIndexChanging
        gvUsers.DataSource = GetUsers()
        gvUsers.PageIndex = e.NewPageIndex
        gvUsers.DataBind()
        gvUsers.SelectedIndex = -1
        gvSummary.DataSource = Nothing
        gvSummary.DataBind()
        pnlUsers.Update()
    End Sub

    Public Sub updateDetails()
        'Try
            Dim itm As ListItem
            Dim buildstr As String = ""

            For Each itm In cblDetails.Items  'build doctypeid string
                If itm.Selected Then
                    If Len(buildstr) < 1 Then
                        buildstr += itm.Value
                    Else
                        buildstr += "," & itm.Value
                    End If
                End If
            Next
            Dim sql As String = "update security set security.doctypeid = '" & buildstr & "' where security.id = @securityid"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.Parameters.AddWithValue("@securityid", btnUpdate.CommandName)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            dr.Close()
            conn.Close()
        'Catch
        'End Try
    End Sub
    Public Sub updateGroupDetails()
        'Try
            Dim itm As ListItem
            Dim buildstr As String = ""

            For Each itm In cblGroupDetails.Items  'build doctypeid string
                If itm.Selected Then
                    If Len(buildstr) < 1 Then
                        buildstr += itm.Value
                    Else
                        buildstr += "," & itm.Value
                    End If
                End If
            Next
            Dim sql As String = "update groups set groups.doctypeid = '" & buildstr & "' where groups.id = @groupid"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.Parameters.AddWithValue("@groupid", btnUpdateGroup.CommandName)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            dr.Close()
            conn.Close()
        'Catch
        'End Try
    End Sub
    Public Sub populateDetails(ByVal id As String)
        Try
            Dim doctypes As String = ""
            Dim sql As String = "select doctypeid from security where id = @id"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.Parameters.AddWithValue("@id", id)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            While dr.Read
                doctypes = dr(0).ToString  'read list of doctypes from userid/group
            End While
            dr.Close()
            conn.Close()

            Dim aryRecord As String() = Split(doctypes, ",")  'create an array of on file doctypes
            Dim item As String
            Dim itm As ListItem
            For Each itm In cblDetails.Items  'clear old entries
                itm.Selected = False
            Next
            For Each item In aryRecord  'auto select each doctype on file
                For Each itm In cblDetails.Items
                    If itm.Value = item Then
                        itm.Selected = True
                    End If
                Next
            Next item
        Catch
        End Try
    End Sub
    Public Sub populateGroupDetails(ByVal id As String)
        Try
            Dim doctypes As String = ""
            Dim sql As String = "select doctypeid from groups where id = @id"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.Parameters.AddWithValue("@id", id)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            While dr.Read
                doctypes = dr(0).ToString  'read list of doctypes from userid/group
            End While
            dr.Close()
            conn.Close()

            Dim aryRecord As String() = Split(doctypes, ",")  'create an array of on file doctypes
            Dim item As String
            Dim itm As ListItem
            For Each itm In cblGroupDetails.Items  'clear old entries
                itm.Selected = False
            Next
            For Each item In aryRecord  'auto select each doctype on file
                For Each itm In cblGroupDetails.Items
                    If itm.Value = item Then
                        itm.Selected = True
                    End If
                Next
            Next item
        Catch
        End Try
    End Sub
    Public Function GetDocTypebyGroup(ByVal group As String) As DataTable
        Dim doctypeIDs As String = ""
        Dim sql As String = ""
        Dim dt As New System.Data.DataTable("doctypes")
        Try
            Dim drow As System.Data.DataRow
            Dim dc As DataColumn
            dc = New DataColumn("id", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("description", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim dr As SqlDataReader

            sql = "select doctypeid from groups where id = " & group
            Dim cmd1 As New SqlCommand(sql, conn)
            dr = cmd1.ExecuteReader
            While dr.Read
                doctypeIDs = dr(0).ToString
            End While
            dr.Close()

            sql = "select id,description from doctype where id in(" & doctypeIDs & ") order by description asc"
            Dim cmd As New SqlCommand(sql, conn)
            dr = cmd.ExecuteReader
            While dr.Read
                drow = dt.NewRow()
                drow.Item("id") = dr(0).ToString
                drow.Item("description") = dr(1).ToString
                dt.Rows.Add(drow)
            End While
            dr.Close()
            conn.Close()
        Catch
            Return dt
        End Try
        Return dt
    End Function
    Public Function GetUsers() As DataTable
        Dim dt As New System.Data.DataTable("users")
        Try
            Dim drow As System.Data.DataRow
            Dim dc As DataColumn
            dc = New DataColumn("name", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            Dim sql As String = "select distinct userid from security order by userid asc"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd As New SqlCommand(sql, conn)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            While dr.Read
                drow = dt.NewRow()
                drow.Item("name") = dr(0).ToString
                dt.Rows.Add(drow)
            End While
            dr.Close()
            conn.Close()

        Catch ex As Exception
            Return dt
        End Try
        Return dt
    End Function
    Public Function GetSummary(ByVal username As String) As DataTable
        Dim dt As New System.Data.DataTable("users")
        Try
            Dim drow As System.Data.DataRow
            Dim dc As DataColumn
            dc = New DataColumn("group", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("doctype", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            Dim sql As String = "SELECT groups.description,security.doctypeid FROM security INNER JOIN groups ON security.groupid = groups.id WHERE (security.userid = '" & username & "') ORDER BY groups.description"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Dim conn1 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            conn1.Open()
            Dim cmd As New SqlCommand(sql, conn)
            Dim cmd1 As New SqlCommand(sql, conn1)
            Dim dr, dr1 As SqlDataReader
            dr = cmd.ExecuteReader
            While dr.Read
                Dim aryRecord As String() = Split(dr(1).ToString, ",")
                Dim item As String
                For Each item In aryRecord
                    cmd1.CommandText = "select description from doctype where id = " & item
                    dr1 = cmd1.ExecuteReader
                    While dr1.Read
                        drow = dt.NewRow()
                        drow.Item("group") = dr(0).ToString
                        drow.Item("doctype") = dr1(0).ToString
                        dt.Rows.Add(drow)
                    End While
                    dr1.Close()
                Next item
            End While
            dr.Close()
            conn1.Close()
            conn.Close()
        Catch ex As Exception
            Return dt
        End Try
        Return dt
    End Function

    Function GetSelectedIndex(ByVal state As Boolean) As Integer
        Dim i As Integer = 0
        If DocTypesDetails.CurrentMode = DetailsViewMode.Insert Then
            i = 1  'default to true for all new entries
        Else
            Select Case state
                Case True
                    i = 1
                Case False
                    i = 0
            End Select
        End If

        Return i
    End Function

End Class
