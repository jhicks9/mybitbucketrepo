Imports System.Data.SqlClient

Partial Class Maintenance_web_DepositRates
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub gvInterest_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvInterest.RowCommand
        Select Case e.CommandName
            Case "Select"
                InterestDetails.HeaderText = "Edit"
                InterestDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#interestDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvInterest.SelectedIndex = -1
                InterestDetails.HeaderText = "Add"
                InterestDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#interestDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
            Case Else
                InterestDetails.ChangeMode(DetailsViewMode.ReadOnly)
        End Select
    End Sub

    Protected Sub InterestDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles InterestDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvInterest.SelectedIndex = -1
    End Sub

    Protected Sub InterestDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles InterestDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblInterestStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvInterest.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#interestDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
        End If
    End Sub
    Protected Sub InterestDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles InterestDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(InterestDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvInterest.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#interestDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub InterestDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles InterestDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(InterestDetails.FindControl("lblError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvInterest.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#interestDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "OutageDetailModalScript", sb.ToString(), False)
        End If
    End Sub
End Class
