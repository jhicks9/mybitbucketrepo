﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="maintenance_web_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="list-group">
                <div class="list-group-item active"><span class="glyphicon glyphicon-globe"></span>&nbsp;Web Maintenance</div>
                <a href="~/maintenance/web/AdminDocs.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Document Edit<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/AccountsSummary.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-user"></span>&nbsp;Account Summary<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/EmpireAnnouncements.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-bullhorn"></span>&nbsp;Empire Announcements<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/WebLinks.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-link"></span>&nbsp;Website Links<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/DepositRates.aspx" runat="server" class="list-group-item">
                    <span class="glyphicon glyphicon-usd"></span>&nbsp;Deposit Interest<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/BillingInserts.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-pdf-o"></span>&nbsp;Bill Inserts<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/PaymentLocations.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-map-marker"></span>&nbsp;Payment Locations<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/AssistanceAgencyList.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-map-marker"></span>&nbsp;Assistance Agencies<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/OutageSummary.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-bolt"></span>&nbsp;Outage Summary<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/CommAsstAccounts.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-user"></span>&nbsp;Comm/Agency Accounts<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/ConvertPLY.aspx" runat="server" class="list-group-item">
                    <span class="glyphicon glyphicon-globe"></span>&nbsp;Convert PLY file<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/Modules.aspx" runat="server" class="list-group-item">
                    <span class="glyphicon glyphicon-picture"></span>&nbsp;Manage Modules<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/controls/SiteSecurity.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-lock"></span>&nbsp;Site Security<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/UserSecurity.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-lock"></span>&nbsp;User Security<span class="pull-right fa fa-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</asp:Content>

