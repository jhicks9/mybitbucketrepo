<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="WebLinks.aspx.vb" Inherits="Maintenance_web_WebLinks" title="Web Links Maintenance" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="form-control">
                            <asp:ListItem Text="Links of Interest" Value="linksofinterest" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Energy Calculators" Value="energycalculators"></asp:ListItem>
                            <asp:ListItem Text="Energy Calculators [Gas]" Value="energycalculators_gas"></asp:ListItem>
                            <asp:ListItem Text="Presentations" Value="presentations"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Residential AR" Value="energysolutionselectric_r_AR"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Business AR" Value="energysolutionselectric_b_AR"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Education AR" Value="energysolutionselectric_e_AR"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Residential MO" Value="energysolutionselectric_r_MO"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Residential MO [Gas]" Value="energysolutionsgas_r_MO"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Business MO" Value="energysolutionselectric_b_MO"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Business MO [Gas]" Value="energysolutionsgas_b_MO"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Residential KS" Value="energysolutionselectric_r_KS"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Business KS" Value="energysolutionselectric_b_KS"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Residential OK" Value="energysolutionselectric_r_OK"></asp:ListItem>
                            <asp:ListItem Text="Energy Solutions Business OK" Value="energysolutionselectric_b_OK"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <asp:GridView ID="gvLinks" runat="server" DataSourceID="dsLinks" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbAdd" runat="server" CausesValidation="false" CommandName="AddRecord"
                                            ToolTip="Add new link" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add link</span></asp:LinkButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbSelectArkansas" runat="server" CausesValidation="false" CommandName="Select"
                                            ToolTip="Edit link" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="listorder" HeaderText="Order" SortExpression="listorder" />
                                <asp:BoundField DataField="title" HeaderText="Name" SortExpression="title" />
                                <asp:BoundField DataField="url" HeaderText="Destination URL" SortExpression="url" />
                                <asp:BoundField DataField="date" HeaderText="Last Update" SortExpression="date" />
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:LinkButton ID="lbAddEmpty" runat="server" CausesValidation="False" ToolTip="Add new link" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add link</span></asp:LinkButton>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate> 
        </asp:UpdatePanel>                

        <div class="modal fade" id="linkDetail" tabindex="-1" role="dialog" aria-labelledby="linkDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateLinksDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="linksDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSA2" CssClass="table table-striped table-bordered table-hover" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False" />
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("title") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                
                                        <asp:templatefield headertext="Destination URL" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtURL" runat="server" Text='<%# Bind("url") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvURL" runat="server" ControlToValidate="txtURL" ErrorMessage="Destination URL is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                     
                                        <asp:templatefield headertext="Order" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Bind("listorder") %>' CssClass="col-md-9 form-control"/>
                                            <asp:RequiredFieldValidator ID="rfvOrder" runat="server" ControlToValidate="txtOrder" ErrorMessage="Order is required" SetFocusOnError="True" Display="Dynamic" />
                                          </edititemtemplate>                                  
                                        </asp:templatefield>                                                                                     
                                        <asp:templatefield headertext="New Window" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <EditItemTemplate>
                                            <asp:DropDownList ID="ddlWindow" runat="server" SelectedValue='<%# Bind("newwindow") %>' SelectedIndex='<%#GetSelectedIndex( DataBinder.Eval(Container.DataItem,"newwindow") ) %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>False</asp:ListItem>
                                                <asp:ListItem>True</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                          </EditItemTemplate>
                                        </asp:templatefield>                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblLinksStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div><%--linkDetail--%>
    </asp:Panel><!-- pnlAuthorized -->
    
    <asp:SqlDataSource ID="dsLinks" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,title,url,CONVERT(VARCHAR(10),date,101) AS date,listorder,newwindow from links where type = @linkType order by listorder,title">
        <SelectParameters>
            <asp:ControlParameter Name="linkType" ControlID="ddlType"  PropertyName="SelectedValue"/>
        </SelectParameters>
    </asp:SqlDataSource>                                                    
    <asp:SqlDataSource ID="sqlDSA2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,title,url,CONVERT(VARCHAR(10),date,101) AS date,listorder,newwindow from links where (links.id = @id)"
        InsertCommand="INSERT INTO links (type,title,url,listorder,newwindow,date) VALUES (@type,@title,@url,@listorder,@newwindow,GETDATE())"
        UpdateCommand="UPDATE links SET title=@title,url=@url,listorder=@listorder,newwindow=@newwindow,date=GETDATE() WHERE (links.id = @id)"
        DeleteCommand="DELETE FROM links WHERE id = @id">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvLinks" Type="Int32" Name="id" PropertyName="SelectedValue"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="title" Type="String" />
            <asp:Parameter Name="url" Type="String" />
            <asp:Parameter Name="listorder" Type="String" />
            <asp:Parameter Name="newwindow" Type="Boolean" />
            <asp:Parameter Name="date" Type="DateTime" />
        </UpdateParameters>                 
        <InsertParameters>
            <asp:ControlParameter Name="type" ControlID="ddlType"  PropertyName="SelectedValue"/>
            <%--<asp:Parameter Name="type" Type="String" DefaultValue="linksofinterest"/>--%>
            <asp:Parameter Name="title" Type="String" />
            <asp:Parameter Name="url" Type="String" />
            <asp:Parameter Name="listorder" Type="String" />
            <asp:Parameter Name="newwindow" Type="Boolean" />            
            <asp:Parameter Name="date" Type="DateTime" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>