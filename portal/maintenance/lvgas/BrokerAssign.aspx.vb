
Partial Class Maintenance_LVgas_BrokerAssign
    Inherits System.Web.UI.Page
    
    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load      
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try

        Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
        'Dim info As ProfileInfo
        lblBrokerMessage.Text = ""

        If ListBox1.SelectedIndex <> -1 Then
            lblBroker.Text = ListBox1.SelectedItem.ToString
        End If
        'ListBox1.Items.Clear()
        ListBox1.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("broker")
        ListBox1.DataBind()
        'For Each info In profiles
        '    ' We need to turn a ProfileInfo into a ProfileCommon 
        '    ' to access the properties to do the search
        '    Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName)
        '    ' Now to just assign those profiles that are brokers
        '    If userProfile.BrokerAssigned.Length < 1 Then
        '        ListBox1.Items.Add(userProfile.UserName)
        '    End If
        'Next
    End Sub
    
  
    Protected Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged

        Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
        Dim info As ProfileInfo
        CheckBoxList1.Items.Clear()
        For Each info In profiles
            ' We need to turn a ProfileInfo into a ProfileCommon 
            ' to access the properties to do the search
            Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName)
            ' Now to just assign those profiles that are brokers            
            If userProfile.CustomerAccountNumber.Length > 1 Then
                CheckBoxList1.Items.Add(New ListItem(userProfile.UserName & " - " & userProfile.CustomerAccountNumber, userProfile.BrokerAssigned))                                
                Dim lstBroker As ListItem
                For Each lstBroker In CheckBoxList1.Items
                    If lblBroker.Text = lstBroker.Value Then
                        lstBroker.Selected = True
                    End If                   
                Next
            End If
        Next       
    End Sub


    Protected Sub Button1_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim list As ListItem
        Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
        Dim info As ProfileInfo
        If lblBroker.Text <> "" Then
            lblBrokerMessage.Text += "Changes Saved. <br/> The following accounts are assigned to " & lblBroker.Text & ": <br/><br/>"
            Dim txtProfileAcctNum = ""
            For Each list In CheckBoxList1.Items
                If list.Selected Then
                    For Each info In profiles
                        ' We need to turn a ProfileInfo into a ProfileCommon 
                        ' to access the properties to do the search
                        Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName, True)
                        ' Now to just assign those profiles that are brokers            
                        If userProfile.CustomerAccountNumber.Length > 1 Then
                            txtProfileAcctNum = userProfile.UserName & " - " & userProfile.CustomerAccountNumber
                            If txtProfileAcctNum = list.Text Then
                                userProfile.BrokerAssigned = lblBroker.Text.ToUpper
                                lblBrokerMessage.Text += list.Text & "<br/>" & "<br/>"                            
                            End If
                        End If
                        userProfile.Save()
                    Next
                End If
            Next
            pnlAssign.Update()
        End If
    End Sub

End Class
