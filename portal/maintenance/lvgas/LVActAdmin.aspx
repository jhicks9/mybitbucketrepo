<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="LVActAdmin.aspx.vb" Inherits="Maintenance_LVgas_LVActAdmin" title="LVG Accounts" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="container_1">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Large Volume Transport Accounts</a></li>
                <li><a href="#tab_b" data-toggle="pill">Gas Broker Accounts</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateAccountsT" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAccountsT" runat="server" DataSourceID="sqlLVT" DataKeyNames="UserId" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="true">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddA" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                    ToolTip="Add new account" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectA" runat="server" CausesValidation="false" CommandName="Select" CommandArgument='<%# Bind("UserId") %>'
                                                    ToolTip="Edit account" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UserName" HeaderText="Username" SortExpression="UserName" />
                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                        <asp:BoundField DataField="CreateDate" HeaderText="Created" SortExpression="CreateDate" />
                                        <asp:BoundField DataField="LastLoginDate" HeaderText="Last Accessed" SortExpression="LastLoginDate" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddAEmpty" runat="server" CausesValidation="False" ToolTip="Add new account" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate> 
                        </asp:UpdatePanel>                                                                     
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateAccountsB" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAccountsB" runat="server" DataSourceID="sqlLVB" DataKeyNames="UserId" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="true">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAddB" runat="server" CausesValidation="false" CommandName="AddRecord"
                                                ToolTip="Add new account" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectB" runat="server" CausesValidation="false" CommandName="Select" CommandArgument='<%# Bind("UserId") %>'
                                                    ToolTip="Edit account" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UserName" HeaderText="Username" SortExpression="UserName" />
                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                        <asp:BoundField DataField="CreateDate" HeaderText="Created" SortExpression="CreateDate" />
                                        <asp:BoundField DataField="LastLoginDate" HeaderText="Last Accessed" SortExpression="LastLoginDate" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                       <asp:LinkButton ID="lbAddBEmpty" runat="server" CausesValidation="False" ToolTip="Add new account" CommandName="AddRecord" CssClass="btn btn-primary"><span class="fa fa-plus">&nbsp;Add account</span></asp:LinkButton>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->
            </div><!-- tab-content -->
        </div><!-- tab container -->


        <div class="modal fade" id="AccountsTDetail" tabindex="-1" role="dialog" aria-labelledby="AccountsTDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateAccountsTDetails" runat="server">
                            <contenttemplate>	                    
                                <asp:DetailsView ID="AccountsTDetails" runat="server" DataKeyNames="UserId" DataSourceID="sqlLVT2" CssClass="table table-striped table-bordered" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="UserId" SortExpression="UserId" HeaderText="UserId" ReadOnly="True" Visible="False"></asp:BoundField>
                                        <asp:templatefield headertext="UserName" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>' CssClass="col-md-9 no-border"/>
                                          </ItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Profile data" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblCustomerToken" runat="server" Text='<%# eval("PropertyValuesString").ToString %>' CssClass="col-md-9 no-border"/>
                                          </ItemTemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Confirm Password" SortExpression="Password2" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control"/>
                                            <asp:CompareValidator id="ConfirmPassword" runat="server" ControlToValidate="txtPassword2" ForeColor="red"
                                                Display="Dynamic" ControlToCompare="txtPassword1" ErrorMessage="Passwords must match." ValidationGroup="AccountsT"/>
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Email" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("email") %>' CssClass="col-md-9 form-control"/>
                                                <asp:RegularExpressionValidator ID="ValidEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Format" 
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic" ValidationGroup="AccountsT" />
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Status" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" />
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                                     
                                        <asp:TemplateField >
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="lbEditUpdate" CommandName="Upd" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="AccountsT"/>
                                                <asp:LinkButton ID="lbEditCancel" CommandName="Can" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                                                <asp:LinkButton ID="lbEditDelete" CommandName="Del" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="AccountsBDetail" tabindex="-1" role="dialog" aria-labelledby="AccountsBDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateAccountsBDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="AccountsBDetails" runat="server" DataKeyNames="UserId" DataSourceID="sqlLVB2" CssClass="table table-striped table-bordered" AutoGenerateRows="False">
                                    <Fields>
                                        <asp:BoundField DataField="UserId" SortExpression="UserId" HeaderText="UserId" ReadOnly="True" Visible="False" />
                                        <asp:templatefield headertext="UserName" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <ItemTemplate>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>' CssClass="col-md-9 form-control no-border" />
                                          </ItemTemplate>                                                                
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control" />
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Confirm Password" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" Text="" CssClass="col-md-9 form-control" />
                                            <asp:CompareValidator id="ConfirmPassword" runat="server" ControlToValidate="txtPassword2" ForeColor="red"
                                                Display="Dynamic" ControlToCompare="txtPassword1" ErrorMessage="Passwords must match." ValidationGroup="AccountsB" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Email" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("email") %>' CssClass="col-md-9 form-control" />
                                                <asp:RegularExpressionValidator ID="ValidEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Format"  ValidationGroup="AccountsB"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">Invalid Email Format</asp:RegularExpressionValidator>
                                          </edititemtemplate>
                                        </asp:templatefield>                                
                                        <asp:templatefield headertext="Status" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Text="" />
                                          </edititemtemplate>
                                        </asp:templatefield>                                                                
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <EditItemTemplate>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Upd" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="AccountsB"/>
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Can" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Del" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="AddAccount" tabindex="-1" role="dialog" aria-labelledby="AddDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateAddAccount" runat="server">
                            <contenttemplate>                            
                                <table class="table table-striped table-bordered">
		                            <tr>
			                            <td colspan="2" class="col-lg-12"><p class="well-sm bg-info text-center"><strong><asp:Label ID="lblType" runat="server" Text="" /></strong></p></td>
		                            </tr>
                                    <asp:panel ID="pnllvtransport" runat="server" Visible="true" >
                                        <tr>
                                            <td class="field-label col-xs-3 active">Customer Token</td>
                                            <td>
                                                <asp:TextBox ID="txtCustAcctTkn" runat="server" CssClass="col-md-9 form-control" />
                                                <asp:RequiredFieldValidator ID="rfvCustAcctTkn" runat="server" ControlToValidate="txtCustAcctTkn" ErrorMessage="required"
                                                    ValidationGroup="CreateUserWizard1" Display="Dynamic" />
                                                <asp:Label ID="lblInvalidAcct" runat="Server" ForeColor="Red" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active"></td>
                                            <td><asp:CheckBox ID="ckSales" runat="server" Text="Sales" CssClass="col-md-9 form-control" /></td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active">Broker Assigned</td>
                                            <td>
                                                <asp:DropDownList ID="dlsBrokers" runat="server" CssClass="col-md-9 form-control"  >
                                                </asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="rfvBrokers" runat="server" ControlToValidate="dlsBrokers" ErrorMessage="required"
                                                    ValidationGroup="CreateUserWizard1" Display="Dynamic" />--%>
                                                <asp:SqlDataSource ID="sqlUsersInRole0" runat="server" ConnectionString="<%$ ConnectionStrings:edgmembershipConnectionString%>"
                                                    SelectCommand="GetUsersInRole" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:Parameter DefaultValue="broker" Name="rolename" Type="String" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field-label col-xs-3 active">Pool Assigned</td>
                                            <td>
                                                <asp:DropDownList ID="dlsPools" runat="server" CssClass="col-md-9 form-control">
                                                </asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="rfvPools" runat="server" ControlToValidate="dlsPools" ErrorMessage="required"
                                                    ValidationGroup="CreateUserWizard1" Display="Dynamic" />--%>
                                            </td>
                                        </tr>
                                    </asp:panel>
                                    <tr>
                                        <td class="field-label col-xs-3 active">Business Name</td>
                                        <td>
                                            <asp:TextBox ID="txtBusinessName" runat="server" CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBusinessName" ErrorMessage="required"
                                                ValidationGroup="CreateUserWizard1" Display="Dynamic" />
                                        </td>
                                    </tr>
                                </table>

                                <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" MembershipProvider="edgMembershipProvider" LoginCreatedUser="False" Width="100%" DisplayCancelButton="true">
                                    <WizardSteps>
                                        <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                            <ContentTemplate>
                                                <table class="table table-striped table-bordered">
                                                    <tr>
                                                        <td class="field-label col-xs-3 active">User Name</td>
                                                        <td>
                                                            <asp:TextBox ID="UserName" runat="server" CssClass="col-md-9 form-control" />
                                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"  ErrorMessage="required"
                                                                 ValidationGroup="CreateUserWizard1" Display="Dynamic" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field-label col-xs-3 active">Password</td>
                                                        <td>
                                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="col-md-9 form-control" />
                                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ValidationGroup="CreateUserWizard1" 
                                                                Display="Dynamic" ErrorMessage="required" />
                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Password"
                                                                ControlToValidate="UserName" Display="Dynamic" ErrorMessage="User Name and Password cannot be the same"
                                                                Operator="NotEqual" ValidationGroup="CreateUserWizard1" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field-label col-xs-3 active">Confirm Password</td>
                                                        <td>
                                                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" CssClass="col-md-9 form-control" />
                                                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="required"
                                                                ValidationGroup="CreateUserWizard1" Display="Dynamic" />
                                                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                                                ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="Password and Confirm Password must match."
                                                                ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field-label col-xs-3 active">E-mail</td>
                                                        <td>
                                                            <asp:TextBox ID="Email" runat="server" CssClass="col-md-9 form-control" />
                                                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" ValidationGroup="CreateUserWizard1"
                                                                Display="Dynamic" ErrorMessage="required" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="color: red">
                                                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>                                
                                            </ContentTemplate>
                                            <CustomNavigationTemplate>
                                                <div class="pull-right">
                                                    <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" CausesValidation="true"
                                                        Text="Create User" ValidationGroup="CreateUserWizard1" CssClass="btn btn-primary"/>
                                                    <asp:Button ID="Button1" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false"
                                                        ValidationGroup="CreateUserWizard1" CssClass="btn btn-default" />
                                                </div>
                                            </CustomNavigationTemplate>
                                        </asp:CreateUserWizardStep>
                                        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server"></asp:CompleteWizardStep>
                                    </WizardSteps>
                                </asp:CreateUserWizard>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div><!-- AddDetail -->
    </asp:Panel><!-- pnlAuthorized -->
    
    <asp:SqlDataSource ID="sqlLVT" runat="server" ConnectionString="<%$ ConnectionStrings:edgmembershipConnectionString%>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate, aspnet_Roles.RoleName FROM aspnet_Users INNER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId INNER JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Roles.RoleName = 'lvtransport')">
    </asp:SqlDataSource>     
    <asp:SqlDataSource ID="sqlLVT2" runat="server" ConnectionString="<%$ ConnectionStrings:edgmembershipConnectionString%>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Profile.PropertyValuesString FROM aspnet_Users INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId inner join aspnet_profile on aspnet_users.userid = aspnet_profile.userid WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvAccountsT" Name="UserId" PropertyName="SelectedValue"/>
        </SelectParameters>        
    </asp:SqlDataSource>          
    <asp:SqlDataSource ID="sqlLVB" runat="server" ConnectionString="<%$ ConnectionStrings:edgmembershipConnectionString%>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate, aspnet_Roles.RoleName FROM aspnet_Users INNER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId INNER JOIN aspnet_Roles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Roles.RoleName = 'broker')">
    </asp:SqlDataSource>  
    <asp:SqlDataSource ID="sqlLVB2" runat="server" ConnectionString="<%$ ConnectionStrings:edgmembershipConnectionString%>"
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email FROM aspnet_Users INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvAccountsB" Name="UserId" PropertyName="SelectedValue"/>
        </SelectParameters>        
    </asp:SqlDataSource>               
</asp:Content>

