﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="maintenance_lvgas_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="list-group">
                <div class="list-group-item active">EDG Maintenance</div>
                <a href="~/maintenance/lvgas/lvactAdmin.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-user"></span>&nbsp;Large Volume Accounts<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/lvgas/announcements.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-bullhorn"></span>&nbsp;Announcements<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/lvgas/poolassign.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Pool Assign<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/lvgas/brokerassign.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Broker Assign<span class="pull-right fa fa-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
