
Partial Class Maintenance_LVgas_PoolAssign
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try

        'Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)

        'Dim info As ProfileInfo
        lblPoolMessage.Text = ""

        If ListBox1.SelectedIndex <> -1 Then
            lblPool.Text = ListBox1.SelectedItem.ToString
        End If
        'ListBox1.Items.Clear()

        
        If Not IsPostBack Then
            ListBox1.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("pool")
            ListBox1.DataBind()
            ListBox2.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("lvtransport")
            ListBox2.DataBind()
        End If
        'For Each info In profiles
        '    ' We need to turn a ProfileInfo into a ProfileCommon 
        '    ' to access the properties to do the search            
        '    Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName)
        '    ' Now to just assign those profiles that are pools
        '    'Roles.GetUsersInRole("Pool")
        '    If userProfile.PoolAssigned.Length > 1 Then                
        '        ListBox1.Items.Add(userProfile.UserName)
        '    End If
        'Next
    End Sub


    Protected Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged

        Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
        Dim info As ProfileInfo
        CheckBoxList1.Items.Clear()
        For Each info In profiles
            ' We need to turn a ProfileInfo into a ProfileCommon 
            ' to access the properties to do the search
            Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName)
            ' Now to just assign those profiles that are brokers            
            If userProfile.CustomerAccountNumber.Length > 1 Then
                CheckBoxList1.Items.Add(New ListItem(userProfile.UserName & " - " & userProfile.CustomerAccountNumber, userProfile.PoolAssigned))
                Dim lstPool As ListItem
                For Each lstPool In CheckBoxList1.Items
                    If lblPool.Text = lstPool.Value Then
                        lstPool.Selected = True
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub Button1_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim list As ListItem
        Dim profiles As ProfileInfoCollection = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All)
        Dim info As ProfileInfo
        'lblPoolMessage.Text += lblPool.Text & "<br/>"
        If lblPool.Text <> "" Then
            lblPoolMessage.Text += "Changes Saved. <br/> The following accounts are assigned to " & lblPool.Text & ": <br/><br/>"
        End If
        Dim txtProfileAcctNum As String = ""
        For Each list In CheckBoxList1.Items
            If list.Selected Then
                For Each info In profiles
                    ' We need to turn a ProfileInfo into a ProfileCommon 
                    ' to access the properties to do the search
                    Dim userProfile As ProfileCommon = ProfileCommon.Create(info.UserName, True)
                    ' Now to just assign those profiles that are brokers            
                    If userProfile.CustomerAccountNumber.Length > 1 Then
                        txtProfileAcctNum = userProfile.UserName & " - " & userProfile.CustomerAccountNumber
                        If txtProfileAcctNum = list.Text Then
                            userProfile.PoolAssigned = lblPool.Text.ToUpper
                            lblPoolMessage.Text += userProfile.UserName & "<br/>" & "<br/>"
                        End If
                    End If
                    userProfile.Save()
                Next
            End If
        Next
        'pnlAssign.Update()

    End Sub

    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        If ListBox2.SelectedIndex = -1 Then
            Label1.Text = "Please select a pool leader"
        Else
            If Not Roles.IsUserInRole(ListBox2.SelectedItem.Text, "pool") Then
                Roles.AddUserToRole(ListBox2.SelectedItem.Text, "pool")
                Response.Redirect(Request.UrlReferrer.AbsolutePath)
            Else
                Label1.Text = ListBox2.SelectedItem.Text & " is already a Pool Leader"
            End If
        End If
        
    End Sub

    Protected Sub btnUnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnAssign.Click
        If ListBox2.SelectedIndex = -1 Then
            Label1.Text = "Please select a pool leader"
        Else
            If Roles.IsUserInRole(ListBox2.SelectedItem.Text, "pool") Then
                Roles.RemoveUserFromRole(ListBox2.SelectedItem.Text, "pool")
                Response.Redirect(Request.UrlReferrer.AbsolutePath)
            Else
                Label1.Text = ListBox2.SelectedItem.Text & " is not a Pool Leader"
            End If
        End If
    End Sub
End Class
