Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OracleClient

Partial Class Maintenance_LVgas_LVActAdmin
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub gvAccountsT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsT.RowCommand        
        Select Case e.CommandName

            Case "Select"
                AccountsTDetails.HeaderText = "Edit"
                AccountsTDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AccountsTDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsTDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                lblType.Text = "lvtransport"
                pnllvtransport.Visible = true

                dlsBrokers.Items.Clear()
                dlsBrokers.Items.Add("")
                dlsBrokers.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("broker")
                dlsBrokers.AppendDataBoundItems = True
                dlsBrokers.DataBind()

                dlsPools.Items.Clear()
                dlsPools.Items.Add("")
                dlsPools.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("pool")        
                dlsPools.AppendDataBoundItems = True
                dlsPools.DataBind()

                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AddAccount').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddAccountDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub gvAccountsB_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsB.RowCommand
        Select Case e.CommandName
            Case "Select"
                AccountsBDetails.HeaderText = "Edit"
                AccountsBDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AccountsBDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsBDetailDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                lblType.Text = "broker"
                pnllvtransport.Visible = false

                dlsBrokers.Items.Clear()
                dlsBrokers.Items.Add("")
                dlsBrokers.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("broker")
                dlsBrokers.AppendDataBoundItems = True
                dlsBrokers.DataBind()

                dlsPools.Items.Clear()
                dlsPools.Items.Add("")
                dlsPools.DataSource = Roles.Providers("edgRoleSqlProvider").GetUsersInRole("pool")        
                dlsPools.AppendDataBoundItems = True
                dlsPools.DataBind()

                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AddAccount').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddAccountDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub AccountsTDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles AccountsTDetails.ItemCommand
        Select Case e.CommandName
            Case "Del"
                DeleteAccount(AccountsTDetails.DataKey.Item(0).ToString)
                gvAccountsT.SelectedIndex = -1
                gvAccountsT.DataBind()
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AccountsTDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsTDetailModalScript", sb.ToString(), False)
            Case "Can"
                gvAccountsT.SelectedIndex = -1
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AccountsTDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsTDetailModalScript", sb.ToString(), False)
            Case "Upd"
                If UpdateAccountT(AccountsTDetails.DataKey.Item(0).ToString) Then
                    gvAccountsT.SelectedIndex = -1
                    gvAccountsT.DataBind()
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#AccountsTDetail').modal('hide');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsTDetailModalScript", sb.ToString(), False)
                End If
            Case Else
        End Select
    End Sub

    Protected Sub AccountsBDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles AccountsBDetails.ItemCommand
        Select Case e.CommandName
            Case "Del"
                DeleteAccount(AccountsBDetails.DataKey.Item(0).ToString)
                gvAccountsB.SelectedIndex = -1
                gvAccountsB.DataBind()
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AccountsBDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsBDetailModalScript", sb.ToString(), False)
            Case "Can"
                gvAccountsB.SelectedIndex = -1
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#AccountsBDetail').modal('hide');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsBDetailModalScript", sb.ToString(), False)
            Case "Upd"
                If UpdateAccountB(AccountsBDetails.DataKey.Item(0).ToString) Then
                    gvAccountsB.SelectedIndex = -1
                    gvAccountsB.DataBind()
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#AccountsBDetail').modal('hide');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AccountsBDetailModalScript", sb.ToString(), False)
                End If
            Case Else
        End Select
    End Sub

    Protected Sub CreateUserWizard1_CreatedUser(ByVal sender As Object, ByVal e As System.EventArgs) Handles CreateUserWizard1.CreatedUser
        Try
            ' Create new user.
            If Not CreateUserWizard1.UserName Is Nothing Then

                Dim thisUserProfile As ProfileCommon = ProfileCommon.Create(CreateUserWizard1.UserName, True)
                If Not Roles.IsUserInRole(CreateUserWizard1.UserName, lblType.Text) Then
                    Dim arUserName(0) As String
                    arUserName(0) = CreateUserWizard1.UserName
                    Dim arRoleName(0) As String
                    arRoleName(0) = lblType.Text
                    Roles.Providers("edgRoleSqlProvider").AddUsersToRoles(arUserName, arRoleName)                                        
                    '***                    
                    thisUserProfile.CustomerAccountNumber = txtCustAcctTkn.Text
                    thisUserProfile.PoolAssigned = dlsPools.SelectedValue.ToString.ToUpper
                    thisUserProfile.Save()

                    Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

                        Const SQLLookup As String = "SELECT DISTINCT cust_token,description FROM Nominations where cust_token=@custToken"
                        Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                        myLookupCommand.Parameters.AddWithValue("@custToken", txtCustAcctTkn.Text) ')
                        myConnection.Open()
                        Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                        Dim tknExists As Boolean
                        If myLookup.Read Then
                            tknExists = True
                        Else
                            tknExists = False
                        End If
                        myLookup.Close()

                        If tknExists = False Then
                            Const SQL As String = "INSERT INTO [Nominations] ([date], [description],[cust_token]) VALUES (@Date, @Descr, @custToken)"
                            Dim myCommand As New SqlCommand(SQL, myConnection)

                            myCommand.Parameters.AddWithValue("@Date", Now)
                            myCommand.Parameters.AddWithValue("@custToken", txtCustAcctTkn.Text) 'txtCustAcctTkn.Text)                            
                            myCommand.Parameters.AddWithValue("@Descr", txtBusinessName.Text)                            
                            myCommand.ExecuteNonQuery()

                        End If
                        myConnection.Close()

                        Context.Response.Redirect(Context.Request.Url.AbsoluteUri)
                    End Using
                    
                End If
            End If

        Catch er As MembershipCreateUserException
        End Try
    End Sub
    
    Protected Sub CreateUserWizard1_CancelButtonClick(sender As Object, e As EventArgs) Handles CreateUserWizard1.CancelButtonClick
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#AddAccount').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "AddAccountModalScript", sb.ToString(), False)
    End Sub

    Public Function UpdateAccountT(ByVal id As String) As Boolean
        Try
            Dim status As Boolean = True
            Dim mbr As MembershipProvider
            Dim u As MembershipUser
            Dim newEmail As TextBox = (CType(AccountsTDetails.FindControl("txtEmail"), TextBox))
            Dim username As Label = (CType(AccountsTDetails.FindControl("lblUserName"), Label))
            Dim lblStatus As Label = (CType(AccountsTDetails.FindControl("lblStatus"), Label))
            Dim pwd As TextBox = (CType(AccountsTDetails.FindControl("txtPassword1"), TextBox))
            Dim pwdconfirm As TextBox = (CType(AccountsTDetails.FindControl("txtPassword2"), TextBox))
            mbr = Membership.Providers.Item("edgMembershipProvider")
            Dim oldpassword As String = mbr.GetPassword(username.Text, vbNull)

            'update email address if changed
            u = mbr.GetUser(username.Text, False)
            If u.Email <> newEmail.Text Then
                If Not Regex.IsMatch(newEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") Then
                    lblStatus.Text += "Invalid Email Format"
                    status = False
                Else
                    u.Email = newEmail.Text
                    mbr.UpdateUser(u)
                End If
            End If

            'update password if necessary
            If Len(pwd.Text) > 0 Then  'a password was keyed
                If pwd.Text = pwdconfirm.Text Then  'verify password confirmation matches
                    If Not Regex.IsMatch(pwd.Text, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*") Then  'ensure password meets criteria
                        lblStatus.Text += "Passwords are required to be at least 8 characters long including one special character (for example: john_123)."
                        status = False
                    Else
                        mbr.ChangePassword(username.Text, oldpassword, pwd.Text)
                    End If
                Else
                    lblStatus.Text = "Passwords do not match."
                    status = False
                End If
            End If
            Return status
        Catch
            Return False
        End Try
    End Function

    Public Function UpdateAccountB(ByVal id As String) As Boolean
        Try
            Dim status As Boolean = True
            Dim mbr As MembershipProvider
            Dim u As MembershipUser
            Dim newEmail As TextBox = (CType(AccountsBDetails.FindControl("txtEmail"), TextBox))
            Dim username As Label = (CType(AccountsBDetails.FindControl("lblUserName"), Label))
            Dim lblStatus As Label = (CType(AccountsBDetails.FindControl("lblStatus"), Label))
            Dim pwd As TextBox = (CType(AccountsBDetails.FindControl("txtPassword1"), TextBox))
            Dim pwdconfirm As TextBox = (CType(AccountsBDetails.FindControl("txtPassword2"), TextBox))
            mbr = Membership.Providers.Item("edgMembershipProvider")
            Dim oldpassword As String = mbr.GetPassword(username.Text, vbNull)

            'update email address if changed
            u = mbr.GetUser(username.Text, False)
            If u.Email <> newEmail.Text Then
                If Not Regex.IsMatch(newEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") Then
                    lblStatus.Text += "Invalid Email Format"
                    status = False
                Else
                    u.Email = newEmail.Text
                    mbr.UpdateUser(u)
                End If
            End If

            'update password if necessary
            If Len(pwd.Text) > 0 Then  'a password was keyed
                If pwd.Text = pwdconfirm.Text Then  'verify password confirmation matches
                    If Not Regex.IsMatch(pwd.Text, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*") Then  'ensure password meets criteria
                        lblStatus.Text += "Passwords are required to be at least 8 characters long including one special character (for example: john_123)."
                        status = False
                    Else
                        mbr.ChangePassword(username.Text, oldpassword, pwd.Text)
                    End If
                Else
                    lblStatus.Text = "Passwords do not match."
                    status = False
                End If
            End If
            Return status
        Catch
            Return False
        End Try
    End Function

    Public Sub DeleteAccount(ByVal id As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("edgmembershipConnectionString").ConnectionString)
        Try 
            'deletes userid entry from aspnet_profile, aspnet_usersinroles, aspnet_membership, aspnet_users
            Dim query1 As String = "delete from aspnet_Profile WHERE UserID = @id"
            Dim query2 As String = "delete from aspnet_UsersInRoles WHERE UserID = @id"
            Dim query3 As String = "delete from aspnet_membership where userID = @id"
            Dim query4 As String = "delete from aspnet_users where userID = @id"
            conn.Open()
            Dim cmd As New SqlCommand(query1, conn)
            cmd.Parameters.AddWithValue("@id", id)
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            cmd.ExecuteNonQuery()
            cmd.CommandText = query3
            cmd.ExecuteNonQuery()
            cmd.CommandText = query4
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
        End Try
    End Sub

    Protected Sub CreateUserWizard1_CreatingUser(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles CreateUserWizard1.CreatingUser
        Dim strValidLVAct As String
        If lblType.Text = "lvtransport" Then
            Dim cwservice As New cw.Service
            Dim tdes As New tripleDES
            Dim encrypt As String
            cwservice.Credentials = System.Net.CredentialCache.DefaultCredentials
            encrypt = tdes.Encrypt(txtCustAcctTkn.Text)
            strValidLVAct = cwservice.isLargeVolume(encrypt)            
            If strValidLVAct = "No" Then
                'check a checkbox for Sales customer or not.
                If ckSales.Checked Then
                    e.Cancel = False
                Else
                    lblInvalidAcct.Text = "Invalid Large Volume Customer Token"
                    e.Cancel = True
                End If
            End If
        End If
    End Sub
End Class
