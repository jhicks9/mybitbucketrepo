<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="BrokerAssign.aspx.vb" Inherits="Maintenance_LVgas_BrokerAssign" title="Broker Assign" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-12">
                <p class="well-sm bg-info text-center"><strong>Select the Broker</strong></p>
                <asp:UpdatePanel ID="pnlAssign" runat="server" UpdateMode="Conditional">
                    <contenttemplate>                        
                        <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="true" Height="150" CssClass="form-control" />
                        <br />
                        Assign LVT customer to <b><asp:Label ID="lblBroker" runat="server"  CssClass="well-sm bg-info text-center"></asp:Label></b>
                        <br /><hr />
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                        </asp:CheckBoxList>  
                        <asp:Button ID="Button1" runat="server" Text="Update" OnClick="Button1_Click1" CssClass="btn btn-default" />
                        <asp:Label ID="lblBrokerMessage" runat="server" Text="" />
                    </contenttemplate>
               </asp:UpdatePanel>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

