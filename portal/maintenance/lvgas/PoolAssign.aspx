<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="PoolAssign.aspx.vb" Inherits="Maintenance_LVgas_PoolAssign" title="Pool Assign" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-6">
                <p class="well-sm bg-info text-center"><strong>Select the Pool</strong></p>
                Select the Pool:
                <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="true" CssClass="form-control" />
                <br /><hr />
                Assign LVT customer to <b><asp:Label ID="lblPool" runat="server"></asp:Label></b>
                <br /><hr />
                <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                </asp:CheckBoxList>  

                <div class="pull-left">
                    <asp:Button ID="Button1" runat="server" Text="Update" OnClick="Button1_Click1" CssClass="btn btn-default" />
                </div>
                <asp:Label ID="lblPoolMessage" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-6">
                <p class="well-sm bg-info text-center"><strong>Assign/Remove Pool Leader</strong></p>
                <asp:ListBox ID="ListBox2" runat="server" CssClass="form-control" />
                <hr />
                <div class="pull-left">
                    <asp:Button ID="btnAssign" runat="server" Text="Assign" OnClick="btnAssign_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnUnAssign" runat="server" Text="Remove" OnClick="btnUnAssign_Click" CssClass="btn btn-default" />
                </div>
                <asp:Label ID="Label1" runat="server" ForeColor="" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>

