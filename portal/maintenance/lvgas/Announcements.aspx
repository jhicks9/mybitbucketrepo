<%@ Page Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="Announcements.aspx.vb" Inherits="maintenance_lvgas_Announcements" title="EDG Announcements" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <asp:Panel ID="pnlAnnouncements" runat="server" Visible="true" >
            <asp:GridView ID="gvAnnouncements" runat="server" DataSourceID="sqlDS1" DataKeyNames="id" AllowPaging="True" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true"
                AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <HeaderTemplate>
                            <asp:LinkButton ID="lbAdd" runat="server" ToolTip="Add new announcement" CausesValidation="false" CommandName="AddRecord"
                                CssClass="btn btn-primary" ><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" ToolTip="Edit announcement" CommandName="Edit" CommandArgument="EditRecord" 
                                CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                          <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="true" Text="Update"></asp:LinkButton>&nbsp;
                          <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel"></asp:LinkButton>&nbsp;
                          <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" CausesValidation="false" Text="Delete" OnClientClick='<%# "return confirm(""Are you sure you want to delete the record?"")" %>'></asp:LinkButton>
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                    <asp:BoundField DataField="id" HeaderText="ID" Visible="False" ReadOnly="True" />
                    <asp:TemplateField HeaderText="Date" SortExpression="sortdate">
                        <ItemTemplate>
                            <asp:Label ID="lblgvDate" runat="server" Text='<%# Bind("date") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div style="position:relative"><!-- fix for datetimepicker/scrolling -->
                                <asp:TextBox ID="txtgvDate" runat="server" Text='<%# Bind("date") %>' CssClass="datetimepickerclass" />
                            </div>
                            <asp:RegularExpressionValidator ID="revgvDate" runat="server" ControlToValidate="txtgvDate" SetFocusOnError="true" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20|21)\d\d" ErrorMessage="MM/DD/YYYY" Display="Dynamic" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type" SortExpression="">
                        <ItemTemplate>
                            <asp:Label ID="lblgvType" runat="server" Text='<%# Bind("type") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>' HtmlEncode="false"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="200" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control" />
                            <ajaxToolKit:HtmlEditorExtender ID="htmlEdit1" runat="server" TargetControlID="txtDescription" EnableSanitization="false" DisplaySourceTab="true" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Beginning Date" SortExpression="begdate">
                        <ItemTemplate>
                            <asp:Label ID="lblgvBegDate" runat="server" Text='<%# Bind("begdate") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div style="position:relative"><!-- fix for datetimepicker/scrolling -->
                                <asp:TextBox ID="txtgvBegDate" runat="server" Text='<%# Bind("begdate") %>'  CssClass="datetimepickerclass" />
                            </div>
                            <asp:RegularExpressionValidator ID="revgvBegDate" runat="server" ControlToValidate="txtgvBegDate" SetFocusOnError="true" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20|21)\d\d" ErrorMessage="MM/DD/YYYY" Display="Dynamic" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ending Date" SortExpression="enddate">
                        <ItemTemplate>
                            <asp:Label ID="lblgvEndDate" runat="server" Text='<%# Bind("enddate") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div style="position:relative"><!-- fix for datetimepicker/scrolling -->
                                <asp:TextBox ID="txtgvEndDate" runat="server" Text='<%# Bind("enddate") %>' CssClass="datetimepickerclass"/>
                            </div>
                            <asp:RegularExpressionValidator ID="revgvEndDate" runat="server" ControlToValidate="txtgvEndDate" SetFocusOnError="true" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20|21)\d\d" ErrorMessage="MM/DD/YYYY" Display="Dynamic" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pagination-ys" />
                <EmptyDataTemplate>
                    <asp:LinkButton ID="lbAddEmpty" runat="server" ToolTip="Add new module" CausesValidation="false" CommandArgument="AddRecord" CommandName="AddRecord" Text="Add Announcement"></asp:LinkButton>
                </EmptyDataTemplate>
            </asp:GridView>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlDetail" runat="server" Visible="False">
        <table class="table table-condensed table-bordered">
            <tr>
                <td colspan="2" class="col-lg-12"><p class="well-sm bg-info text-center"><strong><asp:Label ID="lblFunction" runat="server" Text="Add Announcement" /></strong></p></td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active">ID</td>
                <td><asp:Label ID="lblID" runat="server" Text="" CssClass="col-md-9 form-control no-border" /></td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active">Date</td>
                <td>
                    <div style="position:relative"><!-- fix for datetimepicker/scrolling -->
                        <asp:TextBox ID="txtDate" runat="server" CssClass="col-md-9 form-control datetimepickerclass"/>
                    </div>
                    <asp:RegularExpressionValidator ID="revDate" runat="server" ControlToValidate="txtDate" SetFocusOnError="true" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20|21)\d\d" ErrorMessage="MM/DD/YYYY" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active">Type</td>
                <td>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="col-md-9 form-control">
                        <asp:ListItem Text="EDG Announcement" Value="EDG Announcement" />
                        <asp:ListItem Text="OFO Announcement" Value="OFO Announcement" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active">Description</td>
                <td>
                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="200" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control datetimepicker" />
                    <ajaxToolKit:HtmlEditorExtender ID="htmlEdit1" runat="server" TargetControlID="txtDescription" EnableSanitization="false" DisplaySourceTab="true" />
                </td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active">Beginning Date</td>
                <td>
                    <div style="position:relative"><!-- fix for datetimepicker/scrolling -->
                        <asp:TextBox ID="txtBegDate" runat="server" CssClass="col-md-9 form-control datetimepickerclass" />
                    </div>
                    <asp:RegularExpressionValidator ID="revBegDate" runat="server" ControlToValidate="txtBegDate" SetFocusOnError="true" ValidationExpression="^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$" ErrorMessage="MM/DD/YYYY" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active">Ending Date</td>
                <td>
                    <div style="position:relative"><!-- fix for datetimepicker/scrolling -->
                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="col-md-9 form-control datetimepickerclass" />
                    </div>
                    <asp:RegularExpressionValidator ID="revEndDate" runat="server" ControlToValidate="txtEndDate" SetFocusOnError="true" ValidationExpression="^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$" ErrorMessage="MM/DD/YYYY" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td class="field-label col-xs-3 active"></td>
                <td>
                    <asp:LinkButton ID="lbInsert" CommandName="Insert" runat="server" CausesValidation="true" Text="Add Announcement" CssClass="btn btn-primary" />
                    <asp:LinkButton ID="lbCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="btn btn-default" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnID" runat="server" Value="" />
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Bold="true" />

    </asp:Panel>

    <asp:SqlDataSource ID="sqlDS1" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT id,type,CONVERT(char(10),date,101) AS date,date AS sortdate,description,CONVERT(char(10),begdate,101) AS begdate,CONVERT(CHAR(10),enddate,101) AS enddate FROM messages where (type='EDG Announcement' or type='OFO Announcement') order by sortdate desc,description ASC"
        InsertCommand=""
        UpdateCommand="update messages SET messages.date=@date,messages.begdate=@begdate,messages.enddate=@enddate,messages.description=@description where messages.id = @id"
        DeleteCommand="delete from messages where messages.id = @id">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {   
                $(".datetimepickerclass").datetimepicker({format:"MM/DD/YYYY"});
            });
        </script>
</asp:Content>