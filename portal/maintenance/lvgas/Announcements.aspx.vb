Imports System.Data.SqlClient
Imports System.Data

Partial Class maintenance_lvgas_Announcements
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub gvAnnouncements_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAnnouncements.RowCommand
        Select Case e.CommandName
            Case "Select"
            Case "AddRecord"
                pnlAnnouncements.Visible = False
                pnlDetail.Visible = True
        End Select
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCancel.Click
        pnlDetail.Visible = False
        pnlAnnouncements.Visible = True
    End Sub

    Protected Sub lbInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInsert.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim query As String = "insert into messages (type,date,begdate,enddate,description) values (@type,@date,@begdate,@enddate,@description)"
        Dim cmd As New SqlCommand(query, conn)

        If String.IsNullOrEmpty(txtBegDate.Text) Then
            cmd.Parameters.Add("@begdate", SqlDbType.DateTime).Value = DBNull.Value
        Else
            cmd.Parameters.AddWithValue("@begdate", txtBegDate.Text)
        End If
        If String.IsNullOrEmpty(txtDate.Text) Then
            cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = DBNull.Value
        Else
            cmd.Parameters.AddWithValue("@date", txtDate.Text)
        End If
        If String.IsNullOrEmpty(txtEndDate.Text) Then
            cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = DBNull.Value
        Else
            cmd.Parameters.AddWithValue("@enddate", txtEndDate.Text)
        End If
        cmd.Parameters.AddWithValue("@type", ddlType.SelectedValue)
        'cmd.Parameters.AddWithValue("@description", txtDescription.Text)
        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
        cmd.Dispose()
        conn.Dispose()

        txtDate.Text = ""
        txtBegDate.Text = ""
        txtEndDate.Text = ""
        'txtDescription.Text = ""
        sqlDS1.DataBind()
        gvAnnouncements.DataBind()
        pnlDetail.Visible = False
        pnlAnnouncements.Visible = True
    End Sub

    Protected Sub gvAnnouncements_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAnnouncements.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim desc As Label = CType(e.Row.FindControl("lblgvType"), Label)
                If InStr(desc.Text, "OFO") > 0 Then
                    e.Row.BackColor = System.Drawing.Color.FromArgb(255, 255, 175)
                End If
            End If
        Catch
        End Try
    End Sub
End Class
