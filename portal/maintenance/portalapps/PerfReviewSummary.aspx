﻿<%@ Page Title="" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="PerfReviewSummary.aspx.vb" Inherits="Maintenance_PortalApps_PerfReviewSummary" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div style="padding:0% 2% 0% 0%;width:175px;float:right;text-align:right;"><a href="javascript:PrintThisPage();" ><%--<asp:image ID="imgPrint" ImageUrl="~/Images/icon_printer.png" runat="server" />--%><span class="fa fa-print" style="color:#36587a"></span>&nbsp;Printer Friendly Version</a></div>
<br style="clear:both;"/>
<%--<div style="padding:0% 2% 0% 0%;width:175px;float:right;text-align:right;"><asp:image ID="Image1" ImageUrl="~/Images/icon_read.png" runat="server" /><asp:LinkButton ID="LinkButton1" Text=" Email This Page" runat="server"></asp:LinkButton></div>--%>
<br />
<div id="print_content"><%--begin print content--%>
<div style="width:768px;">
<%--<div style="text-align:center;Font-weight:bold;">THE EMPIRE DISTRICT ELECTRIC COMPANY<BR />PERFORMANCE PLANNING and REVIEW<BR />For Calendar Year 2009</div>
<br /><br />--%>
<div style="border:solid 1px black;float:left;width:33%;padding:3px;">Employee Name: <asp:Label ID="lblName" runat="server"></asp:Label></div><div style="border:solid 1px black;float:left;width:33%;padding:3px;">Title: <asp:Label ID="lblTitle" runat="server"></asp:Label></div><div style="border:solid 1px black;float:left;width:30%;padding:3px;">Location: <asp:Label ID="lblLocation" runat="server"></asp:Label></div>
<br style="clear:both;" />
<br />
<div style="border:none;float:left;width:70%;font-weight:bold;padding:3px;">ACCOUNTABILITIES REVIEW</div>
<br style="clear:both;" />
<div style="border:solid 1px black;float:left;width:27%;padding:3px;"><asp:Label ID="lblDate1" runat="server"></asp:Label> <asp:Label ID="lblClosedBy" runat="server"></asp:Label></div>
<br style="clear:both;" />
<br />

    <asp:GridView ID="gvLearningGoals" Width="99%" BorderColor="Black" AllowSorting="false" 
        BorderStyle="Solid" BorderWidth="1px" DataSourceID="SqlLearning" runat="server" 
        ShowHeader="True" GridLines="Vertical" AutoGenerateColumns="false" 
        CellPadding="5">    
        <RowStyle Height="25px" HorizontalAlign="Left" />
        <Columns>    
            <asp:BoundField DataField="Learning_Goal" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="PERFORMANCE AND LEARNING GOALS" HeaderStyle-Font-Size="Small" ReadOnly="True" >
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Learning_Assessment" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="ASSESSMENT" HeaderStyle-Font-Size="Small" ReadOnly="True" >        
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlLearning" runat="server"></asp:SqlDataSource>

<div style="border:solid 1px black;float:left;width:98%;padding:3px;Font-size:11px;">Employees must meet expectations on each goal to be eligible for: * Merit increase (Adjustment to base) * Incentive Award (Lump sum)
<br />NOTE: Employees who did not meet expectations on a particular goal may be eligible, based on manager's recommendations & director approval.</div>
<br style="clear:both;" />
<br />
<asp:GridView ID="gvIncentive" Width="99%" BorderColor="Black" AllowSorting="false" 
        BorderStyle="Solid" BorderWidth="1px" DataSourceID="SqlIncentive" runat="server" 
        ShowHeader="True" GridLines="Vertical" AutoGenerateColumns="false" 
        CellPadding="5">    
        <RowStyle Height="25px" HorizontalAlign="Left" />
        <Columns>    
            <asp:BoundField DataField="Incentive_Goal" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="INCENTIVE GOALS for SALARIED EMPLOYEES" HeaderStyle-Font-Size="Small" ReadOnly="True" >
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Incentive_Assessment" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="ASSESSMENT" HeaderStyle-Font-Size="Small" ReadOnly="True" >        
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlIncentive" runat="server"></asp:SqlDataSource>
    
    <br style="clear:both;" />
   
<div style="border:none;float:left;width:73%;font-weight:bold;padding:3px;">COMPETENCIES REVIEW</div>
<br style="clear:both;" />
<div style="border:solid 1px black;float:left;width:98%;padding:3px;font-size:small;">ASSESSMENT: HP=HIGHLY PROFICIENT P=PROFICIENT ND=NEEDS DEVELOPMENT</div>
<br style="clear:both;" />
<br />
<asp:GridView ID="gvCompetence" Width="99%" BorderColor="Black" AllowSorting="false" 
        BorderStyle="Solid" BorderWidth="1px" DataSourceID="SqlCompetence" runat="server" 
        ShowHeader="True" GridLines="Vertical" AutoGenerateColumns="false" 
        CellPadding="5">    
        <RowStyle Height="25px" HorizontalAlign="Left" />
        <Columns>    
            <asp:BoundField DataField="Competence_Goal" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="JOB RELATED / CORE COMPETENCIES" HeaderStyle-Font-Size="Small" ReadOnly="True" >
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Competence_Assessment" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="ASSESSMENT" HeaderStyle-Font-Size="Small" ReadOnly="True" >        
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Competence_Suggestion" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="COMPETENCY DEVELOPMENT SUGGESTIONS" HeaderStyle-Font-Size="Small" ReadOnly="True" >
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    
    <asp:SqlDataSource ID="SqlCompetence" runat="server"></asp:SqlDataSource>
    <%--<br />--%>
    <%--<div style="border:none;float:left;width:73%;font-weight:bold;padding:3px;">OTHER OBSERVATIONS</div>--%>
<br style="clear:both;" />
    <br />
    
    <asp:GridView ID="gvComments" Width="99%" BorderColor="Black" AllowSorting="false" 
        BorderStyle="Solid" BorderWidth="1px" DataSourceID="SqlComments" runat="server" 
        ShowHeader="True" GridLines="Vertical" AutoGenerateColumns="false" 
        CellPadding="5">    
        <RowStyle Height="25px" HorizontalAlign="Left" />
        <Columns>    
            <asp:BoundField DataField="Comment_Description" HeaderStyle-BorderColor="Black" 
                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1" 
                HeaderText="COMMENTS" HeaderStyle-Font-Size="Small" ReadOnly="True" >
            <HeaderStyle BorderColor="Black" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
            </asp:BoundField>            
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlComments" runat="server"></asp:SqlDataSource>
    <asp:Label ID="lblNameBottom" runat="server"></asp:Label>
</div>
</div><%--end print content--%>
<script type="text/javascript">
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=768,height=768,left=100,top=25,resizable=yes"; 
   
   var winprint=window.open("<%=printurl %>","Print",sOption); 
   winprint.focus(); 
}
</script>
</asp:Content>


