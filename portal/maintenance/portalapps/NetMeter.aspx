﻿<%@ Page Title="Solar Rebate Application" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="NetMeter.aspx.vb" Inherits="Maintenance_PortalApps_NetMeter" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-12 ">
                <p class="well-sm bg-info text-center"><strong><asp:Label ID="lblPageTitle" runat="server" Text="Net Metering/Solar Rebate Applications" /></strong></p>  
            </div>
        </div>
        <asp:Panel ID="pnlDatas" runat="server">
            <asp:HiddenField ID="hdnID" runat="server" />
            <asp:HiddenField ID="hdnAction" runat="server" />

            <div class="row">
                <div class="col-lg-12">
                    <asp:LinkButton ID="lbAdd" runat="server" CausesValidation="False" Text='Add' CssClass="btn btn-default" />
                    <asp:LinkButton ID="lbBuildQueue" runat="server" CausesValidation="False" Text='Rebuild Queue' CssClass="btn btn-default" />
                    <asp:LinkButton ID="lbSearchParameters" runat="server" CausesValidation="False" Text='Search' CssClass="btn btn-default" />
                    <asp:LinkButton ID="lbOutputExcel" runat="server" CausesValidation="False" Text='Output to Excel' CssClass="btn btn-default" />
                    <asp:LinkButton ID="lbControlRecord" runat="server" CausesValidation="False" Text='Edit Rebate Cap' CssClass="btn btn-default" />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gvData" runat="server" PageSize="35" DataSourceID="dsNetMeter" DataKeyNames="id" AllowPaging="true" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true">
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderTemplate />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("id") %>' CommandName="Select" ToolTip="Detail" Text='Edit' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Queue">
                                    <ItemTemplate>
                                        <asp:Label ID="lblQueueID" runat="server" Text='<%#Eval("queue_id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EDE Contact">
                                    <ItemTemplate><asp:Label ID="lblEDEContact" runat="server" Text='<%#Eval("entered_by")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Acct Pkg">
                                    <ItemTemplate><asp:Label ID="lblAccountPackage" runat="server" style="white-space:nowrap" Text='<%#Eval("acct_pkg")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company Name">
                                    <ItemTemplate><asp:Label ID="lblCompnayName" runat="server" Text='<%#Eval("companyname")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Last Name">
                                    <ItemTemplate><asp:Label ID="lblLastName" runat="server" Text='<%#Eval("last_name")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="First Name">
                                    <ItemTemplate><asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("first_name")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="System Type">
                                    <ItemTemplate><asp:Label ID="lblSystemType" runat="server" Text='<%#Eval("system_type")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="State">
                                    <ItemTemplate><asp:Label ID="lblState" runat="server" Text='<%#Eval("service_state")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Post Marked Date">
                                    <ItemTemplate><asp:Label ID="lblPostMarkDate" runat="server" Text='<%#String.Format("{0:MM/dd/yy}",Eval("sr_postmark_date"))%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Operational Date">
                                    <ItemTemplate><asp:Label ID="lblOperationalDate" runat="server" Text='<%#String.Format("{0:MM/dd/yy}",Eval("operational_date"))%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SR Paid Date">
                                    <ItemTemplate><asp:Label ID="lblSRPaidDate" runat="server" Text='<%#String.Format("{0:MM/dd/yy}",Eval("sr_paid_date"))%>' /></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsNetMeter" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" />
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlDetails" runat="server" Visible="False">
            <div class="row">
                <div class="col-lg-12 ">
                    <table class="table table-condensed table-bordered">
                        <tr><td colspan="2"><asp:Label ID="lblStatusTop" runat="server" ForeColor="Red" Text="" /></td></tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Queue Sequence Order</td>
                            <td class="col-md-9"><asp:Label ID="lblQueueID" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Assigned EDE Contact</td>
                            <td class="col-md-9"><asp:TextBox ID="txtEnteredBy" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Company Name</td>
                            <td class="col-md-9"><asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Last Name</td>
                            <td class="col-md-9"><asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">First Name</td>
                            <td class="col-md-9"><asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Service Address</td>
                            <td class="col-md-9"><asp:TextBox ID="txtServiceAddress" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">City</td>
                            <td class="col-md-9"><asp:TextBox ID="txtServiceCity" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">State</td>
                            <td class="col-md-9"><asp:TextBox ID="txtServiceState" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Zip</td>
                            <td class="col-md-9"><asp:TextBox ID="txtServiceZip" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Home Phone</td>
                            <td class="col-md-9">
                                <asp:TextBox ID="txtHomePhone" runat="server" CssClass="form-control"/>
                                <asp:RegularExpressionValidator ID="revHomePhone" runat="server" ControlToValidate="txtHomePhone"
                                    Display="Dynamic" ErrorMessage="format xxx-xxx-xxxx " ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="avg" />
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Cell Phone</td>
                            <td class="col-md-9">
                                <asp:TextBox ID="txtCellPhone" runat="server" CssClass="form-control"/>
                                <asp:RegularExpressionValidator ID="revCellPhone" runat="server" ControlToValidate="txtCellPhone"
                                    Display="Dynamic" ErrorMessage="format xxx-xxx-xxxx " ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="avg" />
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Email</td>
                            <td class="col-md-9"><asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Mailing Address</td>
                            <td class="col-md-9"><asp:TextBox ID="txtMailAddress" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">City</td>
                            <td class="col-md-9"><asp:TextBox ID="txtMailCity" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">State</td>
                            <td class="col-md-9"><asp:TextBox ID="txtMailState" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Zip</td>
                            <td class="col-md-9"><asp:TextBox ID="txtMailZip" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Acct Pkg</td>
                            <td class="col-md-9"><asp:TextBox ID="txtAcctPkg" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Rate</td>
                            <td class="col-md-9"><asp:TextBox ID="txtRate" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Meter ID</td>
                            <td class="col-md-9"><asp:TextBox ID="txtMeterID" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Meter Manufacturer</td>
                            <td class="col-md-9"><asp:TextBox ID="txtMeterManufacturer" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Net Meter Application Receipt Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtNMAppReceiptDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Solar Rebate Post-Marked Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtSRPostMarkDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">System Type</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSystemType" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">AC System Rating</td>
                            <td class="col-md-9"><asp:TextBox ID="txtACSystemRating" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">DC System Rating</td>
                            <td class="col-md-9"><asp:TextBox ID="txtDCSystemRating" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Solar Module Mfr</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSolarModuleMfr" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Solar Module Model Number</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSolarModuleNum" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Module Rating (DC watts)</td>
                            <td class="col-md-9"><asp:TextBox ID="txtModuleRating" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Inverter Rating(kW)</td>
                            <td class="col-md-9"><asp:TextBox ID="txtInverterRating" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Number of Modules/Panels</td>
                            <td class="col-md-9"><asp:TextBox ID="txtNumberPanels" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">System Rating</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSystemRating" runat="server" Enabled="false" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Date App To Engineering</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtEngineeringDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">W-9 Received Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtW9Received" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SA-76 Amount</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSA76Amount" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SA-76 Received Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtSA76Recieved" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SA-76 Payment Received Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtSA76PaymentReceived" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Detailed Receipts Received Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtDetailReceiptsReceived" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">10yr Warranty Sheet Received Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtWarrantySheetReceived" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Photos Received Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtPhotosReceived" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Conditional Approval Letter Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtLetterDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Denial Letter Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtDenialLetterDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">NM Resubmittal Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtNMResubmittalDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SR Commitment Letter Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtSRCommitmentDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SR Commitment Amount</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSRCommitmentAmount" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Customer Install Completion Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtCompletionDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Commission Test Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtCommissionTestDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SR Affidavit Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtSRAffidavitDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Azimuth</td>
                            <td class="col-md-9"><asp:TextBox ID="txtAzimuth" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Tilt</td>
                            <td class="col-md-9"><asp:TextBox ID="txtTilt" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Operational Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtOperationalDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Annual S-Rec's (estimated)</td>
                            <td class="col-md-9"><asp:TextBox ID="txtAnnualSRec" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SR Paid Date</td>
                            <td class="col-md-9">
                                <div class="input-group date datetimepickerclass">
                                    <asp:TextBox ID="txtSRPaidDate" runat="server" CssClass="form-control"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">SR Paid Amount</td>
                            <td class="col-md-9"><asp:TextBox ID="txtSRPaidAmount" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Comment</td>
                            <td class="col-md-9"><asp:TextBox ID="txtComment" TextMode="MultiLine" Height="75" CssClass="form-control" runat="server" /></td>
                        </tr>
                        <tr><td colspan="2"><asp:Label ID="lblStatusBottom" runat="server" ForeColor="Red" Text="" /></td></tr>
                        <tr>
                            <td class="field-label col-xs-3 active">&nbsp;</td>
                            <td class="col-md-9">
                                <asp:LinkButton ID="lbUpdate" runat="server" CausesValidation="True" ValidationGroup="avg" Text="Update" CssClass="btn btn-primary" />
                                <asp:LinkButton ID="lbClose" runat="server" CausesValidation="False" Text="Cancel" CssClass="btn btn-default" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlControlRecord" runat="server" Visible="false">
            <div class="row">
                <div class="col-lg-12 ">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtControlRecordRebateCap">Rebate Cap</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtControlRecordRebateCap" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active">&nbsp;</td>
                            <td class="col-md-9">
                                <asp:LinkButton ID="lbControlRecordUpdate" runat="server" CausesValidation="True" Text="Update" CssClass="btn btn-primary" />
                                <asp:LinkButton ID="lbControlRecordCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="btn btn-default" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlSearch" runat="server" Visible="false">
            <div class="row">
                <div class="col-lg-12 ">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtSearchCompanyName">Company Name</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtSearchCompanyName" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtSearchLastName">Last Name</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtSearchLastName" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtSearchState">State</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtSearchState" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtSearchAcctPkg">Acct Pkg</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtSearchAcctPkg" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtSearchContact">EDE Contact</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtSearchContact" runat="server" CssClass="form-control" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Search' />
                </div>
            </div>
        </asp:Panel>

    </asp:Panel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="scriptSection" Runat="Server">
    <script type="text/javascript">
        $(function () {
            $('.datetimepickerclass').datetimepicker();
        });
    </script>
</asp:Content>