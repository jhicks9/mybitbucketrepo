﻿Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Data.SqlClient
Partial Class Maintenance_PortalApps_Interruptible
    Inherits System.Web.UI.Page
    Dim ps As New portalSecurity
    'Dim tdes As New tripleDES

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isAdmin(ps.getPageName) Then
            pnlAuthorized.Visible = True
            pnlNotAuthorized.Visible = False
            pnlAdministration.Visible = True
            pnlAdminOnly.Enabled = True
        Else
            pnlAdministration.Visible = False
            If Not ps.isAuthorized(ps.getPageName) Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If
        End If

        SetModuleTabs()

        If cbTimer.Checked Then
            Timer1.Enabled = True
        Else
            Timer1.Enabled = False
        End If
    End Sub

    Public Sub SetModuleTabs()
        If ps.canInsert(ps.getPageName) Then
            btInsert.Enabled = True
            btInsert.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
            btInsert.Font.Bold = False
        End If
        If ps.canUpdate(ps.getPageName) Then
            btUpdate.Enabled = True
            btUpdate.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
            btUpdate.Font.Bold = False
        End If
        If ps.canDelete(ps.getPageName) Then
            btDelete.Enabled = True
            btDelete.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
            btDelete.Font.Bold = False
        End If
    End Sub

    Protected Sub btInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btInsert.Click
        pnlInsert.Visible = True
        btInsert.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        btInsert.Font.Bold = True
        btView.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        btView.Font.Bold = False
        pnlUpdate.Visible = False
        pnlDelete.Visible = False
        pnlDefault.Visible = False
    End Sub

    Protected Sub btUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btUpdate.Click
        pnlUpdate.Visible = True
        btUpdate.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        btUpdate.Font.Bold = True
        btView.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        btView.Font.Bold = False
        pnlInsert.Visible = False
        pnlDelete.Visible = False
        pnlDefault.Visible = False
        Check_For_Interrupt()
    End Sub

    Protected Sub btDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btDelete.Click
        pnlDelete.Visible = True
        btDelete.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        btDelete.Font.Bold = True
        btView.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        btView.Font.Bold = False
        pnlUpdate.Visible = False
        pnlInsert.Visible = False
        pnlDefault.Visible = False
    End Sub

    Protected Sub btView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btView.Click
        pnlDefault.Visible = True
        btView.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        btView.Font.Bold = True
        pnlDelete.Visible = False
        pnlUpdate.Visible = False
        pnlInsert.Visible = False
        Response.Redirect(Request.FilePath)
    End Sub

    Protected Sub DetailsView1_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles DetailsView1.ItemUpdated
        Response.Redirect(Request.FilePath)
    End Sub

    Protected Sub DetailsView1_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles DetailsView1.ItemDeleted
        Response.Redirect(Request.FilePath)
    End Sub

    Protected Sub DetailsView1_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles DetailsView1.ItemInserted
        Response.Redirect(Request.FilePath)
    End Sub

    Protected Sub lnkNewEmail_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvEmails.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub Check_For_Interrupt()
        Try
            Dim txtHrs As TextBox = GridView1.FooterRow.FindControl("txtStartTimeHrs")
            Dim txtMins As TextBox = GridView1.FooterRow.FindControl("txtStartTimeMins")
            Dim ddlhours As DropDownList = GridView1.HeaderRow.FindControl("ddlHours")
            Dim lnk2nd As LinkButton = GridView1.FooterRow.FindControl("lnk2nd")
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQL As String = "SELECT b.StartTime FROM dsm_customer A, dsm_interrupt as B WHERE A.Id = B.CustomerId AND b.date = @date" 'AND CONVERT(VARCHAR(10),b.date,110) = @date"
                Dim myCommand As New SqlCommand(SQL, myConnection)

                myConnection.Open()

                myCommand.Parameters.AddWithValue("@date", Now.Month & "/" & Now.Day & "/" & Now.Year & " 12:00:00 AM")
                Dim myReader As SqlDataReader = myCommand.ExecuteReader
                While myReader.Read
                    Dim arTime As Array = Split(myReader.GetValue(0).ToString, ":")
                    Dim arMins As Array = Split(arTime(1), " ")

                    txtMins.Text = arMins(0)
                    If Right(myReader.GetValue(0).ToString, 2) = "AM" Then
                        txtHrs.Text = arTime(0)
                    Else
                        txtHrs.Text = arTime(0) + 12
                    End If
                    txtHrs.Enabled = False
                    txtMins.Enabled = False
                    ddlhours.Enabled = False
                    lnk2nd.Visible = True
                End While
                myReader.Close()
            End Using
        Catch
        End Try
    End Sub

    Public Sub CreateInterruptRecords(ByVal CustID As Integer, ByVal hours As Integer, ByVal Total As Double, ByVal primaryEmail As String, ByVal secondaryEmail As String, ByVal startTime As DateTime, ByVal endTime As DateTime, ByVal startDate As DateTime)
        Try
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Dim SQL As String = "INSERT INTO dsm_interrupt (CustomerId,hours,date,StartTime,EndTime,ApproveInterrupt) VALUES (@CustID,@hours,@date,@StartTime,@EndTime,'False');" & _
                                    "SELECT id FROM dsm_interrupt WHERE id = @@IDENTITY"
                Dim myCommand As New SqlCommand(SQL, myConnection)

                myCommand.Parameters.AddWithValue("@CustID", CustID)
                myCommand.Parameters.AddWithValue("@hours", hours)
                myCommand.Parameters.AddWithValue("@date", startDate)
                myCommand.Parameters.AddWithValue("@StartTime", startTime.ToShortTimeString)
                myCommand.Parameters.AddWithValue("@EndTime", endTime.ToShortTimeString)

                myConnection.Open()

                Dim InterruptId As Integer = CType(myCommand.ExecuteScalar(), Int32)

                buildNotification(InterruptId, CustID, hours, startDate, primaryEmail, secondaryEmail, startTime, endTime)
                myConnection.Close()
            End Using
        Catch
            lblInterrupt.Text = "CreateInterrupt Error: " & Err.Description
        End Try
    End Sub

    Public Sub buildNotification(ByVal InterruptId As Int32, ByVal CustID As Int32, ByVal hours As Int32, ByVal startDate As DateTime, ByVal primary As String, ByVal secondary As String, ByVal startTime As DateTime, ByVal endTime As DateTime)
        Dim IntId As String = InterruptId.ToString
        Try
            'production:
            'http://www.empiredistrict.com/InterruptAccept.aspx?int="
            'test
            'http://ede-55709/empire/InterruptAccept.aspx?int="
            Dim mailMessage As String = "Dear Customer," & vbCrLf & vbCrLf & "The Empire District Electric Company is scheduling a load curtailment for " & startDate.ToShortDateString & " using your interruptible power." & vbCrLf & _
                                        "The curtailment will begin at " & startTime.ToShortTimeString & " and will end at " & endTime.ToShortTimeString & " (" & hours.ToString & " hours)." & vbCrLf & _
                                        "Please click on the Empire web page link below to acknowledge receipt of this notification." & vbCrLf & vbCrLf & _
                                        "Weblink page: http://www.empiredistrict.com/InterruptAccept.aspx?int=" & IntId.Trim & vbCrLf & vbCrLf & _
                                        "Thank you," & vbCrLf & _
                                        "The Empire District Electric Company" & vbCrLf & vbCrLf & _
                                        "Failure to acknowledge receipt of this notification will result in cancellation of your interruptible contract and rates."
            sendEmail(mailMessage, primary, secondary)
            'removed tdes.Encrypt() because of errors in testing
        Catch
            lblMessage.Text &= "buildNotification: " & Err.Description
        End Try
    End Sub

    Public Sub sendEmail(ByVal mailMessage As String, ByVal primary As String, ByVal secondary As String)
        Dim MailObj As New System.Net.Mail.SmtpClient
        Dim message As New System.Net.Mail.MailMessage()
        Dim fromAddress As New System.Net.Mail.MailAddress("Interruptible@empiredistrict.com")

        Try
            'Build message
            message.From = fromAddress
            message.To.Add(primary)
            message.To.Add(secondary)
            message.Subject = "Empire District Scheduled Interruption"
            'MailObj.Host = "" -- obtained from web.config 03/14/2012
            message.Body = mailMessage
            'send email
            MailObj.Send(message)
        Catch
            lblMessage.Text &= "We're sorry, but there was an error.<br/>" & Err.Description
        Finally
            message.Dispose()
        End Try
    End Sub

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        Dim ddlGroup As DropDownList = GridView1.HeaderRow.FindControl("ddlGroups")
        ddlGroup.Items.Insert(0, "-group-")
    End Sub

    Public Function Check_Existing_Interrupt() As Boolean
        Try
            Dim HasRows As Boolean = False
            Dim txtMonth As TextBox = GridView1.FooterRow.FindControl("txtStartDateMonth")
            Dim txtDay As TextBox = GridView1.FooterRow.FindControl("txtStartDateDay")
            Dim txtYear As TextBox = GridView1.FooterRow.FindControl("txtStartDateYear")
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Dim newdate As String = Nothing

                If Len(txtMonth.Text.ToString) = 1 Then
                    newdate += "0" & txtMonth.Text.ToString
                Else
                    newdate += txtMonth.Text.ToString
                End If
                newdate += "-"
                If Len(txtDay.Text.ToString) = 1 Then
                    newdate += "0" & txtDay.Text.ToString
                Else
                    newdate += txtDay.Text.ToString
                End If
                newdate += "-" & txtYear.Text.ToString
                'newdate = "6/15/2010 12:00:00 AM"

                myConnection.Open()

                Dim SQL As String = "SELECT id FROM dsm_interrupt WHERE CONVERT(VARCHAR(10),date,110) = @date"
                Dim myCommand As New SqlCommand(SQL, myConnection)

                myCommand.Parameters.AddWithValue("@date", newdate)
                Dim myReader As SqlDataReader = myCommand.ExecuteReader

                If myReader.Read Then
                    HasRows = True
                Else
                    HasRows = False
                End If
                myConnection.Close()
            End Using
            Return HasRows
        Catch

        End Try
    End Function

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim lblTotal As Label = GridView1.FooterRow.FindControl("lblTotal")

        If e.CommandName = "Notify" Then

            Dim hours As DropDownList = GridView1.HeaderRow.FindControl("ddlHours")
            Dim txtHrs As TextBox = GridView1.FooterRow.FindControl("txtStartTimeHrs")
            Dim txtMins As TextBox = GridView1.FooterRow.FindControl("txtStartTimeMins")
            Dim txtMonth As TextBox = GridView1.FooterRow.FindControl("txtStartDateMonth")
            Dim txtDay As TextBox = GridView1.FooterRow.FindControl("txtStartDateDay")
            Dim txtYear As TextBox = GridView1.FooterRow.FindControl("txtStartDateYear")
            Dim startTime As DateTime = txtHrs.Text & ":" & txtMins.Text
            Dim endTime As DateTime = startTime.AddHours(CType(hours.SelectedValue, Double))
            Dim lblEndAt As Label = GridView1.FooterRow.FindControl("lblEndDate")
            Dim entryError As Label = GridView1.FooterRow.FindControl("lblEntryError")
            Dim count As Integer = GridView1.Rows.Count
            Dim totalhours As Int32
            Dim InterruptExists As Boolean = False

            InterruptExists = Check_Existing_Interrupt()

            If Convert.ToInt32(txtMonth.Text) >= 1 And Convert.ToInt32(txtMonth.Text) <= 12 Then
                If Convert.ToInt32(txtYear.Text) >= Now.Year And _
                   Convert.ToInt32(txtDay.Text) >= 1 And Convert.ToInt32(txtDay.Text) <= System.DateTime.DaysInMonth(Convert.ToInt32(txtYear.Text), Convert.ToInt32(txtMonth.Text)) And _
                   Convert.ToInt32(txtHrs.Text) >= 0 And Convert.ToInt32(txtHrs.Text) <= 24 And _
                   Convert.ToInt32(txtMins.Text) >= 0 And Convert.ToInt32(txtMins.Text) <= 60 Then
                    Dim startDate As DateTime = New DateTime(txtYear.Text, txtMonth.Text, txtDay.Text, txtHrs.Text, txtMins.Text, 0)
                    Dim endAt As DateTime = startDate.AddHours(CType(hours.SelectedValue, Double))
                    If startDate >= Now.Date.AddHours(4) Then
                        lblEndAt.Text = endAt.ToString
                        If count > 0 Then
                            Dim total As Double = 0
                            Dim row As GridViewRow
                            lblMessage.Text = "Interrupt these customers:<br />"
                            lblTotal.Text = Nothing
                            For Each row In GridView1.Rows
                                Dim cb As CheckBox = row.FindControl("CheckBox1")
                                Dim cbInactive As CheckBox = row.FindControl("Checkbox2")

                                If cbInactive.Checked = False Then
                                    If cb.Checked Then
                                        'If row.Cells(9).Text = "MW" Then
                                        total = total + CType(row.Cells(8).Text, Double)
                                        'Else
                                        'total = total + (CType(row.Cells(8).Text, Double) / 1000)
                                        'End If
                                        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                            Dim newdate As String = Nothing

                                            If Len(txtMonth.Text.ToString) = 1 Then
                                                newdate += "0" & txtMonth.Text.ToString
                                            Else
                                                newdate += txtMonth.Text.ToString
                                            End If
                                            newdate += "-"
                                            If Len(txtDay.Text.ToString) = 1 Then
                                                newdate += "0" & txtDay.Text.ToString
                                            Else
                                                newdate += txtDay.Text.ToString
                                            End If
                                            newdate += "-" & txtYear.Text.ToString

                                            myConnection.Open()

                                            Const SQLEligible As String = "SELECT COUNT(b.CustomerId) AS times, SUM(b.hours) AS totalhours FROM dsm_customer AS a INNER JOIN dsm_interrupt AS b ON a.id = b.CustomerId WHERE (YEAR(b.date) = right(@date,4)) AND (b.CustomerId = @CustomerId)"
                                            Dim myCommandEligible As New SqlCommand(SQLEligible, myConnection)

                                            myCommandEligible.Parameters.AddWithValue("@CustomerId", CType(row.Cells(2).Text, Integer))
                                            myCommandEligible.Parameters.AddWithValue("@date", newdate)

                                            Dim myReaderEligible As SqlDataReader = myCommandEligible.ExecuteReader

                                            While myReaderEligible.Read
                                                totalhours = myReaderEligible("totalhours") + hours.SelectedValue

                                                Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

                                                    If CType(myReaderEligible("times").ToString, Int32) < 10 And totalhours <= 80 Then

                                                        myConnection2.Open()

                                                        Const SQL As String = "SELECT CustomerID,CONVERT(VARCHAR(10),date,110) as date FROM dsm_interrupt WHERE CustomerId = @CustID and CONVERT(VARCHAR(10),date,110) = @date"

                                                        Dim myCommand As New SqlCommand(SQL, myConnection2)

                                                        myCommand.Parameters.AddWithValue("@CustID", CType(row.Cells(2).Text, Integer))
                                                        myCommand.Parameters.AddWithValue("@date", newdate)

                                                        Dim myReader As SqlDataReader = myCommand.ExecuteReader

                                                        If myReader.Read Then
                                                            lblMessage.Text += "<br/>Customer- " & row.Cells(3).Text & " is already scheduled for interrupt for " & startDate & "<br/>"
                                                        Else
                                                            lblMessage.Text &= "<br/> Sending email to: " & " CustId- " & row.Cells(2).Text & row.Cells(3).Text & " <br/>Primary email: " & row.Cells(6).Text & " <br/>secondary email " & row.Cells(7).Text & "<br/>"
                                                            CreateInterruptRecords(CType(row.Cells(2).Text, Integer), CType(hours.SelectedValue, Integer), total, row.Cells(6).Text, row.Cells(7).Text, startTime, endTime, startDate)
                                                        End If
                                                        myReader.Close()
                                                    Else
                                                        'If row.Cells(9).Text = "MW" Then
                                                        total = total - CType(row.Cells(8).Text, Double)
                                                        'Else
                                                        'total = total - (CType(row.Cells(8).Text, Double) / 1000)
                                                        'End If
                                                        lblMessage.Text += "<br/>Customer- " & row.Cells(3).Text & " cannot be interrupted they have already had 10 interruptions and/or have been interrupted or will be interrupted for 80 hours or more. <br/>"
                                                    End If
                                                    myConnection2.Close()
                                                End Using
                                            End While
                                            myReaderEligible.Close()
                                            myConnection.Close()
                                        End Using
                                    End If
                                Else
                                    cb.Checked = False
                                End If
                            Next
                            lblTotal.Text += total.ToString & " KW total "
                            If InterruptExists = False Then
                                Dim MailObj2 As New System.Net.Mail.SmtpClient
                                Dim message2 As New System.Net.Mail.MailMessage()
                                Dim fromAddress2 As New System.Net.Mail.MailAddress("Interruptible@empiredistrict.com")

                                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

                                    myConnection.Open()

                                    Const SQL As String = "SELECT Email_Address FROM dsm_Emails"

                                    Dim myCommand As New SqlCommand(SQL, myConnection)
                                    Dim myReader As SqlDataReader = myCommand.ExecuteReader

                                    Try
                                        message2.From = fromAddress2
                                        While myReader.Read
                                            'Build message
                                            message2.To.Add(myReader("Email_Address").ToString)
                                        End While
                                        message2.Subject = "Curtailment Notification Email -Empire District Electric"
                                        message2.Body = "Dear Customer," & vbCrLf & vbCrLf & "The Empire District Electric Company is scheduling a load curtailment. The curtailment will begin: " & startDate & _
                                                            " and will end: " & endAt & "." & vbCrLf & vbCrLf & _
                                                            "Thank you" & vbCrLf & _
                                                            "The Empire District Electric Company"
                                        Replace(message2.To.ToString, ",", ";")
                                        'send email internal/external                            
                                        MailObj2.Send(message2)
                                        lblMessage.Text += "<br/><br/>" & message2.From.ToString & "<br/>" & Replace(message2.To.ToString, ",", ";") & "<br/>" & message2.Body & "<br/><br/>"
                                    Catch
                                        lblMessage.Text &= "Error sending to PSC.<br/>" & Err.Description
                                    Finally
                                        message2.Dispose()
                                    End Try
                                    myReader.Close()
                                    myConnection.Close()
                                End Using
                            End If
                        End If
                    Else
                        entryError.Text = ""
                        entryError.Text = "Please enter a date at least 4 hours from " & Now
                    End If
                Else
                    entryError.Text = ""
                    entryError.Text = "Invalid date entered."
                End If
            Else
                entryError.Text = ""
                entryError.Text = "Invalid date entered."
            End If
        ElseIf e.CommandName = "CheckAll" Then
            Try
                Dim lChk As LinkButton = GridView1.HeaderRow.FindControl("lnkCheck")
                Dim lUnChk As LinkButton = GridView1.HeaderRow.FindControl("lnkUnCheck")
                Dim row As GridViewRow
                Dim total As Double = 0

                For Each row In GridView1.Rows
                    Dim cb As CheckBox = row.FindControl("CheckBox1")
                    Dim cbInactive As CheckBox = row.FindControl("CheckBox2")

                    If cbInactive.Checked = False Then
                        cb.Checked = True
                        'If row.Cells(9).Text = "MW" Then
                        total = total + CType(row.Cells(8).Text, Double)
                        'Else
                        'total = total + (CType(row.Cells(8).Text, Double) / 1000)
                        'End If
                    Else
                        cb.Checked = False
                    End If
                Next

                lblTotal.Text = total.ToString & " KW total "
                lChk.Visible = False
                lUnChk.Visible = True
            Catch
                lblMessage.Text = "error in Check All" & Err.Description
            End Try
        ElseIf e.CommandName = "unCheckAll" Then
            Try
                Dim lChk As LinkButton = GridView1.HeaderRow.FindControl("lnkCheck")
                Dim lUnChk As LinkButton = GridView1.HeaderRow.FindControl("lnkUnCheck")
                Dim row As GridViewRow

                For Each row In GridView1.Rows
                    Dim cb As CheckBox = row.FindControl("CheckBox1")
                    cb.Checked = False
                Next
                lblTotal.Text = "0 KW total"
                lChk.Visible = True
                lUnChk.Visible = False
            Catch
                lblMessage.Text = "error in unCheckall" & Err.Description
            End Try
        End If
    End Sub

    Protected Sub GridView2_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView2.RowCommand
        If e.CommandName = "escalate" Then
            lblMessage2.Text = ""
            Dim approved As Boolean = Nothing, inActive As Boolean = Nothing
            Dim intId As Int32 = e.CommandArgument
            Dim hours As Int32 = Nothing
            Dim startDate As DateTime = Nothing, startTime As DateTime = Nothing, endTime As DateTime = Nothing
            Dim primaryEmail As String = Nothing, secondaryEmail As String = Nothing, emails As String = Nothing
            Dim test As String = Nothing

            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQL As String = "SELECT B.*, A.PrimaryEmail, A.SecondaryEmail, A.Inactive from dsm_customer A, dsm_interrupt as B where B.id = @id and B.CustomerID = A.id"
                Dim myCommand As New SqlCommand(SQL, myConnection)
                myConnection.Open()
                myCommand.Parameters.AddWithValue("@id", intId)
                Dim myReader As SqlDataReader = myCommand.ExecuteReader
                myReader.Read()
                approved = myReader("ApproveInterrupt")
                inActive = myReader("Inactive")
                startDate = myReader("date")
                If startDate.Date >= Now.Date Then
                    If inActive = False Then
                        If approved = False Then
                            hours = myReader("hours")
                            startTime = myReader("StartTime")
                            endTime = myReader("EndTime")
                            primaryEmail = myReader("PrimaryEmail")
                            secondaryEmail = myReader("SecondaryEmail")
                            emails &= "Primary: " & primaryEmail & " Secondary: " & secondaryEmail & vbCrLf

                            Try
                                'production:
                                'http://www.empiredistrict.com/InterruptAccept.aspx?int=
                                'test
                                'http://ede-55709/empire/InterruptAccept.aspx?int=
                                Dim mailMessage As String = "Dear Customer," & vbCrLf & vbCrLf & "The Empire District Electric Company is scheduling a load curtailment for " & startDate.ToShortDateString & " using your interruptible power." & vbCrLf & _
                                    "The curtailment will begin at " & startTime.ToShortTimeString & " and will end at " & endTime.ToShortTimeString & " (" & hours.ToString & " hours)." & vbCrLf & vbCrLf & _
                                    "Please click on the Empire web page link below to acknowledge receipt of this notification." & vbCrLf & vbCrLf & _
                                    "Weblink page: http://www.empiredistrict.com/InterruptAccept.aspx?int=" & IntId & vbCrLf & vbCrLf & _
                                    "Thank you" & vbCrLf & _
                                    "The Empire District Electric Company" & vbCrLf & vbCrLf & _
                                    "Failure to acknowledge receipt of this notification will result in cancellation of your interruptible contract and rates."
                                sendEmail(mailMessage, primaryEmail, secondaryEmail)
                                myReader.Close()
                                myConnection.Close()

                                Dim mailObj As New System.Net.Mail.SmtpClient
                                Dim message As New System.Net.Mail.MailMessage
                                Dim fromAddress As New System.Net.Mail.MailAddress("Interruptible@empiredistrict.com")

                                Const SQL2 = "SELECT Email_Address FROM dsm_Emails where Send_second_Notification = 'True'"
                                myCommand.CommandText = SQL2
                                myCommand.Connection = myConnection
                                myConnection.Open()
                                myReader = myCommand.ExecuteReader

                                Try
                                    message.From = fromAddress
                                    While myReader.Read
                                        message.To.Add(myReader("Email_Address").ToString)
                                    End While
                                    message.Subject = "2nd curtailment Notification Email -Empire District Electric"
                                    message.Body = "A 2nd curtailment message has been sent on " & Now.Date & " to the following email addresses: " & vbCrLf & emails & " For the curtailment beginning at " & startTime & " and ending at " & endTime & "." & vbCrLf & vbCrLf
                                    mailObj.Send(message)
                                    lblMessage2.Text &= "<br /><br />" & message.From.ToString & "<br /><br />" & message.Body & "<br /><br />"
                                Catch
                                    lblMessage2.Text &= "Error sending to PSC.<br />" & Err.Description
                                Finally
                                    message.Dispose()
                                End Try
                            Catch
                                lblMessage2.Text &= "Escalate_Notification: " & Err.Description
                            End Try
                        Else
                            lblMessage2.Text = "Customer has already approved this interrupt."
                        End If
                    Else
                        lblMessage2.Text = "This is an inactive customer."
                    End If
                Else
                    lblMessage2.Text = "This interrupt date has already passed."
                End If
                myReader.Close()
                myConnection.Close()
            End Using
        Else
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#dv2Detail').modal('show');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "dv2DetailModalScript", sb.ToString(), False)
        End If
    End Sub

    'Public Sub Escalate_Notification(ByVal sender As System.Object, ByVal e As EventArgs)
    ''id, CustomerID, hours, date, ApproveInterrupt, StartTime, EndTime
    ''not linking to gridview so giving a null error.

    'Dim approved As CheckBox = GridView2.HeaderRow.FindControl("Approved")
    'Dim inActive As CheckBox = GridView2.HeaderRow.FindControl("InActive")
    'Dim intID As Int32 = Nothing
    'Dim hours As Label = GridView2.HeaderRow.FindControl("hours")
    'Dim  As CheckBox = GridView2.HeaderRow.FindControl("InActive")
    'Dim  As CheckBox = GridView2.HeaderRow.FindControl("InActive")
    'Dim approved As Boolean = GridView2.SelectedRow.Cells(1).Text, inActive As Boolean = GridView2.SelectedRow.Cells(3).Text
    'Dim IntId As String = Nothing
    'Dim hours As Int32 = GridView2.Rows(Convert.ToInt32(e)).Cells(4).Text
    'Dim startDate As DateTime = GridView2.Rows(Convert.ToInt32(e)).Cells(5).Text
    'Dim emails As String = Nothing
    'Dim primaryEmail As String, secondaryEmail As String
    'Dim startTime As DateTime = GridView2.Rows(Convert.ToInt32(e)).Cells(6).Text, endTime As DateTime = GridView2.Rows(Convert.ToInt32(e)).Cells(7).Text

    'Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
    '    Const SQL As String = "SELECT B.id as InterruptID, A.PrimaryEmail, A.SecondaryEmail as InterruptID from dsm_customer A, dsm_interrupt as B where A.Id = B.CustomerID and convert(varchar(10),b.date,110) = @date and b.StartTime = @sTime and b.EndTime = @eTime"
    '    Dim myCommand As New SqlCommand(SQL, myConnection)
    '    myConnection.Open()
    '    myCommand.Parameters.AddWithValue("@date", ddlDate.SelectedValue)
    '    myCommand.Parameters.AddWithValue("@sTime", startTime)
    '    myCommand.Parameters.AddWithValue("@eTime", endTime)
    '    Dim myReader As SqlDataReader = myCommand.ExecuteReader
    '    While myReader.Read
    '        If inActive = False Then
    '            If approved = False Then
    '                IntId = myReader("InterruptID")
    '                primaryEmail = myReader("PrimaryEmail")
    '                secondaryEmail = myReader("SecondaryEmail")
    '                emails &= "Primary: " & myReader("PrimaryEmail") & " Secondary: " & myReader("SecondaryEmail") & vbCrLf

    '                Try
    '                    'production:
    '                    'https://www.empiredistrict.com/InterruptAccept.aspx?int="
    '                    'test
    '                    'http://ede-55709/empire/InterruptAccept.aspx?int="
    '                    Dim mailMessage As String = "Dear Customer," & vbCrLf & vbCrLf & "The Empire District Electric Company is scheduling a load curtailment for " & startDate.ToShortDateString & " using your interruptible power." & vbCrLf & _
    '                                                "The curtailment will begin at " & startTime.ToShortTimeString & " and will end at " & endTime.ToShortTimeString & " (" & hours.ToString & " hours)." & vbCrLf & vbCrLf & _
    '                                                "Please click on the Empire web page link below to acknowledge receipt of this notification." & vbCrLf & vbCrLf & _
    '                                                "Weblink page: http://ede-55709/empire/InterruptAccept.aspx?int=" & IntId.Trim & vbCrLf & vbCrLf & _
    '                                                "Thank you" & vbCrLf & _
    '                                                "The Empire District Electric Company" & vbCrLf & vbCrLf & _
    '                                                "Failure to acknowledge receipt of this notification will result in cancellation of your interruptible contract and rates."
    '                    sendEmail(mailMessage, primaryEmail, secondaryEmail)
    '                Catch
    '                    lblMessage.Text &= "Escalate_Notification: " & Err.Description
    '                End Try
    '            Else
    '                lblMessage.Text = "Customer has already approved this interrupt."
    '            End If
    '        Else
    '            lblMessage.Text = "This is an inactive customer."
    '        End If

    '    End While
    '    myReader.Close()
    '    myConnection.Close()
    'End Using

    'Dim MailObj As New System.Net.Mail.SmtpClient
    'Dim message As New System.Net.Mail.MailMessage()
    'Dim fromAddress As New System.Net.Mail.MailAddress("Interruptible@empiredistrict.com")

    'Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
    '    myConnection.Open()

    '    Const SQL As String = "SELECT Email_Address FROM dsm_Emails where Send_second_Notification = 'True'"

    '    Dim myCommand As New SqlCommand(SQL, myConnection)
    '    Dim myReader As SqlDataReader = myCommand.ExecuteReader

    '    Try
    '        message.From = fromAddress
    '        While myReader.Read
    '            message.To.Add(myReader("Email_Address").ToString)
    '        End While
    '        message.Subject = "2nd Curtailment Notification Email -Empire District Electric"
    '        message.Body = "A 2nd curtailment message has been sent on " & Now.Date & " to the following email addresses:" & vbCrLf & emails & "For the curtailment beginning at " & startTime & " and ending at " & endTime & "." & vbCrLf & vbCrLf
    '        MailObj.Send(message)
    '        lblMessage.Text &= "<br /><br />" & message.From.ToString & "<br />" & Replace(message.To.ToString, ",", ";") & message.Body & "<br /><br />"
    '    Catch
    '        lblMessage.Text &= "Error sending to PSC.<br />" & Err.Description
    '    Finally
    '        message.Dispose()
    '    End Try
    '    myReader.Close()
    '    myConnection.Close()
    'End Using

    'Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
    '    Const SQL As String = "SELECT A.*,B.id as InterruptId, b.* FROM dsm_customer A, dsm_interrupt as B WHERE A.Id = B.CustomerId AND b.date = @date" 'and ApproveInterrupt = 'False' AND CONVERT(VARCHAR(10),b.date,110) = @date"
    '    Dim myCommand As New SqlCommand(SQL, myConnection)
    '    myConnection.Open()
    '    myCommand.Parameters.AddWithValue("@date", ddlDate.SelectedValue)
    '    Dim myReader As SqlDataReader = myCommand.ExecuteReader
    '    While myReader.Read
    '        approved = myReader("ApproveInterrupt")
    '        If approved = False Then
    '            IntId = myReader("InterruptId")
    '            hours = myReader("hours")
    '            startDate = myReader("date")
    '            primaryEmail = myReader("primaryEmail")
    '            secondaryEmail = myReader("secondaryEmail")
    '            emails &= "Primary: " & myReader("primaryEmail") & " Secondary: " & myReader("secondaryEmail") & vbCrLf
    '            startTime = myReader("startTime")
    '            endTime = myReader("endTime")

    '            Try
    '                'production:
    '                'https://www.empiredistrict.com/InterruptAccept.aspx?int="
    '                'test
    '                'http://ede-55709/empire/InterruptAccept.aspx?int="
    '                Dim mailMessage As String = "Dear Customer," & vbCrLf & vbCrLf & "The Empire District Electric Company is scheduling a load curtailment for " & startDate & " using your interruptible power." & vbCrLf & _
    '                                            "The curtailment will begin at " & startTime & " and will end at " & endTime & " (" & hours.ToString & " hours)." & vbCrLf & vbCrLf & _
    '                                            "Please click on the Empire web page link below to acknowledge receipt of this notification." & vbCrLf & vbCrLf & _
    '                                            "Weblink page: http://ede-55709/empire/InterruptAccept.aspx?int=" & IntId.Trim & vbCrLf & vbCrLf & _
    '                                            "Thank you" & vbCrLf & _
    '                                            "The Empire District Electric Company" & vbCrLf & vbCrLf & _
    '                                            "Failure to acknowledge receipt of this notification will result in cancellation of your interruptible contract and rates."
    '                'lblMessage.Text &= mailMessage
    '                sendEmail(mailMessage, primaryEmail, secondaryEmail)
    '            Catch
    '                lblMessage.Text &= "Escalate_Notification: " & Err.Description
    '            End Try
    '        End If
    '    End While
    '    myReader.Close()
    '    myConnection.Close()
    'End Using

    'Dim MailObj As New System.Net.Mail.SmtpClient
    'Dim message As New System.Net.Mail.MailMessage()
    'Dim fromAddress As New System.Net.Mail.MailAddress("Interruptible@empiredistrict.com")

    'Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

    '    myConnection.Open()

    '    Const SQL As String = "SELECT Email_Address FROM dsm_Emails where Send_second_Notification is not null"

    '    Dim myCommand As New SqlCommand(SQL, myConnection)
    '    Dim myReader As SqlDataReader = myCommand.ExecuteReader
    '    Try
    '        message.From = fromAddress
    '        While myReader.Read
    '            'Build message
    '            message.To.Add(myReader("Email_Address").ToString)
    '        End While
    '        message.Subject = "2nd Curtailment Notification Email -Empire District Electric"
    '        MailObj.Host = "srv-empire32"
    '        message.Body = "A 2nd curtailment message has been sent on " & Now.Date & " to the following email addresses:" & vbCrLf & _
    '                            emails & "For the curtailment beginning at " & startTime & " and ending at " & endTime & "." & vbCrLf & vbCrLf

    '        Replace(message.To.ToString, ",", ";")
    '        'send email internal/external
    '        MailObj.Send(message)
    '        lblMessage.Text += "<br/><br/>" & message.From.ToString & "<br/>" & Replace(message.To.ToString, ",", ";") & "<br/>" & message.Body & "<br/><br/>"
    '    Catch
    '        lblMessage.Text &= "Error sending to PSC.<br/>" & Err.Description
    '    Finally
    '        message.Dispose()
    '    End Try
    '    myReader.Close()
    '    myConnection.Close()
    'End Using
    'End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Dim row As GridViewRow

        For Each row In GridView1.Rows
            row.Cells(6).Visible = False
            row.Cells(7).Visible = False
            GridView1.HeaderRow.Cells(6).Visible = False
            GridView1.HeaderRow.Cells(7).Visible = False
        Next
    End Sub

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        Interrupt_Status(ddlDate.SelectedValue)
    End Sub

    Protected Sub ddlDate_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.DataBound
        Dim ddlNow As ListItem

        For Each ddlNow In ddlDate.Items
            Dim newdate As String = Nothing

            If Len(Now.Month.ToString) = 1 Then
                newdate += "0" & Now.Month.ToString
            Else
                newdate += Now.Month.ToString
            End If
            newdate += "-"
            If Len(Now.Day.ToString) = 1 Then
                newdate += "0" & Now.Day.ToString
            Else
                newdate += Now.Day.ToString
            End If
            newdate += "-" & Now.Year.ToString
            If ddlNow.Selected = False Then
                If ddlNow.Text = newdate Then
                    ddlNow.Selected = True
                    Interrupt_Status(ddlNow.Text)
                    'ElseIf ddlDate.Items.Count >= 1 Then
                    '    ddlNow.Selected = True
                    '    Interrupt_Status(ddlNow.Text)
                End If
            End If
        Next
        ddlDate.Items.Insert(0, "-date-")
    End Sub

    Private Sub Interrupt_Status(ByVal strdate As String)
        lblInfo.Text = strdate
        DetailsView2.HeaderText = strdate
        DetailsView3.HeaderText = Right(strdate, 4)
        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQL As String = "SELECT Wattage,units FROM dsm_customer A, dsm_interrupt as B WHERE A.Id = B.CustomerId AND CONVERT(VARCHAR(10),b.date,110) = @date"
            Dim myCommand As New SqlCommand(SQL, myConnection)

            myConnection.Open()

            myCommand.Parameters.AddWithValue("@date", strdate)

            Dim myReader As SqlDataReader = myCommand.ExecuteReader
            Dim total As Double = Nothing

            lblRequested.Text = Nothing
            While myReader.Read
                'If myReader("units").ToString = "MW" Then
                total += CType(myReader("Wattage").ToString, Double)
                'Else
                'total += (CType(myReader("Wattage").ToString, Double) / 1000)
                'End If
            End While
            lblRequested.Text &= total.ToString
            myReader.Close()

            Const SQL2 As String = "SELECT Wattage,units FROM dsm_customer A, dsm_interrupt as B WHERE A.Id = B.CustomerId AND CONVERT(VARCHAR(10),b.date,110) = @date AND ApproveInterrupt = 'True'"
            Dim myCommand2 As New SqlCommand(SQL2, myConnection)

            myCommand2.Parameters.AddWithValue("@date", strdate)

            Dim myReader2 As SqlDataReader = myCommand2.ExecuteReader
            Dim total2 As Double = Nothing

            lblApproved.Text = Nothing
            While myReader2.Read
                'If myReader2("units").ToString = "MW" Then
                total2 += CType(myReader2("Wattage").ToString, Double)
                'Else
                'total2 += (CType(myReader2("Wattage").ToString, Double) / 1000)
                'End If
            End While
            lblApproved.Text &= total2.ToString
            myReader.Close()
            myConnection.Close()
        End Using
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblRefresh.Text = "Last updated at: " & _
          DateTime.Now.ToLongTimeString()
        ddlDate.DataBind()
        GridView2.DataBind()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTotal As Label = GridView1.FooterRow.FindControl("lblTotal")
        Dim row As GridViewRow
        Dim total As Double = 0
        Dim lChk As LinkButton = GridView1.HeaderRow.FindControl("lnkCheck")
        Dim lUnChk As LinkButton = GridView1.HeaderRow.FindControl("lnkUnCheck")

        For Each row In GridView1.Rows
            Dim cb As CheckBox = row.FindControl("CheckBox1")
            Dim cbInactive As CheckBox = row.FindControl("CheckBox2")

            If cbInactive.Checked = False Then
                If cb.Checked = True Then
                    'If row.Cells(9).Text = "MW" Then
                    total = total + CType(row.Cells(8).Text, Double)
                    'Else
                    total = total + (CType(row.Cells(8).Text, Double) / 1000)
                    'End If
                End If
            Else
                cb.Checked = False
            End If
        Next
        lblTotal.Text = total.ToString & " KW total "
        lChk.Visible = False
        lUnChk.Visible = True
    End Sub

    Protected Sub ddlGroups_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTotal As Label = GridView1.FooterRow.FindControl("lblTotal")
        Dim lChk As LinkButton = GridView1.HeaderRow.FindControl("lnkCheck")
        Dim lUnChk As LinkButton = GridView1.HeaderRow.FindControl("lnkUnCheck")

        Try
            Dim row As GridViewRow
            Dim total As Double = 0

            For Each row In GridView1.Rows
                Dim cb As CheckBox = row.FindControl("CheckBox1")
                Dim ddlGroup As DropDownList = GridView1.HeaderRow.FindControl("ddlGroups")
                Dim cbInactive As CheckBox = row.FindControl("CheckBox2")
                Dim lblGroup As Label = row.FindControl("Label1")

                If cbInactive.Checked = False Then
                    If ddlGroup.SelectedValue = lblGroup.Text Then
                        cb.Checked = True
                        'If row.Cells(9).Text = "MW" Then
                        total = total + CType(row.Cells(8).Text, Double)
                        'Else
                        'total = total + (CType(row.Cells(8).Text, Double) / 1000)
                        'End If
                    Else
                        cb.Checked = False
                    End If
                Else
                    cb.Checked = False
                End If
            Next
            lblTotal.Text = total.ToString & " KW total "
            lChk.Visible = False
            lUnChk.Visible = True
        Catch
            lblMessage.Text = "error in CheckforGroup" & Err.Description
        End Try
    End Sub

    Protected Sub ddlFailCustomers_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFailCustomers.DataBound
        ddlFailCustomers.Items.Insert(0, "--Select Customer--")
        gvFailures.DataBind()
    End Sub

    Protected Sub btnAddFailure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFailure.Click
        lblCustId.Text = Nothing
        If ddlFailCustomers.SelectedItem.Text <> "--Select Customer--" Then
            Try
                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Dim SQLSelect As String = "Select count(CustomerId) as count from dsm_failure where Failure_Date >= @ContractStartDate and Failure_Date < @ContractEndDate AND CustomerId = @CustomerId"
                    Dim mySelectCommand As New SqlCommand(SQLSelect, myConnection)
                    Dim contractstart As Date = New Date(Now.Year, 2, 1)
                    Dim contractend As Date = New Date(Now.AddYears(1).Year, 2, 1)

                    mySelectCommand.Parameters.AddWithValue("@ContractEndDate", contractend)
                    mySelectCommand.Parameters.AddWithValue("@ContractStartDate", contractstart)
                    mySelectCommand.Parameters.AddWithValue("@CustomerId", ddlFailCustomers.SelectedValue)
                    lblCustId.Text += "start: " & contractstart & "<br/>" & "end: " & contractend & "<br/>"

                    Dim SQL As String = "Insert Into dsm_failure (CustomerId,Failure_Date) values(@CustomerId,@Date)"
                    Dim myInsertCommand As New SqlCommand(SQL, myConnection)

                    myInsertCommand.Parameters.AddWithValue("@Date", Now().Date)
                    myInsertCommand.Parameters.AddWithValue("@CustomerId", ddlFailCustomers.SelectedValue)

                    myConnection.Open()

                    Dim count As Integer = CType(mySelectCommand.ExecuteScalar(), Int32)

                    lblCustId.Text += "<br/>" & count.ToString
                    If count < 3 Then
                        myInsertCommand.ExecuteNonQuery()
                    Else
                        lblCustId.Text = "<br/> already inactive"
                    End If
                    If count = 2 Then
                        Dim SQLUpdate As String = "Update dsm_customer set inactive = 'True', ReactivateDate = @ReactDate where Id = @CustomerId"
                        Dim myUpdateCommand As New SqlCommand(SQLUpdate, myConnection)
                        myUpdateCommand.Parameters.AddWithValue("@CustomerId", ddlFailCustomers.SelectedValue)
                        myUpdateCommand.Parameters.AddWithValue("@ReactDate", Now.AddYears(2).Date)
                        myUpdateCommand.ExecuteNonQuery()
                        GridView1.DataBind()
                        lblCustId.Text = "<br/>" & ddlFailCustomers.SelectedItem.Text & " has been inactivated"
                        myConnection.Close()
                    End If
                End Using
                gvFailures.DataBind()
            Catch
                lblCustId.Text += Err.Description
            End Try
        End If
    End Sub

    Protected Sub gvEmails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEmails.RowDeleting
        Label3.Text = e.RowIndex & " - " & e.Keys(0)
    End Sub


End Class