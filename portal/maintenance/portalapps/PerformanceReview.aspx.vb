﻿Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports System.Data.SqlTypes
Imports System.IO

Partial Class Maintenance_PortalApps_PerformanceReview
    Inherits System.Web.UI.Page
    Dim ps As New portalSecurity
    Dim username As String
    Dim tdes As New tripleDES
    Dim managerEmplID As Int32
    Dim employeeEmplID As Int32
    '8/27/2012 - replace SYSADM.PS_JOB with SYSADM.EDE_PORTAL_PS_JOB_VW 
    '8/27/2012 - replace SYSADM.PS_EMPLOYEES with SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If

            SetModuleTabs()
            username = Nothing
            username = Request.ServerVariables("LOGON_USER")
            username = Right(username, Len(username) - InStr(username, "\")).ToLower
            If ps.canUpdate(ps.getPageName) Then
                ddlManagers.Visible = True
                hlUpdate.Enabled = True
                divRevHist.Visible = True
                DropDownList1.Visible = True
                treeDocs.Visible = True
                lnkReport.Visible = True
            End If

            'only Director of HR runs reports****************
            If username = "pbabb" Then
                lnkReport.Visible = True
            Else
                lnkReport.Visible = False
            End If
            'comment for production**************************
            If username = "admgwest" Or username = "admjhicks" Then
                ddlManagers.Visible = True
                hlUpdate.Enabled = True
                DropDownList1.Visible = True
                treeDocs.Visible = True
                lnkReport.Visible = True
            End If
            '************************************************

            If Not IsPostBack Then
                Dim EmptyListM As New ListItem("--Choose Manager--", "")
                ddlManagers.Items.Add(EmptyListM)
                'calYear = Now.Year
                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT [Manager_SignOn],[PS_EMPLID] FROM [PerfEmployee] WHERE ([Manager_SignOn] = @Manager_SignOn)"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@Manager_SignOn", username)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                    'Check to see if signed on employee is a manager using position number*****************************
                    If myLookup.Read Then
                        managerEmplID = myLookup("PS_EMPLID")
                        populate_dropdownlist1()
                        'uncomment upon request from HR
                        'setmeritlabels()
                        gvSchedules.Visible = True
                        pnlManagerView.Visible = True
                    Else
                        gvSchedules.Visible = False
                        pnlManagerView.Visible = False
                        Dim ThisYear = Now.Year
                        Dim firstYear = Year("1/1/2009")
                        Dim x As Int32
                        For x = firstYear To ThisYear + 1
                            ddlReviewYear.Items.Add(New ListItem(x.ToString, x))
                        Next
                        ddlReviewYear.SelectedValue = ThisYear
                        Using myConnectionHist As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Const SQLLookupHist As String = "SELECT [Employee_SignOn],[PS_EMPLID] FROM [PerfEmployee] WHERE ([Employee_SignOn] = @Employee_SignOn)"
                            Dim myLookupHistCommand As New SqlCommand(SQLLookupHist, myConnectionHist)
                            myLookupHistCommand.Parameters.AddWithValue("@Employee_SignOn", username)
                            myConnectionHist.Open()
                            Dim myLookupHist As SqlDataReader = myLookupHistCommand.ExecuteReader
                            'Check to see if signed on employee is a manager using position number*****************************
                            If myLookupHist.Read Then
                                employeeEmplID = myLookupHist("PS_EMPLID")
                            End If

                            myLookupHist.Close()
                            myConnectionHist.Close()
                        End Using
                        If ps.canUpdate(ps.getPageName) Then
                            divRevHist.Visible = False
                            DropDownList1.Visible = True
                            treeDocs.Visible = True
                        Else
                            divRevHist.Visible = True
                            DropDownList1.Visible = False
                            treeDocs.Visible = False
                        End If
                    End If
                    myLookup.Close()
                    myConnection.Close()
                End Using

                lnkSelectedSub.Text = Nothing
                lblToggle.Visible = False
                imgToggle.Visible = False
                ImgSchedule.Visible = False
                ImgReview.Visible = False
                ImgEmail.Visible = False
                ImgPrintPDF.Visible = False
                Dim MyItem As ListItem
                Dim emails As String = Nothing
                For Each MyItem In ckEmails.Items
                    If MyItem.Selected = True Then
                        MyItem.Selected = True
                    End If
                Next
                load_checkbox()
            Else
                If ddlManagers.SelectedValue <> Nothing Then

                    Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                        Const SQLLookup As String = "SELECT [Manager_SignOn] FROM [PerfEmployee] WHERE PS_EMPLID = @PS_EMPLID"
                        Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                        myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", ddlManagers.SelectedValue)
                        myConnection.Open()
                        Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                        'Check to see if signed on employee is a manager using position number*****************************
                        If myLookup.Read Then
                            If Not myLookup("Manager_SignOn") Is DBNull.Value Then
                                username = myLookup("Manager_SignOn")
                            Else
                                username = "User Not Found"
                            End If
                        End If
                        myLookup.Close()
                        myConnection.Close()
                    End Using
                Else
                    Using myConnectionHist As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                        Const SQLLookupHist As String = "SELECT [Employee_SignOn],[PS_EMPLID] FROM [PerfEmployee] WHERE ([Employee_SignOn] = @Employee_SignOn)"
                        Dim myLookupHistCommand As New SqlCommand(SQLLookupHist, myConnectionHist)
                        myLookupHistCommand.Parameters.AddWithValue("@Employee_SignOn", username)
                        myConnectionHist.Open()
                        Dim myLookupHist As SqlDataReader = myLookupHistCommand.ExecuteReader
                        'Check to see if signed on employee is a manager using position number*****************************
                        If myLookupHist.Read Then
                            employeeEmplID = myLookupHist("PS_EMPLID")
                        End If
                        lnkRevHist.Visible = True
                        myLookupHist.Close()
                        myConnectionHist.Close()
                    End Using
                End If
                lblLearnMsg.Visible = False
                lblIncentiveMsg.Visible = False
            End If

            lblMgr.Text = username

            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)

                Const SQLPSLookup As String = "SELECT A.EMPLID, A.NAME, A.POSITION_NBR " & _
                     " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A " & _
                     " WHERE ( A.EFFDT = " & _
                     " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                     " AND A_ED.EFFDT <= SYSDATE) " & _
                     " AND A.EFFSEQ = " & _
                     " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                     " AND A.EFFDT = A_ES.EFFDT) " & _
                     " AND SUBSTR(A.POSITION_NBR,1,2) = :code )" & _
                     " ORDER BY A.NAME ASC"
                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":code", "00")

                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader

                If Not IsPostBack Then
                    While myPSLookup.Read
                        ddlManagers.Items.Add(New ListItem(myPSLookup("NAME"), myPSLookup("EMPLID")))
                    End While
                End If
                '******************************************
                myPSLookup.Close()
                myPSConnection.Close()
            End Using
            lblSelectedYear.Text = ddlReviewYear.SelectedValue
        Catch
            lblEmployeeInfo.Text += Err.Description
        End Try
    End Sub
    Public Sub setmeritlabels()
        lblTotalBaseSalary.Text = Nothing
        lblDollarsAvailable.Text = Nothing
        Dim decTotalBaseSalary As Decimal
        decTotalBaseSalary = Decimal.Round(CType(GetTotalBaseSalary(), Decimal), 2)
        lblTotalBaseSalary.Text = " $" & decTotalBaseSalary.ToString("###,###.#0")
        lblPercent.Text = getPercent()
        Dim decTotal As Decimal
        decTotal = CType(GetTotalBaseSalary(), Decimal) * CType(lblPercent.Text, Decimal) / 100
        lblDollarsAvailable.Text = " $" & decTotal.ToString("###,###.#0") 'String.Format("{0:c}", CType(Decimal.Round(dblTotal, 2).ToString, Int32))
    End Sub
    Public Function getPercent() As String
        Dim strPercent As String
        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

            Dim SQLPercent As String = "SELECT percentage FROM PerfPercent"
            Dim mySQLLookup As New SqlCommand(SQLPercent, myConnection)
            'myPSLookup.Parameters.AddWithValue(":EMPLID", managerEmplID)
            myConnection.Open()
            strPercent = mySQLLookup.ExecuteScalar()
            myConnection.Close()
        End Using
        Return strPercent
    End Function
    Public Sub SetModuleTabs()
        If ps.canInsert(ps.getPageName) Then
            hlInsert.Enabled = True
            hlInsert.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
            hlInsert.Font.Bold = False
        End If
        If ps.canUpdate(ps.getPageName) Then
            hlUpdate.Enabled = True
            hlUpdate.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
            hlUpdate.Font.Bold = False
            ddlManagers.Visible = True
        End If
        If ps.canDelete(ps.getPageName) Then
            hlDelete.Enabled = True
            hlDelete.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
            hlDelete.Font.Bold = False
        End If

    End Sub
    Protected Sub hlInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlInsert.Click
        pnlInsert.Visible = True
        hlInsert.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        hlInsert.Font.Bold = True
        hlView.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        hlView.Font.Bold = False
        pnlUpdate.Visible = False
        pnlDelete.Visible = False
        pnlDefault.Visible = False

    End Sub
    Protected Sub hlUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlUpdate.Click
        pnlUpdate.Visible = True
        hlUpdate.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        hlUpdate.Font.Bold = True
        hlView.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        hlView.Font.Bold = False
        pnlInsert.Visible = False
        pnlDelete.Visible = False
        pnlDefault.Visible = False
    End Sub
    Protected Sub hlDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlDelete.Click
        pnlDelete.Visible = True
        hlDelete.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        hlDelete.Font.Bold = True
        hlView.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        hlView.Font.Bold = False
        pnlUpdate.Visible = False
        pnlInsert.Visible = False
        pnlDefault.Visible = False

    End Sub
    Protected Sub hlView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlView.Click
        pnlDefault.Visible = True
        hlView.BackColor = Drawing.ColorTranslator.FromHtml("#BFCBD7")
        hlView.Font.Bold = True
        pnlDelete.Visible = False
        pnlUpdate.Visible = False
        pnlInsert.Visible = False
        Response.Redirect(Request.FilePath)
    End Sub
    Protected Sub dvIncentive_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles dvIncentive.ItemDeleted
        gvIncentive.DataBind()
    End Sub
    Protected Sub dvIncentive_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvIncentive.ItemInserted
        gvIncentive.DataBind()
    End Sub
    Protected Sub dvIncentive_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvIncentive.ItemUpdated
        gvIncentive.DataBind()
    End Sub
    Protected Sub dvEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvEmployee.DataBound
        If dvEmployee.CurrentMode = DetailsViewMode.Edit Then
            dvEmployee.Focus()
        End If
        If dvEmployee.CurrentMode = DetailsViewMode.Insert Then
            dvEmployee.Focus()
        End If
    End Sub
    Protected Sub dvCoreComp_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvCoreComp.DataBound
        If dvCoreComp.CurrentMode = DetailsViewMode.Edit Then
            dvCoreComp.Focus()
        End If
        If dvCoreComp.CurrentMode = DetailsViewMode.Insert Then
            dvCoreComp.Focus()
        End If
    End Sub
    Protected Sub dvComments_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvComments.DataBound
        If dvComments.CurrentMode = DetailsViewMode.Edit Then
            dvComments.Focus()
        End If
        If dvComments.CurrentMode = DetailsViewMode.Insert Then
            dvComments.Focus()
        End If
    End Sub
    Protected Sub dvCompetence_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvCompetence.DataBound
        If dvCompetence.CurrentMode = DetailsViewMode.Edit Then
            dvCompetence.Focus()
        End If
        If dvCompetence.CurrentMode = DetailsViewMode.Insert Then
            dvCompetence.Focus()
        End If
    End Sub
    Protected Sub dvIncentive_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvIncentive.DataBound
        If dvIncentive.CurrentMode = DetailsViewMode.Edit Then
            dvIncentive.Focus()
        End If
        If dvIncentive.CurrentMode = DetailsViewMode.Insert Then
            dvIncentive.Focus()
        End If
    End Sub
    Protected Sub dvLearning_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvLearning.DataBound
        If dvLearning.CurrentMode = DetailsViewMode.Edit Then
            dvLearning.Focus()
        End If
        If dvLearning.CurrentMode = DetailsViewMode.Insert Then
            dvLearning.Focus()
        End If
    End Sub
    Protected Sub dvComments_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles dvComments.ItemDeleted
        gvComments.DataBind()
    End Sub
    Protected Sub dvComments_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvComments.ItemInserted
        gvComments.DataBind()
    End Sub
    Protected Sub dvComments_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvComments.ItemUpdated
        gvComments.DataBind()
    End Sub
    Protected Sub dvCoreComp_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles dvCoreComp.ItemDeleted
        gvCoreComp.DataBind()
    End Sub
    Protected Sub dvCoreComp_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvCoreComp.ItemInserted
        gvCoreComp.DataBind()
        gvCompetence.DataBind()
    End Sub
    Protected Sub dvCoreComp_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvCoreComp.ItemUpdated
        gvCoreComp.DataBind()
        gvCompetence.DataBind()
    End Sub
    Protected Sub dvCompetence_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles dvCompetence.ItemDeleted
        gvCompetence.DataBind()
    End Sub
    Protected Sub dvCompetence_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvCompetence.ItemInserted
        gvCoreComp.DataBind()
        gvCompetence.DataBind()
    End Sub
    Protected Sub dvCompetence_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvCompetence.ItemUpdated
        gvCoreComp.DataBind()
        gvCompetence.DataBind()
    End Sub
    Protected Sub dvLearning_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles dvLearning.ItemCommand
        If e.CommandName = "Cancel" Then
            gvLearning.SelectedIndex = -1
        End If
    End Sub
    Protected Sub dvIncentive_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles dvIncentive.ItemCommand
        If e.CommandName = "Cancel" Then
            gvIncentive.SelectedIndex = -1
        End If
    End Sub
    Protected Sub dvCoreComp_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles dvCoreComp.ItemCommand
        If e.CommandName = "Cancel" Then
            gvCoreComp.SelectedIndex = -1
        End If
    End Sub
    Protected Sub dvCompetence_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles dvCompetence.ItemCommand
        If e.CommandName = "Cancel" Then
            gvCompetence.SelectedIndex = -1
        End If
    End Sub
    Protected Sub dvComments_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles dvComments.ItemCommand
        If e.CommandName = "Cancel" Then
            gvComments.SelectedIndex = -1
        End If
    End Sub
    Protected Sub dvLearning_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles dvLearning.ItemDeleted
        gvLearning.DataBind()
    End Sub
    Protected Sub dvLearning_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvLearning.ItemInserted
        gvLearning.DataBind()
    End Sub
    Protected Sub dvLearning_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvLearning.ItemUpdated
        gvLearning.DataBind()
    End Sub
    Protected Sub lnkNewLearning_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvLearning.ChangeMode(DetailsViewMode.Insert)
    End Sub
    Protected Sub lnkNewComment_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvComments.ChangeMode(DetailsViewMode.Insert)
    End Sub
    Protected Sub lnkNewIncentive_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvIncentive.ChangeMode(DetailsViewMode.Insert)
    End Sub
    Protected Sub lnkNewCoreComp_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvCoreComp.ChangeMode(DetailsViewMode.Insert)
    End Sub
    Protected Sub lnkNewCompetence_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvCompetence.ChangeMode(DetailsViewMode.Insert)
    End Sub
    Protected Sub lnkNewEmployee_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        dvEmployee.ChangeMode(DetailsViewMode.Insert)
    End Sub
    Public Sub build_links()
        Try
            If DropDownList1.SelectedValue <> "" Then
                Dim calYear As Int32 = ddlReviewYear.SelectedValue
                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT [PS_EMPLID],[Salaried] FROM [PerfEmployee] WHERE ([PS_EMPLID] = @PS_EMPLID)"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", DropDownList1.SelectedValue)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                    Dim name As String = DropDownList1.SelectedItem.Text
                    'Check to see if signed on employee is a manager using position number*****************************
                    If myLookup.Read Then

                        ImgSchedule.Visible = True
                        ImgReview.Visible = True
                        ImgEmail.Visible = True
                        ImgPrintPDF.Visible = True
                        lnkSelectedSub.Text = name
                        lblToggle.Visible = True
                        'imgToggle.Visible = True
                        lnkSchedule.Text = "Schedule Reviews"
                        lnkReview.Text = "Track Reviews"
                        lnkEmail.Text = "Email Reviews"
                        lnkPrintPDF.Text = "Print/View in PDF"
                        lblScheduledName.Text = name
                        lblReviewee.Text = name
                        If myLookup("Salaried") = "True" Or myLookup("Salaried") Is DBNull.Value Then
                            gvIncentive.Visible = True
                        Else
                            gvIncentive.Visible = False
                        End If
                        gvLearning.Visible = True
                        gvLearning.SelectedIndex = -1
                        gvIncentive.SelectedIndex = -1
                        check_for_cores(DropDownList1.SelectedValue, ddlReviewYear.SelectedValue)
                        gvCoreComp.Visible = True
                        gvCoreComp.SelectedIndex = -1
                        gvCompetence.Visible = True
                        gvCompetence.SelectedIndex = -1
                        gvComments.Visible = True
                        gvComments.SelectedIndex = -1
                    Else
                        gvIncentive.Visible = False
                        gvLearning.Visible = False
                        gvCoreComp.Visible = False
                        gvCompetence.Visible = False
                        gvComments.Visible = False
                        lnkSelectedSub.Text = Nothing
                        lblToggle.Visible = False
                        imgToggle.Visible = False
                        ImgSchedule.Visible = False
                        ImgReview.Visible = False
                        ImgEmail.Visible = False
                        ImgPrintPDF.Visible = False
                        lnkSchedule.Text = Nothing
                        lnkReview.Text = Nothing
                        lnkEmail.Text = Nothing
                        lnkPrintPDF.Text = Nothing
                    End If
                    myLookup.Close()
                    myConnection.Close()
                End Using
                'Using myCompetencyConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                '    Const SQLCompetencyCheck As String = "SELECT [REVIEW_YEAR] FROM [PerfCompetencies] WHERE ([REVIEW_YEAR] = @REVIEW_YEAR AND [PS_EMPLID] = @PS_EMPLID)"
                '    Dim myCompetencyCommand As New SqlCommand(SQLCompetencyCheck, myCompetencyConnection)                    
                '    myCompetencyCommand.Parameters.AddWithValue("@REVIEW_YEAR", calYear)
                '    myCompetencyCommand.Parameters.AddWithValue("@PS_EMPLID", DropDownList1.SelectedValue)
                '    myCompetencyConnection.Open()
                '    Dim myCompetencyCheck As SqlDataReader = myCompetencyCommand.ExecuteReader

                '    If myCompetencyCheck.Read Then
                '        'if review year exists for 
                '    Else
                '        lblClosed.Text += "year: " & calYear & " EMPLID: " & DropDownList1.SelectedValue                        
                '        Dim goal() As String = {"Adaptability/Flexibility", "Customer Focus", "Initiative", "Team Work"}
                '        For x = 0 To 3
                '            Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                '                Const SQLInsert As String = "Insert into [PerfCompetencies] ([PS_EMPLID],[Competence_Goal],[Review_Year]) VALUES (@PS_EMPLID,@Competence_Goal,@Review_Year)"
                '                Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)
                '                myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", DropDownList1.SelectedValue)
                '                myInsertCommand.Parameters.AddWithValue("@Competence_Goal", goal(x))                                
                '                myInsertCommand.Parameters.AddWithValue("@Review_Year", calYear)
                '                myInsertConnection.Open()
                '                myInsertCommand.ExecuteNonQuery()
                '                myInsertConnection.Close()
                '                'notification??
                '            End Using
                '        Next
                '    End If
                '    myCompetencyCheck.Close()
                '    myCompetencyConnection.Close()
                'End Using
            Else
                lnkSelectedSub.Text = Nothing
                lblToggle.Visible = False
                imgToggle.Visible = False
                ImgSchedule.Visible = False
                ImgReview.Visible = False
                ImgEmail.Visible = False
                ImgPrintPDF.Visible = False
            End If
        Catch
            lblEmployeeInfo.Text += Err.Description
        End Try
    End Sub

    Public Sub check_for_cores(emp As Int32, year As Int32)

        Dim cmd As New SqlCommand
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        conn.Close()
        cmd.Connection = conn
        cmd.Parameters.AddWithValue("@PS_EMPLID", emp)
        cmd.Parameters.AddWithValue("@Review_Year", year)
        conn.Open()
        Const lookupCF As String = "SELECT B.Competence_Goal,B.Competence_Assessment,B.Competence_Suggestion, B.CG_ID,B.Review_Year,B.Competence_Weight FROM PerfEmployee A, PerfCompetencies B WHERE A.PS_EMPLID = B.PS_EMPLID and A.PS_EMPLID = @PS_EMPLID  AND B.Review_Year = @Review_Year AND B.Competence_Goal = '(CORE) Customer Focus' order by b.CG_ID"
        Const lookupTW As String = "SELECT B.Competence_Goal,B.Competence_Assessment,B.Competence_Suggestion, B.CG_ID,B.Review_Year,B.Competence_Weight FROM PerfEmployee A, PerfCompetencies B WHERE A.PS_EMPLID = B.PS_EMPLID and A.PS_EMPLID = @PS_EMPLID  AND B.Review_Year = @Review_Year AND B.Competence_Goal = '(CORE) Teamwork' order by b.CG_ID"
        Const lookupFA As String = "SELECT B.Competence_Goal,B.Competence_Assessment,B.Competence_Suggestion, B.CG_ID,B.Review_Year,B.Competence_Weight FROM PerfEmployee A, PerfCompetencies B WHERE A.PS_EMPLID = B.PS_EMPLID and A.PS_EMPLID = @PS_EMPLID  AND B.Review_Year = @Review_Year AND B.Competence_Goal = '(CORE) Flexible/Adaptable' order by b.CG_ID"
        Const lookupEC As String = "SELECT B.Competence_Goal,B.Competence_Assessment,B.Competence_Suggestion, B.CG_ID,B.Review_Year,B.Competence_Weight FROM PerfEmployee A, PerfCompetencies B WHERE A.PS_EMPLID = B.PS_EMPLID and A.PS_EMPLID = @PS_EMPLID  AND B.Review_Year = @Review_Year AND B.Competence_Goal = '(CORE) Effective Communication' order by b.CG_ID"
        Const lookupAI As String = "SELECT B.Competence_Goal,B.Competence_Assessment,B.Competence_Suggestion, B.CG_ID,B.Review_Year,B.Competence_Weight FROM PerfEmployee A, PerfCompetencies B WHERE A.PS_EMPLID = B.PS_EMPLID and A.PS_EMPLID = @PS_EMPLID  AND B.Review_Year = @Review_Year AND B.Competence_Goal = '(CORE) Personal Accountability/Initiative' order by b.CG_ID"

        cmd.CommandText = lookupCF
        Dim reader As SqlDataReader = cmd.ExecuteReader
        If reader.Read Then
            reader.Close()
        Else
            reader.Close()
            cmd.CommandText = "Insert into [perfcompetencies] (ps_emplid, competence_goal, review_year) values (@PS_EMPLID, '(CORE) Customer Focus', @Review_Year)"
            cmd.ExecuteNonQuery()
        End If
        cmd.CommandText = lookupTW
        reader = cmd.ExecuteReader
        If reader.Read Then
            reader.Close()
        Else
            reader.Close()
            cmd.CommandText = "Insert into [perfcompetencies] (ps_emplid, competence_goal, review_year) values (@PS_EMPLID, '(CORE) Teamwork', @Review_Year)"
            cmd.ExecuteNonQuery()
        End If
        cmd.CommandText = lookupFA
        reader = cmd.ExecuteReader
        If reader.Read Then
            reader.Close()
        Else
            reader.Close()
            cmd.CommandText = "Insert into [perfcompetencies] (ps_emplid, competence_goal, review_year) values (@PS_EMPLID, '(CORE) Flexible/Adaptable', @Review_Year)"
            cmd.ExecuteNonQuery()
        End If
        cmd.CommandText = lookupEC
        reader = cmd.ExecuteReader
        If reader.Read Then
            reader.Close()
        Else
            reader.Close()
            cmd.CommandText = "Insert into [perfcompetencies] (ps_emplid, competence_goal, review_year) values (@PS_EMPLID, '(CORE) Effective Communication', @Review_Year)"
            cmd.ExecuteNonQuery()
        End If
        cmd.CommandText = lookupAI
        reader = cmd.ExecuteReader
        If reader.Read Then
            reader.Close()
        Else
            reader.Close()
            cmd.CommandText = "Insert into [perfcompetencies] (ps_emplid, competence_goal, review_year) values (@PS_EMPLID, '(CORE) Personal Accountability/Initiative', @Review_Year)"
            cmd.ExecuteNonQuery()
        End If

        conn.Close()
        gvCoreComp.DataBind()

    End Sub

    Public Sub build_status()
        Try
            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)

                Const SQLPSLookup As String = "SELECT A.EMPLID, A.POSITION_NBR " & _
                     " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A " & _
                     " WHERE ( A.EFFDT = " & _
                     " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                     " AND A_ED.EFFDT <= SYSDATE) " & _
                     " AND A.EFFSEQ = " & _
                     " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                     " AND A.EFFDT = A_ES.EFFDT) " & _
                     " AND (A.EMPLID = :EMPLID))"
                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":EMPLID", managerEmplID)
                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader
                'Check to see if signed on employee is a manager using position number*****************************
                If myPSLookup.Read Then

                    lblMgr.Text = username 'myPSLookup("DEPTID") & " - " & myPSLookup("DESCR") & " - " & myPSLookup("POSITION_NBR") & " " & myPSLookup("NAME") & " - " & myPSLookup("REPORTS_TO") & "<br/>"

                    Const SQLPSLookupMgr = "SELECT A.EMPLID " & _
                         " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A " & _
                         " WHERE ( A.EFFDT = " & _
                         " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                    " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                         " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                         " AND A_ED.EFFDT <= SYSDATE) " & _
                         " AND A.EFFSEQ = " & _
                         " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                    " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                         " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                         " AND A.EFFDT = A_ES.EFFDT) " & _
                         " AND A.REPORTS_TO = :POSNUM) " & _
                         " ORDER BY A.NAME ASC"
                    '********end search******************************************

                    Dim myPSLookupMgrCommand As New OracleCommand(SQLPSLookupMgr, myPSConnection)
                    myPSLookupMgrCommand.Parameters.AddWithValue(":POSNUM", myPSLookup("POSITION_NBR"))
                    Dim myPSLookupMgr As OracleDataReader = myPSLookupMgrCommand.ExecuteReader
                    'if there are subordinates list them in a select box****************************

                    Dim EMPIDlist As String = Nothing
                    Dim x As Int32 = 0
                    lblEmployeeInfo.Text = Nothing
                    While myPSLookupMgr.Read

                        If x = 0 Then
                            EMPIDlist += myPSLookupMgr("EMPLID")
                        Else
                            EMPIDlist += "," & myPSLookupMgr("EMPLID")
                        End If
                        x = x + 1

                    End While
                    myPSLookupMgr.Close()
                    Dim calYear As Int32 = ddlReviewYear.SelectedValue
                    Dim sqlSched As String = Nothing
                    If x > 0 Then
                        sqlSched = "SELECT B.Employee_First_Name, B.Employee_Last_Name, B.Employee_Title, A.Scheduled_Review1, A.Scheduled_Review2, " & _
                        " A.Scheduled_Review3, A.Scheduled_Final_Review, A.PS_EMPLID" & _
                        " FROM PerfReviewSchedule AS A RIGHT OUTER JOIN" & _
                        " PerfEmployee AS B ON A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID IN (" & EMPIDlist & ") AND A.Review_Year = " & calYear & "" & _
                        " WHERE (B.PS_EMPLID IN (" & EMPIDlist & "))" & _
                        " ORDER BY B.Employee_Last_Name,B.Employee_First_Name"

                        ViewState("select") = sqlSched
                        SqlSchedules.SelectCommand = sqlSched
                        SqlSchedules.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                        gvSchedules.DataSource = SqlSchedules
                        gvSchedules.DataBind()

                    End If
                End If
                myPSLookup.Close()
                myPSConnection.Close()
            End Using
        Catch
            lblEmployeeInfo.Text += "build_status: " & Err.Description
        End Try
        '****************************************************************

    End Sub
    Protected Sub gvSchedules_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSchedules.DataBound
        gvSchedules.Caption = "Scheduled Reviews"
        imgKey.Visible = True
    End Sub
    Protected Sub gvSchedules_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSchedules.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Text = "status"
            e.Row.Cells(1).Text = "First Name"
            e.Row.Cells(2).Text = "Last Name"
            e.Row.Cells(3).Text = "Title"
            e.Row.Cells(4).Text = "Schd. Review 1"
            e.Row.Cells(5).Text = "Schd. Review 2"
            e.Row.Cells(6).Text = "Schd. Review 3"
            e.Row.Cells(7).Text = "Schd. Final Review"
            e.Row.Cells(8).Text = "Employee ID"

        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            'Bug in conversion from string to date for null values in gridview, review the null date value "1/1/1900"
            Try
                Dim qcount As Int32 = 0
                Dim calYear As Int32 = ddlReviewYear.SelectedValue
                e.Row.Cells(0).ForeColor = Drawing.Color.Green
                e.Row.Cells(4).Text = CType(e.Row.Cells(4).Text, Date).ToShortDateString
                e.Row.Cells(5).Text = CType(e.Row.Cells(5).Text, Date).ToShortDateString
                e.Row.Cells(6).Text = CType(e.Row.Cells(6).Text, Date).ToShortDateString
                e.Row.Cells(7).Text = CType(e.Row.Cells(7).Text, Date).ToShortDateString
                e.Row.Cells(4).ForeColor = Drawing.Color.Green
                e.Row.Cells(5).ForeColor = Drawing.Color.Green
                e.Row.Cells(6).ForeColor = Drawing.Color.Green
                e.Row.Cells(7).ForeColor = Drawing.Color.Green

                If CType(e.Row.Cells(5).Text, Date) = "1/1/1900" Then
                    e.Row.Cells(5).Text = "unscheduled"
                    e.Row.Cells(5).ForeColor = Drawing.Color.Purple
                End If

                If CType(e.Row.Cells(6).Text, Date) = "1/1/1900" Then
                    e.Row.Cells(6).Text = "unscheduled"
                    e.Row.Cells(6).ForeColor = Drawing.Color.Purple
                End If

                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT B.Employee_First_Name,B.Employee_Last_Name,B.Employee_Title,A.Review_Date1,A.Review_Date2,A.Review_Date3,A.Final_Review_Date,B.PS_EMPLID FROM PerfReviewDates A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = @PS_EMPLID AND A.Review_Year = @Review_Year)"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", e.Row.Cells(8).Text)
                    myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                    
                    Dim closed As Boolean
                    Dim gvemplid As Int32 = e.Row.Cells(8).Text
                    closed = flag_if_closed(calYear, gvemplid)
                    If myLookup.Read Then
                        qcount += 1

                        If CType(e.Row.Cells(4).Text, DateTime) <= Now And myLookup("Review_Date1") = "1/1/1900" Then
                            e.Row.Cells(4).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If

                        If e.Row.Cells(5).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(5).Text, DateTime) <= Now And myLookup("Review_Date2") = "1/1/1900" Then
                                e.Row.Cells(5).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If

                        If e.Row.Cells(6).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(6).Text, DateTime) <= Now And myLookup("Review_Date3") = "1/1/1900" Then
                                e.Row.Cells(6).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If

                        If CType(e.Row.Cells(7).Text, DateTime) <= Now And myLookup("Final_Review_Date") = "1/1/1900" Then
                            e.Row.Cells(7).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If

                        If closed Then
                            e.Row.Cells(0).ForeColor = Drawing.Color.Black
                            e.Row.Cells(4).ForeColor = Drawing.Color.Black
                            e.Row.Cells(5).ForeColor = Drawing.Color.Black
                            e.Row.Cells(6).ForeColor = Drawing.Color.Black
                            e.Row.Cells(7).ForeColor = Drawing.Color.Black
                        End If
                    Else
                        If CType(e.Row.Cells(4).Text, DateTime) <= Now Then
                            e.Row.Cells(4).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If
                        If e.Row.Cells(5).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(5).Text, DateTime) <= Now Then
                                e.Row.Cells(5).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If
                        If e.Row.Cells(6).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(6).Text, DateTime) <= Now Then
                                e.Row.Cells(6).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If
                        If CType(e.Row.Cells(7).Text, DateTime) <= Now Then
                            e.Row.Cells(7).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If

                        If closed Then
                            e.Row.Cells(0).ForeColor = Drawing.Color.Black
                            e.Row.Cells(4).ForeColor = Drawing.Color.Black
                            e.Row.Cells(5).ForeColor = Drawing.Color.Black
                            e.Row.Cells(6).ForeColor = Drawing.Color.Black
                            e.Row.Cells(7).ForeColor = Drawing.Color.Black
                        End If
                    End If
                    myLookup.Close()
                    myConnection.Close()

                End Using
            Catch
                'lblEmployeeInfo.Text += "gvschedules_rowdatabound: " & Err.Description
            End Try
        End If
    End Sub
    Protected Sub dvIncentive_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvIncentive.ItemUpdating
        Dim ddlIncentiveAssessment As DropDownList = (CType(dvIncentive.FindControl("ddlIncentiveAssessmentEdit"), DropDownList))
        Dim TextBox2 As TextBox = (CType(dvIncentive.FindControl("TextBox2"), TextBox))
        Dim txtInctcmnt As TextBox = (CType(dvIncentive.FindControl("txtInctcmnt"), TextBox))
        Dim lblIncentEdit As Label = (CType(dvIncentive.FindControl("lblIncentEdit"), Label))

        If e.CommandArgument = "Update" Then
            If ddlIncentiveAssessment.SelectedValue <> "ZZ" Then
                e.NewValues("Incentive_Assessment") = ddlIncentiveAssessment.SelectedValue
            End If
            If ddlIncentiveAssessment.SelectedValue <> "ME" And ddlIncentiveAssessment.SelectedValue <> "ZZ" And ddlIncentiveAssessment.SelectedValue <> "NA" Then
                If txtInctcmnt.Text = "" Then
                    e.Cancel = True
                    lblIncentEdit.Visible = True
                End If
            End If
            If TextBox2.Text = "" Then
                e.Cancel = True
            End If
        End If
    End Sub
    Protected Sub dvIncentive_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvIncentive.ItemInserting
        Dim ddlIncentiveAssessment As DropDownList = (CType(dvIncentive.FindControl("ddlIncentiveAssessmentInsert"), DropDownList))
        Dim TextBox2 As TextBox = (CType(dvIncentive.FindControl("TextBox2"), TextBox))
        Dim txtInctcmnt As TextBox = (CType(dvIncentive.FindControl("txtInctcmnt"), TextBox))
        Dim lblIncentInsert As Label = (CType(dvIncentive.FindControl("lblIncentInsert"), Label))

        If e.CommandArgument = "Insert" Then
            If ddlIncentiveAssessment.SelectedValue <> "ZZ" Then
                e.Values("Incentive_Assessment") = ddlIncentiveAssessment.SelectedValue
            End If
            If ddlIncentiveAssessment.SelectedValue <> "ME" And ddlIncentiveAssessment.SelectedValue <> "ZZ" And ddlIncentiveAssessment.SelectedValue <> "NA" Then
                If txtInctcmnt.Text = "" Then
                    e.Cancel = True
                    lblIncentInsert.Visible = True
                End If
            End If
            If TextBox2.Text = "" Then
                e.Cancel = True
            End If
        End If
    End Sub
    Protected Sub dvCoreComp_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvCoreComp.ItemUpdating
        Dim ddlCoreCompAssessment As DropDownList = (CType(dvCoreComp.FindControl("ddlCoreCompAssessmentEdit"), DropDownList))
        Dim ddlCoreCompWeight As DropDownList = (CType(dvCoreComp.FindControl("ddlCoreCompWeightEdit"), DropDownList))
        Dim ddlCoreCompEdit As DropDownList = (CType(dvCoreComp.FindControl("ddlCoreCompEdit"), DropDownList))
        Dim txtCoreCompEdit As TextBox = (CType(dvCoreComp.FindControl("txtCoreCompEdit"), TextBox))

        If e.CommandArgument = "Update" Then
            If ddlCoreCompAssessment.SelectedValue <> "00" Then
                e.NewValues("Competence_Assessment") = ddlCoreCompAssessment.SelectedValue
            End If
            If ddlCoreCompWeight.SelectedValue <> "NA" Then
                e.NewValues("Competence_Weight") = ddlCoreCompWeight.SelectedValue
            End If
        End If
    End Sub
    Protected Sub dvCompetence_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvCompetence.ItemUpdating
        Dim ddlCompetenceAssessment As DropDownList = (CType(dvCompetence.FindControl("ddlCompetenceAssessmentEdit"), DropDownList))
        Dim ddlCompetenceWeight As DropDownList = (CType(dvCompetence.FindControl("ddlCompetenceWeightEdit"), DropDownList))
        Dim ddlCompEdit As DropDownList = (CType(dvCompetence.FindControl("ddlCompEdit"), DropDownList))
        Dim txtCompEdit As TextBox = (CType(dvCompetence.FindControl("txtCompEdit"), TextBox))

        If e.CommandArgument = "Update" Then
            If ddlCompetenceAssessment.SelectedValue <> "00" Then
                e.NewValues("Competence_Assessment") = ddlCompetenceAssessment.SelectedValue
            End If
            If ddlCompetenceWeight.SelectedValue <> "NA" Then
                e.NewValues("Competence_Weight") = ddlCompetenceWeight.SelectedValue
            End If
            If ddlCompEdit.SelectedValue = "0" And txtCompEdit.Text = "" Then
                e.NewValues("Competence_Goal") = e.OldValues("Competence_Goal")
            Else
                If ddlCompEdit.SelectedValue <> "0" Then
                    e.NewValues("Competence_Goal") = ddlCompEdit.SelectedItem.Text
                Else
                    e.NewValues("Competence_Goal") = txtCompEdit.Text
                End If
            End If
        End If
    End Sub
    Protected Sub dvCoreComp_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvCoreComp.ItemInserting
        Dim ddlCoreCompAssessment As DropDownList = (CType(dvCoreComp.FindControl("ddlCoreCompAssessmentInsert"), DropDownList))
        Dim ddlCoreCompWeight As DropDownList = (CType(dvCoreComp.FindControl("ddlCoreCompWeightInsert"), DropDownList))
        Dim ddlCoreCompInsert As DropDownList = (CType(dvCoreComp.FindControl("ddlCoreCompInsert"), DropDownList))
        Dim txtCoreCompInsert As TextBox = (CType(dvCoreComp.FindControl("txtCoreCompInsert"), TextBox))

        If e.CommandArgument = "Insert" Then
            If ddlCoreCompAssessment.SelectedValue <> "00" Then
                e.Values("Competence_Assessment") = ddlCoreCompAssessment.SelectedValue
            End If
            If ddlCoreCompWeight.SelectedValue <> "NA" Then
                e.Values("Competence_Weight") = ddlCoreCompWeight.SelectedValue
            End If
            If ddlCoreCompInsert.SelectedValue <> "0" Then
                e.Values("Competence_Goal") = ddlCoreCompInsert.SelectedItem.Text
            Else
                'If txtCompInsert.Text = "" Then
                e.Values("Competence_Goal") = txtCoreCompInsert.Text
                'End If
            End If
            If txtCoreCompInsert.Text = "" Then
                If ddlCoreCompInsert.SelectedValue = "0" Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub
    Protected Sub dvCompetence_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvCompetence.ItemInserting
        Dim ddlCompetenceAssessment As DropDownList = (CType(dvCompetence.FindControl("ddlCompetenceAssessmentInsert"), DropDownList))
        Dim ddlCompetenceWeight As DropDownList = (CType(dvCompetence.FindControl("ddlCompetenceWeightInsert"), DropDownList))
        Dim ddlCompInsert As DropDownList = (CType(dvCompetence.FindControl("ddlCompInsert"), DropDownList))
        Dim txtCompInsert As TextBox = (CType(dvCompetence.FindControl("txtCompInsert"), TextBox))

        If e.CommandArgument = "Insert" Then
            If ddlCompetenceAssessment.SelectedValue <> "00" Then
                e.Values("Competence_Assessment") = ddlCompetenceAssessment.SelectedValue
            End If
            If ddlCompetenceWeight.SelectedValue <> "NA" Then
                e.Values("Competence_Weight") = ddlCompetenceWeight.SelectedValue
            End If
            If ddlCompInsert.SelectedValue <> "0" Then
                e.Values("Competence_Goal") = ddlCompInsert.SelectedItem.Text
            Else
                'If txtCompInsert.Text = "" Then
                e.Values("Competence_Goal") = txtCompInsert.Text
                'End If
            End If
            If txtCompInsert.Text = "" Then
                If ddlCompInsert.SelectedValue = "0" Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub
    Protected Sub dvLearning_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvLearning.ItemUpdating
        Dim ddlLearnAssessment As DropDownList = (CType(dvLearning.FindControl("ddlLearnAssessmentEdit"), DropDownList))
        Dim TextBox2 As TextBox = (CType(dvLearning.FindControl("TextBox2"), TextBox))
        Dim txtlearncmnt As TextBox = (CType(dvLearning.FindControl("txtlearncmnt"), TextBox))
        Dim lblLearnEdit As Label = (CType(dvLearning.FindControl("lblLearnEdit"), Label))

        If e.CommandArgument = "Update" Then
            If ddlLearnAssessment.SelectedValue <> "ZZ" Then
                e.NewValues("Learning_Assessment") = ddlLearnAssessment.SelectedValue
            End If
            If ddlLearnAssessment.SelectedValue <> "ME" And ddlLearnAssessment.SelectedValue <> "ZZ" And ddlLearnAssessment.SelectedValue <> "NA" Then
                If txtlearncmnt.Text = "" Then
                    e.Cancel = True
                    lblLearnEdit.Visible = True
                End If
            End If
            If TextBox2.Text = "" Then
                e.Cancel = True
            End If
        End If
    End Sub
    Protected Sub dvLearning_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvLearning.ItemInserting
        Dim ddlLearnAssessment As DropDownList = (CType(dvLearning.FindControl("ddlLearnAssessmentInsert"), DropDownList))
        Dim TextBox2 As TextBox = (CType(dvLearning.FindControl("TextBox2"), TextBox))
        Dim txtlearncmnt As TextBox = (CType(dvLearning.FindControl("txtlearncmnt"), TextBox))
        Dim lblLearnInsert As Label = (CType(dvLearning.FindControl("lblLearnInsert"), Label))

        If e.CommandArgument = "Insert" Then
            If ddlLearnAssessment.SelectedValue <> "ZZ" Then
                e.Values("Learning_Assessment") = ddlLearnAssessment.SelectedValue
            End If
            If ddlLearnAssessment.SelectedValue <> "ME" And ddlLearnAssessment.SelectedValue <> "ZZ" And ddlLearnAssessment.SelectedValue <> "NA" Then
                If txtlearncmnt.Text = "" Then
                    e.Cancel = True
                    lblLearnInsert.Visible = True
                End If
            End If
            If TextBox2.Text = "" Then
                e.Cancel = True
            End If
        End If
    End Sub
    Protected Sub dvEmployee_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles dvEmployee.ItemInserted

        If e.Values("Salaried") = "True" Then
            gvIncentive.Visible = True
        Else
            gvIncentive.Visible = False
        End If
    End Sub
    Protected Sub dvEmployee_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles dvEmployee.ItemUpdated
        If e.NewValues("Salaried") = "True" Then
            gvIncentive.Visible = True
        Else
            gvIncentive.Visible = False
        End If
    End Sub
    Protected Sub ddlReviewYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReviewYear.SelectedIndexChanged
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        lblSchedYear.Text = ddlReviewYear.SelectedValue
        lblTrackYear.Text = ddlReviewYear.SelectedValue
        initialize_schedule()
        initialize_review_date()
        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT [Manager_SignOn],[PS_EMPLID] FROM [PerfEmployee] WHERE ([Manager_SignOn] = @Manager_SignOn)"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@Manager_SignOn", username)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
            'Check to see if signed on employee is a manager using position number*****************************
            If myLookup.Read Then
                managerEmplID = myLookup("PS_EMPLID")
                gvSchedules.Visible = True
                pnlManagerView.Visible = True
            Else
                gvSchedules.Visible = False
                pnlManagerView.Visible = False
            End If
            myLookup.Close()
            myConnection.Close()
        End Using
        build_status()
        build_links()
        Dim closed As Boolean

        closed = review_is_closed(calYear)
        If closed Then
            lblClosed.Text = "Review Closed"
        Else
            lblClosed.Text = Nothing
        End If
        gvLearning.DataBind()
        lblSelectedYear.Text = ddlReviewYear.SelectedValue
    End Sub
    'uncomment for test with dropdown**********
    Protected Sub ddlManagers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlManagers.SelectedIndexChanged
        lblBaseSalary.Visible = False
        lblTotalBaseSalary.Text = Nothing
        lblDollarsAvailable.Text = Nothing
        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT [Manager_SignOn] FROM [PerfEmployee] WHERE ([Manager_SignOn] = @Manager_SignOn)"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@Manager_SignOn", username)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
            'Check to see if signed on employee is a manager using position number*****************************
            If myLookup.Read Then
                managerEmplID = ddlManagers.SelectedValue
                populate_dropdownlist1()
                'uncomment upon request from HR
                'setmeritlabels()
                pnlManagerView.Visible = True
                gvSchedules.Visible = True
            Else
                gvSchedules.Visible = False
                pnlManagerView.Visible = False
                DropDownList1.Items.Clear()
                Dim EmptyList As New ListItem("--Choose Employee--", "")
                DropDownList1.Items.Add(EmptyList)
                lnkSelectedSub.Text = Nothing
                lblToggle.Visible = False
                imgToggle.Visible = False
                ImgSchedule.Visible = False
                ImgReview.Visible = False
                ImgEmail.Visible = False
                ImgPrintPDF.Visible = False
                lnkSchedule.Text = Nothing
                lnkReview.Text = Nothing
                lnkEmail.Text = Nothing
                lnkPrintPDF.Text = Nothing
            End If
            myLookup.Close()
            myConnection.Close()
        End Using
    End Sub
    '***************************************
    Public Sub populate_dropdownlist1()
        'get position code********************************************        

        Try
            lnkSelectedSub.Text = Nothing
            lblToggle.Visible = False
            imgToggle.Visible = False
            ImgSchedule.Visible = False
            ImgReview.Visible = False
            ImgEmail.Visible = False
            ImgPrintPDF.Visible = False
            lnkSchedule.Text = Nothing
            lnkReview.Text = Nothing
            lnkEmail.Text = Nothing
            lnkPrintPDF.Text = Nothing

            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)

                Const SQLPSLookup As String = "SELECT A.EMPLID, A.POSITION_NBR " & _
                     " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A" & _
                     " WHERE ( A.EFFDT = " & _
                     " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                     " AND A_ED.EFFDT <= SYSDATE) " & _
                     " AND A.EFFSEQ = " & _
                     " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                     " AND A.EFFDT = A_ES.EFFDT)" & _
                     " AND (A.EMPLID = :EMPLID))"
                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":EMPLID", managerEmplID)
                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader

                'Check to see if signed on employee is a manager using position number*****************************
                If myPSLookup.Read Then

                    'lblMgr.Text = myPSLookup("DEPTID") & " - " & myPSLookup("DESCR") & " - " & myPSLookup("POSITION_NBR") & " " & myPSLookup("NAME") & " - " & myPSLookup("REPORTS_TO") & "<br/>"
                    ', sysadm.PS_LOCATION_TBL D
                    ', D.DESCRSHORT, D.SETID, D.LOCATION, TO_CHAR(D.EFFDT,'YYYY-MM-DD')
                    'example: total for IT - SM - 502486.4 add field A.ANNUAL_RT
                    Const SQLPSLookupMgr As String = "SELECT A.EMPLID, A.NAME, A.FIRST_NAME, A.LAST_NAME, A.POSITION_NBR, B.JOBCODE, B.DEPTID, C.DESCR, C.SETID" & _
                        " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A, SYSADM.EDE_PORTAL_PS_JOB_VW B, sysadm.PS_JOBCODE_TBL C" & _
                        " WHERE ( A.EFFDT =" & _
                        " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED" & _
                                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                        " AND A.EMPL_RCD = A_ED.EMPL_RCD" & _
                        " AND A_ED.EFFDT <= SYSDATE)" & _
                        " AND A.EFFSEQ =" & _
                        " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES" & _
                                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                        " AND A.EMPL_RCD = A_ES.EMPL_RCD" & _
                        " AND A.EFFDT = A_ES.EFFDT)" & _
                        " AND A.EMPLID = B.EMPLID" & _
                        " AND A.EMPL_RCD = B.EMPL_RCD" & _
                        " AND B.EFFDT =" & _
                        " (SELECT MAX(B_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_JOB_VW B_ED" & _
                                " WHERE(B.EMPLID = B_ED.EMPLID)" & _
                        " AND B.EMPL_RCD = B_ED.EMPL_RCD" & _
                        " AND B_ED.EFFDT <= A.EFFDT)" & _
                        " AND B.EFFSEQ =" & _
                        " (SELECT MAX(B_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_JOB_VW B_ES" & _
                                " WHERE(B.EMPLID = B_ES.EMPLID)" & _
                        " AND B.EMPL_RCD = B_ES.EMPL_RCD" & _
                        " AND B.EFFDT = B_ES.EFFDT)" & _
                        " AND C.JOBCODE = B.JOBCODE" & _
                        " AND C.EFFDT =" & _
                        " (SELECT MAX(C_ED.EFFDT) FROM sysadm.PS_JOBCODE_TBL C_ED" & _
                                " WHERE(C.SETID = C_ED.SETID)" & _
                        " AND C.JOBCODE = C_ED.JOBCODE" & _
                        " AND C_ED.EFFDT <= B.EFFDT) )" & _
                        " AND A.REPORTS_TO = :POSNUM" & _
                        " ORDER BY A.NAME ASC"
                    '" AND D.LOCATION = A.LOCATION" & _
                    '" AND D.EFFDT =" & _
                    '" (SELECT MAX(D_ED.EFFDT) FROM sysadm.PS_LOCATION_TBL D_ED" & _
                    '    " WHERE(D.SETID = D_ED.SETID)" & _
                    '" AND D.LOCATION = D_ED.LOCATION" & _
                    '" AND D_ED.EFFDT <= A.EFFDT) )" & _
                    '********end search******************************************

                    Dim myPSLookupMgrCommand As New OracleCommand(SQLPSLookupMgr, myPSConnection)
                    myPSLookupMgrCommand.Parameters.AddWithValue(":POSNUM", myPSLookup("POSITION_NBR"))
                    Dim myPSLookupMgr As OracleDataReader = myPSLookupMgrCommand.ExecuteReader
                    Dim fName As String = Nothing
                    Dim lName As String = Nothing
                    'if there are subordinates list them in a select box****************************

                    DropDownList1.Items.Clear()
                    Dim EmptyList As New ListItem("--Choose Employee--", "")
                    DropDownList1.Items.Add(EmptyList)
                    ddlReviewYear.Items.Clear()
                    gvIncentive.Visible = False
                    gvLearning.Visible = False
                    gvCoreComp.Visible = False
                    gvCompetence.Visible = False
                    gvComments.Visible = False

                    Dim ThisYear = Now.Year
                    Dim firstYear = Year("1/1/2009")
                    Dim x As Int32
                    For x = firstYear To ThisYear + 1
                        ddlReviewYear.Items.Add(New ListItem(x.ToString, x))
                    Next
                    ddlReviewYear.SelectedValue = ThisYear
                    While myPSLookupMgr.Read
                        Dim EmployeesList As New ListItem(myPSLookupMgr("NAME"), myPSLookupMgr("EMPLID"))
                        DropDownList1.Items.Add(EmployeesList)
                        'insert all subordinates to the database*****                        
                        fName = myPSLookupMgr("FIRST_NAME")
                        lName = myPSLookupMgr("LAST_NAME")
                        Using mySubConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Const SQLSubCheck As String = "SELECT [PS_EMPLID] FROM [PerfEmployee] WHERE ([PS_EMPLID] = @PS_EMPLID)"
                            Dim myLookupCommand As New SqlCommand(SQLSubCheck, mySubConnection)
                            myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", myPSLookupMgr("EMPLID"))
                            mySubConnection.Open()
                            Dim mySubCheck As SqlDataReader = myLookupCommand.ExecuteReader

                            If mySubCheck.Read Then
                                'do nothing for now but we may need to synch with PSOFT DB
                                Using myUpdateConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                    Const SQLUpdate As String = "Update [PerfEmployee] set [Employee_First_Name] = @Employee_First_Name,[Employee_Last_Name] = @Employee_Last_Name,[Employee_Title] = @Employee_Title,[Salaried] = @Salaried WHERE PS_EMPLID = @PS_EMPLID"
                                    Dim myUpdateCommand As New SqlCommand(SQLUpdate, myUpdateConnection)
                                    myUpdateCommand.Parameters.AddWithValue("@PS_EMPLID", myPSLookupMgr("EMPLID"))
                                    myUpdateCommand.Parameters.AddWithValue("@Employee_First_Name", fName)
                                    myUpdateCommand.Parameters.AddWithValue("@Employee_Last_Name", lName)
                                    myUpdateCommand.Parameters.AddWithValue("@Employee_Title", myPSLookupMgr("DESCR"))
                                    myUpdateCommand.Parameters.AddWithValue("@Salaried", True)
                                    myUpdateConnection.Open()
                                    myUpdateCommand.ExecuteNonQuery()
                                    myUpdateConnection.Close()
                                    'notification??
                                End Using
                            Else
                                Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                    Const SQLInsert As String = "Insert into [PerfEmployee] ([PS_EMPLID],[Employee_First_Name],[Employee_Last_Name],[Employee_Title],[Salaried]) VALUES (@PS_EMPLID,@Employee_First_Name,@Employee_Last_Name,@Employee_Title,@Salaried)"
                                    Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)
                                    myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", myPSLookupMgr("EMPLID"))
                                    myInsertCommand.Parameters.AddWithValue("@Employee_First_Name", fName)
                                    myInsertCommand.Parameters.AddWithValue("@Employee_Last_Name", lName)
                                    myInsertCommand.Parameters.AddWithValue("@Employee_Title", myPSLookupMgr("DESCR"))
                                    myInsertCommand.Parameters.AddWithValue("@Salaried", True)
                                    myInsertConnection.Open()
                                    myInsertCommand.ExecuteNonQuery()
                                    myInsertConnection.Close()
                                    'notification??
                                End Using

                            End If
                            mySubCheck.Close()
                            mySubConnection.Close()
                        End Using

                        '********************************************

                    End While
                    build_status()
                    myPSLookupMgr.Close()
                End If
                myPSLookup.Close()
                myPSConnection.Close()
            End Using
        Catch
            lblEmployeeInfo.Text += "populate_dropdownlist1: " & Err.Description
        End Try
        '****************************************************************
    End Sub
    Public Sub get_Review_dates()
        Try
            Dim EMPLID As Int32
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            If DropDownList1.SelectedValue <> "" Then
                EMPLID = DropDownList1.SelectedValue
            End If
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup As String = "SELECT A.Review_Date1,A.Review_Date2,A.Review_Date3,A.Final_Review_Date FROM PerfReviewDates A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = @PS_EMPLID AND A.Review_Year = @Review_Year)"
                Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                myConnection.Open()
                Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                If myLookup.Read Then
                    lblReview8.Text = myLookup("Final_Review_Date")
                    If myLookup("Final_Review_Date") = "1/1/1900" Then
                        lblReview8.Text = "Pending"
                        lblReview8.ForeColor = Drawing.Color.White
                    Else
                        lblReview8.Text = myLookup("Final_Review_Date")
                        lblReview8.ForeColor = Drawing.Color.Black
                    End If
                    lblReview7.Text = myLookup("Review_Date3")
                    If myLookup("Review_Date3") = "1/1/1900" Then
                        lblReview7.Text = "Pending"
                        lblReview7.ForeColor = Drawing.Color.White
                    Else
                        lblReview7.Text = myLookup("Review_Date3")
                        lblReview7.ForeColor = Drawing.Color.Black
                    End If
                    lblReview6.Text = myLookup("Review_Date2")
                    If myLookup("Review_Date2") = "1/1/1900" Then
                        lblReview6.Text = "Pending"
                        lblReview6.ForeColor = Drawing.Color.White
                    Else
                        lblReview6.Text = myLookup("Review_Date2")
                        lblReview6.ForeColor = Drawing.Color.Black
                    End If
                    lblReview5.Text = myLookup("Review_Date1")
                    If myLookup("Review_Date1") = "1/1/1900" Then
                        lblReview5.Text = "Pending"
                        lblReview5.ForeColor = Drawing.Color.White
                    Else
                        lblReview5.Text = myLookup("Review_Date1")
                        lblReview5.ForeColor = Drawing.Color.Black
                    End If

                Else
                    lblReview8.Text = "Pending"
                    lblReview8.ForeColor = Drawing.Color.White
                    lblReview7.Text = "Pending"
                    lblReview7.ForeColor = Drawing.Color.White
                    lblReview6.Text = "Pending"
                    lblReview6.ForeColor = Drawing.Color.White
                    lblReview5.Text = "Pending"
                    lblReview5.ForeColor = Drawing.Color.White

                End If
                myLookup.Close()
                myConnection.Close()
            End Using
        Catch
            lblEmployeeInfo.Text += "get_review_dates " & Err.Description
        End Try
    End Sub
    Public Sub get_scheduled_dates()
        Try
            Dim EMPLID As Int32
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            If DropDownList1.SelectedValue <> "" Then
                EMPLID = DropDownList1.SelectedValue
            End If

            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup As String = "SELECT A.Scheduled_Review1,A.Scheduled_Review2,A.Scheduled_Review3,A.Scheduled_Final_Review FROM PerfReviewSchedule A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = @PS_EMPLID AND A.Review_Year = @Review_Year)"
                Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                myConnection.Open()
                Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

                If myLookup.Read Then
                    lblReview4.Text = myLookup("Scheduled_Final_Review")
                    If myLookup("Scheduled_Final_Review") = "1/1/1900" Then
                        lblReview4.Text = "Unscheduled"
                        lblReview4.ForeColor = Drawing.Color.White
                    Else
                        lblReview4.Text = myLookup("Scheduled_Final_Review")
                        lblReview4.ForeColor = Drawing.Color.Black
                    End If
                    lblReview3.Text = myLookup("Scheduled_Review3")
                    If myLookup("Scheduled_Review3") = "1/1/1900" Then
                        lblReview3.Text = "Unscheduled"
                        lblReview3.ForeColor = Drawing.Color.White
                    Else
                        lblReview3.Text = myLookup("Scheduled_Review3")
                        lblReview3.ForeColor = Drawing.Color.Black
                    End If
                    lblReview2.Text = myLookup("Scheduled_Review2")
                    If myLookup("Scheduled_Review2") = "1/1/1900" Then
                        lblReview2.Text = "Unscheduled"
                        lblReview2.ForeColor = Drawing.Color.White
                    Else
                        lblReview2.Text = myLookup("Scheduled_Review2")
                        lblReview2.ForeColor = Drawing.Color.Black
                    End If
                    lblReview1.Text = myLookup("Scheduled_Review1")
                    If myLookup("Scheduled_Review1") = "1/1/1900" Then
                        lblReview1.Text = "Unscheduled"
                        lblReview1.ForeColor = Drawing.Color.White
                    Else
                        lblReview1.Text = myLookup("Scheduled_Review1")
                        lblReview1.ForeColor = Drawing.Color.Black
                    End If

                Else
                    lblReview4.Text = "Unscheduled"
                    lblReview4.ForeColor = Drawing.Color.White
                    lblReview3.Text = "Unscheduled"
                    lblReview3.ForeColor = Drawing.Color.White
                    lblReview2.Text = "Unscheduled"
                    lblReview2.ForeColor = Drawing.Color.White
                    lblReview1.Text = "Unscheduled"
                    lblReview1.ForeColor = Drawing.Color.White

                End If
                myLookup.Close()
                myConnection.Close()
            End Using

        Catch
            lblEmployeeInfo.Text += "get_schedule_dates: " & Err.Description
        End Try
    End Sub
    Public Sub update_schedule(ByVal EMPLID As Int32, ByVal calYear As String, ByVal Review1 As String, ByVal Review2 As String, ByVal Review3 As String, ByVal Review4 As String)
        Try
            Dim strEmail As String = ""
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup As String = "SELECT [Review_Year] FROM [PerfReviewSchedule] WHERE ([PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year)"
                Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                myConnection.Open()
                Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                If myLookup.Read Then
                    Using myUpdateConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                        Const SQLUpdate As String = "Update [PerfReviewSchedule] SET [Review_Year] = @Review_Year,[Scheduled_Review1] = @Scheduled_Review1,[Scheduled_Review2] = @Scheduled_Review2,[Scheduled_Review3] = @Scheduled_Review3,[Scheduled_Final_Review] = @Scheduled_Final_Review WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
                        Dim myUpdateCommand As New SqlCommand(SQLUpdate, myUpdateConnection)
                        myUpdateCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                        myUpdateCommand.Parameters.AddWithValue("@Review_Year", calYear)
                        myUpdateCommand.Parameters.AddWithValue("@Scheduled_Review1", Review1)
                        myUpdateCommand.Parameters.AddWithValue("@Scheduled_Review2", Review2)
                        myUpdateCommand.Parameters.AddWithValue("@Scheduled_Review3", Review3)
                        myUpdateCommand.Parameters.AddWithValue("@Scheduled_Final_Review", Review4)
                        myUpdateConnection.Open()
                        myUpdateCommand.ExecuteNonQuery()
                        myUpdateConnection.Close()
                        lblMessage.Text = "Schedule Updated"

                    End Using
                Else
                    Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                        Const SQLInsert As String = "Insert into [PerfReviewSchedule] ([PS_EMPLID],[Review_Year],[Scheduled_Review1],[Scheduled_Review2],[Scheduled_Review3],[Scheduled_Final_Review]) VALUES (@PS_EMPLID,@Review_Year,@Scheduled_Review1,@Scheduled_Review2,@Scheduled_Review3,@Scheduled_Final_Review)"
                        Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)

                        myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                        myInsertCommand.Parameters.AddWithValue("@Review_Year", calYear)
                        myInsertCommand.Parameters.AddWithValue("@Scheduled_Review1", Review1)
                        myInsertCommand.Parameters.AddWithValue("@Scheduled_Review2", Review2)
                        myInsertCommand.Parameters.AddWithValue("@Scheduled_Review3", Review3)
                        myInsertCommand.Parameters.AddWithValue("@Scheduled_Final_Review", Review4)
                        myInsertConnection.Open()
                        myInsertCommand.ExecuteNonQuery()
                        myInsertConnection.Close()
                        lblMessage.Text = "Schedule Created"
                    End Using
                End If

                myLookup.Close()
                myConnection.Close()

            End Using
            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup2 As String = "SELECT [Employee_Signon] FROM [PerfEmployee] WHERE ([PS_EMPLID] = @PS_EMPLID)"
                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myConnection2.Open()
                Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

                If myLookup2.Read Then
                    strEmail = myLookup2("Employee_Signon").ToString
                End If
                myLookup2.Close()
                myConnection2.Close()
            End Using

            Dim dates As String = Nothing

            If Review1 <> "" Then
                dates += "First Review: " & Review1 & vbCrLf
            End If
            If Review2 <> "" Then
                dates += "Second Review: " & Review2 & vbCrLf
            End If
            If Review3 <> "" Then
                dates += "Third Review: " & Review3 & vbCrLf
            End If
            If Review4 <> "" Then
                dates += "Final Review: " & Review4 & vbCrLf
            End If
            Dim name As String = Nothing

            name = DropDownList1.SelectedItem.Text
            Send_Email_Schedule(name, calYear, dates, username, strEmail)

        Catch
            lblMessage.Text += Err.Description
        End Try

    End Sub
    Public Sub Send_Email_Schedule(ByVal name As String, ByVal calYear As Int32, ByVal dates As String, ByVal newusername As String, ByVal subEmail As String)
        Dim MailObj As New System.Net.Mail.SmtpClient
        Dim message As New System.Net.Mail.MailMessage()
        Dim fromAddress As New System.Net.Mail.MailAddress("SchedulePerformanceReview@empiredistrict.com")

        Try
            'Build message
            message.From = fromAddress
            'uncomment for production | adds supervisor's name
            message.To.Add(newusername & "@empiredistrict.com")
            'adds employees name
            message.To.Add(subEmail & "@empiredistrict.com")

            message.Subject = "Schedule Performance Review for " & name
            'MailObj.Host = "" -- obtained from web.config 03/14/2012            
            message.Body = "Performance Reviews scheduled for " & name & " on the following dates: " & vbCrLf & dates
            MailObj.Send(message)

        Catch Exp As Exception
            lblEmployeeInfo.Text &= "Schedule cannot be sent, check to see that Network ID is populated for " & name & " and try again. <br/>"
        Finally
            message.Dispose()
            'lblEmployeeInfo.Text = Nothing
        End Try
    End Sub
    Public Sub update_reviewed_Date(ByVal EMPLID As Int32, ByVal calYear As String, ByVal Review1 As String, ByVal Review2 As String, ByVal Review3 As String, ByVal Review4 As String)
        Try
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup As String = "SELECT [Review_Year] FROM [PerfReviewDates] WHERE ([PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year)"
                Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                myConnection.Open()
                Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                If myLookup.Read Then
                    Using myUpdateConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                        Const SQLUpdate As String = "Update [PerfReviewDates] SET [Review_Year] = @Review_Year,[Review_Date1] = @Review_Date1,[Review_Date2] = @Review_Date2,[Review_Date3] = @Review_Date3,[Final_Review_Date] = @Final_Review_Date WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
                        Dim myUpdateCommand As New SqlCommand(SQLUpdate, myUpdateConnection)
                        myUpdateCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                        myUpdateCommand.Parameters.AddWithValue("@Review_Year", calYear)
                        myUpdateCommand.Parameters.AddWithValue("@Review_Date1", Review1)
                        myUpdateCommand.Parameters.AddWithValue("@Review_Date2", Review2)
                        myUpdateCommand.Parameters.AddWithValue("@Review_Date3", Review3)
                        myUpdateCommand.Parameters.AddWithValue("@Final_Review_Date", Review4)
                        myUpdateConnection.Open()
                        myUpdateCommand.ExecuteNonQuery()
                        myUpdateConnection.Close()
                        lblMessage2.Text = "Reviews Updated"

                    End Using
                Else
                    Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                        Const SQLInsert As String = "Insert into [PerfReviewDates] ([PS_EMPLID],[Review_Year],[Review_Date1],[Review_Date2],[Review_Date3],[Final_Review_Date],[Review_Closed]) VALUES (@PS_EMPLID,@Review_Year,@Review_Date1,@Review_Date2,@Review_Date3,@Final_Review_Date,@Review_Closed)"
                        Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)
                        myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                        myInsertCommand.Parameters.AddWithValue("@Review_Year", calYear)
                        myInsertCommand.Parameters.AddWithValue("@Review_Date1", Review1)
                        myInsertCommand.Parameters.AddWithValue("@Review_Date2", Review2)
                        myInsertCommand.Parameters.AddWithValue("@Review_Date3", Review3)
                        myInsertCommand.Parameters.AddWithValue("@Final_Review_Date", Review4)
                        myInsertCommand.Parameters.AddWithValue("@Review_Closed", False)
                        myInsertConnection.Open()
                        myInsertCommand.ExecuteNonQuery()
                        myInsertConnection.Close()
                        lblMessage2.Text = "Review Date Added"
                    End Using
                End If

                myLookup.Close()
                myConnection.Close()

            End Using

        Catch
            lblMessage2.Text += Err.Description
        End Try

    End Sub
    Public Sub initialize_schedule()
        If lblReview1.Text = "Unscheduled" Then
            txtReview1.Text = Nothing
            CalendarExtender1.SelectedDate = Nothing
        Else
            CalendarExtender1.SelectedDate = lblReview1.Text

        End If
        If lblReview2.Text = "Unscheduled" Then
            txtReview2.Text = Nothing
            CalendarExtender2.SelectedDate = Nothing
        Else
            CalendarExtender2.SelectedDate = lblReview2.Text
        End If
        If lblReview3.Text = "Unscheduled" Then
            txtReview3.Text = Nothing
            CalendarExtender3.SelectedDate = Nothing
        Else
            CalendarExtender3.SelectedDate = lblReview3.Text
        End If
        If lblReview4.Text = "Unscheduled" Then
            txtReview4.Text = Nothing
            CalendarExtender4.SelectedDate = Nothing
        Else
            CalendarExtender4.SelectedDate = lblReview4.Text
        End If

    End Sub
    Public Sub initialize_review_date()
        If lblReview5.Text = "Pending" Then
            txtReview5.Text = Nothing
            CalendarExtender5.SelectedDate = Nothing
        Else
            CalendarExtender5.SelectedDate = lblReview5.Text
        End If
        If lblReview6.Text = "Pending" Then
            txtReview6.Text = Nothing
            CalendarExtender6.SelectedDate = Nothing
        Else
            CalendarExtender6.SelectedDate = lblReview6.Text
        End If
        If lblReview7.Text = "Pending" Then
            txtReview7.Text = Nothing
            CalendarExtender7.SelectedDate = Nothing
        Else
            CalendarExtender7.SelectedDate = lblReview7.Text
        End If
        If lblReview8.Text = "Pending" Then
            txtReview8.Text = Nothing
            CalendarExtender8.SelectedDate = Nothing
        Else
            CalendarExtender8.SelectedDate = lblReview8.Text
        End If

    End Sub
    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        If DropDownList1.SelectedIndex > 0 Then
            build_links()
            initialize_schedule()
            initialize_review_date()
            ckEmails.Items.Clear()
            load_checkbox()
            lblEmployeeInfo.Text = Nothing
            'disable all labels, subs, functions, and controls pertaining to Salary until HR says otherwise
            'lblBaseSalary.Visible = True
            'Dim decBaseSalary As Decimal
            'decBaseSalary = GetBaseSalary()
            'lblBaseSalary.Text = " $" & decBaseSalary.ToString("###,###.#0")
            'lblBaseSalary.Font.Bold = True
            'lblBaseSalary.ForeColor = Drawing.Color.DarkGreen
            Dim closed As Boolean
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            lblSchedYear.Text = ddlReviewYear.SelectedValue
            lblTrackYear.Text = ddlReviewYear.SelectedValue
            closed = review_is_closed(calYear)
            If closed Then
                lblClosed.Text = "Review Closed"
                btnOK.Enabled = False
                btnOKrv.Enabled = False
            Else
                lblClosed.Text = Nothing
            End If
        Else
            gvIncentive.Visible = False
            gvLearning.Visible = False
            gvCoreComp.Visible = False
            gvCompetence.Visible = False
            gvComments.Visible = False
            lnkSelectedSub.Text = Nothing
            lblToggle.Visible = False
            imgToggle.Visible = False
            ImgSchedule.Visible = False
            ImgReview.Visible = False
            ImgEmail.Visible = False
            ImgPrintPDF.Visible = False
            lnkSchedule.Text = Nothing
            lnkReview.Text = Nothing
            lnkEmail.Text = Nothing
            lnkPrintPDF.Text = Nothing
            lblBaseSalary.Visible = False
        End If
    End Sub
    Public Function GetBaseSalary() As String
        Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
            Dim strBaseSalary As String
            Dim SQLPSLookup As String = "SELECT ANNUAL_RT FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW WHERE(EMPLID = :EMPLID)"
            Dim myPSLookup As New OracleCommand(SQLPSLookup, myPSConnection)
            myPSLookup.Parameters.AddWithValue(":EMPLID", DropDownList1.SelectedValue)
            myPSConnection.Open()
            strBaseSalary = myPSLookup.ExecuteScalar()
            myPSConnection.Close()

            Return strBaseSalary
        End Using

    End Function
    Public Function GetTotalBaseSalary() As String
        Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
            Dim strPosNum As String
            Dim strTotalBaseSalary As String
            Dim SQLPSLookup As String = "SELECT POSITION_NBR FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW WHERE(EMPLID = :EMPLID)"
            Dim myPSLookup As New OracleCommand(SQLPSLookup, myPSConnection)

            myPSLookup.Parameters.AddWithValue(":EMPLID", managerEmplID)
            myPSConnection.Open()
            strPosNum = myPSLookup.ExecuteScalar()
            myPSConnection.Close()

            Dim SQLPSLookupRpts As String = "SELECT SUM(ANNUAL_RT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW WHERE(REPORTS_TO = :POSNUM AND UNION_CD in ('N01','GN1'))"
            Dim myPSLookupRpts As New OracleCommand(SQLPSLookupRpts, myPSConnection)

            myPSLookupRpts.Parameters.AddWithValue(":POSNUM", strPosNum)
            myPSConnection.Open()
            strTotalBaseSalary = myPSLookupRpts.ExecuteScalar()
            myPSConnection.Close()

            Return strTotalBaseSalary
        End Using

    End Function

    Protected Sub popup_rep_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_report.Load
        Try
            If ddlReviewYear.SelectedValue <> "" Then
                get_scheduled_dates()
            End If
            Dim closed As Boolean
            Dim calYear As Int32 = ddlReviewYear.SelectedValue

            closed = review_is_closed(calYear)

            If closed Or ps.canUpdate(ps.getPageName) Then
                imgCal1.Visible = False
                ImgCal2.Visible = False
                ImgCal3.Visible = False
                ImgCal4.Visible = False
                lnkSchedDel1.Visible = False
                lnkSchedDel2.Visible = False
                btnOK.Enabled = False
            Else
                imgCal1.Visible = True
                ImgCal2.Visible = True
                ImgCal3.Visible = True
                ImgCal4.Visible = True
                lnkSchedDel1.Visible = True
                lnkSchedDel2.Visible = True
                btnOK.Enabled = True
            End If
        Catch ex As Exception
            'lblMessage.Text = ""
        End Try
    End Sub

    Protected Sub popup_1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_1.Load
        Try
            If ddlReviewYear.SelectedValue <> "" Then
                get_scheduled_dates()
            End If
            Dim closed As Boolean
            Dim calYear As Int32 = ddlReviewYear.SelectedValue

            closed = review_is_closed(calYear)

            If closed Or ps.canUpdate(ps.getPageName) Then
                imgCal1.Visible = False
                ImgCal2.Visible = False
                ImgCal3.Visible = False
                ImgCal4.Visible = False
                lnkSchedDel1.Visible = False
                lnkSchedDel2.Visible = False
                btnOK.Enabled = False
            Else
                imgCal1.Visible = True
                ImgCal2.Visible = True
                ImgCal3.Visible = True
                ImgCal4.Visible = True
                lnkSchedDel1.Visible = True
                lnkSchedDel2.Visible = True
                btnOK.Enabled = True
            End If
        Catch ex As Exception
            'lblMessage.Text = ""
        End Try
    End Sub
    Protected Sub popup_2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_2.Load
        Try
            If ddlReviewYear.SelectedValue <> "" Then
                get_Review_dates()
                set_close_button()
            End If
            Dim closed As Boolean
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            closed = review_is_closed(calYear)
            If closed Or ps.canUpdate(ps.getPageName) Then
                ImgCal5.Visible = False
                ImgCal6.Visible = False
                ImgCal7.Visible = False
                ImgCal8.Visible = False
                lnkDelReview1.Visible = False
                lnkDelReview2.Visible = False
                lnkDelReview3.Visible = False
                lnkDelReview4.Visible = False
                btnOKrv.Enabled = False
            Else
                ImgCal5.Visible = True
                ImgCal6.Visible = True
                ImgCal7.Visible = True
                ImgCal8.Visible = True
                lnkDelReview1.Visible = True
                lnkDelReview2.Visible = True
                lnkDelReview3.Visible = True
                lnkDelReview4.Visible = True
                btnOKrv.Enabled = True
            End If
        Catch ex As Exception
            'lblMessage2.Text = ""
        End Try
    End Sub
    Public Sub set_close_button()
        Dim EMPLID As Int32
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim closed As Boolean
        If DropDownList1.SelectedValue <> "" Then
            EMPLID = DropDownList1.SelectedValue
        End If

        Dim ScheduleEmpty As String = Nothing
        Dim ReviewEmpty As String = Nothing
        Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup2 As String = "SELECT A.Final_Review_Date FROM PerfReviewDates A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = @PS_EMPLID AND A.Review_Year = @Review_Year)"
            Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
            myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
            myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
            myConnection2.Open()
            Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

            If myLookup2.Read Then
                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT A.Scheduled_Final_Review FROM PerfReviewSchedule A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = @PS_EMPLID AND A.Review_Year = @Review_Year)"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                    myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

                    If myLookup.Read Then
                        If myLookup2("Final_Review_Date") <> "1/1/1900" And myLookup("Scheduled_Final_Review") <> "1/1/1900" Then
                            btnClose.Enabled = True
                            btnClose.Text = "Annual Review Completed"
                        Else
                            btnClose.Enabled = False
                            btnClose.Text = "Annual Review Incomplete"
                            'End If
                        End If
                    Else
                        btnClose.Enabled = False
                        btnClose.Text = "Annual Review Incomplete"
                    End If
                    myLookup.Close()
                    myConnection.Close()
                End Using
            Else
                btnClose.Enabled = False
                btnClose.Text = "Annual Review Incomplete"
            End If
            myLookup2.Close()
            myConnection2.Close()
        End Using

        closed = review_is_closed(calYear)
        If closed Then
            btnClose.Enabled = False
            btnClose.Text = "Review Closed"
            'disable controls 
        Else
            If ps.canUpdate(ps.getPageName) Then
                btnClose.Enabled = False
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        lblMessage.Text = Nothing
    End Sub
    Protected Sub btnCancelrv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelrv.Click
        lblMessage2.Text = Nothing
    End Sub
    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click        
        If RequiredFieldValidator1.IsValid And RequiredFieldValidator2.IsValid Then
            Dim EMPLID As Int32 = DropDownList1.SelectedValue
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            Dim Review1, Review2, Review3, Review4 As String
            If txtReview1.Text = Nothing Then
                Review1 = DBNull.Value.ToString
            Else
                Review1 = txtReview1.Text

            End If
            If txtReview2.Text = Nothing Then
                Review2 = DBNull.Value.ToString
            Else
                Review2 = txtReview2.Text
            End If
            If txtReview3.Text = Nothing Then
                Review3 = DBNull.Value.ToString
            Else
                Review3 = txtReview3.Text
            End If
            If txtReview4.Text = Nothing Then
                Review4 = DBNull.Value.ToString
            Else
                Review4 = txtReview4.Text
            End If
            update_schedule(EMPLID, calYear, Review1, Review2, Review3, Review4)
            get_scheduled_dates()
            initialize_schedule()
            lblMessage.Text = Nothing
            If Not ViewState("select") Is Nothing Then
                SqlSchedules.SelectCommand = ViewState("select")
                SqlSchedules.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                gvSchedules.DataSource = SqlSchedules
                gvSchedules.DataBind()
                updPerf.Update()
            End If

        End If
    End Sub
    Protected Sub btnOKrv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOKrv.Click

        Dim EMPLID As Int32 = DropDownList1.SelectedValue
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim Review1, Review2, Review3, Review4 As String
        If txtReview5.Text = Nothing Then
            Review1 = DBNull.Value.ToString
        Else
            Review1 = txtReview5.Text

        End If
        If txtReview6.Text = Nothing Then
            Review2 = DBNull.Value.ToString
        Else
            Review2 = txtReview6.Text
        End If
        If txtReview7.Text = Nothing Then
            Review3 = DBNull.Value.ToString
        Else
            Review3 = txtReview7.Text
        End If
        If txtReview8.Text = Nothing Then
            Review4 = DBNull.Value.ToString
        Else
            Review4 = txtReview8.Text
        End If
        ModalPopupExtender2.Hide()
        update_reviewed_Date(EMPLID, calYear, Review1, Review2, Review3, Review4)
        get_Review_dates()
        initialize_review_date()
        lblMessage2.Text = Nothing
        set_close_button()
        If Not ViewState("select") Is Nothing Then
            SqlSchedules.SelectCommand = ViewState("select")
            SqlSchedules.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
            gvSchedules.DataSource = SqlSchedules
            gvSchedules.DataBind()
            updPerf.Update()
        End If

    End Sub
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ModalPopupExtender2.Show()
            Dim name As String = Nothing
            name = DropDownList1.SelectedItem.Text
            Dim EMPLID As Int32 = DropDownList1.SelectedValue
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            Dim strMgrName As String = ""
            Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup As String = "SELECT [Employee_First_Name],[Employee_Last_Name] FROM [PerfEmployee] WHERE ([Manager_SignOn] = @Manager_SignOn)"
                Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                myLookupCommand.Parameters.AddWithValue("@Manager_SignOn", username)
                myConnection.Open()
                Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                'Check to see if signed on employee is a manager using position number*****************************
                If myLookup.Read Then
                    strMgrName = myLookup("Employee_First_Name") & " " & myLookup("Employee_Last_Name")
                Else
                    gvSchedules.Visible = False
                    pnlManagerView.Visible = False
                End If
                myLookup.Close()
                myConnection.Close()
            End Using
            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup2 As String = "UPDATE [PerfReviewDates] SET [REVIEW_CLOSED] = @Review_Closed, [Closed_Date] = @Closed_Date,[Closed_By] = @Closed_By WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
                myLookupCommand2.Parameters.AddWithValue("@Review_Closed", "True")
                myLookupCommand2.Parameters.AddWithValue("@Closed_Date", Now)
                myLookupCommand2.Parameters.AddWithValue("@Closed_By", strMgrName)
                myConnection2.Open()
                myLookupCommand2.ExecuteNonQuery()

            End Using
            lblMessage2.Text = "Performance Review for " & calYear & " has been closed for " & name

            lblMessage2.ForeColor = Drawing.Color.AliceBlue
            lblMessage2.Font.Bold = False
            Dim closed As Boolean

            closed = review_is_closed(calYear)
            If closed Or ps.canUpdate(ps.getPageName) Then
                ImgCal5.Visible = False
                ImgCal6.Visible = False
                ImgCal7.Visible = False
                ImgCal8.Visible = False
            Else
                ImgCal5.Visible = True
                ImgCal6.Visible = True
                ImgCal7.Visible = True
                ImgCal8.Visible = True
            End If
            If Not ViewState("select") Is Nothing Then
                SqlSchedules.SelectCommand = ViewState("select")
                SqlSchedules.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                gvSchedules.DataSource = SqlSchedules
                gvSchedules.DataBind()
                updPerf.Update()
            End If
            ModalPopupExtender2.Hide()
            gvLearning.DataBind()
            gvIncentive.DataBind()
            gvCoreComp.DataBind()
            gvCompetence.DataBind()
            gvComments.DataBind()
            btnClose.Enabled = False
            btnOKrv.Enabled = False
        Catch
            lblMessage2.Text += Err.Description
        End Try
    End Sub
    Public Function review_is_closed(ByVal calYear As Int32) As Boolean
        If DropDownList1.SelectedIndex > 0 Then
            Dim EMPLID As Int32 = DropDownList1.SelectedValue
            Dim closed As Boolean
            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup2 As String = "Select [REVIEW_CLOSED] from [PerfReviewDates] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND [Review_Closed] = @Review_Closed"
                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
                myLookupCommand2.Parameters.AddWithValue("@Review_Closed", True)

                myConnection2.Open()
                Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

                If myLookup2.Read Then
                    If myLookup2("Review_closed") = True Then
                        closed = True
                    Else
                        closed = False
                    End If
                Else
                    closed = False
                End If
                myLookup2.Close()
                myConnection2.Close()
            End Using
            Return closed
        End If
    End Function
    Public Function flag_if_closed(ByVal calYear As Int32, ByVal psemplid As Int32) As Boolean

        Dim closed As Boolean
        Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup2 As String = "Select [REVIEW_CLOSED] from [PerfReviewDates] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND [Review_Closed] = @Review_Closed"
            Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
            myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", psemplid)
            myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
            myLookupCommand2.Parameters.AddWithValue("@Review_Closed", True)

            myConnection2.Open()
            Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

            If myLookup2.Read Then
                If myLookup2("Review_closed") = True Then
                    closed = True
                Else
                    closed = False
                End If
            Else
                closed = False
            End If
            myLookup2.Close()
            myConnection2.Close()
        End Using
        Return closed
    End Function
    Protected Sub gvLearning_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLearning.RowCommand
        Select Case e.CommandName
            Case "Select"
                dvLearning.ChangeMode(DetailsViewMode.Edit)
            Case "New"
                dvLearning.ChangeMode(DetailsViewMode.Insert)
            Case Else
        End Select
    End Sub
    Protected Sub gvLearning_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLearning.RowDataBound
        Dim closed As Boolean
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        closed = review_is_closed(calYear)
        If closed Or ps.canUpdate(ps.getPageName) Then
            e.Row.Cells(0).Visible = False
            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Visible = False
            End If
        Else
            e.Row.Cells(0).Visible = True
        End If
    End Sub
    Protected Sub gvComments_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvComments.RowCommand
        Select Case e.CommandName
            Case "Select"
                dvComments.ChangeMode(DetailsViewMode.Edit)
            Case "New"
                dvComments.ChangeMode(DetailsViewMode.Insert)
            Case Else
        End Select
    End Sub
    Protected Sub gvComments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvComments.RowDataBound
        Dim closed As Boolean
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        closed = review_is_closed(calYear)
        If closed Or ps.canUpdate(ps.getPageName) Then
            e.Row.Cells(0).Visible = False
            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Visible = False
            End If
        Else
            e.Row.Cells(0).Visible = True
        End If
    End Sub
    Protected Sub gvIncentive_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvIncentive.RowCommand
        Select Case e.CommandName
            Case "Select"
                dvIncentive.ChangeMode(DetailsViewMode.Edit)
            Case "New"
                dvIncentive.ChangeMode(DetailsViewMode.Insert)
            Case Else
        End Select
    End Sub
    Protected Sub gvIncentive_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvIncentive.RowDataBound
        Dim closed As Boolean
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        closed = review_is_closed(calYear)
        If closed Or ps.canUpdate(ps.getPageName) Then
            e.Row.Cells(0).Visible = False
            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Visible = False
            End If
        Else
            e.Row.Cells(0).Visible = True
        End If
    End Sub
    Protected Sub gvCoreComp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCoreComp.RowCommand
        Select Case e.CommandName
            Case "Select"
                dvCoreComp.ChangeMode(DetailsViewMode.Edit)
            Case "New"
                dvCoreComp.ChangeMode(DetailsViewMode.Insert)
            Case Else
        End Select
    End Sub
    Protected Sub gvCoreComp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCoreComp.RowDataBound
        Dim closed As Boolean
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        closed = review_is_closed(calYear)
        If closed Or ps.canUpdate(ps.getPageName) Then
            e.Row.Cells(0).Visible = False
            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Visible = False
            End If
        Else
            e.Row.Cells(0).Visible = True
        End If
    End Sub
    Protected Sub gvCompetence_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCompetence.RowCommand
        Select Case e.CommandName
            Case "Select"
                dvCompetence.ChangeMode(DetailsViewMode.Edit)
            Case "New"
                dvCompetence.ChangeMode(DetailsViewMode.Insert)
            Case Else
        End Select
    End Sub
    Protected Sub gvCompetence_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCompetence.RowDataBound
        Dim closed As Boolean
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        closed = review_is_closed(calYear)
        If closed Or ps.canUpdate(ps.getPageName) Then
            e.Row.Cells(0).Visible = False
            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Visible = False
            End If
        Else
            e.Row.Cells(0).Visible = True
        End If
    End Sub
    Private Sub delete_scheduled_date(ByVal review_number As String)
        Try

            Dim name As String = Nothing
            name = DropDownList1.SelectedItem.Text
            Dim EMPLID As Int32 = DropDownList1.SelectedValue
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

                Dim SQLLookup2 As String = ""
                SQLLookup2 = "UPDATE [PerfReviewSchedule] SET [Scheduled_Review" & review_number.ToString & "] = @Scheduled_Review WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
                myLookupCommand2.Parameters.AddWithValue("@Scheduled_Review", "1/1/1900")
                myConnection2.Open()
                myLookupCommand2.ExecuteNonQuery()
            End Using
            lblMessage.Text = "Performance Review Canceled"
            lblMessage.ForeColor = Drawing.Color.AliceBlue
            lblMessage.Font.Bold = False
        Catch
            lblMessage.Text += Err.Description
        End Try
    End Sub
    Private Sub delete_Review_date(ByVal review_Number As String)
        Try

            Dim name As String = Nothing
            name = DropDownList1.SelectedItem.Text
            Dim EMPLID As Int32 = DropDownList1.SelectedValue
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)

                Dim SQLLookup2 As String = ""
                If review_Number = "4" Then
                    SQLLookup2 = "UPDATE [PerfReviewDates] SET [Final_Review_Date] = @Review_Date WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
                Else
                    SQLLookup2 = "UPDATE [PerfReviewDates] SET [Review_Date" & review_Number.ToString & "] = @Review_Date WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
                End If

                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", EMPLID)
                myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
                myLookupCommand2.Parameters.AddWithValue("@Review_Date", "1/1/1900")
                myConnection2.Open()
                myLookupCommand2.ExecuteNonQuery()
            End Using
            lblMessage2.Text = "Review Canceled"
            lblMessage2.ForeColor = Drawing.Color.AliceBlue
            lblMessage2.Font.Bold = False
        Catch
            lblMessage2.Text += Err.Description
        End Try
    End Sub
    Protected Sub lnkSchedDel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSchedDel1.Click
        delete_scheduled_date("2")
        get_scheduled_dates()
        initialize_schedule()
        ModalPopupExtender1.Show()
    End Sub
    Protected Sub lnkSchedDel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSchedDel2.Click
        delete_scheduled_date("3")
        get_scheduled_dates()
        initialize_schedule()
        ModalPopupExtender1.Show()
    End Sub
    Protected Sub lnkDelReview1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDelReview1.Click
        delete_Review_date("1")
        get_Review_dates()
        initialize_review_date()
        ModalPopupExtender2.Show()
    End Sub
    Protected Sub lnkDelReview2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDelReview2.Click
        delete_Review_date("2")
        get_Review_dates()
        initialize_review_date()
        ModalPopupExtender2.Show()
    End Sub
    Protected Sub lnkDelReview3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDelReview3.Click
        delete_Review_date("3")
        get_Review_dates()
        initialize_review_date()
        ModalPopupExtender2.Show()
    End Sub
    Protected Sub lnkDelReview4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDelReview4.Click
        delete_Review_date("4")
        get_Review_dates()
        initialize_review_date()
        set_close_button()
        ModalPopupExtender2.Show()
    End Sub
    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        ModalPopupExtender3.Show()
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim closed As Boolean

        closed = review_is_closed(calYear)

        Dim url As String = "~/Maintenance/PortalApps/PrintToPDF.aspx?a=" & Server.UrlEncode(tdes.Encrypt(DropDownList1.SelectedValue)) & "&b=" & calYear & "&c=" & closed
        Dim Newurl As String = ResolveUrl(url)
        Dim MailObj As New System.Net.Mail.SmtpClient
        Dim message As New System.Net.Mail.MailMessage()
        Dim fromAddress As New System.Net.Mail.MailAddress("PerformanceReview@empiredistrict.com")
        Try
            'Build message
            message.From = fromAddress
            For Each item As ListItem In ckEmails.Items
                If item.Selected = True Then
                    message.To.Add(item.Text)
                End If
            Next
            message.Subject = "Performance Review"
            'MailObj.Host = "" -- obtained from web.config 03/14/2012
            message.Body = " Your " & calYear & " Performance Review is available here: http://" & Server.MachineName & Newurl
            'send email
            If message.To.Count > 0 Then
                MailObj.Send(message)
                lblEmailMessage.Text = "Email(s) sent to: " & message.To.ToString
                ckEmails.Items.Clear()
                load_checkbox()
                lblEmailMessage.Text = ""
                ModalPopupExtender3.Hide()
            Else
                lblEmailMessage.Text = "No emails selected"
            End If
            lblEmailMessage.ForeColor = Drawing.Color.Black
            lblEmailMessage.Font.Bold = False
        Catch Exp As Exception
            'lblEmployeeInfo.Text &= "error sending email"
        Finally
            message.Dispose()
        End Try

    End Sub
    Protected Sub btnCancelSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSend.Click
        ckEmails.Items.Clear()
        load_checkbox()
        lblEmailMessage.Text = ""

    End Sub
    Public Sub load_checkbox()
        Try
            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                Const SQLLookup2 As String = "Select [Employee_SignOn] from [PerfEmployee] WHERE [PS_EMPLID] = @PS_EMPLID"
                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", DropDownList1.SelectedValue)
                myConnection2.Open()
                Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

                If myLookup2.Read Then
                    If Not myLookup2.IsDBNull(0) Then
                        ckEmails.Items.Add(New ListItem(Trim(myLookup2("Employee_SignOn").ToString) & "@empiredistrict.com", 1))
                    Else
                        ckEmails.Items.Add(New ListItem("Please add email for this employee", 0))
                    End If
                End If
                myLookup2.Close()
                myConnection2.Close()
            End Using
            ckEmails.Items.Add(New ListItem(Trim(username) & "@empiredistrict.com", 2))
            Dim strReportsTo As String = ""
            Dim strMgrReportsTo As String = ""
            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)

                Const SQLPSLookup As String = "SELECT A.EMPLID, A.REPORTS_TO " & _
                     " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A " & _
                     " WHERE ( A.EFFDT = " & _
                     " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                     " AND A_ED.EFFDT <= SYSDATE) " & _
                     " AND A.EFFSEQ = " & _
                     " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                     " AND A.EFFDT = A_ES.EFFDT)" & _
                     " AND A.EMPLID = :PS_EMPLID)"

                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":PS_EMPLID", managerEmplID)
                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader

                'uncomment for test with dropdown**********
                If Not IsPostBack Then
                    While myPSLookup.Read

                        strReportsTo = myPSLookup("REPORTS_TO")
                    End While
                End If
                '******************************************
                myPSLookup.Close()
                myPSConnection.Close()
            End Using
            Using myPSConnectionRptTo As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
                Const SQLPSLookupRptTo As String = "SELECT A.EMPLID, A.NAME, A.POSITION_NBR, B.JOBCODE, B.DEPTID, C.DESCR, C.SETID, A.REPORTS_TO, SUBSTR(A.POSITION_NBR,1,4) as JOB" & _
                    " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A, SYSADM.EDE_PORTAL_PS_JOB_VW B, sysadm.PS_JOBCODE_TBL C" & _
                    " WHERE ( A.EFFDT =" & _
                    " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED" & _
                                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                    " AND A.EMPL_RCD = A_ED.EMPL_RCD" & _
                    " AND A_ED.EFFDT <= SYSDATE)" & _
                    " AND A.EFFSEQ =" & _
                    " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES" & _
                                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                    " AND A.EMPL_RCD = A_ES.EMPL_RCD" & _
                    " AND A.EFFDT = A_ES.EFFDT)" & _
                    " AND A.EMPLID = B.EMPLID" & _
                    " AND A.EMPL_RCD = B.EMPL_RCD" & _
                    " AND B.EFFDT =" & _
                    " (SELECT MAX(B_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_JOB_VW B_ED" & _
                                " WHERE(B.EMPLID = B_ED.EMPLID)" & _
                    " AND B.EMPL_RCD = B_ED.EMPL_RCD" & _
                    " AND B_ED.EFFDT <= A.EFFDT)" & _
                    " AND B.EFFSEQ =" & _
                    " (SELECT MAX(B_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_JOB_VW B_ES" & _
                                " WHERE(B.EMPLID = B_ES.EMPLID)" & _
                    " AND B.EMPL_RCD = B_ES.EMPL_RCD" & _
                    " AND B.EFFDT = B_ES.EFFDT)" & _
                    " AND C.JOBCODE = B.JOBCODE" & _
                    " AND C.EFFDT =" & _
                    " (SELECT MAX(C_ED.EFFDT) FROM sysadm.PS_JOBCODE_TBL C_ED" & _
                                " WHERE(C.SETID = C_ED.SETID)" & _
                    " AND C.JOBCODE = C_ED.JOBCODE" & _
                    " AND C_ED.EFFDT <= B.EFFDT) )" & _
                    " AND A.POSITION_NBR = :REPORTS_TO"
                Dim myPSLookupCommandRptTo As New OracleCommand(SQLPSLookupRptTo, myPSConnectionRptTo)
                myPSLookupCommandRptTo.Parameters.AddWithValue(":REPORTS_TO", strReportsTo)
                myPSConnectionRptTo.Open()
                Dim myPSLookupRptTo As OracleDataReader = myPSLookupCommandRptTo.ExecuteReader

                'uncomment for test with dropdown**********
                If Not IsPostBack Then
                    While myPSLookupRptTo.Read

                        Using myConnectionRptMgr As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Dim SQLLookupRptMgr As String = "Select [Manager_SignOn] from [PerfEmployee] WHERE [PS_EMPLID] = " & myPSLookupRptTo("EMPLID")
                            Dim myLookupCommandRptMgr As New SqlCommand(SQLLookupRptMgr, myConnectionRptMgr)
                            myConnectionRptMgr.Open()
                            Dim myLookupRptMgr As SqlDataReader = myLookupCommandRptMgr.ExecuteReader

                            If myLookupRptMgr.Read Then

                                If Not myLookupRptMgr.IsDBNull(0) Then
                                    lblManagerInfo.Text = myLookupRptMgr("Manager_SignOn")
                                    strMgrReportsTo = myLookupRptMgr("Manager_SignOn").ToString
                                Else
                                    strMgrReportsTo = ""
                                End If
                            End If
                            myLookupRptMgr.Close()
                            myConnectionRptMgr.Close()
                        End Using

                    End While
                End If
                '******************************************
                myPSLookupRptTo.Close()
                myPSConnectionRptTo.Close()
            End Using
            If lblManagerInfo.Text <> "" Then
                ckEmails.Items.Add(New ListItem(lblManagerInfo.Text & "@empiredistrict.com", 1))
            Else
                ckEmails.Items.Add(New ListItem("Please contact HR to add Supervisor's email", 0))
            End If
            ckEmails.Items.Add(New ListItem("PerformanceReviews@empiredistrict.com", 4))

        Catch
            lblManagerInfo.Text = Err.Description
        End Try
    End Sub

    Protected Sub lnkPrintPDF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrintPDF.Load

        Try
            lnkPrintPDF.Target = "_blank"
            Dim closed As Boolean
            Dim calYear As Int32 = ddlReviewYear.SelectedValue
            closed = review_is_closed(calYear)
            Dim url As String = "~/Maintenance/PortalApps/PrintToPDF.aspx?a=" & Server.UrlEncode(tdes.Encrypt(DropDownList1.SelectedValue)) & "&b=" & calYear & "&c=" & closed
            lnkPrintPDF.NavigateUrl = "http://" & Server.MachineName & ResolveUrl(url)

        Catch
        End Try
    End Sub
    Protected Sub lnkCopyLearningReview_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim txtLastReviewYear As String = calYear - 1
        Dim txtCurrentReviewYear As String = calYear
        Dim txtEmplID As String = DropDownList1.SelectedValue

        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT [Learning_Goal],[LG_ID] FROM [PerfLearningGoals] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year order by LG_ID"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
            myLookupCommand.Parameters.AddWithValue("@Review_Year", txtLastReviewYear)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

            While myLookup.Read
                Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLInsert As String = "INSERT INTO [PerfLearningGoals] ([PS_EMPLID], [Learning_Goal],[Review_Year]) VALUES (@PS_EMPLID, @Learning_Goal, @Review_Year)"
                    Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)

                    myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
                    myInsertCommand.Parameters.AddWithValue("@Review_Year", txtCurrentReviewYear)
                    myInsertCommand.Parameters.AddWithValue("@Learning_Goal", myLookup("Learning_Goal"))
                    myInsertConnection.Open()
                    myInsertCommand.ExecuteNonQuery()
                    myInsertConnection.Close()

                End Using
            End While
            If Not myLookup.HasRows Then
                lblLearnMsg.Text = "Previous year is empty or unable to be copied"
                lblLearnMsg.Visible = True
            End If
            myLookup.Close()
            myConnection.Close()

        End Using
        gvLearning.DataBind()

    End Sub
    Protected Sub lnkCopyIncentiveReview_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim txtLastReviewYear As String = calYear - 1
        Dim txtCurrentReviewYear As String = calYear
        Dim txtEmplID As String = DropDownList1.SelectedValue

        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT [Incentive_Goal], IG_ID FROM [PerfIncentiveGoals] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year order by IG_ID"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
            myLookupCommand.Parameters.AddWithValue("@Review_Year", txtLastReviewYear)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

            While myLookup.Read
                Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLInsert As String = "INSERT INTO [PerfIncentiveGoals] ([PS_EMPLID],[Incentive_Goal],[Review_Year]) VALUES (@PS_EMPLID,@Incentive_Goal,@Review_Year)"
                    Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)

                    myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
                    myInsertCommand.Parameters.AddWithValue("@Review_Year", txtCurrentReviewYear)
                    myInsertCommand.Parameters.AddWithValue("@Incentive_Goal", myLookup("Incentive_Goal"))
                    myInsertConnection.Open()
                    myInsertCommand.ExecuteNonQuery()
                    myInsertConnection.Close()
                End Using
            End While
            If Not myLookup.HasRows Then
                lblIncentiveMsg.Text = "Previous year is empty or unable to be copied"
                lblIncentiveMsg.Visible = True
            End If
            myLookup.Close()
            myConnection.Close()

        End Using
        gvIncentive.DataBind()
    End Sub
    Protected Sub lnkCopyCoreComp_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim txtLastReviewYear As String = calYear - 1
        Dim txtCurrentReviewYear As String = calYear
        Dim txtEmplID As String = DropDownList1.SelectedValue

        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT Competence_Goal,Competence_Assessment,Competence_Suggestion,Competence_Weight, CG_ID FROM [PerfCompetencies] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND Competence_Goal LIKE '(CORE)%' order by CG_ID"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
            myLookupCommand.Parameters.AddWithValue("@Review_Year", txtLastReviewYear)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

            While myLookup.Read
                Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLInsert As String = "INSERT INTO [PerfCompetencies] ([PS_EMPLID],[Competence_Goal],[Competence_Assessment],[Competence_Suggestion],[Competence_Weight],[Review_Year]) VALUES (@PS_EMPLID,@Competence_Goal,@Competence_Assessment,@Competence_Suggestion,@Competence_Weight,@Review_Year)"
                    Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)

                    myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
                    myInsertCommand.Parameters.AddWithValue("@Review_Year", txtCurrentReviewYear)
                    myInsertCommand.Parameters.AddWithValue("@Competence_Goal", myLookup("Competence_Goal"))
                    myInsertCommand.Parameters.AddWithValue("@Competence_Assessment", myLookup("Competence_Assessment"))
                    myInsertCommand.Parameters.AddWithValue("@Competence_Suggestion", myLookup("Competence_Suggestion"))
                    myInsertCommand.Parameters.AddWithValue("@Competence_Weight", myLookup("Competence_Weight"))
                    myInsertConnection.Open()
                    myInsertCommand.ExecuteNonQuery()
                    myInsertConnection.Close()
                End Using
            End While
            If Not myLookup.HasRows Then
                lblIncentiveMsg.Text = "Previous year is empty or unable to be copied"
                lblIncentiveMsg.Visible = True
            End If
            myLookup.Close()
            myConnection.Close()
        End Using
        gvCoreComp.DataBind()
    End Sub
    Protected Sub lnkCopyCompetencies_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        Dim txtLastReviewYear As String = calYear - 1
        Dim txtCurrentReviewYear As String = calYear
        Dim txtEmplID As String = DropDownList1.SelectedValue

        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT Competence_Goal,Competence_Assessment,Competence_Suggestion,Competence_Weight, CG_ID FROM [PerfCompetencies] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND [Competence_Goal] NOT LIKE '(CORE)%' order by CG_ID"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
            myLookupCommand.Parameters.AddWithValue("@Review_Year", txtLastReviewYear)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

            While myLookup.Read
                Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLInsert As String = "INSERT INTO [PerfCompetencies] ([PS_EMPLID],[Competence_Goal],[Competence_Assessment],[Competence_Suggestion],[Competence_Weight],[Review_Year]) VALUES (@PS_EMPLID,@Competence_Goal,@Competence_Assessment,@Competence_Suggestion,@Competence_Weight,@Review_Year)"
                    Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)

                    myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", txtEmplID)
                    myInsertCommand.Parameters.AddWithValue("@Review_Year", txtCurrentReviewYear)
                    myInsertCommand.Parameters.AddWithValue("@Competence_Goal", myLookup("Competence_Goal"))
                    myInsertCommand.Parameters.AddWithValue("@Competence_Assessment", myLookup("Competence_Assessment"))
                    myInsertCommand.Parameters.AddWithValue("@Competence_Suggestion", myLookup("Competence_Suggestion"))
                    myInsertCommand.Parameters.AddWithValue("@Competence_Weight", myLookup("Competence_Weight"))
                    myInsertConnection.Open()
                    myInsertCommand.ExecuteNonQuery()
                    myInsertConnection.Close()
                End Using
            End While
            If Not myLookup.HasRows Then
                lblIncentiveMsg.Text = "Previous year is empty or unable to be copied"
                lblIncentiveMsg.Visible = True
            End If
            myLookup.Close()
            myConnection.Close()
        End Using
        gvCompetence.DataBind()
    End Sub
    Protected Sub dvComments_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvComments.ItemInserting
        Dim txtcomment As TextBox = (CType(dvComments.FindControl("txtcomment"), TextBox))

        If e.CommandArgument = "Insert" Then
            If txtcomment.Text = "" Then
                e.Cancel = True
            End If
        End If
    End Sub
    Protected Sub dvComments_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvComments.ItemUpdating
        Dim txtcomment As TextBox = (CType(dvComments.FindControl("txtcomment"), TextBox))

        If e.CommandArgument = "Update" Then
            If txtcomment.Text = "" Then
                e.Cancel = True
            End If
        End If
    End Sub

    Protected Sub lnkRevHist_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRevHist.Load
        lnkRevHist.Target = "_blank"
        Dim closed As Boolean
        Dim calYear As Int32 = ddlReviewYear.SelectedValue
        closed = review_is_closed(calYear)
        Dim url As String = "~/Maintenance/PortalApps/PrintToPDF.aspx?a=" & Server.UrlEncode(tdes.Encrypt(employeeEmplID)) & "&b=" & calYear & "&c=" & closed
        lnkRevHist.NavigateUrl = "http://" & Server.MachineName & ResolveUrl(url)
        lnkRevHist.Text = username & " View/Print Review"
    End Sub

    Protected Sub run_reports(ByVal sender As LinkButton, ByVal e As System.EventArgs) Handles lb1.Click, lb2.Click, lb3.Click, lb4.Click, lb5.Click, lb6.Click, lb7.Click
        Dim report As String = ""
        Dim m As String = Today.Month.ToString, d As String = Today.Day.ToString, y As String = Today.Year.ToString, year As String = ddlReviewYear.SelectedValue

        Select Case sender.ID
            Case "lb1"
                report = "ng"
            Case "lb2"
                report = "womry"
            Case "lb3"
                report = "woar"
            Case "lb4"
                report = "pgr"
            Case "lb5"
                report = "igr"
            Case "lb6"
                report = "ccr"
            Case "lb7"
                report = "acr"
        End Select

        Response.Redirect("~/Maintenance/PortalApps/PerformanceReports.aspx?yr=" + year + "&r=" + report + "&m=" + m + "&d=" + d + "&y=" + y)

    End Sub
End Class