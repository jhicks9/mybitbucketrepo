﻿Imports System.Data.SqlClient
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Partial Class Maintenance_PortalApps_PrintToPDF
    Inherits System.Web.UI.Page
    Dim username As String
    Dim PS_EMPLID As Int32
    Dim Review_Year As Int32
    Dim Review_Closed As Boolean
    Public printurl As String = ""
    Dim tparam As String = ""
    Dim ReviewUrl As String
    Dim tdes As New tripleDES
    Dim font1 As Font = FontFactory.GetFont("Times", 14, Font.BOLD, BaseColor.BLACK)
    Dim font2 As Font = FontFactory.GetFont("Times", 12, Font.NORMAL, BaseColor.BLACK)
    Dim font3 As Font = FontFactory.GetFont("Times", 12, Font.BOLD, BaseColor.BLACK)
    Dim font4 As Font = FontFactory.GetFont("Times", 26, Font.NORMAL, BaseColor.BLACK)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'check for parameters
            If Request.QueryString.Count > 0 Then
                PS_EMPLID = tdes.Decrypt(Request.QueryString("a"))
                'PS_EMPLID = Request.QueryString("a")
                Review_Year = Request.QueryString("b")
                Review_Closed = Request.QueryString("c")
                'tparam = "~/print.aspx?t=" & Review_Year
                'printurl = ResolveUrl(tparam)
                Dim m As MemoryStream = New MemoryStream()
                Dim document As New Document(PageSize.LETTER, 10, 10, 10, 10)

                'step 2: we set the ContentType and create an instance of the Writer
                Response.ContentType = "application/pdf"
                PdfWriter.GetInstance(document, m)


                'Dim writer As PdfWriter = Nothing

                'writer = PdfWriter.GetInstance(document, New IO.FileStream("c:\temp\outputtest.pdf", IO.FileMode.Create))
                document.Open()

                Dim imageFilePath As String = Server.MapPath("~/content/images/logo_bw.jpg")
                Dim iLogo As Image = Image.GetInstance(imageFilePath)

                'Dim iLogo As Image = Image.GetInstance("Images/logo_bw.jpg")
                Dim HeadingTable As New PdfPTable(2) 'number of column(s)
                HeadingTable.WidthPercentage = 100
                Dim intArray3() As Integer = {5, 25}
                HeadingTable.SetWidths(intArray3)

                Dim Logo As PdfPCell = Nothing
                Dim Title As PdfPCell = Nothing

                Logo = New PdfPCell(New Phrase(New Chunk(iLogo, 0, 0)))
                Logo.HorizontalAlignment = Element.ALIGN_LEFT
                Logo.VerticalAlignment = Element.ALIGN_BOTTOM
                Logo.BorderWidth = 0
                Logo.PaddingBottom = 2
                Logo.PaddingTop = 2
                HeadingTable.AddCell(Logo)
                HeadingTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                'Dim pHeaderYear As New Paragraph(Review_Year.ToString, font4)
                'pHeaderYear.Alignment = Element.ALIGN_CENTER
                Title = New PdfPCell(New Phrase(New Chunk(Review_Year.ToString, font4)))
                Title.HorizontalAlignment = Element.ALIGN_CENTER
                Title.Indent = -44
                Title.VerticalAlignment = Element.ALIGN_BOTTOM
                Title.BorderWidth = 0
                Title.PaddingBottom = 2
                Title.PaddingTop = 2
                HeadingTable.AddCell(Title)
                HeadingTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 


                Dim pHeaderLine1 As New Paragraph("THE EMPIRE DISTRICT ELECTRIC COMPANY", font2)
                pHeaderLine1.SetAlignment(Element.ALIGN_RIGHT)
                pHeaderLine1.IndentationLeft = 200
                pHeaderLine1.SpacingBefore = 2.0F
                Dim pHeaderLine2 As New Paragraph("PERFORMANCE PLANNING AND REVIEW", font2)
                pHeaderLine2.SetAlignment(Element.ALIGN_RIGHT)
                pHeaderLine2.IndentationLeft = 207

                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT Employee_First_Name,Employee_Last_Name,Employee_Title,Employee_Location,Salaried,PS_EMPLID from PerfEmployee where PS_EMPLID = @PS_EMPLID"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", PS_EMPLID)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

                    If myLookup.Read Then
                        'Employee Info
                        Dim EmployeeTable As New PdfPTable(3) 'number of column(s)
                        EmployeeTable.WidthPercentage = 100

                        Dim NameHCell As PdfPCell = New PdfPCell(New Phrase(New Chunk("Employee Name: " & myLookup("Employee_First_Name").ToString & " " & myLookup("Employee_Last_Name").ToString, font2)))
                        NameHCell.HorizontalAlignment = Element.ALIGN_LEFT
                        NameHCell.BorderWidth = 1
                        NameHCell.PaddingBottom = 2
                        NameHCell.PaddingTop = 2
                        EmployeeTable.AddCell(NameHCell)
                        EmployeeTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        Dim TitleHCell As PdfPCell = New PdfPCell(New Phrase(New Chunk("Title: " & myLookup("Employee_Title").ToString, font2)))
                        TitleHCell.HorizontalAlignment = Element.ALIGN_LEFT
                        TitleHCell.BorderWidth = 1
                        TitleHCell.PaddingBottom = 2
                        TitleHCell.PaddingTop = 2
                        EmployeeTable.AddCell(TitleHCell)
                        EmployeeTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        If Not myLookup("Employee_Location") Is DBNull.Value Then

                            Dim LocationHCell As PdfPCell = New PdfPCell(New Phrase(New Chunk("Location: " & myLookup("Employee_Location").ToString, font2)))
                            LocationHCell.HorizontalAlignment = Element.ALIGN_LEFT
                            LocationHCell.BorderWidth = 1
                            LocationHCell.PaddingBottom = 2
                            LocationHCell.PaddingTop = 2
                            EmployeeTable.AddCell(LocationHCell)
                            EmployeeTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                        Else

                            Dim LocationHCell As PdfPCell = New PdfPCell(New Phrase(New Chunk("Location: ", font2)))
                            LocationHCell.HorizontalAlignment = Element.ALIGN_LEFT
                            LocationHCell.BorderWidth = 1
                            LocationHCell.PaddingBottom = 2
                            LocationHCell.PaddingTop = 2
                            EmployeeTable.AddCell(LocationHCell)
                            EmployeeTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                        End If


                        'Accountabilities Review date and closed by
                        Dim pHeader1 As New Paragraph("ACCOUNTABILITIES REVIEW", font1)
                        Dim AcctReviewTable As New PdfPTable(2) 'number of column(s)
                        AcctReviewTable.WidthPercentage = 100


                        If Review_Closed Then

                            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                Const SQLLookup2 As String = "Select [Review_Closed],[CLOSED_Date],[CLOSED_By] from [PerfReviewDates] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND [Review_Closed] = @Review_Closed"
                                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", PS_EMPLID)
                                myLookupCommand2.Parameters.AddWithValue("@Review_Year", Review_Year)
                                myLookupCommand2.Parameters.AddWithValue("@Review_Closed", True)

                                myConnection2.Open()
                                Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

                                If myLookup2.Read Then
                                    If myLookup2("Review_closed") = True Then

                                        Dim dateHCell As PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("Closed On: " & myLookup2("Closed_Date").ToString, font2)))
                                        dateHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                        dateHCell.BorderWidth = 1
                                        dateHCell.PaddingBottom = 2
                                        dateHCell.PaddingTop = 2
                                        AcctReviewTable.AddCell(dateHCell)
                                        AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                        Dim closedHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("Closed By: " & myLookup2("Closed_By").ToString, font2)))
                                        closedHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                        closedHCell.BorderWidth = 1
                                        closedHCell.PaddingBottom = 2
                                        closedHCell.PaddingTop = 2
                                        AcctReviewTable.AddCell(closedHCell)
                                        AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                    Else

                                        Dim dateHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("Closed_Date not available", font2)))
                                        dateHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                        dateHCell.BorderWidth = 1
                                        dateHCell.PaddingBottom = 2
                                        dateHCell.PaddingTop = 2
                                        AcctReviewTable.AddCell(dateHCell)
                                        AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                        Dim closedHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("", font2)))
                                        closedHCell.HorizontalAlignment = 1
                                        closedHCell.BorderWidth = 1
                                        closedHCell.PaddingBottom = 2
                                        closedHCell.PaddingTop = 2
                                        AcctReviewTable.AddCell(closedHCell)
                                        AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                                    End If
                                Else

                                    Dim dateHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("Closed_Date not available", font2)))
                                    dateHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                    dateHCell.BorderWidth = 1
                                    dateHCell.PaddingBottom = 2
                                    dateHCell.PaddingTop = 2
                                    AcctReviewTable.AddCell(dateHCell)
                                    AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                    Dim closedHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("", font2)))
                                    closedHCell.HorizontalAlignment = 1
                                    closedHCell.BorderWidth = 1
                                    closedHCell.PaddingBottom = 2
                                    closedHCell.PaddingTop = 2
                                    AcctReviewTable.AddCell(closedHCell)
                                    AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                                End If
                                myLookup2.Close()
                                myConnection2.Close()
                            End Using

                        Else

                            Dim dateHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("", font2)))
                            dateHCell.HorizontalAlignment = Element.ALIGN_LEFT
                            dateHCell.BorderWidth = 1
                            dateHCell.PaddingBottom = 2
                            dateHCell.PaddingTop = 2
                            AcctReviewTable.AddCell(dateHCell)
                            AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            Dim closedHCell As pdf.PdfPCell = New pdf.PdfPCell(New Phrase(New Chunk("", font2)))
                            closedHCell.HorizontalAlignment = 1
                            closedHCell.BorderWidth = 1
                            closedHCell.PaddingBottom = 2
                            closedHCell.PaddingTop = 2
                            AcctReviewTable.AddCell(closedHCell)
                            AcctReviewTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                        End If

                        'Performance and Learning Goals
                        Dim PLTable As New PdfPTable(3) 'number of column(s)
                        PLTable.WidthPercentage = 100
                        Dim intArray() As Integer = {40, 15, 45}
                        PLTable.SetWidths(intArray)

                        Dim PL1HCell As PdfPCell = Nothing
                        Dim PL2HCell As PdfPCell = Nothing
                        Dim PL3HCell As PdfPCell = Nothing

                        PL1HCell = New PdfPCell(New Phrase(New Chunk("PERFORMANCE AND LEARNING GOALS", font3)))
                        PL1HCell.HorizontalAlignment = 1
                        PL1HCell.BorderWidth = 1
                        PLTable.AddCell(PL1HCell)
                        PLTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        PL2HCell = New PdfPCell(New Phrase(New Chunk("ASSESSMENT", font3)))
                        PL2HCell.HorizontalAlignment = 1
                        PL2HCell.BorderWidth = 1
                        PLTable.AddCell(PL2HCell)
                        PLTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        PL3HCell = New PdfPCell(New Phrase(New Chunk("RATING COMMENTS", font3)))
                        PL3HCell.HorizontalAlignment = 1
                        PL3HCell.BorderWidth = 1
                        PLTable.AddCell(PL3HCell)
                        PLTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 
                        'Performance and Learning Rows


                        Using myConnectionPL As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Dim PLSQLLookup As String = "SELECT B.Learning_Goal,B.Learning_Assessment,B.Learning_Comment,B.LG_ID from PerfEmployee A, PerfLearningGoals B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.LG_ID"
                            Dim PLLookupCommand As New SqlCommand(PLSQLLookup, myConnectionPL)


                            myConnectionPL.Open()
                            Dim PLLookup As SqlDataReader = PLLookupCommand.ExecuteReader

                            While PLLookup.Read

                                PL1HCell = New PdfPCell(New Phrase(New Chunk(PLLookup("Learning_Goal").ToString, font2)))
                                PL1HCell.HorizontalAlignment = Element.ALIGN_LEFT
                                PL1HCell.BorderWidth = 1
                                'PL1HCell.BorderWidthLeft = 1
                                PL1HCell.PaddingBottom = 10
                                PL1HCell.PaddingTop = 10
                                PLTable.AddCell(PL1HCell)
                                PLTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table                                 

                                PL2HCell = New PdfPCell(New Phrase(New Chunk(PLLookup("Learning_Assessment").ToString, font2)))
                                PL2HCell.HorizontalAlignment = 1
                                PL2HCell.BorderWidth = 1
                                'PL2HCell.BorderWidthLeft = 1
                                'PL2HCell.BorderWidthRight = 1
                                PL2HCell.PaddingBottom = 10
                                PL2HCell.PaddingTop = 10
                                PLTable.AddCell(PL2HCell)
                                PLTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                PL3HCell = New PdfPCell(New Phrase(New Chunk(PLLookup("Learning_Comment").ToString, font2)))
                                PL3HCell.HorizontalAlignment = 1
                                PL3HCell.BorderWidth = 1
                                'PL1HCell.BorderWidthLeft = 1
                                PL3HCell.PaddingBottom = 10
                                PL3HCell.PaddingTop = 10
                                PLTable.AddCell(PL3HCell)
                                PLTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table

                            End While
                            PLLookup.Close()
                            myConnectionPL.Close()
                        End Using



                        'Notes
                        Dim pNote1 As New Paragraph("Employees must meet expectations on each goal to be eligible for: * Merit increase (Adjustment to base) * Incentive Award (Lump sum)", FontFactory.GetFont("ARIAL", 8, Font.NORMAL, BaseColor.BLACK))
                        Dim pNote2 As New Paragraph("NOTE: Employees who did not meet expectations on a particular goal may be eligible, based on manager's recommendations & director approval.", FontFactory.GetFont("ARIAL", 8, Font.NORMAL, BaseColor.BLACK))
                        Dim INTable As New PdfPTable(3) 'number of column(s)
                        If myLookup("Salaried") = True Then

                            'Incentive Goals

                            INTable.WidthPercentage = 100
                            INTable.SetWidths(intArray)

                            Dim INGoalsHCell As PdfPCell = Nothing
                            Dim INAssessmentHCell As PdfPCell = Nothing
                            Dim INCommentHCell As PdfPCell = Nothing

                            INGoalsHCell = New PdfPCell(New Phrase(New Chunk("INCENTIVE GOALS for SALARIED EMPLOYEES", font3)))
                            INGoalsHCell.HorizontalAlignment = 1
                            INGoalsHCell.BorderWidth = 1
                            INTable.AddCell(INGoalsHCell)
                            INTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            INAssessmentHCell = New PdfPCell(New Phrase(New Chunk("ASSESSMENT", font3)))
                            INAssessmentHCell.HorizontalAlignment = 1
                            INAssessmentHCell.BorderWidth = 1
                            INTable.AddCell(INAssessmentHCell)
                            INTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            INCommentHCell = New PdfPCell(New Phrase(New Chunk("RATING COMMENTS", font3)))
                            INCommentHCell.HorizontalAlignment = 1
                            INCommentHCell.BorderWidth = 1
                            INTable.AddCell(INCommentHCell)
                            INTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            Using myConnectionIN As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                Dim INSQLLookup As String = "SELECT B.Incentive_Goal,B.Incentive_Assessment,B.Incentive_Comment,B.IG_ID from PerfEmployee A, PerfIncentiveGoals B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.IG_ID"
                                Dim INLookupCommand As New SqlCommand(INSQLLookup, myConnectionIN)


                                myConnectionIN.Open()
                                Dim INLookup As SqlDataReader = INLookupCommand.ExecuteReader

                                While INLookup.Read

                                    INGoalsHCell = New PdfPCell(New Phrase(New Chunk(INLookup("Incentive_Goal").ToString, font2)))
                                    INGoalsHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                    INGoalsHCell.BorderWidth = 1
                                    'PL1HCell.BorderWidthLeft = 1
                                    INGoalsHCell.PaddingBottom = 10
                                    INGoalsHCell.PaddingTop = 10
                                    INTable.AddCell(INGoalsHCell)
                                    INTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table                                 

                                    INAssessmentHCell = New PdfPCell(New Phrase(New Chunk(INLookup("Incentive_Assessment").ToString, font2)))
                                    INAssessmentHCell.HorizontalAlignment = 1
                                    INAssessmentHCell.BorderWidth = 1
                                    INAssessmentHCell.PaddingBottom = 10
                                    INAssessmentHCell.PaddingTop = 10
                                    INTable.AddCell(INAssessmentHCell)
                                    INTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                    INCommentHCell = New PdfPCell(New Phrase(New Chunk(INLookup("Incentive_Comment").ToString, font2)))
                                    INCommentHCell.HorizontalAlignment = 1
                                    INCommentHCell.BorderWidth = 1
                                    INCommentHCell.PaddingBottom = 10
                                    INCommentHCell.PaddingTop = 10
                                    INTable.AddCell(INCommentHCell)
                                    INTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                                End While
                                INLookup.Close()
                                myConnectionIN.Close()
                            End Using

                        End If

                        'Competencies Key


                        Dim pHeader2 As New Paragraph("COMPETENCIES REVIEW", font1)

                        Dim KeyTable As New PdfPTable(1) 'number of column(s)
                        KeyTable.WidthPercentage = 100

                        Dim CompGoalsHCell As PdfPCell = New PdfPCell(New Phrase(New Chunk("ASSESSMENT: HP=HIGHLY PROFICIENT P=PROFICIENT ND=NEEDS DEVELOPMENT", font2)))
                        CompGoalsHCell.HorizontalAlignment = 1
                        CompGoalsHCell.BorderWidth = 1
                        KeyTable.AddCell(CompGoalsHCell)
                        KeyTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 


                        'Core Competencies Review
                        Dim CCompTable As New PdfPTable(3) 'number of column(s)
                        CCompTable.WidthPercentage = 100
                        Dim IntArray2() As Integer = {40, 15, 45}
                        CCompTable.SetWidths(IntArray2)

                        Dim CompHeadHCell As PdfPCell = Nothing
                        Dim CompHead2HCell As PdfPCell = Nothing
                        Dim CompHead3HCell As PdfPCell = Nothing

                        CompHeadHCell = New PdfPCell(New Phrase(New Chunk("CORE COMPETENCIES", font3)))
                        CompHeadHCell.HorizontalAlignment = 1
                        CompHeadHCell.BorderWidth = 1
                        CCompTable.AddCell(CompHeadHCell)
                        CCompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        CompHead2HCell = New PdfPCell(New Phrase(New Chunk("ASSESSMENT", font3)))
                        CompHead2HCell.HorizontalAlignment = 1
                        CompHead2HCell.BorderWidth = 1
                        CCompTable.AddCell(CompHead2HCell)
                        CCompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        CompHead3HCell = New PdfPCell(New Phrase(New Chunk("CORE COMPETENCY DEVELOPMENT SUGGESTIONS", font3)))
                        CompHead3HCell.HorizontalAlignment = 1
                        CompHead3HCell.BorderWidth = 1
                        CCompTable.AddCell(CompHead3HCell)
                        CCompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        Using myConnectionComp As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Dim CompSQLLookup As String = "SELECT B.Competence_Goal,B.Competence_Assessment,Competence_Suggestion,B.CG_ID from PerfEmployee A, PerfCompetencies B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " AND B.Competence_Goal LIKE '(CORE)%' order by b.CG_ID"
                            Dim CompLookupCommand As New SqlCommand(CompSQLLookup, myConnectionComp)


                            myConnectionComp.Open()
                            Dim CompLookup As SqlDataReader = CompLookupCommand.ExecuteReader

                            While CompLookup.Read

                                CompHeadHCell = New PdfPCell(New Phrase(New Chunk(CompLookup("Competence_Goal").ToString, font2)))
                                CompHeadHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                CompHeadHCell.BorderWidth = 1
                                'PL1HCell.BorderWidthLeft = 1
                                CompHeadHCell.PaddingBottom = 10
                                CompHeadHCell.PaddingTop = 10
                                CCompTable.AddCell(CompHeadHCell)
                                CCompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table                                 

                                CompHead2HCell = New PdfPCell(New Phrase(New Chunk(CompLookup("Competence_Assessment").ToString, font2)))
                                CompHead2HCell.HorizontalAlignment = 1
                                CompHead2HCell.BorderWidth = 1
                                CompHead2HCell.PaddingBottom = 10
                                CompHead2HCell.PaddingTop = 10
                                CCompTable.AddCell(CompHead2HCell)
                                CCompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table

                                CompHead3HCell = New PdfPCell(New Phrase(New Chunk(CompLookup("Competence_Suggestion").ToString, font2)))
                                CompHead3HCell.HorizontalAlignment = 1
                                CompHead3HCell.BorderWidth = 1
                                CompHead3HCell.PaddingBottom = 10
                                CompHead3HCell.PaddingTop = 10
                                CCompTable.AddCell(CompHead3HCell)
                                CCompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            End While
                            CompLookup.Close()
                            myConnectionComp.Close()
                        End Using

                        'Additional Competencies Review
                        Dim ACompTable As New PdfPTable(3) 'number of column(s)
                        ACompTable.WidthPercentage = 100
                        Dim IntArray4() As Integer = {40, 15, 45}
                        ACompTable.SetWidths(IntArray4)

                        Dim ACompHeadHCell As PdfPCell = Nothing
                        Dim ACompHead2HCell As PdfPCell = Nothing
                        Dim ACompHead3HCell As PdfPCell = Nothing

                        ACompHeadHCell = New PdfPCell(New Phrase(New Chunk("ADDITIONAL COMPETENCIES", font3)))
                        ACompHeadHCell.HorizontalAlignment = 1
                        ACompHeadHCell.BorderWidth = 1
                        ACompTable.AddCell(ACompHeadHCell)
                        ACompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        ACompHead2HCell = New PdfPCell(New Phrase(New Chunk("ASSESSMENT", font3)))
                        ACompHead2HCell.HorizontalAlignment = 1
                        ACompHead2HCell.BorderWidth = 1
                        ACompTable.AddCell(ACompHead2HCell)
                        ACompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        ACompHead3HCell = New PdfPCell(New Phrase(New Chunk("ADDITIONAL COMPETENCY DEVELOPMENT SUGGESTIONS", font3)))
                        ACompHead3HCell.HorizontalAlignment = 1
                        ACompHead3HCell.BorderWidth = 1
                        ACompTable.AddCell(ACompHead3HCell)
                        ACompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        Using myConnectionAComp As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Dim ACompSQLLookup As String = "SELECT B.Competence_Goal,B.Competence_Assessment,Competence_Suggestion,B.CG_ID from PerfEmployee A, PerfCompetencies B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " AND B.Competence_Goal not like '(CORE)%' order by b.CG_ID"
                            Dim ACompLookupCommand As New SqlCommand(ACompSQLLookup, myConnectionAComp)


                            myConnectionAComp.Open()
                            Dim ACompLookup As SqlDataReader = ACompLookupCommand.ExecuteReader

                            While ACompLookup.Read

                                ACompHeadHCell = New PdfPCell(New Phrase(New Chunk(ACompLookup("Competence_Goal").ToString, font2)))
                                ACompHeadHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                ACompHeadHCell.BorderWidth = 1
                                'PL1HCell.BorderWidthLeft = 1
                                ACompHeadHCell.PaddingBottom = 10
                                ACompHeadHCell.PaddingTop = 10
                                ACompTable.AddCell(ACompHeadHCell)
                                ACompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table                                 

                                ACompHead2HCell = New PdfPCell(New Phrase(New Chunk(ACompLookup("Competence_Assessment").ToString, font2)))
                                ACompHead2HCell.HorizontalAlignment = 1
                                ACompHead2HCell.BorderWidth = 1
                                ACompHead2HCell.PaddingBottom = 10
                                ACompHead2HCell.PaddingTop = 10
                                ACompTable.AddCell(ACompHead2HCell)
                                ACompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table

                                ACompHead3HCell = New PdfPCell(New Phrase(New Chunk(ACompLookup("Competence_Suggestion").ToString, font2)))
                                ACompHead3HCell.HorizontalAlignment = 1
                                ACompHead3HCell.BorderWidth = 1
                                ACompHead3HCell.PaddingBottom = 10
                                ACompHead3HCell.PaddingTop = 10
                                ACompTable.AddCell(ACompHead3HCell)
                                ACompTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            End While
                            ACompLookup.Close()
                            myConnectionAComp.Close()
                        End Using

                        'Comments 
                        Dim CommTable As New PdfPTable(1) 'number of column(s)
                        CommTable.WidthPercentage = 100
                        Dim CommHeadHCell As PdfPCell = Nothing
                        CommHeadHCell = New PdfPCell(New Phrase(New Chunk("COMMENTS", font3)))
                        CommHeadHCell.HorizontalAlignment = 1
                        CommHeadHCell.BorderWidth = 1
                        CommTable.AddCell(CommHeadHCell)
                        CommTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                        Using myConnectionComm As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Dim CommSQLLookup As String = "SELECT B.Comment_Description,b.CM_ID from PerfEmployee A, PerfComments B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.CM_ID"
                            Dim CommLookupCommand As New SqlCommand(CommSQLLookup, myConnectionComm)


                            myConnectionComm.Open()
                            Dim CommLookup As SqlDataReader = CommLookupCommand.ExecuteReader

                            While CommLookup.Read

                                CommHeadHCell = New PdfPCell(New Phrase(New Chunk(CommLookup("Comment_Description").ToString, font2)))
                                CommHeadHCell.HorizontalAlignment = Element.ALIGN_LEFT
                                CommHeadHCell.BorderWidth = 1
                                CommHeadHCell.PaddingBottom = 10
                                CommHeadHCell.PaddingTop = 10
                                CommTable.AddCell(CommHeadHCell)
                                CommTable.SpacingBefore = 15.0F ' Give some space after the text or it may overlap the table 

                            End While
                            CommLookup.Close()
                            myConnectionComm.Close()
                        End Using
                        Dim pDate As New Paragraph("Date_________________", font1)
                        Dim pEmployeeSignature As New Paragraph("Employee Signature________________________________________", font1)
                        Dim pManagersSignature As New Paragraph("Manager Signature ________________________________________", font1)
                        'signature and date


                        document.Add(HeadingTable)
                        document.Add(pHeaderLine1)
                        document.Add(pHeaderLine2)
                        document.Add(EmployeeTable)
                        document.Add(pHeader1)
                        document.Add(AcctReviewTable)
                        document.Add(PLTable)
                        document.Add(pNote1)
                        document.Add(pNote2)
                        document.Add(INTable)
                        document.NewPage()
                        document.Add(pHeader2)
                        document.Add(KeyTable)
                        document.Add(CCompTable)
                        document.Add(ACompTable)
                        document.Add(CommTable)
                        document.Add(pDate)
                        document.Add(pEmployeeSignature)
                        document.Add(pManagersSignature)
                    End If
                    myLookup.Close()
                    myConnection.Close()

                End Using

                document.Close()
                Response.OutputStream.Write(m.GetBuffer(), 0, m.GetBuffer().Length)
                Response.OutputStream.Flush()
                Response.OutputStream.Close()

            End If
        Catch
            Response.Write(Err.Description)
        End Try
    End Sub

End Class
