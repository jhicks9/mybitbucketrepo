﻿Imports System.IO
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.DataVisualization.Charting

Partial Class Maintenance_PortalApps_ICMP
    Inherits System.Web.UI.Page
    
    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isAdmin(ps.getPageName) Then
            pnlAuthorized.Visible = True
            pnlNotAuthorized.Visible = False
            pnlAdministration.Visible = True
        Else
            pnlAdministration.Visible = False
            If Not ps.isAuthorized(ps.getPageName) Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If
        End If

        If Not IsPostBack Then
            processFiles
        End If

        If String.IsNullOrEmpty(txtBegDate.Text) Then
            txtBegDate.Text = DateTime.Now.addmonths(-1).ToString("MM/dd/yyyy")
        End If
        If String.IsNullOrEmpty (txtEndDate.Text) Then
            txtEndDate.Text = DateTime.Now.ToString("MM/dd/yyyy")
        End If

        loadChart()

    End Sub

    Private Sub loadDaily()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Chart2.Visible = true
            'clear series for chart
            for each series in chart2.Series
                series.Points.Clear()
            Next

            chart2.BackColor = drawing.Color.FromArgb(211, 223, 240)
            chart2.BorderlineDashStyle = ChartDashStyle.Solid
            chart2.BackSecondaryColor = drawing.Color.White
            chart2.BackGradientStyle = GradientStyle.TopBottom
            chart2.BorderlineWidth = 1
            chart2.Palette = ChartColorPalette.BrightPastel
            chart2.BorderlineColor = drawing.Color.FromArgb(26, 59, 105)
            chart2.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            chart2.AntiAliasing = AntiAliasingStyles.All
            chart2.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal
            
            Chart2.Titles(0).Docking = Docking.Bottom
            Chart2.Titles(0).Alignment = Drawing.ContentAlignment.BottomCenter
	        Chart2.Titles(0).Text = "Denials for " & txtSearchDay.Text
	        Chart2.Titles(0).ShadowColor = Drawing.Color.FromArgb(32, 0, 0, 0)
	        Chart2.Titles(0).Font = New Drawing.Font("Trebuchet MS", 14F, Drawing.FontStyle.Bold)
	        Chart2.Titles(0).ShadowOffset = 3
	        Chart2.Titles(0).ForeColor = Drawing.Color.FromArgb(26, 59, 105)
            
	        Chart2.Legends(0).Enabled = True
	        Chart2.Legends(0).ShadowColor = Drawing.Color.FromArgb(32, 0, 0, 0)
	        Chart2.Legends(0).Font = New Drawing.Font("Trebuchet MS", 10F, Drawing.FontStyle.Bold)
	        Chart2.Legends(0).ShadowOffset = 3
	        Chart2.Legends(0).ForeColor = Drawing.Color.FromArgb(26, 59, 105)
	        Chart2.Legends(0).Title = ""
            Chart2.Legends(0).Docking = Docking.Right
            Chart2.Legends(0).Alignment = Drawing.StringAlignment.Near
	        
	        Chart2.ChartAreas(0).BackColor = Drawing.Color.Transparent
	        Chart2.ChartAreas(0).AxisX.IsLabelAutoFit = False
	        Chart2.ChartAreas(0).AxisY.IsLabelAutoFit = False
	        Chart2.ChartAreas(0).AxisX.LabelStyle.Font = New Drawing.Font("Verdana,Arial,Helvetica,sans-serif", 8F, Drawing.FontStyle.Regular)
	        Chart2.ChartAreas(0).AxisY.LabelStyle.Font = New Drawing.Font("Verdana,Arial,Helvetica,sans-serif", 8F, Drawing.FontStyle.Regular)
	        Chart2.ChartAreas(0).AxisY.LineColor = Drawing.Color.FromArgb(64, 64, 64, 64)
	        Chart2.ChartAreas(0).AxisX.LineColor = Drawing.Color.FromArgb(64, 64, 64, 64)
	        Chart2.ChartAreas(0).AxisY.MajorGrid.LineColor = Drawing.Color.FromArgb(64, 64, 64, 64)
	        Chart2.ChartAreas(0).AxisX.MajorGrid.LineColor = Drawing.Color.FromArgb(64, 64, 64, 64)
	        Chart2.ChartAreas(0).AxisX.Interval = 2
            Chart2.ChartAreas(0).AxisX.IntervalOffset = 1
            Chart2.ChartAreas(0).Area3DStyle.Enable3D  = False

            Dim cmd As New SqlCommand("", conn)
            cmd.CommandText = "SELECT [icmp_outside],[icmp_inside],[tcp_outside],[tcp_inside],[udp_outside],[udp_inside] FROM [log_data_for_charts] WHERE ([Log_date] = @Log_date)"
            cmd.Parameters.AddWithValue("@Log_date", txtSearchDay.Text)
            conn.Open()
            Dim myLookup As SqlDataReader = cmd.ExecuteReader
            If myLookup.HasRows Then
                While myLookup.Read
                    Chart2.Series("Deny icmp outside").Points.Add(myLookup("icmp_outside"))
                    Chart2.Series("Deny icmp inside").Points.Add(myLookup("icmp_inside"))
                    Chart2.Series("Deny tcp outside").Points.Add(myLookup("tcp_outside"))
                    Chart2.Series("Deny tcp inside").Points.Add(myLookup("tcp_inside"))
                    Chart2.Series("Deny udp outside").Points.Add(myLookup("udp_outside"))
                    Chart2.Series("Deny udp inside").Points.Add(myLookup("udp_inside"))

                    Chart2.Series("Deny icmp outside").ToolTip = "#VALY"
                    Chart2.Series("Deny icmp inside").ToolTip = "#VALY"
                    Chart2.Series("Deny tcp outside").ToolTip = "#VALY"
                    Chart2.Series("Deny tcp inside").ToolTip = "#VALY"
                    Chart2.Series("Deny udp outside").ToolTip = "#VALY"
                    Chart2.Series("Deny udp inside").ToolTip = "#VALY"
                End While
            End If
            myLookup.Close()
            conn.Close()
        Catch ex As Exception
            lblStatus.Text = Err.Description
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub loadChart()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            'clear series for chart
            for each series in chart1.Series
                series.Points.Clear()
            Next

            chart1.BackColor = drawing.Color.FromArgb(211, 223, 240)
            chart1.BorderSkin.BackColor = drawing.Color.FromArgb(255, 255, 255)
            chart1.BorderSkin.PageColor = drawing.Color.FromArgb(255, 255, 255)
            chart1.BorderlineDashStyle = ChartDashStyle.Solid
            chart1.BackSecondaryColor = drawing.Color.White
            chart1.BackGradientStyle = GradientStyle.TopBottom
            chart1.BorderlineWidth = 1
            chart1.Palette = ChartColorPalette.BrightPastel
            chart1.BorderlineColor = drawing.Color.FromArgb(26, 59, 105)
            chart1.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            chart1.AntiAliasing = AntiAliasingStyles.All
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal

            Chart1.Titles(0).Docking = Docking.Top
            Chart1.Titles(0).Alignment = Drawing.ContentAlignment.TopCenter
            Chart1.Titles(0).Text = "Denials from " & txtBegDate.Text & " to " & txtEndDate.Text
	        Chart1.Titles(0).ShadowColor = Drawing.Color.FromArgb(32, 0, 0, 0)
	        Chart1.Titles(0).Font = New Drawing.Font("Trebuchet MS", 14F, Drawing.FontStyle.Bold)
	        Chart1.Titles(0).ShadowOffset = 3
	        Chart1.Titles(0).ForeColor = Drawing.Color.FromArgb(26, 59, 105)

	        Chart1.Legends(0).Enabled = True
	        Chart1.Legends(0).ShadowColor = Drawing.Color.FromArgb(32, 0, 0, 0)
	        Chart1.Legends(0).Font = New Drawing.Font("Trebuchet MS", 10F, Drawing.FontStyle.Bold)
	        Chart1.Legends(0).ShadowOffset = 3
	        Chart1.Legends(0).ForeColor = Drawing.Color.FromArgb(26, 59, 105)
            Chart1.Legends(0).TitleFont = New Drawing.Font("Trebuchet MS", 10F, Drawing.FontStyle.Bold)
            Chart1.Legends(0).Docking = Docking.Right
            Chart1.Legends(0).Alignment = Drawing.StringAlignment.Near

            Dim cmd As New SqlCommand("", conn)
            cmd.CommandText = "SELECT [Log_date],[icmp_outside],[icmp_inside],[tcp_outside],[tcp_inside],[udp_outside],[udp_inside] FROM [log_data_for_charts] WHERE ([Log_date] >= @begDate AND [Log_date] <= @endDate ) order by Log_date desc"
            cmd.Parameters.AddWithValue("@begDate", txtBegDate.Text)
            cmd.Parameters.AddWithValue("@endDate", txtEndDate.Text)
            conn.Open()
            Dim myLookup2 As SqlDataReader = cmd.ExecuteReader
            If myLookup2.HasRows Then
                While myLookup2.Read
                    Chart1.Series("Deny icmp outside").Points.AddXY(myLookup2("Log_date"), myLookup2("icmp_outside"))
                    Chart1.Series("Deny icmp inside").Points.AddXY(myLookup2("Log_date"), myLookup2("icmp_inside"))
                    Chart1.Series("Deny tcp outside").Points.AddXY(myLookup2("Log_date"), myLookup2("tcp_outside"))
                    Chart1.Series("Deny tcp inside").Points.AddXY(myLookup2("Log_date"), myLookup2("tcp_inside"))
                    Chart1.Series("Deny udp outside").Points.AddXY(myLookup2("Log_date"), myLookup2("udp_outside"))
                    Chart1.Series("Deny udp inside").Points.AddXY(myLookup2("Log_date"), myLookup2("udp_inside"))

                    Chart1.Series("Deny icmp outside").ToolTip = "#VALY"
                    Chart1.Series("Deny icmp inside").ToolTip = "#VALY"
                    Chart1.Series("Deny tcp outside").ToolTip = "#VALY"
                    Chart1.Series("Deny tcp inside").ToolTip = "#VALY"
                    Chart1.Series("Deny udp outside").ToolTip = "#VALY"
                    Chart1.Series("Deny udp inside").ToolTip = "#VALY"
                End While
            End If
            myLookup2.Close()
            conn.Close()
        Catch ex As Exception
            lblStatus.Text = Err.Description
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub lbDateRange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDateRange.Click
        loadChart()
    End Sub

    Protected Sub lbSearchDay_Click(sender As Object, e As EventArgs) Handles lbSearchDay.Click
        loadDaily()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#DayDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DayDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub lbDailyChart_Click(sender As Object, e As EventArgs) Handles lbDailyChart.Click

        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#DayDetail').modal('show');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DayDetailModalScript", sb.ToString(), False)
    End Sub

    Private Sub processFiles()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim cmd As New SqlCommand("", conn)
            Dim drLookup As SqlDataReader
            Dim updateRequired As Boolean = false
            Dim logdataDate As nullable(of DateTime) = nothing
            Dim fullPath As String = "C:\Denial_Logs"
            Dim icmp_count_outside As Int32 = 0
            Dim icmp_count_inside As Int32 = 0
            Dim tcp_count_outside As Int32 = 0
            Dim tcp_count_inside As Int32 = 0
            Dim udp_count_outside As Int32 = 0
            Dim udp_count_inside As Int32 = 0
            Dim dataLine As String = ""

            If System.IO.Directory.Exists (fullPath) Then

                Dim FileEntries() As String = Directory.GetFiles(fullPath)
                For Each filename In FileEntries

                    icmp_count_outside = 0 'reset counter
                    icmp_count_inside = 0 'reset counter
                    tcp_count_outside = 0 'reset counter
                    tcp_count_inside = 0 'reset counter
                    udp_count_outside = 0 'reset counter
                    udp_count_inside = 0 'reset counter
                    logdataDate = Nothing 'reset date

                    Dim stream As Stream = System.IO.File.OpenRead(filename)
                    Using (stream)

                        Dim zipInputStream As New ICSharpCode.SharpZipLib.Zip.ZipInputStream(stream)
	                    Dim zipEntry As ICSharpCode.SharpZipLib.Zip.ZipEntry = zipInputStream.GetNextEntry()
	                    While zipEntry IsNot Nothing 'zip file contains a compressed file
		                    Using sr As new system.IO.memorystream 'unzip compressed file
			                    ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(zipInputStream, sr, New Byte(4095) {})
                                sr.Position = 0
                                Dim filedata = New Byte(sr.length - 1) {}
                                sr.Read(filedata, 0, sr.length)
                                sr.Position = 0

                                Dim streamReader As StreamReader = New StreamReader(sr, System.Text.Encoding.ASCII)
                                Using streamReader 'process unzipped file
                                    While (streamReader.Peek() > -1)
                                        dataLine = streamReader.ReadLine
                                        If logdataDate is Nothing then
                                            logdataDate = Left(dataLine, 10)
                                        End If

                                        If InStr(dataLine, "Deny icmp") Then
                                            If InStr(dataLine, "src outside") Then
                                                icmp_count_outside += 1
                                            End If
                                            If InStr(dataLine, "src inside") Then
                                                icmp_count_inside += 1
                                            End If
                                        End If

                                        If InStr(dataLine, "Deny tcp") Then
                                            If InStr(dataLine, "src outside") Then
                                                tcp_count_outside += 1
                                            End If
                                            If InStr(dataLine, "src inside") Then
                                                tcp_count_inside += 1
                                            End If
                                        End If

                                        If InStr(dataLine, "Deny udp") Then
                                            If InStr(dataLine, "src outside") Then
                                                udp_count_outside += 1
                                            End If
                                            If InStr(dataLine, "src inside") Then
                                                udp_count_inside += 1
                                            End If
                                        End If
                                    End While 'parsing streamreader rows
                                End Using  'streamReader
		                    End Using 'unzipped memorystream
		                    zipEntry = zipInputStream.GetNextEntry()
                            End While 'zipEntry
                        End Using 'zip file

                    cmd.Parameters.Clear 
                    cmd.Parameters.AddWithValue("@icmp_outside", icmp_count_outside)
                    cmd.Parameters.AddWithValue("@icmp_inside", icmp_count_inside)
                    cmd.Parameters.AddWithValue("@tcp_outside", tcp_count_outside)
                    cmd.Parameters.AddWithValue("@tcp_inside", tcp_count_inside)
                    cmd.Parameters.AddWithValue("@udp_outside", udp_count_outside)
                    cmd.Parameters.AddWithValue("@udp_inside", udp_count_inside)
                    cmd.Parameters.AddWithValue("@log_date", logdataDate)
                    conn.open
                    cmd.CommandText = "SELECT count(*)as total from log_data_for_charts where log_data_for_charts.log_date = @log_date"
                    drLookup = cmd.ExecuteReader  'check for control record
                    If drLookup.HasRows Then
                        While drLookup.Read
                            updateRequired = drLookup("total").ToString
                        End While
                    Else
                        updateRequired = false
                    End If
                    drLookup.Close()
                    If updateRequired Then
                        cmd.CommandText = "update log_data_for_charts set icmp_outside=@icmp_outside, icmp_inside=@icmp_inside, tcp_outside=@tcp_outside, tcp_inside=@tcp_inside, udp_outside=@udp_outside, udp_inside=@udp_inside where log_data_for_charts.log_date = @log_date"
                    Else
                        cmd.CommandText = "insert into [log_data_for_charts] ([Log_date],[icmp_outside],[icmp_inside],[tcp_outside],[tcp_inside],[udp_outside],[udp_inside]) values (@log_date,@icmp_outside,@icmp_inside,@tcp_outside,@tcp_inside,@udp_outside,@udp_inside)"
                    End If

                    cmd.ExecuteNonQuery()
                    conn.close

                    File.Delete(filename) 'remove file

                    Next 'next zip file in directory
            End If 'directory exists
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            lblStatus.Text = Err.Description
        End Try
    End Sub
End Class
