﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Maintenance_PortalApps_NetMeter
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'encrypt the viewstate when saving sql statements across postbacks
        Page.RegisterRequiresViewStateEncryption()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If

            pnlDetails.Visible = False
            gvData.SelectedIndex = -1
            If Not IsPostBack Then
                Show_Data()
            End If
            If Not ViewState("currentCommand") Is Nothing Then
                dsNetMeter.SelectCommand = ViewState("currentCommand")
            End If
            txtComment.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) return false;")
        Catch
        End Try
    End Sub

    Public Sub Show_Data()
        Try
            Dim query As String = "select netmeter.id,netmeter.queue_id,netmeter.acct_pkg,netmeter.last_name,netmeter.first_name,netmeter.system_type,netmeter.service_state,netmeter.sr_postmark_date,netmeter.entered_by,netmeter.companyname,netmeter.operational_date,netmeter.sr_paid_date from netmeter where netmeter.controlrecord = 0"
            Dim status As String = ""
            Dim buildSearch As new StringBuilder
            Dim orderby As String = " order by sr_postmark_date ASC"

            If Not String.IsNullOrEmpty(txtSearchLastName.Text) Then
                buildSearch.Append(" and last_name like '%" & txtSearchLastName.Text & "%'")
            End If
            If Not String.IsNullOrEmpty(txtSearchState.Text) Then
                buildSearch.Append (" and service_state ='" & txtSearchState.Text & "'")
            End If
            If Not String.IsNullOrEmpty(txtSearchAcctPkg.Text) Then
                buildSearch.Append(" and acct_pkg like '%" & txtSearchAcctPkg.Text & "%'")
            End If
            If Not String.IsNullOrEmpty (txtSearchCompanyName.Text) Then
                buildSearch.Append(" and companyname like '%" & txtSearchCompanyName.Text & "%'")
            End If
            If Not String.IsNullOrEmpty(txtSearchContact.Text) Then
                buildSearch.Append(" and entered_by like '%" & txtSearchContact.Text & "%'")
            End If

            If buildSearch.Length > 0 Then
                dsNetMeter.SelectCommand = query & buildSearch.ToString & orderby
            Else
                dsNetMeter.SelectCommand = query & status & orderby
            End If
            ViewState("currentCommand") = dsNetMeter.SelectCommand
            dsNetMeter.DataBind()
            gvData.DataBind()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub gvData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvData.RowCommand
        Dim selectticket As String = ""
        If e.CommandName = "Select" Then
            editData (e.CommandArgument)
            pnlDetails.Visible = True
            pnlDatas.Visible = False
        End If
    End Sub

    Public Sub editData(Optional ByVal id As Integer = 0)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader
        Try
            hdnAction.Value = "update"
            conn.Open()
            cmd.CommandText = "select * from netmeter where netmeter.id = " & id
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hdnID.Value = dr("id").ToString
                    lblQueueID.Text = dr("queue_id").ToString
                    txtEnteredBy.Text = dr("entered_by").ToString
                    txtLastName.Text = dr("last_name").ToString
                    txtFirstName.Text = dr("first_name").ToString
                    txtServiceAddress.Text = dr("service_address").ToString
                    txtServiceCity.Text = dr("service_city").ToString
                    txtServiceState.Text = dr("service_state").ToString
                    txtServiceZip.Text = dr("service_zip").ToString
                    txtMailAddress.Text = dr("mailing_address").ToString
                    txtMailCity.Text = dr("mailing_city").ToString
                    txtMailState.Text = dr("mailing_state").ToString
                    txtMailZip.Text = dr("mailing_zip").ToString
                    txtHomePhone.Text = dr("home_phone").ToString
                    txtCellPhone.Text = dr("cell_phone").ToString
                    txtEmail.Text = dr("email").ToString
                    txtAcctPkg.Text = dr("acct_pkg").ToString
                    txtNMAppReceiptDate.Text = dr("nm_app_receipt_date").ToString
                    txtSRPostMarkDate.Text = dr("sr_postmark_date").ToString
                    txtRate.Text = dr("rate").ToString
                    txtSystemType.Text = dr("system_type").ToString
                    txtACSystemRating.Text = dr("ac_system_rating").ToString
                    txtDCSystemRating.Text = dr("dc_system_rating").ToString
                    txtSolarModuleMfr.Text = dr("solar_module_mfr").ToString
                    txtSolarModuleNum.Text = dr("solar_module_num").ToString
                    txtModuleRating.Text = dr("module_rating").ToString
                    txtInverterRating.Text = dr("inverter_rating").ToString
                    txtNumberPanels.Text = dr("number_panels").ToString
                    txtSystemRating.Text = dr("system_rating").ToString
                    txtEngineeringDate.Text = dr("engineering_date").ToString
                    txtW9Received.Text = dr("w9_received").ToString
                    txtSA76Amount.Text = dr("sa76_amount").ToString
                    txtSA76Recieved.Text = dr("sa76_received").ToString
                    txtSA76PaymentReceived.Text = dr("sa76_payment_received").ToString
                    txtDetailReceiptsReceived.Text = dr("detailed_receipts_received").ToString
                    txtWarrantySheetReceived.Text = dr("warranty_sheet_received").ToString
                    txtPhotosReceived.Text = dr("photos_received").ToString
                    txtLetterDate.Text = dr("letter_date").ToString
                    txtDenialLetterDate.Text = dr("denial_letter_date").ToString
                    txtNMResubmittalDate.Text = dr("nm_resubmittal_date").ToString
                    txtSRCommitmentDate.Text = dr("sr_commitment_date").ToString
                    txtSRCommitmentAmount.Text = dr("sr_commitment_amount").ToString
                    txtCompletionDate.Text = dr("completion_date").ToString
                    txtCommissionTestDate.Text = dr("commission_test_date").ToString
                    txtOperationalDate.Text = dr("operational_date").ToString
                    txtSRAffidavitDate.Text = dr("sr_affidavit_date").ToString
                    txtAzimuth.Text = dr("azimuth").ToString
                    txtTilt.Text = dr("tilt").ToString
                    txtAnnualSRec.Text = dr("annual_srec").ToString
                    txtSRPaidDate.Text = dr("sr_paid_date").ToString
                    txtSRPaidAmount.Text = dr("sr_paid_amount").ToString
                    txtComment.Text = dr("comments").ToString
                    txtCompanyName.Text = dr("companyname").ToString
                    txtMeterID.Text = dr("meterid").ToString
                    txtMeterManufacturer.Text = dr("metermanufacturer").ToString
                End While
            End If
            dr.Close()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub

    Public Function updateData() As Boolean
        Dim status As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim query As String = ""
        Dim mRating As Decimal = 0
        Dim nPanels As Integer = 0
        Dim sRating As Decimal = 0
        Dim cAmount As Decimal = 0
        Dim pAmount As Decimal = 0
        Dim sRec As Decimal = 0
        Dim sa76Amount As Decimal = 0
        Try
            Select Case hdnAction.Value
                Case "update"
                    query = "update netmeter set controlrecord=0,entered_by=@entered_by,last_name=@last_name,first_name=@first_name," & _
                        "service_address=@service_address,service_city=@service_city," & _
                        "service_state=@service_state,service_zip=@service_zip," & _
                        "mailing_address=@mailing_address,mailing_city=@mailing_city," & _
                        "mailing_state=@mailing_state,mailing_zip=@mailing_zip," & _
                        "home_phone=@home_phone,cell_phone=@cell_phone,email=@email,acct_pkg=@acct_pkg," & _
                        "nm_app_receipt_date=@nm_app_receipt_date,sr_postmark_date=@sr_postmark_date,rate=@rate," & _
                        "system_type=@system_type,ac_system_rating=@ac_system_rating," & _
                        "dc_system_rating=@dc_system_rating,solar_module_mfr=@solar_module_mfr," & _
                        "solar_module_num=@solar_module_num,module_rating=@module_rating," & _
                        "inverter_rating=@inverter_rating,number_panels=@number_panels,system_rating=@system_rating," & _
                        "engineering_date=@engineering_date,letter_date=@letter_date,sr_commitment_date=@sr_commitment_date," & _
                        "denial_letter_date=@denial_letter_date,nm_resubmittal_date=@nm_resubmittal_date," & _
                        "sr_commitment_amount=@sr_commitment_amount,completion_date=@completion_date," & _
                        "commission_test_date=@commission_test_date,operational_date=@operational_date," & _
                        "sr_affidavit_date=@sr_affidavit_date,annual_srec=@annual_srec," & _
                        "sr_paid_date=@sr_paid_date,sr_paid_amount=@sr_paid_amount,comments=@comments," & _
                        "w9_received=@w9_received,sa76_amount=@sa76_amount,sa76_received=@sa76_received," & _
                        "sa76_payment_received=@sa76_payment_received,detailed_receipts_received=@detailed_receipts_received," & _
                        "warranty_sheet_received=@warranty_sheet_received,photos_received=@photos_received," & _
                        "azimuth=@azimuth,tilt=@tilt,companyname=@companyname,meterid=@meterid,metermanufacturer=@metermanufacturer" & _
                        " where id=@id"
                Case "insert"
                    query = "insert into netmeter (controlrecord,entered_by,last_name,first_name," & _
                        "service_address,service_city,service_state,service_zip," & _
                        "mailing_address,mailing_city,mailing_state,mailing_zip," & _
                        "home_phone,cell_phone,email,acct_pkg,nm_app_receipt_date," & _
                        "sr_postmark_date,rate,system_type,ac_system_rating,dc_system_rating," & _
                        "solar_module_mfr,solar_module_num,module_rating,inverter_rating," & _
                        "number_panels,system_rating,engineering_date,letter_date,denial_letter_date,nm_resubmittal_date," & _
                        "sr_commitment_date,sr_commitment_amount,completion_date,commission_test_date,operational_date," & _
                        "sr_affidavit_date,annual_srec,sr_paid_date,sr_paid_amount,comments," & _
                        "w9_received,sa76_amount,sa76_received,sa76_payment_received,detailed_receipts_received," & _
                        "warranty_sheet_received,photos_received,azimuth,tilt,companyname,meterid,metermanufacturer" & _
                        ")" & _
                        " values (0,@entered_by,@last_name,@first_name,@service_address," & _
                        "@service_city,@service_state,@service_zip,@mailing_address," & _
                        "@mailing_city,@mailing_state,@mailing_zip,@home_phone,@cell_phone," & _
                        "@email,@acct_pkg,@nm_app_receipt_date,@sr_postmark_date,@rate," & _
                        "@system_type,@ac_system_rating,@dc_system_rating,@solar_module_mfr," & _
                        "@solar_module_num,@module_rating,@inverter_rating,@number_panels,@system_rating," & _
                        "@engineering_date,@letter_date,@denial_letter_date,@nm_resubmittal_date,@sr_commitment_date," & _
                        "@sr_commitment_amount,@completion_date,@commission_test_date,@operational_date,@sr_affidavit_date," & _
                        "@annual_srec,@sr_paid_date,@sr_paid_amount,@comments," & _
                        "@w9_received,@sa76_amount,@sa76_received,@sa76_payment_received,@detailed_receipts_received," & _
                        "@warranty_sheet_received,@photos_received,@azimuth,@tilt,@companyname,@meterid,@metermanufacturer" & _
                        ")"
            End Select
                
            cmd.CommandText = query
            cmd.Parameters.AddWithValue("@id", hdnID.Value)
            cmd.Parameters.AddWithValue("@entered_by", txtEnteredBy.Text)
            cmd.Parameters.AddWithValue("@last_name", txtLastName.Text)
            cmd.Parameters.AddWithValue("@first_name", txtFirstName.Text)
            cmd.Parameters.AddWithValue("@service_address", txtServiceAddress.Text)
            cmd.Parameters.AddWithValue("@service_city", txtServiceCity.Text)
            cmd.Parameters.AddWithValue("@service_state", txtServiceState.Text)
            cmd.Parameters.AddWithValue("@service_zip", txtServiceZip.Text)
            cmd.Parameters.AddWithValue("@mailing_address", txtMailAddress.Text)
            cmd.Parameters.AddWithValue("@mailing_city", txtMailCity.Text)
            cmd.Parameters.AddWithValue("@mailing_state", txtMailState.Text)
            cmd.Parameters.AddWithValue("@mailing_zip", txtMailZip.Text)
            cmd.Parameters.AddWithValue("@home_phone", txtHomePhone.Text)
            cmd.Parameters.AddWithValue("@cell_phone", txtCellPhone.Text)
            cmd.Parameters.AddWithValue("@email", txtEmail.Text)
            cmd.Parameters.AddWithValue("@acct_pkg", txtAcctPkg.Text)
            If String.IsNullOrEmpty(txtNMAppReceiptDate.Text) or Not IsDate(txtNMAppReceiptDate.Text) Then
                cmd.Parameters.AddWithValue("@nm_app_receipt_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@nm_app_receipt_date", txtNMAppReceiptDate.Text)
            End If
            If String.IsNullOrEmpty (txtSRPostMarkDate.Text) or Not IsDate(txtSRPostMarkDate.Text) Then
                cmd.Parameters.AddWithValue("@sr_postmark_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@sr_postmark_date", txtSRPostMarkDate.Text)    
            End If
            cmd.Parameters.AddWithValue("@rate", txtRate.Text)
            cmd.Parameters.AddWithValue("@system_type", txtSystemType.Text)
            cmd.Parameters.AddWithValue("@ac_system_rating", txtACSystemRating.Text)
            cmd.Parameters.AddWithValue("@dc_system_rating", txtDCSystemRating.Text)
            cmd.Parameters.AddWithValue("@solar_module_mfr", txtSolarModuleMfr.Text)
            cmd.Parameters.AddWithValue("@solar_module_num", txtSolarModuleNum.Text)
            If String.IsNullOrEmpty(txtModuleRating.Text) or Not IsNumeric(txtModuleRating.Text) Then
                mRating = 0
            Else
                mRating =  Convert.toDouble(txtModuleRating.Text)
            End If
            cmd.Parameters.AddWithValue("@module_rating", mRating)
            cmd.Parameters.AddWithValue("@inverter_rating", txtInverterRating.Text)
            If String.IsNullOrEmpty(txtNumberPanels.Text) or Not IsNumeric(txtNumberPanels.Text) Then
                nPanels = 0
            Else
                nPanels = convert.ToInt32(txtNumberPanels.Text)
            End If
            cmd.Parameters.AddWithValue("@number_panels", nPanels)
            sRating = mRating * nPanels
            cmd.Parameters.AddWithValue("@system_rating", sRating)
            If String.IsNullOrEmpty(txtEngineeringDate.Text) or Not IsDate(txtEngineeringDate.Text) Then
                cmd.Parameters.AddWithValue("@engineering_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@engineering_date", txtEngineeringDate.Text)
            End If
            If String.IsNullOrEmpty(txtW9Received.Text) or Not IsDate(txtW9Received.Text) Then
                cmd.Parameters.AddWithValue("@w9_received", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@w9_received", txtW9Received.Text)
            End If
            If String.IsNullOrEmpty(txtSA76Amount.Text) or Not IsNumeric(txtSA76Amount.Text) Then
                sa76Amount = 0
            Else
                sa76Amount = convert.toDouble(txtSA76Amount.Text)
            End If
            cmd.Parameters.AddWithValue("@sa76_amount", sa76Amount)
            If String.IsNullOrEmpty(txtSA76Recieved.Text) or Not IsDate(txtSA76Recieved.Text) Then
                cmd.Parameters.AddWithValue("@sa76_received", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@sa76_received", txtSA76Recieved.Text)
            End If
            If String.IsNullOrEmpty(txtSA76PaymentReceived.Text) or Not IsDate(txtSA76PaymentReceived.Text) Then
                cmd.Parameters.AddWithValue("@sa76_payment_received", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@sa76_payment_received", txtSA76PaymentReceived.Text)
            End If
            If String.IsNullOrEmpty(txtDetailReceiptsReceived.Text) or Not IsDate(txtDetailReceiptsReceived.Text) Then
                cmd.Parameters.AddWithValue("@detailed_receipts_received", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@detailed_receipts_received", txtDetailReceiptsReceived.Text)
            End If
            If String.IsNullOrEmpty(txtWarrantySheetReceived.Text) or Not IsDate(txtWarrantySheetReceived.Text) Then
                cmd.Parameters.AddWithValue("@warranty_sheet_received", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@warranty_sheet_received", txtWarrantySheetReceived.Text)
            End If
            If String.IsNullOrEmpty(txtPhotosReceived.Text) or Not IsDate(txtPhotosReceived.Text) Then
                cmd.Parameters.AddWithValue("@photos_received", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@photos_received", txtPhotosReceived.Text)
            End If
            If String.IsNullOrEmpty(txtLetterDate.Text) or Not IsDate(txtLetterDate.Text) Then
                cmd.Parameters.AddWithValue("@letter_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@letter_date", txtLetterDate.Text)
            End If
            If String.IsNullOrEmpty(txtDenialLetterDate.Text) or Not IsDate(txtDenialLetterDate.Text) Then
                cmd.Parameters.AddWithValue("@denial_letter_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@denial_letter_date", txtDenialLetterDate.Text)
            End If
            If String.IsNullOrEmpty(txtNMResubmittalDate.Text) or Not IsDate(txtNMResubmittalDate.Text) Then
                cmd.Parameters.AddWithValue("@nm_resubmittal_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@nm_resubmittal_date", txtNMResubmittalDate.Text)
            End If
            If String.IsNullOrEmpty(txtSRCommitmentDate.Text) or Not IsDate(txtSRCommitmentDate.Text)  Then
                cmd.Parameters.AddWithValue("@sr_commitment_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@sr_commitment_date", txtSRCommitmentDate.Text)
            End If
            If String.IsNullOrEmpty(txtSRCommitmentAmount.Text) or Not IsNumeric(txtSRCommitmentAmount.Text) Then
                cAmount = 0
            Else
                cAmount = convert.toDouble(txtSRCommitmentAmount.Text)
            End If
            cmd.Parameters.AddWithValue("@sr_commitment_amount", cAmount)
            If String.IsNullOrEmpty(txtCompletionDate.Text) or Not IsDate(txtCompletionDate.Text) Then
                cmd.Parameters.AddWithValue("@completion_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@completion_date", txtCompletionDate.Text)
            End If
            If String.IsNullOrEmpty(txtCommissionTestDate.Text) or Not IsDate(txtCommissionTestDate.Text) Then
                cmd.Parameters.AddWithValue("@commission_test_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@commission_test_date", txtCommissionTestDate.Text)
            End If
            If String.IsNullOrEmpty(txtOperationalDate.Text) or Not IsDate(txtOperationalDate.Text) Then
                cmd.Parameters.AddWithValue("@operational_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@operational_date", txtOperationalDate.Text)
            End If
            If String.IsNullOrEmpty(txtSRAffidavitDate.Text) or Not IsDate(txtSRAffidavitDate.Text) Then
                cmd.Parameters.AddWithValue("@sr_affidavit_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@sr_affidavit_date", txtSRAffidavitDate.Text)
            End If
            cmd.Parameters.AddWithValue("@azimuth", txtAzimuth.Text)
            cmd.Parameters.AddWithValue("@tilt", txtTilt.Text)
            If String.IsNullOrEmpty(txtAnnualSRec.Text) or Not IsNumeric(txtAnnualSRec.Text) Then
                sRec = 0
            Else
                sRec = convert.toDouble(txtAnnualSRec.Text)
            End If
            cmd.Parameters.AddWithValue("@annual_srec", sRec)
            If String.IsNullOrEmpty(txtSRPaidDate.Text) or Not IsDate(txtSRPaidDate.Text) Then
                cmd.Parameters.AddWithValue("@sr_paid_date", DBNull.Value)
            Else
                cmd.Parameters.AddWithValue("@sr_paid_date", txtSRPaidDate.Text)
            End If
            If String.IsNullOrEmpty(txtSRPaidAmount.Text) or Not IsNumeric(txtSRPaidAmount.Text) Then
                pAmount = 0
            Else
                pAmount = convert.toDouble(txtSRPaidAmount.Text)
            End If
            cmd.Parameters.AddWithValue("@sr_paid_amount", pAmount)
            cmd.Parameters.AddWithValue("@comments", txtComment.Text)
            cmd.Parameters.AddWithValue("@companyname", txtCompanyName.Text)
            cmd.Parameters.AddWithValue("@meterid", txtMeterID.Text)
            cmd.Parameters.AddWithValue("@metermanufacturer", txtMeterManufacturer.Text)

            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            cmd.Dispose()
            conn.Dispose()

            clearData()
            dsNetMeter.DataBind()
            gvData.DataBind()
            status = true
        Catch ex As Exception
            If conn.State <> ConnectionState.Open Then conn.Close()
            status = False
            lblStatusTop.Text = ex.Message
            lblStatusBottom.Text = ex.Message
        End Try
        Return status
    End Function

    Public Sub clearData()
        txtEnteredBy.Text = ""
        txtLastName.Text = ""
        txtFirstName.Text = ""
        txtServiceAddress.Text = ""
        txtServiceCity.Text = ""
        txtServiceState.Text = ""
        txtServiceZip.Text = ""
        txtMailAddress.Text = ""
        txtMailCity.Text = ""
        txtMailState.Text = ""
        txtMailZip.Text = ""
        txtHomePhone.Text = ""
        txtCellPhone.Text = ""
        txtEmail.Text = ""
        txtNMAppReceiptDate.Text = ""
        txtSRPostMarkDate.Text = ""
        txtRate.Text = ""
        txtAcctPkg.Text = ""
        txtSystemType.Text = ""
        txtACSystemRating.Text = ""
        txtDCSystemRating.Text = ""
        txtSolarModuleMfr.Text = ""
        txtSolarModuleNum.Text = ""
        txtInverterRating.Text = ""
        txtModuleRating.Text = ""
        txtNumberPanels.Text = ""
        txtSystemRating.Text = ""
        txtEngineeringDate.Text = ""
        txtW9Received.Text = ""
        txtSA76Amount.Text = ""
        txtSA76Recieved.Text = ""
        txtSA76PaymentReceived.Text = ""
        txtDetailReceiptsReceived.Text = ""
        txtWarrantySheetReceived.Text = ""
        txtPhotosReceived.Text = ""
        txtLetterDate.Text = ""
        txtDenialLetterDate.Text = ""
        txtNMResubmittalDate.Text = ""
        txtSRCommitmentDate.Text = ""
        txtSRCommitmentAmount.Text = ""
        txtCompletionDate.Text = ""
        txtCommissionTestDate.Text = ""
        txtOperationalDate.Text = ""
        txtSRAffidavitDate.Text = ""
        txtAzimuth.Text = ""
        txtTilt.Text = ""
        txtAnnualSRec.Text = ""
        txtSRPaidDate.Text = ""
        txtSRPaidAmount.Text = ""
        txtComment.Text = ""
        txtCompanyName.Text = ""
        txtMeterID.Text = ""
        txtMeterManufacturer.Text = ""
        lblStatusTop.Text = ""
        lblStatusBottom.Text = ""
    End Sub

    Public Sub rebuildQueue()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            conn.Open()
            'clear any queue data from rows without postmark date
            cmd.CommandText = "update netmeter set queue_id=NULL where controlrecord = 0 and netmeter.sr_postmark_date is null"
            cmd.ExecuteNonQuery()
            'rebuild queue with data containing postmark date
            Dim ds As New DataSet
            Dim daQueueInfo As New sqlDataAdapter("select ROW_NUMBER() OVER (ORDER BY netmeter.sr_postmark_date ASC)as queueid,netmeter.id from netmeter where controlrecord = 0 and netmeter.sr_postmark_date is not null", conn)
            daQueueInfo.Fill(ds, "QueueInfo")
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmd.CommandText = "update netmeter set queue_id=" & ds.Tables(0).Rows(i)("queueid") & " where id= " & ds.Tables(0).Rows(i)("id")
                cmd.ExecuteNonQuery()
            Next
            conn.Close()
            dsNetMeter.DataBind()
            gvData.DataBind()
        Catch
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub

    Protected Sub lbBuildQueue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbBuildQueue.Click
        rebuildQueue ()
    End Sub

    Protected Sub lbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAdd.Click
        hdnAction.Value = "insert"
        clearData()
        pnlDatas.Visible = False        
        pnlDetails.Visible = True
    End Sub

    Protected Sub lbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClose.Click
        pnlDetails.Visible = False
        clearData()
        pnlDatas.Visible = True
    End Sub

    Protected Sub lbUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUpdate.Click
        Dim success As Boolean = updateData()
        If success Then
            pnlDetails.Visible = False
            pnlDatas.Visible = True
        Else
            pnlDatas.Visible = False
            pnlDetails.Visible = true
        End If
    End Sub

    Protected Sub lbSearchParameters_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearchParameters.Click
        pnlDatas.Visible = False
        pnlSearch.Visible = True
    End Sub

    Protected Sub lbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearch.Click
        gvData.PageIndex = 0
        Show_Data()
        pnlDetails.Visible = False
        pnlSearch.Visible = False
        pnlDatas.Visible = True
    End Sub

    Protected Sub lbControlRecord_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbControlRecord.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader
        Try
            conn.Open()
            cmd.CommandText = "select top 1 * from netmeter where netmeter.controlrecord = 1"
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    txtControlRecordRebateCap.Text = dr("sr_commitment_amount").ToString
                End While
            Else
                createControlRecord ()
                txtControlRecordRebateCap.Text = "0.00"
            End If
            dr.Close()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
        pnlDatas.Visible = False
        pnlControlRecord.Visible = True
    End Sub

    Protected Sub lbControlRecordCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbControlRecordCancel.Click
        pnlDatas.Visible = True
        pnlControlRecord.Visible = False
    End Sub

    Protected Sub lbControlRecordUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbControlRecordUpdate.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim rcAmount As Decimal = 0
        Try
            conn.Open
            cmd.CommandText = "update netmeter set sr_commitment_date=@sr_commitment_date,sr_commitment_amount=@sr_commitment_amount where controlrecord = 1"
            cmd.Parameters.AddWithValue("@sr_commitment_date", DateTime.Now)
            If String.IsNullOrEmpty(txtControlRecordRebateCap.Text) or Not IsNumeric(txtControlRecordRebateCap.Text) Then
                rcAmount = 0
            Else
                rcAmount = convert.toDouble(txtControlRecordRebateCap.Text)
            End If
            cmd.Parameters.AddWithValue("@sr_commitment_amount", rcAmount)
            cmd.ExecuteNonQuery()  'udpate control record
            conn.Close()
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
            pnlDatas.Visible = True
            pnlControlRecord.Visible = False
    End Sub

    Protected Sub lbOutputExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbOutputExcel.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim dTable As New DataTable
        Try
            conn.Open()
            Dim sql As String = "select queue_id,entered_by,companyname,first_name,last_name,service_address,service_city,service_state,service_zip," & _
                                "home_phone,cell_phone,email,mailing_address,mailing_city,mailing_state,mailing_zip,acct_pkg,rate,meterid,metermanufacturer," & _
                                "nm_app_receipt_date,sr_postmark_date,system_type,ac_system_rating,dc_system_rating,solar_module_mfr,solar_module_num," & _
                                "module_rating,inverter_rating,number_panels,system_rating,engineering_date,w9_received,sa76_amount,sa76_received," & _
                                "sa76_payment_received,detailed_receipts_received,warranty_sheet_received,photos_received,letter_date,denial_letter_date," & _
                                "nm_resubmittal_date,sr_commitment_date,sr_commitment_amount,completion_date,commission_test_date,sr_affidavit_date,azimuth," & _
                                "tilt,operational_date,annual_srec,sr_paid_date,sr_paid_amount,comments from netmeter where controlrecord = 0"
            Dim daTableInfo As New sqlDataAdapter(sql, conn)
            daTableInfo.Fill(dTable)
            conn.Close()

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "Applications-" & DateTime.Now.ToString("yyyy-MM-dd") & ".xls"))
            HttpContext.Current.Response.ContentType = "application/ms-excel"
            If dTable IsNot Nothing Then
				For Each dc As DataColumn In dTable.Columns
                    'If dc.ColumnName = "controlrecord" Or dc.ColumnName = "id" Then 'skip id and controlrecord column
                    'Else
                        HttpContext.Current.Response.Write(dc.ColumnName & vbTab)
                    'End If
				Next
				Response.Write(System.Environment.NewLine)
				For Each dr As DataRow In dTable.Rows
					For i As Integer = 0 To dTable.Columns.Count - 1
                        'If i = 0 or i = 1 Then 'skip id and controlrecord column
                        'Else
                            HttpContext.Current.Response.Write(dr(i).ToString() & vbTab)
                        'End If
					Next
					HttpContext.Current.Response.Write(vbLf)
				Next
				HttpContext.Current.Response.[End]()
			End If
        Catch
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub

    Protected Sub dsNetMeter_Selected(sender As Object, e As SqlDataSourceStatusEventArgs) Handles dsNetMeter.Selected
        If Not e.Exception Is Nothing Then
            If e.Exception.Message.Contains("Invalid object name") Then  'table doesnt exist
                createTable()
                createControlRecord()
            End If
        End If
    End Sub

    Public Sub createControlRecord()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            conn.Open
            cmd.CommandText = "insert into netmeter (controlrecord,sr_commitment_date,sr_commitment_amount,comments) VALUES (1,GETDATE(),0.00,'control record uses sr_commitment_amount to store rebate cap')"
            cmd.ExecuteNonQuery()  'insert control record
            conn.Close()
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Public Sub createTable()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            cmd.CommandText = "CREATE TABLE dbo.netmeter(" & _
                "id int IDENTITY(1,1) NOT NULL," & _
                "controlrecord bit not null," & _
                "queue_id int null," & _
                "entered_by varchar(15) null," & _
                "first_name varchar(50) null," & _
                "last_name varchar(50) null," & _
                "service_address varchar(50) null," & _
                "service_city varchar(50) null," & _
                "service_state varchar(02) null," & _
                "service_zip varchar(10) null," & _
                "mailing_address varchar(50) null," & _
                "mailing_city varchar(50) null," & _
                "mailing_state varchar(02) null," & _
                "mailing_zip varchar(10) null," & _
                "home_phone varchar(12) null," & _
                "cell_phone varchar(12) null," & _
                "email varchar(50) null," & _
                "acct_pkg varchar(15) null," & _
                "sr_postmark_date datetime null," & _
                "nm_app_receipt_date datetime null," & _
                "rate varchar(10) null," & _
                "system_type varchar(10) null," & _
                "ac_system_rating varchar(10) null," & _
                "dc_system_rating varchar(10) null," & _
                "solar_module_mfr varchar(30) null," & _
                "solar_module_num varchar(30) null," & _
                "module_rating decimal (18, 2) null," & _
                "inverter_rating varchar(30) null," & _
                "number_panels int null," & _
                "system_rating decimal (18,2) null," & _
                "engineering_date datetime null," & _
                "letter_date datetime null," & _
                "nm_resubmittal_date datetime null," & _
                "denial_letter_date datetime null," & _
                "sr_commitment_date datetime null," & _
                "sr_commitment_amount decimal (18, 2)," & _
                "completion_date datetime null," & _
                "commission_test_date datetime null," & _
                "operational_date datetime null," & _
                "sr_affidavit_date datetime null," & _
                "annual_srec decimal (18,2) null," & _
                "sr_paid_date datetime null," & _
                "sr_paid_amount decimal (18,2) null," & _
                "comments varchar(256) null," & _
                "w9_received datetime null," & _
                "sa76_amount decimal (18,2) null," & _
                "sa76_received datetime null," & _
                "sa76_payment_received datetime null," & _
                "detailed_receipts_received datetime null," & _
                "warranty_sheet_received datetime null," & _
                "photos_received datetime null," & _
                "azimuth varchar(20) null," & _
                "tilt varchar(20) null," & _
                "companyname varchar(50) null," & _
                "meterid varchar(10) null," & _
                "metermanufacturer varchar(50) null," & _
                "CONSTRAINT pk_netmeter PRIMARY KEY CLUSTERED (id ASC))"
            conn.Open
            cmd.ExecuteNonQuery()  'create the table
            conn.Close
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

End Class
