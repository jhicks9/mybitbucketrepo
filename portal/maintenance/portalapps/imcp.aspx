<%@ Page Title="" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="imcp.aspx.vb" Inherits="maintenance_portalapps_icmp" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class='input-group date datetimepickerclass'>
                        <asp:TextBox ID="txtBegDate" runat="server" CssClass="form-control" />
                        <asp:CompareValidator id="begdateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtBegDate" ErrorMessage="mm/dd/yyyy" Display="Dynamic" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class='input-group date datetimepickerclass'>
                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" />
                        <asp:CompareValidator id="enddateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtEndDate" ErrorMessage="mm/dd/yyyy" Display="Dynamic" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <asp:UpdatePanel ID="updateDateRange" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <asp:LinkButton ID="lbDateRange" runat="server" class="btn btn-primary"><span class="fa fa-bar-chart"></span>&nbsp;Show Chart</asp:LinkButton>
                        <asp:label runat="server" id="lblStatus" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <asp:Chart ID="Chart1" runat="server" class="img-responsive form-control" Width="1280px" Height="450px" >
                            <Legends>
                                <asp:Legend Name="Default"></asp:Legend>
                            </Legends>
                            <Titles>
                                <asp:Title Name="Default Title" Text="Custom Range of Denials">
                                </asp:Title>
                                <asp:Title Name="sub Title" Text="Each line represents one type of denial over the selected date range">
                                </asp:Title>
                            </Titles>
                            <Series>
                                <asp:series XValueType="Date" Name="Deny icmp outside" 
                                    BorderColor="120, 64, 64, 64" Color="Red" YValueType="Int32" 
                                    font="Trebuchet MS, 8.25pt, style=Bold" ChartType="line" 
                                    ShadowOffset="1" MarkerBorderColor="64, 64, 64"></asp:series>
                                <asp:series XValueType="Date" Name="Deny icmp inside" 
                                    BorderColor="120, 64, 64, 64" Color="Violet" YValueType="Int32" 
                                    font="Trebuchet MS, 8.25pt, style=Bold" ChartType="line" 
                                    ShadowOffset="1" MarkerBorderColor="64, 64, 64"></asp:series>  
                                <asp:series XValueType="Date" Name="Deny tcp outside" 
                                    BorderColor="120, 64, 64, 64" Color="Blue" YValueType="Int32" 
                                    font="Trebuchet MS, 8.25pt, style=Bold" ChartType="line" 
                                    ShadowOffset="1" MarkerBorderColor="64, 64, 64"></asp:series>
                                <asp:series XValueType="Date" Name="Deny tcp inside" 
                                    BorderColor="120, 64, 64, 64" Color="Lightblue" YValueType="Int32" 
                                    font="Trebuchet MS, 8.25pt, style=Bold" ChartType="line" 
                                    ShadowOffset="1" MarkerBorderColor="64, 64, 64"></asp:series>
                                <asp:series XValueType="Date" Name="Deny udp outside" 
                                    BorderColor="120, 64, 64, 64" Color="Green" YValueType="Int32" 
                                    font="Trebuchet MS, 8.25pt, style=Bold" ChartType="line" 
                                    ShadowOffset="1" MarkerBorderColor="64, 64, 64"></asp:series>
                                <asp:series XValueType="Date" Name="Deny udp inside" 
                                    BorderColor="120, 64, 64, 64" Color="LightGreen" YValueType="Int32" 
                                    font="Trebuchet MS, 8.25pt, style=Bold" ChartType="line" 
                                    ShadowOffset="1" MarkerBorderColor="64, 64, 64"></asp:series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                    <Area3DStyle Enable3D="False" LightStyle="Realistic" />
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

                    
        <asp:UpdatePanel ID="updateDailyChart" runat="server">
            <contenttemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <asp:LinkButton ID="lbDailyChart" runat="server" class="btn btn-primary"><span class="fa fa-bar-chart"></span>&nbsp;Daily Chart</asp:LinkButton>
                    </div>
                </div>
            </contenttemplate>
        </asp:UpdatePanel>

        <div class="row">
            <div class="col-lg-12">
                <asp:Chart ID="Chart2" runat="server" class="img-responsive form-control" Width="1280px" Height="450px" Visible="false" >
                    <Legends>
                        <asp:Legend Name="Default"></asp:Legend>
                    </Legends>
                    <Titles>
                        <asp:Title Name="Default Title" Text="Number of Denials">
                        </asp:Title>
                        <asp:Title Name="sub Title" Text="Each data point represents one denial entry in the log">
                        </asp:Title>
                    </Titles>
                    <Series>
                        <asp:series XValueType="String" Name="Deny icmp outside" BorderColor="64, 64, 64" 
                            Color="Red" YValueType="Int32" ChartType="column" IsValueShownAsLabel="false" BorderWidth="2"
                            font="Trebuchet MS, 8.25pt, style=Bold" ShadowOffset="1">
                        </asp:series>  
                        <asp:series XValueType="String" Name="Deny icmp inside" BorderColor="64, 64, 64" 
                            Color="Violet" YValueType="Int32" ChartType="column" IsValueShownAsLabel="false" BorderWidth="2"
                            font="Trebuchet MS, 8.25pt, style=Bold" ShadowOffset="1">
                        </asp:series> 
                        <asp:series XValueType="String" Name="Deny tcp outside" BorderColor="64, 64, 64" 
                            Color="Blue" YValueType="Int32" ChartType="column" IsValueShownAsLabel="false" BorderWidth="2"
                            font="Trebuchet MS, 8.25pt, style=Bold" ShadowOffset="1">
                        </asp:series>  
                        <asp:series XValueType="String" Name="Deny tcp inside" BorderColor="64, 64, 64" 
                            Color="lightblue" YValueType="Int32" ChartType="column" IsValueShownAsLabel="false" BorderWidth="2"
                            font="Trebuchet MS, 8.25pt, style=Bold" ShadowOffset="1">
                        </asp:series> 
                        <asp:series XValueType="String" Name="Deny udp outside" BorderColor="64, 64, 64" 
                            Color="Green" YValueType="Int32" ChartType="column" IsValueShownAsLabel="false" BorderWidth="2"
                            font="Trebuchet MS, 8.25pt, style=Bold" ShadowOffset="1">
                        </asp:series>  
                        <asp:series XValueType="String" Name="Deny udp inside" BorderColor="64, 64, 64" 
                            Color="lightgreen" YValueType="Int32" ChartType="column" IsValueShownAsLabel="false" BorderWidth="2"
                            font="Trebuchet MS, 8.25pt, style=Bold" ShadowOffset="1">
                        </asp:series>                              
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <Area3DStyle Enable3D="False" LightStyle="Realistic" />
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
        </div>

        <div class="modal fade" id="DayDetail" tabindex="-1" role="dialog" aria-labelledby="DayDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class='input-group date datetimepickerclass'>
                                        <asp:TextBox ID="txtSearchDay" runat="server" CssClass="form-control" />

                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvSearchDay" runat="server" ControlToValidate="txtSearchDay"
                                            Display="Dynamic" ErrorMessage="required." SetFocusOnError="True" ValidationGroup="searchDay" />
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <asp:LinkButton ID="lbSearchDay" runat="server" class="btn btn-primary" CausesValidation="true" ValidationGroup="searchDay"><span class="fa fa-bar-chart"></span>&nbsp;Show Chart</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- DayDetail -->
      </asp:Panel><!-- pnlAuthorized -->

    <asp:SqlDataSource ID="dsDenials" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
        SelectCommand="SELECT log_id FROM log_data_for_charts WHERE log_date >= @begdate AND log_date <= @enddate">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtBegDate" Type="String" Name="begDate" PropertyName="Text"/>
            <asp:ControlParameter ControlID="txtEndDate" Type="String" Name="endDate" PropertyName="Text"/>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
    
<asp:Content ID="Content1" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {   
                $(".datetimepickerclass").datetimepicker({ format: 'MM/DD/YYYY' });
            });
        </script>
</asp:Content>