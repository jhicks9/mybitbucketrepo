﻿<%@ Page Title="Vulnerable Software Report" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="vslist.aspx.vb" Inherits="Maintenance_PortalApps_vslist" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-12 ">
                <p class="well-sm bg-info text-center"><strong><asp:Label ID="lblPageTitle" runat="server" Text="Vulnerable Software Report" /></strong></p>  
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 text-center">
                <asp:LinkButton ID="lbReport" runat="server" Text="Vulnerable Software Report" CssClass="btn btn-default" />
            </div>
            <div class="col-lg-6 text-center">
                <asp:LinkButton ID="lbSoftwareList" runat="server" Text="Installed Software List" CssClass="btn btn-default"/>
            </div>
        </div>
        
        <asp:Panel ID="pnlReport" runat="server">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gvReport" runat="server" AllowPaging="true" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundField DataField="title" HeaderText="Title" />
                                <asp:BoundField DataField="version" HeaderText="Version" Visible="false" />
                                <asp:BoundField DataField="company" HeaderText="Company" />
                                <asp:BoundField DataField="id" HeaderText="ID" />
                                <asp:BoundField DataField="published" HeaderText="Published" DataFormatString="{0:d}" HtmlEncode="false" />
                                <asp:BoundField DataField="modified" HeaderText="Last Modified" DataFormatString="{0:d}" HtmlEncode="false" />
                                <asp:BoundField DataField="summary" HeaderText="Summary" HtmlEncode="false" />
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <EmptyDataTemplate>No data available</EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 "><asp:Label ID="lblStatus" runat="server" /></div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbUpdateXML" runat="server" Text="Update" CssClass="btn btn-primary" />&nbsp;<asp:Label ID="lblxmlDate" runat="server" />
                </div>
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbOutputExcel" runat="server" CausesValidation="False" Text='Output to Excel' CssClass="btn btn-info" />
                </div>
            </div>
        </asp:Panel><%--end pnlReport--%>

        <asp:Panel ID="pnlSoftwareList" runat="server" Visible="false">
            <asp:HiddenField ID="hdnID" runat="server" />
            <asp:HiddenField ID="hdnAction" runat="server" />
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <asp:GridView ID="gvSoftware" runat="server" DataSourceID="dsSoftware" DataKeyNames="id" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AllowPaging="true" >
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderTemplate />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbSoftwareEdit" runat="server" CausesValidation="False" CommandArgument='<%#Eval("id") %>' CommandName="Select" ToolTip="Detail" Text='Edit' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate><asp:Label ID="lblTitle" runat="server" Text='<%#Eval("title")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="cpe">
                                    <ItemTemplate><asp:Label ID="lblCPE" runat="server" Text='<%#Eval("cpe")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Updated By">
                                    <ItemTemplate><asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Eval("userid")%>' /></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Last update">
                                    <ItemTemplate><asp:Label ID="lblLastUpdate" runat="server" Text='<%#String.Format("{0:MM/dd/yy hh:mm}",Eval("lastupdate"))%>' /></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsSoftware" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                            SelectCommand="select vslist.id,vslist.title,vslist.version,vslist.cpe,vslist.userid,vslist.lastupdate from vslist where vslist.controlrecord = 0" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbUpdateCPE" runat="server" CausesValidation="False" Text="Update CPE data" CssClass="btn btn-primary"/>&nbsp;<asp:Label ID="lblCPEDate" runat="server" />
                </div>
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbAddSoftware" runat="server" CausesValidation="False" Text='Add Software' CssClass="btn btn-info" />
                </div>
            </div>
        </asp:Panel><%--end pnlSoftwareList--%>

        <asp:Panel ID="pnlSoftwareDetails" runat="server" Visible="False">
            <div class="row">
                <div class="col-lg-12 ">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td colspan="2" class="field-label col-xs-12 info text-center"><b>Edit Installed Software</b></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtTitle">Title</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="lblCPE">cpe</label></td>
                            <td class="col-md-9"><asp:label ID="lblCPE" runat="server" CssClass="form-control no-border" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="table-responsive">
                                <asp:GridView ID="gvCPESearchResults" runat="server" SkinID="MinimalGridView" AllowPaging="true" PageSize="25" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-striped table-bordered table-hover">
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <HeaderTemplate />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelectResult" runat="server" CausesValidation="False" CommandArgument='<%#Eval("cpe") %>' commandSource='<%#Eval("title") %>' CommandName="Select" Text='<%#Eval("title") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><asp:LinkButton ID="lbSearchCPE" runat="server" CausesValidation="False" Text='Search' CssClass="btn btn-default" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:LinkButton ID="lbSoftwareUpdate" runat="server" CausesValidation="True" Text="Update" CssClass="btn btn-primary" />
                    <asp:LinkButton ID="lbSoftwareClose" runat="server" CausesValidation="False" Text="Cancel" CssClass="btn btn-default" />
                    <asp:LinkButton ID="lbSoftwareDelete" runat="server" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='return confirm("Are you sure you want to delete the record?");' />
                </div>
            </div>
        </asp:Panel><%--end pnlSoftwareDetails--%>
    </asp:Panel>
</asp:Content>