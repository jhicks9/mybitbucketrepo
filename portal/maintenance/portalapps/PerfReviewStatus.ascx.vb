﻿Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports System.Data.SqlTypes
Imports System.IO

Partial Class Maintenance_PortalApps_PerfReviewStatus
    Inherits System.Web.UI.UserControl
    Dim ps As New portalSecurity
    Dim username As String
    '8/27/2012 - replace SYSADM.PS_JOB with SYSADM.EDE_PORTAL_PS_JOB_VW 
    '8/27/2012 - replace SYSADM.PS_EMPLOYEES with SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            username = Nothing

            If Not IsPostBack Then
                Dim EmptyListM As New ListItem("--Choose Manager--", "")
                ddlManagers.Items.Add(EmptyListM)
                username = Request.ServerVariables("LOGON_USER")
                username = Right(username, Len(username) - InStr(username, "\"))
                ddlReviewYear.Items.Clear()

                Dim ThisYear = Now.Year
                Dim firstYear = Year("1/1/2009")
                Dim x As Int32
                For x = firstYear To ThisYear + 1
                    ddlReviewYear.Items.Add(New ListItem(x.ToString, x))
                Next

                ddlReportYear.Items.Add(New ListItem("--Select Year--", 0))

                Dim y As Int32
                For y = firstYear To ThisYear + 1
                    ddlReportYear.Items.Add(New ListItem(y.ToString, y))
                Next
                'Else
                '    If ddlManagers.SelectedValue <> Nothing Then
                '        Dim userArray As Array = Split(ddlManagers.SelectedItem.Text, ",")
                '        username = Left(userArray(1), 1).ToUpper & userArray(0)
                '    End If
            End If
            lblMgr.Text = username

            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)

                Const SQLPSLookup As String = "SELECT A.EMPLID, A.NAME, A.FIRST_NAME, A.LAST_NAME, A.POSITION_NBR, A.JOBTITLE " & _
                     " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A" & _
                     " WHERE ( A.EFFDT = " & _
                     " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                     " AND A_ED.EFFDT <= SYSDATE) " & _
                     " AND A.EFFSEQ = " & _
                     " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                     " AND A.EFFDT = A_ES.EFFDT)  " & _
                     " AND SUBSTR(A.POSITION_NBR,1,2) = :code) " & _
                     " ORDER BY A.NAME ASC"

                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":code", "00")
                'Response.Write(SQLPSLookup)
                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader

                If Not IsPostBack Then
                    Dim fName As String = Nothing
                    Dim lName As String = Nothing
                    While myPSLookup.Read
                        ddlManagers.Items.Add(New ListItem(myPSLookup("NAME"), myPSLookup("EMPLID")))
                        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                            Const SQLLookup As String = "SELECT PS_EMPLID FROM PerfEmployee WHERE (PS_EMPLID = @PS_EMPLID)"
                            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                            myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", myPSLookup("EMPLID"))
                            myConnection.Open()
                            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                            If myLookup.Read Then
                                'may incorporate update based on differences in the data bases

                            Else
                                fName = myPSLookup("FIRST_NAME")
                                lName = myPSLookup("LAST_NAME")
                                Using myInsertConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                    Const SQLInsert As String = "Insert into [PerfEmployee] ([PS_EMPLID],[Employee_First_Name],[Employee_Last_Name],[Employee_Title],[Salaried]) VALUES (@PS_EMPLID,@Employee_First_Name,@Employee_Last_Name,@Employee_Title,@Salaried)"
                                    Dim myInsertCommand As New SqlCommand(SQLInsert, myInsertConnection)
                                    myInsertCommand.Parameters.AddWithValue("@PS_EMPLID", myPSLookup("EMPLID"))
                                    myInsertCommand.Parameters.AddWithValue("@Employee_First_Name", fName)
                                    myInsertCommand.Parameters.AddWithValue("@Employee_Last_Name", lName)
                                    myInsertCommand.Parameters.AddWithValue("@Employee_Title", myPSLookup("JOBTITLE"))
                                    myInsertCommand.Parameters.AddWithValue("@Salaried", True)
                                    myInsertConnection.Open()
                                    myInsertCommand.ExecuteNonQuery()
                                    myInsertConnection.Close()
                                    'notification??
                                End Using
                                myLookup.Close()
                                myConnection.Close()
                            End If
                        End Using
                    End While
                End If

                myPSLookup.Close()
                myPSConnection.Close()
            End Using
        Catch
            lblMgr.Text += " " & Err.Description
        End Try
    End Sub
    Public Sub populate_ddlSubordinate()
        'get position code********************************************        

        Try

            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
                Const SQLPSLookup As String = "SELECT A.EMPLID, A.POSITION_NBR " & _
                     " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A " & _
                     " WHERE ( A.EFFDT = " & _
                     " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED " & _
                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ED.EMPL_RCD " & _
                     " AND A_ED.EFFDT <= SYSDATE) " & _
                     " AND A.EFFSEQ = " & _
                     " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES " & _
                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                     " AND A.EMPL_RCD = A_ES.EMPL_RCD " & _
                     " AND A.EFFDT = A_ES.EFFDT)  " & _
                     " AND (A.EMPLID = :EMPLID))"


                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":EMPLID", ddlManagers.SelectedValue)

                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader

                'Check to see if signed on employee is a manager using position number*****************************
                If myPSLookup.Read Then

                    'lblMgr.Text = myPSLookup("DEPTID") & " - " & myPSLookup("DESCR") & " - " & myPSLookup("POSITION_NBR") & " " & myPSLookup("NAME") & " - " & myPSLookup("REPORTS_TO") & "<br/>"

                    Const SQLPSLookupMgr As String = "SELECT A.EMPLID, A.NAME" & _
                        " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A" & _
                        " WHERE ( A.EFFDT =" & _
                        " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED" & _
                                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                        " AND A.EMPL_RCD = A_ED.EMPL_RCD" & _
                        " AND A_ED.EFFDT <= SYSDATE)" & _
                        " AND A.EFFSEQ =" & _
                        " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES" & _
                                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                        " AND A.EMPL_RCD = A_ES.EMPL_RCD" & _
                        " AND A.EFFDT = A_ES.EFFDT)" & _
                        " AND A.REPORTS_TO = :POSNUM )" & _
                        " ORDER BY A.NAME ASC"

                    '********end search******************************************

                    Dim myPSLookupMgrCommand As New OracleCommand(SQLPSLookupMgr, myPSConnection)
                    myPSLookupMgrCommand.Parameters.AddWithValue(":POSNUM", myPSLookup("POSITION_NBR"))
                    Dim myPSLookupMgr As OracleDataReader = myPSLookupMgrCommand.ExecuteReader
                    'if there are subordinates list them in a select box****************************

                    ddlSubordinate.Items.Clear()
                    Dim EmptyList As New ListItem("--Choose Employee--", "")
                    ddlSubordinate.Items.Add(EmptyList)
                    ddlReviewYear.Items.Clear()

                    Dim ThisYear = Now.Year
                    Dim firstYear = Year("1/1/2009")
                    Dim x As Int32
                    For x = firstYear To ThisYear + 1
                        ddlReviewYear.Items.Add(New ListItem(x.ToString, x))
                    Next


                    While myPSLookupMgr.Read
                        Dim EmployeesList As New ListItem(myPSLookupMgr("NAME"), myPSLookupMgr("EMPLID"))
                        ddlSubordinate.Items.Add(EmployeesList)
                    End While

                    myPSLookupMgr.Close()
                End If
                myPSLookup.Close()
                myPSConnection.Close()
            End Using

        Catch
            lblEmployeeInfo.Text += "populate_dropdownlist1: " & Err.Description
        End Try
        '****************************************************************
    End Sub

    Public Sub build_status()
        Try
            Using myPSConnection As New OracleConnection(ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
                Const SQLPSLookup As String = "SELECT A.EMPLID,A.POSITION_NBR" & _
                    " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A" & _
                    " WHERE ( A.EFFDT =" & _
                    " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED" & _
                                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                    " AND A.EMPL_RCD = A_ED.EMPL_RCD" & _
                    " AND A_ED.EFFDT <= SYSDATE)" & _
                    " AND A.EFFSEQ =" & _
                    " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES" & _
                                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                    " AND A.EMPL_RCD = A_ES.EMPL_RCD" & _
                    " AND A.EFFDT = A_ES.EFFDT)" & _
                    " AND (A.EMPLID = :EMPLID))"

                Dim myPSLookupCommand As New OracleCommand(SQLPSLookup, myPSConnection)
                myPSLookupCommand.Parameters.AddWithValue(":EMPLID", ddlManagers.SelectedValue)
                myPSConnection.Open()
                Dim myPSLookup As OracleDataReader = myPSLookupCommand.ExecuteReader
                'Check to see if signed on employee is a manager using position number*****************************
                If myPSLookup.Read Then

                    lblMgr.Text = username 'myPSLookup("DEPTID") & " - " & myPSLookup("DESCR") & " - " & myPSLookup("POSITION_NBR") & " " & myPSLookup("NAME") & " - " & myPSLookup("REPORTS_TO") & "<br/>"

                    Const SQLPSLookupMgr As String = "SELECT A.EMPLID" & _
                        " FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A" & _
                        " WHERE ( A.EFFDT =" & _
                        " (SELECT MAX(A_ED.EFFDT) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ED" & _
                                " WHERE(A.EMPLID = A_ED.EMPLID)" & _
                        " AND A.EMPL_RCD = A_ED.EMPL_RCD" & _
                        " AND A_ED.EFFDT <= SYSDATE)" & _
                        " AND A.EFFSEQ =" & _
                        " (SELECT MAX(A_ES.EFFSEQ) FROM SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW A_ES" & _
                                " WHERE(A.EMPLID = A_ES.EMPLID)" & _
                        " AND A.EMPL_RCD = A_ES.EMPL_RCD" & _
                        " AND A.EFFDT = A_ES.EFFDT)" & _
                        " AND A.REPORTS_TO = :POSNUM)" & _
                        " ORDER BY A.NAME ASC"

                    '********end search******************************************

                    Dim myPSLookupMgrCommand As New OracleCommand(SQLPSLookupMgr, myPSConnection)
                    myPSLookupMgrCommand.Parameters.AddWithValue(":POSNUM", myPSLookup("POSITION_NBR"))
                    Dim myPSLookupMgr As OracleDataReader = myPSLookupMgrCommand.ExecuteReader
                    'if there are subordinates list them in a select box****************************

                    Dim EMPIDlist As String = Nothing
                    Dim x As Int32 = 0
                    lblEmployeeInfo.Text = Nothing
                    While myPSLookupMgr.Read

                        If x = 0 Then
                            EMPIDlist += myPSLookupMgr("EMPLID")
                        Else
                            EMPIDlist += "," & myPSLookupMgr("EMPLID")
                        End If
                        x = x + 1

                    End While
                    'lblEmployeeInfo.Text += EMPIDlist
                    myPSLookupMgr.Close()

                    Dim sqlSched As String = Nothing
                    If x > 0 Then
                        sqlSched = "SELECT B.Employee_First_Name, B.Employee_Last_Name, B.Employee_Title, A.Review_Date1, A.Review_Date2, A.Review_Date3, A.Final_Review_Date, A.PS_EMPLID,A.Review_Year" & _
                                   " FROM PerfReviewDates AS A INNER JOIN PerfEmployee AS B ON A.PS_EMPLID = B.PS_EMPLID" & _
                                   " WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID in (" & EMPIDlist & ") AND A.Review_Year = " & ddlReviewYear.SelectedValue & " AND A.Review_Closed = '" & ddlClosed.SelectedValue & "')"

                        'lblEmployeeInfo.Text += sqlSched
                        SqlSchedules.SelectCommand = sqlSched
                        SqlSchedules.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                        gvSchedules.DataSource = SqlSchedules
                        gvSchedules.DataBind()
                    End If
                End If
                myPSLookup.Close()
                myPSConnection.Close()
            End Using

        Catch
            lblEmployeeInfo.Text += "build_status: " & Err.Description
        End Try
        '****************************************************************

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        build_status()

    End Sub


    Protected Sub ddlManagers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlManagers.SelectedIndexChanged
        dvManagerSignon.Visible = True
        If ddlManagers.SelectedIndex <> 0 Then
            lblManager.Text = ddlManagers.SelectedItem.Text
        Else
            lblManager.Text = "Manager not selected"
        End If
        populate_ddlSubordinate()
    End Sub

    Protected Sub gvSchedules_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSchedules.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Text = "status"
            e.Row.Cells(1).Text = "First Name"
            e.Row.Cells(2).Text = "Last Name"
            e.Row.Cells(3).Text = "Title"
            e.Row.Cells(4).Text = "Schd. Review 1"
            e.Row.Cells(5).Text = "Schd. Review 2"
            e.Row.Cells(6).Text = "Schd. Review 3"
            e.Row.Cells(7).Text = "Schd. Final Review"
            e.Row.Cells(8).Text = "Employee ID"
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            'Bug in conversion from string to date for null values in gridview, review the null date value "1/1/1900"
            Try
                Dim qcount As Int32 = 0
                Dim calYear As Int32 = ddlReviewYear.SelectedValue
                e.Row.Cells(0).ForeColor = Drawing.Color.Green
                e.Row.Cells(4).Text = CType(e.Row.Cells(4).Text, Date).ToShortDateString
                e.Row.Cells(5).Text = CType(e.Row.Cells(5).Text, Date).ToShortDateString
                e.Row.Cells(6).Text = CType(e.Row.Cells(6).Text, Date).ToShortDateString
                e.Row.Cells(7).Text = CType(e.Row.Cells(7).Text, Date).ToShortDateString
                e.Row.Cells(4).ForeColor = Drawing.Color.Green
                e.Row.Cells(5).ForeColor = Drawing.Color.Green
                e.Row.Cells(6).ForeColor = Drawing.Color.Green
                e.Row.Cells(7).ForeColor = Drawing.Color.Green

                If CType(e.Row.Cells(5).Text, Date) = "1/1/1900" Then
                    e.Row.Cells(5).Text = "unscheduled"
                    e.Row.Cells(5).ForeColor = Drawing.Color.Purple
                End If

                If CType(e.Row.Cells(6).Text, Date) = "1/1/1900" Then
                    e.Row.Cells(6).Text = "unscheduled"
                    e.Row.Cells(6).ForeColor = Drawing.Color.Purple
                End If

                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT B.Employee_First_Name,B.Employee_Last_Name,B.Employee_Title,A.Review_Date1,A.Review_Date2,A.Review_Date3,A.Final_Review_Date,B.PS_EMPLID FROM PerfReviewDates A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = @PS_EMPLID AND A.Review_Year = @Review_Year)"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", e.Row.Cells(8).Text)
                    myLookupCommand.Parameters.AddWithValue("@Review_Year", calYear)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
                    'Dim img As Image = e.Row.FindControl("img1")
                    Dim closed As Boolean
                    Dim gvemplid As Int32 = e.Row.Cells(8).Text
                    closed = flag_if_closed(calYear, gvemplid)
                    If myLookup.Read Then
                        qcount += 1

                        If CType(e.Row.Cells(4).Text, DateTime) <= Now And myLookup("Review_Date1") = "1/1/1900" Then
                            e.Row.Cells(4).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red 'img.ImageUrl = "~/images/flag_red.png"
                        End If

                        If e.Row.Cells(5).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(5).Text, DateTime) <= Now And myLookup("Review_Date2") = "1/1/1900" Then
                                e.Row.Cells(5).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If

                        If e.Row.Cells(6).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(6).Text, DateTime) <= Now And myLookup("Review_Date3") = "1/1/1900" Then
                                e.Row.Cells(6).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If

                        If CType(e.Row.Cells(7).Text, DateTime) <= Now And myLookup("Final_Review_Date") = "1/1/1900" Then
                            e.Row.Cells(7).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If

                        If closed Then
                            e.Row.Cells(0).ForeColor = Drawing.Color.Black
                            e.Row.Cells(4).ForeColor = Drawing.Color.Black
                            e.Row.Cells(5).ForeColor = Drawing.Color.Black
                            e.Row.Cells(6).ForeColor = Drawing.Color.Black
                            e.Row.Cells(7).ForeColor = Drawing.Color.Black
                        End If
                    Else
                        If CType(e.Row.Cells(4).Text, DateTime) <= Now Then
                            e.Row.Cells(4).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If
                        If e.Row.Cells(5).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(5).Text, DateTime) <= Now Then
                                e.Row.Cells(5).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If
                        If e.Row.Cells(6).Text <> "unscheduled" Then
                            If CType(e.Row.Cells(6).Text, DateTime) <= Now Then
                                e.Row.Cells(6).ForeColor = Drawing.Color.Maroon
                                e.Row.Cells(0).ForeColor = Drawing.Color.Red
                            End If
                        End If
                        If CType(e.Row.Cells(7).Text, DateTime) <= Now Then
                            e.Row.Cells(7).ForeColor = Drawing.Color.Maroon
                            e.Row.Cells(0).ForeColor = Drawing.Color.Red
                        End If

                        If closed Then
                            e.Row.Cells(0).ForeColor = Drawing.Color.Black
                            e.Row.Cells(4).ForeColor = Drawing.Color.Black
                            e.Row.Cells(5).ForeColor = Drawing.Color.Black
                            e.Row.Cells(6).ForeColor = Drawing.Color.Black
                            e.Row.Cells(7).ForeColor = Drawing.Color.Black
                        End If
                    End If
                    myLookup.Close()
                    myConnection.Close()

                End Using
            Catch
                'lblEmployeeInfo.Text += "gvschedules_rowdatabound: " & Err.Description
            End Try
        End If
    End Sub
    Public Function flag_if_closed(ByVal calYear As Int32, ByVal psemplid As Int32) As Boolean

        Dim closed As Boolean
        Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup2 As String = "Select [REVIEW_CLOSED] from [PerfReviewDates] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND [Review_Closed] = @Review_Closed"
            Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
            myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", psemplid)
            myLookupCommand2.Parameters.AddWithValue("@Review_Year", calYear)
            myLookupCommand2.Parameters.AddWithValue("@Review_Closed", True)

            myConnection2.Open()
            Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

            If myLookup2.Read Then
                If myLookup2("Review_closed") = True Then
                    closed = True
                Else
                    closed = False
                End If
            Else
                closed = False
            End If
            myLookup2.Close()
            myConnection2.Close()
        End Using
        Return closed
    End Function
    Protected Sub dvManagerSignon_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvManagerSignon.ItemUpdating
        e.NewValues("Manager_SignOn") = Trim(e.NewValues("Manager_SignOn").ToString.ToLower)
    End Sub


    Protected Sub lnkAllStatusExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllStatusExcel.Click


        gvAllReviews.GridLines = GridLines.Both

        GridViewExportUtil.Export("PerformanceReviews.xls", gvAllReviews)

    End Sub

    Public Class GridViewExportUtil

        Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
            HttpContext.Current.Response.ContentType = "application/ms-excel"

            Dim sw As StringWriter = New StringWriter
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            '  Create a form to contain the grid
            Dim table As Table = New Table

            table.GridLines = gv.GridLines
            '  add the header row to the table

            If (Not (gv.HeaderRow) Is Nothing) Then
                GridViewExportUtil.PrepareControlForExport(gv.HeaderRow)
                table.Rows.Add(gv.HeaderRow)
            End If
            '  add each of the data rows to the table

            For Each row As GridViewRow In gv.Rows
                GridViewExportUtil.PrepareControlForExport(row)
                table.Rows.Add(row)
            Next
            '  add the footer row to the table

            If (Not (gv.FooterRow) Is Nothing) Then
                GridViewExportUtil.PrepareControlForExport(gv.FooterRow)
                table.Rows.Add(gv.FooterRow)
            End If
            '  render the table into the htmlwriter

            table.RenderControl(htw)
            '  render the htmlwriter into the response
            HttpContext.Current.Response.Write(sw.ToString)
            HttpContext.Current.Response.End()
        End Sub



        ' Replace any of the contained controls with literals

        Private Shared Sub PrepareControlForExport(ByVal control As Control)

            Dim i As Integer = 0
            Do While (i < control.Controls.Count)
                Dim current As Control = control.Controls(i)

                If (i = 0 Or i = 1) AndAlso (current.GetType.ToString = "System.Web.UI.WebControls.DataControlFieldHeaderCell" OrElse current.GetType.ToString = "System.Web.UI.WebControls.DataControlFieldCell") Then
                    current.Visible = True
                    i += 1
                    Continue Do
                End If  'remove edit and history columns

                If (TypeOf current Is LinkButton) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
                ElseIf (TypeOf current Is ImageButton) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
                ElseIf (TypeOf current Is HyperLink) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
                ElseIf (TypeOf current Is DropDownList) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
                ElseIf (TypeOf current Is CheckBox) Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                    'TODO: Warning!!!, inline IF is not supported ?
                End If

                If current.HasControls Then
                    GridViewExportUtil.PrepareControlForExport(current)
                End If
                i = (i + 1)
            Loop
        End Sub
    End Class

    Public Sub populate_closed_reviews()
        ddlClosedReviews.Items.Clear()
        ddlClosedReviews.Items.Add(New ListItem("--Select Employee--", 0))
        lblEmployeeInfo.Text = Nothing

        Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLLookup As String = "SELECT A.PS_EMPLID,B.Employee_First_Name,B.Employee_Last_Name FROM PerfReviewDates A, PerfEmployee B WHERE (A.PS_EMPLID = B.PS_EMPLID AND A.Review_Closed = 'True' AND A.Review_Year = @Review_Year)"
            Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
            myLookupCommand.Parameters.AddWithValue("@Review_Year", ddlReportYear.SelectedValue)
            myConnection.Open()
            Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader
            While myLookup.Read()
                ddlClosedReviews.Items.Add(New ListItem(myLookup("Employee_First_Name") & " " & myLookup("Employee_Last_Name"), myLookup("PS_EMPLID")))
            End While
            myLookup.Close()
            myConnection.Close()
        End Using

        lblClosedReviews.Text = "Unlock the " & ddlReportYear.SelectedValue & " Performance Review for "

    End Sub
    Protected Sub ddlReportYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportYear.SelectedIndexChanged
        lnkUnlockReview.Text = Nothing
        lblReopenMsg.Text = Nothing
        If ddlReportYear.SelectedValue <> 0 Then
            Dim sqlAll As String = Nothing

            sqlAll = "SELECT B.Manager_Signon,B.Employee_Signon,B.Employee_Last_Name,B.Employee_First_Name, B.Employee_Title, A.Review_Date1, A.Review_Date2, A.Review_Date3, A.Final_Review_Date, A.Review_Closed,A.PS_EMPLID,A.Review_Year" & _
                                       " FROM PerfReviewDates A, PerfEmployee B" & _
                                       " WHERE (A.Review_Year = " & ddlReportYear.SelectedValue & " AND A.PS_EMPLID = B.PS_EMPLID)" & _
                                       " Order by B.Manager_SignOn desc"

            SqlAllReviews.SelectCommand = sqlAll
            SqlAllReviews.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
            gvAllReviews.DataSource = SqlAllReviews
            gvAllReviews.DataBind()

            populate_closed_reviews()
        End If

    End Sub

    Protected Sub ddlClosedReviews_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClosedReviews.SelectedIndexChanged
        If ddlClosedReviews.SelectedValue <> "0" Then
            lnkUnlockReview.Text = "Open Review"
        Else
            lblReopenMsg.Text = Nothing
            lnkUnlockReview.Text = Nothing
        End If
    End Sub

    Protected Sub lnkUnlockReview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUnlockReview.Click
        Using myUpdateConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            Const SQLUpdate As String = "Update [PerfReviewDates] SET [Review_Closed] = 'False' WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year"
            Dim myUpdateCommand As New SqlCommand(SQLUpdate, myUpdateConnection)
            myUpdateCommand.Parameters.AddWithValue("@PS_EMPLID", ddlClosedReviews.SelectedValue)
            myUpdateCommand.Parameters.AddWithValue("@Review_Year", ddlReportYear.SelectedValue)

            myUpdateConnection.Open()
            myUpdateCommand.ExecuteNonQuery()
            myUpdateConnection.Close()
            lblReopenMsg.Text = " Review has been opened"
            ddlClosedReviews.SelectedIndex = "0"
            ddlReportYear.SelectedIndex = "0"
            lnkUnlockReview.Text = Nothing
            populate_closed_reviews()
        End Using
    End Sub
End Class
