﻿<%@ Page Title="" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="Interruptible.aspx.vb" Inherits="Maintenance_PortalApps_Interruptible" %>
<%@ Register src="~/controls/unAuthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<!--Empty Module with Tabs-->
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
<asp:Panel ID="pnlAuthorized" runat="server" Visible="True"> 

    <!--5 minutes = 300,000 milliseconds
    1 minute = 60,000 milliseconds
    1 hour = 3,600,000 milliseconds
    a millisecond is 1/1,000th of a second -->
    <asp:Timer ID="Timer1" OnTick="Timer1_Tick" Interval="30000" runat="server"></asp:Timer>    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
    </Triggers>
    <ContentTemplate>
        
        
            <div style="float:left;margin-left:5px;padding-right:2px;">
                <asp:Button ID="btView" runat="server" CausesValidation="false" Font-Underline="false" Enabled="true" Text="Normal View" />
            </div>
            <div style="float:right;margin-right:5px;">
                <asp:Button ID="btInsert" runat="server" CausesValidation="false" Font-Underline="false" Enabled="false" Text="Insert" />
                <asp:Button ID="btUpdate" runat="server" CausesValidation="false" Font-Underline="false" Enabled="false" Text="Update" />
                <asp:Button ID="btDelete" runat="server" CausesValidation="false" Font-Underline="false" Enabled="false" Text="Delete" />
                <%--<asp:LinkButton ID="hlInsert" Enabled="false" Font-Underline="false" CausesValidation="false" runat="server">&nbsp;Insert&nbsp;</asp:LinkButton>
                <asp:LinkButton ID="hlUpdate" Enabled="false" Font-Underline="false" CausesValidation="false" runat="server">&nbsp;Update&nbsp;</asp:LinkButton>
                <asp:LinkButton ID="hlDelete" Enabled="false" Font-Underline="false" CausesValidation="false" runat="server">&nbsp;Delete&nbsp;</asp:LinkButton>--%>
                <br />
            </div>
        
        <asp:Panel ID="pnlDefault" Visible="true" runat="server">
            <div style="float:left;">
                <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="True" 
                    DataSourceID="SqlDataSource4" DataTextField="date" DataValueField="date">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                    SelectCommand="SELECT DISTINCT CONVERT(VARCHAR(10),date,110) as date FROM [dsm_interrupt]">
                </asp:SqlDataSource>
            </div>
            <div style="float:left;font-weight:bold;font-size:8pt;">
                <asp:Label ID="lblInfo" runat="server" Text="No date selected" style="margin-left:5px;margin-right:8px;"></asp:Label>
                <br />
                <asp:Label ID="Label1" runat="server" Text="KW Scheduled" style="margin-left:5px;margin-right:3px;"></asp:Label><asp:Label ID="lblRequested" runat="server"></asp:Label>
                <br />
                <asp:Label ID="Label2" runat="server" Text="KW Approved" style="margin-left:5px;margin-right:8px;"></asp:Label><asp:Label ID="lblApproved" runat="server"></asp:Label>
                <br />
            </div>
            <div style="float:right;">
                <asp:CheckBox ID="cbTimer" Text="Auto Refresh:" AutoPostBack="true" TextAlign="Left" runat="server" />
            </div>
            <hr style="clear:both;" />

            <div class="row">
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateGridview2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GridView2" runat="server" DataKeyNames="Id,CustomerId" DataSourceID="SqlDataSource3" 
                                    Caption="Scheduled for Interruption" CaptionAlign="Top"
                                    AllowPaging="true" AutoGenerateColumns="false" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" SelectText="Details" />
                                        <asp:CheckBoxField DataField="Approved" HeaderText="Approved" 
                                            SortExpression="Approved" />
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" 
                                            SortExpression="id" visible="false"/>
                                            <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" 
                                            SortExpression="CustomerId" Visible="false" />
                                        <asp:BoundField DataField="CustomerName" HeaderText="Customer" 
                                            SortExpression="CustomerName" />
                                        <asp:CheckBoxField DataField="InActive" HeaderText="InActive" 
                                            SortExpression="InActive" />
                                        <asp:BoundField DataField="hours" HeaderText="hours" SortExpression="hours" />
                                        <asp:BoundField DataField="date" HeaderText="date" SortExpression="date" DataFormatString="{0:M/dd/yyyy}" HtmlEncode="false"/>
                                        <asp:BoundField DataField="StartTime" HeaderText="Start" SortExpression="StartTime" />
                                        <asp:BoundField DataField="EndTime" HeaderText="End" SortExpression="EndTime" />
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk2nd" runat="server" Text="2nd eMail" CommandArgument='<%# Eval("id")%>' CommandName="escalate" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                SelectCommand="SELECT A.id AS Id,A.CustomerId as CustomerId,B.Name AS CustomerName, B.InActive, A.hours, A.date, A.ApproveInterrupt AS Approved,A.StartTime,A.EndTime FROM dsm_interrupt AS A INNER JOIN dsm_customer AS B ON A.CustomerId = B.id WHERE CONVERT(VARCHAR(10),date,110) = @date">
                <SelectParameters>                    
                    <asp:ControlParameter ControlID="ddlDate" Name="date" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
                
            <div class="row">
                <div class="col-lg-12">
                    <asp:Label ID="lblInterrupt" runat="server"></asp:Label>
                    <asp:Label ID="lblRefresh" runat="server"></asp:Label>
                    <asp:Label ID="lblMessage2" runat="server"></asp:Label>
                </div>
            </div>

            <div class="modal fade" id="dv2Detail" tabindex="-1" role="dialog" aria-labelledby="dv2DetailLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updateDetailsView2" runat="server">
                                <contenttemplate>
                                    <div class="row">
                                        <div class="col-lg-6 ">
                                        <asp:DetailsView ID="DetailsView2" runat="server" CssClass="table table-striped table-bordered table-hover"
                                            AutoGenerateRows="False" DataKeyNames="CustomerId" DataSourceID="SqlDataSource5">
                                            <Fields>
                                                <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" SortExpression="CustomerId" />
                                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                                <asp:BoundField DataField="Wattage" HeaderText="Wattage" SortExpression="Wattage" />
                        
                                                <%--<asp:BoundField DataField="CPD" HeaderText="CPD" SortExpression="CPD" />
                                                <asp:BoundField DataField="MFD" HeaderText="MFD" SortExpression="MFD" />
                                                <asp:BoundField DataField="RateKW_HR" HeaderText="RateKW_HR" SortExpression="RateKW_HR" />--%>
                        
                                                <asp:BoundField DataField="units" HeaderText="units" SortExpression="units" />
                                                <asp:BoundField DataField="hours" HeaderText="hours" SortExpression="hours" />                        
                                                <asp:BoundField DataField="id" HeaderText="Interrupt id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                                                <asp:BoundField DataField="StartTime" HeaderText="Start" SortExpression="StartTime" InsertVisible="False"/>
                                                <asp:BoundField DataField="EndTime" HeaderText="End" SortExpression="EndTime" InsertVisible="False" />  
                                            </Fields>
                                        </asp:DetailsView>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <asp:DetailsView ID="DetailsView3" runat="server" DataSourceID="SqlDataSource6" CssClass="table table-striped table-bordered table-hover">                
                                            </asp:DetailsView>
                                        </div>
                                    </div>
                                </contenttemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div><!-- modal DetailsView2 -->
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                SelectCommand="SELECT A.id AS Id, A.CustomerId,B.Name,B.Wattage,B.units, B.InActive, A.hours, A.date, A.ApproveInterrupt AS Approved,A.StartTime,A.EndTime                                     
                                FROM dsm_interrupt AS A INNER JOIN dsm_customer AS B ON A.CustomerId = B.id WHERE CONVERT(VARCHAR(10),date,110) = @date and A.Id = @id">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GridView2" Name="id" PropertyName="SelectedValue" Type="Int32" />                                                 
                    <asp:ControlParameter ControlID="ddlDate" Name="date" PropertyName="SelectedValue" />                      
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"                                         
                SelectCommand="SELECT COUNT(b.CustomerId) AS Total_Interruptions, SUM(b.hours) AS Total_Hours FROM dsm_customer AS a INNER JOIN dsm_interrupt AS b ON a.id = b.CustomerId WHERE (YEAR(b.date) = right(@date,4)) AND (b.CustomerId = @CustomerId)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlDate" Name="date" PropertyName="SelectedValue"  />
                    <asp:ControlParameter ControlID="Gridview2" Name="CustomerId" PropertyName="SelectedDataKey.Values[CustomerId]" Type="Int32" />                     
                </SelectParameters> 
            </asp:SqlDataSource>
        </asp:Panel><!-- pnlDefault -->

        <asp:Panel ID="pnlInsert" runat="server" Visible="false">
            <div class="row">
                <div class="col-lg-12"><p class="well-sm bg-info text-center"><strong>Email List</strong></p></div>
            </div>
    
            <asp:DetailsView ID="dvEmails" DataKeyNames="Email_Address" AutoGenerateRows="false" EmptyDataText="No Records" HeaderText="Add New" runat="server" 
                DataSourceID="dsEmails" CssClass="table table-striped table-bordered ">
                <Fields>
                    <asp:TemplateField HeaderText="Email Address" InsertVisible="true" HeaderStyle-CssClass="field-label col-xs-3 active">
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextEmail" runat="server" Text='<%# Bind("Email_Address") %>' CssClass="col-md-9 form-control"></asp:TextBox>                
                        </InsertItemTemplate>            
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Send second Notification" InsertVisible="true" HeaderStyle-CssClass="field-label col-xs-3 active">            
                        <InsertItemTemplate>                
                            <asp:CheckBox ID="ckNotify" runat="server" Checked='<%# Bind("Send_second_Notification") %>' CssClass="col-md-9 form-control"/>
                        </InsertItemTemplate>            
                    </asp:TemplateField>   
                    <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="field-label col-xs-3 active">            
                        <InsertItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Insert" CommandArgument="Insert" Text="Insert" CssClass="btn btn-primary" />
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="btn btn-default" />
                        </InsertItemTemplate>
                        <ItemTemplate>                
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="New" Text="New" CssClass="btn btn-primary" />
                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="btn btn-default"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>   
            </asp:DetailsView>

            <asp:GridView ID="gvEmails" runat="server" DataKeyNames="Email_Address" DataSourceID="dsEmails" EmptyDataText="There are no data records to display." PageSize="5" 
                AllowPaging="true" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Email_Address" HeaderText="Email Address" SortExpression="Email_Address" />
                    <asp:CheckBoxField DataField="Send_second_Notification" HeaderText="Send second Notification" SortExpression="Send_second_Notification" />
                </Columns>
                <PagerStyle CssClass="pagination-ys" />
                <EmptyDataTemplate>
                    <asp:LinkButton ID="lnkEmail1" runat="server" CausesValidation="False" CommandArgument="Select" CommandName="New" onclick="lnkNewEmail_Click1" Text="New"></asp:LinkButton>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Label ID="Label3" runat="server" ></asp:Label>
            <asp:SqlDataSource ID="dsEmails" runat="server" OldValuesParameterFormatString="original_{0}" 
                ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                DeleteCommand="DELETE FROM [dsm_Emails] WHERE [Email_Address] = @original_Email_Address" 
                InsertCommand="INSERT INTO [dsm_Emails] ([Email_Address],[Send_second_Notification]) VALUES (@Email_Address,@Send_second_Notification)"         
                SelectCommand="SELECT [Email_Address],[Send_second_Notification] FROM [dsm_Emails]" 
                UpdateCommand="UPDATE [dsm_Emails] SET [Email_Address] = @Email_Address, [Send_second_Notification] = @Send_second_Notification WHERE [Email_Address] = @original_Email_Address">
                <DeleteParameters>
                    <asp:Parameter Name="original_Email_Address" Type="string" />            
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Email_Address" Type="String" />
                    <asp:Parameter Name="Send_second_Notification" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="original_Email_Address" Type="String" />
                    <asp:Parameter Name="Send_second_Notification" Type="String" />            
                </UpdateParameters>
            </asp:SqlDataSource>
        </asp:Panel><!-- pnlInsert -->
        
        <asp:Panel ID="pnlUpdate" runat="server" Visible="false">
            <asp:GridView ID="GridView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource1" Caption="Interruption Administration" CaptionAlign="Top"  AllowSorting="true"
                AllowPaging="True" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false" ShowFooter="true">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Duration: <br />
                            <asp:DropDownList ID="ddlHours" runat="server">                            
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                            <asp:ListItem Text="8" Value="8" Selected="True"></asp:ListItem>                            
                            </asp:DropDownList>
                        </HeaderTemplate>           
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text='<%# Bind("Name") %>'></asp:LinkButton>
                        </ItemTemplate>
                        <FooterTemplate>
                            Start Date:<br />mm/dd/yyyy<br />
                            <asp:TextBox ID="txtStartDateMonth" Width="25px" MaxLength="2" runat="server" />/
                            <asp:TextBox ID="txtStartDateDay" Width="25px" MaxLength="2" runat="server" />/
                            <asp:TextBox ID="txtStartDateYear" Width="45px" MaxLength="4" runat="server" />
                            <br />
                            <br />Start Time:<br />hh:mm<br />(24-hour format)<br>
                            <asp:TextBox ID="txtStartTimeHrs" MaxLength="2" Width="25px" runat="server" />
                            <asp:Label ID="lblColon" Text=":" Font-Size="Large" runat="server" />
                            <asp:TextBox ID="txtStartTimeMins" MaxLength="2" Width="25px" runat="server" />
                            <br />
                            <asp:Label ID="lblTotal" Font-Bold="true" runat="server" />
                            <br />
                            <asp:Label ID="lblEntryError" Font-Bold="true" Font-Color="Red" runat="server" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>                           
                            <asp:LinkButton ID="lnkCheck" runat="server" CausesValidation="false" CommandName="CheckAll">Check All</asp:LinkButton>
                            <asp:LinkButton ID="lnkUnCheck" runat="server" CausesValidation="false" CommandName="unCheckAll" Visible="false">UnCheck All</asp:LinkButton>
                        </HeaderTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" AutoPostBack="true" runat="server" oncheckedchanged="CheckBox1_CheckedChanged" />
                        </ItemTemplate>
                        <FooterTemplate> 
                            End At:<br />
                            <asp:Label ID="lblEndDate" runat="server" ></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Wrap="true" />
                    <asp:BoundField DataField="AcctNum" HeaderText="AcctNum" SortExpression="AcctNum" />
                    <asp:TemplateField HeaderText="Groups" SortExpression="Groups" ShowHeader="true">
                        <HeaderTemplate>                            
                            <asp:DropDownList ID="ddlGroups" runat="server" DataSourceID="dsGroups" DataTextField="groups" DataValueField="groups" AutoPostBack="true" onselectedindexchanged="ddlGroups_SelectedIndexChanged" />
                            <asp:SqlDataSource ID="dsGroups" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" SelectCommand="SELECT DISTINCT [Groups] FROM [dsm_customer]" />
                        </HeaderTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Groups") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Groups") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Notify" Text="Submit" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PrimaryEmail" HeaderText="PrimaryEmail" SortExpression="PrimaryEmail" />   
                    <asp:BoundField DataField="SecondaryEmail" HeaderText="SecondaryEmail" SortExpression="SecondaryEmail" />                             
                    <asp:Boundfield DataField="Wattage" HeaderText="IR" SortExpression="Wattage"/>                                                                                                                                
                    <asp:BoundField DataField="units" HeaderText="units" SortExpression="units" />
                    <asp:TemplateField HeaderText="InActive" SortExpression="InActive">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("InActive") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("InActive") %>' Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CPD" HeaderText="CPD" SortExpression="CPD" />
                    <asp:TemplateField HeaderText="MFD" SortExpression="MFD">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("MFD") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("MFD") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="RateKW_HR" HeaderText="RateKW_HR" SortExpression="RateKW_HR" />
                    <asp:BoundField DataField="Total_Interruptions" HeaderText="Tot Int" SortExpression="Total_Interruptions" />
                    <asp:BoundField DataField="Total_Hours" HeaderText="Tot Hrs" SortExpression="Total_Hours" />
                </Columns>
                <PagerStyle CssClass="pagination-ys" />
                <EmptyDataTemplate>No customers have been created</EmptyDataTemplate>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
                SelectCommand="SELECT id, Name, AcctNum, Groups, PrimaryEmail, SecondaryEmail, Wattage, units, Inactive, ReactivateDate, RateKW_HR, CPD, MFD,
                            (SELECT        COUNT(b.CustomerId) AS Expr1
                            FROM            dsm_customer AS a INNER JOIN
                                                        dsm_interrupt AS b ON a.id = b.CustomerId
                            WHERE        (b.CustomerId = c.id) AND (YEAR(b.date) = YEAR(getutcdate()))) AS Total_Interruptions,
                            (SELECT        SUM(b.hours) AS Expr2
                            FROM            dsm_customer AS a INNER JOIN
                                                        dsm_interrupt AS b ON a.id = b.CustomerId
                            WHERE        (b.CustomerId = c.id) AND (YEAR(b.date) = YEAR(getutcdate()))) AS Total_Hours
                            FROM            dsm_customer AS c">                                                    
            </asp:SqlDataSource>

            <asp:Panel ID="pnlAdminOnly" runat="server" enabled="false">
                <div class="row">
                    <div class="col-lg-6">
                        <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource2"
                            HeaderText="Interruptible Customers" AutoGenerateRows="False" CssClass="table table-striped table-bordered table-hover">
                            <Fields>
                                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                <asp:BoundField DataField="AcctNum" HeaderText="AcctNum" SortExpression="AcctNum" />
                                <asp:BoundField DataField="Groups" HeaderText="Groups" SortExpression="Groups" />
                                <asp:BoundField DataField="PrimaryEmail" HeaderText="PrimaryEmail" SortExpression="PrimaryEmail" />
                                <asp:BoundField DataField="SecondaryEmail" HeaderText="SecondaryEmail" SortExpression="SecondaryEmail" />
                                <asp:BoundField DataField="Wattage" HeaderText="IR" SortExpression="Wattage" />                        
                                <asp:BoundField DataField="units" HeaderText="units" SortExpression="units" />
                                <asp:CheckBoxField DataField="InActive" HeaderText="InActive" SortExpression="InActive" />
                                <asp:BoundField DataField="CPD" HeaderText="CPD" SortExpression="CPD" />
                                <asp:BoundField DataField="MFD" HeaderText="MFD" SortExpression="MFD" />
                                <asp:BoundField DataField="RateKW_HR" HeaderText="RateKW_HR" SortExpression="RateKW_HR" />                       
                                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" CausesValidation="false" />
                            </Fields>                    
                            <EmptyDataTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="New" Text="New Customer"></asp:LinkButton><%--</div>--%>
                            </EmptyDataTemplate>
                        </asp:DetailsView>
                    </div>
                    <div class="col-lg-6">
                        <asp:DetailsView ID="DetailsView4" runat="server" DataSourceID="SqlDataSource7" HeaderText="Interrupt History" HeaderStyle-CssClass="text-center"
                            CssClass="table table-striped table-bordered "></asp:DetailsView>
                    </div>
                </div>

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                    SelectCommand="SELECT * FROM [dsm_customer] WHERE ([id] = @id)" 
                    DeleteCommand="DELETE FROM [dsm_customer] WHERE [id] = @id" 
                    InsertCommand="INSERT INTO [dsm_customer] ([Name], [AcctNum], [Groups], [PrimaryEmail], [SecondaryEmail], [Wattage],[units],[Inactive],[CPD],[MFD],[RateKW_HR]) VALUES (@Name, @AcctNum, @Groups, @PrimaryEmail, @SecondaryEmail, @Wattage,@units,@Inactive,@CPD,@MFD,@RateKW_HR)" 
                    UpdateCommand="UPDATE [dsm_customer] SET [Name] = @Name, [AcctNum] = @AcctNum, [Groups] = @Groups, [PrimaryEmail] = @PrimaryEmail, [SecondaryEmail] = @SecondaryEmail, [Wattage] = @Wattage, [units] = @units, [Inactive] = @Inactive, [CPD] = @CPD,[MFD] = @MFD,[RateKW_HR] = @RateKW_HR WHERE [id] = @id">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GridView1" Name="id" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="AcctNum" Type="String" />
                        <asp:Parameter Name="Groups" Type="String" />
                        <asp:Parameter Name="PrimaryEmail" Type="String" />
                        <asp:Parameter Name="SecondaryEmail" Type="String" />
                        <asp:Parameter Name="Wattage" Type="Decimal" />
                        <asp:Parameter Name="CPD" Type="Decimal" />
                        <asp:Parameter Name="MFD" Type="Decimal" />
                        <asp:Parameter Name="RateKW_HR" Type= "Decimal" />
                        <asp:Parameter Name="units" Type="string" />
                        <asp:Parameter Name="id" Type="Int32" />
                        <asp:Parameter Name="Inactive" Type="Boolean" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="AcctNum" Type="String" />
                        <asp:Parameter Name="Groups" Type="String" />
                        <asp:Parameter Name="PrimaryEmail" Type="String" />
                        <asp:Parameter Name="SecondaryEmail" Type="String" />
                        <asp:Parameter Name="Wattage" Type="Decimal" />
                        <asp:Parameter Name="CPD" Type="Decimal" />
                        <asp:Parameter Name="MFD" Type="Decimal" />
                        <asp:Parameter Name="RateKW_HR" Type="Decimal" />                        
                        <asp:Parameter Name="units" Type="string" />
                        <asp:Parameter Name="Inactive" Type="Boolean" DefaultValue="False" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"                                         
                    SelectCommand="SELECT COUNT(b.CustomerId) AS Total_Interruptions, SUM(b.hours) AS Total_Hours FROM dsm_customer AS a INNER JOIN dsm_interrupt AS b ON a.id = b.CustomerId WHERE (YEAR(b.date) = Year(getutcdate())) AND (b.CustomerId = @Id)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="Gridview1" Name="Id" PropertyName="SelectedValue" Type="Int32" />                     
                    </SelectParameters> 
                </asp:SqlDataSource>

                <div class="row">
                    <div class="col-lg-6">
                    </div>
                    <div class="col-lg-6 pull-right">
                        <asp:DropDownList ID="ddlFailCustomers" AutoPostBack="false" DataSourceID="sqlFailures" DataTextField="Name" DataValueField="Id" runat="server">
                        </asp:DropDownList>
                        <asp:Button ID="btnAddFailure" runat="server" CausesValidation="false" Text="Add to Failure List" />
                        <br /><br />
                        <asp:SqlDataSource ID="SqlFailures" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                            SelectCommand="Select Id,Name from dsm_customer">
                        </asp:SqlDataSource>
                        <asp:Label runat="server" ID="lblCustId"></asp:Label>
                        <asp:GridView ID="gvFailures" runat="server" DataSourceID="SqlFailedList" Caption="Customer Failures" CaptionAlign="Top" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true">                  
                        </asp:GridView>               
                        <asp:SqlDataSource ID="SqlFailedList" ConnectionString = "<%$ ConnectionStrings:documentsConnectionString %>"
                            SelectCommand = "Select A.CustomerId, B.Name,A.Failure_Date from dsm_failure A, dsm_customer B where A.CustomerId = B.Id" runat="server"></asp:SqlDataSource>
                    </div>
                </div>
            </asp:Panel><!-- pnlAdminOnly -->
            <br />
            <asp:Label ID="lblMessage" runat="server" ></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlDelete" runat="server" Visible="false"></asp:Panel>        
   </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
</asp:Content>


