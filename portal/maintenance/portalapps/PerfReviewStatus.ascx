﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PerfReviewStatus.ascx.vb" Inherits="Maintenance_PortalApps_PerfReviewStatus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

    <asp:UpdatePanel ID="updPerf" runat="server">
        <ContentTemplate>
                <div style="text-align:center;font-weight:bold;font-size:large;">
                    Merit Increase Percent:
                    <br />
                    <asp:GridView ID="gvPercent" DataSourceID="SqlDSPercent" SkinID="GridView" 
                        runat="server" AutoGenerateColumns="False">
                        <Columns>
                            <asp:CommandField ShowEditButton="True" />
                            <%--<asp:BoundField DataField="percent" 
                            HeaderText="percent" SortExpression="percent" />--%>
                            <asp:BoundField DataField="percentage" 
                            HeaderText="percent" SortExpression="percentage" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDSPercent" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                        SelectCommand="SELECT [percentage] FROM [PerfPercent]" 
                        UpdateCommand="UPDATE [PerfPercent] SET [percentage] = @percentage">
                    <UpdateParameters>
                        <asp:Parameter Name="percentage" Type="Int32" />
                    </UpdateParameters>                    
                    </asp:SqlDataSource> 
                </div> 
                                                     
                <div style="text-align:center;font-weight:bold;font-size:large;">
                    Assign Manager's Network ID
                </div>
                
                <div style="text-align:left;font-weight:bold;font-size:large;color:#36587a">
                    1.Select a manager from the dropdown:
                </div>
                
                <br />                                    
                
                <div style="float:left;">
                    <asp:DropDownList ID="ddlManagers" runat="server" AutoPostBack="true"></asp:DropDownList>
                    <br />
                    
                </div>                
                
                <div style="float:left;padding-left:3px;padding-top:3px;">
                    <asp:Label ID="lblMgr" runat="server"></asp:Label>
                </div>
                
                <br style="clear:both;"/><br />                                 
                
                <asp:DetailsView ID="dvManagerSignon" runat="server" Visible="false" Height="50px" 
                    Width="275px" AutoGenerateRows="False" DataSourceID="SqlManagerSignOn" CellPadding="4" ForeColor="#333333" GridLines="None" Caption="2. Click Edit and add Network ID" CaptionAlign="Left">                   
                    <Fields>
                        <asp:BoundField DataField="Employee_First_Name" 
                            HeaderText="Employee_First_Name" ReadOnly="True" SortExpression="Employee_First_Name" />
                        <asp:BoundField DataField="Employee_Last_Name" ReadOnly="True" HeaderText="Employee_Last_Name" 
                            SortExpression="Employee_Last_Name" />
                        <asp:BoundField DataField="Employee_Title" ReadOnly="True" HeaderText="Employee_Title" 
                            SortExpression="Employee_Title" />
                        <asp:BoundField DataField="Employee_Location" ReadOnly="True" HeaderText="Employee_Location" 
                            SortExpression="Employee_Location" />
                        <asp:BoundField DataField="Manager_SignOn" HeaderText="Network ID" 
                            SortExpression="Manager_SignOn" />
                        <asp:BoundField DataField="PS_EMPLID" ReadOnly="True" HeaderText="PS_EMPLID" 
                            SortExpression="PS_EMPLID" />
                            <asp:TemplateField ShowHeader="False">
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                    CommandName="Update" CommandArgument="Update" Text="Update"></asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                    CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                    CommandName="Insert" CommandArgument="Insert" Text="Insert"></asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                    CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Edit" Text="Edit"></asp:LinkButton>                                        
                            </ItemTemplate>
                        </asp:TemplateField>       
                    </Fields>
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" /> 
                        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />                       
                        <HeaderStyle BackColor="#113962" ForeColor="White" />
                        <EditRowStyle BorderStyle="None"></EditRowStyle>
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black"></RowStyle>
                        <PagerStyle BackColor="#113962" ForeColor="White" Width="100%" HorizontalAlign="Center"></PagerStyle>                    
                </asp:DetailsView>                    
                                       
                <asp:SqlDataSource ID="SqlManagerSignOn" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>" 
                    SelectCommand="SELECT [Employee_First_Name], [Employee_Last_Name], [Employee_Title], [Employee_Location], [Manager_SignOn], [PS_EMPLID] FROM [PerfEmployee] WHERE ([PS_EMPLID] = @PS_EMPLID)"
                    UpdateCommand="UPDATE [PerfEmployee] SET [Manager_SignOn] = @Manager_SignOn WHERE [PS_EMPLID] = @PS_EMPLID">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlManagers" Name="PS_EMPLID" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="ddlManagers" Name="PS_EMPLID" 
                            PropertyName="SelectedValue" Type="Int32" />  
                        <asp:Parameter Name="Manager_SignOn" Type="String" />                                                                
                    </UpdateParameters>
                </asp:SqlDataSource> 
                
                <br /><br />
            
            <asp:Panel ID="pnlManagerView" runat="server"> 
                                                                                 
            <%--<div style="padding:20px;border:solid 1px black;background-color:White;">--%>                    
                <hr />                       
                <div style="text-align:center;font-weight:bold;font-size:large;">
                    REPORTS
                </div>
                
                <br />
                Retrieve performance review status for employees reporting to 
                <asp:Label ID="lblManager" Font-Bold="true" runat="server" Text="Manager not selected"></asp:Label>
                <asp:DropDownList ID="ddlSubordinate" Visible="false" runat="server">
                </asp:DropDownList>
                <br />
                for performance reviews conducted in the following year
                <asp:DropDownList ID="ddlReviewYear" AutoPostBack="false" runat="server">                                    
                </asp:DropDownList> 
                <br />
                that are currently                
                <asp:DropDownList ID="ddlClosed" AutoPostBack="false" runat="server"> 
                    <asp:ListItem Text="Open" Value="False" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Closed" Value="True"></asp:ListItem>                                                       
                </asp:DropDownList>
                <br />
                (At least one review has to be performed to be displayed as Open)
                <br />
                
                <asp:Button ID="btnSubmit" runat="server" Text="Run Report" />                 
                
                <asp:Label ID="Label5" runat="server"></asp:Label>                                      
                <asp:Label ID="lblEmployeeInfo" runat="server"></asp:Label>
                
                <%--<asp:GridView ID="gvSchedules" runat="server" BackColor="White" 
                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                    GridLines="Vertical">
                    <FooterStyle BackColor="white" ForeColor="#113962" width="100%" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="lightsteelblue" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#113962" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#113962" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="Gainsboro" />                    
                    <Columns>
                    <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Image id="img1" ImageUrl="~/Images/flag_green.png" runat="server" >
                        </asp:Image>
                    </ItemTemplate> 
                    </asp:TemplateField>                                
                    </Columns>
                    <EmptyDataTemplate>
                    --No Open Reviews For This Manager--
                    </EmptyDataTemplate>
                </asp:GridView>--%>
                <asp:GridView ID="gvSchedules" runat="server" BackColor="White" 
                BorderColor="#36587a" BorderStyle="solid" BorderWidth="1px" CellPadding="3" 
                GridLines="Vertical">
               
                <FooterStyle BackColor="white" ForeColor="#113962" width="100%" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="lightsteelblue" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#113962" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#113962" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="Gainsboro" />
                <Columns>
                <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="img1" runat="server" >&nbsp;<i class="fa fa-flag"></i></asp:Label>
                    <%--<asp:Image id="img1" ImageUrl="~/Images/flag_green.png" runat="server" >
                    </asp:Image>--%>
                </ItemTemplate>                
                </asp:TemplateField>                                
                </Columns>
                <EmptyDataTemplate>
                        it's empty                                                     
                </EmptyDataTemplate>
            </asp:GridView>
                <asp:SqlDataSource ID="SqlSchedules" runat="server"></asp:SqlDataSource>
                <hr /> 
                <asp:DropDownList ID="ddlReportYear" AutoPostBack="true" runat="server">
                </asp:DropDownList><asp:LinkButton ID="lnkAllStatusExcel" runat="server" Text="Export All to Excel"></asp:LinkButton>
           <%--</div>--%>   
            <div style="text-align:center;font-weight:bold;font-size:large;">
            Unlock Closed Reviews
       </div>         
                <asp:Label ID="lblClosedReviews" runat="server"></asp:Label><asp:DropDownList ID="ddlClosedReviews" AutoPostBack="true" runat="server">                    
                </asp:DropDownList> <asp:LinkButton ID="lnkUnlockReview" runat="server"></asp:LinkButton><asp:Label ID="lblReopenMsg" runat="server"></asp:Label>                                          
                    </asp:Panel>                                                                                                                  	                      
        </ContentTemplate>
    </asp:UpdatePanel>
    
       
                <asp:GridView ID="gvAllReviews" runat="server" BackColor="White" 
                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                    GridLines="Vertical">
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlAllReviews" runat="server"></asp:SqlDataSource> 
      
<script type="text/javascript" >
        // The following snippet works around a problem where FloatingBehavior
        // doesn't allow drops outside the "content area" of the page - where "content
        // area" is a little unusual for our sample web pages due to their use of CSS
    // for layout.    
        function setBodyHeightToContentHeight() {
            document.body.style.height = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight) + "px";
        }
        setBodyHeightToContentHeight();
        $addHandler(window, "resize", setBodyHeightToContentHeight);
    </script>



