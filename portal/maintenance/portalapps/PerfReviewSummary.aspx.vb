﻿Imports System.Data.SqlClient

Partial Class Maintenance_PortalApps_PerfReviewSummary
    Inherits System.Web.UI.Page
    Dim username As String
    Dim PS_EMPLID As Int32
    Dim Review_Year As Int32
    Dim Review_Closed As Boolean
    Public printurl As String = ""
    Dim tparam As String = ""
    Dim ReviewUrl As String
    Dim tdes As New tripleDES
    '8/27/2012 - replace SYSADM.PS_JOB with SYSADM.EDE_PORTAL_PS_JOB_VW 
    '8/27/2012 - replace SYSADM.PS_EMPLOYEES with SYSADM.EDE_PORTAL_PS_EMPLOYEES_VW
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Request.QueryString.Count > 0 Then
                PS_EMPLID = tdes.Decrypt(Request.QueryString("a"))
                'PS_EMPLID = Request.QueryString("a")
                Review_Year = Request.QueryString("b")
                Review_Closed = Request.QueryString("c")
                tparam = "~/print.aspx?t=" & Review_Year
                printurl = ResolveUrl(tparam)

                Using myConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Const SQLLookup As String = "SELECT Employee_First_Name,Employee_Last_Name,Employee_Title,Employee_Location,Salaried,PS_EMPLID from PerfEmployee where PS_EMPLID = @PS_EMPLID"
                    Dim myLookupCommand As New SqlCommand(SQLLookup, myConnection)
                    myLookupCommand.Parameters.AddWithValue("@PS_EMPLID", PS_EMPLID)
                    myConnection.Open()
                    Dim myLookup As SqlDataReader = myLookupCommand.ExecuteReader

                    If myLookup.Read Then
                        lblName.Text = myLookup("Employee_First_Name") & " " & myLookup("Employee_Last_Name")
                        lblNameBottom.Text = myLookup("Employee_First_Name") & " " & myLookup("Employee_Last_Name")
                        lblTitle.Text = myLookup("Employee_Title")
                        If Not myLookup("Employee_Location") Is DBNull.Value Then
                            lblLocation.Text = myLookup("Employee_Location")
                        Else
                            lblLocation.Text = Nothing
                        End If

                        If Review_Closed Then

                            Using myConnection2 As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                                Const SQLLookup2 As String = "Select [Review_Closed],[CLOSED_Date],[CLOSED_By] from [PerfReviewDates] WHERE [PS_EMPLID] = @PS_EMPLID AND [Review_Year] = @Review_Year AND [Review_Closed] = @Review_Closed"
                                Dim myLookupCommand2 As New SqlCommand(SQLLookup2, myConnection2)
                                myLookupCommand2.Parameters.AddWithValue("@PS_EMPLID", PS_EMPLID)
                                myLookupCommand2.Parameters.AddWithValue("@Review_Year", Review_Year)
                                myLookupCommand2.Parameters.AddWithValue("@Review_Closed", True)

                                myConnection2.Open()
                                Dim myLookup2 As SqlDataReader = myLookupCommand2.ExecuteReader

                                If myLookup2.Read Then
                                    If myLookup2("Review_closed") = True Then
                                        lblClosedBy.Visible = True
                                        lblClosedBy.Text = "Closed By: " & myLookup2("Closed_By")
                                        lblDate1.Text = "Closed On: " & myLookup2("Closed_Date")
                                    Else
                                        lblDate1.Text = "Closed_Date not available"
                                    End If
                                Else
                                    lblDate1.Text = "Closed_Date not available"
                                End If
                                myLookup2.Close()
                                myConnection2.Close()
                            End Using

                        Else
                            lblDate1.Text = "Date: " & Now().ToShortDateString
                        End If

                        SqlLearning.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                        SqlLearning.SelectCommand = "SELECT B.Learning_Goal,B.Learning_Assessment,B.LG_ID from PerfEmployee A, PerfLearningGoals B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.LG_ID"
                        SqlLearning.DataBind()
                        If myLookup("Salaried") = True Then
                            SqlIncentive.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                            SqlIncentive.SelectCommand = "SELECT B.Incentive_Goal,B.Incentive_Assessment,B.IG_ID from PerfEmployee A, PerfIncentiveGoals B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.IG_ID"
                            SqlIncentive.DataBind()
                        End If
                        SqlCompetence.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                        SqlCompetence.SelectCommand = "SELECT B.Competence_Goal,B.Competence_Assessment,Competence_Suggestion,B.CG_ID from PerfEmployee A, PerfCompetencies B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.CG_ID"
                        SqlCompetence.DataBind()

                        SqlComments.ConnectionString = ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString
                        SqlComments.SelectCommand = "SELECT B.Comment_Description,b.CM_ID from PerfEmployee A, PerfComments B where A.PS_EMPLID = B.PS_EMPLID AND A.PS_EMPLID = '" & myLookup("PS_EMPLID") & "' AND B.Review_Year = " & Review_Year & " order by b.CM_ID"
                        SqlComments.DataBind()
                    End If
                    myLookup.Close()
                    myConnection.Close()

                End Using
            End If
        Catch
            Response.Write(Err.Description)
        End Try


    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Dim lblPageHeader As Label = Page.Master.FindControl("lblPageHeader")
        lblPageHeader.Text = "Performance Review"

        Dim menu As Label = Page.Master.FindControl("lblMenu")
        menu.Visible = False

    End Sub
End Class
