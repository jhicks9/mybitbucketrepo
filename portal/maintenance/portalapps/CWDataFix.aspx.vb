﻿Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports System.IO

Partial Class Maintenance_PortalApps_CWDataFix
    Inherits System.Web.UI.Page
    Dim CurrentPage As String
    Dim username As String
    Dim ReferringPage As String
    Dim objStreamWriter As StreamWriter
    Dim logflag As Boolean
    Private Comm As New OracleCommand
    Private Lookup As New OracleCommand
    Dim Objconn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CWPortal").ConnectionString)
    Dim Objdocs As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
    Dim ps As New portalSecurity
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim filename As String
            Dim logday As Int32 = Now.Day
            Dim logmonth As Int32 = Now.Month
            Dim logyear As Int32 = Now.Year
            Dim logdate As String = logday.ToString & logmonth.ToString & logyear.ToString

            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If

            'filename = "\PortalApps\CWDataFix" & logdate & ".html"


            If Not IsPostBack Then
                'javascript popup when Execute Script is clicked
                btnExecScript.Attributes.Add("onClick", "javascript:return confirm('You are submitting changes to the database. Are you sure you want to update these rows?')")
            End If

            'Get userid from database
            username = Request.ServerVariables("LOGON_USER")
            username = Right(username, Len(username) - InStr(username, "\"))
            'ReferringPage = Request.UrlReferrer.ToString
            CurrentPage = Request.Url.ToString

            If IsPostBack Then
                btnExecScript.Visible = False
            End If
        Catch
            logData("[PageLoad]" & Err.Description)
        End Try
    End Sub

    Public Shared Sub OpenPopUp(ByVal opener As System.Web.UI.WebControls.WebControl, ByVal PagePath As String)
        Dim clientScript As String

        'Building the client script- window.open
        clientScript = "window.open('" & PagePath & "')"
        'register the script to the clientside click event of the 'opener' control
        opener.Attributes.Add("onLoad", clientScript)
    End Sub
    Private Sub btnExecScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExecScript.Click
        Try
            Dim log As String = ""

            Select Case cboTableName.SelectedValue
                Case "service_pnt_assign"
                    lblQuery.Text = lblSpa_Query.Text
                    lblSelect.Text = lblSpa_Select.Text
                    lblSpa_Select.Text = Nothing
                    lblSpa_Query.Text = Nothing
                Case "service_order"
                    lblQuery.Text = lblSo_Query.Text
                    lblSelect.Text = lblSo_Select.Text
                    lblSo_Select.Text = Nothing
                    lblSo_Query.Text = Nothing
                Case "account_package"
                    lblQuery.Text = lblAp_Query.Text
                    lblSelect.Text = lblAp_Select.Text
                    lblAp_Select.Text = Nothing
                    lblAp_Query.Text = Nothing
                Case "account_pricing"
                    lblQuery.Text = lblApr_Query.Text
                    lblSelect.Text = lblApr_Select.Text
                    lblApr_Select.Text = Nothing
                    lblApr_Query.Text = Nothing
                Case "account_component"
                    lblQuery.Text = lblAc_Query.Text
                    lblSelect.Text = lblAc_Select.Text
                    lblAc_Select.Text = Nothing
                    lblAc_Query.Text = Nothing
                Case "payment"
                    lblQuery.Text = lblPay_Query.Text
                    lblSelect.Text = lblPay_Select.Text
                    lblPay_Select.Text = Nothing
                    lblPay_Query.Text = Nothing
                Case "equipment"
                    lblQuery.Text = lblEquip_Query.Text
                    lblSelect.Text = lblEquip_Select.Text
                    lblEquip_Select.Text = Nothing
                    lblEquip_Query.Text = Nothing
                Case "--select table--"
                    lblQuery.ForeColor = Drawing.Color.Red
                    lblQuery.Text = "No table selected."
                Case Else
                    lblQuery.ForeColor = Drawing.Color.Red
                    lblQuery.Text = "Table not found."
            End Select
            lblQuery.Visible = True
            Objconn.Open()

            Dim ComSelect As New OracleCommand(lblSelect.Text, Objconn)
            Dim drSelect As OracleDataReader
            Dim x As Int32
            Dim colName, fldValue As String

            Comm = New OracleCommand(lblQuery.Text, Objconn)
            Comm.ExecuteNonQuery()

            drSelect = ComSelect.ExecuteReader
            While drSelect.Read
                For x = 0 To drSelect.FieldCount() - 1
                    colName = drSelect.GetName(x).ToString()
                    fldValue = drSelect.GetValue(x).ToString()
                    log += colName & ":" & fldValue & " "
                Next
            End While
            drSelect.Close()
            Objconn.Close()

            LogData(log)

            btnExecScript.Visible = False
            pnlLogView.Visible = True
            lblQuery.Visible = False
            pnlService_pnt_assign.Visible = False
            pnlService_order.Visible = False
            pnlAccount_package.Visible = False
            pnlAccount_pricing.Visible = False
            pnlAccount_component.Visible = False
            pnlPayment.Visible = False
            pnlEquipment.Visible = False
            lblLog.Visible = True
        Catch ex As exception
            logData("[btnExecScript]" & ex.Message)
        End Try
    End Sub

    Public Sub LogData(ByVal logData As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            cmd.CommandText = "insert into log (date,username,web) VALUES (GETDATE(),@username,@web)"
            cmd.Parameters.AddWithValue("@username", ps.getPageName)
            cmd.Parameters.AddWithValue("@web", Left(logData, 127))
            conn.Open()
            cmd.ExecuteNonQuery()

             ' delete old logs - only keep 2 weeks
            cmd.CommandText = "delete from log where (date <= GETDATE() - 14) and (username = @username)"
            cmd.ExecuteNonQuery()
            conn.Close()

            Dim test As String = ps.getPageName
        Catch ex As Exception
            If ex.Message.Contains("Invalid object name") Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.log(id int IDENTITY(1,1) NOT NULL,ipaddress varchar(25) NULL,date datetime NULL,refer varchar(256) NULL,username varchar(50) NULL,platform varchar(25) NULL,browser varchar(25) NULL,web varchar(128) NULL,html varchar(max) NULL,CONSTRAINT PK_logs PRIMARY KEY CLUSTERED (id ASC))"
                cmd.ExecuteNonQuery()
            End If
            If conn.State = System.Data.ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub btnAc_ShowScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAc_ShowScript.Click
        Try
            Dim strPrevDate As String
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            strPrevDate = cboAc_prev_charge_day.SelectedValue & cboAc_prev_charge_month.SelectedValue & cboAc_prev_charge_year.SelectedValue
            lblAc_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblAc_Select.Text = "SELECT "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboAc_charged_ind.SelectedValue <> "" Then
                lblAc_Query.Text += " Set charged_ind = '" & cboAc_charged_ind.SelectedValue & "',"
                lblAc_Select.Text += " charged_ind,"
            Else
                lblAc_Query.Text += " Set"
            End If
            If chkAc_Null_prev.Checked Then
                lblAc_Query.Text += " prev_charge_date = '',"
                lblAc_Select.Text += " prev_charge_date,"
            Else
                If strPrevDate.Length = 9 Then
                    lblAc_Query.Text += " prev_charge_date = '" & strPrevDate & "',"
                    lblAc_Select.Text += " prev_charge_date,"
                End If
            End If
            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblAc_Query.Text += " update_userid = '" & txtAc_update_userid.Text & "',"
            lblAc_Query.Text += " update_tstamp = " & txtAc_update_tstamp.Text
            lblAc_Select.Text += " update_userid,update_tstamp FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtAc_customer_tkn.Text = Nothing Or txtAc_customer_acct_tkn.Text = Nothing Or txtAc_account_pkg_tkn.Text = Nothing Or txtAc_acct_component_tkn.Text = Nothing Then
                lblAc_Query.ForeColor = Drawing.Color.Red
                lblAc_Query.Font.Bold = True
                lblAc_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblAc_Query.ForeColor = Drawing.Color.Black
                lblAc_Query.Text += " WHERE customer_tkn = " & Trim(txtAc_customer_tkn.Text)
                lblAc_Query.Text += " AND customer_acct_tkn = " & Trim(txtAc_customer_acct_tkn.Text)
                lblAc_Query.Text += " AND account_pkg_tkn = " & Trim(txtAc_account_pkg_tkn.Text)
                lblAc_Query.Text += " AND acct_component_tkn = " & Trim(txtAc_acct_component_tkn.Text)

                lblAc_Select.Text += " WHERE customer_tkn = " & Trim(txtAc_customer_tkn.Text)
                lblAc_Select.Text += " AND customer_acct_tkn = " & Trim(txtAc_customer_acct_tkn.Text)
                lblAc_Select.Text += " AND account_pkg_tkn = " & Trim(txtAc_account_pkg_tkn.Text)
                lblAc_Select.Text += " AND acct_component_tkn = " & Trim(txtAc_acct_component_tkn.Text)
                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnAC]" & Err.Description)
        End Try
    End Sub

    Private Sub btnApr_ShowScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApr_ShowScript.Click
        Try
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            Dim strStartDate, strEndDate As String
            strStartDate = cboApr_start_day.SelectedValue & cboApr_start_month.SelectedValue & cboApr_start_year.SelectedValue
            strEndDate = cboApr_end_day.SelectedValue & cboApr_end_month.SelectedValue & cboApr_end_year.SelectedValue
            lblApr_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblApr_Select.Text = "Select "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboApr_status_desc.SelectedValue <> "--select--" Then
                lblApr_Query.Text += " Set status_desc = '" & cboApr_status_desc.SelectedValue & "',"
                lblApr_Select.Text += "status_desc,"
            Else
                lblApr_Query.Text += " Set"
            End If
            If chkApr_Null_start.Checked Then
                lblApr_Query.Text += " start_date = '',"
                lblApr_Select.Text += " start_date,"
            Else
                If strStartDate.Length = 9 Then
                    lblApr_Query.Text += " start_date = '" & strStartDate & "',"
                    lblApr_Select.Text += " start_date,"
                End If
            End If
            If chkApr_Null_end.Checked Then
                lblApr_Query.Text += " end_date = '',"
                lblApr_Select.Text += " end_date,"
            Else
                If strEndDate.Length = 9 Then
                    lblApr_Query.Text += " end_date = '" & strEndDate & "',"
                    lblApr_Select.Text += " end_date,"
                End If
            End If

            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblApr_Query.Text += " update_userid = '" & txtApr_update_userid.Text & "',"
            lblApr_Query.Text += " update_tstamp = " & txtApr_update_tstamp.Text
            lblApr_Select.Text += " update_userid, update_tstamp FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtApr_customer_tkn.Text = Nothing Or txtApr_customer_acct_tkn.Text = Nothing Or txtApr_account_pkg_tkn.Text = Nothing Or txtApr_acct_pricing_tkn.Text = Nothing Then
                lblApr_Query.ForeColor = Drawing.Color.Red
                lblApr_Query.Font.Bold = True
                lblApr_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblApr_Query.ForeColor = Drawing.Color.Black
                lblApr_Query.Text += " WHERE customer_tkn = " & Trim(txtApr_customer_tkn.Text)
                lblApr_Query.Text += " AND customer_acct_tkn = " & Trim(txtApr_customer_acct_tkn.Text)
                lblApr_Query.Text += " AND account_pkg_tkn = " & Trim(txtApr_account_pkg_tkn.Text)
                lblApr_Query.Text += " AND acct_pricing_tkn = " & Trim(txtApr_acct_pricing_tkn.Text)

                lblApr_Select.Text += " WHERE customer_tkn = " & Trim(txtApr_customer_tkn.Text)
                lblApr_Select.Text += " AND customer_acct_tkn = " & Trim(txtApr_customer_acct_tkn.Text)
                lblApr_Select.Text += " AND account_pkg_tkn = " & Trim(txtApr_account_pkg_tkn.Text)
                lblApr_Select.Text += " AND acct_pricing_tkn = " & Trim(txtApr_acct_pricing_tkn.Text)
                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnApr]" & Err.Description)
        End Try
    End Sub

    Private Sub btnAp_ShowScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAp_ShowScript.Click
        Try
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            Dim strEndDate As String
            strEndDate = cboAp_end_day.SelectedValue & cboAp_end_month.SelectedValue & cboAp_end_year.SelectedValue
            lblAp_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblAp_Select.Text = "SELECT "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboAp_fnl_bill_status_desc.SelectedValue <> "--select--" Then
                lblAp_Query.Text += " Set fnl_bill_status_desc = '" & cboAp_fnl_bill_status_desc.SelectedValue & "',"
                lblAp_Select.Text += " fnl_bill_status_desc,"
            Else
                lblAp_Query.Text += " Set"
            End If
            If chkAP_Null_end.Checked Then
                lblAp_Query.Text += " end_date = '',"
                lblAp_Select.Text += "end_date,"
            Else
                If strEndDate.Length = 9 Then
                    lblAp_Query.Text += " end_date = '" & strEndDate & "',"
                    lblAp_Select.Text += "end_date,"
                End If
            End If
            If cboAp_account_pkg_desc.SelectedValue <> "--select--" Then
                lblAp_Query.Text += " account_pkg_desc = '" & cboAp_account_pkg_desc.SelectedValue & "',"
                lblAp_Select.Text += "account_pkg_desc,"
            End If
            If cboAp_status_desc.SelectedValue <> "--select--" Then
                lblAp_Query.Text += " status_desc = '" & cboAp_status_desc.SelectedValue & "',"
                lblAp_Select.Text += "status_desc,"
            End If
            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblAp_Query.Text += " update_userid = '" & txtAp_update_userid.Text & "',"
            lblAp_Query.Text += " update_tstamp = " & txtAp_update_tstamp.Text
            lblAp_Select.Text += "update_userid, update_tstamp FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtAp_customer_tkn.Text = Nothing Or txtAp_customer_acct_tkn.Text = Nothing Or txtAp_account_pkg_tkn.Text = Nothing Then
                lblAp_Query.ForeColor = Drawing.Color.Red
                lblAp_Query.Font.Bold = True
                lblAp_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblAp_Query.ForeColor = Drawing.Color.Black
                lblAp_Query.Text += " WHERE customer_tkn = " & Trim(txtAp_customer_tkn.Text)
                lblAp_Query.Text += " AND customer_acct_tkn = " & Trim(txtAp_customer_acct_tkn.Text)
                lblAp_Query.Text += " AND account_pkg_tkn = " & Trim(txtAp_account_pkg_tkn.Text)

                lblAp_Select.Text += " WHERE customer_tkn = " & Trim(txtAp_customer_tkn.Text)
                lblAp_Select.Text += " AND customer_acct_tkn = " & Trim(txtAp_customer_acct_tkn.Text)
                lblAp_Select.Text += " AND account_pkg_tkn = " & Trim(txtAp_account_pkg_tkn.Text)
                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnAP]" & Err.Description)
        End Try
    End Sub

    Private Sub btnSo_ShowScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSo_ShowScript.Click
        Try
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            lblSo_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblSo_Select.Text = "SELECT "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboSo_transfer_ind.SelectedValue <> "--select--" Then
                lblSo_Query.Text += " Set transfer_ind = '" & cboSo_transfer_ind.SelectedValue & "',"
                lblSo_Select.Text += "transfer_ind,"
            Else
                lblSo_Query.Text += " Set"
            End If
            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblSo_Query.Text += " update_userid = '" & txtSo_update_userid.Text & "',"
            lblSo_Query.Text += " update_tstamp = " & txtSo_update_tstamp.Text
            lblSo_Select.Text += "update_userid, update_tstamp FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtSo_service_order_tkn.Text = Nothing Then
                lblSo_Query.ForeColor = Drawing.Color.Red
                lblSo_Query.Font.Bold = True
                lblSo_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblSo_Query.ForeColor = Drawing.Color.Black
                lblSo_Query.Font.Bold = False
                lblSo_Query.Text += " WHERE service_order_tkn = " & Trim(txtSo_service_order_tkn.Text)

                lblSo_Select.Text += " WHERE service_order_tkn = " & Trim(txtSo_service_order_tkn.Text)
                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnSo]" & Err.Description)
        End Try
    End Sub

    Private Sub btnSpa_ShowScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpa_ShowScript.Click
        Dim strStartDate, strEndDate As String
        Try
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            strStartDate = cboSpa_start_day.SelectedValue & cboSpa_start_month.SelectedValue & cboSpa_start_year.SelectedValue
            strEndDate = cboSpa_end_day.SelectedValue & cboSpa_end_month.SelectedValue & cboSpa_end_year.SelectedValue
            lblSpa_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblSpa_Select.Text = "SELECT "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboSpa_status_desc.SelectedValue <> "--select--" Then
                lblSpa_Query.Text += " Set status_desc = '" & cboSpa_status_desc.SelectedValue & "',"
                lblSpa_Select.Text += " status_desc,"
            Else
                lblSpa_Query.Text += " Set"
            End If
            If chkSpa_Null_start.Checked Then
                lblSpa_Query.Text += " start_date = '',"
                lblSpa_Select.Text += "start_date,"
            Else
                If strStartDate.Length = 9 Then
                    lblSpa_Query.Text += " start_date = '" & strStartDate & "',"
                    lblSpa_Select.Text += "start_date,"
                End If
            End If
            If chkSpa_Null_end.Checked Then
                lblSpa_Query.Text += " end_date = '',"
                lblSpa_Select.Text += " end_date,"
            Else
                If strEndDate.Length = 9 Then
                    lblSpa_Query.Text += " end_date = '" & strEndDate & "',"
                    lblSpa_Select.Text += " end_date,"
                End If
            End If
            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblSpa_Query.Text += " update_userid = '" & txtSpa_update_userid.Text & "'"
            lblSpa_Select.Text += "update_userid FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtSpa_customer_tkn.Text = Nothing Or txtSpa_customer_acct_tkn.Text = Nothing Or txtSpa_account_pkg_tkn.Text = Nothing Or txtSpa_premise_tkn.Text = Nothing Or txtSpa_service_point_tkn.Text = Nothing Or txtSpa_srvce_pnt_assn_tkn.Text = Nothing Then
                lblSpa_Query.ForeColor = Drawing.Color.Red
                lblSpa_Query.Font.Bold = True
                lblSpa_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblSpa_Query.ForeColor = Drawing.Color.Black
                lblSpa_Query.Text += " WHERE customer_tkn = " & Trim(txtSpa_customer_tkn.Text)
                lblSpa_Query.Text += " AND customer_acct_tkn = " & Trim(txtSpa_customer_acct_tkn.Text)
                lblSpa_Query.Text += " AND account_pkg_tkn = " & Trim(txtSpa_account_pkg_tkn.Text)
                lblSpa_Query.Text += " AND premise_tkn = " & Trim(txtSpa_premise_tkn.Text)
                lblSpa_Query.Text += " AND service_point_tkn = " & Trim(txtSpa_service_point_tkn.Text)
                lblSpa_Query.Text += " AND srvce_pnt_assn_tkn = " & Trim(txtSpa_srvce_pnt_assn_tkn.Text)

                lblSpa_Select.Text += " WHERE customer_tkn = " & Trim(txtSpa_customer_tkn.Text)
                lblSpa_Select.Text += " AND customer_acct_tkn = " & Trim(txtSpa_customer_acct_tkn.Text)
                lblSpa_Select.Text += " AND account_pkg_tkn = " & Trim(txtSpa_account_pkg_tkn.Text)
                lblSpa_Select.Text += " AND premise_tkn = " & Trim(txtSpa_premise_tkn.Text)
                lblSpa_Select.Text += " AND service_point_tkn = " & Trim(txtSpa_service_point_tkn.Text)
                lblSpa_Select.Text += " AND srvce_pnt_assn_tkn = " & Trim(txtSpa_srvce_pnt_assn_tkn.Text)
                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnSpa]" & Err.Description)
        End Try
    End Sub
    Private Sub btnPay_ShowScript_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Pay_ShowScript.Click
        Try
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            lblPay_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblPay_Select.Text = "SELECT "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboPay_Ret_rsn_desc.SelectedValue <> "--select--" Then
                lblPay_Query.Text += " Set return_reason_desc = '" & cboPay_Ret_rsn_desc.SelectedValue & "',"
                lblPay_Select.Text += " return_reason_desc,"
            Else
                lblAc_Query.Text += " Set"
            End If
            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblPay_Query.Text += " update_userid = '" & txtPay_update_userid.Text & "',"
            lblPay_Query.Text += " approval_userid = '" & txtPay_Ap_userid.Text & "',"
            lblPay_Query.Text += " return_userid = '" & txtPay_ret_userid.Text & "',"
            lblPay_Query.Text += " return_tstamp = " & txtPay_ret_tstamp.Text & ","
            lblPay_Query.Text += " update_tstamp = " & txtPay_update_tstamp.Text
            lblPay_Select.Text += " update_userid,approval_userid,return_userid,return_tstamp,update_tstamp FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtPay_customer_tkn.Text = Nothing Or txtPay_Bill_pkg_tkn.Text = Nothing Or txtPay_pmt_tkn.Text = Nothing Then
                lblPay_Query.ForeColor = Drawing.Color.Red
                lblPay_Query.Font.Bold = True
                lblPay_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblPay_Query.ForeColor = Drawing.Color.Black
                lblPay_Query.Text += " WHERE customer_tkn = " & Trim(txtPay_customer_tkn.Text)
                lblPay_Query.Text += " AND billing_pkg_tkn = " & Trim(txtPay_Bill_pkg_tkn.Text)
                lblPay_Query.Text += " AND payment_tkn = " & Trim(txtPay_pmt_tkn.Text)

                lblPay_Select.Text += " WHERE customer_tkn = " & Trim(txtPay_customer_tkn.Text)
                lblPay_Select.Text += " AND billing_pkg_tkn = " & Trim(txtPay_Bill_pkg_tkn.Text)
                lblPay_Select.Text += " AND payment_tkn = " & Trim(txtPay_pmt_tkn.Text)

                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnPay]" & Err.Description)
        End Try
    End Sub
    Protected Sub cboSpa_status_desc_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSpa_status_desc.DataBound
        cboSpa_status_desc.Items.Insert(0, "--select--")
    End Sub
    Protected Sub cboAp_fnl_bill_status_desc_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAp_fnl_bill_status_desc.DataBound
        cboAp_fnl_bill_status_desc.Items.Insert(0, "--select--")
    End Sub
    Protected Sub cboAp_account_pkg_desc_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAp_account_pkg_desc.DataBound
        cboAp_account_pkg_desc.Items.Insert(0, "--select--")
    End Sub
    Protected Sub cboAp_status_desc_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAp_status_desc.DataBound
        cboAp_status_desc.Items.Insert(0, "--select--")
    End Sub

    Protected Sub cboApr_status_desc_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApr_status_desc.DataBound
        cboApr_status_desc.Items.Insert(0, "--select--")
    End Sub

    Protected Sub cboEquip_status_desc_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEquip_status_desc.DataBound
        cboEquip_status_desc.Items.Insert(0, "--select--")
    End Sub
    Private Sub cboTableName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTableName.SelectedIndexChanged
        Try
            pnlService_pnt_assign.Visible = False
            pnlService_order.Visible = False
            pnlAccount_package.Visible = False
            pnlAccount_pricing.Visible = False
            pnlAccount_component.Visible = False
            pnlPayment.Visible = False
            pnlEquipment.Visible = False
            pnlLogView.Visible = False
            lblQuery.Visible = False
            Select Case cboTableName.SelectedValue
                Case "service_pnt_assign"
                    pnlService_pnt_assign.Visible = True
                    txtSpa_update_userid.Text = username
                    txtSpa_update_userid.Enabled = False
                Case "service_order"
                    pnlService_order.Visible = True
                    txtSo_update_userid.Text = username
                    txtSo_update_userid.Enabled = False
                Case "account_package"
                    pnlAccount_package.Visible = True
                    txtAp_update_userid.Text = username
                    txtAp_update_userid.Enabled = False
                Case "account_pricing"
                    pnlAccount_pricing.Visible = True
                    txtApr_update_userid.Text = username
                    txtApr_update_userid.Enabled = False
                Case "account_component"
                    pnlAccount_component.Visible = True
                    txtAc_update_userid.Text = username
                    txtAc_update_userid.Enabled = False
                Case "payment"
                    pnlPayment.Visible = True
                    txtPay_update_userid.Text = username
                    txtPay_update_userid.Enabled = False
                    txtPay_Ap_userid.Text = username
                    txtPay_Ap_userid.Enabled = False
                    txtPay_ret_userid.Text = username
                    txtPay_ret_userid.Enabled = False
                Case "equipment"
                    pnlEquipment.Visible = True
                    txtEquip_update_userid.Text = username
                    txtEquip_update_userid.Enabled = False
                Case "--select table--"
                    lblQuery.ForeColor = Drawing.Color.Red
                    lblQuery.Text = "No table selected."
                Case Else
                    lblQuery.ForeColor = Drawing.Color.Red
                    lblQuery.Text = "Table not found."
            End Select
        Catch
            logData("[cboTableName]" & Err.Description)
        End Try
    End Sub


    Private Sub btn_Equip_ShowScript_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Equip_ShowScript.Click
        Try
            pnlLogView.Visible = False
            lblQuery.Text = Nothing
            lblEquip_Query.Text = "UPDATE " & cboTableName.SelectedValue
            lblEquip_Select.Text = "SELECT "
            '***************************************************************************************************************************
            'Set clause validation, line by line                                                                                       *
            '***************************************************************************************************************************
            If cboEquip_status_desc.SelectedValue <> "--select--" Then
                lblEquip_Query.Text += " Set status_desc = '" & cboEquip_status_desc.SelectedValue & "',"
                lblEquip_Select.Text += "status_desc,"
            Else
                lblEquip_Query.Text += " Set"
            End If
            '***************************************************************************************************************************
            'pre populated                                                                                                             *
            '***************************************************************************************************************************
            lblEquip_Query.Text += " update_userid = '" & txtEquip_update_userid.Text & "',"
            lblEquip_Query.Text += " update_tstamp = " & txtEquip_update_tstamp.Text
            lblEquip_Select.Text += "update_userid, update_tstamp FROM " & cboTableName.SelectedValue
            '***************************************************************************************************************************
            'Where clause validation, all or nothing                                                                                   *
            '***************************************************************************************************************************
            If txtEquip_equipment_tkn.Text = Nothing Then
                lblEquip_Query.ForeColor = Drawing.Color.Red
                lblEquip_Query.Font.Bold = True
                lblEquip_Query.Text = "You must enter a value for all of the required fields"
            Else
                lblEquip_Query.ForeColor = Drawing.Color.Black
                lblEquip_Query.Font.Bold = False
                lblEquip_Query.Text += " WHERE equipment_tkn = " & Trim(txtEquip_equipment_tkn.Text)

                lblEquip_Select.Text += " WHERE equipment_tkn = " & Trim(txtEquip_equipment_tkn.Text)
                btnExecScript.Visible = True
            End If
        Catch
            logData("[btnEquip]" & Err.Description)
        End Try
    End Sub
End Class
