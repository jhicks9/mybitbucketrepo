﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="maintenance_portalapps_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="list-group">
                <div class="list-group-item active"><span class="fa fa-briefcase"></span>&nbsp;Portal Applications</div>
                <a href="~/maintenance/portalapps/vslist.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Vulnerable Software List<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/portalapps/netmeter.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Net Meter-Solar Rebate Queue<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/portalapps/cwdatafix.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Customer Watch Data Fix<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/portalapps/imcp.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-bar-chart"></span>&nbsp;Denial Graphs<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/portalapps/interruptible.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Interruptible Application<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/portalapps/performancereview.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-file-text-o"></span>&nbsp;Performance Review<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/web/Default.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-wrench"></span>&nbsp;Web Maintenance<span class="pull-right fa fa-chevron-right"></span>
                </a>
                <a href="~/maintenance/lvgas/Default.aspx" runat="server" class="list-group-item">
                    <span class="fa fa-wrench"></span>&nbsp;EDG Maintenance<span class="pull-right fa fa-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</asp:Content>

