﻿Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports system.Xml.Linq
Imports System.linq
Imports System.Collections.Generic

Partial Class Maintenance_PortalApps_vslist
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'encrypt the viewstate when saving sql statements across postbacks
        Page.RegisterRequiresViewStateEncryption()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If ps.isAdmin(ps.getPageName) Then
            pnlAuthorized.Visible = True
            pnlNotAuthorized.Visible = False
            pnlAdministration.Visible = True
        Else
            pnlAdministration.Visible = False
            If Not ps.isAuthorized(ps.getPageName) Then
                pnlAuthorized.Visible = False
                pnlNotAuthorized.Visible = True
            End If
        End If

        If Not Page.IsPostBack Then
            lblPageTitle.Text = "Vulnerable Software Report"
            Dim xmlLastUpdate As datetime = getXMLLastUpdate
            If DateDiff(DateInterval.Day, xmlLastUpdate, Now) >= 7 Then 'last upate more than a month
                updateXMLData 'need to update now
                xmlLastUpdate = getXMLLastUpdate 'get updated time
            End If
            lblxmlDate.Text = String.Format("{0:d} xml database last update", xmlLastUpdate.ToString)
            getCPELastUpdate
        End If

        Dim view As New DataView(buildReport)
        view.sort = "title asc"
        gvReport.DataSource = view
        gvReport.DataBind()
        buildCPESearch
    End Sub

    Protected Sub lbUpdateXML_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUpdateXML.Click
        updateXMLData
    End Sub

    Public Function getXMLLastUpdate() As datetime
        Dim lastUpdate As DateTime = Now
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Try
            conn.Open()
            cmd.CommandText = "SELECT vslist.lastupdate from vslist where vslist.controlrecord = 1"
            drLookup = cmd.ExecuteReader  'check for control record
            If drLookup.HasRows Then
                While drLookup.Read
                    lastUpdate = drLookup("lastupdate")
                End While
            Else
                updateXMLData 'need to update now
                lastUpdate = Now
            End If
            drLookup.Close()
        Catch ex As Exception
            If ex.Message.Contains("Invalid object name") Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.vslist(id int IDENTITY(1,1) NOT NULL,controlrecord bit NOT NULL,userid varchar(25) null,lastupdate datetime NULL,title varchar(128) NULL,version varchar(25) NULL,cpe varchar(128) NULL,filename varchar(50) NULL,filebytes varbinary(max) NULL,CONSTRAINT PK_vslist PRIMARY KEY CLUSTERED (id ASC))"
                cmd.ExecuteNonQuery()
            Else
            End If
            logData("[getXMLLastUpdate]" & ex.Message)
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return lastupdate
    End Function

    Public Sub updateXMLData
        Dim crExists As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim insertQuery As String = "insert into vslist (controlrecord,lastupdate,filename,filebytes) VALUES (1,GETDATE(),@filename,@filebytes)"
        Dim updateQuery As String = "update vslist set lastupdate=GETDATE(),filename=@filename,filebytes=@filebytes where vslist.controlrecord = 1"
        Try
            conn.Open()
            cmd.CommandText = "SELECT count(*)as total from vslist where vslist.controlrecord = 1"
            drLookup = cmd.ExecuteReader  'check for control record
            If drLookup.HasRows Then
                While drLookup.Read
                    crExists = drLookup("total").ToString
                End While
            End If
            drLookup.Close()

            Dim client As webclient = new WebClient()
            Using ms As System.IO.Stream = client.OpenRead("http://static.nvd.nist.gov/feeds/xml/cve/nvdcve-2.0-Modified.xml.zip")

            'load stream from file
            'Dim byteArray As Byte() = File.ReadAllBytes("d:\temp\nvdcve-2.0-modified.xml.zip")
            'Using ms as new memorystream
            '    ms.Write(byteArray, 0, CInt(byteArray.Length))
            '    ms.Position = 0 'reset position
            'End Using

                Dim zipInputStream As New ICSharpCode.SharpZipLib.Zip.ZipInputStream(ms)
	            Dim zipEntry As ICSharpCode.SharpZipLib.Zip.ZipEntry = zipInputStream.GetNextEntry()
	            While zipEntry IsNot Nothing
		            Using sr As new system.IO.memorystream '= System.IO.File.Create(path)
			            ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(zipInputStream, sr, New Byte(4095) {})


                        sr.Position = 0
                        Dim filedata = New Byte(sr.length - 1) {}
                        sr.Read(filedata, 0, sr.length)
                        cmd.Parameters.AddWithValue("@filebytes", filedata)
                        cmd.Parameters.AddWithValue("@filename", zipEntry.name)


                        ''write stream to disk ------------------------------------------
                        'sr.Position = 0
                        'Dim data = New Byte(sr.length - 1) {}
                        'sr.Read(data, 0, sr.length)
                        'Dim path = System.IO.Path.Combine("d:\temp\", zipEntry.name)
                        'Using sw = New FileStream(path, FileMode.Create)
                        '    sw.Write(data, 0, data.Length)
                        'End Using
                        ''end write to disk ---------------------------------------------

		            End Using
		            zipEntry = zipInputStream.GetNextEntry()
	            End While
            End Using

            If crExists Then  'if control record exists then update
                cmd.CommandText = updateQuery 
            Else  'insert a new control record
                cmd.CommandText = insertQuery
            End If
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            logData("[updateXMLData]" & ex.Message)
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub lbUpdateCPE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUpdateCPE.Click
        getCPEData
    End Sub

    Public Sub getCPELastUpdate
        Dim ns As xNamespace = "http://cpe.mitre.org/dictionary/2.0"
        Try
            If Not System.IO.File.Exists(System.IO.Path.GetTempPath().ToString & "official-cpe-dictionary_v2.3.xml") Then 'create get new xml file
                getCPEData
            End If
            Dim root As XElement = XElement.Load(System.IO.Path.GetTempPath().ToString & "official-cpe-dictionary_v2.3.xml")
            Dim version As IEnumerable (of XElement) = _
                From generator In root.Elements(ns + "generator") _
                select generator
            for each gEl as xelement in version
                lblCPEDate.Text = getDateFormat(gEl.Element(ns + "timestamp").Value) & " cpe database last update"
            Next
        Catch ex As Exception
            logData("[getCPELastUpdate]" & ex.Message)
        End Try
    End Sub

    Public Sub getCPEData
        Try
            Dim client As webclient = new WebClient()
            Using ms As System.IO.Stream = client.OpenRead("http://static.nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.zip")

                Dim zipInputStream As New ICSharpCode.SharpZipLib.Zip.ZipInputStream(ms)
	            Dim zipEntry As ICSharpCode.SharpZipLib.Zip.ZipEntry = zipInputStream.GetNextEntry()
	            While zipEntry IsNot Nothing
		            Using sr As new system.IO.memorystream '= System.IO.File.Create(path)
			            ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(zipInputStream, sr, New Byte(4095) {})

                        'write stream to disk ------------------------------------------
                        sr.Position = 0
                        Dim data = New Byte(sr.length - 1) {}
                        sr.Read(data, 0, sr.length)
                        Dim path = System.IO.Path.Combine(System.IO.Path.GetTempPath().ToString, zipEntry.name)
                        Using sw = New FileStream(path, FileMode.Create)
                            sw.Write(data, 0, data.Length)
                        End Using
                        'end write to disk ---------------------------------------------

		            End Using
		            zipEntry = zipInputStream.GetNextEntry()
	            End While
            End Using
        Catch ex As Exception
            logData("[getCPEData]" & ex.Message)
        End Try        
    End Sub

    Public function buildReport() As datatable
        Dim dt As New DataTable()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim vuln As xNamespace = "http://scap.nist.gov/schema/vulnerability/0.4"
        Dim ns as XNamespace = "http://scap.nist.gov/schema/feed/vulnerability/2.0"           
        Dim data() As Byte = Nothing
        Dim search As New List(Of String)
        Try
            conn.Open()
            cmd.CommandText = "SELECT vslist.title,vslist.cpe from vslist where vslist.controlrecord = 0"
            drLookup = cmd.ExecuteReader  'get titles to query
            If drLookup.HasRows Then
                While drLookup.Read
                    If String.IsNullOrEmpty (drLookup("cpe").ToString) Then  'add title if cpe is empty
                        If Not String.IsNullOrEmpty (drLookup("title").ToString) Then
                            search.Add(drLookup("title").ToString.ToLower)
                        End If
                    Else
                        search.Add(drLookup("cpe").ToString.ToLower)
                    End If
                End While
            End If
            drLookup.Close()

            cmd.CommandText = "SELECT top 1 vslist.filebytes from vslist where vslist.controlrecord = 1"
            drLookup = cmd.ExecuteReader  'check for control record
            If drLookup.HasRows Then
                While drLookup.Read
                    data = drLookup("filebytes")
                End While
            End If
            drLookup.Close()
            conn.Close()

            Dim dr As DataRow
            dt.Columns.Add(New DataColumn("company", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("title", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("version", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("id", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("published", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("modified", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("summary", System.Type.GetType("System.String")))

            Using sr As new system.IO.memorystream(data)
                sr.Position = 0            
                Dim streamReader As New StreamReader(sr,Encoding.GetEncoding("ISO-8859-1"))
                Dim root As XElement = XElement.Load(streamReader)
                'requires Imports <xmlns:vuln="http://scap.nist.gov/schema/vulnerability/0.4">
                'Dim products As IEnumerable (of XElement) = _
                '    From product In root.<entry>.<vuln:vulnerable-software-list>.Descendants  _
                '    Where product.Value.Contains("firefox") _
                '    select product
                'From product In root.<entry>.<vuln:vulnerable-software-list>.Descendants  _
                Dim products As IEnumerable (of XElement) = _
                    From product In root.Elements(ns + "entry").Elements(vuln + "vulnerable-software-list").Descendants  _
                    Where search.Any(function(val) product.Value.ToLower.Contains(val)) _
                    select product

                for each el as xelement in products
                    dr = dt.NewRow()
                    Dim count As Integer = el.value.Split(":").Length - 1                    
                    If count >= 4 then
                        dr("version") = el.value.Split(":")(4).Trim
                        dr("title") = el.value.Split(":")(3).Trim
                        dr("company") = el.value.Split(":")(2).Trim
                    ElseIf count >= 3 Then
                        dr("title") = el.value.Split(":")(3).Trim
                        dr("company") = el.value.Split(":")(2).Trim
                    ElseIf count >= 2 Then
                        dr("company") = el.value.Split(":")(2).Trim
                    End If
                    dr("id") = el.parent.parent.Element(vuln + "cve-id").Value
                    dr("published") = getDateFormat(el.parent.parent.Element(vuln + "published-datetime").Value)
                    dr("modified") = getDateFormat(el.parent.parent.Element(vuln + "last-modified-datetime").Value)
                    dr("summary") = el.parent.parent.Element(vuln + "summary").Value
                    dt.Rows.Add(dr)
                    'lblStatus.Text = lblStatus.Text & el.Value & "<br/>" & "<br/>"
                Next                
            End Using
        Catch ex As Exception
            logData("[buildReport]" & ex.Message)
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return dt
    End Function

    Public Function getDateFormat(ByVal input As String) As String
        Dim formattedDate As String = ""
        Try
            Dim stripDate As String = input.Split("T")(0).Trim
            formattedDate = stripDate.Split("-")(1).Trim & "-" & stripDate.Split("-")(2).Trim & "-" & stripDate.Split("-")(0).Trim
        Catch
        End Try
        Return formattedDate
    End Function

    Protected Sub gvSoftware_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSoftware.RowCommand
        If e.CommandName = "Select" Then
            gvCPESearchResults.DataSource = nothing 'empty search results gridview
            gvCPESearchResults.DataBind ()
            editData (e.CommandArgument)
            pnlSoftwareDetails.Visible = True
            pnlSoftwareList.Visible = False
        End If
    End Sub

    Public Sub editData(Optional ByVal id As Integer = 0)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd = New SqlCommand("", conn)
        Dim dr As SqlDataReader
        Try
            hdnAction.Value = "update"
            conn.Open()
            cmd.CommandText = "select vslist.id,vslist.title,vslist.cpe,vslist.lastupdate from vslist where vslist.controlrecord = 0 and vslist.id = @id"
            cmd.Parameters.AddWithValue("@id", id)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    hdnID.Value = dr("id").ToString
                    txtTitle.Text = dr("title").ToString
                    lblcpe.Text = dr("cpe").ToString
                End While
            End If
            dr.Close()
            conn.Close()
        Catch ex As Exception
            logData("[editData]" & ex.Message)
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub

    Public Sub clearFormData()
        txtTitle.Text = ""
        lblCPE.Text = ""
    End Sub

    Public Sub updateData()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim query As String = ""
        Try
            Select Case hdnAction.Value
                Case "update"
                    query = "update vslist set controlrecord=0,title=@title,version=@version,cpe=@cpe,userid=@userid,lastupdate=@lastupdate" & _
                        " where id=@id"
                Case "insert"
                    query = "insert into vslist (controlrecord,title,version,cpe,userid,lastupdate" & _
                        ") " & _
                        "values (0,@title,@version,@cpe,@userid,@lastupdate" & _
                        ")"
                Case "delete"
                    query = "delete from vslist where vslist.id=@id"
            End Select
                
            cmd.CommandText = query
            cmd.Parameters.AddWithValue("@id", hdnID.Value)
            cmd.Parameters.AddWithValue("@title", txtTitle.Text)
            cmd.Parameters.AddWithValue("@version", DBNull.Value)
            cmd.Parameters.AddWithValue("@cpe", lblCPE.text)
            cmd.Parameters.AddWithValue("@userid", ps.getUsername)
            cmd.Parameters.AddWithValue("@lastupdate", DateTime.now)

            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            cmd.Dispose()
            conn.Dispose()

            clearFormData()
            dsSoftware.DataBind()
            gvSoftware.DataBind()
        Catch ex As Exception
            logData("[updateData]" & ex.Message)
            If conn.State <> ConnectionState.Open Then conn.Close()
        End Try
    End Sub

    Public Sub buildCPESearch()
        Dim ns As xNamespace = "http://cpe.mitre.org/dictionary/2.0"
        Dim search As New List(Of String)
        Try
            If Not String.IsNullOrEmpty (txtTitle.Text) Then
                search.Add(txtTitle.Text.ToLower)
                Dim dt As New DataTable()
                Dim dr As DataRow
                dt.Columns.Add(New DataColumn("title", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("cpe", System.Type.GetType("System.String")))
                Dim root As XElement = XElement.Load(System.IO.Path.GetTempPath().ToString & "official-cpe-dictionary_v2.3.xml")
                Dim softwarelist As IEnumerable(Of XElement) = _
                    From item In root.Elements(ns + "cpe-item") _
                    Where search.Any( Function(val) item.Element(ns + "title").Value.ToLower.contains(val) ) _
                    Select item
                For Each item As XElement In softwarelist
                    dr = dt.NewRow()
                    dr("title") =  item.Element(ns + "title").Value
                    dr("cpe") = item.Attribute("name").Value
                    dt.Rows.Add(dr)
                Next
                Dim view As New DataView(dt)
                view.sort = "title desc"
                gvCPESearchResults.DataSource = view
                gvCPESearchResults.DataBind ()
            Else
                gvCPESearchResults.DataSource = nothing 'empty search results gridview
                gvCPESearchResults.DataBind ()
            End If
        Catch ex As Exception
            logData("[buildCPESearch]" & ex.Message)
        End Try
    End Sub

    Protected Sub lbOutputExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbOutputExcel.Click
        Try
            Dim dTable As DataTable = buildReport 
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "Vulnerable Software Report-" & DateTime.Now.ToString("yyyy-MM-dd") & ".xls"))
            HttpContext.Current.Response.ContentType = "application/ms-excel"
            If dTable IsNot Nothing Then
				For Each dc As DataColumn In dTable.Columns
						'sep = ";";
                    'If dc.ColumnName = "controlrecord" Or dc.ColumnName = "id" Then 'skip id and controlrecord column
                    'Else
                        HttpContext.Current.Response.Write(dc.ColumnName & vbTab)
                    'End If
				Next
				Response.Write(System.Environment.NewLine)
				For Each dr As DataRow In dTable.Rows
					For i As Integer = 0 To dTable.Columns.Count - 1
                        'If i = 0 or i = 1 Then 'skip id and controlrecord column
                        'Else
                            HttpContext.Current.Response.Write(dr(i).ToString() & vbTab)
                        'End If
					Next
					HttpContext.Current.Response.Write(vbLf)
				Next
				HttpContext.Current.Response.[End]()
			End If
        Catch
        End Try
    End Sub

    Protected Sub lbSearchCPE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearchCPE.Click
        buildCPESearch 
    End Sub

    Protected Sub lbReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbReport.Click
        lblPageTitle.Text = "Vulnerable Software Report"
        txtTitle.Text = "" 'empty title box
        buildCPESearch  'rebuild gridview with empty value
        pnlSoftwareDetails.Visible = false
        pnlSoftwareList.Visible = False
        pnlReport.Visible = true
    End Sub

    Protected Sub lbList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSoftwareList.Click
        lblPageTitle.Text = "Edit Software List"
        txtTitle.Text = "" 'empty title box
        buildCPESearch  'rebuild gridview with empty value
        pnlSoftwareDetails.Visible = false
        pnlReport.Visible = False
        pnlSoftwareList.Visible = true
    End Sub

    Protected Sub lbSoftwareClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSoftwareClose.Click
        gvCPESearchResults.DataSource = nothing 'empty search results gridview
        gvCPESearchResults.DataBind ()
        pnlSoftwareDetails.Visible = false
        pnlSoftwareList.Visible = true
    End Sub

    Protected Sub lbAddSoftware_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddSoftware.Click
        hdnAction.Value = "insert"
        clearFormData()
        txtTitle.Text = "" 'empty title box
        buildCPESearch  'rebuild gridview with empty value
        pnlSoftwareList.Visible = False        
        pnlSoftwareDetails.Visible = True
    End Sub

    Protected Sub lbSoftwareUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSoftwareUpdate.Click
        updateData()
        txtTitle.Text = "" 'empty title box
        buildCPESearch  'rebuild gridview with empty value
        pnlSoftwareDetails.Visible = False
        pnlSoftwareList.Visible = True
    End Sub

    Protected Sub lbSoftwareDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSoftwareDelete.Click
        hdnAction.Value = "delete"
        updateData()
        txtTitle.Text = "" 'empty title box
        buildCPESearch  'rebuild gridview with empty value
        pnlSoftwareDetails.Visible = False
        pnlSoftwareList.Visible = True
    End Sub

    Protected Sub gvReport_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReport.PageIndexChanging
        gvReport.PageIndex = e.NewPageIndex
        gvReport.DataBind()
    End Sub

    Protected Sub gvCPESearchResults_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvCPESearchResults.PageIndexChanging
        gvCPESearchResults.PageIndex = e.NewPageIndex
        buildCPESearch
    End Sub

    Protected Sub gvCPESearchResults_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvCPESearchResults.RowCommand
        If e.CommandName = "Select" Then
            txtTitle.Text = e.CommandSource.Text.ToString
            lblCPE.Text = e.CommandArgument
            gvCPESearchResults.DataSource = nothing 'empty search results gridview
            gvCPESearchResults.DataBind ()
        end if
    End Sub

    Public Function logData(ByVal msg As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim logCmd As New SqlCommand("insert into log (date,username,web) values(@rightnow,'vslist', @msg)", conn)
        Try
            msg = msg.Replace("'", "''") 
            conn.Open()
           
            logCmd.Parameters.AddWithValue("@msg", msg)
            logCmd.Parameters.AddWithValue("@rightnow", DateTime.Now.ToString)
            logCmd.ExecuteNonQuery()
            ' delete old logs - only keep 2 weeks
            logCmd.CommandText = "delete from log where (date <= GETDATE() - 14) and (username = 'vslist')"
            logCmd.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            If ex.Message.Contains("Invalid object name") Then  'table doesnt exist
                logCmd.CommandText = "CREATE TABLE dbo.log(id int IDENTITY(1,1) NOT NULL,ipaddress varchar(25) NULL,date datetime NULL,refer varchar(256) NULL,username varchar(50) NULL,platform varchar(25) NULL,browser varchar(25) NULL,web varchar(128) NULL,html varchar(max) NULL, CONSTRAINT [PK_logs] PRIMARY KEY CLUSTERED (id ASC))"
                logCmd.ExecuteNonQuery()
            Else
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return false
        End Try
        Return True
    End Function
End Class
