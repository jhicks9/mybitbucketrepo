﻿<%@ Page Title="" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="CWDataFix.aspx.vb" Inherits="Maintenance_PortalApps_CWDataFix" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>
<%@ Register src="~/controls/ajaxModalPopup.ascx" tagname="ModalPopup" tagprefix="am1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
        <br />
    </asp:Panel>

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <am1:ModalPopup ID="modalPopup1" runat="server" />
        <asp:UpdatePanel ID="updCWFix" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-condensed table-bordered">
		                    <tr>
			                    <td class="field-label col-xs-3 active">Table Name
			                    <td class="col-md-9">
				                    <asp:dropdownlist id="cboTableName" runat="server" AutoPostBack="True" CssClass="form-control">
					                    <ASP:LISTITEM Value="--select table--">--select table--</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="service_pnt_assign">service_pnt_assign</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="service_order">service_order</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="account_package">account_package</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="account_pricing">account_pricing</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="account_component">account_component</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="payment">payment</ASP:LISTITEM>
					                    <ASP:LISTITEM Value="equipment">equipment</ASP:LISTITEM>
				                    </asp:dropdownlist>
			                    </td>
		                    </tr>
	                    </table>
                    </div>
                </div>

                <asp:Panel id="pnlService_pnt_assign" runat="server" Visible="False">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active">Select Criteria</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<asp:Label id="lblSpa_status_desc" Runat="server" Visible="True" Text="status_desc" />
							</td>
							<td class="col-md-9">								
								<ASP:DROPDOWNLIST id="cboSpa_status_desc" runat="server" DataSourceID="sqlSPa_status_desc" Visible="True" DataValueField="STATUS_DESC" DataTextField="STATUS_DESC" CssClass="form-control">
								</ASP:DROPDOWNLIST>
                                <asp:SqlDataSource ID="sqlSPa_status_desc" SelectCommand="Select distinct status_desc from service_pnt_assign" ConnectionString="<%$ ConnectionStrings:CWPortal %>" runat="server" ProviderName="System.Data.OracleClient"></asp:SqlDataSource>
							</td>  
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_start_date" Runat="server" Visible="True">start_date</ASP:LABEL>
							</td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboSpa_start_day" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-d-">-d-</ASP:LISTITEM>
									<ASP:LISTITEM Value="01">01</ASP:LISTITEM>
									<ASP:LISTITEM Value="02">02</ASP:LISTITEM>
									<ASP:LISTITEM Value="03">03</ASP:LISTITEM>
									<ASP:LISTITEM Value="04">04</ASP:LISTITEM>
									<ASP:LISTITEM Value="05">05</ASP:LISTITEM>
									<ASP:LISTITEM Value="06">06</ASP:LISTITEM>
									<ASP:LISTITEM Value="07">07</ASP:LISTITEM>
									<ASP:LISTITEM Value="08">08</ASP:LISTITEM>
									<ASP:LISTITEM Value="09">09</ASP:LISTITEM>
									<ASP:LISTITEM Value="10">10</ASP:LISTITEM>
									<ASP:LISTITEM Value="11">11</ASP:LISTITEM>
									<ASP:LISTITEM Value="12">12</ASP:LISTITEM>
									<ASP:LISTITEM Value="13">13</ASP:LISTITEM>
									<ASP:LISTITEM Value="14">14</ASP:LISTITEM>
									<ASP:LISTITEM Value="15">15</ASP:LISTITEM>
									<ASP:LISTITEM Value="16">16</ASP:LISTITEM>
									<ASP:LISTITEM Value="17">17</ASP:LISTITEM>
									<ASP:LISTITEM Value="18">18</ASP:LISTITEM>
									<ASP:LISTITEM Value="19">19</ASP:LISTITEM>
									<ASP:LISTITEM Value="20">20</ASP:LISTITEM>
									<ASP:LISTITEM Value="21">21</ASP:LISTITEM>
									<ASP:LISTITEM Value="22">22</ASP:LISTITEM>
									<ASP:LISTITEM Value="23">23</ASP:LISTITEM>
									<ASP:LISTITEM Value="24">24</ASP:LISTITEM>
									<ASP:LISTITEM Value="25">25</ASP:LISTITEM>
									<ASP:LISTITEM Value="26">26</ASP:LISTITEM>
									<ASP:LISTITEM Value="27">27</ASP:LISTITEM>
									<ASP:LISTITEM Value="28">28</ASP:LISTITEM>
									<ASP:LISTITEM Value="29">29</ASP:LISTITEM>
									<ASP:LISTITEM Value="30">30</ASP:LISTITEM>
									<ASP:LISTITEM Value="31">31</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboSpa_start_month" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-mon-">-mon-</ASP:LISTITEM>
									<ASP:LISTITEM Value="jan">January</ASP:LISTITEM>
									<ASP:LISTITEM Value="feb">February</ASP:LISTITEM>
									<ASP:LISTITEM Value="mar">March</ASP:LISTITEM>
									<ASP:LISTITEM Value="apr">April</ASP:LISTITEM>
									<ASP:LISTITEM Value="may">May</ASP:LISTITEM>
									<ASP:LISTITEM Value="jun">June</ASP:LISTITEM>
									<ASP:LISTITEM Value="jul">July</ASP:LISTITEM>
									<ASP:LISTITEM Value="aug">August</ASP:LISTITEM>
									<ASP:LISTITEM Value="sep">September</ASP:LISTITEM>
									<ASP:LISTITEM Value="oct">October</ASP:LISTITEM>
									<ASP:LISTITEM Value="nov">November</ASP:LISTITEM>
									<ASP:LISTITEM Value="dec">December</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboSpa_start_year" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-y-">-yr-</ASP:LISTITEM>
									<ASP:LISTITEM Value="1995">1995</ASP:LISTITEM>
									<ASP:LISTITEM Value="1996">1996</ASP:LISTITEM>
									<ASP:LISTITEM Value="1997">1997</ASP:LISTITEM>
									<ASP:LISTITEM Value="1998">1998</ASP:LISTITEM>
									<ASP:LISTITEM Value="1999">1999</ASP:LISTITEM>
									<ASP:LISTITEM Value="2000">2000</ASP:LISTITEM>
									<ASP:LISTITEM Value="2001">2001</ASP:LISTITEM>
									<ASP:LISTITEM Value="2002">2002</ASP:LISTITEM>
									<ASP:LISTITEM Value="2003">2003</ASP:LISTITEM>
									<ASP:LISTITEM Value="2004">2004</ASP:LISTITEM>
									<ASP:LISTITEM Value="2005">2005</ASP:LISTITEM>
									<ASP:LISTITEM Value="2006">2006</ASP:LISTITEM>
									<ASP:LISTITEM Value="2007">2007</ASP:LISTITEM>
									<ASP:LISTITEM Value="2008">2008</ASP:LISTITEM>
									<ASP:LISTITEM Value="2009">2009</ASP:LISTITEM>
									<ASP:LISTITEM Value="2010">2010</ASP:LISTITEM>
									<ASP:LISTITEM Value="2011">2011</ASP:LISTITEM>
									<ASP:LISTITEM Value="2012">2012</ASP:LISTITEM>
									<ASP:LISTITEM Value="2013">2013</ASP:LISTITEM>
									<ASP:LISTITEM Value="2014">2014</ASP:LISTITEM>
									<ASP:LISTITEM Value="2015">2015</ASP:LISTITEM>
									<ASP:LISTITEM Value="2016">2016</ASP:LISTITEM>
									<ASP:LISTITEM Value="2017">2017</ASP:LISTITEM>
									<ASP:LISTITEM Value="2018">2018</ASP:LISTITEM>
									<ASP:LISTITEM Value="2019">2019</ASP:LISTITEM>
									<ASP:LISTITEM Value="2020">2020</ASP:LISTITEM>
									<ASP:LISTITEM Value="2021">2021</ASP:LISTITEM>
									<ASP:LISTITEM Value="2022">2022</ASP:LISTITEM>
									<ASP:LISTITEM Value="2023">2023</ASP:LISTITEM>
									<ASP:LISTITEM Value="2024">2024</ASP:LISTITEM>
									<ASP:LISTITEM Value="2025">2025</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
                                <ASP:CHECKBOX id="chkSpa_Null_start" runat="server" Text="Null" CssClass="form-control"></ASP:CHECKBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_end_date" Runat="server" Visible="True">end_date</ASP:LABEL>
							</td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboSpa_end_day" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-d-">-d-</ASP:LISTITEM>
									<ASP:LISTITEM Value="01">01</ASP:LISTITEM>
									<ASP:LISTITEM Value="02">02</ASP:LISTITEM>
									<ASP:LISTITEM Value="03">03</ASP:LISTITEM>
									<ASP:LISTITEM Value="04">04</ASP:LISTITEM>
									<ASP:LISTITEM Value="05">05</ASP:LISTITEM>
									<ASP:LISTITEM Value="06">06</ASP:LISTITEM>
									<ASP:LISTITEM Value="07">07</ASP:LISTITEM>
									<ASP:LISTITEM Value="08">08</ASP:LISTITEM>
									<ASP:LISTITEM Value="09">09</ASP:LISTITEM>
									<ASP:LISTITEM Value="10">10</ASP:LISTITEM>
									<ASP:LISTITEM Value="11">11</ASP:LISTITEM>
									<ASP:LISTITEM Value="12">12</ASP:LISTITEM>
									<ASP:LISTITEM Value="13">13</ASP:LISTITEM>
									<ASP:LISTITEM Value="14">14</ASP:LISTITEM>
									<ASP:LISTITEM Value="15">15</ASP:LISTITEM>
									<ASP:LISTITEM Value="16">16</ASP:LISTITEM>
									<ASP:LISTITEM Value="17">17</ASP:LISTITEM>
									<ASP:LISTITEM Value="18">18</ASP:LISTITEM>
									<ASP:LISTITEM Value="19">19</ASP:LISTITEM>
									<ASP:LISTITEM Value="20">20</ASP:LISTITEM>
									<ASP:LISTITEM Value="21">21</ASP:LISTITEM>
									<ASP:LISTITEM Value="22">22</ASP:LISTITEM>
									<ASP:LISTITEM Value="23">23</ASP:LISTITEM>
									<ASP:LISTITEM Value="24">24</ASP:LISTITEM>
									<ASP:LISTITEM Value="25">25</ASP:LISTITEM>
									<ASP:LISTITEM Value="26">26</ASP:LISTITEM>
									<ASP:LISTITEM Value="27">27</ASP:LISTITEM>
									<ASP:LISTITEM Value="28">28</ASP:LISTITEM>
									<ASP:LISTITEM Value="29">29</ASP:LISTITEM>
									<ASP:LISTITEM Value="30">30</ASP:LISTITEM>
									<ASP:LISTITEM Value="31">31</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboSpa_end_month" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-mon-">-mon-</ASP:LISTITEM>
									<ASP:LISTITEM Value="jan">January</ASP:LISTITEM>
									<ASP:LISTITEM Value="feb">February</ASP:LISTITEM>
									<ASP:LISTITEM Value="mar">March</ASP:LISTITEM>
									<ASP:LISTITEM Value="apr">April</ASP:LISTITEM>
									<ASP:LISTITEM Value="may">May</ASP:LISTITEM>
									<ASP:LISTITEM Value="jun">June</ASP:LISTITEM>
									<ASP:LISTITEM Value="jul">July</ASP:LISTITEM>
									<ASP:LISTITEM Value="aug">August</ASP:LISTITEM>
									<ASP:LISTITEM Value="sep">September</ASP:LISTITEM>
									<ASP:LISTITEM Value="oct">October</ASP:LISTITEM>
									<ASP:LISTITEM Value="nov">November</ASP:LISTITEM>
									<ASP:LISTITEM Value="dec">December</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboSpa_end_year" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-y-">-yr-</ASP:LISTITEM>
									<ASP:LISTITEM Value="1995">1995</ASP:LISTITEM>
									<ASP:LISTITEM Value="1996">1996</ASP:LISTITEM>
									<ASP:LISTITEM Value="1997">1997</ASP:LISTITEM>
									<ASP:LISTITEM Value="1998">1998</ASP:LISTITEM>
									<ASP:LISTITEM Value="1999">1999</ASP:LISTITEM>
									<ASP:LISTITEM Value="2000">2000</ASP:LISTITEM>
									<ASP:LISTITEM Value="2001">2001</ASP:LISTITEM>
									<ASP:LISTITEM Value="2002">2002</ASP:LISTITEM>
									<ASP:LISTITEM Value="2003">2003</ASP:LISTITEM>
									<ASP:LISTITEM Value="2004">2004</ASP:LISTITEM>
									<ASP:LISTITEM Value="2005">2005</ASP:LISTITEM>
									<ASP:LISTITEM Value="2006">2006</ASP:LISTITEM>
									<ASP:LISTITEM Value="2007">2007</ASP:LISTITEM>
									<ASP:LISTITEM Value="2008">2008</ASP:LISTITEM>
									<ASP:LISTITEM Value="2009">2009</ASP:LISTITEM>
									<ASP:LISTITEM Value="2010">2010</ASP:LISTITEM>
									<ASP:LISTITEM Value="2011">2011</ASP:LISTITEM>
									<ASP:LISTITEM Value="2012">2012</ASP:LISTITEM>
									<ASP:LISTITEM Value="2013">2013</ASP:LISTITEM>
									<ASP:LISTITEM Value="2014">2014</ASP:LISTITEM>
									<ASP:LISTITEM Value="2015">2015</ASP:LISTITEM>
									<ASP:LISTITEM Value="2016">2016</ASP:LISTITEM>
									<ASP:LISTITEM Value="2017">2017</ASP:LISTITEM>
									<ASP:LISTITEM Value="2018">2018</ASP:LISTITEM>
									<ASP:LISTITEM Value="2019">2019</ASP:LISTITEM>
									<ASP:LISTITEM Value="2020">2020</ASP:LISTITEM>
									<ASP:LISTITEM Value="2021">2021</ASP:LISTITEM>
									<ASP:LISTITEM Value="2022">2022</ASP:LISTITEM>
									<ASP:LISTITEM Value="2023">2023</ASP:LISTITEM>
									<ASP:LISTITEM Value="2024">2024</ASP:LISTITEM>
									<ASP:LISTITEM Value="2025">2025</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
                                <ASP:CHECKBOX id="chkSpa_Null_end" runat="server" Text="Null" CssClass="form-control"></ASP:CHECKBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL>
							</td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_update_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							</td>
						</tr>
                        <tr>
                            <td class="field-label col-xs-3 active">Required Fields</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_customer_tkn" Runat="server" Visible="True">customer_tkn</ASP:LABEL>
							</td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_customer_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_customer_acct_tkn" Runat="server" Visible="True">customer_acct_tkn</ASP:LABEL>
							</td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_customer_acct_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_account_pkg_tkn" Runat="server" Visible="True">account_pkg_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_account_pkg_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_premise_tkn" Runat="server" Visible="True">premise_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_premise_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_service_point_tkn" Runat="server" Visible="True">service_point_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_service_point_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSpa_srvce_pnt_assn_tkn" Runat="server" Visible="True">srvce_pnt_assn_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSpa_srvce_pnt_assn_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:BUTTON id="btnSpa_ShowScript" runat="server" Visible="True" Text="ShowScript" CssClass="btn btn-default"></ASP:BUTTON>
							</td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:LABEL id="lblSpa_Query" runat="server" Visible="True"></ASP:LABEL>
								<ASP:LABEL id="lblSpa_Select" Runat="server" Visible="false"></ASP:LABEL>
							</td>
						</tr>
					</table>
                </asp:Panel><!-- pnlService_pnt_assign -->

                <asp:Panel id="pnlService_order" VISIBLE="False" RUNAT="server">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active text-center">Select Criteria</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSo_transfer_ind" Runat="server" Visible="True">transfer_ind</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboSo_transfer_ind" runat="server" Visible="True" CssClass="form-control">
									<ASP:LISTITEM Value="--select--">--select--</ASP:LISTITEM>
									<ASP:LISTITEM Value="Y">Yes</ASP:LISTITEM>
									<ASP:LISTITEM Value="N">No</ASP:LISTITEM>
								</ASP:DROPDOWNLIST></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSo_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSo_update_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSo_update_tstamp" Runat="server" Visible="True">update_tstamp</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSo_update_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3 active text-center">Required Fields</td>
                            <td class="col-md-9"></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblSo_service_order_tkn" Runat="server" Visible="True">service_order_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtSo_service_order_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:BUTTON id="btnSo_ShowScript" runat="server" Text="ShowScript" CssClass="btn btn-default"></ASP:BUTTON>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:LABEL id="lblSo_Query" runat="server"></ASP:LABEL>
								<ASP:LABEL id="lblSo_Select" Runat="server" Visible="false"></ASP:LABEL>
							</td>
						</tr>
					</table>
                </asp:Panel><!-- pnlService_order -->

                <asp:Panel id="pnlAccount_package" runat="server" Visible="False">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active text-center">Select Criteria</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_account_pkg_desc" Runat="server" Visible="True">account_pkg_desc</ASP:LABEL>
							</td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboAp_account_pkg_desc" runat="server" DataValueField="ACCOUNT_PKG_DESC" DataTextField="ACCOUNT_PKG_DESC" DataSourceID="sqlAp_account_pkg_desc" CssClass="form-control">								
								</ASP:DROPDOWNLIST></td>
								<asp:SqlDataSource ID="sqlAp_account_pkg_desc" SelectCommand="Select distinct account_pkg_desc from account_package" ConnectionString="<%$ ConnectionStrings:CWPortal %>" runat="server" ProviderName="System.Data.OracleClient"></asp:SqlDataSource>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_status_desc" Runat="server" Visible="True">status_desc</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboAp_status_desc" runat="server" DataValueField="STATUS_DESC" DataTextField="STATUS_DESC" DataSourceID="sqlAp_status_desc" CssClass="form-control">								
								</ASP:DROPDOWNLIST></td>
								<asp:SqlDataSource ID="sqlAp_status_desc" SelectCommand="Select distinct status_desc from account_package" ConnectionString="<%$ ConnectionStrings:CWPortal %>" runat="server" ProviderName="System.Data.OracleClient"></asp:SqlDataSource>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_fnl_bill_status_desc" Runat="server" Visible="True">fnl_bill_status_desc</ASP:LABEL></td>
							<td class="col-md-9">
								<asp:DropDownList id="cboAp_fnl_bill_status_desc" runat="server" Visible="True" DataValueField="FNL_BILL_STATUS_DESC" DataTextField="FNL_BILL_STATUS_DESC" DataSourceID="sqlAp_fnl_bill_status_desc" CssClass="form-control">
								</asp:DropDownList></td>
								<asp:SqlDataSource ID="sqlAp_fnl_bill_status_desc" SelectCommand="Select distinct fnl_bill_status_desc from account_package" ConnectionString="<%$ ConnectionStrings:CWPortal %>" runat="server" ProviderName="System.Data.OracleClient"></asp:SqlDataSource>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_end_date" Runat="server" Visible="True">end_date</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboAp_end_day" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-d-">-d-</ASP:LISTITEM>
									<ASP:LISTITEM Value="01">01</ASP:LISTITEM>
									<ASP:LISTITEM Value="02">02</ASP:LISTITEM>
									<ASP:LISTITEM Value="03">03</ASP:LISTITEM>
									<ASP:LISTITEM Value="04">04</ASP:LISTITEM>
									<ASP:LISTITEM Value="05">05</ASP:LISTITEM>
									<ASP:LISTITEM Value="06">06</ASP:LISTITEM>
									<ASP:LISTITEM Value="07">07</ASP:LISTITEM>
									<ASP:LISTITEM Value="08">08</ASP:LISTITEM>
									<ASP:LISTITEM Value="09">09</ASP:LISTITEM>
									<ASP:LISTITEM Value="10">10</ASP:LISTITEM>
									<ASP:LISTITEM Value="11">11</ASP:LISTITEM>
									<ASP:LISTITEM Value="12">12</ASP:LISTITEM>
									<ASP:LISTITEM Value="13">13</ASP:LISTITEM>
									<ASP:LISTITEM Value="14">14</ASP:LISTITEM>
									<ASP:LISTITEM Value="15">15</ASP:LISTITEM>
									<ASP:LISTITEM Value="16">16</ASP:LISTITEM>
									<ASP:LISTITEM Value="17">17</ASP:LISTITEM>
									<ASP:LISTITEM Value="18">18</ASP:LISTITEM>
									<ASP:LISTITEM Value="19">19</ASP:LISTITEM>
									<ASP:LISTITEM Value="20">20</ASP:LISTITEM>
									<ASP:LISTITEM Value="21">21</ASP:LISTITEM>
									<ASP:LISTITEM Value="22">22</ASP:LISTITEM>
									<ASP:LISTITEM Value="23">23</ASP:LISTITEM>
									<ASP:LISTITEM Value="24">24</ASP:LISTITEM>
									<ASP:LISTITEM Value="25">25</ASP:LISTITEM>
									<ASP:LISTITEM Value="26">26</ASP:LISTITEM>
									<ASP:LISTITEM Value="27">27</ASP:LISTITEM>
									<ASP:LISTITEM Value="28">28</ASP:LISTITEM>
									<ASP:LISTITEM Value="29">29</ASP:LISTITEM>
									<ASP:LISTITEM Value="30">30</ASP:LISTITEM>
									<ASP:LISTITEM Value="31">31</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboAp_end_month" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-mon-">-mon-</ASP:LISTITEM>
									<ASP:LISTITEM Value="jan">January</ASP:LISTITEM>
									<ASP:LISTITEM Value="feb">February</ASP:LISTITEM>
									<ASP:LISTITEM Value="mar">March</ASP:LISTITEM>
									<ASP:LISTITEM Value="apr">April</ASP:LISTITEM>
									<ASP:LISTITEM Value="may">May</ASP:LISTITEM>
									<ASP:LISTITEM Value="jun">June</ASP:LISTITEM>
									<ASP:LISTITEM Value="jul">July</ASP:LISTITEM>
									<ASP:LISTITEM Value="aug">August</ASP:LISTITEM>
									<ASP:LISTITEM Value="sep">September</ASP:LISTITEM>
									<ASP:LISTITEM Value="oct">October</ASP:LISTITEM>
									<ASP:LISTITEM Value="nov">November</ASP:LISTITEM>
									<ASP:LISTITEM Value="dec">December</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboAp_end_year" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-y-">-yr-</ASP:LISTITEM>
									<ASP:LISTITEM Value="1995">1995</ASP:LISTITEM>
									<ASP:LISTITEM Value="1996">1996</ASP:LISTITEM>
									<ASP:LISTITEM Value="1997">1997</ASP:LISTITEM>
									<ASP:LISTITEM Value="1998">1998</ASP:LISTITEM>
									<ASP:LISTITEM Value="1999">1999</ASP:LISTITEM>
									<ASP:LISTITEM Value="2000">2000</ASP:LISTITEM>
									<ASP:LISTITEM Value="2001">2001</ASP:LISTITEM>
									<ASP:LISTITEM Value="2002">2002</ASP:LISTITEM>
									<ASP:LISTITEM Value="2003">2003</ASP:LISTITEM>
									<ASP:LISTITEM Value="2004">2004</ASP:LISTITEM>
									<ASP:LISTITEM Value="2005">2005</ASP:LISTITEM>
									<ASP:LISTITEM Value="2006">2006</ASP:LISTITEM>
									<ASP:LISTITEM Value="2007">2007</ASP:LISTITEM>
									<ASP:LISTITEM Value="2008">2008</ASP:LISTITEM>
									<ASP:LISTITEM Value="2009">2009</ASP:LISTITEM>
									<ASP:LISTITEM Value="2010">2010</ASP:LISTITEM>
									<ASP:LISTITEM Value="2011">2011</ASP:LISTITEM>
									<ASP:LISTITEM Value="2012">2012</ASP:LISTITEM>
									<ASP:LISTITEM Value="2013">2013</ASP:LISTITEM>
									<ASP:LISTITEM Value="2014">2014</ASP:LISTITEM>
									<ASP:LISTITEM Value="2015">2015</ASP:LISTITEM>
									<ASP:LISTITEM Value="2016">2016</ASP:LISTITEM>
									<ASP:LISTITEM Value="2017">2017</ASP:LISTITEM>
									<ASP:LISTITEM Value="2018">2018</ASP:LISTITEM>
									<ASP:LISTITEM Value="2019">2019</ASP:LISTITEM>
									<ASP:LISTITEM Value="2020">2020</ASP:LISTITEM>
									<ASP:LISTITEM Value="2021">2021</ASP:LISTITEM>
									<ASP:LISTITEM Value="2022">2022</ASP:LISTITEM>
									<ASP:LISTITEM Value="2023">2023</ASP:LISTITEM>
									<ASP:LISTITEM Value="2024">2024</ASP:LISTITEM>
									<ASP:LISTITEM Value="2025">2025</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:CHECKBOX id="chkAP_Null_end" runat="server" Text="Null" CssClass="form-control"></ASP:CHECKBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAp_update_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_update_tstamp" Runat="server" Visible="True">update_tstamp</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAp_update_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3 active">Required Fields</td>
                            <td class="col-md-9"></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_customer_tkn" Runat="server" Visible="True">customer_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAp_customer_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_customer_acct_tkn" Runat="server" Visible="True">customer_acct_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAp_customer_acct_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAp_account_pkg_tkn" Runat="server" Visible="True">account_pkg_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAp_account_pkg_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:BUTTON id="btnAp_ShowScript" runat="server" Text="ShowScript" CssClass="btn btn-default"></ASP:BUTTON></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:LABEL id="lblAp_Query" runat="server"></ASP:LABEL>
								<ASP:LABEL id="lblAp_Select" Runat="server" Visible="false"></ASP:LABEL>
							</td>
						</tr>
					</table>
                </asp:Panel><!-- pnlAccount_package -->

                <asp:Panel id="pnlAccount_pricing" runat="server" Visible="False">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active text-center">Select Criteria</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_status_desc" Runat="server" Visible="True">status_desc</ASP:LABEL></td>
							<td class="col-md-9">
								<asp:DropDownList id="cboApr_status_desc" runat="server" DataValueField="STATUS_DESC" DataTextField="STATUS_DESC" DataSourceID="sqlApr_status_desc" CssClass="form-control">
								</asp:DropDownList></td>
								<asp:SqlDataSource ID="sqlApr_status_desc" SelectCommand="Select distinct status_desc from account_pricing" ConnectionString="<%$ ConnectionStrings:CWPortal %>" runat="server" ProviderName="System.Data.OracleClient"></asp:SqlDataSource>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_start_date" Runat="server" Visible="True">start_date</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboApr_start_day" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-d-">-d-</ASP:LISTITEM>
									<ASP:LISTITEM Value="01">01</ASP:LISTITEM>
									<ASP:LISTITEM Value="02">02</ASP:LISTITEM>
									<ASP:LISTITEM Value="03">03</ASP:LISTITEM>
									<ASP:LISTITEM Value="04">04</ASP:LISTITEM>
									<ASP:LISTITEM Value="05">05</ASP:LISTITEM>
									<ASP:LISTITEM Value="06">06</ASP:LISTITEM>
									<ASP:LISTITEM Value="07">07</ASP:LISTITEM>
									<ASP:LISTITEM Value="08">08</ASP:LISTITEM>
									<ASP:LISTITEM Value="09">09</ASP:LISTITEM>
									<ASP:LISTITEM Value="10">10</ASP:LISTITEM>
									<ASP:LISTITEM Value="11">11</ASP:LISTITEM>
									<ASP:LISTITEM Value="12">12</ASP:LISTITEM>
									<ASP:LISTITEM Value="13">13</ASP:LISTITEM>
									<ASP:LISTITEM Value="14">14</ASP:LISTITEM>
									<ASP:LISTITEM Value="15">15</ASP:LISTITEM>
									<ASP:LISTITEM Value="16">16</ASP:LISTITEM>
									<ASP:LISTITEM Value="17">17</ASP:LISTITEM>
									<ASP:LISTITEM Value="18">18</ASP:LISTITEM>
									<ASP:LISTITEM Value="19">19</ASP:LISTITEM>
									<ASP:LISTITEM Value="20">20</ASP:LISTITEM>
									<ASP:LISTITEM Value="21">21</ASP:LISTITEM>
									<ASP:LISTITEM Value="22">22</ASP:LISTITEM>
									<ASP:LISTITEM Value="23">23</ASP:LISTITEM>
									<ASP:LISTITEM Value="24">24</ASP:LISTITEM>
									<ASP:LISTITEM Value="25">25</ASP:LISTITEM>
									<ASP:LISTITEM Value="26">26</ASP:LISTITEM>
									<ASP:LISTITEM Value="27">27</ASP:LISTITEM>
									<ASP:LISTITEM Value="28">28</ASP:LISTITEM>
									<ASP:LISTITEM Value="29">29</ASP:LISTITEM>
									<ASP:LISTITEM Value="30">30</ASP:LISTITEM>
									<ASP:LISTITEM Value="31">31</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboApr_start_month" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-mon-">-mon-</ASP:LISTITEM>
									<ASP:LISTITEM Value="jan">January</ASP:LISTITEM>
									<ASP:LISTITEM Value="feb">February</ASP:LISTITEM>
									<ASP:LISTITEM Value="mar">March</ASP:LISTITEM>
									<ASP:LISTITEM Value="apr">April</ASP:LISTITEM>
									<ASP:LISTITEM Value="may">May</ASP:LISTITEM>
									<ASP:LISTITEM Value="jun">June</ASP:LISTITEM>
									<ASP:LISTITEM Value="jul">July</ASP:LISTITEM>
									<ASP:LISTITEM Value="aug">August</ASP:LISTITEM>
									<ASP:LISTITEM Value="sep">September</ASP:LISTITEM>
									<ASP:LISTITEM Value="oct">October</ASP:LISTITEM>
									<ASP:LISTITEM Value="nov">November</ASP:LISTITEM>
									<ASP:LISTITEM Value="dec">December</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboApr_start_year" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-y-">-yr-</ASP:LISTITEM>
									<ASP:LISTITEM Value="1995">1995</ASP:LISTITEM>
									<ASP:LISTITEM Value="1996">1996</ASP:LISTITEM>
									<ASP:LISTITEM Value="1997">1997</ASP:LISTITEM>
									<ASP:LISTITEM Value="1998">1998</ASP:LISTITEM>
									<ASP:LISTITEM Value="1999">1999</ASP:LISTITEM>
									<ASP:LISTITEM Value="2000">2000</ASP:LISTITEM>
									<ASP:LISTITEM Value="2001">2001</ASP:LISTITEM>
									<ASP:LISTITEM Value="2002">2002</ASP:LISTITEM>
									<ASP:LISTITEM Value="2003">2003</ASP:LISTITEM>
									<ASP:LISTITEM Value="2004">2004</ASP:LISTITEM>
									<ASP:LISTITEM Value="2005">2005</ASP:LISTITEM>
									<ASP:LISTITEM Value="2006">2006</ASP:LISTITEM>
									<ASP:LISTITEM Value="2007">2007</ASP:LISTITEM>
									<ASP:LISTITEM Value="2008">2008</ASP:LISTITEM>
									<ASP:LISTITEM Value="2009">2009</ASP:LISTITEM>
									<ASP:LISTITEM Value="2010">2010</ASP:LISTITEM>
									<ASP:LISTITEM Value="2011">2011</ASP:LISTITEM>
									<ASP:LISTITEM Value="2012">2012</ASP:LISTITEM>
									<ASP:LISTITEM Value="2013">2013</ASP:LISTITEM>
									<ASP:LISTITEM Value="2014">2014</ASP:LISTITEM>
									<ASP:LISTITEM Value="2015">2015</ASP:LISTITEM>
									<ASP:LISTITEM Value="2016">2016</ASP:LISTITEM>
									<ASP:LISTITEM Value="2017">2017</ASP:LISTITEM>
									<ASP:LISTITEM Value="2018">2018</ASP:LISTITEM>
									<ASP:LISTITEM Value="2019">2019</ASP:LISTITEM>
									<ASP:LISTITEM Value="2020">2020</ASP:LISTITEM>
									<ASP:LISTITEM Value="2021">2021</ASP:LISTITEM>
									<ASP:LISTITEM Value="2022">2022</ASP:LISTITEM>
									<ASP:LISTITEM Value="2023">2023</ASP:LISTITEM>
									<ASP:LISTITEM Value="2024">2024</ASP:LISTITEM>
									<ASP:LISTITEM Value="2025">2025</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
                                <ASP:CHECKBOX id="chkApr_Null_start" runat="server" Text="Null" CssClass="form-control"></ASP:CHECKBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_end_date" Runat="server" Visible="True">end_date</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboApr_end_day" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-d-">-d-</ASP:LISTITEM>
									<ASP:LISTITEM Value="01">01</ASP:LISTITEM>
									<ASP:LISTITEM Value="02">02</ASP:LISTITEM>
									<ASP:LISTITEM Value="03">03</ASP:LISTITEM>
									<ASP:LISTITEM Value="04">04</ASP:LISTITEM>
									<ASP:LISTITEM Value="05">05</ASP:LISTITEM>
									<ASP:LISTITEM Value="06">06</ASP:LISTITEM>
									<ASP:LISTITEM Value="07">07</ASP:LISTITEM>
									<ASP:LISTITEM Value="08">08</ASP:LISTITEM>
									<ASP:LISTITEM Value="09">09</ASP:LISTITEM>
									<ASP:LISTITEM Value="10">10</ASP:LISTITEM>
									<ASP:LISTITEM Value="11">11</ASP:LISTITEM>
									<ASP:LISTITEM Value="12">12</ASP:LISTITEM>
									<ASP:LISTITEM Value="13">13</ASP:LISTITEM>
									<ASP:LISTITEM Value="14">14</ASP:LISTITEM>
									<ASP:LISTITEM Value="15">15</ASP:LISTITEM>
									<ASP:LISTITEM Value="16">16</ASP:LISTITEM>
									<ASP:LISTITEM Value="17">17</ASP:LISTITEM>
									<ASP:LISTITEM Value="18">18</ASP:LISTITEM>
									<ASP:LISTITEM Value="19">19</ASP:LISTITEM>
									<ASP:LISTITEM Value="20">20</ASP:LISTITEM>
									<ASP:LISTITEM Value="21">21</ASP:LISTITEM>
									<ASP:LISTITEM Value="22">22</ASP:LISTITEM>
									<ASP:LISTITEM Value="23">23</ASP:LISTITEM>
									<ASP:LISTITEM Value="24">24</ASP:LISTITEM>
									<ASP:LISTITEM Value="25">25</ASP:LISTITEM>
									<ASP:LISTITEM Value="26">26</ASP:LISTITEM>
									<ASP:LISTITEM Value="27">27</ASP:LISTITEM>
									<ASP:LISTITEM Value="28">28</ASP:LISTITEM>
									<ASP:LISTITEM Value="29">29</ASP:LISTITEM>
									<ASP:LISTITEM Value="30">30</ASP:LISTITEM>
									<ASP:LISTITEM Value="31">31</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboApr_end_month" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-mon-">-mon-</ASP:LISTITEM>
									<ASP:LISTITEM Value="jan">January</ASP:LISTITEM>
									<ASP:LISTITEM Value="feb">February</ASP:LISTITEM>
									<ASP:LISTITEM Value="mar">March</ASP:LISTITEM>
									<ASP:LISTITEM Value="apr">April</ASP:LISTITEM>
									<ASP:LISTITEM Value="may">May</ASP:LISTITEM>
									<ASP:LISTITEM Value="jun">June</ASP:LISTITEM>
									<ASP:LISTITEM Value="jul">July</ASP:LISTITEM>
									<ASP:LISTITEM Value="aug">August</ASP:LISTITEM>
									<ASP:LISTITEM Value="sep">September</ASP:LISTITEM>
									<ASP:LISTITEM Value="oct">October</ASP:LISTITEM>
									<ASP:LISTITEM Value="nov">November</ASP:LISTITEM>
									<ASP:LISTITEM Value="dec">December</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboApr_end_year" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-y-">-yr-</ASP:LISTITEM>
									<ASP:LISTITEM Value="1995">1995</ASP:LISTITEM>
									<ASP:LISTITEM Value="1996">1996</ASP:LISTITEM>
									<ASP:LISTITEM Value="1997">1997</ASP:LISTITEM>
									<ASP:LISTITEM Value="1998">1998</ASP:LISTITEM>
									<ASP:LISTITEM Value="1999">1999</ASP:LISTITEM>
									<ASP:LISTITEM Value="2000">2000</ASP:LISTITEM>
									<ASP:LISTITEM Value="2001">2001</ASP:LISTITEM>
									<ASP:LISTITEM Value="2002">2002</ASP:LISTITEM>
									<ASP:LISTITEM Value="2003">2003</ASP:LISTITEM>
									<ASP:LISTITEM Value="2004">2004</ASP:LISTITEM>
									<ASP:LISTITEM Value="2005">2005</ASP:LISTITEM>
									<ASP:LISTITEM Value="2006">2006</ASP:LISTITEM>
									<ASP:LISTITEM Value="2007">2007</ASP:LISTITEM>
									<ASP:LISTITEM Value="2008">2008</ASP:LISTITEM>
									<ASP:LISTITEM Value="2009">2009</ASP:LISTITEM>
									<ASP:LISTITEM Value="2010">2010</ASP:LISTITEM>
									<ASP:LISTITEM Value="2011">2011</ASP:LISTITEM>
									<ASP:LISTITEM Value="2012">2012</ASP:LISTITEM>
									<ASP:LISTITEM Value="2013">2013</ASP:LISTITEM>
									<ASP:LISTITEM Value="2014">2014</ASP:LISTITEM>
									<ASP:LISTITEM Value="2015">2015</ASP:LISTITEM>
									<ASP:LISTITEM Value="2016">2016</ASP:LISTITEM>
									<ASP:LISTITEM Value="2017">2017</ASP:LISTITEM>
									<ASP:LISTITEM Value="2018">2018</ASP:LISTITEM>
									<ASP:LISTITEM Value="2019">2019</ASP:LISTITEM>
									<ASP:LISTITEM Value="2020">2020</ASP:LISTITEM>
									<ASP:LISTITEM Value="2021">2021</ASP:LISTITEM>
									<ASP:LISTITEM Value="2022">2022</ASP:LISTITEM>
									<ASP:LISTITEM Value="2023">2023</ASP:LISTITEM>
									<ASP:LISTITEM Value="2024">2024</ASP:LISTITEM>
									<ASP:LISTITEM Value="2025">2025</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:CHECKBOX id="chkApr_Null_end" runat="server" Text="Null" CssClass="form-control"></ASP:CHECKBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtApr_update_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_update_tstamp" Runat="server" Visible="True">update_tstamp</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtApr_update_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX></td>
						</tr>
                        <tr>
                            <td class="field-label col-xs-3 active text-center">Required Fields</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_customer_tkn" Runat="server" Visible="True">customer_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtApr_customer_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_customer_acct_tkn" Runat="server" Visible="True">customer_acct_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtApr_customer_acct_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_account_pkg_tkn" Runat="server" Visible="True">account_pkg_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtApr_account_pkg_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblApr_acct_pricing_tkn" Runat="server" Visible="True">acct_pricing_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtApr_acct_pricing_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:BUTTON id="btnApr_ShowScript" runat="server" Text="ShowScript" CssClass="btn btn-default"></ASP:BUTTON>
							</td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:LABEL id="lblApr_Query" runat="server"></ASP:LABEL>
								<ASP:LABEL id="lblApr_Select" Runat="server" Visible="false"></ASP:LABEL>
							</td>
						</tr>
					</table>
                </asp:Panel><!-- pnlAccount_pricing -->

                <asp:Panel id="pnlAccount_component" runat="server" Visible="False">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active text-center">Select Criteria</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_charged_ind" Runat="server" Visible="True">charged_ind</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboAc_charged_ind" runat="server" Visible="True" CssClass="form-control">
									<ASP:LISTITEM Value="">--select--</ASP:LISTITEM>
									<ASP:LISTITEM Value="Y">Yes</ASP:LISTITEM>
									<ASP:LISTITEM Value="N">No</ASP:LISTITEM>
								</ASP:DROPDOWNLIST></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_prev_charge_date" Runat="server" Visible="True">prev_charge_date</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:DROPDOWNLIST id="cboAc_prev_charge_day" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-d-">-d-</ASP:LISTITEM>
									<ASP:LISTITEM Value="01">01</ASP:LISTITEM>
									<ASP:LISTITEM Value="02">02</ASP:LISTITEM>
									<ASP:LISTITEM Value="03">03</ASP:LISTITEM>
									<ASP:LISTITEM Value="04">04</ASP:LISTITEM>
									<ASP:LISTITEM Value="05">05</ASP:LISTITEM>
									<ASP:LISTITEM Value="06">06</ASP:LISTITEM>
									<ASP:LISTITEM Value="07">07</ASP:LISTITEM>
									<ASP:LISTITEM Value="08">08</ASP:LISTITEM>
									<ASP:LISTITEM Value="09">09</ASP:LISTITEM>
									<ASP:LISTITEM Value="10">10</ASP:LISTITEM>
									<ASP:LISTITEM Value="11">11</ASP:LISTITEM>
									<ASP:LISTITEM Value="12">12</ASP:LISTITEM>
									<ASP:LISTITEM Value="13">13</ASP:LISTITEM>
									<ASP:LISTITEM Value="14">14</ASP:LISTITEM>
									<ASP:LISTITEM Value="15">15</ASP:LISTITEM>
									<ASP:LISTITEM Value="16">16</ASP:LISTITEM>
									<ASP:LISTITEM Value="17">17</ASP:LISTITEM>
									<ASP:LISTITEM Value="18">18</ASP:LISTITEM>
									<ASP:LISTITEM Value="19">19</ASP:LISTITEM>
									<ASP:LISTITEM Value="20">20</ASP:LISTITEM>
									<ASP:LISTITEM Value="21">21</ASP:LISTITEM>
									<ASP:LISTITEM Value="22">22</ASP:LISTITEM>
									<ASP:LISTITEM Value="23">23</ASP:LISTITEM>
									<ASP:LISTITEM Value="24">24</ASP:LISTITEM>
									<ASP:LISTITEM Value="25">25</ASP:LISTITEM>
									<ASP:LISTITEM Value="26">26</ASP:LISTITEM>
									<ASP:LISTITEM Value="27">27</ASP:LISTITEM>
									<ASP:LISTITEM Value="28">28</ASP:LISTITEM>
									<ASP:LISTITEM Value="29">29</ASP:LISTITEM>
									<ASP:LISTITEM Value="30">30</ASP:LISTITEM>
									<ASP:LISTITEM Value="31">31</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboAc_prev_charge_month" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-mon-">-mon-</ASP:LISTITEM>
									<ASP:LISTITEM Value="jan">January</ASP:LISTITEM>
									<ASP:LISTITEM Value="feb">February</ASP:LISTITEM>
									<ASP:LISTITEM Value="mar">March</ASP:LISTITEM>
									<ASP:LISTITEM Value="apr">April</ASP:LISTITEM>
									<ASP:LISTITEM Value="may">May</ASP:LISTITEM>
									<ASP:LISTITEM Value="jun">June</ASP:LISTITEM>
									<ASP:LISTITEM Value="jul">July</ASP:LISTITEM>
									<ASP:LISTITEM Value="aug">August</ASP:LISTITEM>
									<ASP:LISTITEM Value="sep">September</ASP:LISTITEM>
									<ASP:LISTITEM Value="oct">October</ASP:LISTITEM>
									<ASP:LISTITEM Value="nov">November</ASP:LISTITEM>
									<ASP:LISTITEM Value="dec">December</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
								<ASP:DROPDOWNLIST id="cboAc_prev_charge_year" runat="server" CssClass="form-control">
									<ASP:LISTITEM Selected="true" Value="-y-">-yr-</ASP:LISTITEM>
									<ASP:LISTITEM Value="1995">1995</ASP:LISTITEM>
									<ASP:LISTITEM Value="1996">1996</ASP:LISTITEM>
									<ASP:LISTITEM Value="1997">1997</ASP:LISTITEM>
									<ASP:LISTITEM Value="1998">1998</ASP:LISTITEM>
									<ASP:LISTITEM Value="1999">1999</ASP:LISTITEM>
									<ASP:LISTITEM Value="2000">2000</ASP:LISTITEM>
									<ASP:LISTITEM Value="2001">2001</ASP:LISTITEM>
									<ASP:LISTITEM Value="2002">2002</ASP:LISTITEM>
									<ASP:LISTITEM Value="2003">2003</ASP:LISTITEM>
									<ASP:LISTITEM Value="2004">2004</ASP:LISTITEM>
									<ASP:LISTITEM Value="2005">2005</ASP:LISTITEM>
									<ASP:LISTITEM Value="2006">2006</ASP:LISTITEM>
									<ASP:LISTITEM Value="2007">2007</ASP:LISTITEM>
									<ASP:LISTITEM Value="2008">2008</ASP:LISTITEM>
									<ASP:LISTITEM Value="2009">2009</ASP:LISTITEM>
									<ASP:LISTITEM Value="2010">2010</ASP:LISTITEM>
									<ASP:LISTITEM Value="2011">2011</ASP:LISTITEM>
									<ASP:LISTITEM Value="2012">2012</ASP:LISTITEM>
									<ASP:LISTITEM Value="2013">2013</ASP:LISTITEM>
									<ASP:LISTITEM Value="2014">2014</ASP:LISTITEM>
									<ASP:LISTITEM Value="2015">2015</ASP:LISTITEM>
									<ASP:LISTITEM Value="2016">2016</ASP:LISTITEM>
									<ASP:LISTITEM Value="2017">2017</ASP:LISTITEM>
									<ASP:LISTITEM Value="2018">2018</ASP:LISTITEM>
									<ASP:LISTITEM Value="2019">2019</ASP:LISTITEM>
									<ASP:LISTITEM Value="2020">2020</ASP:LISTITEM>
									<ASP:LISTITEM Value="2021">2021</ASP:LISTITEM>
									<ASP:LISTITEM Value="2022">2022</ASP:LISTITEM>
									<ASP:LISTITEM Value="2023">2023</ASP:LISTITEM>
									<ASP:LISTITEM Value="2024">2024</ASP:LISTITEM>
									<ASP:LISTITEM Value="2025">2025</ASP:LISTITEM>
								</ASP:DROPDOWNLIST>
                                <ASP:CHECKBOX id="chkAc_Null_prev" runat="server" Text="Null" CssClass="form-control"></ASP:CHECKBOX>
							</td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAc_update_userid" runat="server" Visible="True" CssClass="form-control no-border"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_update_tstamp" Runat="server" Visible="True">update_tstamp</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAc_update_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX></td>
						</tr>
                        <tr>
                            <td class="field-label col-xs-3 active text-center">Required Fields</td>
                            <td class="col-md-9"></td>
                        </tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_customer_tkn" Runat="server" Visible="True">customer_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAc_customer_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_customer_acct_tkn" Runat="server" Visible="True">customer_acct_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAc_customer_acct_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_account_pkg_tkn" Runat="server" Visible="True">account_pkg_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAc_account_pkg_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
							<td class="field-label col-xs-3 active">
								<ASP:LABEL id="lblAc_acct_component_tkn" Runat="server" Visible="True">acct_component_tkn</ASP:LABEL></td>
							<td class="col-md-9">
								<ASP:TEXTBOX id="txtAc_acct_component_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:BUTTON id="btnAc_ShowScript" runat="server" Text="ShowScript" CssClass="btn btn-default"></ASP:BUTTON>
							</td>
						</tr>
						<tr>
                            <td class="field-label col-xs-3"></td>
							<td class="col-md-9">
								<ASP:LABEL id="lblAc_Query" runat="server"></ASP:LABEL>
								<ASP:LABEL id="lblAc_Select" Runat="server" Visible="false"></ASP:LABEL>
							</td>
						</tr>
                    </table>
                </asp:Panel>

                <asp:Panel id="pnlPayment" VISIBLE="False" RUNAT="server">
                    <div class="row">
                        <div class="col-lg-12">
                                <table class="table table-condensed table-bordered">
                                    <tr>
                                        <td class="field-label col-xs-3 active text-center">Select Criteria</td>
                                        <td class="col-md-9"></td>
                                    </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_Ret_rsn_desc" Runat="server" Visible="True">return_reason_desc</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:DROPDOWNLIST id="cboPay_Ret_rsn_desc" runat="server" Visible="True" CssClass="form-control">
									            <ASP:LISTITEM Value="--select--">--select--</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Account Closed">Account Closed</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Cashier Error">Cashier Error</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Credit Card">Credit Card</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Draft Reversal">Draft Reversal</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Electronic">Electronic</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Insufficient Funds">Insufficient Funds</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Mis-Applied">Mis-Applied</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Other">Other</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Post Dated">Post Dated</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Signature MIssing">Signature MIssing</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Stop Payment">Stop Payment</ASP:LISTITEM>
									            <ASP:LISTITEM Value="Uncollected Funds">Uncollected Funds</ASP:LISTITEM>
								            </ASP:DROPDOWNLIST>
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_update_userid" runat="server" Visible="True" CssClass="form-control" />
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_Ap_userid" Runat="server" Visible="True">approval_userid</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_Ap_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_ret_userid" Runat="server" Visible="True">return_userid</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_ret_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_ret_tstamp" Runat="server" Visible="True">return_tstamp</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_ret_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX>
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_update_tstamp" Runat="server" Visible="True">update_tstamp</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_update_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX>
							            </td>
						            </tr>
                                    <tr>
                                        <td class="field-label col-xs-3 active text-center">Required Fields</td>
                                        <td class="col-md-9"></td>
                                    </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_customer_tkn" Runat="server" Visible="True">customer_tkn</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_customer_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_Bill_pkg_tkn" Runat="server" Visible="True">billing_pkg_tkn</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_Bill_pkg_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3 active">
								            <ASP:LABEL id="lblPay_pmt_tkn" Runat="server" Visible="True">payment_tkn</ASP:LABEL>
							            </td>
							            <td class="col-md-9">
								            <ASP:TEXTBOX id="txtPay_pmt_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX>
							            </td>
						            </tr>
						            <tr>
                                        <td class="field-label col-xs-3"></td>
							            <td class="col-md-9">
								            <ASP:BUTTON id="btn_Pay_ShowScript" runat="server" Text="ShowScript" CssClass="btn btn-default" />
							            </td>
						            </tr>
						            <tr>
							            <td class="field-label col-xs-3"></td>
							            <td>
                                            <ASP:LABEL id="lblPay_Query" runat="server"></ASP:LABEL>
								            <ASP:LABEL id="lblPay_Select" Runat="server" Visible="false"></ASP:LABEL>
							            </td>
						            </tr>
					            </table>
                            </div>
                        </div>
                </asp:Panel>

                <asp:Panel id="pnlEquipment" VISIBLE="False" RUNAT="server">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-condensed table-bordered">
                                <tr>
                                    <td class="field-label col-xs-3 active text-center">Select Criteria</td>
                                    <td class="col-md-9"></td>
                                </tr>
						        <tr>
							        <td class="field-label col-xs-3 active">
								        <ASP:LABEL id="lblEquip_status_desc" Runat="server" Visible="True">status_desc</ASP:LABEL></td>
							        <td class="col-md-9">
							        <asp:DropDownList id="cboEquip_status_desc" runat="server" DataValueField="STATUS_DESC" DataTextField="STATUS_DESC" DataSourceID="sqlEquip_status_desc" CssClass="form-control">
								        </asp:DropDownList></td>
								        <asp:SqlDataSource ID="sqlEquip_status_desc" SelectCommand="Select distinct status_desc from equipment" ConnectionString="<%$ ConnectionStrings:CWPortal %>" runat="server" ProviderName="System.Data.OracleClient"></asp:SqlDataSource>															
						        </tr>
						        <tr>
							        <td class="field-label col-xs-3 active">
								        <ASP:LABEL id="lblEquip_update_userid" Runat="server" Visible="True">update_userid</ASP:LABEL></td>
							        <td class="col-md-9">
								        <ASP:TEXTBOX id="txtEquip_update_userid" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						        </tr>
						        <tr>
							        <td class="field-label col-xs-3 active">
								        <ASP:LABEL id="lblEquip_update_tstamp" Runat="server" Visible="True">update_tstamp</ASP:LABEL></td>
							        <td class="col-md-9">
								        <ASP:TEXTBOX id="txtEquip_update_tstamp" runat="server" Visible="True" Enabled="False" CssClass="form-control no-border">sysdate</ASP:TEXTBOX></td>
						        </tr>
                                <tr>
                                    <td class="field-label col-xs-3 active text-center">Required Fields</td>
                                    <td class="col-md-9"></td>
                                </tr>
						        <tr>
							        <td class="field-label col-xs-3 active">
								        <ASP:LABEL id="lblEquip_equipment_tkn" Runat="server" Visible="True">equipment_tkn</ASP:LABEL></td>
							        <td class="col-md-9">
								        <ASP:TEXTBOX id="txtEquip_equipment_tkn" runat="server" Visible="True" CssClass="form-control"></ASP:TEXTBOX></td>
						        </tr>
						        <tr>
                                    <td class="field-label col-xs-3"></td>
							        <td class="col-md-9">
								        <ASP:BUTTON id="btn_Equip_ShowScript" runat="server" Text="ShowScript" CssClass="btn btn-default" />
							        </td>
						        </tr>
						        <tr>
                                    <td class="field-label col-xs-3"></td>
							        <td class="col-md-9">
								        <ASP:LABEL id="lblEquip_Query" runat="server"></ASP:LABEL>
								        <ASP:LABEL id="lblEquip_Select" Runat="server" Visible="false"></ASP:LABEL>
							        </td>
						        </tr>
					        </table>
                        </div>
                    </div>
                </asp:Panel><!-- pnlEquipment -->

	            <asp:Panel id="pnlLogView" Runat="server" Visible="False" >
	                <asp:Label ID="lblLog" runat="server" Visible="false" />
	            </asp:Panel><!-- pnlLogView -->

                <asp:Label id="lblQuery" Runat="server" Visible="False" />
                <asp:Label id="lblSelect" Runat="server" Visible="False" />
				<asp:Button id="btnExecScript" runat="server" Visible="False" Text="Execute" CssClass="btn btn-primary"></asp:Button>

            </ContentTemplate>
        </asp:UpdatePanel><!-- updCWFix -->
    </asp:Panel><!-- pnlAuthorized -->
</asp:Content>
