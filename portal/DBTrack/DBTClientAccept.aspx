﻿<%@ Page Title="DBTrack Client Accept" Language="VB" MasterPageFile="~/site.master" ValidateRequest="false" AutoEventWireup="false" CodeFile="DBTClientAccept.aspx.vb" Inherits="DBTrack_DBTClientAccept" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:HiddenField ID="hdnSelectedTicket" runat="server" Value="" />
    <asp:HiddenField ID="hdnGroupName" runat="server" Value="" />
    <asp:HiddenField ID="hdnConfirmationMessage" runat="server" Value="" />
    <asp:HiddenField ID="hdnRawDescription" runat="server" Value="" />

    <asp:Panel ID="pnlTickets" runat="server">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="table-responsive">
                    <asp:GridView ID="gvTickets" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" AllowPaging="true" UseAccessibleHeader="true" DataSourceID="SqlDataSourceTickets" >
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblTickets" runat="server" Text="Tickets" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbTicket" runat="server" CausesValidation="False" CommandArgument='<%#Eval("Ticket_Number") %>' CommandName="Select" ToolTip="Ticket Detail"
                                        Text='<%#Eval("Ticket_Number") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOpenDateLabel" runat="server" Text="Open Date" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblOpenDate" runat="server" Text='<%# string.Format( "{0:MM/dd/yy}", Eval("ticket_opendate"))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ticket_assignedto" HeaderText="Assigned To" />
                            <asp:BoundField DataField="ticket_statusid" HeaderText="Status" />
                            <asp:BoundField DataField="TICKET_Abstract" HeaderText="Description" />
                        </Columns>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourceTickets" runat="server" ConnectionString="<%$ ConnectionStrings:DBTrack %>"
                        SelectCommand="SELECT * FROM Tickets WHERE TICKET_ClientID=@username AND (TICKET_ApprovalName_10 IS NOT NULL and TICKET_ApprovalName_20 IS NULL and TICKET_ApprovalName_10 <> 'Rejected') or TICKET_ClientID=@username AND (TICKET_ApprovalName_50 IS NOT NULL and TICKET_ApprovalName_60 IS NULL and TICKET_ApprovalName_50 <> 'Rejected')">
                        <SelectParameters>
                            <asp:Parameter Name="username" Type="String" />
                        </SelectParameters>
	                </asp:SqlDataSource>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlDetails" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-12 ">
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td colspan="2" class="bg-info text-center"><p ><b><asp:Label ID="lblAcceptStepUpper" runat="server" Text="" /></b></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <asp:LinkButton ID="lbCloseUpper" runat="server" CssClass="btn btn-default" CausesValidation="False">Cancel</asp:LinkButton>
                            <asp:LinkButton ID="btnRejectUpper" runat="server" Text="Reject" CssClass="btn btn-warning" CausesValidation="False" OnClientClick="if (!confirm('Are you sure you want to REJECT the ticket?')) return false;" />
                            <asp:LinkButton ID="btnAcceptUpper" runat="server" Text="Accept" CssClass="btn btn-primary" CausesValidation="False" OnClientClick="if (!confirm(acceptTicketConfirmation())) return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Ticket</label></td>
                        <td class="col-md-9"><asp:Label id="lblTicket" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Description</label></td>
                        <td class="col-md-9"><asp:Label id="lblDescription" runat="server" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p><asp:label id="lblApprovalMessage" runat="server" /></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="bg-info text-center"><p><b><asp:Label ID="lblAcceptStepLower" runat="server" Text="" /></b></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <asp:LinkButton ID="lbClose" runat="server" CssClass="btn btn-default" CausesValidation="False">Cancel</asp:LinkButton>
                            <asp:LinkButton ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-warning" CausesValidation="False" OnClientClick="if (!confirm('Are you sure you want to REJECT the ticket?')) return false;" />
                            <asp:LinkButton ID="btnAccept" runat="server" Text="Accept" CssClass="btn btn-primary" CausesValidation="False" OnClientClick="if (!confirm(acceptTicketConfirmation())) return false;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="scriptSection" Runat="Server">
    <script type="text/javascript">
        function acceptTicketConfirmation() {
            var msg = document.getElementById('<%=hdnConfirmationMessage.ClientID%>').value;
            return msg;
        }
    </script>
</asp:Content>