﻿Imports System.Data.SqlClient

Partial Class DBTrack_DBTClientAccept
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        Accept()
        gvTickets.DataBind()
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub

    Protected Sub btnAcceptUpper_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAcceptUpper.Click
        Accept()
        gvTickets.DataBind()
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub

    Private Sub Accept()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("DBTrack").ConnectionString)
        Dim se As New sendEmail
        Dim toList As New ArrayList
        Dim acceptEmailSubject As String = ""
        Dim query As String = ""
        Dim AcceptMessage As String = ""
        Try
            Dim username as String = ps.getUsername
            Dim hours As integer = Now.TimeOfDay.Hours
            Dim minutes As integer = Now.TimeOfDay.Minutes
            Dim seconds As integer = Now.TimeOfDay.Seconds
            Dim ApprovalTimeOnly As datetime = "12/30/1899 " & hours & ":" & minutes & ":" & seconds '- 9 / 9 / 8

            conn.Open()
            If lblAcceptStepLower.Text = "1ST Client Approval" Then
                query = "Update dbo.Tickets SET TICKET_Approvaldate_20 = @ApprovalDate,TICKET_Approvaltime_20 = @ApprovalTime,TICKET_Approvalname_20 = @ApprovalName,TICKET_Description = @AcceptMessage Where TICKET_Number = @ApprovedTicket"
                AcceptMessage = "________________________________________________________________" & vbCrLf & "*** 1ST Client Approval On *** ( " & Today & "   " & TimeOfDay & " ) by " & username.ToUpper & vbCrLf & hdnRawDescription.Value
                acceptEmailSubject = "==> dbtrack ticket #" & hdnSelectedTicket.Value & " has been approved for work by " & username
            ElseIf lblAcceptStepLower.Text = "2ND Client Approval" Then
                query = "Update dbo.Tickets SET TICKET_Approvaldate_60 = @ApprovalDate,TICKET_Approvaltime_60 = @ApprovalTime,TICKET_Approvalname_60 = @ApprovalName,TICKET_Description = @AcceptMessage Where TICKET_Number = @ApprovedTicket"
                AcceptMessage = "________________________________________________________________" & vbCrLf & "*** 2ND Client Approval On *** ( " & Today & "   " & TimeOfDay & " ) by " & username.ToUpper & vbCrLf & hdnRawDescription.Value
                acceptEmailSubject = "==> dbtrack ticket #" & hdnSelectedTicket.Value & " awaiting Director approval for move to production by " & username
            End If

            Dim objCmd As New SqlCommand(query, conn)
            objCmd.Parameters.AddWithValue("@ApprovalDate", Today)
            objCmd.Parameters.AddWithValue("@ApprovalTime", ApprovalTimeOnly)
            objCmd.Parameters.AddWithValue("@ApprovalName", username.ToUpper)
            objCmd.Parameters.AddWithValue("@AcceptMessage", AcceptMessage)
            objCmd.Parameters.AddWithValue("@ApprovedTicket", hdnSelectedTicket.Value)
            objCmd.ExecuteNonQuery()
            conn.Close()

            If lblAcceptStepLower.Text = "1ST Client Approval" Then
                If hdnGroupName.Value = "1ST_LVL" Or hdnGroupName.Value = "2ND_LVL" Then
                    toList.Add("IJOHNSON@empiredistrict.com")
                ElseIf hdnGroupName.Value = "INFORMATION_SYSTEMS" Or hdnGroupName.Value = "WEB_DEVELOPMENT" Then
                    toList.Add("SMALCOLM@empiredistrict.com")
                Else
                    toList.Add("ladams@empiredistrict.com")
                End If
            Else
                toList.Add("ladams@empiredistrict.com") 'cc managers
                toList.Add("IJOHNSON@empiredistrict.com")
                toList.Add("SMALCOLM@empiredistrict.com")
            End If

            se.sendEMail(toList, "dbtrack@empiredistrict.com", acceptEmailSubject, "dbtrack automated email", False)
            
        Catch ex As Exception
            If conn.State = Data.ConnectionState.Open Then
                conn.close
            End If
        End Try
    End Sub

    Private Sub reject()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("DBTrack").ConnectionString)
        Dim se As New sendEmail
        Dim toList As New ArrayList
        Dim rejEmailSubject As String = ""
        Dim RejMessage As String = ""
        Dim query As String = ""
        Try
            Dim username as String = ps.getUsername
            conn.Open()
            If lblAcceptStepLower.Text = "1ST Client Approval" Then
                query = "Update Tickets SET TICKET_Approvalname_20 = @RejEntry, TICKET_Description = @RejMessage Where TICKET_Number = @RejTicket"
                RejMessage = "________________________________________________________________" & vbCrLf & "*** 1ST Client REJECTED On *** ( " & Today & "   " & TimeOfDay & " ) by " & username.ToUpper & vbCrLf & hdnRawDescription.Value
                rejEmailSubject = "==> dbtrack ticket #" & hdnSelectedTicket.Value & " has been rejected for work by " & username
            ElseIf lblAcceptStepLower.Text = "2ND Client Approval" Then
                query = "Update Tickets SET TICKET_Approvalname_60 = @RejEntry, TICKET_Description = @RejMessage Where TICKET_Number = @RejTicket"
                RejMessage = "________________________________________________________________" & vbCrLf & "*** 2ND Client REJECTED On *** ( " & Today & "   " & TimeOfDay & " ) by " & username.ToUpper & vbCrLf & hdnRawDescription.Value
                rejEmailSubject = "==> dbtrack ticket #" & hdnSelectedTicket.Value & " testing has been rejected by " & username
            End If

            Dim objCmd As New SqlCommand(query, conn)
            objCmd.Parameters.AddWithValue("@RejEntry", "Rejected")
            objCmd.Parameters.AddWithValue("@RejMessage", RejMessage)
            objCmd.Parameters.AddWithValue("@RejTicket", hdnSelectedTicket.Value)
            objCmd.ExecuteNonQuery()
            conn.Close()

            If hdnGroupName.Value = "1ST_LVL" Or hdnGroupName.Value = "2ND_LVL" Then
                toList.Add("IJOHNSON@empiredistrict.com")
            ElseIf hdnGroupName.Value = "INFORMATION_SYSTEMS" Or hdnGroupName.Value = "WEB_DEVELOPMENT" Then
                toList.Add("SMALCOLM@empiredistrict.com")
            Else
                toList.Add("ladams@empiredistrict.com")
            End If

            se.sendEMail(toList, "dbtrack@empiredistrict.com", rejEmailSubject, "dbtrack automated email", False)

        Catch ex As Exception
            If conn.State = Data.ConnectionState.Open Then
                conn.close
            End If
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        reject()
        gvTickets.DataBind()
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub

    Protected Sub btnRejectUpper_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRejectUpper.Click
        reject()
        gvTickets.DataBind()
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub

    Protected Sub gvTickets_Load(sender As Object, e As EventArgs) Handles gvTickets.Load
        SqlDataSourceTickets.SelectParameters("username").DefaultValue = ps.getUsername 
    End Sub

    Protected Sub gvTickets_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTickets.RowCommand
        Dim conn As sqlconnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DBTrack").ConnectionString)
        Dim filterDescription As String = ""
        Try
            lblAcceptStepUpper.Text = ""
            lblAcceptStepLower.Text = ""
            lblApprovalMessage.Text = ""
            hdnConfirmationMessage.value = ""
            If e.CommandName = "Select" Then
                Dim myCommand As New SqlCommand("SELECT ticket_number,ticket_description,ticket_assignedTo,Ticket_abstract,ticket_statusid,ticket_groupname,ticket_approvalname_10,ticket_approvalname_20,ticket_approvalname_50,ticket_approvalname_60 FROM tickets WHERE ticket_Number = @ticketnumber", conn)
                myCommand.Parameters.AddWithValue("@ticketnumber", e.CommandArgument)
                conn.Open()
                Dim myReader As SqlDataReader
                myReader = myCommand.ExecuteReader()
                While myReader.Read
                    lblTicket.Text = myReader.GetValue(0).ToString & "  " & myReader.GetString(3).ToString
                    lblDescription.Text = myReader.GetString(1).ToString().Replace(Environment.NewLine,"<br />")
                    ' filter description
                    'filterDescription = myReader.GetString(1).ToString
                    'filterDescription = filterDescription.Replace("<", "&lt")
                    'filterDescription = filterDescription.Replace(">", "&gt")
                    'hdnRawDescription.Value = filterDescription
                    hdnRawDescription.Value = myReader.GetString(1).ToString().Replace("<", "&lt").Replace(">", "&gt").Replace("==&gt", "")
                    '
                    hdnSelectedTicket.Value = myReader.GetValue(0).ToString
                    hdnGroupName.Value = myReader("ticket_groupname").ToString
                    If Not myReader("TICKET_ApprovalName_10") Is DBNull.Value And myReader("TICKET_ApprovalName_20") Is DBNull.Value Then
                        lblAcceptStepUpper.Text = "1ST Client Approval"
                        lblAcceptStepLower.Text = "1ST Client Approval"
                        lblApprovalMessage.Text = "Please carefully review the description for work to be performed on this ticket. If the description is not correct or if it is incomplete, please REJECT this ticket and someone will contact you for additional information."
                        hdnConfirmationMessage.value = "By ACCEPTING this ticket, you are confirming that all approvals have been received for work to be performed as described in the description of this ticket."
                    ElseIf Not myReader("TICKET_ApprovalName_50") Is DBNull.Value And myReader("TICKET_ApprovalName_60") Is DBNull.Value Then
                        lblAcceptStepUpper.Text = "2ND Client Approval"
                        lblAcceptStepLower.Text = "2ND Client Approval"
                        lblApprovalMessage.Text = "Work is now complete on this ticket and is ready to be implemented. If you have any concerns about these changes, the test results or the testing process, then please REJECT this ticket and an IT Manager will contact you for additional information."
                        hdnConfirmationMessage.value = "By ACCEPTING this ticket, you are confirming that all approvals have been received for items changed by this ticket and it is ready to be implemented. You are accepting responsibility for updates to any client created documentation that pertains to this change."
                    End If
                End While
                myReader.Close()
                conn.Close()
                pnlDetails.Visible = True
                pnlTickets.Visible = False
            End If
        Catch ex As Exception
            If conn.State = Data.ConnectionState.Open Then
                conn.close
            End If
        End Try
    End Sub

    Protected Sub lbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClose.Click
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub
    Protected Sub lbCloseUpper_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseUpper.Click
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub
End Class
