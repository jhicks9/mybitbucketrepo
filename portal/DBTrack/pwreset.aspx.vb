﻿Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement
Imports System.Data.OracleClient
Imports System.Data

Partial Class DBTrack_pwreset
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
            txtFName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + txtFName.UniqueID + "','')")
            txtLName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + txtLName.UniqueID + "','')")
            txtAccount.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + txtAccount.UniqueID + "','')")
        Catch
        End Try
    End Sub

    Public Function ToLocalTime(ByVal utcDate As Date) As Date
        Dim localDate As Date = Nothing
        If IsDate(utcDate) Then
            localDate = utcDate.ToLocalTime()
        End If
        Return localDate
    End Function

    Public Sub searchAccounts()
        Try
            Dim ctx As New PrincipalContext(ContextType.Domain)
            Dim searchUser As  UserPrincipal = new UserPrincipal(ctx)
            If Not String.IsNullOrEmpty(txtFName.text) Then
                searchUser.GivenName = txtFName.Text & "*"
            End If
            If Not String.IsNullOrEmpty(txtLName.text) Then
                searchUser.SurName = txtLName.Text & "*"
            End If
            If Not String.IsNullOrEmpty(txtAccount.text) Then
                searchUser.SamAccountName = txtAccount.Text
            End If

            Dim srch As PrincipalSearcher = New PrincipalSearcher(searchUser)
            Dim dt As New System.Data.DataTable("details")
            Dim dr As System.Data.DataRow
            Dim dc As DataColumn
            dc = New DataColumn("displayname", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("email", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("samaccountname", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            For Each found In srch.findall()
                If found.ContextType = ContextType.Domain Then
                    dr = dt.NewRow()
                    dr.Item("samaccountname") = found.SamAccountName
                    Dim de As DirectoryEntry = found.GetUnderlyingObject 
                    Dim user As UserPrincipal =  UserPrincipal.FindByIdentity(ctx, found.samaccountname)
                    dr.Item("displayname") = user.DisplayName
                    dr.Item("email") = user.EmailAddress
                    dt.Rows.Add(dr)
                End If
            Next
            gvSearchResults.DataSource = dt
            gvSearchResults.DataBind
        Catch
        End Try
    End Sub

    Public Sub getAccount(ByVal accountSam As string)
        Try
            clearForm()
            Dim ctx As New PrincipalContext(ContextType.Domain)
            Dim user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, accountSam)
            If user isnot Nothing then
                If user.SamAccountName isnot Nothing then lblAccount.Text = user.SamAccountName
                If user.GivenName isnot Nothing Then lblFirstName.Text = user.GivenName
                If user.Surname isnot Nothing Then lblLastName.Text = user.Surname
                If user.DisplayName isnot Nothing Then lblDisplayName.Text = user.DisplayName
                If user.Description isnot Nothing Then lblDescription.Text = user.Description
                If user.EmailAddress isnot Nothing Then lblEmail.Text = user.EmailAddress
                If user.LastLogon isnot Nothing Then lblLastLogon.Text = ToLocalTime(user.LastLogon).ToString
                If user.LastPasswordSet isnot Nothing Then lblLastPasswordSet.Text = ToLocalTime(user.LastPasswordSet).ToString
                If user.AccountLockoutTime isnot Nothing Then lblAccountLockoutTime.Text = ToLocalTime(user.AccountLockoutTime).ToString
                If user.PasswordNeverExpires Then
                    lblAccountExpires.Text = "Never"
                Else
                    If user.AccountExpirationDate isnot Nothing Then
                         lblAccountExpires.Text = "expired: " & ToLocalTime(user.AccountExpirationDate).ToString
                    Else
                        If user.LastPasswordSet isnot Nothing Then
                            lblAccountExpires.Text = DateDiff("d",Now.date,DateAdd("d",GetMaxPasswordAge.TotalDays,user.LastPasswordSet)).ToString & " days"
                        Else
                            lblAccountExpires.Text = "set on next logon"
                        End If
                        
                    End If
                End If

                Dim userGroupsArray As ArrayList = GetUserGroups(user.SamAccountName)
                For Each item In userGroupsArray
                    ddlUserGroups.Items.Add(item)
                Next

                If user.IsAccountLockedOut Then
                    ckbAccountLocked.Enabled = True
                    ckbAccountLocked.Checked = False
                End If

                Dim de As DirectoryEntry = user.GetUnderlyingObject()
                If de isnot Nothing then
                    Dim manager As UserPrincipal = GetManager(ctx,user)
                    If manager isnot Nothing then
                        lblManager.Text = manager.SamAccountName 
                    Else
                        lblManager.Text = ""
                    End If

                    lblOffice.Text = getPropertyValue(de, "physicalDeliveryOfficeName")            
                    lblTitle.Text = getPropertyValue(de, "title")
                    lblDepartment.Text = getPropertyValue(de, "department")
                    lblCompany.Text = getPropertyValue(de, "company")
                End If 'if de isnot nothing
            End If 'if user isnot nothing

            gvPSInfo.DataSource = getPSInfo(lblFirstName.Text, lblLastName.Text)
            gvPSInfo.DataBind 
        Catch ex As Exception
            lblStatus.Text = ex.Message
        End Try
    End Sub

    Public Function unlockAccount(ByVal accountSam As String) As Boolean
        Dim status As Boolean = False
        Try
            Dim ctx As New PrincipalContext(ContextType.Domain)
            Dim user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, accountSam)
            If user isnot Nothing then
                If user.IsAccountLockedOut Then
                    user.UnlockAccount
                    status = true
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return status
    End Function

    Public Function resetPassword(ByVal accountSam As string, ByVal forcePasswordChange As Boolean) As boolean
        Dim status As Boolean = false
        Try
            Dim ctx As New PrincipalContext(ContextType.Domain)
            Dim user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, accountSam)
            If user isnot Nothing then
                If user.IsAccountLockedOut Then
                    unlockAccount(accountSam)
                End If
                user.SetPassword (txtPassword1.Text)
                If forcePasswordChange Then
                    user.ExpirePasswordNow()
                    user.Save()
                End If
                status = true
            End If
        Catch ex As System.Exception
            Throw ex 'New System.Exception(ex.message)
        End Try
        return status
    End Function

    Public Function GetUserGroups(accountSam As String) As ArrayList
        Dim items As New ArrayList()
        Try
            Dim ctx As New PrincipalContext(ContextType.Domain)
            Dim user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, accountSam)
	        Dim pSearch As PrincipalSearchResult(Of Principal) = user.GetGroups()
	        For Each groupResult As Principal In pSearch
		        items.Add(groupResult.Name)
	        Next
        Catch ex As Exception
            Throw ex
        End Try
	    Return items
    End Function

    Public Function getPSInfo(firstname As String, lastname As string) As DataTable
        Dim dt As New System.Data.DataTable("details")
        Dim dr As System.Data.DataRow
        Try
            Dim dc As DataColumn
            dc = New DataColumn("id", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("name", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("ssn", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("deptname", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            If String.IsNullOrEmpty (lastname) and String.IsNullOrEmpty(firstname) then
            Else
                Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
                Dim query As String = "SELECT EDE_PORTAL_PS_EMPLOYEES_VW.EMPLID,EDE_PORTAL_PS_EMPLOYEES_VW.LAST_NAME,EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME,COALESCE(NULLIF(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME,' '), EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME) AS PREF_NAME,SUBSTR(EDE_PORTAL_PS_EMPLOYEES_VW.NATIONAL_ID, 4, 2) AS SSN, EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME " & _
                    "FROM sysadm.EDE_PORTAL_PS_EMPLOYEES_VW WHERE " & _
                    "UPPER(EDE_PORTAL_PS_EMPLOYEES_VW.LAST_NAME) Like UPPER('" & lblLastName.Text & "%') AND (" & _
                    "UPPER(EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME) LIKE UPPER('%" & lblFirstName.Text & "%') OR " & _
                    "UPPER(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME) LIKE UPPER('%" & lblFirstName.Text & "%'))"

                Dim cmd As New OracleCommand(query, conn)
                Dim datareader As OracleDataReader
                conn.Open()
                datareader = cmd.ExecuteReader
                If datareader.HasRows Then
                    While datareader.Read
                        dr = dt.NewRow()
                        dr.Item("id") = datareader("EMPLID").ToString
                        dr.Item("name") = datareader("PREF_NAME").ToString & " " & datareader("LAST_NAME").ToString 
                        dr.Item("ssn") = datareader("SSN").ToString
                        dr.Item("deptname") = datareader("DEPTNAME").ToString
                        dt.Rows.Add(dr)
                    End While
                End If
                datareader.Close()
                conn.Close()
            End If
        Catch
            Return Nothing
        End Try
        Return dt
    End Function

    Public Sub clearForm
        lblAccount.Text = ""
        lblFirstName.Text = ""
        lblLastName.Text = ""
        lblDisplayName.Text = ""
        lblDescription.Text = ""
        lblEmail.Text = ""
        ckbAccountLocked.Checked = False
        ckbAccountLocked.Enabled = false
        lbUnlock.ForeColor = Drawing.Color.LightGray
        lbUnlock.Enabled = false
        lblManager.Text = ""
        lblOffice.Text = ""
        lblTitle.Text = ""
        lblDepartment.Text = ""
        lblCompany.Text = ""
        lblLastLogon.Text = ""
        lblLastPasswordSet.Text = ""
        lblAccountLockoutTime.Text = ""
        lblAccountExpires.Text = ""
        ckbForcePasswordChange.Checked = False
        txtPassword1.Text = ""
        txtPassword2.Text = ""
        ddlUserGroups.Items.Clear
        txtFName.Text = ""
        txtLName.Text = ""
        txtAccount.Text = ""
    End Sub

    Public Shared Function GetMinPasswordLength() As Integer
	    Using d As System.DirectoryServices.ActiveDirectory.Domain = System.DirectoryServices.ActiveDirectory.Domain.GetCurrentDomain()
		    Using de As DirectoryEntry = d.GetDirectoryEntry()
			    Dim ds As New DirectorySearcher(de, "(objectClass=*)", Nothing, SearchScope.Base)
			    Dim sr As SearchResult = ds.FindOne()
			    Dim minPwdLength As Integer = 0
			    If sr.Properties.Contains("minPwdLength") Then
				    minPwdLength = CInt(sr.Properties("minPwdLength")(0))
			    End If
			    Return minPwdLength
		    End Using
	    End Using
    End Function

    Public Shared Function GetMaxPasswordAge() As TimeSpan
	    Using d As System.DirectoryServices.ActiveDirectory.Domain = System.DirectoryServices.ActiveDirectory.Domain.GetCurrentDomain()
		    Using de As DirectoryEntry = d.GetDirectoryEntry()
			    Dim ds As New DirectorySearcher(de, "(objectClass=*)", Nothing, SearchScope.Base)
			    Dim sr As SearchResult = ds.FindOne()
			    Dim maxPwdAge As TimeSpan = TimeSpan.MinValue
			    If sr.Properties.Contains("maxPwdAge") Then
				    maxPwdAge = TimeSpan.FromTicks(CLng(sr.Properties("maxPwdAge")(0)))
			    End If
			    Return maxPwdAge.Duration()
		    End Using
	    End Using
    End Function

    Public Function getPropertyValue (dirEntry As DirectoryEntry, propertyValue as String) As String
        Dim result As String = ""
        Try
            If dirEntry isnot Nothing then
                If dirEntry.Properties(propertyValue) IsNot Nothing and dirEntry.Properties(propertyValue).Count > 0 Then 'check for a valid value
                    result = dirEntry.Properties(propertyValue)(0).ToString()
                End If
            End If
        Catch ex As Exception
        End Try
        Return result
    End Function

    Public Function GetManager(ctx As PrincipalContext, user As UserPrincipal) As UserPrincipal
	    Dim result As UserPrincipal = Nothing
	    If user IsNot Nothing Then
		    Dim dirEntryForUser As DirectoryEntry = TryCast(user.GetUnderlyingObject(), DirectoryEntry) 'get the DirectoryEntry behind the UserPrincipal object
		    If dirEntryForUser IsNot Nothing Then
			    If dirEntryForUser.Properties("manager") IsNot Nothing  and dirEntryForUser.Properties("manager").Count > 0 Then 'check for a manager name
				    Dim managerDN As String = dirEntryForUser.Properties("manager")(0).ToString()
				    result = UserPrincipal.FindByIdentity(ctx, managerDN) ' find the manager UserPrincipal via the managerDN 
			    End If
		    End If
	    End If
	    Return result
    End Function

    Protected Sub ckbAccountLocked_CheckedChanged(sender As Object, e As EventArgs) Handles ckbAccountLocked.CheckedChanged
        If ckbAccountLocked.Checked = True Then
            lbUnlock.ForeColor = Drawing.Color.Black
            lbUnlock.Enabled = true
        Else
            lbUnlock.ForeColor = Drawing.Color.LightGray
            lbUnlock.Enabled = false
        End If
    End Sub

    Protected Sub lbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearch.Click
        searchAccounts()
        pnlSearch.Visible = False
        pnlSearchResults.Visible = true
    End Sub

    Protected Sub lbAnotherAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAnotherAccount.Click
        clearForm()
        pnlDetails.Visible = False
        pnlSearch.Visible = True
    End Sub

    Protected Sub lbAnotherSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAnotherSearch.Click
        clearForm()
        pnlSearchResults.Visible = False
        pnlSearch.Visible = True
    End Sub

    Protected Sub lbUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbUnlock.Click
        Dim em As new sendEmail
        Try
            If unlockAccount(lblAccount.Text) Then 'unlock successful so send email for ticket creation
                Dim toList As New ArrayList
                toList.Add("magic_admin@empiredistrict.com")
                em.sendEMail (toList,"admin@empiredistrict.com","ON CALL - Account Unlock " & lblAccount.Text, lblAccount.Text & " account unlocked at " & Datetime.Now.ToString & " by " & ps.getUsername & ".",false)

                toList.Clear()
                toList.Add(lblEmail.Text)
                em.sendEMail (toList,"admin@empiredistrict.com","Account Unlock", "Empire’s IT Department has performed an unlock for your account. If you did not initiate this request, please contact the IT Help Desk at extension 5111 or if after normal business hours, call 417-438-4032.",false)
            End If
            getAccount(lblAccount.text)
        Catch ex As Exception
            lblStatus.Text = ex.Message
        End Try
    End Sub

    Protected Sub lbResetPassword_Click(sender As Object, e As EventArgs) Handles lbResetPassword.Click
        Dim em As new sendEmail
        Try
            If Not String.IsNullOrEmpty(txtPassword1.Text ) and Not String.IsNullOrEmpty(txtPassword2.Text) Then
                If txtPassword1.Text = txtPassword2.Text Then
                    If resetPassword(lblAccount.Text, ckbForcePasswordChange.Checked) Then 'if successful send email for ticket creation
                        Dim toList As New ArrayList
                        toList.Add("magic_admin@empiredistrict.com")
                        em.sendEMail (toList,"admin@empiredistrict.com","ON CALL - Account Reset " & lblAccount.Text, lblAccount.Text & " account reset and unlocked at " & Datetime.Now.ToString & " by " & ps.getUsername & ". User is required to change password on first logon.",false)

                        toList.Clear()
                        toList.Add(lblEmail.Text)
                        em.sendEMail (toList,"admin@empiredistrict.com","Account Reset", "Empire’s IT Department has performed a password reset for your account. If you did not initiate this request, please contact the IT Help Desk at extension 5111 or if after normal business hours, call 417-438-4032.",false)

                        getAccount (lblAccount.text)
                        lblPasswordStatus.Text = ""
                    End If
                Else
                    lblPasswordStatus.Text = "passwords do not match"
                End If
            Else
                lblPasswordStatus.Text = "passwords required"
            End If
        Catch ex As Exception
            lblPasswordStatus.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvSearchResults_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvSearchResults.RowCommand
        If e.CommandName = "Select" Then
            getAccount (e.CommandArgument.ToString)
            pnlSearchResults.Visible = False
            pnlDetails.Visible = True
        End If
    End Sub
End Class
