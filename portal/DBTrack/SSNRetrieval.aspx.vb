Imports System.Data.OracleClient
Imports System.Data

Partial Class DBTrack_SSNRetrieval
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ps.isAdmin(ps.getPageName) Then
                pnlAuthorized.Visible = True
                pnlNotAuthorized.Visible = False
                pnlAdministration.Visible = True
                txtLName.Focus()
            Else
                pnlAdministration.Visible = False
                If Not ps.isAuthorized(ps.getPageName) Then
                    pnlAuthorized.Visible = False
                    pnlNotAuthorized.Visible = True
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearch.Click
        gvDetails.DataSource = GetDetailsTable()
        gvDetails.DataBind()
        gvDetails.SelectedIndex = 0
        pnlSearch.Visible = false
        pnlDetails.Visible = true
    End Sub

    Public Function GetDetailsTable() As DataTable
        Dim dt As New System.Data.DataTable("details")
        Dim dr As System.Data.DataRow
        Try
            Dim dc As DataColumn
            dc = New DataColumn("name", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("ssn", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("deptname", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
            Dim query As String = "SELECT EDE_PORTAL_PS_EMPLOYEES_VW.LAST_NAME, COALESCE(NULLIF(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME,' '), EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME) as PREF_NAME, EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME, SUBSTR(EDE_PORTAL_PS_EMPLOYEES_VW.NATIONAL_ID, 4, 2) AS SSN, EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME " & _
                "FROM sysadm.EDE_PORTAL_PS_EMPLOYEES_VW WHERE " & _
                "UPPER(EDE_PORTAL_PS_EMPLOYEES_VW.LAST_NAME)  Like UPPER('%' || :lname || '%') AND " & _
                "UPPER(COALESCE(NULLIF(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME,' '), EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME)) Like UPPER('%' || :fname || '%') " & _
                "ORDER BY upper(EDE_PORTAL_PS_EMPLOYEES_VW.last_name), upper(EDE_PORTAL_PS_EMPLOYEES_VW.first_name)"
            Dim cmd As New OracleCommand(query, conn)
            cmd.Parameters.AddWithValue("lname", txtLName.text)
            cmd.Parameters.AddWithValue("fname", txtFName.text)
            Dim datareader As OracleDataReader
            conn.Open()
            datareader = cmd.ExecuteReader
            If datareader.HasRows Then
                While datareader.Read
                    dr = dt.NewRow()
                    dr.Item("name") = datareader("LAST_NAME").ToString & ", " & datareader("PREF_NAME").ToString
                    dr.Item("ssn") = datareader("SSN").ToString
                    dr.Item("deptname") = datareader("DEPTNAME").ToString
                    dt.Rows.Add(dr)
                End While
            End If
            datareader.Close()
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return dt
    End Function

    Protected Sub lbNewSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbNewSearch.Click
        pnlDetails.Visible = False
        pnlSearch.Visible = true
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gvDetails.DataSource = GetDetailsTable()
        gvDetails.DataBind()
    End Sub

    Protected Function SortDataTable(ByVal dataTable As DataTable, ByVal isPageIndexChanging As Boolean) As DataView
        If Not dataTable Is Nothing Then
            Dim dataView As New DataView(dataTable)
            If GridViewSortExpression <> String.Empty Then
                If isPageIndexChanging Then
                    dataView.Sort = String.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection)
                Else
                    dataView.Sort = String.Format("{0} {1}", GridViewSortExpression, GetSortDirection())
                End If
            End If
            Return dataView
        Else
            Return New DataView()
        End If
    End Function
    Private Property GridViewSortExpression() As String
        Get
            Return IIf(ViewState("SortExpression") = Nothing, String.Empty, ViewState("SortExpression"))
        End Get
        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property
    Private Property GridViewSortDirection() As String
        Get
            Return IIf(ViewState("SortDirection") = Nothing, "ASC", ViewState("SortDirection"))
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property
    Private Function GetSortDirection() As String
        Select Case GridViewSortDirection
            Case "ASC"
                GridViewSortDirection = "DESC"
            Case "DESC"
                GridViewSortDirection = "ASC"
        End Select
        Return GridViewSortDirection
    End Function

    Protected Sub gvDetails_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvDetails.Sorting
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = gvDetails.PageIndex

        gvDetails.DataSource = SortDataTable(GetDetailsTable, False)
        gvDetails.DataBind()
    End Sub
End Class
