<%@ Page Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="SSNRetrieval.aspx.vb" Inherits="DBTrack_SSNRetrieval" title="SSN Lookup" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <asp:Panel ID="pnlSearch" runat="server" Visible="True">
            <div class="row">
                <div class="col-lg-12 ">
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtLName">Last Name</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtLName" runat="server" CssClass="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="field-label col-xs-3 active"><label for="txtFName">First Name</label></td>
                            <td class="col-md-9"><asp:TextBox ID="txtFName" runat="server" CssClass="form-control" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Search' />
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gvDetails" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" />
                                <asp:BoundField DataField="ssn" HeaderText="SSN" SortExpression="ssn" />
                                <asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname" />
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbNewSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Another Search' />
                </div>
            </div>
        </asp:Panel>

        <div class="row">
            <div class="col-lg-12">
                
            </div>
        </div>

    </asp:Panel><%--end pnlAuthorized--%>
</asp:Content>