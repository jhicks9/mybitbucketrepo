Imports System.Security
Imports System.Security.Principal
Imports System.Web.Security
Imports System.IO
Imports System.XML
Imports System.Data
Imports System.Data.SqlClient
Imports System
Partial Class DBTClientTickets
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'encrypt the viewstate when saving sql statements across postbacks
        Page.RegisterRequiresViewStateEncryption()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlDetails.Visible = False
        gvTickets.SelectedIndex = -1
        If Not IsPostBack Then
            txtClientID.Text = getUserName()
            Show_Tickets()
        End If
        If Not ViewState("currentCommand") Is Nothing Then
            SqlDataSourceTickets.SelectCommand = ViewState("currentCommand")
        End If
    End Sub

    Public Function getUserName() As String
        Dim username As String = ""
        username = Request.ServerVariables("LOGON_USER")
        username = Right(username, Len(username) - InStr(username, "\"))
        Return username
    End Function

    Public Sub Show_Tickets()

        Try
            SqlDataSourceTickets.SelectParameters.Clear  'clear all parameters
            Dim buildSearch As New StringBuilder
            buildSearch.Append("select tickets.ticket_number,tickets.ticket_description,tickets.ticket_assignedto,tickets.ticket_opendate,tickets.ticket_abstract,tickets.ticket_statusid from tickets where ")
            If ckIncludeClosed.Checked Then
                buildSearch.append(" tickets.ticket_statusid not in ('*')")  'gets all tickets regardless of status
            Else
                buildSearch.Append(" tickets.ticket_statusid not in ('closed')")
            End If

            If Not String.IsNullOrEmpty(txtClientID.Text) Then
                SqlDataSourceTickets.SelectParameters.Add("clientid", DbType.String, txtClientID.Text)
                buildSearch.Append(" and tickets.ticket_clientid=@clientid")
            End If

            If Not String.IsNullOrEmpty(txtSearchAssignedTo.Text) Then
            SqlDataSourceTickets.SelectParameters.Add("assignedto", DbType.String, txtSearchAssignedTo.Text)
                buildSearch.Append(" and tickets.ticket_assignedto=@assignedto")
            End If

            If Not string.IsNullOrEmpty(txtTicket.Text) Then
                SqlDataSourceTickets.SelectParameters.Add("ticketnumber", DbType.String, txtTicket.Text)
                buildSearch.Append(" and ticket_number = @ticketnumber")
            End If

            If Not String.IsNullOrEmpty(txtSearchDescription.Text) Then
                SqlDataSourceTickets.SelectParameters.Add("description", DbType.String, txtSearchDescription.Text)
                buildSearch.Append(" and ticket_abstract like '%'+@description+'%'")
            End If

            If Not String.IsNullOrEmpty(txtBegDate.Text) And Not String.IsNullOrEmpty(txtEndDate.Text) Then
                SqlDataSourceTickets.SelectParameters.Add("begdate", txtBegDate.Text)
                SqlDataSourceTickets.SelectParameters.Add("enddate", txtEndDate.Text)
                buildSearch.Append(" and tickets.ticket_opendate between @begdate and @enddate")
            End If

            buildSearch.Append(" order by ticket_number DESC")
            SqlDataSourceTickets.SelectCommand = buildSearch.ToString
            ViewState("currentCommand") = SqlDataSourceTickets.SelectCommand
            SqlDataSourceTickets.DataBind()
        Catch
        End Try
    End Sub

    Protected Sub gvTickets_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTickets.RowCommand
        If e.CommandName = "Select" Then
            Dim mySqlConnection As SqlConnection
            mySqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("DBTrack").ConnectionString)
            Dim conn = "SELECT ticket_number,ticket_description,ticket_assignedTo,Ticket_Abstract,ticket_statusid FROM tickets WHERE ticket_Number = @ticketnumber"
            Dim myCommand As New SqlCommand(conn, mySqlConnection)
            myCommand.Parameters.AddWithValue("@ticketnumber", e.CommandArgument)
            mySqlConnection.Open()
            Dim myReader As SqlDataReader
            myReader = myCommand.ExecuteReader()
            While myReader.Read
                lblAbstract.Text = myReader.GetValue(0) & " - " & myReader.GetString(3)
                lblDescription.Text = myReader.GetString(1).ToString().Replace(Environment.NewLine,"<br />")
                lblAssignedTo.Text = myReader.GetString(2)
                lblStatus.Text = myReader.GetString(4)
            End While
            myReader.Close()
            mySqlConnection.Close()
            pnlDetails.Visible = True
            pnlTickets.Visible = False
        End If
    End Sub

    Protected Sub lbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClose.Click
        pnlDetails.Visible = False
        pnlTickets.Visible = True
    End Sub

    Protected Sub lbSearchParameters_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearchParameters.Click
        pnlTickets.Visible = False
        pnlSearch.Visible = True
    End Sub

    Protected Sub lbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearch.Click
        gvTickets.PageIndex = 0
        Show_Tickets()
        pnlDetails.Visible = False
        pnlSearch.Visible = False
        pnlTickets.Visible = True
    End Sub
End Class
