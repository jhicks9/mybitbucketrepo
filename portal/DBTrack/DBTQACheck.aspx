﻿<%@ Page Title="DBTrack QA Check" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="DBTQACheck.aspx.vb" Inherits="DBTrack_DBTQACheck" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:Panel id="pnlModuleContent" Runat="server">
    <div class="row">
        <div class="col-lg-12 ">
	        <table id="screen1" class="table table-condensed table-no-border ">
		        <tr>
			        <td class="col-lg-12"><p class="well-sm bg-info text-center"><strong><asp:Label ID="lblScreen" runat="server" /></strong></p></td>
		        </tr>
		        <tr>
			        <td class="col-lg-12">
				        <ASP:CHECKBOX id="ChkQANeeded" runat="server" Visible="False" class="btn btn-default"></ASP:CHECKBOX>
				        <ASP:CHECKBOX id="chkChangeToDataEntry" runat="server" Visible="False" AutoPostBack="True" class="btn btn-default"></ASP:CHECKBOX>
				        <ASP:LABEL id="lblValReasonMsg" runat="server"></ASP:LABEL>
			        </td>
		        </tr>
		        <tr>
			        <td class="col-lg-12">
				        <ASP:TEXTBOX id="txtNoQAReason" runat="server" Visible="False" TextMode="MultiLine" CssClass="form-control"></ASP:TEXTBOX>
			        </td>
		        </tr>
                <tr>
                    <td class="col-lg-12">
                        <ASP:LABEL id="lblTicketNumber" runat="server" Visible="False" Text="QA FOR TICKET NUMBER:" />
                    </td>
                </tr>
		        <tr>
			        <td class="col-lg-12">
				        <ASP:DROPDOWNLIST id="cboDevList" runat="server" AutoPostBack="True" CssClass="form-control"></ASP:DROPDOWNLIST>
				        <ASP:LABEL id="lblTicketNum" runat="server" ></ASP:LABEL>
			        </td>
		        </tr>
		        <tr>
			        <td class="col-lg-12"><asp:label id="lblAbstract" runat="server" Visible="False" Text=''></asp:label></td>
		        </tr>
		        <tr>
			        <td class="col-lg-12"><ASP:LABEL id="lblDevNeed" runat="server"></ASP:LABEL></td>
		        </tr>
		        <tr>
			        <td class="col-lg-12"><ASP:LABEL id="lblInstructions" runat="server"  Visible="False"></ASP:LABEL></td>
		        </tr>
		        <tr>
			        <td class="col-lg-12">
				        <ASP:RADIOBUTTONLIST id="rdoTemplate" runat="server" >
					        <ASP:LISTITEM Value="PS">PeopleSoft Template</ASP:LISTITEM>
					        <ASP:LISTITEM Value="CW">Customer Watch Template</ASP:LISTITEM>
					        <ASP:LISTITEM Value="TS">Desktop Support</ASP:LISTITEM>
					        <ASP:LISTITEM Value="SC">Source Code - Customer Watch</ASP:LISTITEM>
					        <ASP:LISTITEM Value="NA">Nework ID/Application Access</ASP:LISTITEM>
					        <ASP:LISTITEM Value="WB">Web/Portal Template</ASP:LISTITEM>
				        </ASP:RADIOBUTTONLIST>
			        </td>
		        </tr>
                <tr>
                    <td class="col-lg-12"><ASP:BUTTON id="btnSubTemp" runat="server" Text="Submit" Font-Bold="True" class="btn btn-default"></ASP:BUTTON></td>
                </tr>
	        </table><!--HeadingsPanel is the panel on top of the Template Panels showing the heading for the correct template-->
        </div>
    </div>
</asp:Panel>

<ASP:PANEL id="HeadingsPanel" runat="server" Visible="False">
    <div class="row">
        <div class="col-lg-12 ">
            <table class="table table-condensed table-no-border">
                <tr>
                    <td class="col-lg-12" colspan="3"><p class="well-sm bg-info text-center"><strong>Ticket Information</strong></td>
                </tr>
	            <tr>
		            <td class="col-lg-4">
			            <ASP:LABEL id="lblAttachment" runat="server" Visible="False">Attachments:</ASP:LABEL>
                    </td>
	                <td class="col-lg-4">
                        <ASP:LABEL id="txtAttachment" runat="server" Visible="False" CssClass="form-control no-border" />
                    </td>
                    <td class="col-lg-4"></td>
	            </tr>
	            <tr>
		            <td class="col-lg-4">
			            <ASP:LABEL id="lblLexicon" runat="server" Visible="False">Number of Lexicon Programs: </ASP:LABEL>
		            </td>
                    <td class="col-lg-4">
                        <ASP:LABEL id="txtLexicon" runat="server" Visible="False" CssClass="form-control no-border" />
                    </td>
                    <td class="col-lg-4">
                        <ASP:HYPERLINK id="HyperLink3" runat="server" NavigateUrl="http://cisprod/buildreports" Target="_blank" Visible="False" Text="Commit Log" CssClass="btn btn-default" />
                    </td>
	            </tr>
	            <tr>
		            <td class="col-lg-4">
			            <ASP:LABEL id="lblTktNum" runat="server" Visible="False">QA Check for Ticket: </ASP:LABEL>
                    </td>
                    <td class="col-lg-4">
                        <ASP:LABEL id="txtTicketNum" runat="server" Visible="False" CssClass="form-control no-border" />
                    </td>
                    <td class="col-lg-4">
                        <ASP:LABEL id="txtTicketAbstract" runat="server" Visible="False" CssClass="form-control no-border" />
                    </td>
	            </tr>
            </table>
        </div>
    </div>
</ASP:PANEL>

<br />
<ASP:PANEL id="QAInfoPanel" runat="server" visible="False">
    <div class="row">
        <div class="col-lg-12 ">
            <table class="table table-condensed table-no-border">
                <tr><td class="col-lg-4" colspan="2"><p class="well-sm bg-info text-center"><strong>Misc Information</strong></p></td></tr>
		        <tr>
		            <td class="col-lg-6 text-center">
				        <ASP:LABEL id="lblMisc" runat="server" Visible="False" CssClass="form-control no-border"></ASP:LABEL>
			        </td>			
			        <td class="col-lg-6 text-center">
				        <ASP:LABEL id="lblTeam" runat="server" Visible="False" CssClass="form-control no-border"></ASP:LABEL>
		            </td>
		        </tr>
                <tr>
                    <td class="col-lg-6 text-center">
                        <ASP:TEXTBOX id="txtMiscItems" runat="server" Visible="False" TextMode="MultiLine" CssClass="form-control" />
                    </td>
                    <td class="col-lg-6 text-center">
                        <ASP:TEXTBOX id="txtTeam" runat="server" Visible="False" TextMode="MultiLine" CssClass="form-control"></ASP:TEXTBOX>
                    </td>
                </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12 text center">
				        <ASP:LABEL id="lblTeamError" runat="server"></ASP:LABEL>
			        </td>
		        </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12 text-center">
				        <ASP:LABEL id="lblRejReason" runat="server" Visible="False" CssClass="form-control no-border"
					        ForeColor="#C00000">Reason for Rejection is Required for ALL Failed Tickets:</ASP:LABEL>
			        </td>
		        </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12">
				        <ASP:TEXTBOX id="txtRejReason" runat="server" Visible="False" TextMode="MultiLine" CssClass="form-control"></ASP:TEXTBOX>
			        </td>
		        </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12 text-center">
				        <ASP:BUTTON id="btnAccept" runat="server" Text="Accept" CssClass="btn btn-primary"></ASP:BUTTON>
				        <ASP:BUTTON id="btnReject" runat="server" Visible="False" Text="Reject" CssClass="btn btn-warning"></ASP:BUTTON>
			        </td>
		        </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12">
				        <ASP:LABEL id="lblQANotMatch" runat="server" Visible="False" ForeColor="Red"></ASP:LABEL>
			        </td>
		        </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12">
				        <ASP:LABEL id="lblVerify" runat="server" Visible="False">Label</ASP:LABEL>
			        </td>
		        </tr>
		        <tr>
			        <td colspan="2" class="col-lg-12 text-center">
				        <ASP:HYPERLINK id="Hyperlink2" runat="server" NavigateUrl="~/DBTrack/DBTQACheck.aspx" Target="_parent" CssClass="btn btn-default">Home</ASP:HYPERLINK>
			        </td>
		        </tr>
	        </table>
        </div>
    </div>
</ASP:PANEL>

<ASP:PANEL id="lexiconpanel" Runat="server" Visible="False">
    <div class="row">
        <div class="col-lg-12 ">
            <p class="well-sm text-center bg-info"><strong>Lexicon Files</strong></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ">
            <asp:GridView ID="DataGrid1" runat="server" DataSourceID="sqlDataSource32" Visible="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="True" AllowSorting="false" AllowPaging="false">
            </asp:GridView>
            <asp:SqlDataSource ID="sqlDataSource32" runat="server" ConnectionString="<%$ ConnectionStrings:DBTrack %>" 
                SelectCommand="SELECT LEX_Name,LEX_Date,LEX_Time FROM dbo.Lexicon WHERE LEX_StatusID ='CHECKOUT' and LEX_TicketNumber = @selectticket">
                <SelectParameters>
                    <asp:Parameter Name="selectticket" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</ASP:PANEL>

<br />
<asp:Panel id="Fieldset3" Runat="server" Wrap="False" Visible="false">
    <div class="row">
        <div class="col-lg-12 ">
            <p class="well-sm text-center bg-info"><strong>QA Templates</strong></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-condensed table-no-border">
	            <tr>
		            <td class="col-md-6">
                        <ASP:PANEL id="GNPanel" runat="server" visible="False">
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="GNLabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">General/Miscellaneous</ASP:LABEL>
						            </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckBoxListGN" runat="server" Visible="False" TextAlign="right" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckBoxListGN" runat="server" ForeColor="Navy" visible="False" Font-Size="Smaller" RepeatLayout="Table" >
								            <ASP:LISTITEM Value="1">DBTrack ticket number</ASP:LISTITEM>
								            <ASP:LISTITEM Value="2">System development methodology followed</ASP:LISTITEM>
								            <ASP:LISTITEM Value="3">Source code procedure followed</ASP:LISTITEM>
								            <ASP:LISTITEM Value="4">Final test verification complete</ASP:LISTITEM>
								            <ASP:LISTITEM Value="5">Naming standards followed</ASP:LISTITEM>
								            <ASP:LISTITEM Value="6">Report layout meets standards</ASP:LISTITEM>
								            <ASP:LISTITEM Value="7">JAVA program meets layout standards</ASP:LISTITEM>
								            <ASP:LISTITEM Value="8">Documentation supplied by IS to clients is complete</ASP:LISTITEM>
								            <ASP:LISTITEM Value="9">Technical support notification of change (if necessary) is complete</ASP:LISTITEM>
								            <ASP:LISTITEM Value="10">Client Documentation updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="11">IS Procedures and Security Policies updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="12">Client Emails</ASP:LISTITEM>
								            <ASP:LISTITEM Value="13">Vendor emails</ASP:LISTITEM>
								            <ASP:LISTITEM Value="14">Test results</ASP:LISTITEM>
								            <ASP:LISTITEM Value="15">Database/Configuration changes</ASP:LISTITEM>
								            <ASP:LISTITEM Value="16">Vendor and/or Client special instructions</ASP:LISTITEM>
								            <ASP:LISTITEM Value="17">SQL Script(s) meets standards</ASP:LISTITEM>
								            <ASP:LISTITEM Value="18">Command/Resource File change</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
                    <%--==========================================================================================================================================================================--%>
		            <td class="col-md-6">
                        <ASP:PANEL id="TSPanel" Runat="server"  Wrap="False" visible="False"><!--each cell(td) contains a table of 2 panels of checkboxes and their QA equivalent-->
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="TSLabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">New PC/Replacement PC</ASP:LABEL>
					                </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckboxlistTS" runat="server" Visible="False" TextAlign="left" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckboxlistTS" runat="server" Visible="False" ForeColor="Navy" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM Value="91">Property tag affixed to PC or PC related equipment</ASP:LISTITEM>
								            <ASP:LISTITEM Value="92">Property Accountin information recorded in Furniture Tag folder</ASP:LISTITEM>
								            <ASP:LISTITEM Value="93">Copy of associated PO's placed in PC folder</ASP:LISTITEM>
								            <ASP:LISTITEM Value="94">Machine Name correct/description updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="95">Active Directory Computers Updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="96">Micorsoft Offece license purchased/upgraded</ASP:LISTITEM>
								            <ASP:LISTITEM Value="97">DBTrack Hardware Inventory updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="98">DBTrack Software Inventory updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="99">DBTrack Client Information updated</ASP:LISTITEM>
								            <ASP:LISTITEM Value="100">Non-Asset tag affixed to computer or computer related equipment</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
                    <%--==========================================================================================================================================================================--%>
		            <td class="col-md-6">
                        <ASP:PANEL id="PSPanel" Runat="server" Wrap="False" visible="False">
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="PSLabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">PeopleSoft</ASP:LABEL>
						            </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckboxlistPS" runat="server" Visible="False" TextAlign="left" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckboxlistPS" runat="server" Visible="False" ForeColor="Navy" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM Value="31">Download from PS website</ASP:LISTITEM>
								            <ASP:LISTITEM Value="32">Review pre-requisites</ASP:LISTITEM>
								            <ASP:LISTITEM Value="33">Download pre-requisites</ASP:LISTITEM>
								            <ASP:LISTITEM Value="34">Print Installation Instructions</ASP:LISTITEM>
								            <ASP:LISTITEM Value="35">Follow Installation Instructions(move any files/folders to test server)</ASP:LISTITEM>
								            <ASP:LISTITEM Value="36">Preliminary testing complete</ASP:LISTITEM>
								            <ASP:LISTITEM Value="37">Client testing complete</ASP:LISTITEM>
								            <ASP:LISTITEM Value="38">Client testing approval</ASP:LISTITEM>
								            <ASP:LISTITEM Value="39">Move to production</ASP:LISTITEM>
								            <ASP:LISTITEM Value="40">Client production verification</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
                    <%--==========================================================================================================================================================================--%>
		            <td class="col-md-6">
                        <ASP:PANEL id="CWPanel" Runat="server" Wrap="False" visible="False">
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="CWLabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">Customer Watch</ASP:LABEL>
						            </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckboxlistCW" runat="server" Visible="False" TextAlign="left" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckboxlistCW" runat="server" Visible="False" ForeColor="Navy" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM Value="61">Review release notes (if Conversant released)</ASP:LISTITEM>
								            <ASP:LISTITEM Value="62">Download from Conversant FTP site &amp; Checkout to local PC</ASP:LISTITEM>
								            <ASP:LISTITEM Value="63">Database/Configuration changes</ASP:LISTITEM>
								            <ASP:LISTITEM Value="64">Empire Modifications</ASP:LISTITEM>
								            <ASP:LISTITEM Value="65">Build and Deploy to test environment</ASP:LISTITEM>
								            <ASP:LISTITEM Value="66">Preliminary testing complete</ASP:LISTITEM>
								            <ASP:LISTITEM Value="67">Client testing complete (unit,batch,parallel)</ASP:LISTITEM>
								            <ASP:LISTITEM Value="68">Client testing approval</ASP:LISTITEM>
								            <ASP:LISTITEM Value="69">Client notification of change</ASP:LISTITEM>
								            <ASP:LISTITEM Value="70">Verification of Timestamps</ASP:LISTITEM>
								            <ASP:LISTITEM Value="71">Move to production</ASP:LISTITEM>
								            <ASP:LISTITEM Value="72">Client production verification</ASP:LISTITEM>
								            <ASP:LISTITEM Value="73">BJS Batch Processing Procedures followed</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
                    <%--==========================================================================================================================================================================--%>
		            <td class="col-md-6">
                        <ASP:PANEL id="SCPanel" Runat="server" Wrap="False" visible="False">
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="SCLabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">Customer Watch Source Code Changes</ASP:LABEL>
						            </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckboxlistSC" runat="server" Visible="False" TextAlign="left" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckboxlistSC" runat="server" Visible="False" ForeColor="Navy" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM Value="121">Customer Watch</ASP:LISTITEM>
								            <ASP:LISTITEM Value="122">Master Ticket Created</ASP:LISTITEM>
								            <ASP:LISTITEM Value="123">Check List on file</ASP:LISTITEM>
								            <ASP:LISTITEM Value="124">Pre-stage r/o date and time prior to tickets QA date &amp; time</ASP:LISTITEM>
								            <ASP:LISTITEM Value="125">Code moved to production</ASP:LISTITEM>
								            <ASP:LISTITEM Value="126">Verify Lexicon that code has been checked in</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
                    <%--==========================================================================================================================================================================--%>
		            <td class="col-md-6">
                        <ASP:PANEL id="NAPanel" Runat="server" Wrap="False" visible="False">
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="NALabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">Network ID/Application Access</ASP:LABEL>
						            </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckboxlistNA" runat="server" Visible="False" TextAlign="left" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
                                    </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckboxlistNA" runat="server" Visible="False" ForeColor="Navy" Font-Size="Smaller" RepeatLayout="Table">
								            <asp:ListItem Value="151">Network</asp:ListItem>
								            <asp:ListItem Value="152">Form received, signed and filed</asp:ListItem>
								            <asp:ListItem Value="153">ID created</asp:ListItem>
								            <asp:ListItem Value="154">Email account created</asp:ListItem>
								            <asp:ListItem Value="155">Security group assigned</asp:ListItem>
								            <asp:ListItem Value="156">Remote Access/VPN/SHIVA</asp:ListItem>
								            <asp:ListItem Value="157">PeopleSoft</asp:ListItem>
								            <asp:ListItem Value="158">Form received, signed and filed</asp:ListItem>
								            <asp:ListItem Value="159">ID created</asp:ListItem>
								            <asp:ListItem Value="160">Operator preferences listed in ticket</asp:ListItem>
								            <asp:ListItem Value="161">Assigned Class and operator preferences</asp:ListItem>
								            <asp:ListItem Value="162">Customer Watch</asp:ListItem>
								            <asp:ListItem Value="163">Form received, signed and filed</asp:ListItem>
								            <asp:ListItem Value="164">ID created</asp:ListItem>
								            <asp:ListItem Value="165">Assigned Profile</asp:ListItem>
								            <asp:ListItem Value="166">Assigned Component(s)</asp:ListItem>
								            <asp:ListItem Value="167">DBTrack Client ID</asp:ListItem>
								            <asp:ListItem Value="168">Client ID created</asp:ListItem>
								            <asp:ListItem Value="169">Employee transfer/termination</asp:ListItem>
								            <asp:ListItem Value="170">DBTrack ticket created</asp:ListItem>
								            <asp:ListItem Value="171">Network ID deleted</asp:ListItem>
								            <asp:ListItem Value="172">Exchange mailbox deleted</asp:ListItem>
								            <asp:ListItem Value="173">Shiva Account deleted</asp:ListItem>
								            <asp:ListItem Value="174">Peoplesoft FIN/HR account deleted</asp:ListItem>
								            <asp:ListItem Value="175">Customer Watch account deleted</asp:ListItem>
								            <asp:ListItem Value="176">DBTrack client delete ticket created</asp:ListItem>
								            <asp:ListItem Value="177">Form received, signed and filed</asp:ListItem>
								            <asp:ListItem Value="178">Security Groups unassigned</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
                    <%--==========================================================================================================================================================================--%>
		            <td class="col-md-6">
                        <ASP:PANEL id="WebPanel" Runat="server" Wrap="False" visible="False">
				            <table class="table table-condensed table-no-border">
					            <tr>
						            <td colspan="2" class="text-center">
							            <ASP:LABEL id="WebLabelHead" Runat="server" Font-Bold="True" ForeColor="Indigo">Web/Portal</ASP:LABEL>
						            </td>
					            </tr>
					            <tr>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="QACheckboxlistWeb" runat="server" Visible="False" TextAlign="left" Font-Size="Smaller" RepeatLayout="Table">
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
								            <ASP:LISTITEM>-</ASP:LISTITEM>
							            </ASP:CHECKBOXLIST>
						            </td>
						            <td valign="top">
							            <ASP:CHECKBOXLIST id="CheckboxlistWeb" runat="server" Visible="False" ForeColor="Navy" Font-Size="Smaller" RepeatLayout="Table">
								            <asp:ListItem Value="181">ASPX/HTML Change</asp:ListItem>
								            <asp:ListItem Value="182">Codebehind Change</asp:ListItem>
								            <asp:ListItem Value="183">JavaScript Used</asp:ListItem>
								            <asp:ListItem Value="184">Custom Control</asp:ListItem>
								            <asp:ListItem Value="185">Class Created</asp:ListItem>
								            <asp:ListItem Value="186">Permissions Change</asp:ListItem>
								            <asp:ListItem Value="187">Tested in IE6</asp:ListItem>
								            <asp:ListItem Value="188">Tested in IE7</asp:ListItem>
								            <asp:ListItem Value="189">Tested in Mozilla Firefox</asp:ListItem>
								            <asp:ListItem Value="180">Tested in Safari</asp:ListItem>
								            <asp:ListItem Value="191">Tested in Opera</asp:ListItem>
								            <asp:ListItem Value="192">Form received, signed and filed</asp:ListItem>
								            <asp:ListItem Value="193">Files Checked out in Lexicon</asp:ListItem>
								            <asp:ListItem Value="194">Client Testing Approval</asp:ListItem>
								            <asp:ListItem Value="195">Client Emails</asp:ListItem>
								            <asp:ListItem Value="196">Database/Configuration changes</asp:ListItem>
								            <asp:ListItem Value="197">Web Service changes</asp:ListItem>
								            <asp:ListItem Value="198">Web.config changes</asp:ListItem>
								            <asp:ListItem Value="199">Sitemap updated</asp:ListItem>
								            <asp:ListItem Value="200">Portal (IE only)</asp:ListItem>
								            <asp:ListItem Value="201">DBTrack ticket created</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
								            <asp:ListItem>-</asp:ListItem>
							            </ASP:CHECKBOXLIST>
						            </td>
					            </tr>
				            </table>
			            </ASP:PANEL>
		            </td>
	            </tr>
            </table>
        </div>
    </div>
</asp:Panel>

<ASP:PANEL id="Panel1" runat="server" Visible="false">
	<table>
		<tr>
			<td>
				<ASP:TEXTBOX id="txtTicketDescr" runat="server" Visible="False" TextMode="MultiLine"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtUserID" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtUserName" runat="server" Visible="True"></ASP:TEXTBOX>
				<ASP:LABEL id="lblEmpName" runat="server"></ASP:LABEL>
				<ASP:TEXTBOX id="txtGroup" runat="server" Visible="False"></ASP:TEXTBOX>
			</td>
		</tr>
		<tr>
			<td>
				<ASP:TEXTBOX id="txtCheck" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtQA_Switch" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="TxtArray" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtValArray" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtQAcheck" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtvaliduser" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtQATeam" runat="server" Visible="False"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtDBTrackTech" runat="server" Visible="False"></ASP:TEXTBOX>
			</td>
		</tr>
	</table>
</ASP:PANEL>
<%--</div>--%>
</asp:Content>


