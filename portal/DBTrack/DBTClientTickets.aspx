<%@ Page Language="VB" MasterPageFile="~/Site.master" ValidateRequest="false" AutoEventWireup="false" CodeFile="DBTClientTickets.aspx.vb" Inherits="DBTClientTickets" title="DBTrack Tickets" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlTickets" runat="server">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="table-responsive">
                    <asp:GridView ID="gvTickets" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" AllowPaging="true" UseAccessibleHeader="true" DataSourceID="SqlDataSourceTickets" >
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblTickets" runat="server" Text="Tickets" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%#Eval("Ticket_Number") %>' CommandName="Select" ToolTip="Ticket Detail"
                                        Text='<%#Eval("Ticket_Number") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOpenDateLabel" runat="server" Text="Open Date" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblOpenDate" runat="server" Text='<%# string.Format( "{0:MM/dd/yy}", Eval("ticket_opendate"))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblAssignedToLabel" runat="server" Text="Assigned To" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssignedTo" runat="server" Text='<%#Eval("ticket_assignedto")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblStatusLabel" runat="server" Text="Status" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("ticket_statusid")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text="Description" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("TICKET_Abstract")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <asp:LinkButton ID="lbSearchParameters" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Search Tickets' />
                <%--<asp:LinkButton ID="CogLinkButton" CssClass="btn btn-default" runat="server" ><span class="glyphicon glyphicon-cog"></span></asp:LinkButton>--%>
            </div>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlDetails" runat="server">
        <div class="row">
            <div class="col-lg-12 ">
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Assigned To</label></td>
                        <td class="col-md-9"><asp:Label id="lblAssignedTo" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Status</label></td>
                        <td class="col-md-9"><asp:Label id="lblStatus" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Ticket</label></td>
                        <td class="col-md-9"><asp:Label id="lblAbstract" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Description</label></td>
                        <td class="col-md-9"><asp:Label id="lblDescription" runat="server" /></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <asp:LinkButton ID="lbClose" runat="server" CssClass="btn btn-default" CausesValidation="False">Close</asp:LinkButton>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlSearch" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-12 ">
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td class="field-label col-xs-3 active"><label for="txtTicket">Ticket Number</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtTicket" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label for="txtBegDate">Date Range</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtBegDate" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active">
                        <td class="col-md-9"><asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>ClientID</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtClientID" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Assigned To</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtSearchAssignedTo" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Description</label></td>
                        <td class="col-md-9"><asp:TextBox ID="txtSearchDescription" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"></td>
                        <td class="col-md-9"><asp:CheckBox ID="ckIncludeClosed" runat="server" class="btn btn-default" Text="Include closed tickets" /></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Search' />
            </div>
        </div>
    </asp:Panel>

    <asp:Label id="lblTest" Runat="server" />
    <asp:SqlDataSource ID="SqlDataSourceTickets" runat="server" ConnectionString="<%$ ConnectionStrings:DBTrack %>" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptSection" Runat="Server">
        <script type="text/javascript">
            $(function () {
                $('#<%=txtBegDate.ClientId %>').datetimepicker();
                $('#<%=txtEndDate.ClientId %>').datetimepicker();
            });
        </script>
</asp:Content>

