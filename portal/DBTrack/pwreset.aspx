﻿<%@ Page Title="Account Reset" Language="VB" MasterPageFile="~/site.master" AutoEventWireup="false" CodeFile="pwreset.aspx.vb" Inherits="DBTrack_pwreset" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">

        <asp:Panel ID="pnlSearch" runat="server" Visible="True" DefaultButton="lbSearch">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="txtFName" class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtFName" runat="server" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtLName" class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtLName" runat="server" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Account Name</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtAccount" runat="server" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:LinkButton ID="lbSearch" runat="server" class="btn btn-default" CausesValidation="False" Text='Search' OnClick="lbSearch_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlSearchResults" runat="server" Visible="False" DefaultButton="lbAnotherSearch">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gvSearchResults" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblSearchResultsAccount" runat="server" Text="Account Name" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbSearchResultsAccount" runat="server" CausesValidation="False" CommandArgument='<%#Eval("samaccountname") %>' CommandName="Select" ToolTip="account"
                                            Text='<%#Eval("samaccountname") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="displayname" HeaderText="Full Name"/>
                                <asp:BoundField DataField="email" HeaderText="Email" />
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:LinkButton ID="lbAnotherSearch" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Another Search' />
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlDetails" runat="server" Visible="False" DefaultButton="lbAnotherAccount">
        <div class="row">
            <div class="col-lg-12 ">
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td class="field-label col-xs-3 active bg-info"><label>Account</label></td>
                        <td class="col-md-9 bg-info"><asp:label ID="lblAccount" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>First Name</label></td>
                        <td class="col-md-9"><asp:label ID="lblFirstName" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Last Name</label></td>
                        <td class="col-md-9"><asp:label ID="lblLastName" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Display Name</label></td>
                        <td class="col-md-9"><asp:label ID="lblDisplayName" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Email</label></td>
                        <td class="col-md-9"><asp:label ID="lblEmail" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Description</label></td>
                        <td class="col-md-9"><asp:label ID="lblDescription" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Office</label></td>
                        <td class="col-md-9"><asp:label ID="lblOffice" runat="server" /></td>
                    </tr>
                    <tr><td colspan="2" style="background-color:lightgray">&nbsp;</td></tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Job Title</label></td>
                        <td class="col-md-9"><asp:label ID="lblTitle" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Department</label></td>
                        <td class="col-md-9"><asp:label ID="lblDepartment" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Company</label></td>
                        <td class="col-md-9"><asp:label ID="lblCompany" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Manager</label></td>
                        <td class="col-md-9"><asp:label ID="lblManager" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Groups</label></td>
                        <td class="col-md-9"><asp:DropDownList ID="ddlUserGroups" runat="server" CssClass="form-control" /></td>
                    </tr>
                    <tr><td colspan="2" style="background-color:lightgray">&nbsp;</td></tr>
                    <tr>
                        <td colspan="2">
                            <div class="table-responsive">
                            <asp:GridView ID="gvPSInfo" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AllowPaging="false" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Employee ID">
                                        <ItemTemplate>
                                            <asp:Label  ID="lblID" runat="server" Text='<%#Eval("id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label  ID="lblName" runat="server" Text='<%#Eval("name") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label  ID="lblDeptName" runat="server" Text='<%#Eval("deptname") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SSN">
                                        <ItemTemplate>
                                            <asp:Label  ID="lblSSN" runat="server" Text='<%#Eval("ssn") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="2" style="background-color:lightgray">&nbsp;</td></tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Last logon</label></td>
                        <td class="col-md-9"><asp:label ID="lblLastLogon" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Last password set</label></td>
                        <td class="col-md-9"><asp:label ID="lblLastPasswordSet" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Password expires</label></td>
                        <td class="col-md-9"><asp:label ID="lblAccountExpires" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Account Locked</label></td>
                        <td>
                            <asp:checkbox id="ckbAccountLocked" runat="server" enabled="false" AutoPostBack="true" TextAlign="Right" />
                            <asp:LinkButton ID="lbUnlock" runat="server" CausesValidation="False" Text='unlock account' Enabled="false" ForeColor="LightGray" /><br />
                            <asp:label ID="lblAccountLockoutTime" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="field-label col-xs-3 active"><label>Password</label></td>
                        <td class="col-md-9">
                            <table class="table-responsive">
                                <tr>
                                    <td><asp:textbox id="txtPassword1" runat="server" TextMode="Password" CssClass="form-control"/></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:textbox id="txtPassword2" runat="server" TextMode="Password" CssClass="form-control" /><br />
                                        <asp:label ID="lblPasswordStatus" runat="server" CssClass="bg-danger" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><asp:checkbox id="ckbForcePasswordChange" runat="server" enabled="true" Checked="false" TextAlign="Right" Text="User must change password at next logon" /></td>
                                </tr>
                                <tr>
                                    <td><asp:LinkButton ID="lbResetPassword" runat="server" CausesValidation="False" Text='Reset Password' /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <asp:label ID="lblStatus" runat="server" CssClass="bg-warning" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <asp:LinkButton ID="lbAnotherAccount" runat="server" CssClass="btn btn-default" CausesValidation="False" Text='Another Account' />
            </div>
        </div>

        </asp:Panel><%--end pnlDetails--%>
    </asp:Panel><%--end pnlAuthorized--%>
</asp:Content>