﻿Imports System.Data.OracleClient
Imports System.Data

Partial Class eDirectory
    Inherits System.Web.UI.Page

    Protected Sub lbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSearch.Click
        gvDetails.DataSource = GetDetailsTable()
        gvDetails.DataBind()
        gvDetails.SelectedIndex = 0
        pnlSearch.Visible = False
        pnlSearchResults.Visible = True
    End Sub

    Public Function GetDetailsTable() As DataTable
        Dim dt As New System.Data.DataTable("details")
        Dim dr As System.Data.DataRow
        Try
            Dim dc As DataColumn
            dc = New DataColumn("emplid", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("name", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("descr", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("deptname", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
            dc = New DataColumn("jobtitle", System.Type.GetType("System.String"))
            dt.Columns.Add(dc)

            Dim query As String = "Select DISTINCT EDE_PORTAL_PS_EMPLOYEES_VW.EMPLID, EDE_PORTAL_PS_EMPLOYEES_VW.LAST_NAME, COALESCE(NULLIF(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME,' '), EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME) as PREF_NAME, EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME, PS_LOCATION_TBL.DESCR, EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME, EDE_PORTAL_PS_EMPLOYEES_VW.JOBTITLE From sysadm.EDE_PORTAL_PS_EMPLOYEES_VW,sysadm.PS_LOCATION_TBL " & _
            "WHERE sysadm.EDE_PORTAL_PS_EMPLOYEES_VW.LOCATION = sysadm.PS_LOCATION_TBL.LOCATION AND " & _
            "PS_LOCATION_TBL.EFFDT = (SELECT MAX(LOC1.EFFDT) FROM sysadm.PS_LOCATION_TBL LOC1 WHERE PS_LOCATION_TBL.LOCATION = LOC1.LOCATION) AND " & _
            "EDE_PORTAL_PS_EMPLOYEES_VW.EMPL_STATUS IN ('A','L','P','S') AND " & _
            "UPPER(EDE_PORTAL_PS_EMPLOYEES_VW.LAST_NAME)  Like UPPER('%' || :lname || '%') AND " & _
            "UPPER(COALESCE(NULLIF(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME,' '), EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME)) Like UPPER('%' || :fname || '%') AND " & _
            "UPPER(PS_LOCATION_TBL.DESCR) Like UPPER('%' || :location || '%') AND " & _
            "UPPER(EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME) Like UPPER('%' || :department || '%') " & _
            "Order BY upper(EDE_PORTAL_PS_EMPLOYEES_VW.last_name), upper(COALESCE(NULLIF(EDE_PORTAL_PS_EMPLOYEES_VW.PREF_FIRST_NAME,' '), EDE_PORTAL_PS_EMPLOYEES_VW.FIRST_NAME))"

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
            Dim cmd As New OracleCommand(query, conn)
            cmd.Parameters.AddWithValue("lname", txtLName.text)
            cmd.Parameters.AddWithValue("fname", txtFName.text)
            cmd.Parameters.AddWithValue("location", ddlLocation.SelectedValue)
            cmd.Parameters.AddWithValue("department", ddlDepartment.SelectedValue)
            Dim datareader As OracleDataReader
            conn.Open()
            datareader = cmd.ExecuteReader
            If datareader.HasRows Then
                While datareader.Read
                    dr = dt.NewRow()
                    dr.Item("emplid") = datareader("EMPLID").ToString
                    dr.Item("name") = datareader("LAST_NAME").ToString & ", " & datareader("PREF_NAME").ToString
                    dr.Item("descr") = datareader("DESCR").ToString
                    dr.Item("deptname") = datareader("DEPTNAME").ToString
                    dr.Item("jobtitle") = datareader("JOBTITLE").ToString
                    dt.Rows.Add(dr)
                End While
            End If
            datareader.Close()
            conn.Close()
        Catch
        End Try
        Return dt
    End Function

    Public Function GetDetail(ByVal id As String) As Boolean
        Dim status As Boolean = False
        Try
            Dim query As String = "Select DISTINCT EDE_PORTAL_PS_EMPLOYEES_VW.EMPLID, EDE_PORTAL_PS_EMPLOYEES_VW.NAME, PS_LOCATION_TBL.DESCR, EDE_PORTAL_PS_EMPLOYEES_VW.DEPTNAME, EDE_PORTAL_PS_EMPLOYEES_VW.JOBTITLE, EDE_PORTAL_PS_EMPLOYEES_VW.WORK_PHONE From sysadm.EDE_PORTAL_PS_EMPLOYEES_VW,sysadm.PS_LOCATION_TBL " & _
                                           "Where EDE_PORTAL_PS_EMPLOYEES_VW.LOCATION = PS_LOCATION_TBL.LOCATION AND " & _
                                           "EDE_PORTAL_PS_EMPLOYEES_VW.EMPLID = :empid"
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("PeopleSoft").ConnectionString)
            Dim cmd As New OracleCommand(query, conn)
            Dim idParam As New OracleParameter("empid",OracleType.Int32)
            idParam.Value = Convert.toInt32(id)
            cmd.Parameters.Add(idparam)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    lblDetailName.Text = dr(1).ToString
                    lblDetailTitle.Text = dr(4).ToString
                    lblDetailDepartment.Text = dr(3).ToString
                    lblDetailLocation.Text = dr(2).ToString
                    lblDetailPhone.Text = dr(5).ToString
                End While
                imgPhoto.ImageUrl = ResolveUrl("~/edirImage.aspx") & "?id=" & id
                status = True
            Else
                status = False
            End If
            dr.Close()
            conn.Close()
        Catch
            Return False
        End Try
        Return status
    End Function

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gvDetails.DataSource = GetDetailsTable()
        gvDetails.DataBind()
    End Sub

    Protected Sub gvDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails.RowCommand
        Try
            If e.CommandName = "Select" Then
                If GetDetail(e.CommandArgument.ToString) Then
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#myModal').modal('show');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DetailsModalScript", sb.ToString(), False)
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub lbAnotherSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAnotherSearch.Click
        txtLName.Text = ""
        txtFName.Text = ""
        ddlDepartment.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        pnlSearchResults.Visible = False
        pnlSearch.Visible = True
    End Sub

    Protected Function SortDataTable(ByVal dataTable As DataTable, ByVal isPageIndexChanging As Boolean) As DataView
        If Not dataTable Is Nothing Then
            Dim dataView As New DataView(dataTable)
            If GridViewSortExpression <> String.Empty Then
                If isPageIndexChanging Then
                    dataView.Sort = String.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection)
                Else
                    dataView.Sort = String.Format("{0} {1}", GridViewSortExpression, GetSortDirection())
                End If
            End If
            Return dataView
        Else
            Return New DataView()
        End If
    End Function
    Private Property GridViewSortExpression() As String
        Get
            Return IIf(ViewState("SortExpression") = Nothing, String.Empty, ViewState("SortExpression"))
        End Get
        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property
    Private Property GridViewSortDirection() As String
        Get
            Return IIf(ViewState("SortDirection") = Nothing, "ASC", ViewState("SortDirection"))
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property
    Private Function GetSortDirection() As String
        Select Case GridViewSortDirection
            Case "ASC"
                GridViewSortDirection = "DESC"
            Case "DESC"
                GridViewSortDirection = "ASC"
        End Select
        Return GridViewSortDirection
    End Function

    Protected Sub gvDetails_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvDetails.Sorting
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = gvDetails.PageIndex

        gvDetails.DataSource = SortDataTable(GetDetailsTable, False)
        gvDetails.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtLName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + txtLName.UniqueID + "','')")
        txtFName.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + txtFName.UniqueID + "','')")
        txtLName.Focus()
    End Sub

    Protected Sub ddlDepartment_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.DataBound
        ddlDepartment.Items.Insert(0, New ListItem("- Select -", ""))
    End Sub

    Protected Sub ddlLocation_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.DataBound
        ddlLocation.Items.Insert(0, New ListItem("- Select -", ""))
    End Sub
End Class
