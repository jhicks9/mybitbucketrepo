﻿<%@ WebHandler Language="VB" Class="FileHandler" %>

Imports System
Imports System.Web
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

Public Class FileHandler : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim fname As String = ""
            Dim id As String = ""
            Dim dtype As String = ""
            Dim contenttype As String = ""
            Dim view As Boolean = false
            Dim ps As New portalSecurity
            
            If Not String.IsNullOrEmpty(context.Request.QueryString("dtype")) Then
                dtype = context.Request.QueryString("dtype")
            End If
            If Not String.IsNullOrEmpty(context.Request.QueryString("view")) Then
                view = true
            End If
            If Not String.IsNullOrEmpty(context.Request.QueryString("id")) Then
                id = context.Request.QueryString("id")
            End If
            If IsNumeric(id) Then
                Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
                    Using cmd As New SqlCommand("", conn)
                        Select Case dtype 'determine which table to read
                            Case "inserts"
                                cmd.CommandText = "select filename from billinsert where billinsert.id=@id"
                                cmd.Parameters.Add(New SqlParameter("id", id))
                             Case "modules"
                                cmd.CommandText = "select modules.module_filename as filename from modules where module_id=@id"
                                cmd.Parameters.Add(New SqlParameter("id", id))
                            Case Else
                                cmd.CommandText = "select documents.filename, documents.doctypeid, documents.groupid, doctype.restricted, (select security.doctypeid from documents as documents_1 inner join doctype AS doctype_1 on documents_1.doctypeid = doctype_1.id inner join security ON documents_1.groupid = security.groupid where (security.userid = @userid) and (documents_1.id = @id)) AS seclist from documents inner join doctype on documents.doctypeid = doctype.id where (documents.id = @id)"
                                cmd.Parameters.Add(New SqlParameter("id", id))
                                cmd.Parameters.Add(New SqlParameter("userid", ps.getUsername))                                
                        End Select
                       
                        conn.Open()
                        Dim datareader As SqlDataReader = cmd.ExecuteReader()
                        If datareader.HasRows Then
                            While datareader.Read
                                fname = datareader.GetValue(0).ToString
                            End While
                        End If
                        datareader.Close()
                        
                        If Not String.IsNullOrEmpty(fname) Then 'file found
                            Select Case LCase(Path.GetExtension(fname))
                                Case ".htm", ".html"
                                    contenttype = "text/HTML"
                                Case ".txt"
                                    contenttype = "text/plain"
                                Case ".doc", ".rtf"
                                    contenttype = "Application/msword"
                                Case ".ppt"
                                    contenttype = "application/vnd.ms-powerpoint"
                                Case ".csv", ".xls"
                                    contenttype = "Application/x-msexcel"
                                Case ".pdf"
                                    contenttype = "Application/pdf"
                                Case ".xml", ".xsd"
                                    contenttype = "application/xml"
                                Case ".jpg", ".jpeg"
                                    contenttype = "image/jpeg"
                                Case Else
                                    contenttype = "text/plain"
                            End Select
                            If Not string.IsNullOrEmpty(contenttype) Then 'valid content type
                                context.Response.ContentType = contenttype
                                If Not view then 'download file unless view is specified
                                    context.Response.AppendHeader("content-disposition", "attachment; filename=" & fname)    
                                End If
                                Select Case dtype
                                    Case "inserts"
                                        cmd.CommandText = "SELECT imagedata from billinsert where id=@id and filename=@filename"
                                    Case "modules"
                                        cmd.CommandText = "select modules.module_image from modules where modules.module_id=@id and modules.module_filename=@filename"
                                    Case Else
                                        cmd.CommandText  = "Select filebytes from documents where id=@id and filename=@filename"        
                                End Select
                                cmd.Parameters.Add(New SqlParameter("filename", fname))
                                Dim file() As Byte = CType(cmd.ExecuteScalar(), Byte())
                                context.Response.BinaryWrite(file)
                            End If
                        Else
                            Throw New System.Exception("Document not found")
                        End If
                        
                        cmd.Connection.Close()
                        conn.Close()
                    End Using  'end command
                End Using 'end connection
            Else
                Throw New System.Exception("Document not found")
            End If
        Catch ex As Exception
            ' ** displays message in browser
            context.Response.Write(ex.Message)
        End Try
    End Sub
    
    Public Sub DisplayDefault()  'empty graphic
        Try
            Dim path As String = HttpContext.Current.Server.MapPath("~/images/ico_empty.png")
            Dim byteArray As Array = File.ReadAllBytes(path)
            HttpContext.Current.Response.BinaryWrite(byteArray)
        Catch
        End Try
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class