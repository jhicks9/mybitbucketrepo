﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ajaxModalPopup.ascx.vb" Inherits="ajaxModalPopup" %>

<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title text-center">
                    <i class="fa fa-refresh fa-spin fa-large"></i> Loading...
                </div>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<%--<a class="btn" data-toggle="modal" href="#loadingModal" onclick = "$('.loadingModal').show()" >Launch Modal</a>--%>

<script type="text/javascript">

    Sys.Net.WebRequestManager.add_invokingRequest(onInvoke);
    Sys.Net.WebRequestManager.add_completedRequest(onComplete);

    function onInvoke(sender, args) {
        $('#loadingModal').modal('show');
    }

    function onComplete(sender, args) {
        $('#loadingModal').modal('hide');
    }

    function pageUnload() {
        Sys.Net.WebRequestManager.remove_invokingRequest(onInvoke);
        Sys.Net.WebRequestManager.remove_completedRequest(onComplete);
    }
</script>