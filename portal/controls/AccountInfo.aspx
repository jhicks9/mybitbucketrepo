﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="AccountInfo.aspx.vb" Inherits="controls_AccountInfo" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-lg-12 ">
            <table class="table table-condensed table-bordered">
                <tr>
                    <td class="field-label col-xs-3 active bg-info"><label>Account</label></td>
                    <td class="col-md-9 bg-info"><asp:label ID="lblAccount" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>First Name</label></td>
                    <td class="col-md-9"><asp:label ID="lblFirstName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Last Name</label></td>
                    <td class="col-md-9"><asp:label ID="lblLastName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Display Name</label></td>
                    <td class="col-md-9"><asp:label ID="lblDisplayName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Email</label></td>
                    <td class="col-md-9"><asp:label ID="lblEmail" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Description</label></td>
                    <td class="col-md-9"><asp:label ID="lblDescription" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Office</label></td>
                    <td class="col-md-9"><asp:label ID="lblOffice" runat="server" /></td>
                </tr>
                <tr><td colspan="2" style="background-color:lightgray">&nbsp;</td></tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Job Title</label></td>
                    <td class="col-md-9"><asp:label ID="lblTitle" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Department</label></td>
                    <td class="col-md-9"><asp:label ID="lblDepartment" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Company</label></td>
                    <td class="col-md-9"><asp:label ID="lblCompany" runat="server" /></td>
                </tr>
                <tr>
                    <td class="field-label col-xs-3 active"><label>Manager</label></td>
                    <td class="col-md-9"><asp:label ID="lblManager" runat="server" /></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
