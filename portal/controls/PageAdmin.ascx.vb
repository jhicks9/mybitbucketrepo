﻿
Partial Class controls_PageAdmin
    Inherits System.Web.UI.UserControl

    Dim ps As New portalSecurity
    Public Function getParentUserControlName() As String
        Try  'Must reside in the control.  User control name is not available via classes.
            Dim ucname As String = ""
            Dim ucn As Array
            ucname = Parent.TemplateControl.[GetType].ToString().Replace("ASP.", "").Replace("_ascx", ".ascx").Replace("_aspx", ".aspx")
            ucn = ucname.Split("_")
            Return ucname.Split("_")(ucn.Length - 1).ToString
        Catch
            Return ""
        End Try
    End Function

    Protected Sub lbPageAdminSecurity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPageAdminSecurity.Click
        If pnlPageAdminUsers.Visible Then
            pnlPageAdminUsers.Visible = False
        Else
            pnlPageAdminUsers.Visible = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sqlDSUsers.SelectParameters (0).DefaultValue = getParentUserControlName()
        hdnPageAdminUsername.Value = ps.getUsername
    End Sub

    Protected Sub gvPageAdminUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPageAdminUsers.RowCommand
        Select Case e.CommandName
            Case "Select"
                PageAdminUserDetails.HeaderText = "Edit user"
                PageAdminUserDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#pageAdmin').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvPageAdminUsers.SelectedIndex = -1
                PageAdminUserDetails.HeaderText = "Add new user"
                PageAdminUserDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#pageAdmin').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub
    Protected Sub gvUsers_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPageAdminUsers.Sorting
        gvPageAdminUsers.SelectedIndex = -1
    End Sub
    Protected Sub gvUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPageAdminUsers.PageIndexChanging
        gvPageAdminUsers.SelectedIndex = -1
    End Sub

    Protected Sub PageAdminUserDetails_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageAdminUserDetails.DataBound
        Try
            Dim txtUN As TextBox = CType(PageAdminUserDetails.FindControl("txtPageAdminUserName"), TextBox)
            Dim lblStatus As Label = CType(PageAdminUserDetails.FindControl("lblStatus"), Label)
            If (PageAdminUserDetails.CurrentMode = DetailsViewMode.Edit) Then
                Dim insertButton As LinkButton = CType(PageAdminUserDetails.FindControl("lbItemInsert"), LinkButton)
                insertButton.Visible = false 'no need to display insert button on an edit
                Dim update As LinkButton = CType(PageAdminUserDetails.FindControl("lbEditUpdate"), LinkButton)
                update.Attributes("onclick") = String.Format("return ValidateUser( '{0}', '{1}');", txtUN.ClientID, lblStatus.ClientID)
            End If
            If (PageAdminUserDetails.CurrentMode = DetailsViewMode.Insert) Then
                Dim update As LinkButton = CType(PageAdminUserDetails.FindControl("lbEditUpdate"), LinkButton)
                Dim delete As LinkButton = CType(PageAdminUserDetails.FindControl("lbEditDelete"), LinkButton)
                update.Visible = False 'no need to display update button on an insert
                delete.Visible = false 'no need to display delete button on an insert
                Dim insert As LinkButton = CType(PageAdminUserDetails.FindControl("lbItemInsert"), LinkButton)
                insert.Attributes("onclick") = String.Format("return ValidateUser( '{0}', '{1}');", txtUN.ClientID, lblStatus.ClientID)
            End If
        Catch
        End Try
    End Sub
    Protected Sub PageAdminUserDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles PageAdminUserDetails.ItemCommand
        Dim sb As New StringBuilder()
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvPageAdminUsers.SelectedIndex = -1
    End Sub
    Protected Sub PageAdminUserDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles PageAdminUserDetails.ItemDeleted
        gvPageAdminUsers.SelectedIndex = -1
        gvPageAdminUsers.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#pageAdmin').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DetailModalScript", sb.ToString(), False)
    End Sub
    Protected Sub PageAdminUserDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles PageAdminUserDetails.ItemInserted
        gvPageAdminUsers.DataBind()
        gvPageAdminUsers.PageIndex = gvPageAdminUsers.PageCount - 1 'Go to the last page to show inserted record
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#pageAdmin').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DetailModalScript", sb.ToString(), False)
    End Sub
    Protected Sub PageAdminUserDetails_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles PageAdminUserDetails.ItemInserting
        sqlDSUsers2.InsertParameters("pageid").DefaultValue = gvPageAdminUsers.DataKeys(0).Values("pageid").ToString
    End Sub
    Protected Sub PageAdminUserDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles PageAdminUserDetails.ItemUpdated
        gvPageAdminUsers.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#pageAdmin').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "DetailModalScript", sb.ToString(), False)
    End Sub
    Function GetUserTypeSelectedIndex(ByVal value As String) As Integer
        Dim i As Integer = 0
        If PageAdminUserDetails.CurrentMode = DetailsViewMode.Insert Then
            i = 1  'default to 'user' for all new entries
        Else
            Select Case value
                Case "user"
                    i = 1
                Case "admin"
                    i = 0
            End Select
        End If
        Return i
    End Function

    Protected Sub gvPageAdminUsers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPageAdminUsers.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            'do not allow changes to your own account -- disabled 09/29/2015
            'Dim lb As LinkButton = e.Row.FindControl("lbSelect")
            'If e.Row.Cells(1).Text = ps.getUsername Then
            '    lb.Enabled = False
            'End If
        End If
    End Sub
End Class