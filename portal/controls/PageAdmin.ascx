﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PageAdmin.ascx.vb" Inherits="controls_PageAdmin" %>

<div style="padding-top:5px"">

    <div class="row">
        <div class="col-lg-12">
            <asp:LinkButton ID="lbPageAdminSecurity" CssClass="btn btn-default pull-right" runat="server" CausesValidation="false"><span class="fa fa-key"></span></asp:LinkButton>
            <asp:HiddenField ID="hdnPageAdminUsername" runat="server" Value="" />
        </div>
    </div>

    <asp:Panel ID="pnlPageAdminUsers" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="updateGridview" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvPageAdminUsers" runat="server" DataSourceID="sqlDSUsers" DataKeyNames="id,pageid" AllowPaging="true" OnRowCommand="gvPageAdminUsers_RowCommand" Width="100%" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbAdd" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                                CommandName="AddRecord" CausesValidation="False"><span class="fa fa-user-plus"></span> Add new User</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="false" CommandName="Select" CssClass="btn btn-default"
                                                CommandArgument='<%# Eval("pageid") %>'><span class="fa fa-user"></span> Edit User</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    <asp:BoundField DataField="username" HeaderText="User name" SortExpression="username" />
                                    <asp:BoundField DataField="usertype" HeaderText="Type" SortExpression="usertype" />
                                    <asp:CheckBoxField DataField="insertrecord" HeaderText="insert" SortExpression="insertrecord" />
                                    <asp:CheckBoxField DataField="updaterecord" HeaderText="update" SortExpression="updaterecord" />
                                    <asp:CheckBoxField DataField="deleterecord" HeaderText="delete" SortExpression="deleterecord" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:LinkButton ID="lbAddEmpty" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                        CommandName="AddRecord" CausesValidation="False"><span class="fa fa-user-plus"></span> Add new user</asp:LinkButton>
                                </EmptyDataTemplate>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </asp:Panel>

    <div class="modal fade" id="pageAdmin" tabindex="-1" role="dialog" aria-labelledby="pageAdminLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="updatePageAdminDetails" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="PageAdminUserDetails" runat="server" DataKeyNames="id" DataSourceID="sqlDSUsers2" Width="100%" CssClass="table table-striped table-bordered table-hover"
                                    AutoGenerateRows="False">
                                <Fields>
                                    <asp:templatefield headertext="Username" HeaderStyle-CssClass="field-label col-xs-3 active">
                                        <edititemtemplate>
                                            <asp:TextBox ID="txtPageAdminUserName" runat="server" Text='<%# Bind("username") %>' CssClass="col-md-9 form-control"></asp:TextBox><br />
                                            <asp:Label ID="lblStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" CssClass="danger"></asp:Label>
                                        </edititemtemplate>
                                    </asp:templatefield>
                                    <asp:templatefield headertext="Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlPageAdminUserType" runat="server" SelectedValue='<%# Bind("usertype") %>' SelectedIndex='<%#GetUserTypeSelectedIndex( DataBinder.Eval(Container.DataItem,"usertype") ) %>' CssClass="col-md-9 form-control">
                                                <asp:ListItem>admin</asp:ListItem>
                                                <asp:ListItem>user</asp:ListItem>
                                            </asp:DropDownList>                                                                
                                        </EditItemTemplate>
                                    </asp:templatefield>                    
                                    <asp:CheckBoxField DataField="insertrecord" HeaderText="Insert" SortExpression="insertrecord" HeaderStyle-CssClass="field-label col-xs-3 active" />
                                    <asp:CheckBoxField DataField="updaterecord" HeaderText="Update" SortExpression="updaterecord" HeaderStyle-CssClass="field-label col-xs-3 active" />
                                    <asp:CheckBoxField DataField="deleterecord" HeaderText="Delete" SortExpression="deleterecord" HeaderStyle-CssClass="field-label col-xs-3 active" />
                                </Fields>
                                <FooterTemplate>
                                    <div class="pull-right">
                                        <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" CausesValidation="false" Text="Delete" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete the user?"")" %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Close" CssClass="btn btn-default" data-dismiss="modal" />
                                        <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" CausesValidation="false" Text="Save changes" CssClass="btn btn-primary" />
                                        <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="false" Text="Save changes" CssClass="btn btn-primary" />
                                    </div>
                                </FooterTemplate>
                            </asp:DetailsView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</div>
   
<asp:SqlDataSource ID="sqlDSUsers" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select * from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id AND ps_pages.name = @parentusercontrol order by ps_security.username">
    <SelectParameters>
        <asp:Parameter Name="parentusercontrol" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqlDSUsers2" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select * from ps_security where (ps_security.id = @id)"
    InsertCommand="insert into ps_security (pageid,username,usertype,insertrecord,updaterecord,deleterecord) VALUES (@pageid,@username,@usertype,@insertrecord,@updaterecord,@deleterecord)"
    UpdateCommand="update ps_security set username=@username,usertype=@usertype,insertrecord=@insertrecord,updaterecord=@updaterecord,deleterecord=@deleterecord where id = @id"
    DeleteCommand="delete from ps_security where id = @id">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvPageAdminUsers" Name="id" PropertyName="SelectedValue"/>
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="pageid" Type="Int32" />
    </InsertParameters>
    <DeleteParameters>
        <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue" ControlID="gvPageAdminUsers"></asp:ControlParameter>
    </DeleteParameters>                       
</asp:SqlDataSource>

<script type="text/javascript">
    function ValidateUser(txtid, statusid) {
        var unid = '<%= hdnPageAdminUsername.ClientID %>';
        var un = document.getElementById(unid).value;
        var txt = document.getElementById(txtid).value;
        var status = document.getElementById(statusid);
        status.innerHTML = '';
        if (txt == '') {
            status.innerHTML = 'Username required';
            return false;   }
        if (txt == un) {
            status.innerHTML = 'Cannot add yourself';
            return false;   }
        return true;   }
</script>
