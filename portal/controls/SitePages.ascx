﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SitePages.ascx.vb" Inherits="controls_SitePages" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>

<asp:Panel ID="pnlSitePagesNotAuthorized" runat="server" Visible="False">
    <uc5:unauthorized ID="unAuthorized1" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlSitePagesAdministration" runat="server" Visible="False">
    <a1:Administration ID="PageAdmin1" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlSitePagesAuthorized" runat="server" Visible="True">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <asp:UpdatePanel ID="pnlSitePages" runat="server" >
                    <ContentTemplate>
                        <asp:GridView ID="gvSitePages" runat="server" DataSourceID="dsSitePages" DataKeyNames="id" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbAdd" runat="server" CommandName="AddRecord" CausesValidation="false" CssClass="btn btn-primary"
                                            ToolTip="Add new page"><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbDetails" runat="server" CausesValidation="false" CommandName="Select" CommandArgument="EditRecord"
                                            ToolTip="Edit page" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="description" HeaderText="Page" SortExpression="description" />
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <EmptyDataTemplate>
                                <asp:LinkButton ID="lbAdd" runat="server" CommandName="AddRecord" CausesValidation="false" CssClass="btn btn-primary"
                                            ToolTip="Add new page"><span class="glyphicon glyphicon-plus">&nbsp;Add</span></asp:LinkButton>
                                Add new page/module<br />                            
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate> 
                </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="modal fade" id="SitePagesDetail" tabindex="-1" role="dialog" aria-labelledby="SitePagesDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="pnlSitePagesDetails" runat="server">
                            <contenttemplate>                     
                                <asp:DetailsView ID="SitePagesDetails" runat="server" DataKeyNames="id" DataSourceID="dsSitePagesDetail" CssClass="table table-striped table-bordered table-hover"
                                    AutogenerateRows="false">
                                    <Fields>
                                        <asp:templatefield headertext="Name" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name") %>' CssClass="col-md-9 form-control" />
                                            <asp:Label ID="lblSitePagesValidation" runat="server" Text="" ForeColor="Red" />
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="required" SetFocusOnError="True" Display="Dynamic" ValidationGroup="sitepagesdetails" />
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:templatefield headertext="Path" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtPath" runat="server" Text='<%# Bind("path") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvPath" runat="server" ControlToValidate="txtPath" ErrorMessage="required" SetFocusOnError="True" ValidationGroup="sitepagesdetails"/>
                                          </edititemtemplate>
                                        </asp:templatefield>                                                        
                                        <asp:templatefield headertext="Description" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("description") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" ErrorMessage="required" SetFocusOnError="True" ValidationGroup="sitepagesdetails"/>
                                          </edititemtemplate>
                                        </asp:templatefield>
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:Label ID="lblSitePagesError" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="sitepagesdetails"/>
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="sitepagesdetails"/>
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal"/>
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Label ID="lblSitePagesStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" />

                                <asp:GridView ID="gvSitePagesUsers" runat="server" DataSourceID="dsSitePagesUsers" AllowPaging="true" PageSize="3" AllowSorting="false" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="username" HeaderText="Username" />
                                        <asp:BoundField DataField="usertype" HeaderText="Type" />
                                        <asp:BoundField DataField="description" HeaderText="Description" />
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate></EmptyDataTemplate>
                                </asp:GridView>
                            </contenttemplate>
                        </asp:UpdatePanel><!-- pnlPagesDetails -->
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel><!-- pnlSitePagesAuthorized -->

<asp:SqlDataSource ID="dsSitePages" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select id,description from ps_pages order by description">
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSitePagesDetail" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_pages.id,ps_pages.name,ps_pages.path,ps_pages.description from ps_pages where (ps_pages.id = @id)"
    InsertCommand="insert into ps_pages (name,path,description) VALUES (@name,@path,@description)"
    UpdateCommand="update ps_pages SET name=@name,path=@path,description=@description where id = @id"
    DeleteCommand="delete from ps_pages where id = @id">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvSitePages" Name="id" PropertyName="SelectedValue"/>
    </SelectParameters>
    <InsertParameters>
        <asp:ControlParameter ControlID="gvSitePages" Type="Int32" Name="pageid" PropertyName="SelectedDataKey.Value"/>
        <asp:Parameter Name="usertype" Type="String" DefaultValue="admin"/>
        <asp:Parameter Name="username" Type="String" />
    </InsertParameters>
    <DeleteParameters>
        <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue" ControlID="gvSitePages"></asp:ControlParameter>
    </DeleteParameters>                       
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSitePagesUsers" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_security.username,ps_security.usertype,ps_pages.description from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id and pageid = @pageid order by ps_security.usertype, ps_security.username">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvSitePages" Type="Int32" Name="pageid" PropertyName="SelectedDataKey.Value"/>
    </SelectParameters>
</asp:SqlDataSource>