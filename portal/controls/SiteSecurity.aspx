﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="SiteSecurity.aspx.vb" Inherits="controls_SiteSecurity" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorized" tagprefix="uc5" %>
<%@ Register src="~/controls/PageAdmin.ascx" tagname="Administration" tagprefix="a1" %>
<%@ Register src="~/controls/SitePages.ascx" tagname="SitePages" tagprefix="sp1" %>
<%@ Register src="~/controls/SiteAdmin.ascx" tagname="SiteAdmin" tagprefix="sa1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlNotAuthorized" runat="server" Visible="False">
        <uc5:unauthorized ID="unAuthorized1" runat="server" />
    </asp:Panel>
    
    <asp:Panel ID="pnlAdministration" runat="server" Visible="False">
        <a1:Administration ID="PageAdmin1" runat="server" />
    </asp:Panel>

    <asp:Panel ID="pnlAuthorized" runat="server" Visible="True">
        <div class="row">
            <div class="col-lg-6">
                <sa1:SiteAdmin ID="SiteAdmin1" runat="server" />
            </div>
            <div class="col-lg-6">
                <sp1:SitePages ID="SitePages1" runat="server" />
            </div>
        </div>
        
    </asp:Panel><%--end pnlAuthorized--%>
</asp:Content>
