﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SiteAdmin.ascx.vb" Inherits="controls_SiteAdmin" %>
<%@ Register src="~/controls/unauthorized.ascx" tagname="unAuthorizedSiteAdmin" tagprefix="uc5" %>

    
<asp:Panel ID="pnlSiteAdminNotAuthorized" runat="server" Visible="False">
    <uc5:unAuthorizedSiteAdmin ID="unAuthorizedSiteAdmin1" runat="server" />
</asp:Panel>
            
<asp:Panel ID="pnlSiteAdminAuthorized" runat="server" Visible="False">
    <div class="row">
        <div class="col-lg-12">
            <asp:LinkButton ID="lbSecurity" CssClass="btn btn-default pull-right" runat="server" ><span class="fa fa-key"></span></asp:LinkButton>
        </div>
    </div>
    <asp:Panel ID="pnlSiteAdminUsers" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="updateSiteAdmins" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvSiteAdmins" runat="server" DataSourceID="dsSiteAdmins" DataKeyNames="id,username" AllowPaging="true" PageSize="5" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbAdd" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                                CommandName="AddRecord" CausesValidation="False"><span class="fa fa-user-plus"></span> Add new admin</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="false" CommandName="Select" CssClass="btn btn-default"
                                                CommandArgument='<%# Eval("id") %>'><span class="fa fa-user"></span> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="username" HeaderText="User name" SortExpression="username" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:LinkButton ID="lbAddEmpty" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                        CommandName="AddRecord" CausesValidation="False"><span class="fa fa-user-plus"></span> Add new admin</asp:LinkButton>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </ContentTemplate> 
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </asp:Panel><!-- pnlSiteAdminUsers -->

    <div class="modal fade" id="siteadminsDetail" tabindex="-1" role="dialog" aria-labelledby="siteadminsDetailLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <asp:UpdatePanel ID="updateSiteAdminsDetails" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="SiteAdminUserDetails" runat="server" DataKeyNames="id" DataSourceID="dsSiteAdminsUserDetails" CssClass="table table-striped table-bordered"
                                    AutoGenerateRows="False">
                                <Fields>
                                    <asp:BoundField DataField="id" ReadOnly="true" Visible="false" />
                                    <asp:templatefield headertext="Username" HeaderStyle-CssClass="field-label col-xs-3 active">
                                        <edititemtemplate>
                                            <asp:TextBox ID="txtSiteAdminUserName" runat="server" Text='<%# Bind("username") %>' CssClass="col-md-9 form-control" />
                                            <asp:Label ID="lblStatus" runat="server" Text="" ForeColor="Red" Font-Bold="true" CssClass="danger" />
                                        </edititemtemplate>
                                    </asp:templatefield>
                                    <asp:templatefield headertext="Type" HeaderStyle-CssClass="field-label col-xs-3 active">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Eval("usertype") %>' CssClass="col-md-9 form-control no-border" />
                                        </EditItemTemplate>
                                    </asp:templatefield>
                                </Fields>
                                <FooterTemplate>
                                    <div class="pull-right">
                                        <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" CausesValidation="false" Text="Delete" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete the user?"")" %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" CausesValidation="false" Text="Close" CssClass="btn btn-default" data-dismiss="modal"></asp:LinkButton>
                                        <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" CausesValidation="false" Text="Save changes" CssClass="btn btn-primary"></asp:LinkButton>
                                        <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" CausesValidation="false" Text="Save changes" CssClass="btn btn-primary"></asp:LinkButton>        
                                    </div>
                                </FooterTemplate>
                            </asp:DetailsView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

        <div class="container_1">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#tab_a" data-toggle="pill">Administrators</a></li>
                <li><a href="#tab_b" data-toggle="pill">Site Users</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updatePageAdmin" runat="server">
                            <contenttemplate>
                            <asp:GridView ID="gvPageAdmins" runat="server" DataSourceID="dsPageAdmins" DataKeyNames="id" AllowSorting="true" AllowPaging="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbSelectSiteUser" runat="server" CausesValidation="false" CommandName="Select" 
                                                ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="id" SortExpression="id" HeaderText="id" ReadOnly="True" Visible="False"></asp:BoundField>
                                    <asp:BoundField DataField="description" SortExpression="description" HeaderText="Page/Module" />
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                                <EmptyDataTemplate>No Pages</EmptyDataTemplate>
                            </asp:GridView>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-A -->
                <div class="tab-pane" id="tab_b">
                    <div class="table-responsive">
                        <asp:UpdatePanel ID="updateSiteUsers" runat="server">
                            <contenttemplate>
                            <asp:GridView ID="gvSiteUsers" runat="server" DataSourceID="dsSiteUsers" DataKeyNames="username" AllowSorting="true" AllowPaging="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbSelectSiteUser" runat="server" CausesValidation="false" CommandName="Select"
                                                ToolTip="Edit" CssClass="btn btn-default"><span class="glyphicon glyphicon-edit">&nbsp;Edit</span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="username" HeaderText="Username" SortExpression="username" />
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                                <EmptyDataTemplate>No users defined</EmptyDataTemplate>
                              </asp:GridView>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div><!-- tab-B -->
            </div><!-- tab-content -->
        </div><!-- tab container -->

        <div class="modal fade" id="siteusersDetail" tabindex="-1" role="dialog" aria-labelledby="siteusersDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updateSiteUsersDetails" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="SiteUsersDetails" runat="server" DataKeyNames="username" DataSourceID="dsSiteUsersDetails" CssClass="table table-striped table-bordered"
                                    AutoGenerateRows="false">
                                    <Fields>
                                        <asp:templatefield headertext="Username" HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <edititemtemplate>
                                                <asp:TextBox ID="txtUserNames" runat="server" Text='<%# Bind("username") %>' CssClass="col-md-9 form-control" />
                                                <asp:RequiredFieldValidator ID="rfvUserNames" runat="server" ControlToValidate="txtUserNames" ErrorMessage="required" SetFocusOnError="True"
                                                    Display="Dynamic" ValidationGroup="SiteUsersDetails" />
                                            </edititemtemplate>                                                                   
                                        </asp:templatefield>
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="SiteUsersDetails" />
                                                <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                                <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                                <asp:GridView ID="gvSiteUserPages" runat="server" DataSourceID="dsSiteUserPages" PageSize="5" AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="usertype" HeaderText="Type" />
                                        <asp:TemplateField HeaderText="Page/Module" SortExpression="description">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlSelectSiteUserPage" runat="server" NavigateUrl='<%# ResolveUrl(Eval("path")) %>' Text='<%# Bind("description") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate></EmptyDataTemplate>
                                </asp:GridView>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pageadminsDetail" tabindex="-1" role="dialog" aria-labelledby="pageadminsDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updatePageAdminDetails" runat="server">
                            <contenttemplate>
                                <p class="well-sm bg-info text-center"><strong><asp:Label ID="lblPageAdminDetailsDesc" runat="server" /></strong></p>
                                <asp:GridView ID="gvPageAdminDetails" runat="server" DataSourceID="dsPageAdminsDetail" DataKeyNames="id" AllowSorting="true" AllowPaging="true" CssClass="table table-striped table-bordered table-hover" UseAccessibleHeader="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="true" Visible="false" />
                                        <asp:TemplateField ShowHeader="False">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbAdd" CssClass="btn btn-primary" runat="server" ToolTip="Add new admin" 
                                                    CommandName="AddRecord" CausesValidation="False"><span class="fa fa-user-plus"></span> Add new admin</asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbSelect" runat="server" CausesValidation="false" CommandName="Select" 
                                                    CssClass="btn btn-default"><span class="fa fa-user"></span> Edit</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>   
                                        <asp:TemplateField HeaderText="Administrators" ShowHeader="True">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPageAdminDetailUser" runat="server" Text='<%# Bind("username") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="lbAddEmpty" CssClass="btn btn-primary" runat="server" ToolTip="Add new user" 
                                            CommandName="AddRecord" CausesValidation="False"><span class="fa fa-user-plus"></span> Add new admin</asp:LinkButton>
                                        Add new administrator
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="row">
                                    <div class="col-lg-12 active">
                                        <div class="pull-right">
                                        <asp:LinkButton ID="lbPageAdminDetailsClose" CommandName="Cancel" runat="server" CausesValidation="false" Text="Close" CssClass="btn btn-default" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pageadminsUserDetail" tabindex="-1" role="dialog" aria-labelledby="pageadminsDetailLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <asp:UpdatePanel ID="updatePageAdminsUserDetail" runat="server">
                            <contenttemplate>
                                <asp:DetailsView ID="PageAdminUsersDetails" runat="server" DataKeyNames="id" DataSourceID="dsPageAdminUsersDetails" 
                                    CssClass="table table-striped table-bordered" AutoGenerateRows="false">
                                    <Fields>
                                        <asp:templatefield headertext="Username" HeaderStyle-CssClass="field-label col-xs-3 active">
                                          <edititemtemplate>
                                            <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("username") %>' CssClass="col-md-9 form-control" />
                                            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="required" Display="Dynamic" SetFocusOnError="True" ValidationGroup="PageAdminUsersDetail" />
                                          </edititemtemplate>                                                                   
                                        </asp:templatefield>
                                        <asp:TemplateField HeaderStyle-CssClass="field-label col-xs-3 active">
                                            <InsertItemTemplate>
                                              <asp:LinkButton ID="lbItemInsert" CommandName="Insert" runat="server" Font-Bold="true" Text="Insert" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="PageAdminUsersDetail" />
                                              <asp:LinkButton ID="lbInsertCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                            </InsertItemTemplate>                                
                                            <EditItemTemplate>
                                              <asp:LinkButton ID="lbEditUpdate" CommandName="Update" runat="server" Font-Bold="true" Text="Update" CausesValidation="true" CssClass="btn btn-primary" ValidationGroup="PageAdminUsersDetail" />
                                              <asp:LinkButton ID="lbEditCancel" CommandName="Cancel" runat="server" Font-Bold="true" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" data-dismiss="modal" />
                                              <asp:LinkButton ID="lbEditDelete" CommandName="Delete" runat="server" Font-Bold="true" Text="Delete" CausesValidation="false" CssClass="btn btn-warning" OnClientClick='<%# "return confirm(""Are you sure you want to delete this record?"")" %>' />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                            </contenttemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

</asp:Panel><!-- pnlSiteAdminAuthorized -->

<asp:SqlDataSource ID="dsSiteUsers" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select distinct ps_security.username from ps_security order by ps_security.username">
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSiteUsersDetails" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select distinct ps_security.username from ps_security where (ps_security.username = @username)"
    UpdateCommand="update ps_security SET username=@newusername where username = @username"
    DeleteCommand="delete from ps_security where username = @username">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvSiteUsers" Type="String" Name="username" PropertyName="SelectedValue"/>
    </SelectParameters>
    <UpdateParameters>
        <asp:ControlParameter ControlID="gvSiteUsers" Type="String" Name="username" PropertyName="SelectedValue"></asp:ControlParameter>
        <asp:Parameter Type="String" DefaultValue="" Name="newusername" />
    </UpdateParameters>         
    <DeleteParameters>
        <asp:ControlParameter ControlID="gvSiteUsers" Type="String" Name="username" PropertyName="SelectedValue"></asp:ControlParameter>
    </DeleteParameters>                       
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSiteUserPages" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_pages.description,ps_pages.path,ps_security.usertype from ps_security inner join ps_pages on ps_security.pageid = ps_pages.id and ps_security.username = @username">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvSiteUsers" Type="String" Name="username" PropertyName="SelectedDataKey.Value"/>
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSiteAdmins" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_security.id,ps_security.username from ps_security where (ps_security.usertype = 'site') and (ps_security.pageid in (select ps_pages.id from ps_pages where (ps_pages.name = 'SiteAdmin.ascx')))">
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSiteAdminsUserDetails" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_security.id, ps_security.username, ps_security.usertype from ps_security where (ps_security.id = @id)"
    UpdateCommand="update ps_security SET username=@username where ps_security.id = @id"
    InsertCommand="insert into ps_security (pageid,username,usertype) select top 1 ps_pages.id,@username,'site' from ps_pages where (ps_pages.name = 'SiteAdmin.ascx')"
    DeleteCommand="delete from ps_security where id = @id">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvSiteAdmins" Type="String" Name="id" PropertyName="SelectedValue"/>
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsPageAdmins" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select id,description from ps_pages order by description">
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsPageAdminsDetail" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_security.id,ps_security.username from ps_security where ps_security.pageid = @pageid and ps_security.usertype = 'admin' order by username asc">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvPageAdmins" Type="String" Name="pageid" PropertyName="SelectedValue"/>
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsPageAdminUsersDetails" runat="server" ConnectionString="<%$ ConnectionStrings:documentsConnectionString %>"
    SelectCommand="select ps_security.id, ps_security.username from ps_security where (ps_security.id = @id)"
    InsertCommand="insert into ps_security (pageid,username,usertype) VALUES (@pageid,@username,@usertype)"
    UpdateCommand="update ps_security SET username=@username where id = @id"
    DeleteCommand="delete from ps_security where id = @id">
    <SelectParameters>
        <asp:ControlParameter ControlID="gvPageAdminDetails" Type="Int32" Name="id" PropertyName="SelectedValue"/>
    </SelectParameters>
    <InsertParameters>
        <asp:ControlParameter ControlID="gvPageAdmins" Type="Int32" Name="pageid" PropertyName="SelectedDataKey.Value"/>
        <asp:Parameter Name="usertype" Type="String" DefaultValue="admin"/>
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="userid" Type="String" />
    </UpdateParameters>
    <DeleteParameters>
        <asp:ControlParameter Type="Int32" Name="id" PropertyName="SelectedValue" ControlID="gvPageAdminDetails"></asp:ControlParameter>
    </DeleteParameters>
</asp:SqlDataSource>