﻿Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement

Partial Class controls_AccountInfo
    Inherits System.Web.UI.Page

    Dim ps As New portalSecurity
    Dim ad As New portalAD

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Dim ctx As New PrincipalContext(ContextType.Domain)
            Dim user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, ps.getUsername)
            If user isnot Nothing then
                If user.SamAccountName isnot Nothing then lblAccount.Text = user.SamAccountName
                If user.GivenName isnot Nothing Then lblFirstName.Text = user.GivenName
                If user.Surname isnot Nothing Then lblLastName.Text = user.Surname
                If user.DisplayName isnot Nothing Then lblDisplayName.Text = user.DisplayName
                If user.Description isnot Nothing Then lblDescription.Text = user.Description
                If user.EmailAddress isnot Nothing Then lblEmail.Text = user.EmailAddress


                Dim de As DirectoryEntry = user.GetUnderlyingObject()
                If de isnot Nothing then
                    Dim manager As UserPrincipal = ad.GetManager(ctx,user)
                    If manager isnot Nothing then
                        lblManager.Text = manager.SamAccountName 
                    Else
                        lblManager.Text = ""
                    End If

                    lblOffice.Text = ad.getPropertyValue(de, "physicalDeliveryOfficeName")            
                    lblTitle.Text = ad.getPropertyValue(de, "title")
                    lblDepartment.Text = ad.getPropertyValue(de, "department")
                    lblCompany.Text = ad.getPropertyValue(de, "company")
                End If 'if de isnot nothing
            End If 'if user isnot nothing
        Catch ex As Exception
        End Try
    End Sub
End Class
