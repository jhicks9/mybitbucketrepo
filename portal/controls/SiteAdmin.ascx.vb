﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class controls_SiteAdmin
    Inherits System.Web.UI.UserControl

    Dim ps As New portalSecurity
    Public Function getUserControlName() As String
        Try  'Must reside in the control.  User control name is not available via classes.
            Dim ucname As String = ""
            ucname = [GetType].ToString().Replace("ASP.", "").Replace("_ascx", ".ascx")
            Return ucname.Split("_")(1).ToString
        Catch
            Return ""
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isSiteAdmin Then
            pnlSiteAdminAuthorized.Visible = True
        Else
            pnlSiteAdminAuthorized.Visible = False
            If Not ps.isAuthorized(getUserControlName) Then
                pnlSiteAdminAuthorized.Visible = False
                pnlSiteAdminNotAuthorized.Visible = True
            End If
        End If
    End Sub

    Protected Sub lbSecurity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSecurity.Click
        If pnlSiteAdminUsers.Visible Then
            pnlSiteAdminUsers.Visible = False
        Else
            pnlSiteAdminUsers.Visible = True
        End If
    End Sub

'============================== Site Admins Maintenance =================================================================================

    Protected Sub gvSiteAdmins_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSiteAdmins.RowCommand
        Select Case e.CommandName
            Case "Select"
                SiteAdminUserDetails.HeaderText = "Edit user"
                SiteAdminUserDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#siteadminsDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "siteadminsDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                gvSiteAdmins.SelectedIndex = -1
                SiteAdminUserDetails.HeaderText = "Add new user"
                SiteAdminUserDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#siteadminsDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "siteadminsDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub SiteAdminUserDetails_DataBound(sender As Object, e As EventArgs) Handles SiteAdminUserDetails.DataBound
        Try
            Dim insertButton As LinkButton = CType(SiteAdminUserDetails.FindControl("lbItemInsert"), LinkButton)
            Dim updateButton As LinkButton = CType(SiteAdminUserDetails.FindControl("lbEditUpdate"), LinkButton)
            Dim deleteButton As LinkButton = CType(SiteAdminUserDetails.FindControl("lbEditDelete"), LinkButton)

            If (SiteAdminUserDetails.CurrentMode = DetailsViewMode.Edit) Then
                insertButton.Visible = false 'no need to display insert button on an edit
            End If
            If (SiteAdminUserDetails.CurrentMode = DetailsViewMode.Insert) Then
                updateButton.Visible = False 'no need to display update button on an insert
                deleteButton.Visible = false 'no need to display delete button on an insert
            End If
        Catch
        End Try
    End Sub

    Protected Sub SiteAdminUserDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles SiteAdminUserDetails.ItemInserted
        gvSiteAdmins.DataBind()
        gvSiteAdmins.PageIndex = gvSiteAdmins.PageCount - 1 'Go to the last page to show inserted record
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#siteadminsDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "siteadminsDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub SiteAdminUserDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles SiteAdminUserDetails.ItemUpdated
        gvSiteAdmins.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#siteadminsDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "siteadminsDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub SiteAdminUserDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles SiteAdminUserDetails.ItemDeleted
        gvSiteAdmins.SelectedIndex = -1
        gvSiteAdmins.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#siteadminsDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "siteadminsDetailModalScript", sb.ToString(), False)
    End Sub

'============================== End Site Admins Maintenance ============================================================================
'============================== Site Users Maintenance =================================================================================

    Protected Sub gvSiteUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSiteUsers.RowCommand
        Select Case e.CommandName
            Case "Select"
                SiteUsersDetails.HeaderText = "Edit user"
                SiteUsersDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#siteusersDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "siteusersDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub SiteUsersDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles SiteUsersDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
            Case "Update"
            Case "Insert"
        End Select
        gvSiteUsers.SelectedIndex = -1
    End Sub

    Protected Sub SiteUsersDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles SiteUsersDetails.ItemDeleted
        gvSiteUsers.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#siteusersDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SiteUsersDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub SiteUsersDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles SiteUsersDetails.ItemInserted
        gvSiteUsers.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#siteusersDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SiteUsersDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub SiteUsersDetails_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles SiteUsersDetails.ItemUpdating
        dsSiteUsersDetails.UpdateParameters(1).DefaultValue = e.NewValues.Item(0).ToString
    End Sub

    Protected Sub SiteUsersDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles SiteUsersDetails.ItemUpdated
        gvSiteUsers.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#siteusersDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SiteUsersDetailModalScript", sb.ToString(), False)
    End Sub

    '============================== End Site Users Maintenance =============================================================================
    '============================== Page Admin Maintenance =================================================================================

    Protected Sub gvPageAdmins_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPageAdmins.RowCommand
        Select Case e.CommandName
            Case "Select"
                lblPageAdminDetailsDesc.Text = e.CommandArgument
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#pageadminsDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "pageadminsDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub gvPageAdminDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPageAdminDetails.RowCommand
        Select Case e.CommandName
            Case "Select"
                PageAdminUsersDetails.HeaderText = "Edit administrator"
                PageAdminUsersDetails.ChangeMode(DetailsViewMode.Edit)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#pageadminsUserDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "pageadminUserDetailModalScript", sb.ToString(), False)
            Case "AddRecord"
                PageAdminUsersDetails.HeaderText = "Add new administrator"
                PageAdminUsersDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#pageadminsUserDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "pageadminUserDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub PageAdminUsersDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles PageAdminUsersDetails.ItemDeleted
        gvPageAdminDetails.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#pageadminsUserDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "pageadminUserDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub PageAdminUsersDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles PageAdminUsersDetails.ItemInserted
        gvPageAdminDetails.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#pageadminsUserDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "pageadminUserDetailModalScript", sb.ToString(), False)
    End Sub

    Protected Sub PageAdminUsersDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles PageAdminUsersDetails.ItemUpdated
        gvPageAdminDetails.DataBind()
        Dim sb As New StringBuilder()
        sb.Append("<script type='text/javascript'>")
        sb.Append("$('#pageadminsUserDetail').modal('hide');")
        sb.Append("</script>")
        ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "pageadminUserDetailModalScript", sb.ToString(), False)
    End Sub

    '============================== End Page Admin Maintenance =============================================================================

End Class
