﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class controls_SitePages
    Inherits System.Web.UI.UserControl

    Dim ps As New portalSecurity
    Public Function getUserControlName() As String
        Try  'Must reside in the control.  User control name is not available via classes.
            Dim ucname As String = ""
            ucname = [GetType].ToString().Replace("ASP.", "").Replace("_ascx", ".ascx")
            Return ucname.Split("_")(1).ToString
        Catch
            Return ""
        End Try
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ps.isAdmin(getUserControlName) Then
            pnlSitePagesAuthorized.Visible = True
            pnlSitePagesNotAuthorized.Visible = False
            pnlSitePagesAdministration.Visible = True
        Else
            pnlSitePagesAdministration.Visible = False
            If Not ps.isAuthorized(getUserControlName) Then
                pnlSitePagesAuthorized.Visible = False
                pnlSitePagesNotAuthorized.Visible = True
            End If
        End If
    End Sub

    Protected Sub gvSitePages_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSitePages.RowCommand
        Select Case e.CommandName
            Case "Select"
                If e.CommandArgument = "EditRecord" Then  'Edit the username
                    SitePagesDetails.HeaderText = "Edit page/module"
                    SitePagesDetails.ChangeMode(DetailsViewMode.Edit)
                    Dim sb As New StringBuilder()
                    sb.Append("<script type='text/javascript'>")
                    sb.Append("$('#SitePagesDetail').modal('show');")
                    sb.Append("</script>")
                    ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SitePagesDetailModalScript", sb.ToString(), False)
                Else
                End If
            Case "AddRecord"
                gvSitePages.SelectedIndex = -1
                SitePagesDetails.HeaderText = "Add new page/module"
                SitePagesDetails.ChangeMode(DetailsViewMode.Insert)
                Dim sb As New StringBuilder()
                sb.Append("<script type='text/javascript'>")
                sb.Append("$('#SitePagesDetail').modal('show');")
                sb.Append("</script>")
                ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SitePagesDetailModalScript", sb.ToString(), False)
            Case Else
        End Select
    End Sub

    Protected Sub SitePagesDetails_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewCommandEventArgs) Handles SitePagesDetails.ItemCommand
        Select Case e.CommandName
            Case "Delete"
            Case "Cancel"
                gvSitePages.SelectedIndex = -1
                lblSitePagesStatus.Text = ""
            Case "Update"
            Case "Insert"
        End Select
    End Sub

    Protected Sub SitePagesDetails_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles SitePagesDetails.ItemDeleted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                lblSitePagesStatus.Text = e.Exception.Message.ToString '#547 foreign key constraint
                e.ExceptionHandled = True
            End If
        Else
            gvSitePages.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#SitePagesDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SitePagesDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub SitePagesDetails_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeleteEventArgs) Handles SitePagesDetails.ItemDeleting
        If SecurityExists(e.Keys.Item(0).ToString) Then
            sender.FindControl("lblSitePagesValidation").Visible = True
            sender.FindControl("lblSitePagesValidation").text = "Security is defined for this page"
            e.Cancel = True
        Else
            sender.FindControl("lblSitePagesValidation").text = ""
            sender.FindControl("lblSitePagesValidation").Visible = False
            e.Cancel = False
            gvSitePages.SelectedIndex = -1
        End If
    End Sub

    Protected Sub SitePagesDetails_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertedEventArgs) Handles SitePagesDetails.ItemInserted
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(SitePagesDetails.FindControl("lblSitePagesError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists.<br /><br />"
                Else
                    lbl.Text = e.Exception.Message.ToString & "<br /><br />"
                End If
                e.ExceptionHandled = True
                e.KeepInInsertMode = True
            End If
        Else
            gvSitePages.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#SitePagesDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SitePagesDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Protected Sub SitePagesDetails_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles SitePagesDetails.ItemInserting
        If PageExists(e.Values.Item(0).ToString) Then
            sender.FindControl("lblSitePagesValidation").text = "Name Already Exists"
            sender.FindControl("lblSitePagesValidation").Visible = True
            e.Cancel = True
        Else
            sender.FindControl("lblSitePagesValidation").text = ""
            sender.FindControl("lblSitePagesValidation").Visible = False
            e.Cancel = False
            gvSitePages.SelectedIndex = -1
        End If
    End Sub

    Protected Sub SitePagesDetails_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles SitePagesDetails.ItemUpdating
        If e.OldValues.Item(0).ToString <> e.NewValues.Item(0).ToString Then  'only validate if an updated page/module name is entered
            If PageExists(e.NewValues.Item(0).ToString) Then
                sender.FindControl("lblSitePagesValidation").text = "Name Already Exists"
                sender.FindControl("lblSitePagesValidation").Visible = True
                e.Cancel = True
            Else
                sender.FindControl("lblSitePagesValidation").text = ""
                sender.FindControl("lblSitePagesValidation").Visible = False
                e.Cancel = False
                gvSitePages.SelectedIndex = -1
            End If
        Else
            gvSitePages.SelectedIndex = -1
        End If
    End Sub

    Protected Sub SitePagesDetails_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs) Handles SitePagesDetails.ItemUpdated
        If e.Exception IsNot Nothing Then
            Dim Exp As SqlException = TryCast(e.Exception, SqlException)
            If Exp IsNot Nothing Then
                Dim lbl As Label = CType(SitePagesDetails.FindControl("lblSitePagesError"), Label)
                If (Exp.Number = 2601) Or (Exp.Number = 2627) Then ' duplicate key 
                    lbl.Text = "Record already exists."
                Else
                    lbl.Text = e.Exception.Message.ToString
                End If
                e.ExceptionHandled = True
                e.KeepInEditMode = True
            End If
        Else
            gvSitePages.DataBind()
            Dim sb As New StringBuilder()
            sb.Append("<script type='text/javascript'>")
            sb.Append("$('#SitePagesDetail').modal('hide');")
            sb.Append("</script>")
            ScriptManager.RegisterClientScriptBlock(sender, sender.[GetType](), "SitePagesDetailModalScript", sb.ToString(), False)
        End If
    End Sub

    Public Function PageExists(ByVal name As String) As Boolean
        Try
            Dim exists As Boolean = False
            Dim sql As String = "SELECT count(*) from ps_pages where (name = '" & name & "')"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd0 As New SqlCommand(sql, conn)
            Dim dr0 As SqlDataReader
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    exists = dr0.GetValue(0)
                End While
            End If
            dr0.Close()
            conn.Close()
            Return exists
        Catch
            Return True  'default to true prohibits page from being added
        End Try
    End Function

    Public Function SecurityExists(ByVal id As String) As Boolean
        Try
            Dim exists As Boolean = False
            Dim sql As String = "SELECT count(*) from ps_security where (pageid = " & id & ")"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
            conn.Open()
            Dim cmd0 As New SqlCommand(sql, conn)
            Dim dr0 As SqlDataReader
            dr0 = cmd0.ExecuteReader
            If dr0.HasRows Then
                While dr0.Read
                    exists = dr0.GetValue(0)
                End While
            End If
            dr0.Close()
            conn.Close()
            Return exists
        Catch
            Return True  'default to true prohibits page from being deleted
        End Try
    End Function
End Class
