﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="unAuthorized.ascx.vb" Inherits="unAuthorized" %>

<div class="row">
    <div class="col-lg-12 ">
        <p class="well-sm bg-warning">
            <span><strong><i style="font-size:60px" class="fa fa-lock"></i></strong></span>
            <br />
            You are not authorized to use this area.
        </p>
    </div>
</div>