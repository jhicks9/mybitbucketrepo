Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OracleClient
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Imports System.IO
Imports System.Net
Imports Gif.Components

<WebService(Namespace:="http://empiredistrict.com/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod(Description:="Validate customer account number including SSN")> _
    Public Function ValidateCustomer(ByVal custToken As String, ByVal billpToken As String, ByVal custName As String, ByVal custSSN As String) As Integer
        Dim result As Integer = 0  '0=Failed,1=Pass,2=SSN Failed
        Dim custTitle As String = ""
        Dim custFirst As String = ""
        Dim custLast As String = ""
        Dim custSuffix As String = ""
        Dim custRSSN As String = ""
        Dim custFTax As String = ""
        Dim custSTax As String = ""
        Dim buildname As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select count(*) from billing_package inner join customer on billing_package.customer_tkn = customer.customer_tkn " & _
            "where (billing_package.customer_tkn = " & cw_tkn & ") " & _
            "and (billing_package.billing_pkg_tkn = " & billpToken & ") " & _
            "and (billing_package.end_date is null or billing_package.end_date > sysdate) " '& _

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                result = dr.GetValue(0)
            End While
            dr.Close()

            If result Then  'account number is good so verify name
                If custName <> "N/A" Then  'do not verify Name if "N/A" is passed
                    query = "Select upper(Title_Name),upper(First_Name),upper(Last_Name),upper(Suffix_Name),social_sec_code,fed_tax_id_num,state_tax_id_num From customer Where Customer_Tkn = " & cw_tkn
                    Dim cmd1 As New OracleCommand(query, conn)
                    dr = cmd1.ExecuteReader
                    While dr.Read
                        custTitle = dr.GetValue(0).ToString
                        custFirst = dr.GetValue(1).ToString
                        custLast = dr.GetValue(2).ToString
                        custSuffix = dr.GetValue(3).ToString
                        custRSSN = dr.GetValue(4).ToString
                        custFTax = dr.GetValue(5).ToString
                        custSTax = dr.GetValue(6).ToString
                    End While
                    dr.Close()
                    conn.Close()

                    If custTitle <> "" Then   'Build the name exactly as on statement
                        buildname &= custTitle & " "
                    End If
                    If custFirst <> "" Then
                        buildname &= custFirst & " "
                    End If
                    If custLast <> "" Then
                        buildname &= custLast
                    End If
                    If custSuffix <> "" Then
                        buildname &= " " & custSuffix
                    End If

                    If custLast <> "" Then  'Compare name entered against assembled name from database
                        If Trim(custName.ToUpper) = buildname Then
                            result = 1
                        Else
                            result = 0
                        End If
                    Else
                        If Trim(custName.ToUpper) = custFirst Then  'only compare first name for company names
                            result = 1
                        Else
                            result = 0
                        End If
                    End If
                End If  ' End N/A
            End If  ' End Verify Name

            If result Then 'account number AND name passed so verify SSN or Tax ID Number
                If custSSN <> "N/A" Then  'do not verify SSN if "N/A" is passed
                    If (custSSN = Right(custRSSN, 4)) Or (custSSN = Right(custFTax, 4)) Or (custSSN = Right(custSTax, 4)) Then
                        result = 1
                    Else
                        result = 2
                    End If
                End If  ' End N/A
            End If  ' End Verify SSN

        Catch
            result = 0
        End Try

        Return result
    End Function

    <WebMethod(Description:="Show payment information")> _
    Public Function GetPaymentInfo(ByVal custToken As String, ByVal billpToken As String, ByVal history As String) As DataSet
        Dim dsPaymentInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select to_char(a.payment_amt + a.donation_amt,'$99,999,999.99PR') as paymentamount, to_char(a.payment_date,'mm-dd-yyyy') as paymentdate" & _
                                  " from payment a" & _
                                  " where customer_tkn = '" & cw_tkn & "'" & _
                                  " and billing_pkg_tkn = '" & billpToken & "'" & _
                                  " and a.payment_date > (sysdate - " & history & ")" & _
                                  " and a.return_reason_desc is null" & _
                                  " order by payment_date desc "

            Dim daPaymentInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daPaymentInfo.Fill(dsPaymentInfo, "PaymentInfo")
            conn.Close()

        Catch
            Return Nothing
        End Try
        Return dsPaymentInfo
    End Function

    <WebMethod(Description:="Show current balance due")> _
    Public Function GetCurrentDue(ByVal custToken As String, ByVal billpToken As String) As String
        Dim balancedue As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select to_char(sum(a.charge_amt),'$99,999,999.99PR') from account_charge a,account_package b" & _
                                  " where a.customer_tkn = '" & cw_tkn & "'" & _
                                  " and b.billing_pkg_tkn = '" & billpToken & "'" & _
                                  " and a.customer_tkn = b.customer_tkn" & _
                                  " and a.customer_acct_tkn = b.customer_acct_tkn" & _
                                  " and a.account_pkg_tkn = b.account_pkg_tkn" & _
                                  " and a.cust_statement_tkn is not null" & _
                                  " and a.canceled_date is null"
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                balancedue = dr(0).ToString
            End While
            dr.Close()
            conn.Close()

            ' sample code to alter datasets (leaving comments for future reference)
            'Dim dr As DataRow
            'Dim increment As Integer = 0
            'For Each dr In dsPaymentInfo.Tables(0).Rows
            '  If Left(dsPaymentInfo.Tables("PaymentInfo").Rows(increment).Item("type_desc"), 9) = "Collector" Then
            '    dsPaymentInfo.Tables("PaymentInfo").Rows(increment).Item("type_desc") = "Counter"
            '  End If
            '  increment = increment + 1
            'Next
            ' end sample code
        Catch
            Return Nothing
        End Try
        Return balancedue
    End Function

    <WebMethod(Description:="Retrieve list of statements")> _
    Public Function GetBillInfo(ByVal custToken As String, ByVal billToken As String, ByVal history As String) As DataSet
        Dim dsBillInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select cust_statement_tkn,to_char(current_Stmt_Date,'mm-dd-yyyy') as stmt_date" & _
            " from customer_statement" & _
            " where customer_tkn = " & cw_tkn & _
            " and billing_pkg_tkn = " & billToken & _
            " and current_stmt_date > sysdate - " & history & " " & _
            " order by current_stmt_date desc"
            Dim daBillInfo As New OracleDataAdapter(query, conn)

            conn.Open()
            daBillInfo.Fill(dsBillInfo, "BillInfo")
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return dsBillInfo
    End Function

    <WebMethod(Description:="Retrieve billing packages")> _
    Public Function GetBillPackage(ByVal custToken As String) As String
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim xbillpkg As String = Nothing

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select billing_pkg_tkn from billing_package where customer_tkn = " & cw_tkn
            query &= " and (end_date is null or end_date > (sysdate - 90))"
            'disregard open only status
            'query &= " and (customer_tkn, billing_pkg_tkn) in "
            'query &= " (Select customer_tkn, billing_pkg_tkn "
            'query &= " from account_package where status_desc in ('Active','Landlord - Active','Transferring','Disconnect Non-Pay')) "
            'query &= " order by billing_pkg_tkn"
            Dim cmd0 As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd0.ExecuteReader
            While dr.Read
                If xbillpkg Is Nothing Then
                    xbillpkg = dr("billing_pkg_tkn")
                Else
                    xbillpkg = xbillpkg & "," & dr("billing_pkg_tkn")
                End If
            End While
            dr.Close()

            conn.Close()
        Catch
            Return Nothing
        End Try
        Return xbillpkg
    End Function
	
    <WebMethod(Description:="Retrieve LVG billing packages")> _
    Public Function GetLVGBillPackage(ByVal custToken As String) As String
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim xbillpkg As String = Nothing

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select billing_pkg_tkn from billing_package where customer_tkn = " & cw_tkn
            query &= " and stmt_cycle_num = 71 and (end_date is null or end_date > (sysdate - 90))"

            Dim cmd0 As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd0.ExecuteReader
            While dr.Read
                If xbillpkg Is Nothing Then
                    xbillpkg = dr("billing_pkg_tkn")
                Else
                    xbillpkg = xbillpkg & "," & dr("billing_pkg_tkn")
                End If
            End While
            dr.Close()

            conn.Close()
        Catch
            Return Nothing
        End Try
        Return xbillpkg
    End Function

    <WebMethod(Description:="Retrieve statement")> _
    Public Function GetStatement(ByVal stmtToken As String, ByVal templateToken As String) As String
        Dim xmlstmt As String = ""
        Dim xsltemp As String = ""
        Dim tempToken As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(stmtToken)
            Dim query As String = "select DOCUMENT_DATA,system_template_tkn from system_document where entity_tokens = 'Cust_Statement_Tkn=" & cw_tkn & "'"
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim cmd0 As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd0.ExecuteReader
            While dr.Read
                xmlstmt = dr.GetString(0)
                tempToken = dr.GetValue(1)
            End While
            dr.Close()
            'if the templateToken has a value then use it, else use the database tempToken
            If templateToken <> "" Then
                tempToken = templateToken
            End If

            query = "select TEMPLATE_DATA from system_template where system_template_tkn = '" & tempToken & "'"
            Dim cmd1 As New OracleCommand(query, conn)
            dr = cmd1.ExecuteReader
            While dr.Read
                xsltemp = dr.GetString(0)
            End While
            dr.Close()
            'End If
            conn.Close()
            Return xmlstmt & "-,-" & xsltemp
        Catch
            Return "Error:" & Err.Description
        End Try

    End Function

    <WebMethod(Description:="Retrieve usage information to build web chart")> _
    Public Function GetChartInfo(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String, ByVal history As String, ByVal spaToken As String, ByVal spToken As String, ByVal premToken As String) As String
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim xusage As String = Nothing
        Dim xusagedate As String = Nothing
        Dim xread As String = Nothing

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim query As String = "select install_date, remove_date, equipment_tkn from equipment_assign" & _
                                  " where (premise_tkn, service_point_tkn) in " & _
                                  " (select premise_tkn, service_point_tkn from service_pnt_assign" & _
                                  " where customer_tkn = " & cw_tkn & _
                                  " and customer_acct_tkn = " & acctToken & _
                                  " and account_pkg_tkn = " & acctpToken & _
                                  " and srvce_pnt_assn_tkn = " & spaToken & ")" & _
                                  " order by install_date asc"

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim cmd As New OracleCommand(query, conn)
            Dim dr, dr1 As OracleDataReader
            Dim query2 As String = ""
            Dim startdate As Date = Date.Today

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                ' ==Calculate amount of histoy
                cmd.CommandText = "select start_date from service_pnt_assign where premise_tkn = " & premToken & " and service_point_tkn = " & spToken & " and srvce_pnt_assn_tkn = " & spaToken
                dr1 = cmd.ExecuteReader
                While dr1.Read
                    If dr1.HasRows Then
                        startdate = dr1("start_date").ToString
                    End If
                End While
                dr1.Close()
                If startdate.Date <> Date.Today Then  'if a startdate is read from cw, then recalculate the amount of history to show based on that startdate
                    If (DateDiff("d", startdate, Date.Now) < Convert.ToInt32(history)) Then
                        history = DateDiff("d", startdate, Date.Now).ToString
                    End If
                End If
                ' ==End calculate history
                query2 = "select usage_adjust_num + usage_num as um,to_char(read_date,'mm-dd-yyyy'),usage_read_num,usage_days_cnt,estimate_ind" & _
                         " from meter_read" & _
                         " where equipment_tkn = " & dr(2).ToString & _
                         " and usage_num is not null" & _
                         " and read_date >= sysdate - " & history & _
                         " and read_date >= to_date('" & dr(0).ToString & "','mm/dd/yyyy hh:mi:ss am')"
                If Not IsDBNull(dr(1)) Then
                    query2 += " and read_date <= to_date('" & dr(1).ToString & "','mm/dd/yyyy hh:mi:ss am')"
                End If
                query2 += " and canceled_ind = 'N' order by read_date asc"

                cmd.CommandText = query2
                dr1 = cmd.ExecuteReader
                While dr1.Read

                    If xusage Is Nothing Then
                        xusage = dr1(0).ToString
                        xusagedate = dr1(1).ToString
                        xread = dr1(2).ToString
                    Else
                        xusage = xusage & "," & dr1(0).ToString
                        xusagedate = xusagedate & "," & dr1(1).ToString
                        xread = xread & "," & dr1(2).ToString
                    End If

                End While
                dr1.Close()

            End While
            dr.Close()
            conn.Close()

        Catch
            Return Nothing
        End Try
        Return xusage & ";" & xusagedate & ";" & xread
    End Function

    <WebMethod(Description:="Retrieve usage information")> _
    Public Function GetUsageInfo(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String, ByVal history As String, ByVal spaToken As String, ByVal spToken As String, ByVal premToken As String) As DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim actualCharge As Single = 0
        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim query As String = "select install_date, remove_date, equipment_tkn from equipment_assign" & _
                                  " where (premise_tkn, service_point_tkn) in " & _
                                  " (select premise_tkn, service_point_tkn from service_pnt_assign" & _
                                  " where customer_tkn = " & cw_tkn & _
                                  " and customer_acct_tkn = " & acctToken & _
                                  " and account_pkg_tkn = " & acctpToken & _
                                  " and srvce_pnt_assn_tkn = " & spaToken & ")" & _
                                  " order by install_date desc"

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim cmd As New OracleCommand(query, conn)
            Dim dr, dr1, dr2, dr3 As OracleDataReader
            Dim query2 As String = ""
            Dim startdate As Date = Date.Today

            Dim dsUsageInfo As New DataSet("ChartInfo")
            Dim dt As New System.Data.DataTable("Usage")
            dsUsageInfo.Tables.Add(dt)
            dt.Columns.Add("usage_num", GetType(String))
            dt.Columns.Add("read_date", GetType(String))
            dt.Columns.Add("usage_read_num", GetType(String))
            dt.Columns.Add("usage_days_cnt", GetType(String))
            dt.Columns.Add("estimate_ind", GetType(String))
            dt.Columns.Add("demand_read", GetType(String))
            dt.Columns.Add("demand_billed", GetType(String))
            dt.Columns.Add("billing_date", GetType(String))
            dt.Columns.Add("total_bill", GetType(String))
            dt.Columns.Add("usage_charge", GetType(String))
            dt.Columns.Add("actual_charge", GetType(String))

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                ' ==Calculate amount of histoy
                cmd.CommandText = "select start_date from service_pnt_assign where premise_tkn = " & premToken & " and service_point_tkn = " & spToken & " and srvce_pnt_assn_tkn = " & spaToken
                dr1 = cmd.ExecuteReader
                While dr1.Read
                    If dr1.HasRows Then
                        startdate = dr1("start_date").ToString
                    End If
                End While
                dr1.Close()
                If startdate.Date <> Date.Today Then  'if a startdate is read from cw, then recalculate the amount of history to show based on that startdate
                    If (DateDiff("d", startdate, Date.Now) < Convert.ToInt32(history)) Then
                        history = DateDiff("d", startdate, Date.Now).ToString
                    End If
                End If
                ' ==End calculate history
                query2 = "select to_char(usage_adjust_num + usage_num,'9,999,999,999'),to_char(read_date,'mm-dd-yyyy')," & _
                         " usage_read_num,usage_days_cnt,estimate_ind," & _
                         " to_char(read_demand,'9,999,999,999.99999'),to_char(calculated_demand + adjusted_demand,'9,999,999,999.99')" & _
                         " from meter_read" & _
                         " where equipment_tkn = " & dr(2).ToString & _
                         " and usage_num is not null" & _
                         " and read_date >= sysdate - " & history & _
                         " and read_date >= to_date('" & dr(0).ToString & "','mm/dd/yyyy hh:mi:ss am')"
                If Not IsDBNull(dr(1)) Then
                    query2 += " and read_date <= to_date('" & dr(1).ToString & "','mm/dd/yyyy hh:mi:ss am')"
                End If
                query2 += " and canceled_ind = 'N' order by read_date desc"

                cmd.CommandText = query2
                dr1 = cmd.ExecuteReader
                While dr1.Read
                    Dim drow As System.Data.DataRow
                    drow = dt.NewRow()

                    cmd.CommandText = "select to_char(current_stmt_date,'mm-dd-yyyy')as billing_date,total_stmt_amt,to_char(current_stmt_date,'dd-mon-yyyy')as statement_date,cust_statement_tkn from customer_statement " & _
                    " where customer_tkn = " & cw_tkn & _
                    " and current_stmt_date >= to_date('" & dr1(1).ToString & "','mm/dd/yyyy hh:mi:ss am')" & _
                    " and ROWNUM <= 1 order by current_stmt_date desc"
                    dr2 = cmd.ExecuteReader
                    While dr2.Read
                        If dr2.HasRows Then
                            drow.Item("billing_date") = dr2("billing_date").ToString
                            drow.Item("total_bill") = dr2("total_stmt_amt").ToString
                            drow.Item("usage_charge") = dr2("total_stmt_amt").ToString  'set a default usage charge as total statement amount

                            'cmd.CommandText = "select usage_chrg_amt from component_charge where type_desc = 'Usage Charge' and (customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn, billing_charge_tkn) in (select customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn, billing_charge_tkn from billing_charge where (customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn) IN (select customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn from account_charge " & _
                            '"where cust_statement_tkn = " & dr2("cust_statement_tkn").ToString & _
                            '" and stmt_date = '" & dr2("statement_date").ToString & _
                            '"' and customer_tkn = " & cw_tkn & _
                            '" and customer_acct_tkn = " & acctToken & _
                            '" and account_pkg_tkn = " & acctpToken & "))"

                            cmd.CommandText = "select (usage_chrg_amt + charge_amt)as charge, type_desc, tax_amt as taxes from component_charge where type_desc <> 'Contract' and (customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn, billing_charge_tkn) in (select customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn, billing_charge_tkn from billing_charge where (customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn) IN (select customer_tkn, customer_acct_tkn, account_pkg_tkn, account_charge_tkn from account_charge " & _
                            "where cust_statement_tkn = " & dr2("cust_statement_tkn").ToString & _
                            " and stmt_date = '" & dr2("statement_date").ToString & _
                            "' and customer_tkn = " & cw_tkn & _
                            " and customer_acct_tkn = " & acctToken & _
                            " and account_pkg_tkn = " & acctpToken & "))"

                            dr3 = cmd.ExecuteReader
                            While dr3.Read
                                If dr3("type_desc").ToString.ToLower = "usage charge" Then
                                    drow.Item("usage_charge") = dr3("charge").ToString  'override default usage charge with actual charge if possible
                                End If
                                actualCharge += (dr3("charge") + dr3("taxes"))
                            End While
                            dr3.Close()
                            drow.Item("actual_charge") = actualCharge.ToString
                            actualCharge = 0
                        End If
                    End While
                    dr2.Close()

                    drow.Item("usage_num") = dr1(0).ToString
                    drow.Item("read_date") = dr1(1).ToString
                    drow.Item("usage_read_num") = dr1(2).ToString
                    drow.Item("usage_days_cnt") = dr1(3).ToString
                    drow.Item("estimate_ind") = dr1(4).ToString
                    drow.Item("demand_read") = dr1(5).ToString
                    drow.Item("demand_billed") = dr1(6).ToString
                    dt.Rows.Add(drow)
                End While
                dr1.Close()
            End While
            dr.Close()
            conn.Close()

            Return dsUsageInfo
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve customer token(s) by SSN")> _
    Public Function GetCustomerToken(ByVal custSSN As String) As DataSet
        Dim dsCustomerTokens As New DataSet
        Dim cw_ssn As String
        Dim tdes As New tripleDES
        Try
            cw_ssn = tdes.Decrypt(custSSN)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select billing_package.customer_tkn,billing_package.billing_pkg_tkn," & _
                                       " upper(title_name),upper(customer.first_name),upper(customer.last_name),upper(customer.suffix_name)" & _
                                       " from billing_package inner join customer on billing_package.customer_tkn = customer.customer_tkn" & _
                                       " where customer.social_sec_code = '" & cw_ssn & "'" & _
                                       " and (billing_package.end_date is null or billing_package.end_date > sysdate)" & _
                                       " order by billing_package.customer_tkn,billing_package.billing_pkg_tkn"
            Dim daCustomerTokens As New OracleDataAdapter(query, conn)
            conn.Open()
            daCustomerTokens.Fill(dsCustomerTokens, "CustomerTokens")
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return dsCustomerTokens
    End Function

    <WebMethod(Description:="Retrieve account tokens for usage data")> _
    Public Function GetAccountTokens(ByVal custToken As String, ByVal billpToken As String) As DataSet
        Dim dsAccountTokens As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select c.account_pkg_desc,c.customer_tkn, c.customer_acct_tkn, c.account_pkg_tkn, a.ln1_addr, b.unit_code, a.city_name, a.state_code, a.postal_code, d.end_date, d.srvce_pnt_assn_tkn, d.service_point_tkn, d.premise_tkn, c.revenue_class_desc" & _
                                       " from premise a, service_point b, account_package c, service_pnt_assign d" & _
                                       " where c.customer_tkn = '" & cw_tkn & "'" & _
                                       " and c.billing_pkg_tkn = '" & billpToken & "'" & _
                                       " and c.customer_tkn = d.customer_tkn" & _
                                       " and c.customer_acct_tkn = d.customer_acct_tkn" & _
                                       " and c.account_pkg_tkn = d.account_pkg_tkn" & _
                                       " and d.premise_tkn = a.premise_tkn" & _
                                       " and d.premise_tkn = b.premise_tkn" & _
                                       " and d.service_point_tkn = b.service_point_tkn"
            'disregard account package type
            'query &= " and c.account_pkg_desc in('Gas','Electric','Water')"
            query &= " and d.end_date is null order by d.end_date"

            Dim daAccountTokens As New OracleDataAdapter(query, conn)
            conn.Open()
            daAccountTokens.Fill(dsAccountTokens, "AccountTokens")
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return dsAccountTokens
    End Function

    <WebMethod(Description:="Retrieve the account delinquent date")> _
    Public Function GetDelinquentDate(ByVal custToken As String, ByVal billpToken As String) As String
        Dim ddate As String = Nothing
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT to_char(MAX(Delinquent_Date),'mm-dd-yyyy') FROM Customer_Statement " & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Billing_Pkg_Tkn = " & billpToken
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                ddate = dr(0).ToString
            End While
            dr.Close()

            conn.Close()
        Catch
            Return Nothing
        End Try
        Return ddate
    End Function

    <WebMethod(Description:="Retrieve co-customer information")> _
    Public Function GetCoCustomer(ByVal custToken As String, ByVal billpToken As String) As String
        Dim cocust As String = "None on file"
        Dim buildstr As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT Customer_Name FROM Customer" & _
                                  " WHERE Customer_Tkn IN " & _
                                  " (SELECT Co_Customer_Tkn From Co_Customer_Assign " & _
                                    " WHERE Customer_Tkn = " & cw_tkn & _
                                    " AND Billing_Pkg_Tkn = " & billpToken & _
                                    " AND (End_Date IS NULL OR End_Date > SYSDATE)) "
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()

            dr = cmd.ExecuteReader
            While dr.Read
                buildstr = dr(0).ToString
                If Len(buildstr) > 0 And InStr(buildstr, ",") > 0 Then
                    If Left(cocust, 4) = "None" Then
                        cocust = buildstr.Split(",")(1).Trim & " " & buildstr.Split(",")(0).Trim
                    Else
                        cocust = cocust & ", " & buildstr.Split(",")(1).Trim & " " & buildstr.Split(",")(0).Trim
                    End If
                End If
            End While
            dr.Close()

            conn.Close()
            Return cocust
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve payment date")> _
    Public Function GetPaymentDate(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim paymentinfo As String = Nothing
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT to_char(Charge_Date, 'mm-dd-yyyy'), " & _
                                  " to_char(SUM(Adjustment_Amt * -1),'$99,999,999.99PR') " & _
                                  " FROM Billing_Charge " & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken & _
                                  " AND Type_Desc = 'Payment' " & _
                                  " AND Canceled_Date IS NULL " & _
                                  " GROUP BY Charge_Date " & _
                                  " ORDER BY Charge_Date DESC"
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                paymentinfo = dr(0).ToString
                paymentinfo = paymentinfo & ";" & dr(1).ToString
            End If
            dr.Close()

            conn.Close()

            Return paymentinfo
        Catch
            Return Nothing
        End Try
        Return paymentinfo
    End Function

    <WebMethod(Description:="Retrieve autopay status")> _
    Public Function GetOnAutopay(ByVal custToken As String) As String
        ' replaced with function GetAutoPayStatus 11-06-2014
        ' can remove this function after the next successful WEB build (performing back-to-back builds is not possible)
        ' code should remain short term to not break current calls for status (commission and myaccount)
        Dim onautopay As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select count(*) from payment_method " & _
                                  " where customer_tkn = " & cw_tkn & _
                                  "and (end_date Is null Or end_date > sysdate) "

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                If dr(0) > 0 Then
                    onautopay = "Yes"
                Else
                    onautopay = "No"
                End If
            End While
            dr.Close()
            conn.Close()
            Return onautopay
        Catch
            Return ""
        End Try
        Return onautopay
    End Function

    <WebMethod(Description:="Retrieve autopay status")> _
    Public Function GetAutoPayStatus(ByVal custToken As String, ByVal billpToken As String) As String
        Dim onautopay As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select count(1) from billing_package bp inner join payment_method pm on pm.customer_tkn = bp.customer_tkn" & _
                " and pm.payment_method_tkn = bp.payment_method_tkn" & _
                " where bp.customer_tkn = " & cw_tkn & _
                " and bp.billing_pkg_tkn = " & billpToken & _
                " and (pm.end_date is NULL or pm.end_date > sysdate)"

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                If dr(0) > 0 Then
                    onautopay = "Yes"
                Else
                    onautopay = "No"
                End If
            End While
            dr.Close()
            conn.Close()
            Return onautopay
        Catch
            Return ""
        End Try
        Return onautopay
    End Function

    <WebMethod(Description:="Retrieve average payment plan status")> _
    Public Function GetOnAveragePayment(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim onaveragepay As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT count(*) FROM account_contract" & _
                                  " WHERE customer_tkn = " & cw_tkn & _
                                  " AND customer_acct_tkn = " & acctToken & _
                                  " AND account_pkg_tkn = " & acctpToken & _
                                  " AND type_desc = 'APP'" & _
                                  " AND (end_date IS NULL OR end_date > sysdate)"

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                If dr(0) > 0 Then
                    onaveragepay = "Yes"
                Else
                    onaveragepay = "No"
                End If
            End While
            dr.Close()
            conn.Close()
            Return onaveragepay
        Catch
            Return ""
        End Try
        Return onaveragepay
    End Function

    <WebMethod(Description:="Retrieve current disconnect for nonpay order date")> _
    Public Function GetDNPDate(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim dnpdate As String = Nothing
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT to_char(Requested_Date, 'mm-dd-yyyy') FROM Service_Order" & _
                                  " WHERE Service_Order_Tkn IN " & _
                                  " (SELECT Service_Order_Tkn FROM Account_Order " & _
                                    " WHERE Customer_Tkn = " & cw_tkn & _
                                    " AND Customer_Acct_Tkn = " & acctToken & _
                                    " AND Account_Pkg_Tkn = " & acctpToken & ") " & _
                                  " AND Type_Desc = 'Disconnect - Non Pay' " & _
                                  " AND Status_Desc = 'In-Progress' "
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                dnpdate = dr(0)
            End If
            dr.Close()

            conn.Close()
            Return dnpdate
        Catch
            Return Nothing
        End Try
        Return dnpdate
    End Function

    <WebMethod(Description:="Retrieve deposit information")> _
    Public Function GetDepositInfo(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As DataSet
        Dim dsDepositInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT to_char(Guaranty_Amt,'$99,999,999.99PR') as ga, to_char(Guaranty_Pd_Date, 'mm-dd-yyyy') as gpd," & _
                                  " Paid_In_Full_Ind, to_char(Paid_In_Full_Date, 'mm-dd-yyyy') as pfd, to_char(Refund_Amt,'$99,999,999.99PR')as ra, to_char(Refund_Date, 'mm-dd-yyyy') as rd, Type_Desc " & _
                                  " FROM Account_Guaranty " & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken & _
                                  " AND Type_Desc in ('Cash Deposit','Guarantor') " & _
                                  " AND (refund_date >= '01jan2000' or refund_date is null) "

            Dim daPaymentInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daPaymentInfo.Fill(dsDepositInfo, "PaymentInfo")
            conn.Close()
            Return dsDepositInfo
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve unpaid deposit information")> _
    Public Function GetUnpaidDepositInfo(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim unpaiddeposit As String = "0.0"
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " select sum(principal_amt) from account_contract" & _
                                  " WHERE customer_tkn = " & cw_tkn & _
                                  " AND customer_acct_tkn = " & acctToken & _
                                  " AND account_pkg_tkn = " & acctpToken & _
                                  " AND type_desc = 'Deposit'" & _
                                  " AND status_desc = 'Active'"
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                unpaiddeposit = dr(0)
            End If
            dr.Close()

            conn.Close()
            Return unpaiddeposit
        Catch
            Return unpaiddeposit
        End Try
    End Function

    <WebMethod(Description:="Retrieve deferred payment contract information")> _
    Public Function GetPaymentContractInfo(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As DataSet
        Dim dsContractInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select to_char(principal_amt,'$99,999,999.99PR') as pa, to_char(installment_amt,'$99,999,999.99PR') as ia, installment_qty, contract_date, type_desc " & _
                                   "from account_contract " & _
                                   "where customer_tkn = " & cw_tkn & _
                                   " and customer_acct_tkn = " & acctToken & _
                                   " and account_pkg_tkn = " & acctpToken & _
                                   " and principal_amt <> 0" & _
                                   " and start_date < sysdate " & _
                                   " and (end_date is null or end_date > sysdate) " & _
                                   " and type_desc in ('Payment Agreement', 'Cold Weather Agreement') "

            Dim daDeferredPaymentInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daDeferredPaymentInfo.Fill(dsContractInfo, "DeferredPaymentInfo")
            conn.Close()
            Return dsContractInfo
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve AP data")> _
    Public Function GetAPData(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim apdata As String = Nothing
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT to_char(Start_Date, 'mm-dd-yyyy'), to_char(cut_order_date, 'mm-dd-yyyy'), to_char(cut_followup_date, 'mm-dd-yyyy'), disc_bal_due_amt, status_desc " & _
                                  " FROM Account_Package" & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                apdata += dr(0).ToString & ";"
                apdata += dr(1).ToString & ";"
                apdata += dr(2).ToString & ";"
                apdata += dr(3).ToString & ";"
                apdata += dr(4).ToString
            End If
            dr.Close()

            conn.Close()
            Return apdata
        Catch
            Return ";;;;"
        End Try
    End Function

    Public Function GetArrearsDLQDate(ByVal custStmtTkn As String) As Date
        Try
            Dim delinqDate As Date
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT Delinquent_Date " & _
                                  " FROM Customer_Statement " & _
                                  " WHERE Cust_Statement_Tkn = " & custStmtTkn
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                delinqDate = dr(0)
            End If
            dr.Close()

            conn.Close()
            Return delinqDate
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve arrears breakdown")> _
    Public Function GetArrearsBreakdown(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String, ByVal billpToken As String) As String
        Dim arrearsdata As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim total As Double = 0.0
        Dim unstatemented As Double = 0.0
        Dim current As Double = 0.0
        Dim pastDue As Double = 0.0
        Dim days30 As Double = 0.0
        Dim days60 As Double = 0.0
        Dim days90 As Double = 0.0
        Dim days120 As Double = 0.0
        Dim writeOffBankruptcy As Double = 0.0
        Dim finalBillStatus As String = ""

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT Fnl_Bill_Status_Desc " & _
                                  " FROM Account_Package" & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken
            Dim query2 As String = " SELECT Cust_Statement_Tkn, Charge_Amt, Stmt_Date, Type_Desc, Sub_Type_Desc, Arrears_Date, Account_Charge_Tkn " & _
                                  " FROM Account_Charge " & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken & _
                                  " AND Charge_Amt <> 0 " & _
                                  " ORDER BY Stmt_Date ASC "

            Dim cmd As New OracleCommand(query, conn)
            Dim cmd2 As New OracleCommand(query2, conn)
            Dim dr As OracleDataReader
            Dim dr2 As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            dr2 = cmd2.ExecuteReader
            If dr.Read Then
                If Not IsDBNull(dr(0)) Then
                    finalBillStatus = dr(0)
                End If
            End If

            While dr2.Read
                Dim amount As Double = dr2(1)
                If finalBillStatus.Equals("Write-Off") Or finalBillStatus.Equals("Bankruptcy") Then
                    writeOffBankruptcy += amount
                ElseIf IsDBNull(dr2(0)) Then
                    unstatemented += amount
                Else
                    Dim delinqDate As Date = GetArrearsDLQDate(dr2(0))
                    If delinqDate > Now Then
                        current += amount
                    Else
                        Dim arrearsDate As Date = dr2(5)
                        Dim span As TimeSpan = Now.Subtract(arrearsDate)
                        If span.Days < 30 Then
                            pastDue += amount
                        ElseIf span.Days < 60 Then
                            days30 += amount
                        ElseIf span.Days < 90 Then
                            days60 += amount
                        ElseIf span.Days < 120 Then
                            days90 += amount
                        Else
                            days120 += amount
                        End If
                    End If
                End If
                total += amount
            End While

            dr.Close()
            dr2.Close()

            conn.Close()
            arrearsdata = Format(total, "c").ToString & ";"
            arrearsdata += Format(unstatemented, "c").ToString & ";"
            arrearsdata += Format(current, "c").ToString & ";"
            arrearsdata += Format(pastDue, "c").ToString & ";"
            arrearsdata += Format(days30, "c").ToString & ";"
            arrearsdata += Format(days60, "c").ToString & ";"
            arrearsdata += Format(days90, "c").ToString & ";"
            arrearsdata += Format(days120, "c").ToString & ";"
            arrearsdata += Format(writeOffBankruptcy, "c").ToString

            Return arrearsdata
        Catch
            Return ";;;;;;;;;"
        End Try
    End Function

    <WebMethod(Description:="Retrieve customer credit history")> _
    Public Function GetCreditHistory(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim credit As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim paidOnTime As Integer = 0
        Dim notPaidOnTime As Integer = 0
        Dim unpaidByPenalty As Integer = 0
        Dim eligibleForDisconnect As Integer = 0
        Dim shutOffNotice As Integer = 0
        Dim nonpayServiceOrder As Integer = 0
        Dim discForNonPay As Integer = 0
        Dim completeNonPaySO As Integer = 0
        Dim paidToAvoidDisc As Integer = 0
        Dim collectionNotice As Integer = 0
        Dim eligibleForWO As Integer = 0
        Dim writeOff As Integer = 0
        Dim woReinstate As Integer = 0
        Dim assignAgency As Integer = 0
        Dim bankruptcy As Integer = 0
        Dim bankruptcyDismissal As Integer = 0
        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT Type_Desc FROM Account_Credit" & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken & _
                                  " AND Credit_Action_Date >= (SYSDATE - 365) "
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                Select Case dr(0).ToString
                    Case "Paid On Time"
                        paidOnTime += 1
                    Case "Not Paid On Time"
                        notPaidOnTime += 1
                    Case "Unpaid By Penalty"
                        unpaidByPenalty += 1
                    Case "Eligible For Disconnect"
                        eligibleForDisconnect += 1
                    Case "Shut-Off Notice"
                        shutOffNotice += 1
                    Case "Nonpay Disc Service Order"
                        nonpayServiceOrder += 1
                    Case "Disconnect For Non-Pay"
                        discForNonPay += 1
                    Case "Complete NonPayDisconnect"
                        completeNonPaySO += 1
                    Case "Paid To Avoid Disconnect"
                        paidToAvoidDisc += 1
                    Case "Collection Notice"
                        collectionNotice += 1
                    Case "Eligible For Write-Off"
                        eligibleForWO += 1
                    Case "Write-Off"
                        writeOff += 1
                    Case "Write-Off Reinstated"
                        woReinstate += 1
                    Case "Assign Agency"
                        assignAgency += 1
                    Case "Bankruptcy"
                        bankruptcy += 1
                    Case "Bankruptcy Dismissal"
                        bankruptcyDismissal += 1
                End Select
            End While
            dr.Close()

            conn.Close()
            credit = paidOnTime.ToString & ";"
            credit += notPaidOnTime.ToString & ";"
            credit += unpaidByPenalty.ToString & ";"
            credit += eligibleForDisconnect.ToString & ";"
            credit += shutOffNotice.ToString & ";"
            credit += nonpayServiceOrder.ToString & ";"
            credit += discForNonPay.ToString & ";"
            credit += completeNonPaySO.ToString & ";"
            credit += paidToAvoidDisc.ToString & ";"
            credit += collectionNotice.ToString & ";"
            credit += eligibleForWO.ToString & ";"
            credit += writeOff.ToString & ";"
            credit += woReinstate.ToString & ";"
            credit += assignAgency.ToString & ";"
            credit += bankruptcy.ToString & ";"
            credit += bankruptcyDismissal.ToString

            Return credit
        Catch
            Return ";;;;;;;;;;;;;;;;"
        End Try
    End Function

    <WebMethod(Description:="Retrieve if customers are allowed to pay with check")> _
    Public Function GetCheckAllowed(ByVal custToken As String) As String
        Dim checkallowed As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT check_allowed_ind " & _
                                  " FROM Customer" & _
                                  " WHERE Customer_Tkn = " & cw_tkn
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                If dr(0).Equals("Y") Then
                    checkallowed = "Yes"
                Else
                    checkallowed = "No"
                End If
            End If
            dr.Close()
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return checkallowed
    End Function

    <WebMethod(Description:="Retrieve number of returned checks")> _
    Public Function GetReturnedCheck(ByVal custToken As String) As String
        Dim returnedcheck As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT Count(1) " & _
                                  " FROM Payment " & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                    " AND Payment_Date >= SYSDATE - 365 " & _
                                    " AND Payment_Date <= SYSDATE " & _
                                    " AND Return_Reason_Desc IN ('Account Closed', 'Electronic', 'Insufficient Funds')"
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            If dr.Read Then
                returnedcheck = dr(0).ToString
            End If
            dr.Close()
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return returnedcheck
    End Function

    <WebMethod(Description:="Retrieve if customers are considered large volume")> _
    Public Function isLargeVolume(ByVal custToken As String) As String
        Dim lv As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT COUNT(*) " & _
                               " FROM CENT.ACCOUNT_PACKAGE A INNER JOIN" & _
                               " CENT.BILLING_PACKAGE B ON A.CUSTOMER_TKN = B.CUSTOMER_TKN AND A.BILLING_PKG_TKN = B.BILLING_PKG_TKN" & _
                               " WHERE (A.ACCOUNT_PKG_DESC = 'Gas') AND ((A.CUSTOMER_TKN, A.CUSTOMER_ACCT_TKN, A.ACCOUNT_PKG_TKN) IN" & _
                               " (SELECT C.CUSTOMER_TKN, C.CUSTOMER_ACCT_TKN, C.ACCOUNT_PKG_TKN" & _
                               " FROM CENT.ACCOUNT_PRICING C INNER JOIN" & _
                               " CENT.PRICING_PLAN D ON C.VENDOR_TKN = D.VENDOR_TKN AND C.PACKAGE_TKN = D.PACKAGE_TKN AND" & _
                               " C.PRICING_PLAN_TKN = D.PRICING_PLAN_TKN" & _
                               " WHERE (D.PLAN_DESC LIKE 'Lg%Trans%') AND (A.STATUS_DESC = 'Active'))) AND (A.CUSTOMER_TKN = '" & cw_tkn & "')"
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                If dr(0) > 0 Then
                    lv = "Yes"
                Else
                    lv = "No"
                End If
            End While
            dr.Close()
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return lv
    End Function

    <WebMethod(Description:="Retrieve if customers are Brokers")> _
    Public Function isBroker(ByVal custToken As String) As String
        Dim pooltkn As String = Nothing
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT pool_tkn" & _
                               " FROM CENT.Broker_Assign" & _
                               " WHERE (CUSTOMER_TKN = '" & cw_tkn & "')"
            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                If dr(0) > 0 Then
                    pooltkn = dr("pool_tkn").ToString
                Else
                    pooltkn = Nothing
                End If
            End While
            dr.Close()
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return pooltkn
    End Function

    <WebMethod(Description:="Retrieve charge and tax information for a statement (Commission use)")> _
    Public Function GetCommissionChargeInfo(ByVal stmtToken As String) As String
        Dim chargeAmt As Decimal = 0.0
        Dim taxAmt As Decimal = 0.0
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(stmtToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            'Dim query1 As String = " select sum(calc_price_amt + adjustment_amt), sum(price_tax_amt) from billing_charge " & _
            '                       " where cust_statement_tkn = " & cw_tkn & _
            '                       " and type_desc = 'Charge'"
            Dim query As String = " select sum(calc_price_amt + adjustment_amt), sum(price_tax_amt) from billing_charge bc " & _
                                  " where cust_statement_tkn = " & cw_tkn & _
                                  " and type_desc = 'Charge' " & _
                                  " and not exists (select 1 from account_charge ac " & _
                                  " where ac.customer_tkn = bc.customer_tkn " & _
                                  " and ac.customer_acct_tkn = bc.customer_acct_tkn " & _
                                  " and ac.account_pkg_tkn = bc.account_pkg_tkn " & _
                                  " and ac.account_charge_tkn = bc.account_charge_tkn " & _
                                  " and type_desc = 'Contract' and sub_type_desc = 'APP')"

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                If Not IsDBNull(dr(0)) Then
                    chargeAmt = chargeAmt + dr(0)
                End If
                If Not IsDBNull(dr(1)) Then
                    taxAmt = taxAmt + dr(1)
                End If
            End While
            conn.Close()

            Return String.Format("{0:c}", chargeAmt) & ";" & String.Format("{0:c}", taxAmt) & ";" & String.Format("{0:c}", chargeAmt + taxAmt)
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve charge and tax information for a statement (Customer use)")> _
    Public Function GetCustomerChargeInfo(ByVal stmtToken As String) As String
        Dim returndata As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(stmtToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select to_char(total_stmt_amt,'$99,999,999.99PR') as tsa, to_char(delinquent_date, 'mm-dd-yyyy') from customer_statement " & _
                                  " where(cust_statement_tkn = " & cw_tkn & ")"

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                returndata = dr(0).ToString & ";" & dr(1).ToString
            End While
            conn.Close()

            Return returndata
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve customer account total")> _
    Public Function GetAccountTotal(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As String
        Dim total As Double = 0.0
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " SELECT SUM(Charge_Amt) " & _
                                  " FROM Account_Charge" & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken & _
                                  " AND Charge_Amt <> 0 " & _
                                  " AND Sub_Type_Desc <> 'APP' "
            Dim query2 As String = " SELECT SUM(Epp_Status_Amt) " & _
                                  " FROM Account_Contract" & _
                                  " WHERE Customer_Tkn = " & cw_tkn & _
                                  " AND Customer_Acct_Tkn = " & acctToken & _
                                  " AND Account_Pkg_Tkn = " & acctpToken & _
                                  " AND Type_Desc = 'APP' " & _
                                  " AND Start_Date <= SYSDATE " & _
                                  " AND (End_Date IS NULL OR End_Date >= SYSDATE) "
            Dim query3 As String = " SELECT SUM(Principal_Amt) " & _
                                 " FROM Account_Contract" & _
                                 " WHERE Customer_Tkn = " & cw_tkn & _
                                 " AND Customer_Acct_Tkn = " & acctToken & _
                                 " AND Account_Pkg_Tkn = " & acctpToken & _
                                 " AND Type_Desc <> 'APP' " & _
                                 " AND Type_Desc <> 'Deposit' " & _
                                 " AND Start_Date <= SYSDATE " & _
                                 " AND (End_Date IS NULL OR End_Date >= SYSDATE) "
            Dim cmd As New OracleCommand(query, conn)
            Dim cmd2 As New OracleCommand(query2, conn)
            Dim cmd3 As New OracleCommand(query3, conn)
            Dim dr As OracleDataReader
            Dim dr2 As OracleDataReader
            Dim dr3 As OracleDataReader

            conn.Open()
            dr = cmd.ExecuteReader
            dr2 = cmd2.ExecuteReader
            dr3 = cmd3.ExecuteReader
            If dr.Read Then
                If Not IsDBNull(dr(0)) Then
                    total += dr(0)
                End If
            End If
            If dr2.Read Then
                If Not IsDBNull(dr2(0)) Then
                    total += dr2(0)
                End If
            End If
            If dr3.Read Then
                If Not IsDBNull(dr3(0)) Then
                    total += dr3(0)
                End If
            End If
            dr.Close()
            dr2.Close()
            dr3.Close()
            conn.Close()
            Return Format(total, "c").ToString
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve customer log information")> _
    Public Function GetLogs(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String, ByVal History As String) As DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = " select create_tstamp, create_userid, type_desc, log_text from customer_log " & _
                                   " where customer_tkn = " & cw_tkn & _
                                   " and create_tstamp >= sysdate - " & History & _
                                   " and create_tstamp >= '01jan2008' " & _
                                   " and create_userid not like 'Batch_%' " & _
                                   " and type_desc in ('Commission Complaint', 'Construction Design Note', 'CSR Note', 'Deceased', 'Identity Theft', 'Instant ID Verification', 'Letter', 'MGR Note', 'Payment Method Note', 'Resolution', 'Returned Mail', 'Social Security Check', 'Tampering') " & _
                                   " order by create_tstamp desc "
            Dim query2 As String = " select create_tstamp, create_userid, type_desc, log_text from account_pkg_log " & _
                                   " where customer_tkn = " & cw_tkn & _
                                   " and customer_acct_tkn = " & acctToken & _
                                   " and account_pkg_tkn = " & acctpToken & _
                                   " and create_tstamp >= sysdate - " & History & _
                                   " and create_tstamp >= '01jan2008' " & _
                                   " and create_userid not like 'Batch_%' " & _
                                   " and type_desc in ('Agency Note', 'Cold Weather Notes', 'Commission Complaint', 'CSR Note', 'Disconnect Nonpay', 'Disputed', 'High Bill Complaint', 'Letter', 'MGR Note', 'Payment Agreement', 'Resolution') " & _
                                   " order by create_tstamp desc "

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            Dim ds As DataSet = New DataSet("Customer Logs")
            Dim logs As New DataTable("Logs")
            ds.Tables.Add(logs)
            logs.Columns.Add("Date", GetType(Date))
            logs.Columns.Add("Userid", GetType(String))
            logs.Columns.Add("Type", GetType(String))
            logs.Columns.Add("Description", GetType(String))
            Dim log_data(3) As Object

            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                log_data(0) = dr(0)  'date
                log_data(1) = dr(1).ToString  'userid
                log_data(2) = dr(2).ToString  'type
                log_data(3) = dr(3).ToString  'description
                logs.Rows.Add(log_data)
            End While

            dr.Close()
            cmd.CommandText = query2
            dr = cmd.ExecuteReader
            While dr.Read
                log_data(0) = dr(0)  'date
                log_data(1) = dr(1).ToString  'userid
                log_data(2) = dr(2).ToString  'type
                log_data(3) = dr(3).ToString  'description
                logs.Rows.Add(log_data)
            End While
            dr.Close()
            conn.Close()

            Return ds
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Check customer for printed, in progress disconnect non pay service order")> _
    Public Function GetInProgressDisconnect(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As Integer
        Dim result As Integer = 0  '0=False,1=True
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select count(a.customer_tkn) from account_order a, service_order b " & _
            "where a.service_order_tkn = b.service_order_tkn and " & _
            "b.status_desc = 'In-Progress' and " & _
            "b.type_desc = 'Disconnect - Non Pay' and " & _
            "b.printed_ind = 'Y' and " & _
            "a.customer_tkn = " & cw_tkn & " and " & _
            "a.customer_acct_tkn = " & acctToken & " and " & _
            "a.account_pkg_tkn = " & acctpToken

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                result = dr.GetValue(0)
            End While
            dr.Close()
        Catch
            Return 0
        End Try
        Return result
    End Function

    <WebMethod(Description:="Show agency pledge information")> _
    Public Function GetPledgeInfo(ByVal custToken As String, ByVal acctToken As String, ByVal acctpToken As String) As DataSet
        Dim dsPledgeInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select acct_pkg_log_tkn as id, to_char(create_tstamp, 'mm/dd/yyyy')as ts, create_userid, type_desc, log_text " & _
            "from(account_pkg_log) " & _
            "where type_desc = 'Web Assistance Pledge' and " & _
            "customer_tkn = " & cw_tkn & " and " & _
            "customer_acct_tkn = " & acctToken & " and " & _
            "account_pkg_tkn = " & acctpToken & " and " & _
            "create_tstamp > sysdate - 30"

            Dim daPledgeInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daPledgeInfo.Fill(dsPledgeInfo, "PledgeInfo")
            conn.Close()
        Catch
            Return Nothing
        End Try
        Return dsPledgeInfo
    End Function
    '==============================================================outage information=================================================================================================================================
    <WebMethod(Description:="Get last valid OMS data read timestamp")> _
    Public Function GetLastOMSRead() As String
        Dim lastupdatetime As String = Now.ToString & ";15"  'default values
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("SELECT top 1 outages.lastupdate, outages.interval from outages where controlrecord = 1", conn)
        Try
            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    lastupdatetime = dr("lastupdate").ToString & ";" & dr("interval").ToString
                End While
                dr.Close()
            End If
            conn.Close()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return lastupdatetime
    End Function

    <WebMethod(Description:="Show customer outage information")> _
    Public Function GetOutages() As DataSet
        'Dim dsOutageInfo As New DataSet
        Dim oInfo As New outageInfo("", "", "", False, New DataSet)  'clears all log info
        Try
            refreshNow(oInfo)
            If oInfo.refreshNow Then  'elapsed time has passed--time for fresh oms data read
                UpdateNextOMSReadTime(oInfo)  'update OMS read attempt timestamp -- doesnt matter if good OMS read or not (will delay next read attempt until set interval)
                oInfo.dsOutages = getOMSOutages()  'read outages from OMS
                If oInfo.dsOutages.Tables.Count > 0 AndAlso oInfo.dsOutages.Tables(0).Columns.Count > 0 Then  'valid OMS dataset returned
                    oInfo.logInfo += "[OMS Successful read]"
                    storeOMSData(oInfo)  'update SQL tables with current OMS outages if successful OMS read
                Else  ' read SQL instead of OMS data because of error reading OMS
                    oInfo.logInfo += "[*OMS Read Error*]"
                    getSQLOutages(oInfo)
                End If
            Else  'still within time limit of last oms read--use sql data instead
                getSQLOutages(oInfo)
            End If
            LogReadData(oInfo)  'creates entry in the log table for what occured for this particular outage map read
        Catch
            Return Nothing
        End Try
        Return oInfo.dsOutages
    End Function

    <WebMethod(Description:="Retrieve total outages")> _
    Public Function GetOutageTotal() As String
        Dim total As String = Nothing
        Try
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OMS").ConnectionString)
            Dim query As String = "select sum(num_cust) as NUM FROM aeven WHERE curent = 'T' and open_and_curent = 'T' and num_cust > 0"
            Dim cmd0 As New OracleCommand(query, conn)
            Dim dr As OracleDataReader

            conn.Open()
            dr = cmd0.ExecuteReader
            While dr.Read
                total = dr.GetValue(0).ToString
            End While
            dr.Close()

            conn.Close()
        Catch
            Return Nothing
        End Try
        Return total
    End Function

    Public Function getOMSOutages() As DataSet
        Dim dsOutageInfo As New DataSet
        Try
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OMS").ConnectionString)
            Dim query As String = "select esz, sum(num_cust) as NUM FROM aeven WHERE curent = 'T' and open_and_curent = 'T' and num_cust > 0 GROUP By esz"
            Dim daOutageInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daOutageInfo.Fill(dsOutageInfo, "OutageInfo")
            conn.Close()
        Catch
        End Try
        Return dsOutageInfo
    End Function

    Public Function getSQLOutages(ByVal oInfo As outageInfo) As outageInfo
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            'get OMS data from SQL tables
            Dim query As String = "select esz, total as NUM FROM outages WHERE controlrecord = 0 order by id"
            Dim daOutageInfo As New SqlDataAdapter(query, conn)
            conn.Open()
            daOutageInfo.Fill(oInfo.dsOutages, "OutageInfo")
            conn.Close()
            oInfo.logInfo += "[SQL Successful read]"

            ' set display time to be the timestamp from the last good OMS read
            Dim timeData As String = GetLastOMSRead()
            Dim lastupdate As String = timeData.Split(";")(0).ToString
            Dim interval As String = timeData.Split(";")(1).ToString
            Dim logTime As String = Nothing
            Dim parseDate As DateTime = Nothing
            If IsDate(lastupdate) Then
                parseDate = lastupdate
                logTime = Format(parseDate, "HH" & ":" & "mm" & ":" & "ss")
            Else
                logTime = lastupdate
            End If
            oInfo.logInfo &= "[Get last OMS read " & logTime & "]"

            oInfo.logData = "[" 'empty the logdata to store data for this SQL read
            Dim dt As DataTable = oInfo.dsOutages.Tables("OutageInfo")
            For Each row As DataRow In dt.Rows
                oInfo.logData += row(0).ToString & "-" & row(1).ToString & ";"
            Next
            oInfo.logData += "]"  'finish logdata store for this SQL read
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            oInfo.logInfo += "[*SQL Read Error*]"
        End Try
        Return oInfo
    End Function

    Public Function UpdateNextOMSReadTime(ByVal oInfo As outageInfo) As outageInfo
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim cmd As New SqlCommand("", conn)
            Dim dr As SqlDataReader
            conn.Open()
            'update control record with current timestamp  --  doesnt matter if good OMS read or not (will delay next read attempt until set interval)
            cmd.CommandText = "update outages set outages.refreshdate=GETDATE() where outages.controlrecord = 1"
            dr = cmd.ExecuteReader
            dr.Close()
            oInfo.logInfo &= "[Saved refreshdate " & Format(Now, "HH" & ":" & "mm" & ":" & "ss") & "]"
            conn.Close()
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            oInfo.logInfo &= "[*UpdateOMSReadTime Error*]"
        End Try
        Return oInfo
    End Function

    Public Function storeOMSData(ByVal oInfo As outageInfo) As outageInfo
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Try
            Dim query As String = ""
            Dim cmd As New SqlCommand(query, conn)
            Dim dr As SqlDataReader

            conn.Open()
            'remove last OMS read data
            cmd.CommandText = "delete from outages where controlrecord = 0"
            dr = cmd.ExecuteReader
            dr.Close()
            oInfo.logInfo &= "[Removed old SQL data]"
            'store current OMS read data
            Dim dt As DataTable = oInfo.dsOutages.Tables("OutageInfo")
            oInfo.logData = "[" 'empty the logdata to store data for this OMS read
            For Each row As DataRow In dt.Rows
                oInfo.logData += row(0).ToString & "-" & row(1).ToString & ";"
                cmd.CommandText = "insert into outages (controlrecord,lastupdate,esz,total) VALUES (0,GETDATE()," & row(0).ToString & "," & row(1).ToString & ")"
                dr = cmd.ExecuteReader
                dr.Close()
            Next
            oInfo.logData &= "]"  'finish logdata store for this OMS read
            oInfo.logInfo &= "[Saved OMS data to SQL]"
            'update timestamp indicating good OMS read
            cmd.CommandText = "update outages set lastupdate=GETDATE() where controlrecord = 1"
            dr = cmd.ExecuteReader
            dr.Close()
            oInfo.logInfo &= "[Saved lastupdate " & Format(Now, "HH" & ":" & "mm" & ":" & "ss") & "]"
            conn.Close()
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            oInfo.logInfo &= "[*StoreOMSData Error*]"
        End Try
        Return oInfo
    End Function

    Public Function refreshNow(ByVal oInfo As outageInfo) As outageInfo
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("SELECT top 1 refreshdate,interval from outages where controlrecord = 1", conn)
        Try
            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    'if the refresh interval has expired return true (time for an actual OMS data read)
                    If DateDiff(DateInterval.Minute, dr(0), Now) >= dr(1).ToString Then
                        oInfo.refreshNow = True
                    Else
                        oInfo.refreshNow = False
                    End If
                End While
                dr.Close()
            Else
                ' if control record not present, create one
                dr.Close()
                cmd.CommandText = "insert into outages (controlrecord,lastupdate,refreshdate,interval) VALUES (1,GETDATE(),GETDATE(),15)"
                dr = cmd.ExecuteReader
                dr.Close()
                oInfo.logInfo &= "[Created new control record]"
            End If
            conn.Close()
        Catch ex As Exception
            oInfo.logInfo &= "[*RefreshNow Error*]"
            If ex.Message.Contains("Invalid object name") Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.outages(id int IDENTITY(1,1) NOT NULL,controlrecord bit NOT NULL,lastupdate datetime NULL,refreshdate datetime NULL,interval int NULL,esz varchar(20) NULL,total varchar(20) NULL,CONSTRAINT PK_outages PRIMARY KEY CLUSTERED (id ASC))"
                cmd.ExecuteNonQuery()
                oInfo.logInfo &= "[Created outages table]"
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            oInfo.refreshNow = False
        End Try
        Return oInfo
    End Function

    Public Sub LogReadData(ByVal oInfo As outageInfo)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Try
            cmd.CommandText = "insert into log (date,username,refer,web) VALUES (GETDATE(),'OutageMap','" & Left(oInfo.logData, 255) & "',' " & Left(oInfo.logInfo, 127) & "')"
            Dim dr As SqlDataReader
            ' Log Outage Map read activities
            conn.Open()
            dr = cmd.ExecuteReader
            dr.Close()
            ' delete old logs - only keep 2 weeks
            cmd.CommandText = "delete from log where (date <= GETDATE() - 14) and (username = 'OutageMap')"
            dr = cmd.ExecuteReader
            dr.Close()
            conn.Close()
        Catch ex As Exception
            If ex.Message.Contains("Invalid object name") Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.log(id int IDENTITY(1,1) NOT NULL,ipaddress varchar(25) NULL,date datetime NULL,refer varchar(256) NULL,username varchar(50) NULL,platform varchar(25) NULL,browser varchar(25) NULL,web varchar(128) NULL,html varchar(max) NULL,CONSTRAINT PK_logs PRIMARY KEY CLUSTERED (id ASC))"
                cmd.ExecuteNonQuery()
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub
    '==============================================================end outage information=============================================================================================================================
    <WebMethod(Description:="Delete a customer account")> _
    Public Function DeleteAccount(ByVal type As String, ByVal Value As String) As Boolean
        Dim tdes As New tripleDES
        Dim id As String = ""
        Dim query As String = ""

        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
            conn.Open()

            If type = "account" Then
                query = "SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Membership.IsLockedOut, aspnet_Membership.LastLoginDate,aspnet_Profile.PropertyValuesString "
                query &= "FROM aspnet_Users INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId INNER JOIN aspnet_Profile ON aspnet_Users.UserId = aspnet_Profile.UserId "
                query &= "WHERE (PATINDEX('" & Value & "', aspnet_Profile.PropertyValuesString) > 0)"
            ElseIf type = "username" Then
                query = "SELECT aspnet_Users.UserId,aspnet_Users.username,aspnet_membership.email,aspnet_membership.islockedout,aspnet_membership.lastlogindate FROM aspnet_Users INNER JOIN aspnet_membership ON aspnet_Users.UserId = aspnet_membership.UserId "
                query &= "where (aspnet_Users.UserName = '" & Value & "')"
            End If
            If query <> "" Then
                Dim cmd0 As New SqlCommand(query, conn)
                Dim dr0 As SqlDataReader
                dr0 = cmd0.ExecuteReader
                While dr0.Read
                    id = dr0(0).ToString  'get the userid to delete
                End While
                dr0.Close()
            End If

            'deletes userid entry from aspnet_profile, aspnet_usersinroles, aspnet_membership, aspnet_users
            Dim query1 As String = "delete from aspnet_Profile WHERE UserID ='" & id & "'"
            Dim query2 As String = "delete from aspnet_UsersInRoles WHERE UserID ='" & id & "'"
            Dim query3 As String = "delete from aspnet_membership where userID ='" & id & "'"
            Dim query4 As String = "delete from aspnet_users where userID ='" & id & "'"
            Dim cmd As New SqlCommand(query1, conn)
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            dr.Close()
            cmd.CommandText = query2
            dr = cmd.ExecuteReader
            dr.Close()
            cmd.CommandText = query3
            dr = cmd.ExecuteReader
            dr.Close()
            cmd.CommandText = query4
            dr = cmd.ExecuteReader
            dr.Close()

            conn.Close()
        Catch
            Return False
        End Try
        Return True
    End Function

    <WebMethod(Description:="Retrieve LVT Gas Nomination Information")> _
    Public Function GetNominationInfo(ByVal custToken As String, ByVal nomMonth As String, ByVal nomYear As String) As DataSet
        Dim dsNomInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES
        Dim strDate As String = nomMonth & "/" & nomYear
        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT TO_CHAR(IMBALANCE_DATE,'MM/DD/YYYY'), READ," & _
                                  " CITY_GATE_READ, NOMINATION_MMBTU, NOMINATION_MCF, BTU_MEASUREMENT, IMBALANCE, MTD_IMBALANCE, PERCENT_NOM," & _
                                  " ABS_IMBALANCE, TEN_PERCENT_NOM_MCF, OUT_OF_TOLERANCE, OUT_OF_TOLERANCE_PERCENT_1, OUT_OF_TOLERANCE_PERCENT_2,Pipeline_Name,Marketer,Interconnect,Pool,demand,demand_date,ESTIMATED" & _
                                  " FROM (CENT.NOMINATION_IMBALANCE)"
            If nomYear Is Nothing Then
                query += " WHERE (TO_CHAR(IMBALANCE_DATE, 'MM/YYYY') = TO_CHAR(SYSDATE, 'MM/YYYY')) AND (CUSTOMER_TKN = '" & cw_tkn & "')"
            Else
                query += " WHERE (TO_CHAR(IMBALANCE_DATE, 'MM/YYYY') = '" & strDate & "') AND (CUSTOMER_TKN = '" & cw_tkn & "')"
            End If
            query += " ORDER BY IMBALANCE_DATE ASC"

            Dim daNominationInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daNominationInfo.Fill(dsNomInfo, "NominationInfo")
            conn.Close()
            Return dsNomInfo
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve Pool members")> _
    Public Function GetBrokerAssigned(ByVal pooltkn As String) As DataSet
        Dim dsBrokerInfo As New DataSet
        Dim cw_tkn As String
        Dim tdes As New tripleDES        
        Try
            cw_tkn = tdes.Decrypt(pooltkn)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select customer_name, customer_tkn" & _
                                  " from(customer)" & _
                                  " where customer_tkn IN(" & _
                                        " Select customer_tkn" & _
                                        " from(account_package)" & _
                                        " where (customer_tkn, customer_acct_tkn, account_pkg_tkn) IN(" & _
                                                " select customer_tkn, customer_acct_tkn, account_pkg_tkn" & _
                                                " from(account_pool_assign)" & _
                                                " where pool_tkn = '" & cw_tkn & "'))"

            Dim daBrokerInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daBrokerInfo.Fill(dsBrokerInfo, "BrokerAssigned")
            conn.Close()
            Return dsBrokerInfo
        Catch
            Return Nothing
        End Try
    End Function
    <WebMethod(Description:="Retrieve Large Volume Gas Transport Broker")> _
        Public Function GetLVBrokerXML() As String
        Dim LVBrokerInfo As String = Nothing
        Try

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "Select CONFIG_TEXT" & _
                                  " FROM         SYSTEM_CONFIGURATION" & _
                                  " WHERE     (CONFIG_NAME = 'EDE_Gas_Billing')"
            Dim cmd0 As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd0.ExecuteReader
            While dr.Read
                LVBrokerInfo = dr.GetValue(0)
            End While
            conn.Close()
            Return LVBrokerInfo
        Catch
            Return Nothing
        End Try
    End Function

    <WebMethod(Description:="Retrieve Large Volume Gas Transport Customer Names for Broker")> _
        Public Function GetLVBrokerCustomersName(ByVal tokens As String) As DataSet
        Dim dsBrokerCustInfo As New DataSet
        Try

            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "Select Customer_Name,Customer_Tkn" & _
                                  " FROM         Customer" & _
                                  " WHERE     (Customer_Tkn in ('" & tokens & "'))"
            Dim daBrokerInfo As New OracleDataAdapter(query, conn)
            conn.Open()
            daBrokerInfo.Fill(dsBrokerCustInfo, "CustomerNamesTokens")
            conn.Close()
            Return dsBrokerCustInfo
        Catch
            Return Nothing
        End Try
    End Function
    <WebMethod(Description:="Retrieve All LVT Gas Customers from Nomination_Imbalance table")> _
    Public Function GetAllLVCustomers() As DataSet
        Dim dsAllLVCust As New DataSet
        Try
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "SELECT DISTINCT CUSTOMER_NAME, CUSTOMER_TKN" & _
                                  " FROM NOMINATION_IMBALANCE" & _
                                  " ORDER BY CUSTOMER_NAME"

            Dim daAllLVCust As New OracleDataAdapter(query, conn)
            conn.Open()
            daAllLVCust.Fill(dsAllLVCust, "AllLVCustomers")
            conn.Close()
            Return dsAllLVCust
        Catch
            Return Nothing
        End Try
    End Function
    <WebMethod(Description:="Retrieve a stock quote from yahoo")> _
    Public Function GetStockQuote(ByVal symbol As String) As String
        Dim quote As String = Nothing
        Try
            Dim quoteURL As String = "http://download.finance.yahoo.com/d/quotes.csv?s=" & symbol & "&f=sl1d1t1c1ohgvj1pp2wern"
            Dim wreq As HttpWebRequest
            Dim wres As HttpWebResponse
            Dim sr As StreamReader

            wreq = WebRequest.Create(quoteURL)
            wreq.Timeout = 5000
            wres = wreq.GetResponse()
            sr = New StreamReader(wres.GetResponseStream())
            quote = sr.ReadToEnd
            sr.Close()
            Return quote
        Catch
            Return Nothing
        End Try
    End Function
    <WebMethod(Description:="Retrieve customer ebilling status")> _
    Public Function GetEBillingStatus(ByVal custToken As String, ByVal billpToken As String) As Integer
        Dim result As Integer = 0  '0=not enrolled,1=enrolled
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select count(*) from billing_address,billing_package " & _
            "where (billing_address.customer_tkn = " & cw_tkn & ") " & _
            "and (billing_address.billing_pkg_tkn = " & billpToken & ") " & _
            "and (billing_package.customer_tkn = " & cw_tkn & ") " & _
            "and (billing_package.billing_pkg_tkn = " & billpToken & ") " & _
            "and (billing_package.end_date is null or billing_package.end_date > sysdate) " & _
            "and (billing_address.start_date < sysdate) " & _
            "and (billing_address.end_date is null or billing_address.end_date > sysdate) " & _
            "and (billing_address.start_date < sysdate) and (billing_address.delivery_method_desc = 'Electronic Bill')"

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                result = dr.GetValue(0)
            End While
            dr.Close()
        Catch
            result = 0
        End Try

        Return result
    End Function
    <WebMethod(Description:="Retrieve customer email address")> _
    Public Function GetEmailAddress(ByVal custToken As String, ByVal billpToken As String) As String
        Dim emailaddress As String = ""
        Dim cw_tkn As String
        Dim tdes As New tripleDES

        Try
            cw_tkn = tdes.Decrypt(custToken)
            Dim conn As New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CustomerWatch").ConnectionString)
            Dim query As String = "select e_mail_addr from billing_address " & _
            "where (billing_address.customer_tkn = " & cw_tkn & ") " & _
            "and (billing_address.billing_pkg_tkn = " & billpToken & ") " & _
            "and (billing_address.end_date is null or billing_address.end_date > sysdate) " & _
            "and (billing_address.start_date <= sysdate)"

            Dim cmd As New OracleCommand(query, conn)
            Dim dr As OracleDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            While dr.Read
                emailaddress = dr.GetValue(0)
            End While
            dr.Close()
        Catch
            emailaddress = ""
        End Try
        Return emailaddress
    End Function
    <WebMethod(Description:="Retrieve coordinates from zipcode")> _
    Public Function getLatLngbyZip(ByVal zipcode As String) As String
        Dim latLng As String = "-094.50266;37.11334;JOPLIN;MO" 'default values lat, lon, city, state
        Try
            latLng = getLatLngData(zipcode)
        Catch
        End Try
        return latLng
    End Function

    Private Function getLatLngData (ByVal zipcode As String) As String
        Dim conn As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath("~/App_Data/zipcodes.mdb"))
        Dim latLng As String = "-094.50266;37.11334;JOPLIN;MO" 'default values lat, lon, city, state
        Try 
            Dim query As String = "SELECT longitude,latitude,city,stateabbreviation FROM ZIPCODES,STATES where zipcodes.statecode=states.statecode and zipcode = '" & zipcode & "'"
            conn.Open()
            Dim cmd As New OleDbCommand(query, conn)
            Dim dr As OleDbDataReader
            dr = cmd.ExecuteReader
            While dr.Read
                latLng = dr.GetValue(1).ToString & ";" & dr.GetValue(0).ToString & ";" & dr.GetValue(2).ToString & ";" & dr.GetValue(3).ToString
            End While
            dr.Close()
            conn.Close()
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return latlng
    End Function

    <WebMethod(Description:="Retrieve zip data based on distance")> _
    Public Function GetZipByDistance(ByVal latitude As String, ByVal longitude As String, ByVal distance As String) As Dataset
        Dim dsZipInfo As New DataSet
        Dim conn As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath("~/App_Data/zipcodes.mdb"))
        Try
            Dim query As String = "select zd.zipcode,sqr((69.1 * (Latitude - " & latitude & ")) ^ 2 + (69.1 * (zd.Longitude - " & longitude &") * cos(" & latitude & "/57.3)) ^ 2) as distance from zipcodes zd where sqr((69.1 * (Latitude - " & latitude & ")) ^ 2 + (69.1 * (zd.Longitude - " & longitude & ") * cos(" & latitude & "/57.3)) ^ 2) < " & distance
            Dim da As New OleDbDataAdapter(query, conn)
            conn.Open()
            da.Fill(dsZipInfo, "ZipInfo")
            conn.Close()
        Catch
        End Try
        Return dsZipInfo
    End Function
    <WebMethod(Description:="Retrieve weather data from forecast.io")> _
    Public Function GetWeatherAPIData(ByVal zipcode As String) As Byte()
        Dim ms As MemoryStream = New MemoryStream
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim insertRequired As Boolean = False
        Dim updateRequired As Boolean = False
        Dim holdData As Byte() = Nothing
        Dim json As String = ""
        Try
            cmd.CommandText = "SELECT radarimages.imagedata, radarimages.lastupdate from dbo.radarimages where radarimages.zipcode = '" & zipcode & "'"
            conn.Open()
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then  'weather data on file
                While dr.Read
                    If Not dr("imagedata") Is System.DBNull.Value Then  'weather data found -- process it
                        holdData = dr("imagedata")  'hold last good read
                        Dim imagebytes As Byte() = Nothing  'create a byte array to hold the data
                        imagebytes = CType(dr("imagedata"), Byte())  'convert the data object to bytes
                        ms = New System.IO.MemoryStream(imagebytes, 0, imagebytes.Length)  'Read the byte array into a MemoryStream
                        If DateDiff(DateInterval.Minute, dr("lastupdate"), Now) >= 30 Then  'weather data is older than 30 minutes -- update it
                            updateRequired = True
                        End If
                    End If
                End While
            Else 'no rows (weather data doesnt exist)
                insertRequired = True
            End If
            dr.Close()

            If updateRequired Or insertRequired Then  'update the weather data with latest from api
                Dim locationData As String = getLatLngData(zipcode) 'get latitude/longitude for api
                Dim latitude As String = Math.Round(Val(locationData.Split (";")(0).ToString), 2).ToString 'round latitude to 2 decimal places -- also drops + sign
                Dim longitude As String = Math.Round(Val(locationData.Split (";")(1).ToString), 2).ToString 'round longitude to 2 decimal places
                Dim appId As String = "0fe5341d83647b4d81e74c8f0e8c4855"
                Dim url As String = String.Format("https://api.forecast.io/forecast/{0}/{1},{2}", appId, latitude, longitude)

                Using client As New WebClient()
                    json = client.DownloadString(url)
                End Using

                Dim imageBytes() as Byte = System.Text.Encoding.ASCII.GetBytes(json)

                If insertRequired Then
                    cmd.CommandText = "insert into radarimages (radarid,radarproduct,zipcode,imagedata,lastupdate,filename) values (@radarid,@radarproduct,@zipcode,@imagedata,@lastupdate,@filename)"
                ElseIf updateRequired Then
                    cmd.CommandText = "update radarimages set imagedata=@imagedata,lastupdate=@lastupdate,filename=@filename where zipcode=@zipcode"
                End If

                cmd.Parameters.AddWithValue("@radarid", "999")
                cmd.Parameters.AddWithValue("@radarproduct", "N0R")
                cmd.Parameters.AddWithValue("@zipcode", zipcode)
                cmd.Parameters.AddWithValue("@imagedata", imageBytes)
                cmd.Parameters.AddWithValue("@lastupdate", Now.ToString)
                cmd.Parameters.AddWithValue("@filename", zipcode & "_" & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & Now.Hour.ToString & Now.Minute.ToString & ".json")
                cmd.ExecuteNonQuery()  'update api data in database

                ms = New System.IO.MemoryStream(imagebytes, 0, imagebytes.Length)  'Read the byte array into a MemoryStream -- returns latest data from api
            Else 'update not required  -- ms should already contain last good read
            End If
            conn.Close()
        Catch ex As Exception
            If ex.Message = "Invalid object name 'dbo.radarimages'." Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.radarimages(id int IDENTITY(1,1) NOT NULL,radarid varchar(5) NOT NULL,radarproduct varchar(5) NOT NULL,zipcode varchar(10) NULL,imagedata varbinary(max) NOT NULL,lastupdate datetime NOT NULL,filename varchar(50) not null)"
                cmd.ExecuteNonQuery()
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            If holdData IsNot Nothing Then ms = New System.IO.MemoryStream(holdData, 0, holdData.Length) 'return last good read if exception thrown
        End Try
            ms.Position = 0
            Return ms.ToArray()
    End Function
    '==============================================================create animated noaa image=========================================================================================================================
    <WebMethod(Description:="Retrieve animated image from NOAA")> _
    Public Function GetNOAAImage(ByVal product As String, ByRef rid As String, ByVal frames As Integer) As Byte()
        Dim ms As New MemoryStream
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim insertRequired As Boolean = False
        Dim updateRequired As Boolean = False
        Dim imagebytes As Byte() = Nothing  'create a byte array to hold the image
        Dim holdImage As Byte() = Nothing
        Try
            cmd.CommandText = "SELECT radarimages.imagedata, radarimages.lastupdate from dbo.radarimages where radarimages.radarid = '" & rid & "' and radarimages.radarproduct = '" & product & "'"
            conn.Open()
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then  'radar image on file
                While dr.Read
                    If Not dr("imagedata") Is System.DBNull.Value Then
                        holdImage = dr("imagedata")  'hold last good image
                        imagebytes = CType(dr("imagedata"), Byte())  'convert the image object to bytes
                        Dim testupdate As DateTime = dr("lastupdate")
                        ms = New System.IO.MemoryStream(imagebytes, 0, imagebytes.Length)  ''Read the byte array into a MemoryStream
                        If DateDiff(DateInterval.Minute, dr("lastupdate"), Now) >= 15 Then  'animated image is older than 15 minutes -- update from NOAA data
                            updateRequired = True
                        End If
                    End If
                End While
            Else 'no rows (image doesnt exist)
                insertRequired = True
            End If
            dr.Close()

            If updateRequired Then  'update the animated image with latest built from NOAA data
                ms = buildImage(product, rid, frames)  'build new animated image from NOAA data
                If ms.Length > 0 Then  'image appears to be valid -- process it
                    imagebytes = ms.GetBuffer()  'ms.ToArray()
                    Dim cmdUpdate As New SqlCommand("update radarimages set imagedata=@imagedata,lastupdate=@lastupdate,filename=@filename where radarid=@radarid and radarproduct = @radarproduct", conn)
                    cmdUpdate.Parameters.AddWithValue("@radarid", rid)
                    cmdUpdate.Parameters.AddWithValue("@radarproduct", product)
                    cmdUpdate.Parameters.AddWithValue("@imagedata", imagebytes)
                    cmdUpdate.Parameters.AddWithValue("@lastupdate", Now.ToString)
                    cmdUpdate.Parameters.AddWithValue("@filename", product & "_" & rid & "_" & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & Now.Hour.ToString & Now.Minute.ToString & ".gif")
                    cmdUpdate.ExecuteNonQuery()  'update image in database
                Else  'image is not valid
                    Dim cmdUpdate As New SqlCommand("update radarimages set lastupdate=@lastupdate where radarid=@radarid and radarproduct = @radarproduct", conn)
                    cmdUpdate.Parameters.AddWithValue("@radarid", rid)
                    cmdUpdate.Parameters.AddWithValue("@radarproduct", product)
                    cmdUpdate.Parameters.AddWithValue("@lastupdate", Now.ToString)
                    cmdUpdate.ExecuteNonQuery()  'update database to wait another 15 minutes before attempting to access NOAA again
                    If holdImage IsNot Nothing Then ms = New System.IO.MemoryStream(holdImage, 0, holdImage.Length) 'return last known good image
                End If
            End If
            If insertRequired Then
                ms = buildImage(product, rid, frames)  'build new animated image from NOAA data
                imagebytes = ms.GetBuffer()   ' ms.ToArray()
                Dim cmdInsert As New SqlCommand("insert into radarimages (radarid,radarproduct,imagedata,lastupdate,filename) values (@radarid,@radarproduct,@imagedata,@lastupdate,@filename)", conn)
                cmdInsert.Parameters.AddWithValue("@radarid", rid)
                cmdInsert.Parameters.AddWithValue("@radarproduct", product)
                cmdInsert.Parameters.AddWithValue("@imagedata", imageBytes)
                cmdInsert.Parameters.AddWithValue("@lastupdate", Now.ToString)
                cmdInsert.Parameters.AddWithValue("@filename", product & "_" & rid & "_" & Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & Now.Hour.ToString & Now.Minute.ToString & ".gif")
                cmdInsert.ExecuteNonQuery()  'save image in database
            End If

            conn.Close()
            Return ms.ToArray()
        Catch ex As Exception
            If ex.Message = "Invalid object name 'dbo.radarimages'." Then  'table doesnt exist
                cmd.CommandText = "CREATE TABLE dbo.radarimages(id int IDENTITY(1,1) NOT NULL,radarid varchar(5) NOT NULL,radarproduct varchar(5) NOT NULL,zipcode varchar(10) NULL,imagedata varbinary(max) NOT NULL,lastupdate datetime NOT NULL,filename varchar(50) not null)"
                cmd.ExecuteNonQuery()
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            If holdImage IsNot Nothing Then ms = New System.IO.MemoryStream(holdImage, 0, holdImage.Length) 'return last good image if an exception caused any issues
            Return ms.ToArray()
        End Try
    End Function
    Private Function buildImage(ByVal product As String, ByVal rid As String, ByVal frames As Integer) As MemoryStream
        Dim fsAnimatedRadar As New MemoryStream
        Dim transparentGif As Byte() = Convert.FromBase64String("R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==")
        Try
            Dim imagePaths As New Generic.List(Of String)
            Dim imageName As New Generic.List(Of String)
            Dim arrayCount As Integer = 0
            Dim url As String = "http://radar.weather.gov/ridge/RadarImg/" & product & "/" & rid & "/"
            Dim client As New WebClient
            client.Headers.Add("user-agent", "Mozilla/5.0(Linux; U; Android 2.3.5; en-gb; LG-P500 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
            Dim webhtml As String = client.DownloadString(url)  'download web page containing links and filenames of NOAA radar images                
            Dim arrayLinks As ArrayList = ParseLinks(webhtml)  'parse the web page to extract the file names
            arrayLinks.Sort()  'sort the list
            arrayLinks.Reverse()  'play oldest image first -- through the latest

            If frames > arrayLinks.Count - 1 Then
                frames = arrayLinks.Count - 1
            End If
            ' Build array of required number of frames (images)
            For arrayCount = frames - 1 To 0 Step -1
                If (arrayLinks(arrayCount).indexOf(".gif") <> -1) Then  'is the current url a .gif image
                    imagePaths.Add(url & arrayLinks(arrayCount).ToString)  'build array filenames to retrieve from NOAA
                End If
            Next

            Dim ef As AnimatedGifEncoder = New AnimatedGifEncoder()
            ef.Start(fsAnimatedRadar)
            ef.SetTransparent(System.Drawing.Color.White)
            ef.SetDelay(150)  'delay between frames
            ef.SetRepeat(0)  '-1:no repeat,0:always repeat
            Dim i As Integer = 0
            For Each item As String In imagePaths
                Dim imageStream As New MemoryStream(saveURLBinary(imagePaths(i), arrayLinks((imagePaths.Count - 1) - i).ToString))  'retrieve the image from NOAA
                If imageStream.Length > 0 Then 'skip empty frames -- images gave 404 error from NOAA
                    ef.AddFrame(System.Drawing.Image.FromStream(imageStream))  'add the NOAA image to the animated gif
                    'ef.AddFrame(System.Drawing.Image.FromStream(New System.IO.MemoryStream(transparentGif, 0, transparentGif.Length)))  'empty file
                End If
                imageStream.Flush()
                imageStream.Close()
                imageStream.Dispose()
                i = i + 1
            Next
            ef.Finish()

            Return fsAnimatedRadar
        Catch ex As Exception
            Return New MemoryStream  'return empty during any exception
        End Try
    End Function
    Private Function saveURLBinary(ByVal url As String, Optional ByVal filename As String = "", Optional ByRef UserName As String = "", Optional ByRef Password As String = "") As Byte()
        Dim TempStream As New System.IO.MemoryStream
        Dim mywebResp As HttpWebResponse = Nothing
        Try
            'System.Net.ServicePointManager.ServerCertificateValidationCallback = Function() True  'Ignore bad https certificates - expired, untrusted, bad name, etc.
            Dim mywebReq As HttpWebRequest = CType(HttpWebRequest.Create(url), HttpWebRequest)
            mywebReq.UserAgent = "Mozilla/5.0(Linux; U; Android 2.3.5; en-gb; LG-P500 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"
            'mywebReq.UserAgent = context.Request.ServerVariables.Item("HTTP_USER_AGENT").ToString()
            mywebReq.Timeout = 4000 '4 seconds
            If Not String.IsNullOrEmpty(UserName) Then
                mywebReq.Credentials = New NetworkCredential(UserName, Password)
            End If
            mywebResp = mywebReq.GetResponse()
            Dim SourceStream As System.IO.Stream = mywebResp.GetResponseStream()
            'SourceStream has no ReadAll, so we must read data block-by-block
            'Temporary Buffer and block size
            Dim Buffer(4096) As Byte, BlockSize As Integer
            Do
                BlockSize = SourceStream.Read(Buffer, 0, 4096)
                If BlockSize > 0 Then TempStream.Write(Buffer, 0, BlockSize)
            Loop While BlockSize > 0
            'Dim outStream As System.IO.Stream = System.IO.File.Create(System.IO.Path.GetTempPath.ToString & filename)  'save file
            'TempStream.WriteTo(outStream)
            'outStream.Flush()
            'outStream.Close()
            SourceStream.Close()
            SourceStream.Flush()
            mywebResp.Close()
        Catch ex As Exception
        Finally
        End Try
        Return TempStream.ToArray()  'return the document binary data (if used as function)
    End Function
    Private Function ParseLinks(ByVal HTML As String) As ArrayList
        Dim objRegEx As System.Text.RegularExpressions.Regex
        Dim objMatch As System.Text.RegularExpressions.Match
        Dim arrLinks As New System.Collections.ArrayList()
        ' Create regular expression
        objRegEx = New System.Text.RegularExpressions.Regex( _
            "a.*href\s*=\s*(?:""(?<1>[^""]*)""|(?<1>\S+))", _
            System.Text.RegularExpressions.RegexOptions.IgnoreCase Or _
            System.Text.RegularExpressions.RegexOptions.Compiled)
        ' Match expression to HTML
        objMatch = objRegEx.Match(HTML)
        ' Loop through matches and add <1> to ArrayList
        While objMatch.Success
            Dim strMatch As String
            strMatch = objMatch.Groups(1).ToString
            arrLinks.Add(strMatch)
            objMatch = objMatch.NextMatch()
        End While
        ' Pass back results
        Return arrLinks
    End Function
    '==============================================================end create animated noaa image=====================================================================================================================
End Class
