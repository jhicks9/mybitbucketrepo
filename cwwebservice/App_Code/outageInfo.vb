﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class outageInfo
    Private _logInfo As String = ""
    Private _logData As String = ""
    Private _refreshNow As Boolean = Nothing
    Private _dsOutages As DataSet

    Public Property logInfo As String
        Get
            Return _logInfo
        End Get
        Set(ByVal value As String)
            _logInfo = value
        End Set
    End Property

    Public Property logData As String
        Get
            Return _logData
        End Get
        Set(ByVal value As String)
            _logData = value
        End Set
    End Property

    Public Property refreshNow As Boolean
        Get
            Return _refreshNow
        End Get
        Set(value As Boolean)
            _refreshNow = value
        End Set
    End Property

    Public Property dsOutages As DataSet
        Get
            Return _dsOutages
        End Get
        Set(value As DataSet)
            _dsOutages = value
        End Set
    End Property

    Public Sub New(ByVal upDateTime As String, ByVal logInfo As String, ByVal logData As String, ByVal refreshNow As Boolean, ByVal dsOutages As DataSet)
        Me.logInfo = logInfo
        Me.logData = logData
        Me.refreshNow = refreshNow
        Me.dsOutages = dsOutages
    End Sub
End Class
