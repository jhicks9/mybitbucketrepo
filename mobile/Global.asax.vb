﻿' Note: For instructions on enabling IIS6 or IIS7 classic mode, 
' visit http://go.microsoft.com/?LinkId=9394802

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Shared Sub RegisterGlobalFilters(ByVal filters As GlobalFilterCollection)
        filters.Add(New HandleErrorAttribute())
    End Sub

    Shared Sub RegisterRoutes(ByVal routes As RouteCollection)
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")

        ' MapRoute takes the following parameters, in order:
        ' (1) Route name
        ' (2) URL with parameters
        ' (3) Parameter defaults
        routes.MapRoute(
            "Account", _
            "Account/statementImage/{stkn}", _
            New With {.controller = "Account", .action = "statementImage", .stkn = ""} _
        )
        routes.MapRoute(
            "OutageMap", _
            "Outages/Map/{id}/{amount}", _
            New With {.controller = "Outages", .action = "Map", .id = "", .amount = ""} _
        )
        routes.MapRoute(
            "PaymentLocations", _
            "PLocations/Browse/{pltype}/{plstate}/{page}", _
            New With {.controller = "PLocations", .action = "Browse", .id = "", .pltype = "", .plstate = "", .page = ""} _
        )
        routes.MapRoute( _
            "Default", _
            "{controller}/{action}/{id}", _
            New With {.controller = "Home", .action = "Index", .id = UrlParameter.Optional} _
        )

    End Sub

    Sub Application_Start()
        AreaRegistration.RegisterAllAreas()

        RegisterGlobalFilters(GlobalFilters.Filters)
        RegisterRoutes(RouteTable.Routes)

        'insert mobile view engine -- if mobile device looks for viewname.Mobile.vbhtml
        ViewEngines.Engines.Clear()
        ViewEngines.Engines.Add(New VBRazorViewEngine())
    End Sub
End Class
