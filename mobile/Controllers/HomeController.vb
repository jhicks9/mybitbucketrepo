﻿Imports System.Data.SqlClient

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Dim cwservice As New cw.Service

    Function Index() As ActionResult
        ViewData("Message") = "Welcome to ASP.NET MVC!"

        Return View()
    End Function

    Function About() As ActionResult
        Dim versionInfo As New VersionViewModel
        Try
            versionInfo.version = GetType(MvcApplication).Assembly.GetName.Version.ToString
            versionInfo.builddate = System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly.Location)
        Catch
        End Try
        Return View(versionInfo)
    End Function

    Function StockQuote(Optional ByVal symbol As String = "ede") As ActionResult
        Dim quote As String = cwservice.GetStockQuote(symbol)
        Dim sq As New Global.mobile.StockQuote
        If Not String.IsNullOrEmpty(quote) AndAlso ((quote.Split(",").Length - 1) = 15) Then
            sq.symbol = removeQuotes(quote.Split(",")(0).Trim)
            sq.title = quote.Split(",")(15).Trim
            sq.price = quote.Split(",")(1).Trim
            If quote.Split(",")(4).Trim = "0.00" Then
                sq.trend = "none"
            Else
                If Left(quote.Split(",")(4).Trim, 1) = "-" Then
                    sq.trend = "down"
                Else
                    sq.trend = "up"
                End If
            End If
            sq.change = quote.Split(",")(4).Trim & " (" & removeQuotes(quote.Split(",")(11).Trim) & ")"
            sq.time = removeQuotes(quote.Split(",")(2).Trim) & " " & removeQuotes(quote.Split(",")(3).Trim) & " Eastern"
            sq.previousclose = removeQuotes(quote.Split(",")(10).Trim)
            sq.open = removeQuotes(quote.Split(",")(5).Trim)
            sq.dayrange = removeQuotes(quote.Split(",")(7).Trim) & " - " & removeQuotes(quote.Split(",")(6).Trim)
            sq.yearrange = removeQuotes(quote.Split(",")(12).Trim)
            sq.volume = removeQuotes(quote.Split(",")(8).Trim)
            sq.market = removeQuotes(quote.Split(",")(9).Trim)
        Else
            sq.time = "Not available at this time"
        End If

        Return View(sq)
    End Function

    Public Function removeQuotes(ByVal inputString As String) As String
        Try
            If Left(inputString, 1) = """" Then
                inputString = Right(inputString, inputString.Length - 1)
            End If
            If Right(inputString, 1) = """" Then
                inputString = Left(inputString, inputString.Length - 1)
            End If
            Return inputString
        Catch
            Return inputString
        End Try
    End Function

    '
    ' GET: /Contacts

    Function Contacts() As ActionResult
        Return View()
    End Function
End Class
