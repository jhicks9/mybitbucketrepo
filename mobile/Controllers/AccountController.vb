﻿Imports System.Diagnostics.CodeAnalysis
Imports System.Security.Principal
Imports System.Web.Routing
Imports System.Security.Cryptography
Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Xml.XPath
Imports ibex4
Imports ibex4.logging
Imports mobile.Helpers
Imports System.Data.SqlClient

Public Class AccountController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Account/Index
    <RequireHttps()>
    <Authorize()> _
    Public Function Index() As ActionResult
        Return View()
    End Function

    '
    ' GET: /Account/LogOn
    <RequireHttps()>
    Public Function LogOn() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Account/LogOn
    <RequireHttps()>
    <HttpPost()> _
    Public Function LogOn(ByVal model As LogOnModel, ByVal returnUrl As String) As ActionResult
        If ModelState.IsValid Then
            Dim authUser As Boolean = True
            Dim userInfo As MembershipUser = Membership.GetUser(model.UserName)  'See if this user exists
            If userInfo Is Nothing Then  'The user entered an invalid username
                authUser = False
                ModelState.AddModelError("", "Your login attempt was not successful.")
            Else
                If userInfo.IsLockedOut Then  'See if the user is locked out
                    If Now > userInfo.LastLockoutDate.AddMinutes(2) Then  'if > 30 minutes then unlock account
                        userInfo.UnlockUser()
                        authUser = True
                    Else
                        ModelState.AddModelError("", "Account has been locked. Please try again in 30 minutes.")
                        authUser = False  'account is locked -- do not authenticate
                    End If
                End If

                If authUser Then  'account exists and is not locked -- proceed to authenticate user
                    If Membership.ValidateUser(model.UserName, model.Password) Then
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe)
                        If Url.IsLocalUrl(returnUrl) AndAlso returnUrl.Length > 1 AndAlso returnUrl.StartsWith("/") _
                           AndAlso Not returnUrl.StartsWith("//") AndAlso Not returnUrl.StartsWith("/\\") Then
                            Return Redirect(returnUrl)
                        Else
                            Return RedirectToAction("Index", "Account")
                        End If
                    Else
                        ModelState.AddModelError("", "Your login attempt was not successful.")
                    End If
                End If  ' ******
            End If
        End If

        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    ' GET: /Account/LogOff

    Public Function LogOff() As ActionResult
        FormsAuthentication.SignOut()
        Return RedirectToAction("Index", "Home")
    End Function

    '
    ' GET: /Account/Register
    <RequireHttps()>
    Public Function Register() As ActionResult
        Dim model As New RegisterModel
        Return View(model)
    End Function

    '
    ' POST: /Account/Register
    <RequireHttps()>
    <HttpPost()> _
    Public Function Register(ByVal model As RegisterModel) As ActionResult
        If ModelState.IsValid Then
            Dim RegisterAccountSucceeded As Boolean = False

            Dim cwservice As New cw.Service
            Dim validCustomer As Integer = cwservice.ValidateCustomer(Encrypt(model.custToken), model.billPkgToken, model.name, model.ssnToken)
            '0=Failed,1=Pass,2=SSN Failed
            Select Case validCustomer
                Case 0
                    ModelState.AddModelError("", "Cannot verify account. Please check information.")
                Case 1
                    ' Check if account previously registered
                    Dim previouslyRegistered As Boolean = AccountExists(model.custToken, model.billPkgToken)
                    If previouslyRegistered Then
                        ModelState.AddModelError("", "Online account already registered.")
                    Else
                        ' Attempt to register the user
                        Dim UserNameExists As Boolean = UserExists(model.UserName)  'Check if username already exists
                        If UserNameExists Then
                            ModelState.AddModelError("", String.Format("User Name {0} is already being used.", model.UserName))
                        Else
                            Dim createStatus As MembershipCreateStatus
                            Membership.CreateUser(model.UserName, model.Password, model.Email, Nothing, Nothing, True, Nothing, createStatus)

                            If createStatus = MembershipCreateStatus.Success Then
                                ' add user to roles
                                If Not Roles.IsUserInRole(model.UserName, "Customer") Then
                                    Roles.AddUserToRole(model.UserName, "Customer")
                                End If
                                'add user profile
                                Dim _userProfile As ProfileBase = ProfileBase.Create(model.UserName)
                                _userProfile.SetPropertyValue("CustomerAccountNumber", model.custToken)
                                _userProfile.SetPropertyValue("BillingPackageToken", model.billPkgToken)
                                _userProfile.Save()

                                'add ebilling table info
                                updateEbillingTable(3, model.custToken, model.billPkgToken, model.UserName)

                                'login new registered account
                                FormsAuthentication.SetAuthCookie(model.UserName, False)
                                Return RedirectToAction("EditSuccess", New With {.type = "r"})
                            Else
                                ModelState.AddModelError("", ErrorCodeToString(createStatus))
                            End If  'created account successful
                        End If  'user name already exists
                    End If  'previously registered
                Case 2
                    ModelState.AddModelError("", "Unable to verify account.")
            End Select

            If RegisterAccountSucceeded Then
                Return RedirectToAction("EditSuccess", New With {.type = "r"})
            End If

        End If
        Return View(model)
    End Function

    '
    ' GET: /Account/ChangePassword
    <RequireHttps()>
    <Authorize()> _
    Public Function ChangePassword() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Account/ChangePassword
    <RequireHttps()>
    <Authorize()> _
    <HttpPost()> _
    Public Function ChangePassword(ByVal model As ChangePasswordModel) As ActionResult
        If ModelState.IsValid Then
            Dim changePasswordSucceeded As Boolean
            Try
                Dim currentUser As MembershipUser = Membership.GetUser(User.Identity.Name, True)
                changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword)
            Catch ex As Exception
                changePasswordSucceeded = False
            End Try

            If changePasswordSucceeded Then
                Return RedirectToAction("EditSuccess", New With {.type = "p"})
            Else
                ModelState.AddModelError("", "Password change was unsuccessful.")
            End If
        End If
        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    ' GET: /Account/ResetAccount
    <RequireHttps()>
    Public Function ResetAccount() As ActionResult
        Return View()
    End Function

    '
    ' GET: /Account/RPasswordEmail
    <RequireHttps()>
    Public Function RPasswordEmail() As ActionResult
        Dim model As New RecoverPasswordModel
        Return View(model)
    End Function

    '
    ' Post: /Account/RPasswordEmail
    <RequireHttps()>
    <HttpPost()> _
    Public Function RPasswordEmail(ByVal model As RecoverPasswordModel) As ActionResult
        If ModelState.IsValid Then
            If String.IsNullOrEmpty(model.Email) Then
                ModelState.AddModelError("Email", "required")
            Else
                Dim username As String = Membership.GetUserNameByEmail(model.Email)
                If Len(username) > 0 Then  'valid email address found, send email
                    Dim MailObj As New System.Net.Mail.SmtpClient
                    Dim message As New System.Net.Mail.MailMessage()
                    Dim strEmailBody As String = "We received your request for a MyPassword reset from " & Request.ServerVariables("REMOTE_ADDR") & " on " & Now.ToString & ".  For security reasons, we are unable to provide your old password.  However, you may select a new password by following the directions below." & vbCrLf & vbCrLf
                    strEmailBody = strEmailBody & "**IMPORTANT! If you did not request a MyPassword reset, delete this email. Only follow the instructions if you need to reset your MyPassword." & vbCrLf & vbCrLf
                    strEmailBody = strEmailBody & "To reset your MyPassword, simply click on the following link:" & vbCrLf
                    strEmailBody = strEmailBody & "https://www.empiredistrict.com/RetrievePasswordEmail.aspx?u=" & Server.UrlEncode(Encrypt(username)) & "&e=" & Server.UrlEncode(Encrypt(model.Email))
                    Dim fromAddress As New System.Net.Mail.MailAddress("customer.service@empiredistrict.com")
                    message.From = fromAddress
                    message.To.Add(model.Email)
                    message.Subject = "MyPassword Reset"
                    message.Body = strEmailBody
                    MailObj.Send(message)
                End If
                Return RedirectToAction("EditSuccess", New With {.type = "rpe", .em = model.Email})
            End If
        End If
        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function
    '
    ' GET: /Account/RPassword
    <RequireHttps()>
    Public Function RPassword() As ActionResult
        Dim model As New RecoverPasswordModel
        Return View(model)
    End Function

    '
    ' Post: /Account/RPassword
    <RequireHttps()>
    <HttpPost()> _
    Public Function RPassword(ByVal model As RecoverPasswordModel) As ActionResult
        If ModelState.IsValid Then
            Dim validData As Boolean = True

            If String.IsNullOrEmpty(model.custToken) Then
                validData = False
                ModelState.AddModelError("custToken", "required")
            End If
            If String.IsNullOrEmpty(model.billPkgToken) Then
                validData = False
                ModelState.AddModelError("custToken", "required")
            End If
            If String.IsNullOrEmpty(model.name) Then
                validData = False
                ModelState.AddModelError("name", "required")
            End If
            If String.IsNullOrEmpty(model.ssnToken) Then
                validData = False
                ModelState.AddModelError("ssnToken", "required")
            End If
            If String.IsNullOrEmpty(model.Password) Then
                validData = False
                ModelState.AddModelError("Password", "required")
            End If

            If validData Then
                Dim cwservice As New cw.Service
                Dim validCustomer As Integer = cwservice.ValidateCustomer(Encrypt(model.custToken), model.billPkgToken, model.name, model.ssnToken)
                '0=Failed,1=Pass,2=SSN Failed
                Select Case validCustomer
                    Case 0
                        'cant find customer watch account
                        ModelState.AddModelError("", "Cannot verify account. Please check information.")
                    Case 1
                        Dim userName As String = GetAccount(model.custToken, model.billPkgToken)  ' attempt to retrieve myaccount user
                        If Not String.IsNullOrEmpty(userName) Then 'account exists so attempt password change
                            Dim mUser As MembershipUser = Membership.GetUser(userName)
                            If mUser.IsLockedOut Then  'if account is locked out
                                mUser.UnlockUser()  'unlock account
                            End If
                            'change password
                            mUser.ChangePassword(mUser.GetPassword.ToString, model.Password)
                            Return RedirectToAction("EditSuccess", New With {.type = "p"})
                        Else
                            ModelState.AddModelError("", "Account not registered.")  'account valid in customer watch but not created on web
                        End If
                    Case 2
                        'found customer watch account, but no ssn or taxid on file
                        ModelState.AddModelError("", "Unable to verify account.")
                End Select
            End If
        End If
        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    ' GET: /Account/RUserName
    <RequireHttps()>
    Public Function RUserName() As ActionResult
        Dim model As New RecoverUserNameModel
        Return View(model)
    End Function

    '
    'Post: /Account/RUserName
    <RequireHttps()>
    <HttpPost()> _
    Public Function RUserName(ByVal model As RecoverUserNameModel) As ActionResult
        If ModelState.IsValid Then
            Dim userNameExists As String = GetAccount(model.custToken, model.billPkgToken)
            If Not String.IsNullOrEmpty(userNameExists) Then
                Return RedirectToAction("EditSuccess", New With {.type = "ru", .un = HttpUtility.HtmlEncode(userNameExists)})
            Else
                ModelState.AddModelError("", "Recovering User Name was unsuccessful.")
            End If
        End If
        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    '
    ' GET: /Account/ChangeUserName
    <RequireHttps()>
    <Authorize()> _
    Public Function ChangeUserName() As ActionResult
        Dim model As New ChangeUserNameModel
        Dim currentUser As MembershipUser = Membership.GetUser(User.Identity.Name, True)
        model.currentUserName = currentUser.UserName
        Return View(model)
    End Function

    '
    ' POST: /Account/ChangeUserName
    <RequireHttps()>
    <Authorize()> _
    <AcceptVerbs(HttpVerbs.Post)> _
    Public Function ChangeUserName(ByVal model As ChangeUserNameModel) As ActionResult
        If ModelState.IsValid Then  'validation performed in model
            Dim changeUserNameSucceeded As Boolean = False
            Try
                changeUserNameSucceeded = UpdateUsername(model.currentUserName, model.newUserName)
            Catch ex As Exception
                changeUserNameSucceeded = False
            End Try
            If changeUserNameSucceeded Then
                FormsAuthentication.SetAuthCookie(model.newUserName, False)
                Return RedirectToAction("EditSuccess", New With {.type = "u"})
            Else
                ModelState.AddModelError("", "User Name update was unsuccessful.")
            End If
        End If
        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    Protected Function UpdateUsername(ByVal oldUsername As String, ByVal newUsername As String) As Boolean
        Dim recordExists As Boolean = False
        Dim status As Boolean = False
        Dim userId As System.Guid = Nothing
        Dim applicationId As System.Guid = Nothing
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader

        Try
            conn.Open()
            cmdLookup.CommandText = "select aspnet_Users.UserId, aspnet_Applications.ApplicationId from dbo.aspnet_Users, dbo.aspnet_Applications where aspnet_Users.LoweredUserName = '" & oldUsername.ToLower & "' and aspnet_Users.ApplicationId = aspnet_Applications.ApplicationId"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    userId = drLookup("UserId")
                    applicationId = drLookup("ApplicationId")
                End While
            End If
            drLookup.Close()

            If (Not userId = Nothing) And (Not applicationId = Nothing) Then 'old account exists
                recordExists = UserExists(newUsername.ToLower)
                If recordExists Then  'new account already used
                    status = False
                Else 'make the change
                    cmd.CommandText = "update dbo.aspnet_Users set aspnet_Users.UserName = '" & newUsername & "', aspnet_Users.LoweredUserName = '" & newUsername.ToLower & _
                    "' WHERE aspnet_Users.UserId = '" & userId.ToString & "' and aspnet_Users.ApplicationId = '" & applicationId.ToString & "'"
                    cmd.ExecuteNonQuery()
                    status = True
                End If
            Else  'old account does not exist
                status = False
            End If

            conn.Close()
            Return status
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return False
        End Try
    End Function

    Public Function GetAccount(ByVal customer_tkn As String, ByVal billing_pkg_tkn As String) As String
        Dim connExists As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Try
            Dim userName As String = Nothing
            Dim cmdLookup As New SqlCommand("", connExists)
            Dim drLookup As SqlDataReader

            connExists.Open()
            cmdLookup.CommandText = "select aspnet_Users.UserName AS result from aspnet_Users inner join aspnet_Membership on aspnet_Users.UserId = aspnet_Membership.UserId inner join aspnet_Profile ON aspnet_Users.UserId = aspnet_Profile.UserId where (patindex('" & customer_tkn & billing_pkg_tkn & "', aspnet_Profile.PropertyValuesString) > 0)"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    userName = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            connExists.Close()
            Return userName
        Catch
            If connExists.State <> ConnectionState.Closed Then connExists.Close()
            Return Nothing
        End Try
    End Function
    Public Function UserExists(ByVal username As String) As Boolean
        Dim connExists As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Try
            Dim recordExists As Boolean = False
            Dim cmdLookup As New SqlCommand("", connExists)
            Dim drLookup As SqlDataReader

            connExists.Open()
            cmdLookup.CommandText = "select count(*) as result from dbo.aspnet_Users, dbo.aspnet_Applications where aspnet_Users.LoweredUserName = '" & username.ToLower & "' and aspnet_Users.ApplicationId = aspnet_Applications.ApplicationId"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            connExists.Close()
            Return recordExists
        Catch
            If connExists.State <> ConnectionState.Closed Then connExists.Close()
            Return False
        End Try
    End Function

    Public Function AccountExists(ByVal customer_tkn As String, ByVal billing_pkg_tkn As String) As Boolean
        Dim connExists As New SqlConnection(ConfigurationManager.ConnectionStrings("empiremembershipConnectionString").ConnectionString)
        Try
            Dim recordExists As Boolean = False
            Dim cmdLookup As New SqlCommand("", connExists)
            Dim drLookup As SqlDataReader

            connExists.Open()
            cmdLookup.CommandText = "select count(*) AS result from aspnet_Users inner join aspnet_Membership on aspnet_Users.UserId = aspnet_Membership.UserId inner join aspnet_Profile ON aspnet_Users.UserId = aspnet_Profile.UserId where (patindex('" & customer_tkn & billing_pkg_tkn & "', aspnet_Profile.PropertyValuesString) > 0)"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            connExists.Close()
            Return recordExists
        Catch
            If connExists.State <> ConnectionState.Closed Then connExists.Close()
            Return False
        End Try
    End Function


    '
    ' GET: /Account/ChangeEmail
    <RequireHttps()>
    <Authorize()> _
    Public Function ChangeEmail() As ActionResult
        Dim model As New ChangeEmailModel
        model.currentEmail = syncEmail()
        Return View(model)
    End Function

    '
    ' POST: /Account/ChangeEmail
    <RequireHttps()>
    <Authorize()> _
    <AcceptVerbs(HttpVerbs.Post)> _
    Public Function ChangeEmail(ByVal model As ChangeEmailModel) As ActionResult
        If ModelState.IsValid Then  'validation performed in model
            Dim changeEmailSucceeded As Boolean = False
            Try
                Dim currentUser As MembershipUser = Membership.GetUser(User.Identity.Name, True)
                currentUser.Email = model.newEmail
                Membership.UpdateUser(currentUser)
                updateEbillingTable(3) 'build database entry for customerwatch
                changeEmailSucceeded = True
            Catch ex As Exception
                changeEmailSucceeded = False
            End Try
            If changeEmailSucceeded Then
                Return RedirectToAction("EditSuccess", New With {.type = "e"})
            Else
                ModelState.AddModelError("", "Email update was unsuccessful.")
            End If
        End If
        ' If we got this far, something failed, redisplay form
        Return View(model)
    End Function

    Public Function syncEmail() As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim cwEmail As String = ""
        Dim msEmail As String = ""
        Dim pendingEmail As String = ""
        Dim email As String = ""
        Dim cwservice As New cw.Service
        Try
            Dim custTokens As New CWTokensModel
            custTokens = getCustomerTokens()
            'get email from membership services
            msEmail = Membership.GetUser(User.Identity.Name).Email
            email = msEmail  'set display email address

            'get email from customer watch
            cwEmail = cwservice.GetEmailAddress(Encrypt(custTokens.customer_tkn), custTokens.billing_pkg_tkn)

            'get for any "pending" email changes
            conn.Open()
            cmdLookup.CommandText = "SELECT ebilling.email from ebilling where ebilling.customer_tkn = '" & custTokens.customer_tkn & "' and ebilling.billing_pkg_tkn = '" & custTokens.billing_pkg_tkn & "' and status=1"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    pendingEmail = drLookup("email").ToString
                End While
                email = pendingEmail  'display current email address  -- currently what is in membership services AND pending
            Else 'if there are no pending email changes
                'sync membership services with customer watch if the emails do not match
                If (Len(cwEmail) > 0) Then  'if customer watch has email address
                    If (cwEmail <> msEmail) Then  'if customer watch does not match membership services
                        Dim u As MembershipUser
                        u = Membership.GetUser(User.Identity.Name)
                        u.Email = cwEmail
                        Membership.UpdateUser(u)
                    End If
                    email = cwEmail  'display current email address  -- currently on file in customer watch
                End If
            End If
            drLookup.Close()
            conn.Close()
            Return email
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
            Return email
        End Try
    End Function

    Public Sub updateEbillingTable(ByVal actioncode As Integer, Optional customer_tkn As String = "", Optional billing_pkg_tkn As String = "", Optional ByVal username As String = "") 'build ebilling table data for customer watch
        Dim recordExists As Boolean = False
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("documentsConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("", conn)
        Dim cmdLookup As New SqlCommand("", conn)
        Dim drLookup As SqlDataReader
        Dim comment As String = ""
        Dim emailAddress As String = ""

        Try
            Dim custTokens As New CWTokensModel
            If (customer_tkn.Length > 0) And (billing_pkg_tkn.Length > 0) Then
                'use passed in tokens (when registering new accounts)
                custTokens.customer_tkn = customer_tkn
                custTokens.billing_pkg_tkn = billing_pkg_tkn
            Else
                'lookup tokens (when updating previously registered accounts)
                custTokens = getCustomerTokens()
            End If
            If (username.Length > 0) Then
                'use passed in username (when registering new accounts)
                emailAddress = Membership.GetUser(username).Email
            Else
                'lookup email (when updating previously registered accounts)
                emailAddress = Membership.GetUser(User.Identity.Name).Email
            End If

            Select Case actioncode
                Case 1
                    comment = "activate ebilling"
                Case 2
                    comment = "opt-out ebilling"
                Case 3
                    comment = "update email"
            End Select

            Dim insertquery As String = "insert into ebilling(customer_tkn,billing_pkg_tkn,email,status,actioncode,comment,lastupdate) " & _
            "values('" & custTokens.customer_tkn & "','" & custTokens.billing_pkg_tkn & "','" & emailAddress & "'," & 1 & "," & actioncode & ",'" & comment & "','" & Now & "')"
            Dim updatequery As String = "update ebilling set ebilling.status=1,ebilling.actioncode=" & actioncode & ",ebilling.email='" & emailAddress & "',ebilling.lastupdate='" & Now & "',ebilling.comment='" & comment & "' where ebilling.customer_tkn = '" & custTokens.customer_tkn & "' and ebilling.billing_pkg_tkn = '" & custTokens.billing_pkg_tkn & "'"

            conn.Open()  'add record if it doesnt exist, otherwise update record
            cmdLookup.CommandText = "SELECT count(*) as result from ebilling where ebilling.customer_tkn = '" & custTokens.customer_tkn & "' and ebilling.billing_pkg_tkn = '" & custTokens.billing_pkg_tkn & "'"
            drLookup = cmdLookup.ExecuteReader
            If drLookup.HasRows Then
                While drLookup.Read
                    recordExists = drLookup("result").ToString
                End While
            End If
            drLookup.Close()
            If recordExists Then
                cmd.CommandText = updatequery
            Else
                cmd.CommandText = insertquery
            End If
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End Try
    End Sub

    '
    ' GET: /Account/EditSuccess

    Public Function EditSuccess() As ActionResult
        Try
            Dim model As New AccountUpdateType
            Select Case Request.QueryString("type")
                Case "u"
                    model.type = "User Name"
                    model.description = String.Format("Your {0} has been updated successfully.", model.type)
                Case "e"
                    model.type = "Email"
                    model.description = String.Format("Your {0} has been updated successfully.", model.type)
                Case "p"
                    model.type = "Password"
                    model.description = String.Format("Your {0} has been updated successfully.", model.type)
                Case "r"
                    model.type = "account"
                    model.description = String.Format("Your {0} has been created successfully.", model.type)
                Case "ru"
                    model.type = "User Name"
                    model.description = String.Format("Your user name is: {0}.", Request.QueryString("un"))
                Case "rpe"
                    model.type = "Password"
                    model.description = String.Format("An email has been sent to {0}. Please follow the instructions in the email to reset MyPassword.", Request.QueryString("em"))
                Case Else
                    model.type = "unknown"
            End Select
            Return View(model)
        Catch
            Return View()
        End Try
    End Function

    '
    'GET: /Account/Payments
    <RequireHttps()>
    <Authorize(Roles:="Customer")> _
    Public Function Payments(ByVal page As System.Nullable(Of Integer)) As ActionResult
        Try
            Dim cwservice As New cw.Service
            Dim paymentsViewModel As New PaymentsViewModel
            Dim paymentInfoModel As New List(Of PaymentsModel)

            'get account information
            Dim custTokens As New CWTokensModel
            custTokens = getCustomerTokens(Request.QueryString("acct"))

            'load payment info model
            Dim ds As New DataSet
            ds = cwservice.GetPaymentInfo(Encrypt(custTokens.customer_tkn), custTokens.billing_pkg_tkn, 825)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim payment As New PaymentsModel
                payment.paymentamount = ds.Tables(0).Rows(i)("paymentamount").ToString()
                payment.paymentdate = ds.Tables(0).Rows(i)("paymentdate").ToString()
                paymentInfoModel.Add(payment)
            Next

            Const pageSize As Integer = 6
            Dim paginatedPayments = New PaginatedList(Of PaymentsModel)(paymentInfoModel.AsQueryable, If(page, 0), pageSize)
            paymentsViewModel.paymentinfo = paginatedPayments

            'load amount due
            paymentsViewModel.paymentdue = cwservice.GetCurrentDue(Encrypt(custTokens.customer_tkn), custTokens.billing_pkg_tkn)
            'load account tokens
            paymentsViewModel.accountnumber = custTokens.customer_tkn & "-" & custTokens.billing_pkg_tkn & "-" & custTokens.cdToken
            Return View(paymentsViewModel)
        Catch
            Return View()
        End Try
    End Function

    '
    'GET: /Accounts/Usage
    <RequireHttps()>
    <Authorize(Roles:="Customer")> _
    Public Function Usage(ByVal page As System.Nullable(Of Integer)) As ActionResult
        Try
            Dim cwservice As New cw.Service
            Dim UsageViewModel As New UsageViewModel
            Dim UsageAccountPackageModel As New List(Of CWTokensModel)
            Dim UsageInfoModel As New List(Of UsageModel)
            Dim buildAccountPackage As Boolean = False

            'get account information
            Dim custTokens As New CWTokensModel
            custTokens = getCustomerTokens(Request.QueryString("acct"))

            If Request.QueryString("actpkg") Is Nothing Then
                buildAccountPackage = True  'need to build the accountpackage list
            Else
                If ((Request.QueryString("actpkg").Split("-").Length - 1) = 6) Then
                    'build usage info
                    Dim acctTokens As Array = Split(Request.QueryString("actpkg"), "-")
                    Dim ds As New DataSet
                    ds = cwservice.GetUsageInfo(Encrypt(acctTokens(0)), acctTokens(1), acctTokens(2), "396", acctTokens(3), acctTokens(4), acctTokens(5))
                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                        Dim loadData As New UsageModel
                        loadData.read_date = FormatDateTime(ds.Tables(0).Rows(i)("read_date").ToString(), DateFormat.ShortDate)
                        loadData.usage_num = ds.Tables(0).Rows(i)("usage_num").ToString()
                        loadData.usage_read_num = ds.Tables(0).Rows(i)("usage_read_num").ToString()
                        Select Case acctTokens(6)
                            Case "E"
                                loadData.uom = "KWH"
                            Case "W"
                                loadData.uom = "KGAL"
                            Case "G"
                                loadData.uom = "CCF"
                            Case Else
                                loadData.uom = ""
                        End Select
                        loadData.estimate_ind = ds.Tables(0).Rows(i)("estimate_ind").ToString()
                        loadData.usage_days_cnt = ds.Tables(0).Rows(i)("usage_days_cnt").ToString()
                        loadData.demand_read = ds.Tables(0).Rows(i)("demand_read").ToString()
                        loadData.demand_billed = ds.Tables(0).Rows(i)("demand_billed").ToString()
                        'loadData.billing_date = ds.Tables(0).Rows(i)("billing_date").ToString()
                        'loadData.total_bill = ds.Tables(0).Rows(i)("total_bill").ToString()
                        'loadData.usage_charge = ds.Tables(0).Rows(i)("usage_charge").ToString()
                        'loadData.actual_charge = ds.Tables(0).Rows(i)("actual_charge").ToString()
                        UsageInfoModel.Add(loadData)
                    Next
                    buildAccountPackage = False
                Else   'bad querystring so rebuild accountpackages
                    buildAccountPackage = True
                End If
            End If

            If buildAccountPackage Then
                Dim selectPackage As New Regex("Electric|Gas|Water", RegexOptions.IgnoreCase)
                'build account package list
                Dim ds As New DataSet
                ds = cwservice.GetAccountTokens(Encrypt(custTokens.customer_tkn), custTokens.billing_pkg_tkn)
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim loadData As New CWTokensModel
                    loadData.customer_acct_tkn = ds.Tables(0).Rows(i)("customer_acct_tkn").ToString()
                    loadData.customer_tkn = custTokens.customer_tkn
                    loadData.account_pkg_tkn = ZeroPad(ds.Tables(0).Rows(i)("account_pkg_tkn").ToString(), 3)
                    loadData.srvce_pnt_assn_tkn = ds.Tables(0).Rows(i)("srvce_pnt_assn_tkn").ToString()
                    loadData.account_pkg_desc = ds.Tables(0).Rows(i)("account_pkg_desc").ToString()
                    loadData.ln1_addr = ds.Tables(0).Rows(i)("ln1_addr").ToString()
                    loadData.city_name = ds.Tables(0).Rows(i)("city_name").ToString()
                    loadData.state_code = ds.Tables(0).Rows(i)("state_code").ToString()
                    loadData.service_point_tkn = ds.Tables(0).Rows(i)("service_point_tkn").ToString()
                    loadData.premise_tkn = ds.Tables(0).Rows(i)("premise_tkn").ToString()
                    If selectPackage.IsMatch(loadData.account_pkg_desc) Then 'only load packages for electric, gas, or water
                        UsageAccountPackageModel.Add(loadData)
                    End If
                Next
            End If

            UsageViewModel.accountnumber = custTokens.customer_tkn & "-" & custTokens.billing_pkg_tkn & "-" & custTokens.cdToken
            UsageViewModel.accountpackage = Request.QueryString("actpkg")
            UsageViewModel.accountpackageinfo = UsageAccountPackageModel
            'UsageViewModel.usageinfo = UsageInfoModel
            Const pageSize As Integer = 6
            Dim paginatedUsage = New PaginatedList(Of UsageModel)(UsageInfoModel.AsQueryable, If(page, 0), pageSize)
            UsageViewModel.usageinfo = paginatedUsage
            Return View(UsageViewModel)
        Catch
            Return View()
        End Try
    End Function

    '
    'GET: /Account/Statements
    <RequireHttps()>
    <Authorize(Roles:="Customer")> _
    Public Function Statements() As ActionResult
        Try
            Dim cwservice As New cw.Service
            Dim statmentsViewModel As New StatementsViewModel
            Dim statementInfoModel As New List(Of StatementsModel)
            Dim rtnStatement As String = ""
            Dim totalStatementAmount As String = ""

            'get account information
            Dim custTokens As New CWTokensModel
            custTokens = getCustomerTokens(Request.QueryString("acct"))

            'load statement info model
            Dim ds As New DataSet
            ds = cwservice.GetBillInfo(Encrypt(custTokens.customer_tkn), custTokens.billing_pkg_tkn, "90")
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim loadData As New StatementsModel
                loadData.statementdate = ds.Tables(0).Rows(i)("stmt_date").ToString()
                'load amount due and due date
                rtnStatement = cwservice.GetCustomerChargeInfo(Encrypt(ds.Tables(0).Rows(i)("cust_statement_tkn").ToString()))
                If Len(rtnStatement) > 0 And InStr(rtnStatement, ";") > 0 Then  'valid statement info
                    totalStatementAmount = rtnStatement.Split(";")(0).Trim
                    If Strings.Left(totalStatementAmount, 1) = "<" And Strings.Right(totalStatementAmount, 1) = ">" Then  'clearly show credit on amount
                        totalStatementAmount += " CR"
                    End If
                    loadData.amountdue = totalStatementAmount
                    loadData.delinquentdate = rtnStatement.Split(";")(1).Trim
                    loadData.statementToken = ds.Tables(0).Rows(i)("cust_statement_tkn").ToString()
                Else
                    loadData.amountdue = ""
                    loadData.delinquentdate = ""
                End If
                statementInfoModel.Add(loadData)
            Next
            statmentsViewModel.statementinfo = statementInfoModel

            'load account tokens
            statmentsViewModel.accountnumber = custTokens.customer_tkn & "-" & custTokens.billing_pkg_tkn & "-" & custTokens.cdToken
            'End If
            Return View(statmentsViewModel)
        Catch
            Return View()
        End Try
    End Function

    '
    'GET: /Account/ListAccounts
    <RequireHttps()>
    <Authorize(Roles:="Customer")> _
    Public Function ListAccounts() As ActionResult
        'get account info
        Try
            Dim cwservice As New cw.Service
            Dim custTokens As New CWTokensModel
            custTokens = getCustomerTokens()
            'get billing package tokens from web service
            Dim rtnBillPackages As String = cwservice.GetBillPackage(Encrypt(custTokens.customer_tkn))
            Dim billPkgTkn As Array = Split(ZeroPad(rtnBillPackages, 2), ",")
            'build model
            Dim billingPackages As New List(Of BillingPackagesModel)
            For i = 0 To (billPkgTkn.Length - 1)
                Dim loadData As New BillingPackagesModel
                loadData.customer_tkn = ZeroPad(custTokens.customer_tkn, 6)
                loadData.billing_pkg_tkn = ZeroPad(billPkgTkn(i).ToString, 2)
                loadData.checkdigit_tkn = CalculateCheckDigit(ZeroPad(custTokens.customer_tkn, 6) & ZeroPad(billPkgTkn(i).ToString, 2))
                billingPackages.Add(loadData)
            Next
            Return View(billingPackages)
        Catch
            Return View()
        End Try
    End Function

    '
    'GET: /Account/statementImage/5
    <RequireHttps()>
    Public Function statementImage(ByVal stkn As String) As ActionResult
        Dim pdfStream As New MemoryStream
        Try
            Dim cwservice As New cw.Service
            Dim rtnStatement As String = cwservice.GetStatement(Encrypt(stkn), "894")

            Dim rtnValueArray() As String = Split(rtnStatement, "-,-")
            If rtnValueArray(0) = "Error" Then
            Else
                'create the FO using xml/xsl
                Dim rs As XmlReaderSettings = New XmlReaderSettings()
                Dim xslt As XslCompiledTransform = New XslCompiledTransform()
                rs.DtdProcessing = DtdProcessing.Parse
                rs.IgnoreWhitespace = True
                Dim x As XmlDocument = New XmlDocument
                x.LoadXml(rtnValueArray(1))
                Dim xmlNode As XmlNodeReader = New XmlNodeReader(x)
                xslt.Load(System.Xml.XmlReader.Create(xmlNode, rs))
                Dim xmldoc As XmlDocument = New XmlDocument
                xmldoc.LoadXml(rtnValueArray(0))
                Dim xmlNode2 As XmlNodeReader = New XmlNodeReader(xmldoc)
                Dim foStream As New System.IO.MemoryStream
                xslt.Transform(System.Xml.XmlReader.Create(xmlNode2, rs), Nothing, foStream)
                'IBEX ***************** create pdf using generated FO
                Dim key As String = "0002008082310A491433269D1E1B5F2ALLR/YAXSSIRD03WS9XZ5AG=="
                ibex4.licensing.Generator.setRuntimeKey(key)
                Dim doc As New FODocument
                Using (foStream)
                    doc.generate(foStream, pdfStream, False)
                End Using
                foStream.Close()
                foStream.Dispose()
                'end Ibex ************************

                'Dim fs As New IO.FileStream("u:\temp\test.pdf", IO.FileMode.CreateNew)  'create file copy
                'pdfStream.WriteTo(fs)
                'fs.Close()

                'Dim byteInfo As Byte() = pdfStream.ToArray()
                'pdfStream.Write(byteInfo, 0, byteInfo.Length)
                pdfStream.Position = 0
            End If
        Catch
        End Try
        Return New FileStreamResult(pdfStream, "application/pdf") 'File(pdfStream, "application/pdf")
    End Function

#Region "Status Code"
    Public Function ErrorCodeToString(ByVal createStatus As MembershipCreateStatus) As String
        ' See http://go.microsoft.com/fwlink/?LinkID=177550 for
        ' a full list of status codes.
        Select Case createStatus
            Case MembershipCreateStatus.DuplicateUserName
                Return "User name already exists. Please enter a different user name."

            Case MembershipCreateStatus.DuplicateEmail
                Return "A user name for that e-mail address already exists. Please enter a different e-mail address."

            Case MembershipCreateStatus.InvalidPassword
                Return "The password provided is invalid. Please enter a valid password value."

            Case MembershipCreateStatus.InvalidEmail
                Return "The e-mail address provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidAnswer
                Return "The password retrieval answer provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidQuestion
                Return "The password retrieval question provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.InvalidUserName
                Return "The user name provided is invalid. Please check the value and try again."

            Case MembershipCreateStatus.ProviderError
                Return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator."

            Case MembershipCreateStatus.UserRejected
                Return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator."

            Case Else
                Return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator."
        End Select
    End Function
#End Region

    Function getCustomerTokens(Optional ByVal acct As String = "") As CWTokensModel
        ' retrieves customer token, billing package token, and calculates the checkdigit.
        Dim cwTokens As New CWTokensModel
        Try
            If acct Is Nothing Then acct = ""
            Dim count As Integer = acct.Split("-").Length - 1
            If count = 2 Then 'use account number from passed value -- used for looking up secondary billptoken(s)
                cwTokens.customer_tkn = Split(acct, "-")(0)
                cwTokens.billing_pkg_tkn = Split(Request.QueryString("acct"), "-")(1)
                cwTokens.cdToken = Split(Request.QueryString("acct"), "-")(2)
            Else  'not valid account number passed in -- use account number from membership services
                Dim _userProfile As ProfileBase = ProfileBase.Create(HttpContext.User.Identity.Name)
                cwTokens.customer_tkn = Convert.ToString(_userProfile.GetPropertyValue("CustomerAccountNumber"))
                cwTokens.billing_pkg_tkn = Convert.ToString(_userProfile.GetPropertyValue("BillingPackageToken"))
                cwTokens.cdToken = CalculateCheckDigit(ZeroPad(cwTokens.customer_tkn, 6) & ZeroPad(cwTokens.billing_pkg_tkn, 2))
            End If
            Return (cwTokens)
        Catch
            Return Nothing
        End Try

    End Function

    Function ZeroPad(ByVal inputStr As String, ByVal reqLength As Integer) As String
        Try
            Dim output As String = ""
            Dim requiredZeros As Integer = 0

            requiredZeros = reqLength - inputStr.Length
            For i = 1 To requiredZeros
                output &= "0"
            Next
            output &= inputStr
            Return output
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Function CalculateCheckDigit(ByVal acct As String) As String
        Try
            Dim sum As Integer = 0
            Dim digit As Integer = 0
            Dim number As Integer = 0
            Dim chrArray() As Char

            chrArray = acct.ToCharArray
            For intCounter = (chrArray.Length - 1) To 0 Step -1 'Iterate through the string in reverse.
                digit = Val(chrArray(intCounter)) 'convert to integer
                number = (((chrArray.Length - 1) - intCounter) Mod 2)
                If number = 0 Then
                    number = 2  'if character position is even
                Else
                    number = 1  'if character position is odd
                End If
                sum += Math.Floor((digit * number) / 10)
                sum += ((digit * number) Mod 10)
            Next

            Dim ckdigit As Integer = sum Mod 10
            If ckdigit = 0 Then
                Return 0
            Else
                Return (10 - ckdigit).ToString
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Const sKey As String = "++XIiGymvbg=_kZ&%($\3=wx"
    Function Encrypt(ByVal sIn As String) As String
        Dim DES As New TripleDESCryptoServiceProvider()
        Dim hashMD5 As New MD5CryptoServiceProvider()

        ' Compute the MD5 hash.
        DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey))
        ' Set the cipher mode.
        DES.Mode = CipherMode.ECB
        ' Create the encryptor.
        Dim DESEncrypt As ICryptoTransform = DES.CreateEncryptor()
        ' Get a byte array of the string.
        Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(sIn)
        ' Transform and return the string.
        Return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))
    End Function
    Function Decrypt(ByVal sOut As String) As String
        Dim DES As New TripleDESCryptoServiceProvider()
        Dim hashMD5 As New MD5CryptoServiceProvider()

        ' Compute the MD5 hash.
        DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey))
        ' Set the cipher mode.
        DES.Mode = CipherMode.ECB
        ' Create the decryptor.
        Dim DESDecrypt As ICryptoTransform = DES.CreateDecryptor()
        Dim Buffer As Byte() = Convert.FromBase64String(sOut)
        ' Transform and return the string.
        Return System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))
    End Function
End Class
