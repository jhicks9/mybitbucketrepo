﻿Imports System.Web.Mvc

Public Class VBRazorViewEngine
    Inherits RazorViewEngine

    Public Overrides Function FindView(controllerContext As ControllerContext, viewName As String, masterName As String, useCache As Boolean) As ViewEngineResult
        Dim result As ViewEngineResult = Nothing
        Dim request = controllerContext.HttpContext.Request
        Dim gv As New Helpers.globalVal

        If gv.IsMobileBrowser = True Then
            result = MyBase.FindView(controllerContext, viewName & ".Mobile", masterName, useCache)
        End If

        If result Is Nothing OrElse result.View Is Nothing Then  'Fall back to desktop view if no other view has been selected 
            result = MyBase.FindView(controllerContext, viewName, masterName, useCache)
        End If
        Return result
    End Function
End Class
