﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("mobile")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Empire District Electric")> 
<Assembly: AssemblyProduct("mobile")> 
<Assembly: AssemblyCopyright("Copyright © Empire District Electric 2013")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

' The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("d14a400c-ed9b-4e94-940a-f47e99e4dd63")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.5.3.0")> 
<Assembly: AssemblyFileVersion("1.5.3.0")> 
