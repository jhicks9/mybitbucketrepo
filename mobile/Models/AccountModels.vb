﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization

Public Class RecoverPasswordModel
    Implements IValidatableObject

    Private custTokenValue As String
    Private billPkgTokenValue As String
    Private chkDigitTokenValue As String
    Private nameValue As String
    Private ssnTokenValue As String
    Private passwordValue As String
    Private confirmPasswordValue As String
    Private emailValue As String
    Private confirmEmailValue As String

    <Display(Name:="Account")> _
    Public Property custToken() As String
        Get
            Return custTokenValue
        End Get
        Set(ByVal value As String)
            custTokenValue = value
        End Set
    End Property

    Public Property billPkgToken() As String
        Get
            Return billPkgTokenValue
        End Get
        Set(ByVal value As String)
            billPkgTokenValue = value
        End Set
    End Property

    Public Property chkDigitToken() As String
        Get
            Return chkDigitTokenValue
        End Get
        Set(ByVal value As String)
            chkDigitTokenValue = value
        End Set
    End Property

    <Display(Name:="Name")> _
    Public Property name() As String
        Get
            Return nameValue
        End Get
        Set(ByVal value As String)
            nameValue = value
        End Set
    End Property

    <Display(name:="SSN")> _
    Public Property ssnToken() As String
        Get
            Return ssnTokenValue
        End Get
        Set(ByVal value As String)
            ssnTokenValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    <Display(name:="Password")> _
    Public Property Password() As String
        Get
            Return passwordValue
        End Get
        Set(ByVal value As String)
            passwordValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    <Display(name:="Confirm password")> _
    Public Property ConfirmPassword() As String
        Get
            Return confirmPasswordValue
        End Get
        Set(ByVal value As String)
            confirmPasswordValue = value
        End Set
    End Property

    <Display(Name:="Email")> _
    Public Property Email() As String
        Get
            Return emailValue
        End Get
        Set(ByVal value As String)
            emailValue = value
        End Set
    End Property

    <Display(name:="Confirm Email")> _
    Public Property confirmEmail() As String
        Get
            Return confirmEmailValue
        End Get
        Set(ByVal value As String)
            confirmEmailValue = value
        End Set
    End Property

    Public Function Validate(ByVal validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim results As List(Of ValidationResult) = New List(Of ValidationResult)
        'required data validation performed in controller as model is used multiple ways via multiple views

        If Not String.IsNullOrEmpty(Password) Then
            If Password.Length > 0 AndAlso Not (Password = ConfirmPassword) Then
                results.Add(New ValidationResult("Passwords do not match", New String() {"Password"}))
            Else
                If (Password.Length > 0 AndAlso Not Regex.IsMatch(Password, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*")) Then
                    results.Add(New ValidationResult("Passwords are required to be at least 8 characters long including one special character (for example: john_123).", New String() {"Password"}))
                End If
            End If
        End If

        If Not String.IsNullOrEmpty(ssnToken) Then
            If ssnToken.Length > 4 Then
                results.Add(New ValidationResult("last four digits of SSN only", New String() {"ssnToken"}))
            End If
        End If

        If Not String.IsNullOrEmpty(Email) Then
            If Email.Length > 0 AndAlso Not (Email = confirmEmail) Then
                results.Add(New ValidationResult("Emails do not match", New String() {"Email"}))
            Else
                If (Email.Length > 0 AndAlso Not Regex.IsMatch(Email, "^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")) Then
                    results.Add(New ValidationResult("Invalid email address.", New String() {"Email"}))
                End If
            End If
        End If
        Return results
    End Function
End Class
Public Class RecoverUserNameModel
    Implements IValidatableObject
    Private custTokenValue As String
    Private billPkgTokenValue As String
    Private chkDigitTokenValue As String

    <Display(Name:="Account")> _
    Public Property custToken() As String
        Get
            Return custTokenValue
        End Get
        Set(ByVal value As String)
            custTokenValue = value
        End Set
    End Property

    Public Property billPkgToken() As String
        Get
            Return billPkgTokenValue
        End Get
        Set(ByVal value As String)
            billPkgTokenValue = value
        End Set
    End Property

    Public Property chkDigitToken() As String
        Get
            Return chkDigitTokenValue
        End Get
        Set(ByVal value As String)
            chkDigitTokenValue = value
        End Set
    End Property

    Public Function Validate(ByVal validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim results As List(Of ValidationResult) = New List(Of ValidationResult)
        If custToken = Nothing Or billPkgToken = Nothing Or chkDigitToken = Nothing Then
            results.Add(New ValidationResult("required", New String() {"custToken"}))
        End If
        Return results
    End Function
End Class

Public Class ChangeUserNameModel
    Implements IValidatableObject

    Private currentUserNameValue As String
    Private newUserNameValue As String

    Public Property currentUserName() As String
        Get
            Return currentUserNameValue
        End Get
        Set(ByVal value As String)
            currentUserNameValue = value
        End Set
    End Property
    Public Property newUserName() As String
        Get
            Return newUserNameValue
        End Get
        Set(ByVal value As String)
            newUserNameValue = value
        End Set
    End Property

    Public Function Validate(ByVal validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim results As List(Of ValidationResult) = New List(Of ValidationResult)

        If newUserName = Nothing Then  'username required
            results.Add(New ValidationResult("required", New String() {"newUserName"}))
        Else
            If newUserName.Length > 12 Then  'username 12 digits or less
                results.Add(New ValidationResult("User Name too long.", New String() {"newUserName"}))
            ElseIf newUserName = currentUserName Then  'username not same as current username
                results.Add(New ValidationResult("User Name is the same.", New String() {"newUserName"}))
            Else
                Dim usr As MembershipUser = Membership.GetUser(newUserName)
                If usr IsNot Nothing Then  'username not already in use
                    results.Add(New ValidationResult(String.Format("User Name {0} is already being used.", newUserName), New String() {"newUserName"}))
                End If
            End If
        End If
        Return results
    End Function
End Class

Public Class ChangeEmailModel
    Implements IValidatableObject

    Private currentEmailValue As String
    Private newEmailValue As String

    Public Property currentEmail() As String
        Get
            Return currentEmailValue
        End Get
        Set(ByVal value As String)
            currentEmailValue = value
        End Set
    End Property
    Public Property newEmail() As String
        Get
            Return newEmailValue
        End Get
        Set(ByVal value As String)
            newEmailValue = value
        End Set
    End Property

    Public Function Validate(ByVal validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim results As List(Of ValidationResult) = New List(Of ValidationResult)

        If newEmail = Nothing Then
            results.Add(New ValidationResult("required", New String() {"newEmail"}))
        Else
            If newEmail = currentEmail Then
                results.Add(New ValidationResult("Your current email and the new email entered are the same.", New String() {"newEmail"}))
            End If
            If (newEmail.Length > 0 AndAlso Not Regex.IsMatch(newEmail, "^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")) Then
                results.Add(New ValidationResult("Invalid email address.", New String() {"newEmail"}))
            End If
        End If
        Return results
    End Function
End Class

Public Class ChangePasswordModel
    Implements IValidatableObject

    Private oldPasswordValue As String
    Private newPasswordValue As String
    Private confirmPasswordValue As String

    <DataType(DataType.Password)> _
    Public Property OldPassword() As String
        Get
            Return oldPasswordValue
        End Get
        Set(ByVal value As String)
            oldPasswordValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    Public Property NewPassword() As String
        Get
            Return newPasswordValue
        End Get
        Set(ByVal value As String)
            newPasswordValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    <Display(Name:="Confirm new password")> _
    <Compare("NewPassword", ErrorMessage:="The new password and confirmation password do not match.")> _
    Public Property ConfirmPassword() As String
        Get
            Return confirmPasswordValue
        End Get
        Set(ByVal value As String)
            confirmPasswordValue = value
        End Set
    End Property

    Public Function Validate(ByVal validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim results As List(Of ValidationResult) = New List(Of ValidationResult)

        If OldPassword = Nothing Then
            results.Add(New ValidationResult("required", New String() {"oldPassword"}))
        End If
        If NewPassword = Nothing Then
            results.Add(New ValidationResult("required.", New String() {"newPassword"}))
        End If

        If results.Count = 0 Then
            If (NewPassword.Length > 0 AndAlso Not Regex.IsMatch(NewPassword, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*")) Then
                results.Add(New ValidationResult("Passwords are required to be at least 8 characters long including one special character (for example: john_123).", New String() {"newPassword"}))
            End If
        End If
        Return results
    End Function
End Class

Public Class LogOnModel
    Private userNameValue As String
    Private passwordValue As String
    Private rememberMeValue As Boolean

    <Required()> _
    <Display(Name:="User Name")> _
    Public Property UserName() As String
        Get
            Return userNameValue
        End Get
        Set(ByVal value As String)
            userNameValue = value
        End Set
    End Property

    <Required()> _
    <DataType(DataType.Password)> _
    <Display(Name:="Password")> _
    Public Property Password() As String
        Get
            Return passwordValue
        End Get
        Set(ByVal value As String)
            passwordValue = value
        End Set
    End Property

    <Display(Name:="Remember me?")> _
    Public Property RememberMe() As Boolean
        Get
            Return rememberMeValue
        End Get
        Set(ByVal value As Boolean)
            rememberMeValue = value
        End Set
    End Property
End Class

Public Class RegisterModel
    Implements IValidatableObject

    Private custTokenValue As String
    Private billPkgTokenValue As String
    Private chkDigitTokenValue As String
    Private nameValue As String
    Private ssnTokenValue As String

    Private userNameValue As String
    Private passwordValue As String
    Private confirmPasswordValue As String
    Private emailValue As String

    <Display(Name:="Account")> _
    Public Property custToken() As String
        Get
            Return custTokenValue
        End Get
        Set(ByVal value As String)
            custTokenValue = value
        End Set
    End Property

    Public Property billPkgToken() As String
        Get
            Return billPkgTokenValue
        End Get
        Set(ByVal value As String)
            billPkgTokenValue = value
        End Set
    End Property

    Public Property chkDigitToken() As String
        Get
            Return chkDigitTokenValue
        End Get
        Set(ByVal value As String)
            chkDigitTokenValue = value
        End Set
    End Property

    <Display(Name:="Name")> _
    Public Property name() As String
        Get
            Return nameValue
        End Get
        Set(ByVal value As String)
            nameValue = value
        End Set
    End Property

    <Display(name:="SSN")> _
    Public Property ssnToken() As String
        Get
            Return ssnTokenValue
        End Get
        Set(ByVal value As String)
            ssnTokenValue = value
        End Set
    End Property

    <Display(Name:="User Name")> _
    Public Property UserName() As String
        Get
            Return userNameValue
        End Get
        Set(ByVal value As String)
            userNameValue = value
        End Set
    End Property

    <Display(Name:="Email")> _
    Public Property Email() As String
        Get
            Return emailValue
        End Get
        Set(ByVal value As String)
            emailValue = value
        End Set
    End Property


    <DataType(DataType.Password)> _
    <Display(name:="Password")> _
    Public Property Password() As String
        Get
            Return passwordValue
        End Get
        Set(ByVal value As String)
            passwordValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    <Display(name:="Confirm password")> _
    <Compare("Password", ErrorMessage:="Passwords must match.")> _
    Public Property ConfirmPassword() As String
        Get
            Return confirmPasswordValue
        End Get
        Set(ByVal value As String)
            confirmPasswordValue = value
        End Set
    End Property

    Public Function Validate(ByVal validationContext As ValidationContext) As IEnumerable(Of ValidationResult) Implements IValidatableObject.Validate
        Dim results As List(Of ValidationResult) = New List(Of ValidationResult)
        Dim validData As Boolean = True

        If custToken = Nothing Or billPkgToken = Nothing Or chkDigitToken = Nothing Then
            validData = False
            results.Add(New ValidationResult("required", New String() {"custToken"}))
        End If
        If name = Nothing Then
            validData = False
            results.Add(New ValidationResult("required", New String() {"name"}))
        End If
        If ssnToken = Nothing Then
            validData = False
            results.Add(New ValidationResult("required", New String() {"ssnToken"}))
        Else
            If ssnToken.Length > 4 Then
                validData = False
                results.Add(New ValidationResult("last four digits of SSN only", New String() {"ssnToken"}))
            End If
        End If

        If UserName = Nothing Then  'username required
            results.Add(New ValidationResult("required", New String() {"UserName"}))
        Else
            If UserName.Length > 16 Then  'username 15 digits or less
                results.Add(New ValidationResult("User Name too long", New String() {"UserName"}))
            End If
        End If

        If Email = Nothing Then
            results.Add(New ValidationResult("required", New String() {"Email"}))
        Else
            If (Email.Length > 0 AndAlso Not Regex.IsMatch(Email, "^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")) Then
                results.Add(New ValidationResult("Invalid email address", New String() {"Email"}))
            End If
        End If

        If Password = Nothing Then
            results.Add(New ValidationResult("required", New String() {"Password"}))
            'attribute requires password and confirm password to match -- no need to double check
        ElseIf (Password.Length > 0 AndAlso Not Regex.IsMatch(Password, "(?=^.{8,12}$)(?=.*[^A-Za-z0-9])^.*")) Then
            results.Add(New ValidationResult("password not valid", New String() {"Password"}))
        End If

        Return results
    End Function
End Class
