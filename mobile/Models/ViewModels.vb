﻿Imports System.Data.Entity
Imports mobile.Helpers

Public Class OutageDetailViewModel
    Public Property outagerange As String
    Public Property amount As Integer
    Public Property zone As String
    Public Property bingpolydata As String
    Public Property googlepolydata As String
    Public Property fillcolor As String
End Class

Public Class OutagesViewModel
    Public outageinfo As List(Of OutagesModel)
    Public lastupdate As String
End Class

Public Class OutagesModel
    Public Property esz As String
    Public Property eszdesc As String
    Public Property outagenum As Integer
    Public Property outagerange As String
    Public Property fillcolor As String
End Class

Public Class StockQuote
    Public Property symbol As String = Nothing
    Public Property title As String = Nothing
    Public Property price As String = Nothing
    Public Property change As String = Nothing
    Public Property previousclose As String = Nothing
    Public Property open As String = Nothing
    Public Property dayrange As String = Nothing
    Public Property volume As String = Nothing
    Public Property market As String = Nothing
    Public Property yearrange As String = Nothing
    Public Property time As String = Nothing
    Public Property trend As String = Nothing
End Class

Public Class AccountUpdateType
    Public Property type As String
    Public Property description As String
End Class

Public Class BillingPackagesModel
    Public Property customer_tkn As String
    Public Property billing_pkg_tkn As String
    Public Property checkdigit_tkn As String
End Class

Public Class PaymentsViewModel
    Public paymentinfo As PaginatedList(Of PaymentsModel)
    Public paymentdue As String
    Public accountnumber As String
End Class

Public Class PaymentsModel
    Public Property paymentdate As String
    Public Property paymentamount As String
End Class

Public Class UsageViewModel
    Public accountpackageinfo As IEnumerable(Of CWTokensModel)
    Public usageinfo As PaginatedList(Of UsageModel)
    Public accountnumber As String
    Public accountpackage As String
End Class

Public Class UsageModel
    Public Property read_date As String
    Public Property usage_read_num As String
    Public Property usage_num As String
    Public Property uom As String
    Public Property estimate_ind As String
    Public Property usage_days_cnt As String
    Public Property demand_read As String
    Public Property demand_billed As String
    'Public Property billing_date As String
    'Public Property total_bill As String
    'Public Property usage_charge As String
    'Public Property actual_charge As String
End Class

Public Class StatementsViewModel
    Public statementinfo As IEnumerable(Of StatementsModel)
    Public accountnumber As String
End Class

Public Class StatementsModel
    Public Property statementdate As String
    Public Property amountdue As String
    Public Property delinquentdate As String
    Public Property statementToken As String
End Class

Public Class CWTokensModel
    Public Property customer_tkn As String
    Public Property billing_pkg_tkn As String
    Public Property cdToken As String
    Public Property account_pkg_desc As String
    Public Property customer_acct_tkn As String
    Public Property account_pkg_tkn As String
    Public Property ln1_addr As String
    Public Property unit_code As String
    Public Property city_name As String
    Public Property state_code As String
    Public Property end_date As String
    Public Property srvce_pnt_assn_tkn As String
    Public Property service_point_tkn As String
    Public Property premise_tkn As String
    Public Property revenue_class_desc As String
End Class

Public Class SendEmailModel
    Public Property name As String
    Public Property emailAddress As String
    Public Property emailSubject As String
    Public Property emailBody As String
    Public Property emailTo As String
    Public Property phone As String
    Public Property address As String
    Public Property city As String
    Public Property state As String
    Public Property zip As String
    Public Property problem As String
    Public Property locationCodeA As String
    Public Property locationCodeB As String
    Public Property locationCodeC As String
    'Public Property lastupdate As String
End Class

Public Class AggregatePaymentLocationsModel
    Public Property plstates As IQueryable(Of String)
    Public Property pltypes As IQueryable(Of String)
End Class

Public Class VersionViewModel
    Public Property version As String
    Public Property builddate As DateTime
End Class

Public Class UnitedStatesStates
    Public Shared ReadOnly StateDictionary As IDictionary(Of String, String) = New Dictionary(Of String, String)() From { _
     {"Select a State", ""}, _
     {"Alabama", "AL"}, _
     {"Alaska", "AK"}, _
     {"American Samoa", "AS"}, _
     {"Arizona ", "AZ"}, _
     {"Arkansas", "AR"}, _
     {"California ", "CA"}, _
     {"Colorado ", "CO"}, _
     {"Connecticut", "CT"}, _
     {"Delaware", "DE"}, _
     {"District Of Columbia", "DC"}, _
     {"Federated States Of Micronesia", "FM"}, _
     {"Florida", "FL"}, _
     {"Georgia", "GA"}, _
     {"Guam ", "GU"}, _
     {"Hawaii", "HI"}, _
     {"Idaho", "ID"}, _
     {"Illinois", "IL"}, _
     {"Indiana", "IN"}, _
     {"Iowa", "IA"}, _
     {"Kansas", "KS"}, _
     {"Kentucky", "KY"}, _
     {"Louisiana", "LA"}, _
     {"Maine", "ME"}, _
     {"Marshall Islands", "MH"}, _
     {"Maryland", "MD"}, _
     {"Massachusetts", "MA"}, _
     {"Michigan", "MI"}, _
     {"Minnesota", "MN"}, _
     {"Mississippi", "MS"}, _
     {"Missouri", "MO"}, _
     {"Montana", "MT"}, _
     {"Nebraska", "NE"}, _
     {"Nevada", "NV"}, _
     {"New Hampshire", "NH"}, _
     {"New Jersey", "NJ"}, _
     {"New Mexico", "NM"}, _
     {"New York", "NY"}, _
     {"North Carolina", "NC"}, _
     {"North Dakota", "ND"}, _
     {"Northern Mariana Islands", "MP"}, _
     {"Ohio", "OH"}, _
     {"Oklahoma", "OK"}, _
     {"Oregon", "OR"}, _
     {"Palau", "PW"}, _
     {"Pennsylvania", "PA"}, _
     {"Puerto Rico", "PR"}, _
     {"Rhode Island", "RI"}, _
     {"South Carolina", "SC"}, _
     {"South Dakota", "SD"}, _
     {"Tennessee", "TN"}, _
     {"Texas", "TX"}, _
     {"Utah", "UT"}, _
     {"Vermont", "VT"}, _
     {"Virgin Islands", "VI"}, _
     {"Virginia", "VA"}, _
     {"Washington", "WA"}, _
     {"West Virginia", "WV"}, _
     {"Wisconsin", "WI"}, _
     {"Wyoming", "WY"} _
    }
    Public Shared ReadOnly Property StateSelectList() As SelectList
        Get
            Return New SelectList(StateDictionary, "Value", "Key")
        End Get
    End Property
End Class
