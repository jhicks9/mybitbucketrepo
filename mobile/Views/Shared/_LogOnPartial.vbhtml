﻿@code
    Dim navLabel As String = "MyAccount"
    Dim navDest As String = "Logon"
    If Request.IsAuthenticated Then
        navLabel = User.Identity.Name
        navDest = "Index"
    End If
End Code
<a class="trcenter" href='@Url.Action(navDest, "Account")'>
    <div class="tdcenter"><i class="icon-user nav-icon"></i></div>
    <div class="tdleft nospace">@navLabel</div>
    <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
</a>