﻿@*<div class="footer">
    <a href='@Url.Action("Contacts", "Home")'>Contact Us</a>
    <i class="icon-circle" style="font-size:.8em;padding:0 5px 0 5px"></i>
    <a href='@Url.Action("About", "Home")'>About Us</a>
    <i class="icon-circle" style="font-size:.8em;padding:0 5px 0 5px"></i>
    <a href='https://www.empiredistrict.com' target="_self">Full Site</a>
    <br />
    Copyright © 2007-@DateTime.Now.Year.ToString
</div>*@

<div id="footer" style="position:relative;margin-top:-3.5em;/* negative value of footer height */height:3.5em;">
    <table class="fullwidth">
        <tr>
            <td style="width:25%">
                <ul class="footer" style="list-style:none">
                    <li><a href='@Url.Action("Index", "Home")'><i class="icon-home nav-icon"></i><br />Home</a></li>
                </ul>
            </td>
            <td style="width:25%">
                <ul class="footer" style="list-style:none">
                    <li><a href='@Url.Action("Index", "Account")'><i class="icon-user nav-icon"></i><br />MyAccount</a></li>
                </ul>
            </td>
            <td style="width:25%">
                <ul class="footer" style="list-style:none">
                    <li><a href='@Url.Action("Contacts", "Home")'><i class="icon-phone nav-icon"></i><br />Contact Us</a></li>
                </ul>
            </td>
            <td style="width:25%">
                <ul class="footer" style="list-style:none">
                    <li><a href='@Url.Action("About", "Home")'><i class="icon-info-sign nav-icon"></i><br />About Us</a></li>
                </ul>
            </td>
        </tr>
    </table>
</div><!-- /footer -->