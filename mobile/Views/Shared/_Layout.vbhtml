﻿<!DOCTYPE html>
<html>
<head>
    <title>@ViewData("Title")</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    @*<link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />*@
    <link href="@Url.Content("~/Content/font-awesome.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/mobile.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.8.3.min.js")" type="text/javascript"></script>
    @RenderSection("scripts", False)
    @RenderSection("css", False)
</head>
<body class="body">

    <div id="pagewrap" style="min-height:100%"><!-- pagewrap for sticky footer -->
@code If IsSectionDefined("Header") Then End Code
        <div class="header">
            @RenderSection("Header")
        </div>
@code Else
          @Html.Partial("_HeaderPartial")
      End If End Code

        <div class="content" style="padding-bottom:3.5em"><!-- content: padding must match footer height -->
            @RenderBody()
        </div><!-- /content -->
    </div><!-- /pagewrap -->

@code If IsSectionDefined("Footer") Then End Code
        <div class="footer">
            @RenderSection("Footer", required:=False)
        </div>
@code Else
        @Html.Partial("_FooterPartial")
      End If End Code
</body>
</html>