﻿@Code
    Dim gv As New mobile.Helpers.globalVal
    If gv.IsMobileApp Then  'only display footer on true mobile browser -- not our developed app
    Else
End Code
@*<div class="mobile-footer">
    <a href='@Url.Action("Contacts", "Home")' data-role='none'>Contact Us</a>
    <i class="icon-circle" style="padding:0 5px 0 5px"></i>
    <a href='@Url.Action("About", "Home")' data-role='none'>About Us</a>
    <i class="icon-circle" style="padding:0 5px 0 5px"></i>
    <a href='https://www.empiredistrict.com' target="_blank" data-role='none'>Full Site</a>
    <br />
    Copyright © 2007-@DateTime.Now.Year.ToString
</div>*@

<div data-role="footer" data-theme="f">
<div class="mobile-footer">
    <div class="ui-grid-c">
        <div class="ui-block-a" >
            <a href='@Url.Action("Index", "Home")' data-role="button">
                <table class="nav-table fullwidth">
                    <tr><td><i class="icon-home"></i></td></tr>
                    <tr><td>Home</td></tr>
                </table>
            </a>
        </div>
        <div class="ui-block-b">
            <a href='@Url.Action("Index", "Account")' data-role="button" rel="external">
                <table class="nav-table fullwidth">
                    <tr><td><i class="icon-user"></i></td></tr>
                    <tr><td>MyAccount</td></tr>
                </table>
            </a>
        </div>
        <div class="ui-block-c">
            <a href='@Url.Action("Contacts", "Home")' data-role="button">
                <table class="nav-table fullwidth">
                    <tr><td><i class="icon-phone"></i></td></tr>
                    <tr><td>Contact Us</td></tr>
                </table>
            </a>
        </div>
        <div class="ui-block-d">
            <a href='@Url.Action("About", "Home")' data-role="button">
                <table class="nav-table fullwidth">
                    <tr><td><i class="icon-info-sign"></i></td></tr>
                    <tr><td>About Us</td></tr>
                </table>
            </a>
        </div>
    </div>
    </div>
</div><!-- /footer -->
@Code End If End Code