﻿<!DOCTYPE html>
<html>
<head>
    <title>@ViewData("Title")</title>
    <meta name="viewport" content="width=device-width" />
    @*<link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />*@

    <link href="@Url.Content("~/Content/mobile-theme.min.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/jquery.mobile.structure-1.3.1.min.css")" rel="stylesheet" type="text/css" />
    @*<link href="@Url.Content("~/Content/jquery.mobile-1.3.0.min.css")" rel="stylesheet" type="text/css" />*@
    <link href="@Url.Content("~/Content/font-awesome.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/mobile.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.8.3.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.mobile-1.3.1.min.js")" type="text/javascript"></script>

    @RenderSection("scripts", False)
    @RenderSection("css", False)
</head>
<body>
    <div data-role="page" data-theme="f">
@code If IsSectionDefined("Header") Then End Code
        <div data-role="header" data-theme="f">
            @RenderSection("Header")
        </div>
@code Else
          @Html.Partial("_HeaderPartial.Mobile")
      End If End Code

        <div data-role="content">
            @RenderBody()
        </div>
        
@code If IsSectionDefined("Footer") Then End Code
        <div data-role="footer" data-theme="f">
            @RenderSection("Footer", required:=False)
        </div>
@code Else
        @Html.Partial("_FooterPartial.Mobile")
      End If End Code        
    </div>
</body>
</html>
