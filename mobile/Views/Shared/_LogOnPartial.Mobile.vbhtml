﻿@code
    Dim navLabel As String = "MyAccount"
    Dim navDest As String = "Logon"
    If Request.IsAuthenticated Then
        navLabel = User.Identity.Name
        navDest = "Index"
    End If
End Code

<a href='@Url.Action(navDest, "Account")' rel="external">
    <table class="nav-table"><tr>
        <td class="tdleft"><i class="icon-user"></i>&nbsp;@navLabel</td>
        <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
    </tr></table>
</a>