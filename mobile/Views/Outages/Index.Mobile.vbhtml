﻿@Code
    ViewData("Title") = "Outages"
End Code

<div class="h4">Outage Center</div>
<ul data-role="listview" data-inset="true">
    <li data-icon="false"><a href='@Url.Action("Outages", "Outages")'>
        <table class="nav-table"><tr>
            <td class="tdleft"><i class="icon-bolt"></i>&nbsp;Outages</td>
            <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
        </tr></table>
    </a></li>
    <li data-icon="false"><a href='@Url.Action("ReportOutage", "Outages")'>
        <table class="nav-table"><tr>
            <td class="tdleft"><i class="icon-envelope"></i>&nbsp;Report Outage</td>
            <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
        </tr></table>
    </a></li>
</ul>