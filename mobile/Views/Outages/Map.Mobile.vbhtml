﻿@Code 
    Layout = Nothing  'causes page to not use master page.  
End Code

@ModelType mobile.OutageDetailViewModel

<!DOCTYPE html>
<html>
    <head>
        <title>@ViewData("Title")</title>
        <meta name="viewport" content="width=device-width" />
        <script type="text/javascript" src="https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBfpSajyrqjINoJyVWJbFwmqjypj_SMB8&sensor=false"></script>
        <style type="text/css">body{height:100% !important;margin:0;padding:0;top:0;font:1em arial,verdana,sans-serif;}</style>
    </head>
    <body onload="launchMap();">
        <div id="map_content" style="width:100%;background-color:#113962;color:#000">
            <div id="mapInfo" style="padding:10px;text-align:center;">
                <b>@model.zone area</b><br />
                <b>@Model.amount customers affected</b>
            </div>
            <div id='myMap' style='position:relative;'></div>
        </div>
    </body>
    <script type="text/javascript">
        var map = null;
        var latMin = 0;var lonMin = 0;var latMax = 0;var lonMax = 0;  //strict panning boundaries
        var initView = null;
        var pinLayer = new Microsoft.Maps.EntityCollection();
        var points = @Model.bingpolydata;
        var center = simplePolygonCentroid(points);

        var gCenter = new google.maps.LatLng(center.latitude, center.longitude);
        var gMarkers = [];
        var strictBounds = null;
        var gpoints = @Model.googlepolydata;

        function loadGoogleMap() {
            try {
                var mapOptions = {
                    center: gCenter,
                    zoom: 9,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    disableDoubleClickZoom: false,
                    panControl: true,
                    draggable: true,
                    streetViewControl: false
                };

                map = new google.maps.Map(document.getElementById('myMap'), mapOptions);

                google.maps.event.addListener(map, 'zoom_changed', function () {
                    if (map.getZoom() > 12) {
                        map.setZoom(12);
                        //map.setCenter(gCenter);
                    }
                    if (map.getZoom() < 8) {
                        map.setZoom(8);
                        //map.setCenter(gCenter);
                    }
                });

                google.maps.event.addListener(map, 'bounds_changed', function () {
                    if (strictBounds == null) {
                        strictBounds = map.getBounds();
                    }
                });

                google.maps.event.addListener(map, 'center_changed', function () {
                    if (strictBounds.contains(map.getCenter())) return;
                    map.setZoom(9);
                    map.setCenter(gCenter);
                });

                var pushpinColor = 'blue';
                if ('@Model.fillcolor' == '#92A1F6'){pushpinColor = 'blue';}
                if ('@Model.fillcolor' == '#00FF21'){pushpinColor = 'green';}
                if ('@Model.fillcolor' == '#FFFF21'){pushpinColor = 'yellow';}
                if ('@Model.fillcolor' == '#FFB218'){pushpinColor = 'orange';}
                if ('@Model.fillcolor' == '#FF0033'){pushpinColor = 'red';}
                if ('@Model.fillcolor' == '#FF51FF'){pushpinColor = 'pink';}
                if ('@Model.fillcolor' == '#C8AAF1'){pushpinColor = 'purple';}
                var baseUrl = '@Url.Content("~")';
                var pushpinPath = location.protocol + "//" + location.host + baseUrl + 'Content/images/outages-pushpin-' + pushpinColor + '.png';
                var poly = new google.maps.Polygon({paths:gpoints,clickable:true,strokeColor:'#404040',strokeOpacity:0.8,strokeWeight:1,fillColor:'@Model.fillcolor',fillOpacity:0.40});
                poly.setMap(map);
                var marker = new google.maps.Marker({position:gCenter,map:map,icon:pushpinPath});
            }
            catch (E) { alert(E.message); }
        }

        function loadBingMap() {
            map = new Microsoft.Maps.Map(document.getElementById('myMap'),
            { showMapTypeSelector: false,
                credentials: 'AhKzS2kz7-h2jU5EDEeAwBL_PCaaIx2ANTryFwMqw8xlCk6Jc3Y7tuDMjGATRy-P',
                showScalebar: false,
                showDashboard: true,
                enableClickableLogo: false,
                enableSearchLogo: false,
                disableKeyboardInput: true,
                fixedMapPosition: true
            });
            map.setView({ zoom: 9, center: center, mapTypeId: Microsoft.Maps.MapTypeId.road});

            map.getZoomRange = function () {
                return {
                    max: 12,
                    min: 8
                };
            };

            //disables map drag/scroll/zoom
            Microsoft.Maps.Events.addHandler(map, 'mousewheel', function (e) {
                e.handled = true;
                return true;
            });

            //disables map drag/scroll/zoom
            //Microsoft.Maps.Events.addHandler(map, 'mousedown', function (e) {
            //    e.handled = true;
            //    return true;
            //});

            Microsoft.Maps.Events.addHandler(map, 'viewchange', function (e) {
                // do not pan outside original boundaries
                var tCenter = map.getTargetBounds();
                if ((tCenter.center.latitude < latMin) || (tCenter.center.latitude > latMax) || (tCenter.center.longitude < lonMin) || (tCenter.center.longitude > lonMax)) {
                    map.setView({ 'zoom': 9, 'center': center });
                }
            });

            Microsoft.Maps.Events.addHandler(map, 'viewchangeend', function (e) {
                //get initial map boundaries
                if (initView == null) {
                    initView = map.getBounds();
                    var topLeft = initView.getNorthwest();
                    var bottomRight = initView.getSoutheast();
                    latMin = bottomRight.latitude;
                    lonMin = topLeft.longitude;
                    latMax = topLeft.latitude;
                    lonMax = bottomRight.longitude;
                }
            });

            //set zoom limits
            var restrictZoom = function () {
                if (map.getZoom() <= map.getZoomRange().min) {
                    map.setView({
                        'zoom': map.getZoomRange().min,
                        'animate': false
                    });
                }
                else if (map.getZoom() >= map.getZoomRange().max) {
                    map.setView({
                        'zoom': map.getZoomRange().max,
                        'animate': false
                    });
                }
            };

            var strColorBlue = new Microsoft.Maps.Color(100,0,38,255);
            var strColorGreen = new Microsoft.Maps.Color(100,0,255,33);
            var strColorYellow = new Microsoft.Maps.Color(100,255,255,33);
            var strColorOrange = new Microsoft.Maps.Color(100,255,178,24);
            var strColorRed = new Microsoft.Maps.Color(100,255,0,0);
            var strColorPink = new Microsoft.Maps.Color(100,255,81,255);
            var strColorPurple = new Microsoft.Maps.Color(100,135,62,244);
            var currentColor = strColorBlue;
            var pushpinColor = 'blue';
            if ('@Model.fillcolor' == '#92A1F6'){currentColor = strColorBlue;pushpinColor = 'blue';}
            if ('@Model.fillcolor' == '#00FF21'){currentColor = strColorGreen;pushpinColor = 'green';}
            if ('@Model.fillcolor' == '#FFFF21'){currentColor = strColorYellow;pushpinColor = 'yellow';}
            if ('@Model.fillcolor' == '#FFB218'){currentColor = strColorOrange;pushpinColor = 'orange';}
            if ('@Model.fillcolor' == '#FF0033'){currentColor = strColorRed;pushpinColor = 'red';}
            if ('@Model.fillcolor' == '#FF51FF'){currentColor = strColorPink;pushpinColor = 'pink';}
            if ('@Model.fillcolor' == '#C8AAF1'){currentColor = strColorPurple;pushpinColor = 'purple';}
            var poly1 = new Microsoft.Maps.Polygon(points,{fillColor: currentColor,strokeColor: new Microsoft.Maps.Color(200,100,100,100),strokeThickness: 1});
            pinLayer.push(poly1);
            center = simplePolygonCentroid(points);

            var baseUrl = '@Url.Content("~")';
            var pushpinPath = location.protocol + "//" + location.host + baseUrl + 'Content/images/outages-pushpin-' + pushpinColor + '.png';
            var pin1 = new Microsoft.Maps.Pushpin(simplePolygonCentroid(points), {text:'', draggable: false,icon:pushpinPath});
            pinLayer.push(pin1);
            map.entities.push(pinLayer);
        }

        function simplePolygonCentroid(points) { // param can be either a Bing Map v7 Point class or Location class
            var sumY = 0; var sumX = 0; var partialSum = 0; var sum = 0;
            points.push(points[0]);  //close polygon
            var n = points.length;
            for (var i=0; i < n-1; i++){
                partialSum = points[i].longitude * points[i+1].latitude - points[i+1].longitude * points[i].latitude;
                sum += partialSum;
                sumX += (points[i].longitude + points[i + 1].longitude) * partialSum;
                sumY += (points[i].latitude + points[i + 1].latitude) * partialSum;
            }
            var area = 0.5 * sum;
            return new Microsoft.Maps.Location(sumY / 6 / area, sumX / 6 / area);
        }

        function isBingMap() {
            var today = new Date();
            var mm = today.getMonth() + 1; //January is 0! -- months are a zero starting array
            if ((mm % 2 == 0)) {  //if even month
                return true;  //use bing map
            } else {
                return false;  //use google map
            }
        }

        function launchMap() {
            document.getElementById('myMap').style.width = getWidth() + 'px';
            document.getElementById('myMap').style.height = getWidth() + 'px';
            document.getElementById('mapInfo').style.backgroundColor = '@Model.fillcolor';
            var useBingMap = isBingMap();
            if (useBingMap == true) {
                loadBingMap();
            } else {
                loadGoogleMap();
            }
        }

        function getWidth() {
            if (self.innerWidth) {
                return self.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientHeight){
                return document.documentElement.clientWidth;
            }
            else if (document.body) {
                return document.body.clientWidth;
            }
            return 0;
        }
    </script>
</html>