﻿@ModelType mobile.SendEmailModel

@Code
    ViewData("Title") = "Report Outage"
End Code

@code
    Html.ValidationSummary()
    Using Html.BeginForm
        @Html.AntiForgeryToken()
End Code

<div class="h4">Report Outage</div>
<div class="nav-table center">
<table>
    <tr>
        <td>@Html.LabelFor(Function(model) model.name, "Name")</td>
        <td>
            @Html.TextBoxFor(Function(model) model.name, New With {Key .placeholder = "name"})            
            @Html.ValidationMessageFor(Function(model) model.name)
        </td>
    </tr>
    <tr>
        <td>@Html.LabelFor(Function(model) model.phone, "Phone")</td>
        <td>
            @Html.TextBoxFor(Function(model) model.phone, New With {Key .type = "tel", .placeholder = "phone"})            
            @Html.ValidationMessageFor(Function(model) model.phone)
        </td>
    </tr>
    <tr>
        <td>@Html.LabelFor(Function(model) model.address, "Address")</td>
        <td>
            @Html.TextBoxFor(Function(model) model.address, New With {Key .placeholder = "address"})
            @Html.ValidationMessageFor(Function(model) model.address)
        </td>
    </tr>
    <tr>
        <td>@Html.LabelFor(Function(model) model.city, "City")</td>
        <td>
            @Html.TextBoxFor(Function(model) model.city, New With {Key .placeholder = "city"})
            @Html.ValidationMessageFor(Function(model) model.city)
        </td>
    </tr>
    <tr>
        <td>@Html.LabelFor(Function(model) model.state, "State")</td>
        <td>
            @Html.DropDownListFor(Function(model) model.state, mobile.UnitedStatesStates.StateSelectList, New With {Key .data_mini = "false", .data_theme = "c"})
            @Html.ValidationMessageFor(Function(model) model.state)
        </td>
    </tr>
    <tr>
        <td>@Html.LabelFor(Function(model) model.zip, "Zip Code")</td>
        <td>
            @Html.TextBoxFor(Function(model) model.zip, New With {Key .type = "tel", .placeholder = "zip code"})
            @Html.ValidationMessageFor(Function(model) model.zip)
        </td>
    </tr>
    <tr>
        <td>@Html.LabelFor(Function(model) model.locationCodeA , "Location Code")</td>
        <td>
            <table style="border-collapse:collapse;"><tr style="padding:0">
                <td style="padding:0">@Html.TextBoxFor(Function(model) model.locationCodeA, New With {Key .type = "tel", .style = "width:55px"})</td>
                <td><b>-</b></td>
                <td>@Html.TextBoxFor(Function(model) model.locationCodeB, New With {Key .type = "tel", .style = "width:30px"})</td>
                <td><b>-</b></td>
                <td>@Html.TextBoxFor(Function(model) model.locationCodeC, New With {Key .type = "tel", .style = "width:30px"})</td>
            </tr></table>
        </td>
    </tr>
    <tr>
        <td><label for="selectDescription" class="select">Problem</label></td>
        <td>
            <select name="selectDescription" id="selectDescription" data-theme="c">
		            <option value="">Select best description</option>
		            <option value="Arcs">Arcs</option>
		            <option value="Fire">Fire</option>
		            <option value="Flash">Flash</option>
		            <option value="Lights blinking">Lights blinking</option>
		            <option value="Lights dim/bright">Lights dim/bright</option>
		            <option value="Lights off">Lights off</option>
		            <option value="Lights off again">Lights off again</option>
		            <option value="Lights on">Lights on</option>
		            <option value="Noise from equipment">Noise from equipment</option>
		            <option value="Partial power">Partial power</option>
		            <option value="Pole down">Pole down</option>
		            <option value="Structure Fire">Structure Fire</option>
		            <option value="Tree in line">Tree in line</option>
		            <option value="Trees/limbs sparking">Trees/limbs sparking</option>
		            <option value="Wire down">Wire down</option>
            </select>
        </td>
    </tr>
</table>
</div>

<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            <a class="trcenter" href='@Url.Action("Index", "Outages")'>
                <div class="tdcenter"><i class="icon-remove nav-icon" style="color:Red"></i></div>
                <div class="tdcenter">Cancel</div>
            </a>
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            <a class="trcenter" href="#" data-role="button" onclick="$(this).closest('form').submit()">
                <div class="tdcenter"><i class="icon-ok nav-icon" style="color:Green"></i></div>
                <div class="tdcenter nospace">Submit Outage</div>
            </a>
        </div>
    </div>
</div>
@code End Using End Code