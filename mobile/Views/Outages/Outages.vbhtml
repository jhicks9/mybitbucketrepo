﻿@ModelType mobile.OutagesViewModel

@Code
    ViewData("Title") = "Outages"
End Code

<div class="h4">Outages</div>
<div class="nav-table center">
@code 
    Dim outageCount As Integer = 0
    For Each outage In Model.outageinfo      
        If outage.outagerange = "less than 10" Then  'dont display less than 10 outages
        Else
End Code
    <a class="trcenter" href='@Url.Action("Map", "Outages", New With {.id = outage.esz, .amount = outage.outagenum})'>
        <div class="tdcenter">
            <i class="icon-bolt nav-icon" style="background-color:@outage.fillcolor;padding:5px 10px 5px 10px;border:1px solid #113962"></i>
        </div>
        <div class="tdleft" style="">@outage.eszdesc area<br /> @outage.outagenum customers affected</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
@code 
            outagecount += 1
        End If
    Next
End code
</div><!-- nav-table -->
<div class="nav-table center">
@code
    If outageCount = 0 Then
End code
    <br />
    <div style="color:Maroon">No current outages</div>
    <br />
@code
End If
Dim displayDate As System.DateTime = DateTime.Parse(Model.lastupdate.ToString).AddDays(0)
Dim displayLastUpdate As String = String.Format("{0:MM/dd/yyyy hh:mm tt}", displayDate)
End Code
    <div class="tr">Last updated:&nbsp;@displayLastUpdate</div>
</div><!-- nav-table -->