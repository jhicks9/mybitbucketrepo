﻿@Code
    ViewData("Title") = "Outages"
End Code

<div class="h4">Outage Center</div>
<div class="nav-table center">
    <a class="trcenter" href='@Url.Action("Outages", "Outages")'>
        <div class="tdcenter"><i class="icon-bolt nav-icon"></i></div>
        <div class="tdleft nospace">Outages</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("ReportOutage", "Outages")'>
        <div class="tdcenter"><i class="icon-envelope nav-icon"></i></div>
        <div class="tdleft nospace">Report Outage</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
</div>