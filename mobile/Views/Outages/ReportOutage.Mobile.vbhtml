﻿@ModelType mobile.SendEmailModel

@Code
    ViewData("Title") = "Report Outage"
End Code

@code
    Html.ValidationSummary()
    Using Html.BeginForm
        @Html.AntiForgeryToken()
End Code

    <div class="h4">Report Outage</div>
    
    @Html.TextBoxFor(Function(model) model.name, New With {Key .placeholder = "Name"})
    @Html.ValidationMessageFor(Function(model) model.name)

    @Html.TextBoxFor(Function(model) model.phone, New With {Key .type = "tel", .placeholder = "Phone"})
    @Html.ValidationMessageFor(Function(model) model.phone)


    @Html.TextBoxFor(Function(model) model.address, New With {Key .placeholder = "Address"})
    @Html.ValidationMessageFor(Function(model) model.address)
        
    @Html.TextBoxFor(Function(model) model.city, New With {Key .placeholder = "City"})
    @Html.ValidationMessageFor(Function(model) model.city)
    
    @Html.DropDownListFor(Function(model) model.state, mobile.UnitedStatesStates.StateSelectList, New With {Key .data_mini = "false", .data_theme = "c"})
    @Html.ValidationMessageFor(Function(model) model.state)

    @Html.TextBoxFor(Function(model) model.zip, New With {Key .type = "tel", .placeholder = "Zip code"})
    @Html.ValidationMessageFor(Function(model) model.zip)
                
    <table>
        <tr>
            <td style="width:50%;">
                @Html.TextBoxFor(Function(model) model.locationCodeA, New With {Key .type = "tel", .placeholder = "Location code", .style = ""})                
            </td>
            <td><b>-</b></td>
            <td style="width:25%">
                @Html.TextBoxFor(Function(model) model.locationCodeB, New With {Key .type = "tel", .style = ""})                
            </td>
            <td><b>-</b></td>
            <td style="width:25%">
                @Html.TextBoxFor(Function(model) model.locationCodeC, New With {Key .type = "tel", .style = ""})                
            </td>
        </tr>
    </table>
    
    <select name="selectDescription" id="selectDescription" data-theme="c">
		    <option value="">Select best description of problem</option>
		    <option value="Arcs">Arcs</option>
		    <option value="Fire">Fire</option>
		    <option value="Flash">Flash</option>
		    <option value="Lights blinking">Lights blinking</option>
		    <option value="Lights dim/bright">Lights dim/bright</option>
		    <option value="Lights off">Lights off</option>
		    <option value="Lights off again">Lights off again</option>
		    <option value="Lights on">Lights on</option>
		    <option value="Noise from equipment">Noise from equipment</option>
		    <option value="Partial power">Partial power</option>
		    <option value="Pole down">Pole down</option>
		    <option value="Structure Fire">Structure Fire</option>
		    <option value="Tree in line">Tree in line</option>
		    <option value="Trees/limbs sparking">Trees/limbs sparking</option>
		    <option value="Wire down">Wire down</option>
    </select>

    <fieldset class="ui-grid-a">
        <legend></legend>
        <div class="ui-block-a">
            <a href='@Url.Action("Index", "Outages")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
        </div>
        <div class="ui-block-b">
            <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Submit</a>
        </div>
    </fieldset>
@code End Using End Code