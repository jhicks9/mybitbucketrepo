﻿@ModelType mobile.OutagesViewModel

@Code
    ViewData("Title") = "Outages"
End Code

<div class="h4">Outages</div>
<ul data-role="listview" data-inset="true">
@code 
    Dim outageCount As Integer = 0
    For Each outage In Model.outageinfo      
        If outage.outagerange = "less than 10" Then  'dont display less than 10 outages
        Else
End Code
    <li data-icon="false">
        <a href='@Url.Action("Map", "Outages", New With {.id = outage.esz, .amount = outage.outagenum})' rel="external">
            <table class="nav-table"><tr>
                <td class="tdleft" style="width:1%;padding-right:3px;">
                    <i class="icon-bolt" style="font-size:1.5em;background-color:@outage.fillcolor;padding:3px;border:1px solid #113962"></i>
                </td>
                <td class="tdleft" style="font-weight:normal;font-size:.8em"><b> @outage.eszdesc area </b><br /> @outage.outagenum customers affected</td>
                <td class="tdright"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
@code 
            outagecount += 1
        End If
    Next
End code
</ul>
@code
    If outageCount = 0 Then
End code
<div style="color:Maroon">No current outages</div><br />
@code
End If
Dim displayDate As System.DateTime = DateTime.Parse(Model.lastupdate.ToString).AddDays(0)
Dim displayLastUpdate As String = String.Format("{0:MM/dd/yyyy hh:mm tt}", displayDate)
End Code
Last updated:&nbsp;@displayLastUpdate