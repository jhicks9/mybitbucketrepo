﻿@Modeltype mobile.AggregatePaymentLocationsModel

@Code
    ViewData("Title") = "Index"
    Dim iconName As String = ""
End Code

<div class="h4">Payment Locations</div>
@code   If Request.QueryString("pltype") Is Nothing Then End code

<div class="nav-table center">
    <div class="trcenter" style="text-align:center;padding:5px;">
        <div class="tdcenter"></div>
        <div class="tdcenter">Select a Service</div>
        <div class="tdcenter"></div>
    </div>
@code   For Each item In Model.pltypes
                Select Case item.ToString
                    Case "Gas"
                        iconName = "icon-fire"
                    Case "Water"
                        iconName = "icon-tint"
                    Case "Electric"
                        iconName = "icon-bolt"
                    Case Else
                        iconName = "icon-map-marker"
                End Select
end code
    <a class="trcenter" href='@Url.Action("Index", "PLocations", New With {.pltype = item.ToString})'>
        <div class="tdcenter"><i class="@iconName nav-icon"></i></div>
        <div class="tdleft nospace">@item.ToString</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
@code   Next End code
</div>
@code   Else end code

<div class="nav-table center">
    <div class="trcenter" style="text-align:center;padding:5px;">
        <div class="tdcenter"></div>
        <div class="tdcenter">Select a State</div>
        <div class="tdcenter"></div>
    </div>
@code
            For Each item In Model.plstates
                Select Case Request.QueryString("pltype")
                    Case "Gas"
                        iconName = "icon-fire"
                    Case "Water"
                        iconName = "icon-tint"
                    Case "Electric"
                        iconName = "icon-bolt"
                    Case Else
                        iconName = "icon-map-marker"
                        
                End Select
end code
    <a class="trcenter" href='@Url.Action("Browse", "PLocations", New With {.pltype = Request.QueryString ("pltype"), .plstate = item.ToString})'>
        <div class="tdcenter"><i class="@iconName nav-icon"></i></div>
        <div class="tdleft nospace">@item.ToString</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
@code   Next end code
</div>
@code   End If End Code