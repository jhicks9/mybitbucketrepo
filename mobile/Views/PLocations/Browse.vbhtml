﻿@ModelType mobile.Helpers.PaginatedList(Of mobile.PLocation)

@Code
    ViewData("Title") = "Browse"
End Code

<div class="nav-table center">
   @code For Each plocation In Model End Code
    <a class="trcenter" href='@Url.Action("Details", "PLocations", New With {.id = plocation.id})'>
        <div class="tdcenter"><i class="icon-map-marker nav-icon"></i></div>
        <div class="tdleft nospace cellpadding">
            @plocation.city,&nbsp;@plocation.state<br />
            @plocation.name
        </div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
   @code Next End Code
</div>

<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            @code If Not (Model.HasPreviousPage) Then End code
            <div class="trcenter">
                <div class="tdleft disabled cellpadding"><i class="icon-chevron-left nav-icon"></i></div>
                <div class="tdright disabled">Previous</div>
            </div>
            @code Else End Code
            <a class="trcenter" href="@Url.RouteUrl("PaymentLocations", New With {.controller = "PLocations", .action = "Browse", .page = (Model.PageIndex - 1)})">
                <div class="tdleft cellpadding"><i class="icon-chevron-left nav-icon"></i></div>
                <div class="tdright">Previous</div>
            </a>
            @code End If End Code
        </div>        
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            @code If Not (Model.HasNextPage) Then End code
            <div class="trcenter">
                <div class="tdleft disabled">Next</div>
                <div class="tdright cellpadding disabled"><i class="icon-chevron-right nav-icon"></i></div>
            </div>
            @code Else End Code
            <a href="@Url.RouteUrl("PaymentLocations", New With {.controller = "PLocations", .action = "Browse", .page = (Model.PageIndex + 1)})">
                <div class="tdleft">Next</div>
                <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
            </a>
            @code End If End code
        </div>
    </div>
</div>