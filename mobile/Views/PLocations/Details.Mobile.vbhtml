﻿@ModelType mobile.plocation

@Code
    ViewData("Title") = "Details"
    Dim centerMap As String = Html.Encode(Model.address & " " & Model.city & ", " & Model.state)
End Code

<div style="text-align:center">
    <div class="h4">@Model.name</div>
    @Model.address <br />
    @Model.city,&nbsp;@Model.state&nbsp;&nbsp;@Model.zip
    <br /><br />
    <a href="tel:@Model.phone" data-role="button" data-inline="true">
        <table class="nav-table"><tr>
            <td class="tdleft"><i class="icon-phone nav-icon"></i></td>
            <td class="tdright">&nbsp;@Model.phone</td>
        </tr></table>
    </a>

    <div id="map_content" style="">
        <p>javascript required to render the map</p>
    </div>
</div>

<script type="text/javascript" >
    $(document).ready(function () {
        renderMap();
    });

    function renderMap() {
        var mapWidth = $('#map_content').width();
        var mapHeight = mapWidth;
        var mapDim = mapWidth + "x" + mapHeight;
        if (mapWidth > 1100) { mapWidth = 1100; }
        if (mapHeight > 900) { mapHeight = 900; }
        var mapDimBing = mapWidth + "," + mapHeight;
        var imageSrcGoogle = "<img id=\"map\" src=\"https://maps.google.com/maps/api/staticmap?center=@centerMap&scale=1&zoom=15&size=" + mapDim + "&maptype=roadmap&markers=size:mid|color:blue|@centerMap&key=AIzaSyBbJ0uBkGA_ZknD00z0ioYkcBL9sZ7e-z8&sensor=false\" alt=\"\" />";
        var imageSrcBing = "<img id=\"map\" src=\"https://dev.virtualearth.net/REST/V1/Imagery/Map/Road/@centerMap?zoomLevel=15&mapSize=" + mapDimBing + "&fmt=jpeg&key=AhKzS2kz7-h2jU5EDEeAwBL_PCaaIx2ANTryFwMqw8xlCk6Jc3Y7tuDMjGATRy-P\" alt=\"\" />";
        
        var useBingMap = isBingMap();
        if (useBingMap == true) {
            $("#map_content").html(imageSrcBing);
        } else {
            $("#map_content").html(imageSrcGoogle);
        }
    }

    function isBingMap() {
        var today = new Date();
        var mm = today.getMonth() + 1; //January is 0! -- months are a zero starting array
        if ((mm % 2 == 0)) {  //if even month
            return true;  //use bing map
        } else {
            return false;  //use google map
        }
    }
</script>