﻿@ModelType mobile.ChangePasswordModel

@Code
    ViewData("Title") = "Change Password"
    Using Html.BeginForm()
End Code

<div class="h4">Change Password</div>
<br />
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

<div class="nav-table center">
    <div class="trcenter" style="padding:0">
        <div class="td" style="padding:3px">@Html.LabelFor(Function(m) m.OldPassword, "Current Password")</div>
        <div class="td" style="padding:3px">@Html.PasswordFor(Function(m) m.OldPassword, New With {Key .placeholder = "Current Password"})</div>
        <div class="td" style="padding:3px">@Html.ValidationMessageFor(Function(m) m.OldPassword)</div>
    </div>

    <div class="trcenter" style="padding:0">
        <div class="td" style="padding:3px">@Html.LabelFor(Function(m) m.NewPassword, "New Password")</div>
        <div class="td" style="padding:3px">@Html.PasswordFor(Function(m) m.NewPassword, New With {Key .placeholder = "New Password"})</div>
        <div class="td" style="padding:3px">@Html.ValidationMessageFor(Function(m) m.NewPassword)</div>
    </div>

    <div class="trcenter" style="padding:0">
        <div class="td" style="padding:3px">@Html.LabelFor(Function(m) m.ConfirmPassword, "Confirm Password")</div>
        <div class="td" style="padding:3px">@Html.PasswordFor(Function(m) m.ConfirmPassword, New With {Key .placeholder = "Confirm Password"})</div>
        <div class="td" style="padding:3px">@Html.ValidationMessageFor(Function(m) m.ConfirmPassword)</div>
    </div>
    <div class="trcenter" style="width:100%;text-align:center;padding:0;">
        @Html.ValidationSummary(True)
    </div>
</div>

<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            <a class="trcenter" href='@Url.Action("Index", "Account")'>
                <div class="tdcenter cellpadding"><i class="icon-remove nav-icon" style="color:Red"></i></div>
                <div class="tdcenter nospace">Cancel</div>
            </a>
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            <a class="trcenter" href="#" data-role="button" onclick="$(this).closest('form').submit()">
                <div class="tdcenter cellpadding"><i class="icon-ok nav-icon" style="color:Green"></i></div>
                <div class="tdcenter nospace">Submit</div>
            </a>
        </div>
    </div>
</div>
@Code End Using End code
