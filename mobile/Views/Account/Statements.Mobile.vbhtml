﻿@ModelType mobile.statementsViewModel

@Code
    ViewData("Title") = "Statements"
End Code

<div class="h4"><i class="icon-file-alt"></i>&nbsp;MyStatements</div>
<br />
@Model.accountnumber<br />
<table class="data-table">
    <tr class="ui-bar-f">
        <td class="tdcellcenter"><i class="icon-calendar"></i>&nbsp;Statement Date</td>
        <td class="tdcellcenter"><b>$</b>&nbsp;Amount Due</td>
        <td class="tdcellcenter"><i class="icon-calendar"></i>&nbsp;Delinquent Date</td>
    </tr>
    @code For Each statement In Model.statementinfo End code
    <tr>
        <td class="tdcellcenter">
            <a href='@Url.Action("statementImage", "Account", New With {.stkn = statement.statementToken})' data-rel="dialog" >@statement.statementdate</a>
        </td>
        <td class="tdcellcenter">@statement.amountdue</td>
        <td class="tdcellcenter">@statement.delinquentdate </td>
    </tr>
    @code Next End Code
</table>

@*disable ajax loading for this page so statement pdfs will load*@
<script type="text/javascript">
    $.mobile.ajaxEnabled = false;
</script>