﻿@ModelType mobile.RecoverPasswordModel
    
@Code
    ViewData("Title") = "FPassword"
    Using Html.BeginForm
End Code

<div class="h4">Reset MyPassword</div>

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
    
<br />
<fieldset>
    <legend>Verify Account</legend>
    <p style="text-align:center">You will need to know your account number, your name <i>(exactly as it appears on your statement)</i>, and the last four digits of your Social Security Number to register.</p>
    <table class="nav-table center">
        <tr>
            <td>@Html.LabelFor(Function(model) model.custToken, "Account Number")</td>
            <td>
                <table style="border-collapse:collapse;"><tr style="padding:0">
                    <td style="padding:0">@Html.TextBoxFor(Function(model) model.custToken, New With {Key .type = "tel", .style = "width:55px", .placeholder = "Account"})</td>
                    <td><b>-</b></td>
                    <td>@Html.TextBoxFor(Function(model) model.billPkgToken , New With {Key .type = "tel", .style = "width:30px"})</td>
                    <td><b>-</b></td>
                    <td>@Html.TextBoxFor(Function(model) model.chkDigitToken, New With {Key .type = "tel", .style = "width:30px"})</td>
                </tr></table>
            </td>
        </tr>
        <tr><td></td><td>@Html.ValidationMessageFor(Function(m) m.custToken)</td></tr>
        <tr>
            <td>@Html.LabelFor(Function(model) model.name)</td>
            <td>@Html.TextBoxFor(Function(model) model.name, New With {.placeholder = "Name"})</td>
        </tr>
        <tr><td></td><td>@Html.ValidationMessageFor(Function(model) model.name)</td></tr>
        <tr>
            <td>@Html.LabelFor(Function(model) model.ssnToken)</td>
            <td>@Html.TextBoxFor(Function(model) model.ssnToken, New With {.placeholder = "SSN"})</td>
        </tr>
        <tr><td></td><td>@Html.ValidationMessageFor(Function(model) model.ssnToken)</td></tr>
    </table>
</fieldset>
<p style="text-align:center">Passwords are required to be at least 8 characters long including one special character (for example: john_123).</p>
<table class="nav-table center">
    <tr>
        <td>@Html.LabelFor(Function(m) m.Password)</td>
        <td>@Html.PasswordFor(Function(m) m.Password, New With {.placeholder = "Password"})</td>
    </tr>
    <tr><td></td><td>@Html.ValidationMessageFor(Function(m) m.Password)</td></tr>
    <tr>
        <td>@Html.LabelFor(Function(m) m.ConfirmPassword)</td>
        <td>@Html.PasswordFor(Function(m) m.ConfirmPassword, New With {.placeholder = "Confirm Password"})</td>
    </tr>
    <tr><td></td><td>@Html.ValidationMessageFor(Function(m) m.ConfirmPassword)</td></tr>
</table>

<div style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>
<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            <a class="trcenter" href='@Url.Action("ResetAccount", "Account")'>
                <div class="tdcenter cellpadding"><i class="icon-remove nav-icon" style="color:Red"></i></div>
                <div class="tdcenter nospace">Cancel</div>
            </a>
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            <a class="trcenter" href="#" data-role="button" onclick="$(this).closest('form').submit()">
                <div class="tdcenter cellpadding"><i class="icon-ok nav-icon" style="color:Green"></i></div>
                <div class="tdcenter nospace">Submit</div>
            </a>
        </div>
    </div>
</div>
@code End Using End Code