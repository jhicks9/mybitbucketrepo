﻿@ModelType mobile.RecoverPasswordModel
    
@Code
    ViewData("Title") = "FPasswordEmail"
    Using Html.BeginForm
End Code

<div class="h4">Reset MyPassword</div>

<div class="nav-table center">
    <div class="trcenter" style="padding:3px">
        <div class="tdright">@Html.LabelFor(Function(model) model.Email)</div>
        <div class="tdleft">@Html.TextBoxFor(Function(model) model.Email, New With {.placeholder = "Email"})</div>
    </div>
    <div class="trcenter" style="padding:3px">
        <div class="tdright"></div>
        <div class="tdleft">@Html.ValidationMessageFor(Function(model) model.Email)</div>
    </div>

    <div class="trcenter" style="padding:3px">
        <div class="tdright">@Html.LabelFor(Function(model) model.confirmEmail)</div>
        <div class="tdleft">@Html.TextBoxFor(Function(model) model.confirmEmail, New With {.placeholder = "Confirm Email"})</div>
    </div>
    <div class="trcenter" style="padding:3px">
        <div class="tdleft"></div>
        <div class="tdleft">@Html.ValidationMessageFor(Function(model) model.confirmEmail)</div>
    </div>
</div>
<div style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>

<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            <a class="trcenter" href='@Url.Action("ResetAccount", "Account")'>
                <div class="tdcenter cellpadding"><i class="icon-remove nav-icon" style="color:Red"></i></div>
                <div class="tdcenter nospace">Cancel</div>
            </a>
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            <a class="trcenter" href="#" data-role="button" onclick="$(this).closest('form').submit()">
                <div class="tdcenter cellpadding"><i class="icon-ok nav-icon" style="color:Green"></i></div>
                <div class="tdcenter nospace">Submit</div>
            </a>
        </div>
    </div>
</div>
@code End Using End Code