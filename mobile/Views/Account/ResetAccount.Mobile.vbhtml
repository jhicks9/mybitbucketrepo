﻿@Code
    ViewData("Title") = "ResetAccount"
End Code

<div class="h4">Reset MyAccount Password</div>

<ul data-role="listview" data-inset="true">
    <li data-icon="false">
        <a href='@Url.Action("RPasswordEmail", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-envelope"></i>&nbsp;Reset by Email</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("RPassword", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-lock"></i>&nbsp;Reset by Account</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("RUserName", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-user"></i>&nbsp;Recover User Name</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
</ul>