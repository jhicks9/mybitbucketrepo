﻿@ModelType Ienumerable(Of mobile.BillingPackagesModel)

@Code
    ViewData("Title") = "ListAccounts"
    Dim actionname As String = "Payments"
    Select Case Request.QueryString("actionname")
        Case "payments"
            actionname = "Payments"
        Case "statements"
            actionname = "Statements"
        Case "usage"
            actionname = "Usage"
    End Select
    Dim acctInfo As String = ""
End Code

<div class="h4">Select an Account</div>
<div class="nav-table center">
@code 
    For Each account In Model
      acctInfo = account.customer_tkn & "-" & account.billing_pkg_tkn & "-" & account.checkdigit_tkn     
End code
    <a class="trcenter" href='@Url.Action(actionname, "Account", New With {.acct = acctInfo})'>
        <div class="tdcenter"><i class="icon-folder-open-alt nav-icon"></i></div>
        <div class="tdleft nospace">@account.customer_tkn-@account.billing_pkg_tkn-@account.checkdigit_tkn</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
@code Next End Code
</div>