﻿@ModelType mobile.ChangeEmailModel 
    
@Code
    ViewData("Title") = "ChangeEmail"
        Using Html.BeginForm
End Code

<div class="h4">Change Email</div>
@Html.TextBoxFor(Function(model) model.currentEmail, New With {Key .disabled = "disabled"})
@Html.HiddenFor(Function(model) model.currentEmail)
    
@Html.TextBoxFor(Function(model) model.newEmail, New With {Key .placeholder = "New Email"})
@Html.ValidationMessageFor(Function(model) model.newEmail)

<div class="tr" style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>
<fieldset class="ui-grid-a">
    <legend></legend>
    <div class="ui-block-a">
        <a href='@Url.Action("Index", "Account")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
    </div>
    <div class="ui-block-b">
        <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Submit</a>
    </div>
</fieldset>
@code End Using End Code