﻿@ModelType mobile.AccountUpdateType
    
@Code
    ViewData("Title") = "Updated"
End Code

<div class="h4">Update @Model.type</div>
<br />
<div class="tr" style="width:100%;text-align:center;padding:0;">@Model.description </div>
<br />
<div class="tr" style="width:100%;text-align:center;padding:0;">
    <a href="@Url.Action("Index", "Account")" data-role="button">Continue</a>
</div>