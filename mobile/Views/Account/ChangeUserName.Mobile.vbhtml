﻿@ModelType mobile.ChangeUserNameModel

@Code
    ViewData("Title") = "ChangeUserName"
    Using Html.BeginForm
End Code

<div class="h4">Change User Name</div>
@Html.TextBoxFor(Function(model) model.currentUserName, New With {Key .disabled = "disabled"})
@Html.HiddenFor(Function(model) model.currentUserName)

@Html.TextBoxFor(Function(model) model.newUserName, New With {Key .placeholder = "New User Name"})
@Html.ValidationMessageFor(Function(model) model.newUserName)

<div class="tr" style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>
<fieldset class="ui-grid-a">
    <legend></legend>
    <div class="ui-block-a">
        <a href='@Url.Action("Index", "Account")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
    </div>
    <div class="ui-block-b">
        <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Submit</a>
    </div>
</fieldset>
@code End Using End Code
