﻿@ModelType mobile.ChangePasswordModel

@Code
    ViewData("Title") = "Change Password"
    Using Html.BeginForm()
End Code

<div class="h4">Change Password</div>
<br />
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Html.PasswordFor(Function(m) m.OldPassword, New With {Key .placeholder = "Current Password"})
@Html.ValidationMessageFor(Function(m) m.OldPassword)

@Html.PasswordFor(Function(m) m.NewPassword, New With {Key .placeholder = "New Password"})
@Html.ValidationMessageFor(Function(m) m.NewPassword)

@Html.PasswordFor(Function(m) m.ConfirmPassword, New With {Key .placeholder = "Confirm New Password"})
@Html.ValidationMessageFor(Function(m) m.ConfirmPassword)

<div class="tr" style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>    
<fieldset class="ui-grid-a">
    <legend></legend>
    <div class="ui-block-a">
        <a href='@Url.Action("Index", "Account")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
    </div>
    <div class="ui-block-b">
        <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Submit</a>
    </div>
</fieldset>
@Code End Using End code
