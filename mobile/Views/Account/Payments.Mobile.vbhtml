﻿@modeltype mobile.PaymentsViewModel 

@Code
    ViewData("Title") = "Payments"
End Code

<div class="h4">$&nbsp;MyPayments</div>
<br />
<table class="nav-table fullwidth">
    <tr>
        <td class="tdleft">@Model.accountnumber</td>
        <td class="tdright">Due:&nbsp;@Model.paymentdue</td>
    </tr>
</table>

@code   If Model.paymentinfo Is Nothing Then End code
<br />Data not available at this time.<br /><br />
@code   Else End code
<table class="data-table">
    <tr class="ui-bar-f">
        <td class="tdcellcenter"><i class="icon-calendar"></i>&nbsp;Date</td>
        <td class="tdcellcenter">$&nbsp;Amount</td>
    </tr>
        @code For Each payment In Model.paymentinfo End code
    <tr>
        <td class="tdcellcenter">@payment.paymentdate</td>
        <td class="tdcellcenter">@payment.paymentamount</td>
    </tr>
        @code Next End Code
</table>
@code   End If end code

<div class="ui-grid-a">
    <div class="ui-block-a">
        <a href="@Url.Action("Payments", "Account", New With {.acct = Model.accountnumber, .page = (Model.paymentinfo.PageIndex - 1)})" data-role="button" data-mini="false" data-icon="false"
            @code If Not (Model.paymentinfo.HasPreviousPage) Then End code
                class="ui-disabled"
            @code End If End Code
        >
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-chevron-left nav-icon"></i></td>
                <td class="tdcenter fullwidth">Previous</td>
            </tr></table>
        </a>
    </div>
    <div class="ui-block-b">
        <a href="@Url.Action("Payments", "Account", New With {.acct = Model.accountnumber, .page = (Model.paymentinfo.PageIndex + 1)})" data-role="button" data-mini="false" data-icon"none"
        @code If Not (Model.paymentinfo.HasNextPage) Then End code
            class="ui-disabled"
        @code End If End code
        >
            <table class="nav-table"><tr>
                <td class="tdcenter fullwidth">Next</td>
                <td class="tdright"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </div>
</div>