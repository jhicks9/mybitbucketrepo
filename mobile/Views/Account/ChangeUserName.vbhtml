﻿@ModelType mobile.ChangeUserNameModel

@Code
    ViewData("Title") = "ChangeUserName"
    Using Html.BeginForm
End Code
<div class="h4">Change User Name</div>
<div class="nav-table center">
    <div class="trcenter" style="padding:0">
        <div class="td" style="padding:3px">@Html.LabelFor(Function(model) model.currentUserName, "Current User Name")</div>
        <div class="td" style="padding:3px">
            @Html.TextBoxFor(Function(model) model.currentUserName, New With {Key .disabled = "disabled"})
            @Html.HiddenFor(Function(model) model.currentUserName)        
        </div>
    </div>
    <div class="trcenter" style="padding:0">
        <div class="td" style="padding:3px">@Html.LabelFor(Function(model) model.newUserName, "New User Name")</div>
        <div class="td" style="padding:3px">
            @Html.TextBoxFor(Function(model) model.newUserName, New With {Key .placeholder = "new User Name"})        
            @Html.ValidationMessageFor(Function(model) model.newUserName)
        </div>
    </div>
</div>
<div class="trcenter" style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>

<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            <a class="trcenter" href='@Url.Action("Index", "Account")'>
                <div class="tdcenter cellpadding"><i class="icon-remove nav-icon" style="color:Red"></i></div>
                <div class="tdcenter nospace">Cancel</div>
            </a>
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            <a class="trcenter" href="#" data-role="button" onclick="$(this).closest('form').submit()">
                <div class="tdcenter cellpadding"><i class="icon-ok nav-icon" style="color:Green"></i></div>
                <div class="tdcenter nospace">Submit</div>
            </a>
        </div>
    </div>
</div>
@code End Using End Code
