﻿@ModelType mobile.LogonModel

@Code
    ViewData("Title") = "Your Account"
End Code

<p>Logged in as <strong>@User.Identity.Name</strong>.</p>

<ul data-role="listview" data-inset="true">
    <li data-icon="false">
        <a href='@Url.Action("ListAccounts", "Account", New With {.actionname = "payments"})'>
            <table class="nav-table"><tr>
                <td class="tdleft"><span style="padding-left:5px;padding-right:5px;">$</span>&nbsp;MyPayments</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("ListAccounts", "Account", New With {.actionname = "statements"})'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-file-alt"></i>&nbsp;MyStatements</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("ListAccounts", "Account", New With {.actionname = "usage"})'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-bar-chart"></i>&nbsp;MyUsage</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("ChangePassword", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-lock"></i>&nbsp;Change MyPassword</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("ChangeUserName", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-user"></i>&nbsp;Change MyUsername</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("ChangeEmail", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-envelope"></i>&nbsp;Change MyEmail</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("LogOff", "Account")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-unlock"></i>&nbsp;Log out</td>
                <td class="tdright"></td>
            </tr></table>
        </a>
    </li>
</ul>