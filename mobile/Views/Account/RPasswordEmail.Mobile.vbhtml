﻿@ModelType mobile.RecoverPasswordModel
    
@Code
    ViewData("Title") = "RPasswordEmail.Mobile"
        Using Html.BeginForm
End Code

<div class="h4">Reset MyPassword</div>
@Html.TextBoxFor(Function(model) model.Email, New With {Key .placeholder = "Email"})
@Html.ValidationMessageFor(Function(model) model.Email)
@Html.TextBoxFor(Function(model) model.confirmEmail, New With {Key .placeholder = "Confirm Email"})
@Html.ValidationMessageFor(Function(model) model.confirmEmail)
<div class="fullwidth" style="text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>
<fieldset class="ui-grid-a">
    <legend></legend>
    <div class="ui-block-a">
        <a href='@Url.Action("ResetAccount", "Account")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
    </div>
    <div class="ui-block-b">
        <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Submit</a>
    </div>
</fieldset>
@code End Using End Code