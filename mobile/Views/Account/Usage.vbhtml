﻿@ModelType mobile.UsageViewModel

@Code
    ViewData("Title") = "Usage"
    Dim actTokens As String = ""
    Using Html.BeginForm
End Code

<div class="h4"><i class="icon-bar-chart"></i>&nbsp;MyUsage</div><br />
@Model.accountnumber 
<div class="nav-table center">
@code
    For Each accountpackage In Model.accountpackageinfo
        actTokens = accountpackage.customer_tkn & "-" & accountpackage.customer_acct_tkn & "-" & accountpackage.account_pkg_tkn & "-"
    actTokens &= accountpackage.srvce_pnt_assn_tkn & "-" & accountpackage.service_point_tkn & "-" & accountpackage.premise_tkn & "-" & Left(accountpackage.account_pkg_desc, 1)
End code
    <a class="trcenter" href='@Url.Action("Usage", "Account", New With {.actpkg = actTokens})'>
        <div class="tdleft cellpadding">
            @accountpackage.customer_tkn-@accountpackage.customer_acct_tkn-@accountpackage.account_pkg_tkn&nbsp;&nbsp;@accountpackage.account_pkg_desc&nbsp;service
            <br />
            @accountpackage.ln1_addr @accountpackage.unit_code&nbsp;&nbsp;@accountpackage.city_name,&nbsp;@accountpackage.state_code
        </div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
@code
    Next
End Code
</div>

<table class="data-table" style="background-color:transparent">
@code
    Dim icon As String = ""
    For Each usage In Model.usageinfo
        Select Case usage.uom
            Case "KWH"
                icon = "icon-bolt"
            Case "KGAL"
                icon = "icon-tint"
            Case "CCF"
                icon = "icon-fire"
            Case Else
                icon = "icon-bolt" 
        End Select
end code

    <tr>
        <th colspan="2" style="text-align:left;background-color:#afafaf"><b><i class="icon-calendar"></i>&nbsp;@usage.read_date</b></th>
        <th colspan="2" style="text-align:right;background-color:#afafaf"><b>@usage.usage_days_cnt</b>&nbsp;usage days</th>
    </tr>
    <tr>
        <td rowspan="2" style="padding-right:5px"><i class="@icon nav-icon"></i></td>
        <td>Meter Read:</td><td>@usage.usage_read_num</td>
        <td>&nbsp;Estimated:&nbsp;@usage.estimate_ind</td>
    </tr>
    <tr>
        <td>Usage Amount:</td>
        <td>@usage.usage_num&nbsp;@usage.uom</td>
        <td></td>
    </tr>
@code
        If (IsNumeric(usage.demand_read)) And (System.Convert.ToSingle(usage.demand_read) > 0) Or (IsNumeric(usage.demand_billed)) And (System.Convert.ToSingle(usage.demand_billed) > 0) Then
End Code
        <tr>
            <td></td>
            <td>Demand Read:</td>
            <td>@usage.demand_read</td>
            <td>Demand Billed:&nbsp;@usage.demand_billed</td>
        </tr>
@code 
        End If
    Next
end Code
</table>

<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            @code If Not (Model.usageinfo.HasPreviousPage) Then End code
            <div class="trcenter">
                <div class="tdleft disabled cellpadding"><i class="icon-chevron-left nav-icon"></i></div>
                <div class="tdcenter disabled">Previous</div>
            </div>
            @code Else End Code
            <a href="@Url.Action("Usage", "Account", New With {.actpkg = Model.accountpackage, .page = (Model.usageinfo.PageIndex - 1)})">
                <div class="tdright cellpadding"><i class="icon-chevron-left nav-icon"></i></div>
                <div class="tdcenter">Previous</div>
            </a>
            @code End If End Code
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            @code If Not (Model.usageinfo.HasNextPage) Then End code
            <div class="trcenter">
                <div class="tdleft disabled">Next</div>
                <div class="tdright cellpadding disabled"><i class="icon-chevron-right nav-icon"></i></div>
            </div>
            @code Else End Code
            <a href="@Url.Action("Usage", "Account", New With {.actpkg = Model.accountpackage, .page = (Model.usageinfo.PageIndex + 1)})">
                <div class="tdleft">Next</div>
                <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
            </a>
            @code End If End code
        </div>
    </div>
</div>
@code   
    End Using
end code