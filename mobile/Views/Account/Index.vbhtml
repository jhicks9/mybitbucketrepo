﻿@ModelType mobile.LogonModel

@Code
    ViewData("Title") = "Your Account"
End Code

<p>Logged in as <strong>@User.Identity.Name</strong>.</p>
<div class="nav-table center">
    <a class="trcenter" href='@Url.Action("ListAccounts", "Account", New With {.actionname = "payments"})'>
        <div class="tdcenter nav-icon" style="padding-left:5px;padding-right:5px">$</div>
        <div class="tdleft nospace">MyPayments</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("ListAccounts", "Account", New With {.actionname = "statements"})'>
        <div class="tdcenter"><i class="icon-file-alt nav-icon"></i></div>
        <div class="tdleft nospace">MyStatements</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("ListAccounts", "Account", New With {.actionname = "usage"})'>
        <div class="tdcenter"><i class="icon-bar-chart nav-icon"></i></div>
        <div class="tdleft nospace">MyUsage</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("ChangePassword", "Account")'>
        <div class="tdcenter"><i class="icon-lock nav-icon"></i></div>
        <div class="tdleft nospace">Change MyPassword</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("ChangeUserName", "Account")'>
        <div class="tdcenter"><i class="icon-user nav-icon"></i></div>
        <div class="tdleft nospace">Change MyUsername</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("ChangeEmail", "Account")'>
        <div class="tdcenter"><i class="icon-envelope nav-icon"></i></div>
        <div class="tdleft nospace">Change MyEmail Address</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("LogOff", "Account")' style="color:#00BFFF">
        <div class="tdcenter"><i class="icon-unlock nav-icon"></i></div>
        <div class="tdleft nospace">Log out</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
</div>