﻿@ModelType mobile.LogOnModel

@Code
    ViewData("Title") = "Log On"
End Code

<div class="col-grid-a">
    <div class="col-block-a" style="text-align:left">
        <div class="h4">Log In</div>
    </div>
    <div class="col-block-b" style="text-align:right">
        <a href='@Url.Action("Register", "Account")' style="text-decoration:underline">Register</a>
        <i class="icon-circle nav-icon" style="font-size:.8em;padding:0 5px 0 5px"></i>
        <a href='@Url.Action("ResetAccount", "Account")' style="text-decoration:underline">Forgot Password</a>
    </div>
</div>
<br />

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@code Using Html.BeginForm("Logon", "Account", FormMethod.Post) End code
<div class="nav-table center">
    <div class="tr" style="padding:0">
        <div class="td" style="padding:0">@Html.LabelFor(Function(m) m.UserName)</div>
    </div>
    <div class="tr" style="padding:0">
        <div class="td" style="padding:0">@Html.TextBoxFor(Function(m) m.UserName, New With {Key .placeholder = "User Name"})</div>
    </div>
    <div class="tr" style="padding:0">
        <div class="td" style="padding:0">@Html.ValidationMessageFor(Function(m) m.UserName)</div>
    </div>
    <div class="tr" style="padding:0">
        <div class="td" style="padding:0">@Html.LabelFor(Function(m) m.Password)</div>
    </div>
    <div class="tr" style="padding:0">
        <div class="td" style="padding:0">@Html.PasswordFor(Function(m) m.Password, New With {Key .placeholder = "Password"})</div>
    </div>
    <div class="tr" style="padding:0">
        <div class="td" style="padding:0">@Html.ValidationMessageFor(Function(m) m.Password)</div>
    </div>
    <div class="tr" style="padding:0;display:none">
        <div class="td" style="padding:0">@Html.CheckBoxFor(Function(m) m.RememberMe) &nbsp;@Html.LabelFor(Function(m) m.RememberMe)</div>
    </div>
    <a class="tr" style="padding:0" href="#" data-role="button" onclick="$(this).closest('form').submit()">
        <div class="td"><i class="icon-ok nav-icon" style="color:Green"></i>&nbsp;Log In</div>
    </a>
</div>
@code End Using end code
<div style="width:100%;text-align:center;padding:0;margin-left:auto;margin-right:auto;">
    @Html.ValidationSummary("", "Login was unsuccessful.")
</div>