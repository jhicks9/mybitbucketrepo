﻿@ModelType mobile.AccountUpdateType
    
@Code
    ViewData("Title") = "Updated"
End Code

<div class="h4">Update @Model.type</div>
<br />@Model.description<br />
<a href="@Url.Action("Index", "Account")" data-role="button">Continue</a>