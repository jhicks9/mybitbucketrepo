﻿@ModelType mobile.UsageViewModel

@Code
    ViewData("Title") = "Usage"
    Dim actTokens As String = ""
    Using Html.BeginForm
End Code

<div class="h4"><i class="icon-bar-chart"></i>&nbsp;MyUsage</div><br />
@Model.accountnumber 
<ul data-role="listview" data-inset="true" data-dividertheme="f">
@code
    For Each accountpackage In Model.accountpackageinfo
        actTokens = accountpackage.customer_tkn & "-" & accountpackage.customer_acct_tkn & "-" & accountpackage.account_pkg_tkn & "-"
        actTokens &= accountpackage.srvce_pnt_assn_tkn & "-" & accountpackage.service_point_tkn & "-" & accountpackage.premise_tkn & "-" & Left(accountpackage.account_pkg_desc, 1)
End code
    <li data-icon="false">
        <a href='@Url.Action("Usage", "Account", New With {.actpkg = actTokens})'>
            <table class="nav-table"><tr>
                <td class="tdleft">
                        <div class="data-list"><b>@accountpackage.customer_tkn-@accountpackage.customer_acct_tkn-@accountpackage.account_pkg_tkn&nbsp;&nbsp;@accountpackage.account_pkg_desc&nbsp;service</b><br />
                        @accountpackage.ln1_addr @accountpackage.unit_code&nbsp;&nbsp;@accountpackage.city_name,&nbsp;@accountpackage.state_code</div>
                </td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
@code
    Next
End Code
</ul>

@code
    Dim icon As String = ""
    For Each usage In Model.usageinfo
        Select Case usage.uom
            Case "KWH"
                icon = "icon-bolt"
            Case "KGAL"
                icon = "icon-tint"
            Case "CCF"
                icon = "icon-fire"
            Case Else
                icon = "icon-bolt" 
        End Select
end code
            <table class="data-table">
                <tr class="ui-bar-f">
                    <td colspan="2" class="tdleft" style="border:none"><i class="icon-calendar"></i>&nbsp;@usage.read_date</td>
                    <td colspan="2" class="tdright" style="border:none">@usage.usage_days_cnt&nbsp;usage days</td> 
                </tr>
                <tr>
                    <td rowspan="2" style="border:none"><i class="@icon nav-icon"></i></td>
                    <td style="border:none"><strong>Meter Read:</strong></td>
                    <td style="border:none">@usage.usage_read_num</td>
                    <td style="border:none"><strong>&nbsp;Estimated:</strong>&nbsp;@usage.estimate_ind</td>
                </tr>
                <tr>
                    <td style="border:none"><strong>Usage Amount:</strong></td>
                    <td style="border:none">@usage.usage_num&nbsp;@usage.uom</td>
                    <td style="border:none"></td>
                </tr>
@code If (IsNumeric(usage.demand_read)) And (System.Convert.ToSingle(usage.demand_read) > 0) Or (IsNumeric(usage.demand_billed)) And (System.Convert.ToSingle(usage.demand_billed) > 0) Then End Code
                <tr>
                    <td style="border:none"></td>
                    <td style="border:none"><strong>Demand Read:</strong></td>
                    <td style="border:none">@usage.demand_read</td>
                    <td style="border:none"><strong>Demand Billed:</strong>@usage.demand_billed</td>
                </tr>
@code End If End Code
            </table>
@code    
    Next
end Code


<div class="ui-grid-a">
    <div class="ui-block-a">
        <a href="@Url.Action("Usage", "Account", New With {.actpkg = Model.accountpackage, .page = (Model.usageinfo.PageIndex - 1)})" data-role="button" data-mini="false" data-icon="false"
            @code If Not (Model.usageinfo.HasPreviousPage) Then End code
                class="ui-disabled"
            @code End If End Code
        >
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-chevron-left nav-icon"></i></td>
                <td class="tdcenter fullwidth">Previous</td>
            </tr></table>
        </a>
    </div>
    <div class="ui-block-b">
        <a href="@Url.Action("Usage", "Account", New With {.actpkg = Model.accountpackage, .page = (Model.usageinfo.PageIndex + 1)})" data-role="button" data-mini="false" data-icon"none"
        @code If Not (Model.usageinfo.HasNextPage) Then End code
            class="ui-disabled"
        @code End If End code
        >
            <table class="nav-table"><tr>
                <td class="tdcenter fullwidth">Next</td>
                <td class="tdright"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </div>
</div>
@code   
    End Using
end code