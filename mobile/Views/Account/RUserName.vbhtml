﻿@ModelType mobile.RecoverUserNameModel

@Code
    ViewData("Title") = "FUserName"
    Using Html.BeginForm
End Code

<div class="h4">Forgot User Name</div>
<table class="nav-table center">
<tr>
    <td>@Html.LabelFor(Function(model) model.custToken, "Account Number")</td>
    <td>
        <table style="border-collapse:collapse;"><tr style="padding:0">
            <td style="padding:0">@Html.TextBoxFor(Function(model) model.custToken, New With {Key .type = "tel", .style = "width:55px", .placeholder = "Account"})</td>
            <td><b>-</b></td>
            <td>@Html.TextBoxFor(Function(model) model.billPkgToken , New With {Key .type = "tel", .style = "width:30px"})</td>
            <td><b>-</b></td>
            <td>@Html.TextBoxFor(Function(model) model.chkDigitToken, New With {Key .type = "tel", .style = "width:30px"})</td>
        </tr></table>
    </td>
    <td>@Html.ValidationMessageFor(Function(m) m.custToken)</td>
</tr>
</table>

<div style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>
<div class="col-grid-a">
    <div class="col-block-a">
        <div class="nav-table center">
            <a class="trcenter" href='@Url.Action("ResetAccount", "Account")'>
                <div class="tdcenter cellpadding"><i class="icon-remove nav-icon" style="color:Red"></i></div>
                <div class="tdcenter nospace">Cancel</div>
            </a>
        </div>
    </div>
    <div class="col-block-b">
        <div class="nav-table center">
            <a class="trcenter" href="#" data-role="button" onclick="$(this).closest('form').submit()">
                <div class="tdcenter cellpadding"><i class="icon-ok nav-icon" style="color:Green"></i></div>
                <div class="tdcenter nospace">Submit</div>
            </a>
        </div>
    </div>
</div>
@code End Using End Code