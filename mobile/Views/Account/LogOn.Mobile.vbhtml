﻿@ModelType mobile.LogOnModel

@Code
    ViewData("Title") = "Log On"
End Code

<div class="ui-grid-a">
    <div class="ui-block-a" style="width:20%">
        <div class="h4">Log In</div>
    </div>
    <div class="ui-block-b" style="width:80%;text-align:right;color:#113962">
        <a href='@Url.Action("Register", "Account")' data-role='none' style="text-decoration:underline">Register</a>
        <i class="icon-circle" style="font-size:.8em;padding:0 5px 0 5px"></i>
        <a href='@Url.Action("ResetAccount", "Account")' data-role='none' style="text-decoration:underline">Forgot Password</a>
    </div>
</div>
<br />

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Using Html.BeginForm("Logon", "Account", FormMethod.Post, New With {.data_ajax = "false"})
    
    @Html.TextBoxFor(Function(m) m.UserName, New With {Key .placeholder = "User Name"})
    @Html.ValidationMessageFor(Function(m) m.UserName)
    
    @Html.PasswordFor(Function(m) m.Password, New With {Key .placeholder = "Password"})
    @Html.ValidationMessageFor(Function(m) m.Password)
    
    @<div style="display:none">
        @Html.CheckBoxFor(Function(m) m.RememberMe)
        @Html.LabelFor(Function(m) m.RememberMe)
    </div>
    @<p><input type="submit" value="Log In" /></p>
End Using

@Html.ValidationSummary("", "Login was unsuccessful.")