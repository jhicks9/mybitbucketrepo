﻿@Code
    ViewData("Title") = "ResetAccount"
End Code

<div class="h4">Reset MyAccount Password</div>

<div class="nav-table center">
    <a class="trcenter" href='@Url.Action("RPasswordEmail", "Account")'>
        <div class="tdcenter"><i class="icon-envelope nav-icon"></i></div>
        <div class="tdleft nospace">Reset by Email</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("RPassword", "Account")'>
        <div class="tdcenter"><i class="icon-lock nav-icon"></i></div>
        <div class="tdleft nospace">Reset by Account</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("RUserName", "Account")'>
        <div class="tdcenter"><i class="icon-user nav-icon"></i></div>
        <div class="tdleft nospace">Recover User Name</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
</div>
