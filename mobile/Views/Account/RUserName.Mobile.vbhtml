﻿@ModelType mobile.RecoverUserNameModel

@Code
    ViewData("Title") = "FUserName"
    Using Html.BeginForm
End Code

<div class="h4">Forgot User Name</div>
<table class="fullwidth">
    <tr>
        <td style="">
            @Html.TextBoxFor(Function(model) model.custToken, New With {Key .type = "tel", .placeholder = "Account", .style = ""})
        </td>
        <td><b>-</b></td>
        <td style="width:25%">
            @Html.TextBoxFor(Function(model) model.billPkgToken, New With {Key .type = "tel", .style = ""})                
        </td>
        <td><b>-</b></td>
        <td style="width:25%">
            @Html.TextBoxFor(Function(model) model.chkDigitToken, New With {Key .type = "tel", .style = ""})                
        </td>
    </tr>
    <tr>
        <td colspan="5">@Html.ValidationMessageFor(Function(m) m.custToken)</td>
    </tr>
</table>

<div style="width:100%;text-align:center;padding:0;">
    @Html.ValidationSummary(True)
</div>

<fieldset class="ui-grid-a">
    <legend></legend>
    <div class="ui-block-a">
        <a href='@Url.Action("ResetAccount", "Account")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
    </div>
    <div class="ui-block-b">
        <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Submit</a>
    </div>
</fieldset>
@code End Using End Code