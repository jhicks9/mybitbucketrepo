﻿@ModelType Ienumerable(Of mobile.BillingPackagesModel)

@Code
    ViewData("Title") = "ListAccounts"
    Dim actionname As String = "Payments"
    Select Case Request.QueryString("actionname")
        Case "payments"
            actionname = "Payments"
        Case "statements"
            actionname = "Statements"
        Case "usage"
            actionname = "Usage"
    End Select
    Dim acctInfo As String = ""
End Code

<div class="h4">Select an Account</div>
<ul data-role="listview" data-inset="true">
@code 
    For Each account In Model
      acctInfo = account.customer_tkn & "-" & account.billing_pkg_tkn & "-" & account.checkdigit_tkn     
End code
    <li data-icon="false">
        <a href='@Url.Action(actionname, "Account", New With {.acct = acctInfo})'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-folder-open-alt"></i>&nbsp;@account.customer_tkn-@account.billing_pkg_tkn-@account.checkdigit_tkn</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
@code Next End Code
</ul>