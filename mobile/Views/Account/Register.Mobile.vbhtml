﻿@ModelType mobile.RegisterModel

@Code
    ViewData("Title") = "Register MyAccount"
    Using Html.BeginForm
End Code
    <div class="h4">Register for MyAccount</div>

    <div class="ui-body ui-body-f">
        Verify Account
        <table class="nav-table fullwidth">
            <tr>
                <td>
                    @Html.TextBoxFor(Function(model) model.custToken, New With {Key .type = "tel", .placeholder = "Account", .style = ""})                
                </td>
                <td><b>-</b></td>
                <td>
                    @Html.TextBoxFor(Function(model) model.billPkgToken, New With {Key .type = "tel", .style = ""})                
                </td>
                <td><b>-</b></td>
                <td>
                    @Html.TextBoxFor(Function(model) model.chkDigitToken, New With {Key .type = "tel", .style = ""})                
                </td>
                <td>@Html.ValidationMessageFor(Function(model) model.custToken)</td>
            </tr>
        </table>
        @Html.TextBoxFor(Function(model) model.name, New With {Key .placeholder = "Name"})
        @Html.ValidationMessageFor(Function(model) model.name)
        @Html.TextBoxFor(Function(model) model.ssnToken, New With {Key .type = "tel", .placeholder = "SSN"})
        @Html.ValidationMessageFor(Function(model) model.ssnToken)
    </div>

    @Html.TextBoxFor(Function(model) model.UserName, New With {Key .placeholder = "User Name"})
    @Html.ValidationMessageFor(Function(model) model.UserName)

    @Html.PasswordFor(Function(m) m.Password, New With {.placeholder = "Password"})
    @Html.ValidationMessageFor(Function(m) m.Password)

    @Html.PasswordFor(Function(m) m.ConfirmPassword, New With {.placeholder = "Confirm Password"})
    @Html.ValidationMessageFor(Function(m) m.ConfirmPassword)

    @Html.TextBoxFor(Function(m) m.Email, New With {.placeholder = "Email"})
    @Html.ValidationMessageFor(Function(m) m.Email)

    <div style="width:100%;text-align:center;padding:0;">@Html.ValidationSummary(True)</div>

    <fieldset class="ui-grid-a">
        <legend></legend>
        <div class="ui-block-a">
            <a href='@Url.Action("Index", "Account")' data-role="button" data-theme="c"><i class="icon-remove" style="color:Red"></i>&nbsp;Cancel</a>
        </div>
        <div class="ui-block-b">
            <a href="#" data-role="button" onclick="$(this).closest('form').submit()"><i class="icon-ok" style="color:Green"></i>&nbsp;Register</a>
        </div>
    </fieldset>
@code End Using End Code