﻿@ModelType mobile.statementsViewModel

@Code
    ViewData("Title") = "Statements"
End Code

<div class="h4"><i class="icon-file-alt"></i>&nbsp;MyStatements</div>
<br />
@Model.accountnumber<br />
<table class="data-table" style="background-color:transparent">
    <tr>
        <th class="tdcenter"><i class="icon-calendar"></i>&nbsp;Statement Date</th>
        <th class="tdcenter"><b>$</b>&nbsp;Amount Due</th>
        <th class="tdcenter"><i class="icon-calendar"></i>&nbsp;Delinquent Date</th>
    </tr>
    @code For Each statement In Model.statementinfo End code
    <tr>
        <td class="tdcenter">
            <a href='@Url.Action("statementImage", "Account", New With {.stkn = statement.statementToken})'>@statement.statementdate</a>
        </td>
        <td class="tdcenter">@statement.amountdue</td>
        <td class="tdcenter">@statement.delinquentdate </td>
    </tr>
    @code Next End Code
</table>

@*disable ajax loading for this page so statement pdfs will load*@
<script type="text/javascript">
    $.mobile.ajaxEnabled = false;
</script>