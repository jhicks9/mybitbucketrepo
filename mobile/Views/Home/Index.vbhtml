﻿@Code
    ViewData("Title") = "Empire District"
End Code
<div class="nav-table center">
    <a class="trcenter" href='@Url.Action("Index", "Plocations")'>
        <div class="tdcenter"><i class="icon-map-marker nav-icon"></i></div>
        <div class="tdleft nospace">Payment Locations</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("Index", "Outages")'>
        <div class="tdcenter"><i class="icon-bolt nav-icon"></i></div>
        <div class="tdleft nospace">Outages</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    <a class="trcenter" href='@Url.Action("StockQuote", "Home")'>
        <div class="tdcenter"><i class="icon-bar-chart nav-icon"></i></div>
        <div class="tdleft nospace">Stock Quote</div>
        <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
    </a>
    @Html.Partial("_LogOnPartial")
</div>