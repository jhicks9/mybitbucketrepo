﻿@ModelType mobile.SendEmailModel

@Code
    ViewData("Title") = "Contacts"
End Code

@code If Request.QueryString("cntype") Is Nothing Then End code
    <div class="h4">Contact Us</div>
    <div class="nav-table center">
        <div class="trcenter" style="text-align:center;padding:5px;">
            <div class="tdcenter"></div>
            <div class="tdcenter">Select a Method</div>
            <div class="tdcenter"></div>
        </div>
        <a class="trcenter" href='@Url.Action("Contacts", "Home", New With {.cntype = "Email"})'>
            <div class="tdcenter"><i class="icon-envelope nav-icon"></i></div>
            <div class="tdleft nospace">Email</div>
            <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
        </a>
        <a class="trcenter" href='@Url.Action("Contacts", "Home", New With {.cntype = "Phone"})'>
            <div class="tdcenter"><i class="icon-phone nav-icon"></i></div>
            <div class="tdleft nospace">Phone</div>
            <div class="tdright cellpadding"><i class="icon-chevron-right nav-icon"></i></div>
        </a>
    </div>

@code Else
          If Request.QueryString("cntype") = "Email" Then End code
            <div class="h4">Contact us by Email</div>
            <table style="width:75%;margin-left:auto;margin-right:auto;">
                <tr><td class="nospace"><b><a href="mailto:customer.service@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Customer Service</a></b></td></tr>
                <tr><td>Contact regarding bills, payments, assistance agencies, service, or customer inquiries. To report an outage, please select the Report Outage contact information.</td></tr>
                <tr><td class="nospace"><b><a href='@Url.Action("ReportOutage", "Outages")'><i class="icon-envelope nav-icon"></i>&nbsp;Report Outage</a></b></td></tr>
                <tr><td>Please contact if you are experiencing a service interruption.</td></tr>
                <tr><td class="nospace"><b><a href="mailto:corp.communications@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Corporate Communications</a></b></td></tr>
                <tr><td>Send a message for questions about media stories or news releases.</td></tr>
                <tr><td class="nospace"><b><a href="mailto:mailto:investor.relations@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Investor Relations</a></b></td></tr>
                <tr><td>Please email for questions regarding stock or investing in Empire.</td></tr>
                <tr><td class="nospace"><b><a href="mailto:employment@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Employment</a></b></td></tr>
                <tr><td>Contact if interested in following up on an application, keeping your application file active, or for general human resources questions.</td></tr>
                <tr><td class="nospace"><b><a href="mailto:economic.development@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Economic Development</a></b></td></tr>
                <tr><td>Email if interested in learning about or exploring possible economic development opportunities with Empire.</td></tr>
                <tr><td class="nospace"><b><a href="mailto:vegetationmanagement@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Vegetation Management</a></b></td></tr>
                <tr><td>Send a message regarding trimming of branches and brush around power lines and system maintenance.</td></tr>
                <tr><td class="nospace"><b><a href="mailto:speakers.circuit@empiredistrict.com"><i class="icon-envelope nav-icon"></i>&nbsp;Speakers Circuit</a></b></td></tr>
                <tr><td>Contact if you need a speaker or presentation for your civic organization, community group, or for a children's or youth organization.</td></tr>
            </table>
          @Code End If
          If Request.QueryString("cntype") = "Phone" Then End code
            <div class="h4">Contact us by Phone</div>

            <div class="nav-table center">
                <a class="trcenter" href="tel:800-206-2300">
                    <div class="tdcenter"><i class="icon-phone nav-icon"></i></div>
                    <div class="tdleft nospace">800-206-2300</div>
                    <div class="tdleft nospace cellpadding" style="font-weight:normal">Electric & Water Service</div>
                </a>
                <a class="trcenter" href="tel:800-424-0427">
                    <div class="tdcenter"><i class="icon-phone nav-icon"></i></div>
                    <div class="tdleft nospace">800-424-0427</div>
                    <div class="tdleft nospace cellpadding" style="font-weight:normal">Gas Service</div>
                </a>
                <a class="trcenter" href="tel:800-406-9220">
                    <div class="tdcenter" style="color:#00BFFF"><i class="icon-phone nav-icon"></i></div>
                    <div class="tdleft nospace" style="color:#00BFFF">800-406-9220</div>
                    <div class="tdleft nospace cellpadding" style="font-weight:normal">Report a Gas Leak</div>
                </a>
                <a class="trcenter" href="tel:417-625-5100">
                    <div class="tdcenter"><i class="icon-phone nav-icon"></i></div>
                    <div class="tdleft nospace">417-625-5100</div>
                    <div class="tdleft nospace cellpadding" style="font-weight:normal">Corporate Office (for vendors)</div>
                </a>
            </div>
          @code End If
          End Code
@code End If End Code

