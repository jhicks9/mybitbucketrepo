﻿@Code
    ViewData("Title") = "Empire District"
End Code

<ul data-role="listview" data-inset="true">
    <li data-icon="false">
        <a href='@Url.Action("Index", "Plocations")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-map-marker"></i>&nbsp;Payment Locations</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("Index", "Outages")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-bolt"></i>&nbsp;Outages</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">
        <a href='@Url.Action("StockQuote", "Home")'>
            <table class="nav-table"><tr>
                <td class="tdleft"><i class="icon-bar-chart"></i>&nbsp;Stock Quote</td>
                <td class="tdright fullwidth"><i class="icon-chevron-right nav-icon"></i></td>
            </tr></table>
        </a>
    </li>
    <li data-icon="false">@Html.Partial("_LogOnPartial.Mobile")</li>
</ul>