﻿@ModelType mobile.StockQuote

@Code
    ViewData("Title") = "StockQuote"
End Code

<div class="ui-body ui-body-f">
    <div class="h4">@Model.symbol&nbsp;Stock Quote</div>
    <div data-role="collapsible">
        <h3>
            @Model.price&nbsp;
    @code
        If Model.trend = "down" Then
    End code
            <span style='color:#EE0000'>@Model.change</span>
    @code
        ElseIf Model.trend = "up" Then
    End code
            <span style='color:#008000'>@Model.change</span>
    @code
        Else
    End code
            <span style='color:#000000'>@Model.change</span>
    @code    
        End If
    End Code
        </h3>
        <table class="nav-table">
            <tr><td class="tdleft">Change</td><td class="tdleft">@Model.change </td></tr>
            <tr><td class="tdleft">Previous Close</td><td class="tdleft">@Model.previousclose </td></tr>
            <tr><td class="tdleft">Open</td><td class="tdleft">@Model.open </td></tr>
            <tr><td class="tdleft">Day Range</td><td class="tdleft">@Model.dayrange </td></tr>
            <tr><td class="tdleft">52wk Range</td><td class="tdleft">@Model.yearrange </td></tr>
            <tr><td class="tdleft">Volume</td><td class="tdleft">@Model.volume </td></tr>
            <tr><td class="tdleft">Market Cap</td><td class="tdleft">@Model.market </td></tr>
        </table> 
    </div>
    <br />
    @Model.time
</div>