﻿@ModelType mobile.StockQuote

@Code
    ViewData("Title") = "StockQuote"
End Code

<div class="h4">@Model.symbol&nbsp;Stock Quote</div>
@code
    If Model.time = "Not available at this time" Then
    Else
End Code
        <div style="text-align:center">
            <h3>
                        @Model.price&nbsp;
                @code
                    If Model.trend = "down" Then
                End code
                        <span style='color:#EE0000'>@Model.change</span>
                @code
                    ElseIf Model.trend = "up" Then
                End code
                        <span style='color:#008000'>@Model.change</span>
                @code
                    Else
                End code
                        <span style='color:#000000'>@Model.change</span>
                @code    
                    End If
                End Code
            </h3>
        </div>

        <div class="col-grid-a">
            <div class="col-block-a">
                <div style="text-align:right;padding-right:5px">
                    Change<br />
                    Previous Close<br />
                    Open<br />
                    Day Range<br />
                    52wk Range<br />
                    Volume<br />
                    Market Cap
                </div>
            </div>
            <div class="col-block-b">
                <div style="text-align:left;padding-left:5px">
                    @Model.change <br />
                    @Model.previousclose<br />
                    @Model.open<br />
                    @Model.dayrange<br />
                    @Model.yearrange<br />
                    @Model.volume<br />
                    @Model.market
                </div>
            </div>
        </div>  
@code
    End If
End Code
<br />
<div style="text-align:center">@Model.time</div>
<br />