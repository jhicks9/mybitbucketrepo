﻿@ModelType mobile.VersionViewModel
    
@Code
    ViewData("Title") = "About Us"
End Code

<div class="h4">About Us</div>
<br />
<div>
    Founded in October 1909 as a part of Cities Services Company, The Empire District Electric Company is an investor-owned, regulated utility company, based in Joplin, Missouri, that provides electric, natural gas (through its wholly owned subsidiary, The Empire District Gas Company), and water service, with approximately 216,000 customers in Missouri, Kansas, Oklahoma, and Arkansas. A subsidiary of the Company also provides fiber optic services. Empire has been listed on the New York Stock Exchange under EDE since 1946.
</div>
<p style="text-align:center">
    Mobile Application&nbsp;v @Model.version<br />
    @Model.builddate.ToString<Br />
    Copyright © 2007-@DateTime.Now.Year.ToString
</p>